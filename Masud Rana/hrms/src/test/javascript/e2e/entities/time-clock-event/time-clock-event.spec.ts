import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { TimeClockEventComponentsPage, TimeClockEventDeleteDialog, TimeClockEventUpdatePage } from './time-clock-event.page-object';

const expect = chai.expect;

describe('TimeClockEvent e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let timeClockEventComponentsPage: TimeClockEventComponentsPage;
  let timeClockEventUpdatePage: TimeClockEventUpdatePage;
  let timeClockEventDeleteDialog: TimeClockEventDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load TimeClockEvents', async () => {
    await navBarPage.goToEntity('time-clock-event');
    timeClockEventComponentsPage = new TimeClockEventComponentsPage();
    await browser.wait(ec.visibilityOf(timeClockEventComponentsPage.title), 5000);
    expect(await timeClockEventComponentsPage.getTitle()).to.eq('hrmsApp.timeClockEvent.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(timeClockEventComponentsPage.entities), ec.visibilityOf(timeClockEventComponentsPage.noResult)),
      1000
    );
  });

  it('should load create TimeClockEvent page', async () => {
    await timeClockEventComponentsPage.clickOnCreateButton();
    timeClockEventUpdatePage = new TimeClockEventUpdatePage();
    expect(await timeClockEventUpdatePage.getPageTitle()).to.eq('hrmsApp.timeClockEvent.home.createOrEditLabel');
    await timeClockEventUpdatePage.cancel();
  });

  it('should create and save TimeClockEvents', async () => {
    const nbButtonsBeforeCreate = await timeClockEventComponentsPage.countDeleteButtons();

    await timeClockEventComponentsPage.clickOnCreateButton();

    await promise.all([
      timeClockEventUpdatePage.setEvent_typeInput('event_type'),
      timeClockEventUpdatePage.setBreak_start_timeInput('2000-12-31'),
      timeClockEventUpdatePage.setBreak_end_timeInput('2000-12-31'),
      timeClockEventUpdatePage.setTotal_break_durationInput('5'),
      timeClockEventUpdatePage.setPrev_position_idInput('5'),
      timeClockEventUpdatePage.setNew_position_idInput('5'),
      timeClockEventUpdatePage.setNoteInput('note'),
      timeClockEventUpdatePage.companySelectLastOption(),
      timeClockEventUpdatePage.timeClockSelectLastOption(),
    ]);

    expect(await timeClockEventUpdatePage.getEvent_typeInput()).to.eq('event_type', 'Expected Event_type value to be equals to event_type');
    expect(await timeClockEventUpdatePage.getBreak_start_timeInput()).to.eq(
      '2000-12-31',
      'Expected break_start_time value to be equals to 2000-12-31'
    );
    expect(await timeClockEventUpdatePage.getBreak_end_timeInput()).to.eq(
      '2000-12-31',
      'Expected break_end_time value to be equals to 2000-12-31'
    );
    expect(await timeClockEventUpdatePage.getTotal_break_durationInput()).to.eq(
      '5',
      'Expected total_break_duration value to be equals to 5'
    );
    expect(await timeClockEventUpdatePage.getPrev_position_idInput()).to.eq('5', 'Expected prev_position_id value to be equals to 5');
    expect(await timeClockEventUpdatePage.getNew_position_idInput()).to.eq('5', 'Expected new_position_id value to be equals to 5');
    expect(await timeClockEventUpdatePage.getNoteInput()).to.eq('note', 'Expected Note value to be equals to note');

    await timeClockEventUpdatePage.save();
    expect(await timeClockEventUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await timeClockEventComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last TimeClockEvent', async () => {
    const nbButtonsBeforeDelete = await timeClockEventComponentsPage.countDeleteButtons();
    await timeClockEventComponentsPage.clickOnLastDeleteButton();

    timeClockEventDeleteDialog = new TimeClockEventDeleteDialog();
    expect(await timeClockEventDeleteDialog.getDialogTitle()).to.eq('hrmsApp.timeClockEvent.delete.question');
    await timeClockEventDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(timeClockEventComponentsPage.title), 5000);

    expect(await timeClockEventComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
