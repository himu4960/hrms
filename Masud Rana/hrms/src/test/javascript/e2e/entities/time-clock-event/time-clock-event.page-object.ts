import { element, by, ElementFinder } from 'protractor';

export class TimeClockEventComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('hrms-time-clock-event div table .btn-danger'));
  title = element.all(by.css('hrms-time-clock-event div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('hrmsTranslate');
  }
}

export class TimeClockEventUpdatePage {
  pageTitle = element(by.id('hrms-time-clock-event-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  idInput = element(by.id('field_id'));
  event_typeInput = element(by.id('field_event_type'));
  break_start_timeInput = element(by.id('field_break_start_time'));
  break_end_timeInput = element(by.id('field_break_end_time'));
  total_break_durationInput = element(by.id('field_total_break_duration'));
  prev_position_idInput = element(by.id('field_prev_position_id'));
  new_position_idInput = element(by.id('field_new_position_id'));
  noteInput = element(by.id('field_note'));

  companySelect = element(by.id('field_company'));
  timeClockSelect = element(by.id('field_timeClock'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('hrmsTranslate');
  }

  async setIdInput(id: string): Promise<void> {
    await this.idInput.sendKeys(id);
  }

  async getIdInput(): Promise<string> {
    return await this.idInput.getAttribute('value');
  }

  async setEvent_typeInput(event_type: string): Promise<void> {
    await this.event_typeInput.sendKeys(event_type);
  }

  async getEvent_typeInput(): Promise<string> {
    return await this.event_typeInput.getAttribute('value');
  }

  async setBreak_start_timeInput(break_start_time: string): Promise<void> {
    await this.break_start_timeInput.sendKeys(break_start_time);
  }

  async getBreak_start_timeInput(): Promise<string> {
    return await this.break_start_timeInput.getAttribute('value');
  }

  async setBreak_end_timeInput(break_end_time: string): Promise<void> {
    await this.break_end_timeInput.sendKeys(break_end_time);
  }

  async getBreak_end_timeInput(): Promise<string> {
    return await this.break_end_timeInput.getAttribute('value');
  }

  async setTotal_break_durationInput(total_break_duration: string): Promise<void> {
    await this.total_break_durationInput.sendKeys(total_break_duration);
  }

  async getTotal_break_durationInput(): Promise<string> {
    return await this.total_break_durationInput.getAttribute('value');
  }

  async setPrev_position_idInput(prev_position_id: string): Promise<void> {
    await this.prev_position_idInput.sendKeys(prev_position_id);
  }

  async getPrev_position_idInput(): Promise<string> {
    return await this.prev_position_idInput.getAttribute('value');
  }

  async setNew_position_idInput(new_position_id: string): Promise<void> {
    await this.new_position_idInput.sendKeys(new_position_id);
  }

  async getNew_position_idInput(): Promise<string> {
    return await this.new_position_idInput.getAttribute('value');
  }

  async setNoteInput(note: string): Promise<void> {
    await this.noteInput.sendKeys(note);
  }

  async getNoteInput(): Promise<string> {
    return await this.noteInput.getAttribute('value');
  }

  async companySelectLastOption(): Promise<void> {
    await this.companySelect.all(by.tagName('option')).last().click();
  }

  async companySelectOption(option: string): Promise<void> {
    await this.companySelect.sendKeys(option);
  }

  getCompanySelect(): ElementFinder {
    return this.companySelect;
  }

  async getCompanySelectedOption(): Promise<string> {
    return await this.companySelect.element(by.css('option:checked')).getText();
  }

  async timeClockSelectLastOption(): Promise<void> {
    await this.timeClockSelect.all(by.tagName('option')).last().click();
  }

  async timeClockSelectOption(option: string): Promise<void> {
    await this.timeClockSelect.sendKeys(option);
  }

  getTimeClockSelect(): ElementFinder {
    return this.timeClockSelect;
  }

  async getTimeClockSelectedOption(): Promise<string> {
    return await this.timeClockSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class TimeClockEventDeleteDialog {
  private dialogTitle = element(by.id('hrms-delete-timeClockEvent-heading'));
  private confirmButton = element(by.id('hrms-confirm-delete-timeClockEvent'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('hrmsTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
