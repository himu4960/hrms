import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { TimeZoneComponentsPage, TimeZoneDeleteDialog, TimeZoneUpdatePage } from './time-zone.page-object';

const expect = chai.expect;

describe('TimeZone e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let timeZoneComponentsPage: TimeZoneComponentsPage;
  let timeZoneUpdatePage: TimeZoneUpdatePage;
  let timeZoneDeleteDialog: TimeZoneDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load TimeZones', async () => {
    await navBarPage.goToEntity('time-zone');
    timeZoneComponentsPage = new TimeZoneComponentsPage();
    await browser.wait(ec.visibilityOf(timeZoneComponentsPage.title), 5000);
    expect(await timeZoneComponentsPage.getTitle()).to.eq('hrmsApp.timeZone.home.title');
    await browser.wait(ec.or(ec.visibilityOf(timeZoneComponentsPage.entities), ec.visibilityOf(timeZoneComponentsPage.noResult)), 1000);
  });

  it('should load create TimeZone page', async () => {
    await timeZoneComponentsPage.clickOnCreateButton();
    timeZoneUpdatePage = new TimeZoneUpdatePage();
    expect(await timeZoneUpdatePage.getPageTitle()).to.eq('hrmsApp.timeZone.home.createOrEditLabel');
    await timeZoneUpdatePage.cancel();
  });

  it('should create and save TimeZones', async () => {
    const nbButtonsBeforeCreate = await timeZoneComponentsPage.countDeleteButtons();

    await timeZoneComponentsPage.clickOnCreateButton();

    await promise.all([
      timeZoneUpdatePage.setNameInput('name'),
      timeZoneUpdatePage.setCodeInput('code'),
      timeZoneUpdatePage.setIso_codeInput('iso_code'),
    ]);

    expect(await timeZoneUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await timeZoneUpdatePage.getCodeInput()).to.eq('code', 'Expected Code value to be equals to code');
    expect(await timeZoneUpdatePage.getIso_codeInput()).to.eq('iso_code', 'Expected Iso_code value to be equals to iso_code');
    const selectedIs_active = timeZoneUpdatePage.getIs_activeInput();
    if (await selectedIs_active.isSelected()) {
      await timeZoneUpdatePage.getIs_activeInput().click();
      expect(await timeZoneUpdatePage.getIs_activeInput().isSelected(), 'Expected is_active not to be selected').to.be.false;
    } else {
      await timeZoneUpdatePage.getIs_activeInput().click();
      expect(await timeZoneUpdatePage.getIs_activeInput().isSelected(), 'Expected is_active to be selected').to.be.true;
    }
    const selectedIs_deleted = timeZoneUpdatePage.getIs_deletedInput();
    if (await selectedIs_deleted.isSelected()) {
      await timeZoneUpdatePage.getIs_deletedInput().click();
      expect(await timeZoneUpdatePage.getIs_deletedInput().isSelected(), 'Expected is_deleted not to be selected').to.be.false;
    } else {
      await timeZoneUpdatePage.getIs_deletedInput().click();
      expect(await timeZoneUpdatePage.getIs_deletedInput().isSelected(), 'Expected is_deleted to be selected').to.be.true;
    }
    const selectedIs_default = timeZoneUpdatePage.getIs_defaultInput();
    if (await selectedIs_default.isSelected()) {
      await timeZoneUpdatePage.getIs_defaultInput().click();
      expect(await timeZoneUpdatePage.getIs_defaultInput().isSelected(), 'Expected is_default not to be selected').to.be.false;
    } else {
      await timeZoneUpdatePage.getIs_defaultInput().click();
      expect(await timeZoneUpdatePage.getIs_defaultInput().isSelected(), 'Expected is_default to be selected').to.be.true;
    }

    await timeZoneUpdatePage.save();
    expect(await timeZoneUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await timeZoneComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last TimeZone', async () => {
    const nbButtonsBeforeDelete = await timeZoneComponentsPage.countDeleteButtons();
    await timeZoneComponentsPage.clickOnLastDeleteButton();

    timeZoneDeleteDialog = new TimeZoneDeleteDialog();
    expect(await timeZoneDeleteDialog.getDialogTitle()).to.eq('hrmsApp.timeZone.delete.question');
    await timeZoneDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(timeZoneComponentsPage.title), 5000);

    expect(await timeZoneComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
