import { element, by, ElementFinder } from 'protractor';

export class TimeZoneComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('hrms-time-zone div table .btn-danger'));
  title = element.all(by.css('hrms-time-zone div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('hrmsTranslate');
  }
}

export class TimeZoneUpdatePage {
  pageTitle = element(by.id('hrms-time-zone-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  idInput = element(by.id('field_id'));
  nameInput = element(by.id('field_name'));
  codeInput = element(by.id('field_code'));
  iso_codeInput = element(by.id('field_iso_code'));
  is_activeInput = element(by.id('field_is_active'));
  is_deletedInput = element(by.id('field_is_deleted'));
  is_defaultInput = element(by.id('field_is_default'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('hrmsTranslate');
  }

  async setIdInput(id: string): Promise<void> {
    await this.idInput.sendKeys(id);
  }

  async getIdInput(): Promise<string> {
    return await this.idInput.getAttribute('value');
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  async setCodeInput(code: string): Promise<void> {
    await this.codeInput.sendKeys(code);
  }

  async getCodeInput(): Promise<string> {
    return await this.codeInput.getAttribute('value');
  }

  async setIso_codeInput(iso_code: string): Promise<void> {
    await this.iso_codeInput.sendKeys(iso_code);
  }

  async getIso_codeInput(): Promise<string> {
    return await this.iso_codeInput.getAttribute('value');
  }

  getIs_activeInput(): ElementFinder {
    return this.is_activeInput;
  }

  getIs_deletedInput(): ElementFinder {
    return this.is_deletedInput;
  }

  getIs_defaultInput(): ElementFinder {
    return this.is_defaultInput;
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class TimeZoneDeleteDialog {
  private dialogTitle = element(by.id('hrms-delete-timeZone-heading'));
  private confirmButton = element(by.id('hrms-confirm-delete-timeZone'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('hrmsTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
