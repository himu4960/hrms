import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { CustomFieldComponentsPage, CustomFieldDeleteDialog, CustomFieldUpdatePage } from './custom-field.page-object';

const expect = chai.expect;

describe('CustomField e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let customFieldComponentsPage: CustomFieldComponentsPage;
  let customFieldUpdatePage: CustomFieldUpdatePage;
  let customFieldDeleteDialog: CustomFieldDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load CustomFields', async () => {
    await navBarPage.goToEntity('custom-field');
    customFieldComponentsPage = new CustomFieldComponentsPage();
    await browser.wait(ec.visibilityOf(customFieldComponentsPage.title), 5000);
    expect(await customFieldComponentsPage.getTitle()).to.eq('hrmsApp.customField.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(customFieldComponentsPage.entities), ec.visibilityOf(customFieldComponentsPage.noResult)),
      1000
    );
  });

  it('should load create CustomField page', async () => {
    await customFieldComponentsPage.clickOnCreateButton();
    customFieldUpdatePage = new CustomFieldUpdatePage();
    expect(await customFieldUpdatePage.getPageTitle()).to.eq('hrmsApp.customField.home.createOrEditLabel');
    await customFieldUpdatePage.cancel();
  });

  it('should create and save CustomFields', async () => {
    const nbButtonsBeforeCreate = await customFieldComponentsPage.countDeleteButtons();

    await customFieldComponentsPage.clickOnCreateButton();

    await promise.all([
      customFieldUpdatePage.setCompany_idInput('5'),
      customFieldUpdatePage.setNameInput('name'),
      customFieldUpdatePage.setCustom_field_typeInput('custom_field_type'),
      customFieldUpdatePage.setPermission_typeInput('permission_type'),
      customFieldUpdatePage.setValueInput('value'),
    ]);

    expect(await customFieldUpdatePage.getCompany_idInput()).to.eq('5', 'Expected company_id value to be equals to 5');
    expect(await customFieldUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await customFieldUpdatePage.getCustom_field_typeInput()).to.eq(
      'custom_field_type',
      'Expected Custom_field_type value to be equals to custom_field_type'
    );
    expect(await customFieldUpdatePage.getPermission_typeInput()).to.eq(
      'permission_type',
      'Expected Permission_type value to be equals to permission_type'
    );
    expect(await customFieldUpdatePage.getValueInput()).to.eq('value', 'Expected Value value to be equals to value');

    await customFieldUpdatePage.save();
    expect(await customFieldUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await customFieldComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last CustomField', async () => {
    const nbButtonsBeforeDelete = await customFieldComponentsPage.countDeleteButtons();
    await customFieldComponentsPage.clickOnLastDeleteButton();

    customFieldDeleteDialog = new CustomFieldDeleteDialog();
    expect(await customFieldDeleteDialog.getDialogTitle()).to.eq('hrmsApp.customField.delete.question');
    await customFieldDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(customFieldComponentsPage.title), 5000);

    expect(await customFieldComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
