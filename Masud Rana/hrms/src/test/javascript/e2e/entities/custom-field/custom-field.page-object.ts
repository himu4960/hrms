import { element, by, ElementFinder } from 'protractor';

export class CustomFieldComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('hrms-custom-field div table .btn-danger'));
  title = element.all(by.css('hrms-custom-field div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('hrmsTranslate');
  }
}

export class CustomFieldUpdatePage {
  pageTitle = element(by.id('hrms-custom-field-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  idInput = element(by.id('field_id'));
  company_idInput = element(by.id('field_company_id'));
  nameInput = element(by.id('field_name'));
  custom_field_typeInput = element(by.id('field_custom_field_type'));
  permission_typeInput = element(by.id('field_permission_type'));
  valueInput = element(by.id('field_value'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('hrmsTranslate');
  }

  async setIdInput(id: string): Promise<void> {
    await this.idInput.sendKeys(id);
  }

  async getIdInput(): Promise<string> {
    return await this.idInput.getAttribute('value');
  }

  async setCompany_idInput(company_id: string): Promise<void> {
    await this.company_idInput.sendKeys(company_id);
  }

  async getCompany_idInput(): Promise<string> {
    return await this.company_idInput.getAttribute('value');
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  async setCustom_field_typeInput(custom_field_type: string): Promise<void> {
    await this.custom_field_typeInput.sendKeys(custom_field_type);
  }

  async getCustom_field_typeInput(): Promise<string> {
    return await this.custom_field_typeInput.getAttribute('value');
  }

  async setPermission_typeInput(permission_type: string): Promise<void> {
    await this.permission_typeInput.sendKeys(permission_type);
  }

  async getPermission_typeInput(): Promise<string> {
    return await this.permission_typeInput.getAttribute('value');
  }

  async setValueInput(value: string): Promise<void> {
    await this.valueInput.sendKeys(value);
  }

  async getValueInput(): Promise<string> {
    return await this.valueInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CustomFieldDeleteDialog {
  private dialogTitle = element(by.id('hrms-delete-customField-heading'));
  private confirmButton = element(by.id('hrms-confirm-delete-customField'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('hrmsTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
