import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { EmployeeSkillComponentsPage, EmployeeSkillDeleteDialog, EmployeeSkillUpdatePage } from './employee-skill.page-object';

const expect = chai.expect;

describe('EmployeeSkill e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let employeeSkillComponentsPage: EmployeeSkillComponentsPage;
  let employeeSkillUpdatePage: EmployeeSkillUpdatePage;
  let employeeSkillDeleteDialog: EmployeeSkillDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load EmployeeSkills', async () => {
    await navBarPage.goToEntity('employee-skill');
    employeeSkillComponentsPage = new EmployeeSkillComponentsPage();
    await browser.wait(ec.visibilityOf(employeeSkillComponentsPage.title), 5000);
    expect(await employeeSkillComponentsPage.getTitle()).to.eq('hrmsApp.employeeSkill.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(employeeSkillComponentsPage.entities), ec.visibilityOf(employeeSkillComponentsPage.noResult)),
      1000
    );
  });

  it('should load create EmployeeSkill page', async () => {
    await employeeSkillComponentsPage.clickOnCreateButton();
    employeeSkillUpdatePage = new EmployeeSkillUpdatePage();
    expect(await employeeSkillUpdatePage.getPageTitle()).to.eq('hrmsApp.employeeSkill.home.createOrEditLabel');
    await employeeSkillUpdatePage.cancel();
  });

  it('should create and save EmployeeSkills', async () => {
    const nbButtonsBeforeCreate = await employeeSkillComponentsPage.countDeleteButtons();

    await employeeSkillComponentsPage.clickOnCreateButton();

    await promise.all([employeeSkillUpdatePage.setCompany_idInput('5'), employeeSkillUpdatePage.setNameInput('name')]);

    expect(await employeeSkillUpdatePage.getCompany_idInput()).to.eq('5', 'Expected company_id value to be equals to 5');
    expect(await employeeSkillUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');

    await employeeSkillUpdatePage.save();
    expect(await employeeSkillUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await employeeSkillComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last EmployeeSkill', async () => {
    const nbButtonsBeforeDelete = await employeeSkillComponentsPage.countDeleteButtons();
    await employeeSkillComponentsPage.clickOnLastDeleteButton();

    employeeSkillDeleteDialog = new EmployeeSkillDeleteDialog();
    expect(await employeeSkillDeleteDialog.getDialogTitle()).to.eq('hrmsApp.employeeSkill.delete.question');
    await employeeSkillDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(employeeSkillComponentsPage.title), 5000);

    expect(await employeeSkillComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
