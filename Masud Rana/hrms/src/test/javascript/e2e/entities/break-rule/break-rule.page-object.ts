import { element, by, ElementFinder } from 'protractor';

export class BreakRuleComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('hrms-break-rule div table .btn-danger'));
  title = element.all(by.css('hrms-break-rule div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('hrmsTranslate');
  }
}

export class BreakRuleUpdatePage {
  pageTitle = element(by.id('hrms-break-rule-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  idInput = element(by.id('field_id'));
  company_idInput = element(by.id('field_company_id'));
  nameInput = element(by.id('field_name'));
  rule_descriptionInput = element(by.id('field_rule_description'));

  companySelect = element(by.id('field_company'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('hrmsTranslate');
  }

  async setIdInput(id: string): Promise<void> {
    await this.idInput.sendKeys(id);
  }

  async getIdInput(): Promise<string> {
    return await this.idInput.getAttribute('value');
  }

  async setCompany_idInput(company_id: string): Promise<void> {
    await this.company_idInput.sendKeys(company_id);
  }

  async getCompany_idInput(): Promise<string> {
    return await this.company_idInput.getAttribute('value');
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  async setRule_descriptionInput(rule_description: string): Promise<void> {
    await this.rule_descriptionInput.sendKeys(rule_description);
  }

  async getRule_descriptionInput(): Promise<string> {
    return await this.rule_descriptionInput.getAttribute('value');
  }

  async companySelectLastOption(): Promise<void> {
    await this.companySelect.all(by.tagName('option')).last().click();
  }

  async companySelectOption(option: string): Promise<void> {
    await this.companySelect.sendKeys(option);
  }

  getCompanySelect(): ElementFinder {
    return this.companySelect;
  }

  async getCompanySelectedOption(): Promise<string> {
    return await this.companySelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class BreakRuleDeleteDialog {
  private dialogTitle = element(by.id('hrms-delete-breakRule-heading'));
  private confirmButton = element(by.id('hrms-confirm-delete-breakRule'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('hrmsTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
