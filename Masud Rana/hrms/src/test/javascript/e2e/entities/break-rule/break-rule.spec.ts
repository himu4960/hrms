import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { BreakRuleComponentsPage, BreakRuleDeleteDialog, BreakRuleUpdatePage } from './break-rule.page-object';

const expect = chai.expect;

describe('BreakRule e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let breakRuleComponentsPage: BreakRuleComponentsPage;
  let breakRuleUpdatePage: BreakRuleUpdatePage;
  let breakRuleDeleteDialog: BreakRuleDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load BreakRules', async () => {
    await navBarPage.goToEntity('break-rule');
    breakRuleComponentsPage = new BreakRuleComponentsPage();
    await browser.wait(ec.visibilityOf(breakRuleComponentsPage.title), 5000);
    expect(await breakRuleComponentsPage.getTitle()).to.eq('hrmsApp.breakRule.home.title');
    await browser.wait(ec.or(ec.visibilityOf(breakRuleComponentsPage.entities), ec.visibilityOf(breakRuleComponentsPage.noResult)), 1000);
  });

  it('should load create BreakRule page', async () => {
    await breakRuleComponentsPage.clickOnCreateButton();
    breakRuleUpdatePage = new BreakRuleUpdatePage();
    expect(await breakRuleUpdatePage.getPageTitle()).to.eq('hrmsApp.breakRule.home.createOrEditLabel');
    await breakRuleUpdatePage.cancel();
  });

  it('should create and save BreakRules', async () => {
    const nbButtonsBeforeCreate = await breakRuleComponentsPage.countDeleteButtons();

    await breakRuleComponentsPage.clickOnCreateButton();

    await promise.all([
      breakRuleUpdatePage.setCompany_idInput('5'),
      breakRuleUpdatePage.setNameInput('name'),
      breakRuleUpdatePage.setRule_descriptionInput('rule_description'),
      breakRuleUpdatePage.companySelectLastOption(),
    ]);

    expect(await breakRuleUpdatePage.getCompany_idInput()).to.eq('5', 'Expected company_id value to be equals to 5');
    expect(await breakRuleUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await breakRuleUpdatePage.getRule_descriptionInput()).to.eq(
      'rule_description',
      'Expected Rule_description value to be equals to rule_description'
    );

    await breakRuleUpdatePage.save();
    expect(await breakRuleUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await breakRuleComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last BreakRule', async () => {
    const nbButtonsBeforeDelete = await breakRuleComponentsPage.countDeleteButtons();
    await breakRuleComponentsPage.clickOnLastDeleteButton();

    breakRuleDeleteDialog = new BreakRuleDeleteDialog();
    expect(await breakRuleDeleteDialog.getDialogTitle()).to.eq('hrmsApp.breakRule.delete.question');
    await breakRuleDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(breakRuleComponentsPage.title), 5000);

    expect(await breakRuleComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
