import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { UserCompanyComponentsPage, UserCompanyDeleteDialog, UserCompanyUpdatePage } from './user-company.page-object';

const expect = chai.expect;

describe('UserCompany e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let userCompanyComponentsPage: UserCompanyComponentsPage;
  let userCompanyUpdatePage: UserCompanyUpdatePage;
  let userCompanyDeleteDialog: UserCompanyDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load UserCompanies', async () => {
    await navBarPage.goToEntity('user-company');
    userCompanyComponentsPage = new UserCompanyComponentsPage();
    await browser.wait(ec.visibilityOf(userCompanyComponentsPage.title), 5000);
    expect(await userCompanyComponentsPage.getTitle()).to.eq('hrmsApp.userCompany.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(userCompanyComponentsPage.entities), ec.visibilityOf(userCompanyComponentsPage.noResult)),
      1000
    );
  });

  it('should load create UserCompany page', async () => {
    await userCompanyComponentsPage.clickOnCreateButton();
    userCompanyUpdatePage = new UserCompanyUpdatePage();
    expect(await userCompanyUpdatePage.getPageTitle()).to.eq('hrmsApp.userCompany.home.createOrEditLabel');
    await userCompanyUpdatePage.cancel();
  });

  it('should create and save UserCompanies', async () => {
    const nbButtonsBeforeCreate = await userCompanyComponentsPage.countDeleteButtons();

    await userCompanyComponentsPage.clickOnCreateButton();

    await promise.all([
      userCompanyUpdatePage.userSelectLastOption(),
      userCompanyUpdatePage.companySelectLastOption(),
      userCompanyUpdatePage.roleSelectLastOption(),
    ]);

    const selectedIs_active = userCompanyUpdatePage.getIs_activeInput();
    if (await selectedIs_active.isSelected()) {
      await userCompanyUpdatePage.getIs_activeInput().click();
      expect(await userCompanyUpdatePage.getIs_activeInput().isSelected(), 'Expected is_active not to be selected').to.be.false;
    } else {
      await userCompanyUpdatePage.getIs_activeInput().click();
      expect(await userCompanyUpdatePage.getIs_activeInput().isSelected(), 'Expected is_active to be selected').to.be.true;
    }
    const selectedIs_deleted = userCompanyUpdatePage.getIs_deletedInput();
    if (await selectedIs_deleted.isSelected()) {
      await userCompanyUpdatePage.getIs_deletedInput().click();
      expect(await userCompanyUpdatePage.getIs_deletedInput().isSelected(), 'Expected is_deleted not to be selected').to.be.false;
    } else {
      await userCompanyUpdatePage.getIs_deletedInput().click();
      expect(await userCompanyUpdatePage.getIs_deletedInput().isSelected(), 'Expected is_deleted to be selected').to.be.true;
    }

    await userCompanyUpdatePage.save();
    expect(await userCompanyUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await userCompanyComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last UserCompany', async () => {
    const nbButtonsBeforeDelete = await userCompanyComponentsPage.countDeleteButtons();
    await userCompanyComponentsPage.clickOnLastDeleteButton();

    userCompanyDeleteDialog = new UserCompanyDeleteDialog();
    expect(await userCompanyDeleteDialog.getDialogTitle()).to.eq('hrmsApp.userCompany.delete.question');
    await userCompanyDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(userCompanyComponentsPage.title), 5000);

    expect(await userCompanyComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
