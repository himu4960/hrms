import { element, by, ElementFinder } from 'protractor';

export class UserCompanyComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('hrms-user-company div table .btn-danger'));
  title = element.all(by.css('hrms-user-company div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('hrmsTranslate');
  }
}

export class UserCompanyUpdatePage {
  pageTitle = element(by.id('hrms-user-company-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  idInput = element(by.id('field_id'));
  is_activeInput = element(by.id('field_is_active'));
  is_deletedInput = element(by.id('field_is_deleted'));

  userSelect = element(by.id('field_user'));
  companySelect = element(by.id('field_company'));
  roleSelect = element(by.id('field_role'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('hrmsTranslate');
  }

  async setIdInput(id: string): Promise<void> {
    await this.idInput.sendKeys(id);
  }

  async getIdInput(): Promise<string> {
    return await this.idInput.getAttribute('value');
  }

  getIs_activeInput(): ElementFinder {
    return this.is_activeInput;
  }

  getIs_deletedInput(): ElementFinder {
    return this.is_deletedInput;
  }

  async userSelectLastOption(): Promise<void> {
    await this.userSelect.all(by.tagName('option')).last().click();
  }

  async userSelectOption(option: string): Promise<void> {
    await this.userSelect.sendKeys(option);
  }

  getUserSelect(): ElementFinder {
    return this.userSelect;
  }

  async getUserSelectedOption(): Promise<string> {
    return await this.userSelect.element(by.css('option:checked')).getText();
  }

  async companySelectLastOption(): Promise<void> {
    await this.companySelect.all(by.tagName('option')).last().click();
  }

  async companySelectOption(option: string): Promise<void> {
    await this.companySelect.sendKeys(option);
  }

  getCompanySelect(): ElementFinder {
    return this.companySelect;
  }

  async getCompanySelectedOption(): Promise<string> {
    return await this.companySelect.element(by.css('option:checked')).getText();
  }

  async roleSelectLastOption(): Promise<void> {
    await this.roleSelect.all(by.tagName('option')).last().click();
  }

  async roleSelectOption(option: string): Promise<void> {
    await this.roleSelect.sendKeys(option);
  }

  getRoleSelect(): ElementFinder {
    return this.roleSelect;
  }

  async getRoleSelectedOption(): Promise<string> {
    return await this.roleSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class UserCompanyDeleteDialog {
  private dialogTitle = element(by.id('hrms-delete-userCompany-heading'));
  private confirmButton = element(by.id('hrms-confirm-delete-userCompany'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('hrmsTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
