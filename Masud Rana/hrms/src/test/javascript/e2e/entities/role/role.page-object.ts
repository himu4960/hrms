import { element, by, ElementFinder } from 'protractor';

export class RoleComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('hrms-role div table .btn-danger'));
  title = element.all(by.css('hrms-role div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('hrmsTranslate');
  }
}

export class RoleUpdatePage {
  pageTitle = element(by.id('hrms-role-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  idInput = element(by.id('field_id'));
  menusInput = element(by.id('field_menus'));
  is_activeInput = element(by.id('field_is_active'));
  is_deletedInput = element(by.id('field_is_deleted'));

  applicationTypeSelect = element(by.id('field_applicationType'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('hrmsTranslate');
  }

  async setIdInput(id: string): Promise<void> {
    await this.idInput.sendKeys(id);
  }

  async getIdInput(): Promise<string> {
    return await this.idInput.getAttribute('value');
  }

  async setMenusInput(menus: string): Promise<void> {
    await this.menusInput.sendKeys(menus);
  }

  async getMenusInput(): Promise<string> {
    return await this.menusInput.getAttribute('value');
  }

  getIs_activeInput(): ElementFinder {
    return this.is_activeInput;
  }

  getIs_deletedInput(): ElementFinder {
    return this.is_deletedInput;
  }

  async applicationTypeSelectLastOption(): Promise<void> {
    await this.applicationTypeSelect.all(by.tagName('option')).last().click();
  }

  async applicationTypeSelectOption(option: string): Promise<void> {
    await this.applicationTypeSelect.sendKeys(option);
  }

  getApplicationTypeSelect(): ElementFinder {
    return this.applicationTypeSelect;
  }

  async getApplicationTypeSelectedOption(): Promise<string> {
    return await this.applicationTypeSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class RoleDeleteDialog {
  private dialogTitle = element(by.id('hrms-delete-role-heading'));
  private confirmButton = element(by.id('hrms-confirm-delete-role'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('hrmsTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
