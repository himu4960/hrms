import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { PermissionTypeComponentsPage, PermissionTypeDeleteDialog, PermissionTypeUpdatePage } from './permission-type.page-object';

const expect = chai.expect;

describe('PermissionType e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let permissionTypeComponentsPage: PermissionTypeComponentsPage;
  let permissionTypeUpdatePage: PermissionTypeUpdatePage;
  let permissionTypeDeleteDialog: PermissionTypeDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load PermissionTypes', async () => {
    await navBarPage.goToEntity('permission-type');
    permissionTypeComponentsPage = new PermissionTypeComponentsPage();
    await browser.wait(ec.visibilityOf(permissionTypeComponentsPage.title), 5000);
    expect(await permissionTypeComponentsPage.getTitle()).to.eq('hrmsApp.permissionType.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(permissionTypeComponentsPage.entities), ec.visibilityOf(permissionTypeComponentsPage.noResult)),
      1000
    );
  });

  it('should load create PermissionType page', async () => {
    await permissionTypeComponentsPage.clickOnCreateButton();
    permissionTypeUpdatePage = new PermissionTypeUpdatePage();
    expect(await permissionTypeUpdatePage.getPageTitle()).to.eq('hrmsApp.permissionType.home.createOrEditLabel');
    await permissionTypeUpdatePage.cancel();
  });

  it('should create and save PermissionTypes', async () => {
    const nbButtonsBeforeCreate = await permissionTypeComponentsPage.countDeleteButtons();

    await permissionTypeComponentsPage.clickOnCreateButton();

    await promise.all([permissionTypeUpdatePage.setNameInput('name'), permissionTypeUpdatePage.setKeyInput('key')]);

    expect(await permissionTypeUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await permissionTypeUpdatePage.getKeyInput()).to.eq('key', 'Expected Key value to be equals to key');

    await permissionTypeUpdatePage.save();
    expect(await permissionTypeUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await permissionTypeComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last PermissionType', async () => {
    const nbButtonsBeforeDelete = await permissionTypeComponentsPage.countDeleteButtons();
    await permissionTypeComponentsPage.clickOnLastDeleteButton();

    permissionTypeDeleteDialog = new PermissionTypeDeleteDialog();
    expect(await permissionTypeDeleteDialog.getDialogTitle()).to.eq('hrmsApp.permissionType.delete.question');
    await permissionTypeDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(permissionTypeComponentsPage.title), 5000);

    expect(await permissionTypeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
