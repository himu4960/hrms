import { element, by, ElementFinder } from 'protractor';

export class BranchComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('hrms-branch div table .btn-danger'));
  title = element.all(by.css('hrms-branch div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('hrmsTranslate');
  }
}

export class BranchUpdatePage {
  pageTitle = element(by.id('hrms-branch-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  nameInput = element(by.id('field_name'));
  phoneInput = element(by.id('field_phone'));
  emailInput = element(by.id('field_email'));
  address_line1Input = element(by.id('field_address_line1'));
  address_line2Input = element(by.id('field_address_line2'));
  address_cityInput = element(by.id('field_address_city'));
  address_provinceInput = element(by.id('field_address_province'));
  address_countryInput = element(by.id('field_address_country'));
  address_latitudeInput = element(by.id('field_address_latitude'));
  address_longitudeInput = element(by.id('field_address_longitude'));
  is_activeInput = element(by.id('field_is_active'));

  breakRuleSelect = element(by.id('field_breakRule'));
  timeZoneSelect = element(by.id('field_timeZone'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('hrmsTranslate');
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  async setPhoneInput(phone: string): Promise<void> {
    await this.phoneInput.sendKeys(phone);
  }

  async getPhoneInput(): Promise<string> {
    return await this.phoneInput.getAttribute('value');
  }

  async setEmailInput(email: string): Promise<void> {
    await this.emailInput.sendKeys(email);
  }

  async getEmailInput(): Promise<string> {
    return await this.emailInput.getAttribute('value');
  }

  async setAddress_line1Input(address_line1: string): Promise<void> {
    await this.address_line1Input.sendKeys(address_line1);
  }

  async getAddress_line1Input(): Promise<string> {
    return await this.address_line1Input.getAttribute('value');
  }

  async setAddress_line2Input(address_line2: string): Promise<void> {
    await this.address_line2Input.sendKeys(address_line2);
  }

  async getAddress_line2Input(): Promise<string> {
    return await this.address_line2Input.getAttribute('value');
  }

  async setAddress_cityInput(address_city: string): Promise<void> {
    await this.address_cityInput.sendKeys(address_city);
  }

  async getAddress_cityInput(): Promise<string> {
    return await this.address_cityInput.getAttribute('value');
  }

  async setAddress_provinceInput(address_province: string): Promise<void> {
    await this.address_provinceInput.sendKeys(address_province);
  }

  async getAddress_provinceInput(): Promise<string> {
    return await this.address_provinceInput.getAttribute('value');
  }

  async setAddress_countryInput(address_country: string): Promise<void> {
    await this.address_countryInput.sendKeys(address_country);
  }

  async getAddress_countryInput(): Promise<string> {
    return await this.address_countryInput.getAttribute('value');
  }

  async setAddress_latitudeInput(address_latitude: string): Promise<void> {
    await this.address_latitudeInput.sendKeys(address_latitude);
  }

  async getAddress_latitudeInput(): Promise<string> {
    return await this.address_latitudeInput.getAttribute('value');
  }

  async setAddress_longitudeInput(address_longitude: string): Promise<void> {
    await this.address_longitudeInput.sendKeys(address_longitude);
  }

  async getAddress_longitudeInput(): Promise<string> {
    return await this.address_longitudeInput.getAttribute('value');
  }

  getIs_activeInput(): ElementFinder {
    return this.is_activeInput;
  }

  async breakRuleSelectLastOption(): Promise<void> {
    await this.breakRuleSelect.all(by.tagName('option')).last().click();
  }

  async breakRuleSelectOption(option: string): Promise<void> {
    await this.breakRuleSelect.sendKeys(option);
  }

  getBreakRuleSelect(): ElementFinder {
    return this.breakRuleSelect;
  }

  async getBreakRuleSelectedOption(): Promise<string> {
    return await this.breakRuleSelect.element(by.css('option:checked')).getText();
  }

  async timeZoneSelectLastOption(): Promise<void> {
    await this.timeZoneSelect.all(by.tagName('option')).last().click();
  }

  async timeZoneSelectOption(option: string): Promise<void> {
    await this.timeZoneSelect.sendKeys(option);
  }

  getTimeZoneSelect(): ElementFinder {
    return this.timeZoneSelect;
  }

  async getTimeZoneSelectedOption(): Promise<string> {
    return await this.timeZoneSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class BranchDeleteDialog {
  private dialogTitle = element(by.id('hrms-delete-branch-heading'));
  private confirmButton = element(by.id('hrms-confirm-delete-branch'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('hrmsTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
