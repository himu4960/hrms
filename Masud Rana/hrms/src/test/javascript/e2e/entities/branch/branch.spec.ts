import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  BranchComponentsPage,
  /* BranchDeleteDialog, */
  BranchUpdatePage,
} from './branch.page-object';

const expect = chai.expect;

describe('Branch e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let branchComponentsPage: BranchComponentsPage;
  let branchUpdatePage: BranchUpdatePage;
  /* let branchDeleteDialog: BranchDeleteDialog; */
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Branches', async () => {
    await navBarPage.goToEntity('branch');
    branchComponentsPage = new BranchComponentsPage();
    await browser.wait(ec.visibilityOf(branchComponentsPage.title), 5000);
    expect(await branchComponentsPage.getTitle()).to.eq('hrmsApp.branch.home.title');
    await browser.wait(ec.or(ec.visibilityOf(branchComponentsPage.entities), ec.visibilityOf(branchComponentsPage.noResult)), 1000);
  });

  it('should load create Branch page', async () => {
    await branchComponentsPage.clickOnCreateButton();
    branchUpdatePage = new BranchUpdatePage();
    expect(await branchUpdatePage.getPageTitle()).to.eq('hrmsApp.branch.home.createOrEditLabel');
    await branchUpdatePage.cancel();
  });

  /* it('should create and save Branches', async () => {
        const nbButtonsBeforeCreate = await branchComponentsPage.countDeleteButtons();

        await branchComponentsPage.clickOnCreateButton();

        await promise.all([
            branchUpdatePage.setNameInput('name'),
            branchUpdatePage.setPhoneInput('phone'),
            branchUpdatePage.setEmailInput('email'),
            branchUpdatePage.setAddress_line1Input('address_line1'),
            branchUpdatePage.setAddress_line2Input('address_line2'),
            branchUpdatePage.setAddress_cityInput('address_city'),
            branchUpdatePage.setAddress_provinceInput('address_province'),
            branchUpdatePage.setAddress_countryInput('address_country'),
            branchUpdatePage.setAddress_latitudeInput('5'),
            branchUpdatePage.setAddress_longitudeInput('5'),
            branchUpdatePage.breakRuleSelectLastOption(),
            branchUpdatePage.timeZoneSelectLastOption(),
        ]);

        expect(await branchUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
        expect(await branchUpdatePage.getPhoneInput()).to.eq('phone', 'Expected Phone value to be equals to phone');
        expect(await branchUpdatePage.getEmailInput()).to.eq('email', 'Expected Email value to be equals to email');
        expect(await branchUpdatePage.getAddress_line1Input()).to.eq('address_line1', 'Expected Address_line1 value to be equals to address_line1');
        expect(await branchUpdatePage.getAddress_line2Input()).to.eq('address_line2', 'Expected Address_line2 value to be equals to address_line2');
        expect(await branchUpdatePage.getAddress_cityInput()).to.eq('address_city', 'Expected Address_city value to be equals to address_city');
        expect(await branchUpdatePage.getAddress_provinceInput()).to.eq('address_province', 'Expected Address_province value to be equals to address_province');
        expect(await branchUpdatePage.getAddress_countryInput()).to.eq('address_country', 'Expected Address_country value to be equals to address_country');
        expect(await branchUpdatePage.getAddress_latitudeInput()).to.eq('5', 'Expected address_latitude value to be equals to 5');
        expect(await branchUpdatePage.getAddress_longitudeInput()).to.eq('5', 'Expected address_longitude value to be equals to 5');
        const selectedIs_active = branchUpdatePage.getIs_activeInput();
        if (await selectedIs_active.isSelected()) {
            await branchUpdatePage.getIs_activeInput().click();
            expect(await branchUpdatePage.getIs_activeInput().isSelected(), 'Expected is_active not to be selected').to.be.false;
        } else {
            await branchUpdatePage.getIs_activeInput().click();
            expect(await branchUpdatePage.getIs_activeInput().isSelected(), 'Expected is_active to be selected').to.be.true;
        }

        await branchUpdatePage.save();
        expect(await branchUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await branchComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last Branch', async () => {
        const nbButtonsBeforeDelete = await branchComponentsPage.countDeleteButtons();
        await branchComponentsPage.clickOnLastDeleteButton();

        branchDeleteDialog = new BranchDeleteDialog();
        expect(await branchDeleteDialog.getDialogTitle())
            .to.eq('hrmsApp.branch.delete.question');
        await branchDeleteDialog.clickOnConfirmButton();
        await browser.wait(ec.visibilityOf(branchComponentsPage.title), 5000);

        expect(await branchComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
