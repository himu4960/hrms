import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  EmployeeComponentsPage,
  /* EmployeeDeleteDialog, */
  EmployeeUpdatePage,
} from './employee.page-object';

const expect = chai.expect;

describe('Employee e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let employeeComponentsPage: EmployeeComponentsPage;
  let employeeUpdatePage: EmployeeUpdatePage;
  /* let employeeDeleteDialog: EmployeeDeleteDialog; */
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Employees', async () => {
    await navBarPage.goToEntity('employee');
    employeeComponentsPage = new EmployeeComponentsPage();
    await browser.wait(ec.visibilityOf(employeeComponentsPage.title), 5000);
    expect(await employeeComponentsPage.getTitle()).to.eq('hrmsApp.employee.home.title');
    await browser.wait(ec.or(ec.visibilityOf(employeeComponentsPage.entities), ec.visibilityOf(employeeComponentsPage.noResult)), 1000);
  });

  it('should load create Employee page', async () => {
    await employeeComponentsPage.clickOnCreateButton();
    employeeUpdatePage = new EmployeeUpdatePage();
    expect(await employeeUpdatePage.getPageTitle()).to.eq('hrmsApp.employee.home.createOrEditLabel');
    await employeeUpdatePage.cancel();
  });

  /* it('should create and save Employees', async () => {
        const nbButtonsBeforeCreate = await employeeComponentsPage.countDeleteButtons();

        await employeeComponentsPage.clickOnCreateButton();

        await promise.all([
            employeeUpdatePage.setCodeInput('code'),
            employeeUpdatePage.setFirst_nameInput('first_name'),
            employeeUpdatePage.setMiddle_nameInput('middle_name'),
            employeeUpdatePage.setLast_nameInput('last_name'),
            employeeUpdatePage.setDobInput('2000-12-31'),
            employeeUpdatePage.setEmailInput('email'),
            employeeUpdatePage.setPersonal_emailInput('personal_email'),
            employeeUpdatePage.setPhoneInput('phone'),
            employeeUpdatePage.setTelephoneInput('telephone'),
            employeeUpdatePage.setNationalityInput('nationality'),
            employeeUpdatePage.setPhoto_urlInput('photo_url'),
            employeeUpdatePage.setInvitation_tokenInput('invitation_token'),
            employeeUpdatePage.setInvitation_expiryInput('PT12S'),
            employeeUpdatePage.genderSelectLastOption(),
            employeeUpdatePage.religionSelectLastOption(),
            employeeUpdatePage.maritalStatusSelectLastOption(),
            employeeUpdatePage.bloodGroupSelectLastOption(),
            employeeUpdatePage.companySelectLastOption(),
            employeeUpdatePage.branchSelectLastOption(),
        ]);

        expect(await employeeUpdatePage.getCodeInput()).to.eq('code', 'Expected Code value to be equals to code');
        expect(await employeeUpdatePage.getFirst_nameInput()).to.eq('first_name', 'Expected First_name value to be equals to first_name');
        expect(await employeeUpdatePage.getMiddle_nameInput()).to.eq('middle_name', 'Expected Middle_name value to be equals to middle_name');
        expect(await employeeUpdatePage.getLast_nameInput()).to.eq('last_name', 'Expected Last_name value to be equals to last_name');
        expect(await employeeUpdatePage.getDobInput()).to.eq('2000-12-31', 'Expected dob value to be equals to 2000-12-31');
        expect(await employeeUpdatePage.getEmailInput()).to.eq('email', 'Expected Email value to be equals to email');
        expect(await employeeUpdatePage.getPersonal_emailInput()).to.eq('personal_email', 'Expected Personal_email value to be equals to personal_email');
        expect(await employeeUpdatePage.getPhoneInput()).to.eq('phone', 'Expected Phone value to be equals to phone');
        expect(await employeeUpdatePage.getTelephoneInput()).to.eq('telephone', 'Expected Telephone value to be equals to telephone');
        expect(await employeeUpdatePage.getNationalityInput()).to.eq('nationality', 'Expected Nationality value to be equals to nationality');
        expect(await employeeUpdatePage.getPhoto_urlInput()).to.eq('photo_url', 'Expected Photo_url value to be equals to photo_url');
        const selectedIs_active = employeeUpdatePage.getIs_activeInput();
        if (await selectedIs_active.isSelected()) {
            await employeeUpdatePage.getIs_activeInput().click();
            expect(await employeeUpdatePage.getIs_activeInput().isSelected(), 'Expected is_active not to be selected').to.be.false;
        } else {
            await employeeUpdatePage.getIs_activeInput().click();
            expect(await employeeUpdatePage.getIs_activeInput().isSelected(), 'Expected is_active to be selected').to.be.true;
        }
        const selectedIs_deleted = employeeUpdatePage.getIs_deletedInput();
        if (await selectedIs_deleted.isSelected()) {
            await employeeUpdatePage.getIs_deletedInput().click();
            expect(await employeeUpdatePage.getIs_deletedInput().isSelected(), 'Expected is_deleted not to be selected').to.be.false;
        } else {
            await employeeUpdatePage.getIs_deletedInput().click();
            expect(await employeeUpdatePage.getIs_deletedInput().isSelected(), 'Expected is_deleted to be selected').to.be.true;
        }
        const selectedInvitation_accepted = employeeUpdatePage.getInvitation_acceptedInput();
        if (await selectedInvitation_accepted.isSelected()) {
            await employeeUpdatePage.getInvitation_acceptedInput().click();
            expect(await employeeUpdatePage.getInvitation_acceptedInput().isSelected(), 'Expected invitation_accepted not to be selected').to.be.false;
        } else {
            await employeeUpdatePage.getInvitation_acceptedInput().click();
            expect(await employeeUpdatePage.getInvitation_acceptedInput().isSelected(), 'Expected invitation_accepted to be selected').to.be.true;
        }
        expect(await employeeUpdatePage.getInvitation_tokenInput()).to.eq('invitation_token', 'Expected Invitation_token value to be equals to invitation_token');
        expect(await employeeUpdatePage.getInvitation_expiryInput()).to.contain('12', 'Expected invitation_expiry value to be equals to 12');

        await employeeUpdatePage.save();
        expect(await employeeUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await employeeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last Employee', async () => {
        const nbButtonsBeforeDelete = await employeeComponentsPage.countDeleteButtons();
        await employeeComponentsPage.clickOnLastDeleteButton();

        employeeDeleteDialog = new EmployeeDeleteDialog();
        expect(await employeeDeleteDialog.getDialogTitle())
            .to.eq('hrmsApp.employee.delete.question');
        await employeeDeleteDialog.clickOnConfirmButton();
        await browser.wait(ec.visibilityOf(employeeComponentsPage.title), 5000);

        expect(await employeeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
