import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { LeaveTypeComponentsPage, LeaveTypeDeleteDialog, LeaveTypeUpdatePage } from './leave-type.page-object';

const expect = chai.expect;

describe('LeaveType e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let leaveTypeComponentsPage: LeaveTypeComponentsPage;
  let leaveTypeUpdatePage: LeaveTypeUpdatePage;
  let leaveTypeDeleteDialog: LeaveTypeDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load LeaveTypes', async () => {
    await navBarPage.goToEntity('leave-type');
    leaveTypeComponentsPage = new LeaveTypeComponentsPage();
    await browser.wait(ec.visibilityOf(leaveTypeComponentsPage.title), 5000);
    expect(await leaveTypeComponentsPage.getTitle()).to.eq('leaveType.home.title');
    await browser.wait(ec.or(ec.visibilityOf(leaveTypeComponentsPage.entities), ec.visibilityOf(leaveTypeComponentsPage.noResult)), 1000);
  });

  it('should load create LeaveType page', async () => {
    await leaveTypeComponentsPage.clickOnCreateButton();
    leaveTypeUpdatePage = new LeaveTypeUpdatePage();
    expect(await leaveTypeUpdatePage.getPageTitle()).to.eq('leaveType.home.createOrEditLabel');
    await leaveTypeUpdatePage.cancel();
  });

  it('should create and save LeaveTypes', async () => {
    const nbButtonsBeforeCreate = await leaveTypeComponentsPage.countDeleteButtons();

    await leaveTypeComponentsPage.clickOnCreateButton();

    await promise.all([
      leaveTypeUpdatePage.setCompany_idInput('5'),
      leaveTypeUpdatePage.setNameInput('name'),
      leaveTypeUpdatePage.setMax_days_allowedInput('5'),
      leaveTypeUpdatePage.setMax_employee_allowedInput('5'),
      leaveTypeUpdatePage.setLeave_in_advance_daysInput('5'),
      leaveTypeUpdatePage.setDescriptionInput('description'),
    ]);

    expect(await leaveTypeUpdatePage.getCompany_idInput()).to.eq('5', 'Expected company_id value to be equals to 5');
    expect(await leaveTypeUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    const selectedIs_partial_day_request_allowed = leaveTypeUpdatePage.getIs_partial_day_request_allowedInput();
    if (await selectedIs_partial_day_request_allowed.isSelected()) {
      await leaveTypeUpdatePage.getIs_partial_day_request_allowedInput().click();
      expect(
        await leaveTypeUpdatePage.getIs_partial_day_request_allowedInput().isSelected(),
        'Expected is_partial_day_request_allowed not to be selected'
      ).to.be.false;
    } else {
      await leaveTypeUpdatePage.getIs_partial_day_request_allowedInput().click();
      expect(
        await leaveTypeUpdatePage.getIs_partial_day_request_allowedInput().isSelected(),
        'Expected is_partial_day_request_allowed to be selected'
      ).to.be.true;
    }
    expect(await leaveTypeUpdatePage.getMax_days_allowedInput()).to.eq('5', 'Expected max_days_allowed value to be equals to 5');
    const selectedIs_max_employee_allowed_enabled = leaveTypeUpdatePage.getIs_max_employee_allowed_enabledInput();
    if (await selectedIs_max_employee_allowed_enabled.isSelected()) {
      await leaveTypeUpdatePage.getIs_max_employee_allowed_enabledInput().click();
      expect(
        await leaveTypeUpdatePage.getIs_max_employee_allowed_enabledInput().isSelected(),
        'Expected is_max_employee_allowed_enabled not to be selected'
      ).to.be.false;
    } else {
      await leaveTypeUpdatePage.getIs_max_employee_allowed_enabledInput().click();
      expect(
        await leaveTypeUpdatePage.getIs_max_employee_allowed_enabledInput().isSelected(),
        'Expected is_max_employee_allowed_enabled to be selected'
      ).to.be.true;
    }
    expect(await leaveTypeUpdatePage.getMax_employee_allowedInput()).to.eq('5', 'Expected max_employee_allowed value to be equals to 5');
    const selectedIs_leave_in_advance_days_enabled = leaveTypeUpdatePage.getIs_leave_in_advance_days_enabledInput();
    if (await selectedIs_leave_in_advance_days_enabled.isSelected()) {
      await leaveTypeUpdatePage.getIs_leave_in_advance_days_enabledInput().click();
      expect(
        await leaveTypeUpdatePage.getIs_leave_in_advance_days_enabledInput().isSelected(),
        'Expected is_leave_in_advance_days_enabled not to be selected'
      ).to.be.false;
    } else {
      await leaveTypeUpdatePage.getIs_leave_in_advance_days_enabledInput().click();
      expect(
        await leaveTypeUpdatePage.getIs_leave_in_advance_days_enabledInput().isSelected(),
        'Expected is_leave_in_advance_days_enabled to be selected'
      ).to.be.true;
    }
    expect(await leaveTypeUpdatePage.getLeave_in_advance_daysInput()).to.eq('5', 'Expected leave_in_advance_days value to be equals to 5');
    const selectedAuto_assign_new_employee_enabled = leaveTypeUpdatePage.getAuto_assign_new_employee_enabledInput();
    if (await selectedAuto_assign_new_employee_enabled.isSelected()) {
      await leaveTypeUpdatePage.getAuto_assign_new_employee_enabledInput().click();
      expect(
        await leaveTypeUpdatePage.getAuto_assign_new_employee_enabledInput().isSelected(),
        'Expected auto_assign_new_employee_enabled not to be selected'
      ).to.be.false;
    } else {
      await leaveTypeUpdatePage.getAuto_assign_new_employee_enabledInput().click();
      expect(
        await leaveTypeUpdatePage.getAuto_assign_new_employee_enabledInput().isSelected(),
        'Expected auto_assign_new_employee_enabled to be selected'
      ).to.be.true;
    }
    expect(await leaveTypeUpdatePage.getDescriptionInput()).to.eq('description', 'Expected Description value to be equals to description');

    await leaveTypeUpdatePage.save();
    expect(await leaveTypeUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await leaveTypeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last LeaveType', async () => {
    const nbButtonsBeforeDelete = await leaveTypeComponentsPage.countDeleteButtons();
    await leaveTypeComponentsPage.clickOnLastDeleteButton();

    leaveTypeDeleteDialog = new LeaveTypeDeleteDialog();
    expect(await leaveTypeDeleteDialog.getDialogTitle()).to.eq('leaveType.delete.question');
    await leaveTypeDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(leaveTypeComponentsPage.title), 5000);

    expect(await leaveTypeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
