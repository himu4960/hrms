import { element, by, ElementFinder } from 'protractor';

export class LeaveTypeComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('hrms-leave-type div table .btn-danger'));
  title = element.all(by.css('hrms-leave-type div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('hrmsTranslate');
  }
}

export class LeaveTypeUpdatePage {
  pageTitle = element(by.id('hrms-leave-type-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  idInput = element(by.id('field_id'));
  company_idInput = element(by.id('field_company_id'));
  nameInput = element(by.id('field_name'));
  is_partial_day_request_allowedInput = element(by.id('field_is_partial_day_request_allowed'));
  max_days_allowedInput = element(by.id('field_max_days_allowed'));
  is_max_employee_allowed_enabledInput = element(by.id('field_is_max_employee_allowed_enabled'));
  max_employee_allowedInput = element(by.id('field_max_employee_allowed'));
  is_leave_in_advance_days_enabledInput = element(by.id('field_is_leave_in_advance_days_enabled'));
  leave_in_advance_daysInput = element(by.id('field_leave_in_advance_days'));
  auto_assign_new_employee_enabledInput = element(by.id('field_auto_assign_new_employee_enabled'));
  descriptionInput = element(by.id('field_description'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('hrmsTranslate');
  }

  async setIdInput(id: string): Promise<void> {
    await this.idInput.sendKeys(id);
  }

  async getIdInput(): Promise<string> {
    return await this.idInput.getAttribute('value');
  }

  async setCompany_idInput(company_id: string): Promise<void> {
    await this.company_idInput.sendKeys(company_id);
  }

  async getCompany_idInput(): Promise<string> {
    return await this.company_idInput.getAttribute('value');
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  getIs_partial_day_request_allowedInput(): ElementFinder {
    return this.is_partial_day_request_allowedInput;
  }

  async setMax_days_allowedInput(max_days_allowed: string): Promise<void> {
    await this.max_days_allowedInput.sendKeys(max_days_allowed);
  }

  async getMax_days_allowedInput(): Promise<string> {
    return await this.max_days_allowedInput.getAttribute('value');
  }

  getIs_max_employee_allowed_enabledInput(): ElementFinder {
    return this.is_max_employee_allowed_enabledInput;
  }

  async setMax_employee_allowedInput(max_employee_allowed: string): Promise<void> {
    await this.max_employee_allowedInput.sendKeys(max_employee_allowed);
  }

  async getMax_employee_allowedInput(): Promise<string> {
    return await this.max_employee_allowedInput.getAttribute('value');
  }

  getIs_leave_in_advance_days_enabledInput(): ElementFinder {
    return this.is_leave_in_advance_days_enabledInput;
  }

  async setLeave_in_advance_daysInput(leave_in_advance_days: string): Promise<void> {
    await this.leave_in_advance_daysInput.sendKeys(leave_in_advance_days);
  }

  async getLeave_in_advance_daysInput(): Promise<string> {
    return await this.leave_in_advance_daysInput.getAttribute('value');
  }

  getAuto_assign_new_employee_enabledInput(): ElementFinder {
    return this.auto_assign_new_employee_enabledInput;
  }

  async setDescriptionInput(description: string): Promise<void> {
    await this.descriptionInput.sendKeys(description);
  }

  async getDescriptionInput(): Promise<string> {
    return await this.descriptionInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class LeaveTypeDeleteDialog {
  private dialogTitle = element(by.id('hrms-delete-leaveType-heading'));
  private confirmButton = element(by.id('hrms-confirm-delete-leaveType'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('hrmsTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
