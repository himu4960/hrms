import { browser, ExpectedConditions as ec, promise } from 'protractor';

import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';
import { CompanyComponentsPage, CompanyDeleteDialog, CompanyUpdatePage } from './company.page-object';

const expect = chai.expect;

describe('Company e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let companyComponentsPage: CompanyComponentsPage;
  let companyUpdatePage: CompanyUpdatePage;
  let companyDeleteDialog: CompanyDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Companies', async () => {
    await navBarPage.goToEntity('company');
    companyComponentsPage = new CompanyComponentsPage();
    await browser.wait(ec.visibilityOf(companyComponentsPage.title), 5000);
    expect(await companyComponentsPage.getTitle()).to.eq('hrmsApp.company.home.title');
    await browser.wait(ec.or(ec.visibilityOf(companyComponentsPage.entities), ec.visibilityOf(companyComponentsPage.noResult)), 1000);
  });

  it('should load create Company page', async () => {
    await companyComponentsPage.clickOnCreateButton();
    companyUpdatePage = new CompanyUpdatePage();
    expect(await companyUpdatePage.getPageTitle()).to.eq('hrmsApp.company.home.createOrEditLabel');
    await companyUpdatePage.cancel();
  });

  it('should create and save Companies', async () => {
    const nbButtonsBeforeCreate = await companyComponentsPage.countDeleteButtons();

    await companyComponentsPage.clickOnCreateButton();

    await promise.all([
      companyUpdatePage.setNameInput('name'),
      companyUpdatePage.setPhoneInput('phone'),
      companyUpdatePage.setEmailInput('email'),
      companyUpdatePage.setAddressInput('address'),
      companyUpdatePage.setLogoUrlLargeInput(
        'https://cdn.dribbble.com/users/24078/screenshots/15522433/media/e92e58ec9d338a234945ae3d3ffd5be3.jpg?compress=1&resize=400x300'
      ),
      companyUpdatePage.setLogoUrlMediumInput(
        'https://cdn.dribbble.com/users/24078/screenshots/15522433/media/e92e58ec9d338a234945ae3d3ffd5be3.jpg?compress=1&resize=400x300'
      ),
      companyUpdatePage.setLogoUrlSmallInput(
        'https://cdn.dribbble.com/users/24078/screenshots/15522433/media/e92e58ec9d338a234945ae3d3ffd5be3.jpg?compress=1&resize=400x300'
      ),
      companyUpdatePage.companyTypeSelectLastOption(),
      companyUpdatePage.applicationTypeSelectLastOption(),
    ]);

    expect(await companyUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await companyUpdatePage.getPhoneInput()).to.eq('phone', 'Expected Phone value to be equals to phone');
    expect(await companyUpdatePage.getEmailInput()).to.eq('email', 'Expected Email value to be equals to email');
    expect(await companyUpdatePage.getAddressInput()).to.eq('address', 'Expected Address value to be equals to address');
    expect(await companyUpdatePage.getLogoUrlLargeInput()).to.eq(
      'logo_url_large',
      'Expected Logo_url_large value to be equals to logo_url_large'
    );
    expect(await companyUpdatePage.getLogouUrlMediumInput()).to.eq(
      'logo_url_medium',
      'Expected Logo_url_medium value to be equals to logo_url_medium'
    );
    expect(await companyUpdatePage.getLogoUrlSmallInput()).to.eq(
      'logo_url_small',
      'Expected Logo_url_small value to be equals to logo_url_small'
    );
    const selectedStatus = companyUpdatePage.getStatusInput();
    if (await selectedStatus.isSelected()) {
      await companyUpdatePage.getStatusInput().click();
      expect(await companyUpdatePage.getStatusInput().isSelected(), 'Expected status not to be selected').to.be.false;
    } else {
      await companyUpdatePage.getStatusInput().click();
      expect(await companyUpdatePage.getStatusInput().isSelected(), 'Expected status to be selected').to.be.true;
    }

    await companyUpdatePage.save();
    expect(await companyUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await companyComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Company', async () => {
    const nbButtonsBeforeDelete = await companyComponentsPage.countDeleteButtons();
    await companyComponentsPage.clickOnLastDeleteButton();

    companyDeleteDialog = new CompanyDeleteDialog();
    expect(await companyDeleteDialog.getDialogTitle()).to.eq('hrmsApp.company.delete.question');
    await companyDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(companyComponentsPage.title), 5000);

    expect(await companyComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
