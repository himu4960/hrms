import { element, by, ElementFinder } from 'protractor';

export class CompanyComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('hrms-company div table .btn-danger'));
  title = element.all(by.css('hrms-company div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('hrmsTranslate');
  }
}

export class CompanyUpdatePage {
  pageTitle = element(by.id('hrms-company-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  idInput = element(by.id('field_id'));
  nameInput = element(by.id('field_name'));
  phoneInput = element(by.id('field_phone'));
  emailInput = element(by.id('field_email'));
  addressInput = element(by.id('field_address'));
  logo_url_largeInput = element(by.id('field_logo_url_large'));
  logo_url_mediumInput = element(by.id('field_logo_url_medium'));
  logo_url_smallInput = element(by.id('field_logo_url_small'));
  statusInput = element(by.id('field_status'));

  companyTypeSelect = element(by.id('field_companyType'));
  applicationTypeSelect = element(by.id('field_applicationType'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('hrmsTranslate');
  }

  async setIdInput(id: string): Promise<void> {
    await this.idInput.sendKeys(id);
  }

  async getIdInput(): Promise<string> {
    return await this.idInput.getAttribute('value');
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  async setPhoneInput(phone: string): Promise<void> {
    await this.phoneInput.sendKeys(phone);
  }

  async getPhoneInput(): Promise<string> {
    return await this.phoneInput.getAttribute('value');
  }

  async setEmailInput(email: string): Promise<void> {
    await this.emailInput.sendKeys(email);
  }

  async getEmailInput(): Promise<string> {
    return await this.emailInput.getAttribute('value');
  }

  async setAddressInput(address: string): Promise<void> {
    await this.addressInput.sendKeys(address);
  }

  async getAddressInput(): Promise<string> {
    return await this.addressInput.getAttribute('value');
  }

  async setLogoUrlLargeInput(logo_url_large: string): Promise<void> {
    await this.logo_url_largeInput.sendKeys(logo_url_large);
  }

  async getLogoUrlLargeInput(): Promise<string> {
    return await this.logo_url_largeInput.getAttribute('value');
  }

  async setLogoUrlMediumInput(logo_url_medium: string): Promise<void> {
    await this.logo_url_mediumInput.sendKeys(logo_url_medium);
  }

  async getLogouUrlMediumInput(): Promise<string> {
    return await this.logo_url_mediumInput.getAttribute('value');
  }

  async setLogoUrlSmallInput(logo_url_small: string): Promise<void> {
    await this.logo_url_smallInput.sendKeys(logo_url_small);
  }

  async getLogoUrlSmallInput(): Promise<string> {
    return await this.logo_url_smallInput.getAttribute('value');
  }

  getStatusInput(): ElementFinder {
    return this.statusInput;
  }

  async companyTypeSelectLastOption(): Promise<void> {
    await this.companyTypeSelect.all(by.tagName('option')).last().click();
  }

  async companyTypeSelectOption(option: string): Promise<void> {
    await this.companyTypeSelect.sendKeys(option);
  }

  getCompanyTypeSelect(): ElementFinder {
    return this.companyTypeSelect;
  }

  async getCompanyTypeSelectedOption(): Promise<string> {
    return await this.companyTypeSelect.element(by.css('option:checked')).getText();
  }

  async applicationTypeSelectLastOption(): Promise<void> {
    await this.applicationTypeSelect.all(by.tagName('option')).last().click();
  }

  async applicationTypeSelectOption(option: string): Promise<void> {
    await this.applicationTypeSelect.sendKeys(option);
  }

  getApplicationTypeSelect(): ElementFinder {
    return this.applicationTypeSelect;
  }

  async getApplicationTypeSelectedOption(): Promise<string> {
    return await this.applicationTypeSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CompanyDeleteDialog {
  private dialogTitle = element(by.id('hrms-delete-company-heading'));
  private confirmButton = element(by.id('hrms-confirm-delete-company'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('hrmsTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
