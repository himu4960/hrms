import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { MenuComponentsPage, MenuDeleteDialog, MenuUpdatePage } from './menu.page-object';

const expect = chai.expect;

describe('Menu e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let menuComponentsPage: MenuComponentsPage;
  let menuUpdatePage: MenuUpdatePage;
  let menuDeleteDialog: MenuDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Menus', async () => {
    await navBarPage.goToEntity('menu');
    menuComponentsPage = new MenuComponentsPage();
    await browser.wait(ec.visibilityOf(menuComponentsPage.title), 5000);
    expect(await menuComponentsPage.getTitle()).to.eq('hrmsApp.menu.home.title');
    await browser.wait(ec.or(ec.visibilityOf(menuComponentsPage.entities), ec.visibilityOf(menuComponentsPage.noResult)), 1000);
  });

  it('should load create Menu page', async () => {
    await menuComponentsPage.clickOnCreateButton();
    menuUpdatePage = new MenuUpdatePage();
    expect(await menuUpdatePage.getPageTitle()).to.eq('hrmsApp.menu.home.createOrEditLabel');
    await menuUpdatePage.cancel();
  });

  it('should create and save Menus', async () => {
    const nbButtonsBeforeCreate = await menuComponentsPage.countDeleteButtons();

    await menuComponentsPage.clickOnCreateButton();

    await promise.all([
      menuUpdatePage.setNameInput('name'),
      menuUpdatePage.setIconInput('icon'),
      menuUpdatePage.setEnd_pointInput('end_point'),
      menuUpdatePage.setRouter_linkInput('router_link'),
      menuUpdatePage.setPermissionsInput('permissions'),
      menuUpdatePage.setParent_idInput('5'),
      menuUpdatePage.setPositionInput('5'),
    ]);

    expect(await menuUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await menuUpdatePage.getIconInput()).to.eq('icon', 'Expected Icon value to be equals to icon');
    expect(await menuUpdatePage.getEnd_pointInput()).to.eq('end_point', 'Expected End_point value to be equals to end_point');
    expect(await menuUpdatePage.getRouter_linkInput()).to.eq('router_link', 'Expected Router_link value to be equals to router_link');
    expect(await menuUpdatePage.getPermissionsInput()).to.eq('permissions', 'Expected Permissions value to be equals to permissions');
    expect(await menuUpdatePage.getParent_idInput()).to.eq('5', 'Expected parent_id value to be equals to 5');
    expect(await menuUpdatePage.getPositionInput()).to.eq('5', 'Expected position value to be equals to 5');
    const selectedIs_active = menuUpdatePage.getIs_activeInput();
    if (await selectedIs_active.isSelected()) {
      await menuUpdatePage.getIs_activeInput().click();
      expect(await menuUpdatePage.getIs_activeInput().isSelected(), 'Expected is_active not to be selected').to.be.false;
    } else {
      await menuUpdatePage.getIs_activeInput().click();
      expect(await menuUpdatePage.getIs_activeInput().isSelected(), 'Expected is_active to be selected').to.be.true;
    }
    const selectedIs_deleted = menuUpdatePage.getIs_deletedInput();
    if (await selectedIs_deleted.isSelected()) {
      await menuUpdatePage.getIs_deletedInput().click();
      expect(await menuUpdatePage.getIs_deletedInput().isSelected(), 'Expected is_deleted not to be selected').to.be.false;
    } else {
      await menuUpdatePage.getIs_deletedInput().click();
      expect(await menuUpdatePage.getIs_deletedInput().isSelected(), 'Expected is_deleted to be selected').to.be.true;
    }

    await menuUpdatePage.save();
    expect(await menuUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await menuComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Menu', async () => {
    const nbButtonsBeforeDelete = await menuComponentsPage.countDeleteButtons();
    await menuComponentsPage.clickOnLastDeleteButton();

    menuDeleteDialog = new MenuDeleteDialog();
    expect(await menuDeleteDialog.getDialogTitle()).to.eq('hrmsApp.menu.delete.question');
    await menuDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(menuComponentsPage.title), 5000);

    expect(await menuComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
