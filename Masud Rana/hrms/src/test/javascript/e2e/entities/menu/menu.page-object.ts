import { element, by, ElementFinder } from 'protractor';

export class MenuComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('hrms-menu div table .btn-danger'));
  title = element.all(by.css('hrms-menu div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('hrmsTranslate');
  }
}

export class MenuUpdatePage {
  pageTitle = element(by.id('hrms-menu-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  idInput = element(by.id('field_id'));
  nameInput = element(by.id('field_name'));
  iconInput = element(by.id('field_icon'));
  end_pointInput = element(by.id('field_end_point'));
  router_linkInput = element(by.id('field_router_link'));
  permissionsInput = element(by.id('field_permissions'));
  parent_idInput = element(by.id('field_parent_id'));
  positionInput = element(by.id('field_position'));
  is_activeInput = element(by.id('field_is_active'));
  is_deletedInput = element(by.id('field_is_deleted'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('hrmsTranslate');
  }

  async setIdInput(id: string): Promise<void> {
    await this.idInput.sendKeys(id);
  }

  async getIdInput(): Promise<string> {
    return await this.idInput.getAttribute('value');
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  async setIconInput(icon: string): Promise<void> {
    await this.iconInput.sendKeys(icon);
  }

  async getIconInput(): Promise<string> {
    return await this.iconInput.getAttribute('value');
  }

  async setEnd_pointInput(end_point: string): Promise<void> {
    await this.end_pointInput.sendKeys(end_point);
  }

  async getEnd_pointInput(): Promise<string> {
    return await this.end_pointInput.getAttribute('value');
  }

  async setRouter_linkInput(router_link: string): Promise<void> {
    await this.router_linkInput.sendKeys(router_link);
  }

  async getRouter_linkInput(): Promise<string> {
    return await this.router_linkInput.getAttribute('value');
  }

  async setPermissionsInput(permissions: string): Promise<void> {
    await this.permissionsInput.sendKeys(permissions);
  }

  async getPermissionsInput(): Promise<string> {
    return await this.permissionsInput.getAttribute('value');
  }

  async setParent_idInput(parent_id: string): Promise<void> {
    await this.parent_idInput.sendKeys(parent_id);
  }

  async getParent_idInput(): Promise<string> {
    return await this.parent_idInput.getAttribute('value');
  }

  async setPositionInput(position: string): Promise<void> {
    await this.positionInput.sendKeys(position);
  }

  async getPositionInput(): Promise<string> {
    return await this.positionInput.getAttribute('value');
  }

  getIs_activeInput(): ElementFinder {
    return this.is_activeInput;
  }

  getIs_deletedInput(): ElementFinder {
    return this.is_deletedInput;
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class MenuDeleteDialog {
  private dialogTitle = element(by.id('hrms-delete-menu-heading'));
  private confirmButton = element(by.id('hrms-confirm-delete-menu'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('hrmsTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
