import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { WorkShiftComponentsPage, WorkShiftDeleteDialog, WorkShiftUpdatePage } from './work-shift.page-object';

const expect = chai.expect;

describe('WorkShift e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let workShiftComponentsPage: WorkShiftComponentsPage;
  let workShiftUpdatePage: WorkShiftUpdatePage;
  let workShiftDeleteDialog: WorkShiftDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load WorkShifts', async () => {
    await navBarPage.goToEntity('work-shift');
    workShiftComponentsPage = new WorkShiftComponentsPage();
    await browser.wait(ec.visibilityOf(workShiftComponentsPage.title), 5000);
    expect(await workShiftComponentsPage.getTitle()).to.eq('hrmsApp.workShift.home.title');
    await browser.wait(ec.or(ec.visibilityOf(workShiftComponentsPage.entities), ec.visibilityOf(workShiftComponentsPage.noResult)), 1000);
  });

  it('should load create WorkShift page', async () => {
    await workShiftComponentsPage.clickOnCreateButton();
    workShiftUpdatePage = new WorkShiftUpdatePage();
    expect(await workShiftUpdatePage.getPageTitle()).to.eq('hrmsApp.workShift.home.createOrEditLabel');
    await workShiftUpdatePage.cancel();
  });

  it('should create and save WorkShifts', async () => {
    const nbButtonsBeforeCreate = await workShiftComponentsPage.countDeleteButtons();

    await workShiftComponentsPage.clickOnCreateButton();

    await promise.all([
      workShiftUpdatePage.setCompany_idInput('5'),
      workShiftUpdatePage.setBranch_idInput('5'),
      workShiftUpdatePage.setTitleInput('title'),
      workShiftUpdatePage.setDesignation_idInput('5'),
      workShiftUpdatePage.setPublished_atInput('2000-12-31'),
      workShiftUpdatePage.setPublished_byInput('5'),
      workShiftUpdatePage.setStart_dateInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      workShiftUpdatePage.setEnd_dateInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      workShiftUpdatePage.setStart_timeInput('2000-12-31'),
      workShiftUpdatePage.setEnd_timeInput('2000-12-31'),
      workShiftUpdatePage.setTotal_work_shift_durationInput('5'),
      workShiftUpdatePage.setDeleted_atInput('2000-12-31'),
      workShiftUpdatePage.setDeleted_byInput('5'),
    ]);

    expect(await workShiftUpdatePage.getCompany_idInput()).to.eq('5', 'Expected company_id value to be equals to 5');
    expect(await workShiftUpdatePage.getBranch_idInput()).to.eq('5', 'Expected branch_id value to be equals to 5');
    expect(await workShiftUpdatePage.getTitleInput()).to.eq('title', 'Expected Title value to be equals to title');
    expect(await workShiftUpdatePage.getDesignation_idInput()).to.eq('5', 'Expected designation_id value to be equals to 5');
    const selectedIs_published = workShiftUpdatePage.getIs_publishedInput();
    if (await selectedIs_published.isSelected()) {
      await workShiftUpdatePage.getIs_publishedInput().click();
      expect(await workShiftUpdatePage.getIs_publishedInput().isSelected(), 'Expected is_published not to be selected').to.be.false;
    } else {
      await workShiftUpdatePage.getIs_publishedInput().click();
      expect(await workShiftUpdatePage.getIs_publishedInput().isSelected(), 'Expected is_published to be selected').to.be.true;
    }
    expect(await workShiftUpdatePage.getPublished_atInput()).to.eq('2000-12-31', 'Expected published_at value to be equals to 2000-12-31');
    expect(await workShiftUpdatePage.getPublished_byInput()).to.eq('5', 'Expected published_by value to be equals to 5');
    expect(await workShiftUpdatePage.getStart_dateInput()).to.contain(
      '2001-01-01T02:30',
      'Expected start_date value to be equals to 2000-12-31'
    );
    expect(await workShiftUpdatePage.getEnd_dateInput()).to.contain(
      '2001-01-01T02:30',
      'Expected end_date value to be equals to 2000-12-31'
    );
    expect(await workShiftUpdatePage.getStart_timeInput()).to.eq('2000-12-31', 'Expected start_time value to be equals to 2000-12-31');
    expect(await workShiftUpdatePage.getEnd_timeInput()).to.eq('2000-12-31', 'Expected end_time value to be equals to 2000-12-31');
    expect(await workShiftUpdatePage.getTotal_work_shift_durationInput()).to.eq(
      '5',
      'Expected total_work_shift_duration value to be equals to 5'
    );
    expect(await workShiftUpdatePage.getDeleted_atInput()).to.eq('2000-12-31', 'Expected deleted_at value to be equals to 2000-12-31');
    expect(await workShiftUpdatePage.getDeleted_byInput()).to.eq('5', 'Expected deleted_by value to be equals to 5');

    await workShiftUpdatePage.save();
    expect(await workShiftUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await workShiftComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last WorkShift', async () => {
    const nbButtonsBeforeDelete = await workShiftComponentsPage.countDeleteButtons();
    await workShiftComponentsPage.clickOnLastDeleteButton();

    workShiftDeleteDialog = new WorkShiftDeleteDialog();
    expect(await workShiftDeleteDialog.getDialogTitle()).to.eq('hrmsApp.workShift.delete.question');
    await workShiftDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(workShiftComponentsPage.title), 5000);

    expect(await workShiftComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
