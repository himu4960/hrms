import { element, by, ElementFinder } from 'protractor';

export class WorkShiftComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('hrms-work-shift div table .btn-danger'));
  title = element.all(by.css('hrms-work-shift div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('hrmsTranslate');
  }
}

export class WorkShiftUpdatePage {
  pageTitle = element(by.id('hrms-work-shift-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  idInput = element(by.id('field_id'));
  company_idInput = element(by.id('field_company_id'));
  branch_idInput = element(by.id('field_branch_id'));
  titleInput = element(by.id('field_title'));
  designation_idInput = element(by.id('field_designation_id'));
  is_publishedInput = element(by.id('field_is_published'));
  published_atInput = element(by.id('field_published_at'));
  published_byInput = element(by.id('field_published_by'));
  start_dateInput = element(by.id('field_start_date'));
  end_dateInput = element(by.id('field_end_date'));
  start_timeInput = element(by.id('field_start_time'));
  end_timeInput = element(by.id('field_end_time'));
  total_work_shift_durationInput = element(by.id('field_total_work_shift_duration'));
  deleted_atInput = element(by.id('field_deleted_at'));
  deleted_byInput = element(by.id('field_deleted_by'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('hrmsTranslate');
  }

  async setIdInput(id: string): Promise<void> {
    await this.idInput.sendKeys(id);
  }

  async getIdInput(): Promise<string> {
    return await this.idInput.getAttribute('value');
  }

  async setCompany_idInput(company_id: string): Promise<void> {
    await this.company_idInput.sendKeys(company_id);
  }

  async getCompany_idInput(): Promise<string> {
    return await this.company_idInput.getAttribute('value');
  }

  async setBranch_idInput(branch_id: string): Promise<void> {
    await this.branch_idInput.sendKeys(branch_id);
  }

  async getBranch_idInput(): Promise<string> {
    return await this.branch_idInput.getAttribute('value');
  }

  async setTitleInput(title: string): Promise<void> {
    await this.titleInput.sendKeys(title);
  }

  async getTitleInput(): Promise<string> {
    return await this.titleInput.getAttribute('value');
  }

  async setDesignation_idInput(designation_id: string): Promise<void> {
    await this.designation_idInput.sendKeys(designation_id);
  }

  async getDesignation_idInput(): Promise<string> {
    return await this.designation_idInput.getAttribute('value');
  }

  getIs_publishedInput(): ElementFinder {
    return this.is_publishedInput;
  }

  async setPublished_atInput(published_at: string): Promise<void> {
    await this.published_atInput.sendKeys(published_at);
  }

  async getPublished_atInput(): Promise<string> {
    return await this.published_atInput.getAttribute('value');
  }

  async setPublished_byInput(published_by: string): Promise<void> {
    await this.published_byInput.sendKeys(published_by);
  }

  async getPublished_byInput(): Promise<string> {
    return await this.published_byInput.getAttribute('value');
  }

  async setStart_dateInput(start_date: string): Promise<void> {
    await this.start_dateInput.sendKeys(start_date);
  }

  async getStart_dateInput(): Promise<string> {
    return await this.start_dateInput.getAttribute('value');
  }

  async setEnd_dateInput(end_date: string): Promise<void> {
    await this.end_dateInput.sendKeys(end_date);
  }

  async getEnd_dateInput(): Promise<string> {
    return await this.end_dateInput.getAttribute('value');
  }

  async setStart_timeInput(start_time: string): Promise<void> {
    await this.start_timeInput.sendKeys(start_time);
  }

  async getStart_timeInput(): Promise<string> {
    return await this.start_timeInput.getAttribute('value');
  }

  async setEnd_timeInput(end_time: string): Promise<void> {
    await this.end_timeInput.sendKeys(end_time);
  }

  async getEnd_timeInput(): Promise<string> {
    return await this.end_timeInput.getAttribute('value');
  }

  async setTotal_work_shift_durationInput(total_work_shift_duration: string): Promise<void> {
    await this.total_work_shift_durationInput.sendKeys(total_work_shift_duration);
  }

  async getTotal_work_shift_durationInput(): Promise<string> {
    return await this.total_work_shift_durationInput.getAttribute('value');
  }

  async setDeleted_atInput(deleted_at: string): Promise<void> {
    await this.deleted_atInput.sendKeys(deleted_at);
  }

  async getDeleted_atInput(): Promise<string> {
    return await this.deleted_atInput.getAttribute('value');
  }

  async setDeleted_byInput(deleted_by: string): Promise<void> {
    await this.deleted_byInput.sendKeys(deleted_by);
  }

  async getDeleted_byInput(): Promise<string> {
    return await this.deleted_byInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class WorkShiftDeleteDialog {
  private dialogTitle = element(by.id('hrms-delete-workShift-heading'));
  private confirmButton = element(by.id('hrms-confirm-delete-workShift'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('hrmsTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
