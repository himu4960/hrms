{
  "settings": {
    "title": "User settings for [<strong>{{username}}</strong>]",
    "form": {
      "firstname": "First Name",
      "firstname.placeholder": "Your first name",
      "lastname": "Last Name",
      "lastname.placeholder": "Your last name",
      "language": "Language",
      "button": "Save Settings"
    },
    "messages": {
      "error": {
        "fail": "<strong>An error has occurred!</strong> Settings could not be saved.",
        "emailexists": "<strong>Email is already in use!</strong> Please choose another one."
      },
      "success": "<strong>Settings saved!</strong>",
      "validate": {
        "firstname": {
          "required": "Your first name is required.",
          "minlength": "Your first name is required to be at least 1 character",
          "maxlength": "Your first name cannot be longer than 50 characters"
        },
        "lastname": {
          "required": "Your last name is required.",
          "minlength": "Your last name is required to be at least 1 character",
          "maxlength": "Your last name cannot be longer than 50 characters"
        }
      }
    },
    "sidebar": {
      "application": "Application",
      "shiftPlanning": "Shift Planning",
      "timeClock": "Time Clock",
      "leaveAndAvailability": "Leave & Availability"
    },
    "selectBox": {
      "weekName": {
        "saturday": "Saturday",
        "sunday": "Sunday",
        "monday": "Monday",
        "tuesday": "Tuesday",
        "wednesday": "Wednesday",
        "thursday": "Thursday",
        "friday": "Friday"
      },
      "shifts": {
        "allShift": "All Shift",
        "week1": "1 Week",
        "week2": "2 Weeks",
        "week3": "3 Weeks",
        "month1": "1 Month",
        "month2": "2 Months",
        "month3": "3 Months",
        "month4": "4 Months",
        "month5": "5 Months"
      },
      "requiredSkill": {
        "off": "Off",
        "any": "Any",
        "all": "All"
      }
    }
  },
  "applicationSettings": {
    "location": {
      "title": "Local",
      "country.label": "Country",
      "timezone.label": "Timezone",
      "deafult_language.label": "Deafult Language",
      "date_format.label": "Date Format"
    },
    "employee": {
      "title": "Employee Settings",
      "employee_name_format.label": "Naming format of Employee",
      "employee_can_view_report.label": "Allow employees to view reports",
      "employee_can_edit_profile.label": "Allow employees to edit profile",
      "employee_can_view_staff_gallery.label": "Allow employees to view gallery of other employees",
      "employee_can_view_staff_details.label": "Allow employees to view details of other employees",
      "employee_mgnt_can_view_eid.label": "Allow management to view employee code",
      "employee_can_view_whos_on.label": "Allow employees to view 'Who’s on Now' list",
      "employee_can_request_account.label": "Allow employees to request an account",
      "employee_can_view_uploaded_files.label": "Allow employees to view uploaded files"
    },
    "auth": {
      "title": "Auth Settings",
      "auth_auto_logout_time.label": "Auto logout time in minutes",
      "auth_password_expiry_time.label": "Set the automatic password expiration date",
      "office_working_hours.label": "Office working hours"
    }
  },
  "shiftPlanningSettings": {
    "planning_option_title": "Shift Planning Options",
    "employee_can_add_work_units.label": "Allow employees to add work units",
    "employee_can_be_scheduled_to_immediate_shifts.label": "Allow employees to be scheduled for immediate shifts",
    "employee_can_drop_shifts.label": "Allow employee to drop shifts",
    "employee_can_release_shifts.label": "Allow employee to release shift",
    "employee_can_see_assigned_staffs.label": "Allow the employee to view other assigned employees",
    "employee_can_see_co_worker_shifts.label": "Allow the employee to view others shifts",
    "employee_can_see_co_worker_vacations.label": "Allow the employee to view others vacation",
    "employee_can_view_all_schedules.label": "Allow the employee to view all schedules",
    "employee_can_view_shifts_in_advance.label": "Allow employee to view shifts in advance",
    "notes_permissions_allowed_users.label": "Notes permissions allowed for",
    "required_skills_on_shift.label": "Is skill required on shift",

    "pickup": "Shift Pickup",
    "trades_approvals_title": "Trades and Approvals",
    "shift_notes_visibility_allowed_users.label": "Allow employee to view shift notes",
    "shift_planning_start_day.label": "Shift planning start day",
    "schedule_automatic_conflicts_allowed.label": "Allow scheduling conflicts",
    "shift_overlapping_disallowed.label": "Restrict overlapping shift",
    "show_employee_skills_in_scheduler.label": "Allow employee to view skills in scheduler",
    "show_overnight_shifts.label": "Allow to view overnight shift",
    "longer_than_24hours_shifts_allowed.label": "Allow to create longer than 24 hours shift",
    "past_date_shifts_allowed.label": "Allow past dates to be set on shifts",
    "show_pending_leave_request.label": "Allow to view pending leave request",
    "use_draft_publish_schedule_method.label": "Allow employee to create draft schedule",
    "work_unit_module_enabled.label": "Work unit module enabled",
    "selectBox": {
      "allowedUsers": {
        "managerAndSupervisor": "Managers And Supervisors",
        "managerAndSupervisorAndScheduler": "Managers, Supervisors And Scheduler",
        "allUser": "All Users"
      }
    }
  },
  "timeClockSettings": {
    "title": "Time Clock Settings",
    "selectBox": {
      "minutes": {
        "OFF": "Off",
        "MIN_10": "10 Mintues",
        "MIN_15": "15 Mintues",
        "MIN_20": "20 Mintues",
        "MIN_25": "25 Mintues",
        "MIN_30": "30 Mintues"
      },
      "reminder": {
        "BEFORE_15_MIN_SHIFT_START": "Before 15 Minutes Shift Start",
        "BEGINNING_SHIFT": "Beginning Shift",
        "AFTER_15_MIN_SHIFT_START": "After 15 Minutes Shift Start",
        "AFTER_30_MIN_SHIFT_START": "After 30 Minutes Shift Start",
        "AFTER_45_MIN_SHIFT_START": "After 45 Minutes Shift Start",
        "AFTER_1_HOUR_SHIFT_START": "After 1 Hour Shift Start"
      },
      "days": {
        "anytime": "Anytime",
        "DAY_1": "1 Day",
        "DAY_3": "3 Days",
        "DAY_5": "5 Days",
        "DAY_10": "10 Days",
        "DAY_15": "15 Days",
        "DAY_30": "30 Days",
        "DAY_60": "60 Days"
      }
    },
    "enable_auto_punch_out.label": "Enable auto punch out",
    "allow_employee_to_work_after_shift.label": "Allow employees to work after shift in minutes",
    "late_countable_time_start.label": "Set pre-shift punch-in time in minutes",
    "pre_clock_countable_time_start.label": "Allow to pre-shift punch-in",
    "break_button_enabled.label": "Allow the employee to take a break",
    "notification_late_reminder_for_managers.label": "Notification late reminder for managers",
    "notification_late_reminder_for_schedulers.label": "Notification late reminder for schedulers",
    "notification_not_clocked_out_reminder_for_managers.label": "Notification not clocked out reminder for managers",
    "notification_not_clocked_out_reminder_for_schedulers.label": "Notification not clocked out reminder for schedulers",
    "pre_clock_enabled.label": "Pre clock enabled",
    "restrict_employee_clock_in_out.label": "Restrict employee to clock in & clock out",
    "should_have_position_set.label": "Is Allow to set position",
    "should_have_remote_site_set.label": "Should have remote site set",
    "show_address_under_time_clock.label": "Show address under time clock set",
    "time_clock_enabled.label": "Time clock enabled",
    "time_clock_tips_enabled.label": "Time clock tips enabled",
    "timesheet_allow_employee_to_add_past_x_days.label": "Allow employee to add the last x days to the timesheet",
    "timesheet_empl_can_edit.label": "Allow employees to edit timesheets",
    "timesheet_employee_can_edit_without_reason.label": "Allow employees to edit timesheets without any reason",
    "timesheet_employee_can_import.label": "Allow employees to import timesheets",
    "timesheet_employee_can_manually_add.label": "Allow employees to manually add timesheets",
    "timesheet_employee_can_view_notes.label": "Allow employees to view timesheet notes",
    "timesheet_notes_mandatory.label": "Timesheet notes mandatory",
    "timesheet_prevent_employee_to_edit_approved_time.label": "Timesheet prevent employee to edit approved time",
    "pre_clock_mandatory.label": "Set Pre clock mandatory"
  },
  "leaveAndAvailabilitySettings": {
    "title": "Leave & Availability Settings",
    "leave_module_enabled.label": "Leave module enabled",
    "deduct_weekends_from_leaves.label": "Set weekend days",
    "employees_can_book_vacation.label": "Allow employee to request leave",
    "employees_can_cancel_previously_approved_leave_requests.label": "Allow employee to cancel approved leave"
  }
}
