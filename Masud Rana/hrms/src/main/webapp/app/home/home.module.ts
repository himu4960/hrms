import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { AnnouncementModule } from 'app/announcement/announcement.module';
import { OnLeaveModule } from './components/on-leave/on-leave.module';
import { LateForWorkModule } from './components/late-for-work/late-for-work.module';
import { SharedFilesModule } from './components/shared-files/shared-files.module';
import { AnniversaryModule } from './components/anniversary/anniversary.module';
import { BirthdayModule } from './components/birthday/birthday.module';
import { EmployeeOnlineModule } from 'app/employee/components/employee-online/employee-online.module';
import { TimeClockAddModule } from './../time-clock/components/time-clock-add/time-clock-add.module';
import { UpcomingShiftsModule } from './components/upcoming-shifts/upcoming-shifts.module';

import { HomeComponent } from './home.component';
import { FileDeleteDeleteDialogComponent } from './components/shared-files/delete/file-delete-dialog.component';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';
@NgModule({
  imports: [
    SharedModule,
    AnnouncementModule,
    OnLeaveModule,
    LateForWorkModule,
    SharedFilesModule,
    AnniversaryModule,
    BirthdayModule,
    EmployeeOnlineModule,
    TimeClockAddModule,
    UpcomingShiftsModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomeComponent,
        canActivate: [AuthGuard],
      },
    ]),
  ],
  declarations: [HomeComponent, FileDeleteDeleteDialogComponent],
  entryComponents: [FileDeleteDeleteDialogComponent],
})
export class HomeModule {}
