import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { SharedFilesComponent } from './shared-files.component';

@NgModule({
  imports: [SharedModule],
  declarations: [SharedFilesComponent],
  exports: [SharedFilesComponent],
})
export class SharedFilesModule {}
