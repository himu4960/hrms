import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';

import { AppState } from 'app/store';

import { IAnniversary } from './anniversary.model';
import { ITimeZone } from 'app/entities/time-zone/time-zone.model';

import { EFilterMonth } from 'app/shared/enum/filter-month.enum';

import { EmployeeService } from 'app/employee/service/employee.service';

import { selectApplicationSettings } from 'app/store/company-settings/company-settings.selectors';

import { ApplicationSettings } from 'app/store/states/application-settings.interface';

@Component({
  selector: 'hrms-anniversary',
  templateUrl: './anniversary.component.html',
  styleUrls: ['./anniversary.component.scss'],
})
export class AnniversaryComponent implements OnInit {
  @Input() employeesAnniversary: IAnniversary[] = [];
  timeZone!: ITimeZone;

  private subscriptions: Subscription = new Subscription();

  constructor(private store: Store<AppState>, private readonly employeeService: EmployeeService) {}

  get eFilterMonth(): any {
    return EFilterMonth;
  }

  ngOnInit(): void {
    this.getTimeZone();
  }

  public filterAnniversary(e: any): void {
    const value = Number(e.target.value);
    if (value === EFilterMonth.THIS_MONTH) {
      const currentMonth = new Date().getUTCMonth();
      this.loadAnniversary(currentMonth);
    }
    if (value === EFilterMonth.NEXT_MONTH) {
      const nextMonth = new Date().getUTCMonth() + 1;
      this.loadAnniversary(nextMonth);
    }
  }

  public getTimeZone(): void {
    this.subscriptions.add(
      this.store.select(selectApplicationSettings).subscribe((settings: ApplicationSettings) => {
        if (settings) {
          this.timeZone = settings.time_zone;
        }
      })
    );
  }

  public loadAnniversary(month?: number): void {
    this.employeeService.getEmployeesAnniversary(month).subscribe(res => {
      if (res.body) {
        this.employeesAnniversary = this.employeeService.findTodayAnniversaries(res.body);
      }
    });
  }
}
