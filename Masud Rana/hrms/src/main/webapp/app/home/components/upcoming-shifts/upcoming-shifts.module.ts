import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { UpcomingShiftsComponent } from './upcoming-shifts.component';
import { UpcomingShiftsDialogComponent } from './view/upcoming-shifts-dialog.component';

@NgModule({
  imports: [SharedModule],
  declarations: [UpcomingShiftsComponent, UpcomingShiftsDialogComponent],
  exports: [UpcomingShiftsComponent, UpcomingShiftsDialogComponent],
})
export class UpcomingShiftsModule {}
