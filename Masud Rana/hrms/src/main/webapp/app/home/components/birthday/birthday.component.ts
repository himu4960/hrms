import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';

import { AppState } from 'app/store';

import { IBirthday } from './birthday.model';
import { ITimeZone } from 'app/entities/time-zone/time-zone.model';

import { EFilterMonth } from 'app/shared/enum/filter-month.enum';

import { EmployeeService } from 'app/employee/service/employee.service';

import { selectApplicationSettings } from 'app/store/company-settings/company-settings.selectors';

import { ApplicationSettings } from 'app/store/states/application-settings.interface';

@Component({
  selector: 'hrms-birthday',
  templateUrl: './birthday.component.html',
  styleUrls: ['./birthday.component.scss'],
})
export class BirthdayComponent implements OnInit, OnDestroy {
  @Input() employeesBirthday: IBirthday[] = [];
  timeZone!: ITimeZone;

  private subscriptions: Subscription = new Subscription();

  constructor(private store: Store<AppState>, private readonly employeeService: EmployeeService) {}

  get eFilterMonth(): any {
    return EFilterMonth;
  }

  ngOnInit(): void {
    this.getTimeZone();
  }

  public filterBirthday(e: any): void {
    const value = Number(e.target.value);
    if (value === EFilterMonth.THIS_MONTH) {
      const currentMonth = new Date().getUTCMonth();
      this.loadBirthday(currentMonth);
    }
    if (value === EFilterMonth.NEXT_MONTH) {
      const nextMonth = new Date().getUTCMonth() + 1;
      this.loadBirthday(nextMonth);
    }
  }

  public getTimeZone(): void {
    this.subscriptions.add(
      this.store.select(selectApplicationSettings).subscribe((settings: ApplicationSettings) => {
        if (settings) {
          this.timeZone = settings.time_zone;
        }
      })
    );
  }

  public loadBirthday(month?: number): void {
    this.employeeService.getEmployeesBirthday(month).subscribe(res => {
      if (res.body) {
        this.employeesBirthday = this.employeeService.findTodayBirthdays(res.body);
      }
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
