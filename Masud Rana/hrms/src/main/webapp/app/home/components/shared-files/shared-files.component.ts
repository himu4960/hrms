import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { of, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { AppState } from 'app/store';

import { ITimeZone } from 'app/entities/time-zone/time-zone.model';
import { IFile } from '../../../file-upload/file.model';

import { selectApplicationSettings } from 'app/store/company-settings/company-settings.selectors';
import { ApplicationSettings } from 'app/store/states/application-settings.interface';

import { ToastService } from './../../../shared/components/toast/toast.service';
import { PermissionService } from './../../../shared/service/permission.service';
import { FileService } from '../../../file-upload/file.service';
import { FileDeleteDeleteDialogComponent } from './delete/file-delete-dialog.component';
import { EModalReasonType } from 'app/shared/enum';

@Component({
  selector: 'hrms-shared-files',
  templateUrl: './shared-files.component.html',
  styleUrls: ['./shared-files.component.scss'],
})
export class SharedFilesComponent implements OnInit, OnDestroy {
  isEmployee = true;
  @Input() files: any[] = [];
  timeZone!: ITimeZone;

  private subscriptions: Subscription = new Subscription();
  constructor(
    private fileService: FileService,
    private store: Store<AppState>,
    private toastService: ToastService,
    private translateService: TranslateService,
    private permissionService: PermissionService,
    protected modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.isEmployee = this.permissionService.isEmployee();
    this.loadTimeZone();
  }

  public loadTimeZone(): void {
    this.subscriptions.add(
      this.store
        .select(selectApplicationSettings)
        .pipe(
          switchMap((settings: ApplicationSettings) => {
            if (settings) {
              this.timeZone = settings.time_zone;
            }
            return of(null);
          })
        )
        .subscribe()
    );
  }

  public loadSharedFiles(): void {
    this.fileService.findAll().subscribe(res => {
      if (res?.body) {
        this.files = res.body;
      }
    });
  }

  public onDownload(file: IFile): void {
    if (file.id !== undefined) {
      this.fileService.onDownload(file).subscribe(res => {
        if (res.body) {
          window.open(res.body.path_url);
        }
      });
    }
  }

  public delete(id?: number): void {
    if (id !== undefined) {
      const modalRef = this.modalService.open(FileDeleteDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
      modalRef.componentInstance.id = id;
      modalRef.closed.subscribe(reason => {
        if (reason === EModalReasonType.DELETED) {
          this.toastService.showSuccessToast(this.translateService.instant('file.delete.success'));
          this.loadSharedFiles();
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
