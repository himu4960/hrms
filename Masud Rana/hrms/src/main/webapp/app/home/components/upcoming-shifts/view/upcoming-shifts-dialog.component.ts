import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IWorkShift } from './../../../../shift-planning/models/work-shift.model';

@Component({
  templateUrl: './upcoming-shifts-dialog.component.html',
})
export class UpcomingShiftsDialogComponent {
  upcomingShift?: IWorkShift;

  constructor(public activeModal: NgbActiveModal) {}

  public cancel(): void {
    this.activeModal.dismiss();
  }
}
