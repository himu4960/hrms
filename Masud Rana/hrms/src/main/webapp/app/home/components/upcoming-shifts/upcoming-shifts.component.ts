import { Component, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IWorkShift } from './../../../shift-planning/models/work-shift.model';
import { UpcomingShiftsDialogComponent } from './view/upcoming-shifts-dialog.component';

@Component({
  selector: 'hrms-upcoming-shifts',
  templateUrl: './upcoming-shifts.component.html',
})
export class UpcomingShiftsComponent {
  @Input() upcomingShifts: IWorkShift[] = [];
  @Input() titleClass: any = 'box-title text-blue-dark text-center mb-3 pt-1';

  constructor(protected modalService: NgbModal) {}

  public view(upcomingShift?: IWorkShift): void {
    const modalRef = this.modalService.open(UpcomingShiftsDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.upcomingShift = upcomingShift;
  }
}
