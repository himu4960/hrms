import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { LateForWorkComponent } from './late-for-work.component';

@NgModule({
  imports: [SharedModule],
  declarations: [LateForWorkComponent],
  exports: [LateForWorkComponent],
})
export class LateForWorkModule {}
