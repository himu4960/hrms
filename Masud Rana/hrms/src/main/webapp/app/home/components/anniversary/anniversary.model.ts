import { IEmployeeDesignation } from 'app/employee/models/employee.model';

export interface IAnniversary {
  code?: string;
  first_name: string;
  middle_name?: string;
  last_name: string;
  join_date: number;
  anniversary_date?: number;
  anniversary_month?: number;
  is_today_anniversary?: boolean;
  photo_url?: string;
  employee_designations: IEmployeeDesignation[];
}
