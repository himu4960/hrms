import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { AnniversaryComponent } from './anniversary.component';

@NgModule({
  imports: [SharedModule],
  declarations: [AnniversaryComponent],
  exports: [AnniversaryComponent],
})
export class AnniversaryModule {}
