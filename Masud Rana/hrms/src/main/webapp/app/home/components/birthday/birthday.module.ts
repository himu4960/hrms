import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { BirthdayComponent } from './birthday.component';

@NgModule({
  imports: [SharedModule],
  declarations: [BirthdayComponent],
  exports: [BirthdayComponent],
})
export class BirthdayModule {}
