import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { OnLeaveComponent } from './on-leave.component';

@NgModule({
  imports: [SharedModule],
  declarations: [OnLeaveComponent],
  exports: [OnLeaveComponent],
})
export class OnLeaveModule {}
