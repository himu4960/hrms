import { Component, Input } from '@angular/core';

@Component({
  selector: 'hrms-on-leave',
  templateUrl: './on-leave.component.html',
  styleUrls: ['./on-leave.component.scss'],
})
export class OnLeaveComponent {
  @Input() leaves: any[] = [];
}
