import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from 'app/store';

import { IFilterDateRange } from 'app/shared/forms/generic-filter-form/models/generic-filter.model';

import { TimeSheetReportService } from 'app/reports/time-sheet/time-sheet-report.service';
import { GenericFilterService } from 'app/shared/forms/generic-filter-form/generic-filter.service';

@Component({
  selector: 'hrms-late-for-work',
  templateUrl: './late-for-work.component.html',
  styleUrls: ['./late-for-work.component.scss'],
})
export class LateForWorkComponent {
  isSaving = false;
  @Input() lates: any[] = [];

  constructor(
    private store: Store<AppState>,
    private readonly timeSheetReportService: TimeSheetReportService,
    private readonly genericFilterService: GenericFilterService
  ) {}

  filter(): void {
    this.isSaving = true;
    const filterRange: IFilterDateRange = this.genericFilterService.getFilterRange(0, new Date(), new Date());
    const filterParam: any = {
      start_Date: filterRange.start_date,
      end_date: filterRange.end_date,
    };
    this.timeSheetReportService.getTimeSheetLateSummary(filterParam).subscribe(
      (data: any) => {
        this.lates = data.body;
        this.isSaving = false;
      },
      () => {
        this.isSaving = false;
      }
    );
  }
}
