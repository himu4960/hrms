import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { EModalReasonType } from 'app/shared/enum';

import { FileService } from '../../../../file-upload/file.service';

@Component({
  templateUrl: './file-delete-dialog.component.html',
})
export class FileDeleteDeleteDialogComponent {
  isSaving = false;
  id?: number;

  constructor(public activeModal: NgbActiveModal, private fileService: FileService) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(): void {
    if (this.id !== undefined) {
      this.isSaving = true;
      this.fileService.delete(this.id).subscribe(() => {
        this.isSaving = false;
        this.activeModal.close(EModalReasonType.DELETED);
      });
    }
  }
}
