import { IEmployeeDesignation } from 'app/employee/models/employee.model';

export interface IBirthday {
  code?: string;
  first_name: string;
  middle_name?: string;
  last_name: string;
  dob: number;
  birthday_date?: number;
  birthday_month?: number;
  is_today_birthday?: boolean;
  photo_url?: string;
  employee_designations: IEmployeeDesignation[];
}
