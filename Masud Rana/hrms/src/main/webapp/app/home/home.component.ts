import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { AppState } from 'app/store';

import { Account } from 'app/core/auth/account.model';
import { IHomeData } from 'app/shared/models/common.model';
import { IWorkShift } from 'app/shift-planning/models/work-shift.model';

import { AccountService } from 'app/core/auth/account.service';
import { CommonService } from './../shared/service/common.service';

import { selectApplicationSettings } from 'app/store/company-settings/company-settings.selectors';

import { ApplicationSettings } from 'app/store/states/application-settings.interface';

@Component({
  selector: 'hrms-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HomeComponent implements OnInit, OnDestroy {
  leaveRequestCount = 0;
  allShiftCount = 0;
  unpublishedShiftCount = 0;
  timeSheetCount = 0;
  announcements: any[] = [];
  lates: any[] = [];
  leaves: any[] = [];
  files: any[] = [];
  upcomingShifts: IWorkShift[] = [];
  employeesAnniversary: any[] = [];
  employeesBirthday: any[] = [];
  account: Account | null = null;
  authSubscription?: Subscription;

  private subscriptions: Subscription = new Subscription();

  constructor(
    private store: Store<AppState>,
    private accountService: AccountService,
    private router: Router,
    private readonly commonService: CommonService
  ) {}

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => {
      this.account = account;
    });

    this.subscriptions.add(
      this.store.select(selectApplicationSettings).subscribe((settings: ApplicationSettings) => {
        if (settings) {
          if (settings.locale_country_id) {
            this.loadHomeData();
          }
        }
      })
    );
  }

  public loadHomeData(): void {
    this.commonService.getDashboardData().subscribe((homeData: IHomeData) => {
      this.lates = homeData.lates;
      this.announcements = homeData.announcements;
      this.leaves = homeData.on_leaves;
      this.files = homeData.files;
      this.employeesAnniversary = homeData.anniversaries;
      this.employeesBirthday = homeData.birthdays;
      this.leaveRequestCount = homeData.leave_request_count;
      this.allShiftCount = homeData.all_shift_count;
      this.unpublishedShiftCount = homeData.unpublished_shift_count;
      this.timeSheetCount = homeData.time_sheet_count;
      this.upcomingShifts = homeData.upcoming_shifts;
    });
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.router.navigate(['/login']);
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
    this.subscriptions.unsubscribe();
  }
}
