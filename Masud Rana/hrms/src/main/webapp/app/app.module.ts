import { NgModule, LOCALE_ID, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import locale from '@angular/common/locales/en';
import { BrowserModule, Title } from '@angular/platform-browser';
import { ServiceWorkerModule } from '@angular/service-worker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { NgbDateAdapter, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgProgressModule } from 'ngx-progressbar';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import * as dayjs from 'dayjs';

// NgRx
import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { SharedModule } from 'app/shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { EntityRoutingModule } from './entities/entity-routing.module';

// jhipster-needle-angular-add-module-import JHipster will add new module here
import './config/dayjs';
import { NgbDateDayjsAdapter } from './config/datepicker-adapter';
import { fontAwesomeIcons } from './config/font-awesome-icons';
import { translatePartialLoader, missingTranslationHandler } from './config/translation.config';

import { httpInterceptorProviders } from 'app/core/interceptor/index';

import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';

import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';

import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';
import { MainNavigationComponent } from './layouts/main-navigation/main-navigation.component';

import { environment } from './environments/environment';

import { effects, metaReducers, reducers } from './store';

import { CustomRouteSerializer } from './store/route/custom-route-serializer';

import { AutoLogoutService } from './core/auth/auto-logout.service';

export const config: SocketIoConfig = {
  url: environment.URL,
};

@NgModule({
  imports: [
    SocketIoModule.forRoot(config),
    NgProgressModule,
    BrowserAnimationsModule,
    BrowserModule,
    NgxSpinnerModule,
    SharedModule,
    NgMultiSelectDropDownModule.forRoot(),
    // jhipster-needle-angular-add-module JHipster will add new module here
    EntityRoutingModule,
    AppRoutingModule,
    // Set this to true to enable service worker (PWA)
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: false }),
    HttpClientModule,
    NgxWebstorageModule.forRoot({ prefix: 'hrms', separator: '-', caseSensitive: true }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translatePartialLoader,
        deps: [HttpClient],
      },
      missingTranslationHandler: {
        provide: MissingTranslationHandler,
        useFactory: missingTranslationHandler,
      },
    }),
    // NgRx
    StoreRouterConnectingModule.forRoot({
      serializer: CustomRouteSerializer,
    }),
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot(effects),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    Title,
    { provide: LOCALE_ID, useValue: 'en' },
    { provide: NgbDateAdapter, useClass: NgbDateDayjsAdapter },
    httpInterceptorProviders,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, MainNavigationComponent],
  bootstrap: [MainComponent],
})
export class AppModule {
  constructor(iconLibrary: FaIconLibrary, datePickerConfig: NgbDatepickerConfig, autoLogoutService: AutoLogoutService) {
    registerLocaleData(locale);
    iconLibrary.addIcons(...fontAwesomeIcons);
    datePickerConfig.minDate = { year: dayjs().subtract(100, 'year').year(), month: 1, day: 1 };
    autoLogoutService.check();
  }
}
