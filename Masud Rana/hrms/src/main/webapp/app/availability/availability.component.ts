import { Component } from '@angular/core';

@Component({
  selector: 'hrms-availability',
  templateUrl: './availability.component.html',
  styleUrls: ['./availability.component.scss'],
})
export class AvailabilityComponent {}
