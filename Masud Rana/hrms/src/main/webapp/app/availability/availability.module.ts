import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';

import { AvailabilityComponent } from './availability.component';

@NgModule({
  declarations: [AvailabilityComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: AvailabilityComponent,
        canActivate: [AuthGuard],
      },
    ]),
  ],
})
export class AvailabilityModule {}
