export interface Country {
  id: number;
  name: string;
  code: string;
  iso_code: string;
  is_active: boolean;
  is_deleted: boolean;
  is_default: boolean;
}
