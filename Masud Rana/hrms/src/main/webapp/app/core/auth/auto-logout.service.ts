import { Injectable, NgZone } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from 'app/store';
import { selectApplicationSettings } from 'app/store/company-settings/company-settings.selectors';
import { logoutUserAction } from 'app/store/user/user.actions';

import { AuthServerProvider } from './auth-jwt.service';
import { StateStorageService } from './state-storage.service';

import { ApplicationSettings } from 'app/store/states/application-settings.interface';

@Injectable({
  providedIn: 'root',
})
export class AutoLogoutService {
  isLoggedIn = false;
  autoLogoutTime = 0;

  constructor(
    private readonly ngZone: NgZone,
    private readonly store: Store<AppState>,
    private readonly storageService: StateStorageService,
    private readonly authServerProvider: AuthServerProvider
  ) {
    this.store.select(selectApplicationSettings).subscribe((applicationSettings: ApplicationSettings) => {
      this.autoLogoutTime = applicationSettings?.auth_auto_logout_time;
    });
    this.storageService.setLastAction(Date.now());
    this.check();
    this.initListener();
    this.initInterval();
  }

  /**
   * start event listener
   */
  initListener(): void {
    this.ngZone.runOutsideAngular(() => {
      document.body.addEventListener('click', () => this.reset());
    });
  }

  /**
   * time interval
   */
  initInterval(): void {
    this.ngZone.runOutsideAngular(() => {
      setInterval(() => {
        if (this.autoLogoutTime > 0) {
          this.check();
        }
      }, 10000);
    });
  }

  /**
   * reset timer
   */
  reset(): void {
    this.storageService.setLastAction(Date.now());
  }

  /**
   * check timer
   */
  check(): void {
    const now = Date.now();
    const timeLeft = parseInt(this.storageService.getLastAction(), 10) + this.autoLogoutTime;
    const diff = timeLeft - now;
    const isTimeout = diff < 0;
    this.isLoggedIn = this.isUserLoggedIn();
    this.ngZone.run(() => {
      if (isTimeout && this.isLoggedIn && this.autoLogoutTime > 0) {
        this.storageService.clearStorage();
        this.store.dispatch(logoutUserAction());
      }
    });
  }

  /**
   *check if a user is logged in
   */
  isUserLoggedIn(): boolean {
    const token = this.authServerProvider.getToken();
    return token === '' ? false : true;
  }
}
