import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { SessionStorageService } from 'ngx-webstorage';
import { Observable, ReplaySubject, of } from 'rxjs';
import { shareReplay, tap, catchError, map } from 'rxjs/operators';

import { StateStorageService } from 'app/core/auth/state-storage.service';
import { ApplicationConfigService } from '../config/application-config.service';

import { Account } from 'app/core/auth/account.model';
import { IUserCompany } from './account.model';
import { IMenu } from 'app/entities/menu/menu.model';
import { ICompanySelectionPayload } from './../../account/company-selection/company-selection.model';
import { IUpdateRole } from 'app/entities/role/role.model';

type JwtToken = {
  token: string;
  refresh_token: string;
};

@Injectable({ providedIn: 'root' })
export class AccountService {
  private userIdentity: Account | null = null;
  private authenticationState = new ReplaySubject<Account | null>(1);
  private accountCache$?: Observable<Account | null>;

  constructor(
    private translateService: TranslateService,
    private sessionStorage: SessionStorageService,
    private storageService: StateStorageService,
    private http: HttpClient,
    private stateStorageService: StateStorageService,
    private router: Router,
    private applicationConfigService: ApplicationConfigService
  ) {}

  save(account: Account): Observable<{}> {
    return this.http.post(this.applicationConfigService.getEndpointFor('account'), account);
  }

  changeBusiness(payload: ICompanySelectionPayload): Observable<JwtToken> {
    return this.http
      .post<JwtToken>(this.applicationConfigService.getEndpointFor('account/change-business'), payload)
      .pipe(map(response => this.authenticateUpdateSuccess(response)));
  }

  updateRole(role: IUpdateRole): Observable<any> {
    return this.http.put(this.applicationConfigService.getEndpointFor('account/role'), role);
  }

  authenticate(identity: Account | null): void {
    this.userIdentity = identity;
    this.authenticationState.next(this.userIdentity);
  }

  hasAnyAuthority(roleIds: number[]): boolean {
    if (!this.userIdentity) {
      return false;
    }
    if (!Array.isArray(roleIds)) {
      roleIds = [roleIds];
    }
    return this.userIdentity.user_companies.some((user_company: IUserCompany) => roleIds.includes(user_company.role_id));
  }

  identity(force?: boolean): Observable<Account | null> {
    if (!this.accountCache$ || force || !this.isAuthenticated()) {
      this.accountCache$ = this.fetch().pipe(
        tap((account: Account | null) => {
          this.authenticate(account);

          // After retrieve the account info, the language will be changed to
          // the user's preferred language configured in the account setting
          if (account?.langKey) {
            const langKey = this.sessionStorage.retrieve('locale') ?? account.langKey;
            this.translateService.use(langKey);
          }

          if (account) {
            this.navigateToStoredUrl();
          }
        }),
        shareReplay(),
        catchError((error: any) => of(error))
      );
    }
    return this.accountCache$;
  }

  isAuthenticated(): boolean {
    return this.userIdentity !== null;
  }

  getAuthenticationState(): Observable<Account | null> {
    return this.authenticationState.asObservable();
  }

  getImageUrl(): string {
    return this.userIdentity?.imageUrl ?? '';
  }

  getRoleMenu(): Observable<IMenu[]> {
    return this.http
      .get<IMenu[]>(this.applicationConfigService.getEndpointFor(`/account/menu`))
      .pipe(map((res: IMenu[]) => this.menuPermission(res)))
      .pipe(map((res: IMenu[]) => this.stateStorageService.setMenu(res)));
  }

  private fetch(): Observable<Account> {
    return this.http.get<Account>(this.applicationConfigService.getEndpointFor('account'));
  }

  private navigateToStoredUrl(): void {
    // previousState can be set in the authExpiredInterceptor and in the userRouteAccessService
    // if login is successful, go to stored previousState and clear previousState
    const previousUrl = this.stateStorageService.getUrl();
    if (previousUrl) {
      this.stateStorageService.clearUrl();
      this.router.navigateByUrl(previousUrl);
    }
  }

  private authenticateUpdateSuccess(jwt: JwtToken): JwtToken {
    this.storageService.setAuthenticationToken(jwt);
    return jwt;
  }

  private menuPermission(menus: IMenu[]): IMenu[] {
    return menus.filter((menu: IMenu) => menu.is_menu === true);
  }
}
