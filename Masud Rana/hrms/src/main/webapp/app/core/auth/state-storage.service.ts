import { Injectable } from '@angular/core';

import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

import { IMenu } from 'app/entities/menu/menu.model';
import { IRegisterCompany } from 'app/register/register.model';

import { User, UserCompany } from 'app/store/states/user.interface';

export enum ELocalStorageKeys {
  PREV_URL = 'previousUrl',
  MENU = 'menu',
  SETTINGS = 'settings',
  USER_DATA = 'user',
  SELECTED_COMPANY = 'selectedCompany',
  AUTH_TOKEN = 'authenticationToken',
  REFRESH_TOKEN = 'refreshToken',
  LAST_ACTION_TIME = 'lastActionTime',
  LANGUAGE = 'lang',
  REGISTER_USER = 'registered_user_id',
  REGISTER_USER_FORM_DATA = 'registered_user_form_data',
}

type JwtToken = {
  token: string;
  refresh_token: string;
};

@Injectable({ providedIn: 'root' })
export class StateStorageService {
  constructor(private $localStorage: LocalStorageService, private sessionStorage: SessionStorageService) {}
  /** ........... REGISTER USER .......... */
  getRegisteredUser(): any {
    return this.$localStorage.retrieve(ELocalStorageKeys.REGISTER_USER);
  }

  setRegisteredUser(user_id: number): void {
    this.$localStorage.store(ELocalStorageKeys.REGISTER_USER, user_id);
  }

  removeRegisteredUser(): void {
    this.$localStorage.clear(ELocalStorageKeys.REGISTER_USER);
  }

  getRegisteredUserFormData(): any {
    return this.$localStorage.retrieve(ELocalStorageKeys.REGISTER_USER_FORM_DATA);
  }

  setRegisteredUserFormData(data: IRegisterCompany): void {
    this.$localStorage.store(ELocalStorageKeys.REGISTER_USER_FORM_DATA, data);
  }

  removeRegisteredUserFormData(): void {
    this.$localStorage.clear(ELocalStorageKeys.REGISTER_USER_FORM_DATA);
  }

  /** ........... SETTINGS .......... */
  getCompanySettings(): any {
    return this.$localStorage.retrieve(ELocalStorageKeys.SETTINGS);
  }
  setCompanySettings(data: any): void {
    this.$localStorage.store(ELocalStorageKeys.SETTINGS, data);
  }
  clearCompanySettings(): void {
    this.$localStorage.clear(ELocalStorageKeys.SETTINGS);
  }
  clearStorage(): void {
    this.$localStorage.clear();
    this.sessionStorage.clear();
  }

  /** ........... PREV_URL .......... */
  storeUrl(url: string): void {
    this.$localStorage.store(ELocalStorageKeys.PREV_URL, url);
  }
  getUrl(): string | null {
    return this.$localStorage.retrieve(ELocalStorageKeys.PREV_URL) as string | null;
  }
  clearUrl(): void {
    this.$localStorage.clear(ELocalStorageKeys.PREV_URL);
  }

  /** ........... USER_DATA .......... */
  storeUserData(userData: User): void {
    this.$localStorage.store(ELocalStorageKeys.USER_DATA, userData);
  }
  getUserData(): User | null {
    return this.$localStorage.retrieve(ELocalStorageKeys.USER_DATA) as User | null;
  }
  removeUserData(): void {
    this.$localStorage.clear(ELocalStorageKeys.USER_DATA);
  }

  setAuthenticationToken(jwt: JwtToken): void {
    this.$localStorage.store(ELocalStorageKeys.AUTH_TOKEN, jwt.token);
    this.$localStorage.store(ELocalStorageKeys.REFRESH_TOKEN, jwt.refresh_token);
    this.sessionStorage.store(ELocalStorageKeys.AUTH_TOKEN, jwt.token);
  }

  /** ........... MENU .......... */
  getMenu(): IMenu[] {
    const menu: IMenu[] = JSON.parse(this.$localStorage.retrieve(ELocalStorageKeys.MENU));
    return menu;
  }
  setMenu(menu: IMenu[]): IMenu[] {
    this.$localStorage.store(ELocalStorageKeys.MENU, JSON.stringify(menu)) as string;
    return menu;
  }

  /** ........... SELECTED_COMPANY .......... */
  setCurrentCompany(company: any): void {
    this.$localStorage.store(ELocalStorageKeys.SELECTED_COMPANY, company);
  }
  getCurrentCompany(): UserCompany {
    return this.$localStorage.retrieve(ELocalStorageKeys.SELECTED_COMPANY);
  }
  clearCurrentCompany(): void {
    this.$localStorage.clear(ELocalStorageKeys.SELECTED_COMPANY);
  }

  /**set last action
   * @param value
   */
  setLastAction(value: any): void {
    localStorage.setItem(ELocalStorageKeys.LAST_ACTION_TIME, JSON.stringify(value));
  }

  /** .... get last action .....*/
  getLastAction(): string {
    return localStorage.getItem(ELocalStorageKeys.LAST_ACTION_TIME) ?? '';
  }
}
