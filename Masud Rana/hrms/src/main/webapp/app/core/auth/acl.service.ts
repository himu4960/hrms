import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { IMenu } from 'app/entities/menu/menu.model';
import { IPermissionType } from 'app/entities/role/permission-types.model';

import { EPermission } from 'app/shared/enum/permission.enum';

import { StateStorageService } from './state-storage.service';

@Injectable({
  providedIn: 'root',
})
export class AclService {
  constructor(private storageService: StateStorageService, private router: Router) {}

  public isGranted(permission: string): boolean {
    try {
      const segments = this.router.url.split('?');
      const resource = segments[0];
      const acl = this.find(resource);
      const permission_type: IPermissionType | undefined = acl?.permission_type?.filter(
        (permissionType: IPermissionType) => permissionType.key === permission
      )[0];
      if (permission_type && permission_type.key === permission) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }

  // Only for list pages
  get canAdd(): boolean {
    return this.isGranted(EPermission.CAN_ADD);
  }

  // Only for list pages
  get canEdit(): boolean {
    return this.isGranted(EPermission.CAN_EDIT);
  }

  // Only for list pages
  get canView(): boolean {
    return this.isGranted(EPermission.CAN_VIEW);
  }

  // Only for list pages
  get canDelete(): boolean {
    return this.isGranted(EPermission.CAN_DELETE);
  }

  get canApprove(): boolean {
    return this.isGranted(EPermission.CAN_APPROVE);
  }

  get canPublish(): boolean {
    return this.isGranted(EPermission.CAN_PUBLISH);
  }

  get canReject(): boolean {
    return this.isGranted(EPermission.CAN_REJECT);
  }

  // Only for SysUser list pages
  get canChangePassword(): boolean {
    return this.isGranted(EPermission.CAN_CHANGE_PASSWORD);
  }

  private find(resource: string): IMenu | undefined {
    try {
      const acl = this.storageService.getMenu();
      return acl.filter((role: IMenu) => {
        return role.router_link === resource;
      })[0];
    } catch (__) {
      return;
    }
  }
}
