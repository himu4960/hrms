import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { PermissionService } from 'app/shared/service/permission.service';

@Injectable({ providedIn: 'root' })
export class SuperAdminRoutePermissionGuard implements CanActivate {
  isSuperAdmin = false;
  constructor(private router: Router, private permissionService: PermissionService) {
    this.isSuperAdmin = this.permissionService.isSuperAdmin();
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.isSuperAdmin) {
      this.router.navigate(['/404']);
      return false;
    }
    return true;
  }
}
