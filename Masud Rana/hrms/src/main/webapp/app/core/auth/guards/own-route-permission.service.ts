import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';

import { AppState } from './../../../store/index';

import { selectCurrentCompany } from 'app/store/user/user.selectors';

import { PermissionService } from 'app/shared/service/permission.service';

@Injectable({ providedIn: 'root' })
export class OwnRoutePermissionGuard implements CanActivate {
  constructor(private router: Router, private permissionService: PermissionService, private store: Store<AppState>) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const isEmployee = this.permissionService.isEmployee();
    const employee_id = route.params.id;
    if (isEmployee) {
      this.store.select(selectCurrentCompany).subscribe(currentCompany => {
        if (currentCompany?.employee.id === Number(employee_id)) {
          return true;
        } else {
          this.router.navigate(['/404']);
          return false;
        }
      });
    }

    return true;
  }
}
