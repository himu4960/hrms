import { ICompany } from 'app/company/company.model';

export interface IUserCompany {
  company_id?: number;
  role_id: number;
  company: ICompany;
  is_active: boolean;
  is_owner: boolean;
}

export class Account {
  constructor(
    public activated: boolean,
    public user_companies: IUserCompany[],
    public email: string,
    public firstName: string | null,
    public langKey: string,
    public lastName: string | null,
    public username: string,
    public imageUrl: string | null
  ) {}
}
