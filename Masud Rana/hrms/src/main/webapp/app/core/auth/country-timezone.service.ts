import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApplicationConfigService } from '../config/application-config.service';
import { Country } from './country-timezone.model';

@Injectable({ providedIn: 'root' })
export class CountryTimezoneService {
  constructor(private http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  getAllCountries(): Observable<Country[]> {
    return this.http.get<Country[]>(this.applicationConfigService.getEndpointFor('countries/all'));
  }
}
