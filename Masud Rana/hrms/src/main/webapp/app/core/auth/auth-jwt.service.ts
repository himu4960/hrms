import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

import { JwtHelperService } from '@auth0/angular-jwt';

import { ApplicationConfigService } from '../config/application-config.service';
import { Login } from 'app/login/login.model';
import { ELocalStorageKeys, StateStorageService } from './state-storage.service';

type JwtToken = {
  token: string;
  refresh_token: string;
};

@Injectable({ providedIn: 'root' })
export class AuthServerProvider {
  constructor(
    private http: HttpClient,
    private $localStorage: LocalStorageService,
    private $sessionStorage: SessionStorageService,
    private stateStorageService: StateStorageService,
    private applicationConfigService: ApplicationConfigService
  ) {}

  getToken(): string {
    const tokenInLocalStorage: string | null = this.$localStorage.retrieve(ELocalStorageKeys.AUTH_TOKEN);
    const tokenInSessionStorage: string | null = this.$sessionStorage.retrieve('authenticationToken');
    return tokenInLocalStorage ?? tokenInSessionStorage ?? '';
  }

  getAuthenticationToken(): string {
    return this.$localStorage.retrieve(ELocalStorageKeys.AUTH_TOKEN);
  }

  public decodeJwtToken(token: string): any {
    const helper = new JwtHelperService();
    return helper.decodeToken(token);
  }

  login(credentials: Login): Observable<void> {
    return this.http.post<JwtToken>(this.applicationConfigService.getEndpointFor('login'), credentials).pipe(
      map(response => {
        this.authenticateSuccess(response, credentials.rememberMe);
      })
    );
  }

  logout(): Observable<void> {
    return new Observable(observer => {
      this.stateStorageService.clearStorage();
      observer.complete();
    });
  }

  refreshAccessToken(refresh_token: string): Observable<any> {
    return this.http.post(this.applicationConfigService.getEndpointFor('account/refresh-token'), { refresh_token });
  }

  private authenticateSuccess(response: JwtToken, rememberMe: boolean): void {
    const jwt = response.token;
    if (rememberMe) {
      this.$localStorage.store(ELocalStorageKeys.AUTH_TOKEN, jwt);
      this.$sessionStorage.clear(ELocalStorageKeys.AUTH_TOKEN);
    } else {
      this.$sessionStorage.store(ELocalStorageKeys.AUTH_TOKEN, jwt);
      this.$localStorage.clear(ELocalStorageKeys.AUTH_TOKEN);
    }
  }
}
