import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { PermissionService } from 'app/shared/service/permission.service';

@Injectable({ providedIn: 'root' })
export class RoutePermissionGuard implements CanActivate {
  isEmployee = true;
  constructor(private router: Router, private permissionService: PermissionService) {
    this.isEmployee = this.permissionService.isEmployee();
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.isEmployee) {
      this.router.navigate(['/404']);
      return false;
    }
    return true;
  }
}
