import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';

import { AppState } from 'app/store';

import { selectState } from '../../../store/user/user.selectors';

@Injectable({ providedIn: 'root' })
export class NotAuthGuard implements CanActivate {
  isActive = true;

  private subscriptions: Subscription = new Subscription();

  constructor(private router: Router, private store: Store<AppState>) {}

  canActivate(): boolean {
    this.subscriptions.add(
      this.store.select(selectState).subscribe(state => {
        if (!state.selectedCompany) {
          this.isActive = true;
        } else {
          const token = state.jwtToken;
          if (token) {
            this.router.navigate(['/home']);
            this.isActive = false;
          }
        }
      })
    );
    return this.isActive;
  }
}
