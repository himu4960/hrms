import { Injectable } from '@angular/core';
import { environment } from 'app/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ApplicationConfigService {
  getEndpointFor(api: string, microservice?: string): string {
    if (microservice) {
      return `${environment.BASE_API_END_POINT}services/${microservice}/${api}`;
    }
    return `${environment.BASE_API_END_POINT}${api}`;
  }
}
