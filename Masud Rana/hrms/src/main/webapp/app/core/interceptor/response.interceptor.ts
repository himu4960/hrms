import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';

import { ToastService } from './../../shared/components/toast/toast.service';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
  constructor(private toastService: ToastService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      map(event => {
        if (event instanceof HttpResponse) {
          if (request.method !== 'GET' && request.method !== 'POST' && event.body?.message) {
            this.toastService.showSuccessToast(event.body.message);
          }
          event = event.clone({ body: this.modifyBody(event.body) });
        }
        return event;
      })
    );
  }

  private modifyBody(body: any): any {
    return body?.data;
  }
}
