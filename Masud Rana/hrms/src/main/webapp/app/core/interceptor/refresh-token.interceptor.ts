import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable, throwError } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

import { Store } from '@ngrx/store';
import { LocalStorageService } from 'ngx-webstorage';
import { AppState } from 'app/store';
import { JwtHelperService } from '@auth0/angular-jwt';

import { AuthServerProvider } from '../auth/auth-jwt.service';
import { ELocalStorageKeys, StateStorageService } from '../auth/state-storage.service';
import { EventManager, EventWithContent } from '../util/event-manager.service';
import { ToastService } from 'app/shared/components/toast/toast.service';

import { logoutUserAction } from 'app/store/user/user.actions';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {
  private jwtHelper = new JwtHelperService();

  private refreshTokenInProgress = false;

  constructor(
    private readonly store: Store<AppState>,
    private router: Router,
    private stateStorageService: StateStorageService,
    private localStorage: LocalStorageService,
    private eventManager: EventManager,
    private toastService: ToastService,
    private authServerProvider: AuthServerProvider
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError(error => {
        if (error instanceof HttpErrorResponse && error.status === 401) {
          return this.handle401Error(request, next);
        }
        if (!(error.status === 401 && (error.message === '' || error.url?.includes('/account')))) {
          if (error.error.message) {
            this.toastService.showDangerToast(error.error.message);
          }
          this.eventManager.broadcast(new EventWithContent('hrmsApp.httpError', error));
        }
        return next.handle(request);
      })
    );
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.refreshTokenInProgress) {
      this.refreshTokenInProgress = true;
      const refreshToken = this.localStorage.retrieve(ELocalStorageKeys.REFRESH_TOKEN);
      const refreshTokenExpired = this.jwtHelper.isTokenExpired(refreshToken);

      if (refreshTokenExpired) {
        this.logoutUser();
        this.eventManager.broadcast(
          new EventWithContent('hrmsApp.httpError', {
            status: false,
            message: 'Session expired',
          })
        );
      } else {
        const accessToken = this.localStorage.retrieve(ELocalStorageKeys.AUTH_TOKEN);
        const accessTokenExpired = this.jwtHelper.isTokenExpired(accessToken);
        if (accessTokenExpired) {
          return this.authServerProvider.refreshAccessToken(refreshToken).pipe(
            switchMap((response: any) => {
              this.refreshTokenInProgress = false;
              this.stateStorageService.setAuthenticationToken(response);
              return next.handle(this.addTokenHeader(request, response.token));
            }),
            catchError((err: any) => {
              this.refreshTokenInProgress = false;
              this.logoutUser();
              return throwError(err);
            })
          );
        }
      }
    }
    return next.handle(request);
  }

  private addTokenHeader(request: HttpRequest<any>, token: string): HttpRequest<any> {
    if (!token) {
      return request;
    }

    return request.clone({ headers: request.headers.set('Authorization', `Bearer ${token}`) });
  }

  private logoutUser(): void {
    this.stateStorageService.clearStorage();
    this.store.dispatch(logoutUserAction());
    this.router.navigate(['/login']);
  }
}
