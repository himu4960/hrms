import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { AppState } from 'app/store';
import { Store } from '@ngrx/store';

import { selectApplicationSettings } from 'app/store/company-settings/company-settings.selectors';
import { ApplicationConfigService } from '../config/application-config.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private store: Store<AppState>,
    private localStorage: LocalStorageService,
    private sessionStorage: SessionStorageService,
    private applicationConfigService: ApplicationConfigService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const serverApiUrl = this.applicationConfigService.getEndpointFor('');
    if (!request.url || (request.url.startsWith('http') && !(serverApiUrl && request.url.startsWith(serverApiUrl)))) {
      return next.handle(request);
    }

    const token: string | null = this.localStorage.retrieve('authenticationToken') ?? this.sessionStorage.retrieve('authenticationToken');
    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`,
        },
      });
    }

    this.store.select(selectApplicationSettings).subscribe(applicationSetting => {
      const timeZoneId = applicationSetting?.locale_timezone_id;
      if (timeZoneId) {
        request = request.clone({
          setHeaders: {
            timeZoneId: `${timeZoneId}`,
          },
        });
      }
    });
    return next.handle(request);
  }
}
