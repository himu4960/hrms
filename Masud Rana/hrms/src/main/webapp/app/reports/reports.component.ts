import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';

import { PermissionService } from 'app/shared/service/permission.service';

@Component({
  selector: 'hrms-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ReportsComponent implements OnInit {
  isEmployee = true;

  constructor(private permissionService: PermissionService) {}

  ngOnInit(): void {
    this.isEmployee = this.permissionService?.isEmployee();
  }
}
