import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ReportService {
  parseTimeSheetsIntoJsonArray(unparsedTimeSheets: any[], dateColumns: string[]): any {
    const timeSheets: any[] = unparsedTimeSheets;
    let totalColckInDuration = 0;
    const columnSums: any = {};

    unparsedTimeSheets.forEach((row: any, index: number) => {
      let i = 0;
      if (row?.total_clock_in) {
        totalColckInDuration += Number(row.total_clock_in);
      }
      Object.keys(row).forEach((column: string) => {
        if (dateColumns[i] === column) {
          const parsedTimeSheet = row?.[column];
          let totalDuration = 0;
          parsedTimeSheet.forEach((item: any) => {
            if (item?.diff) {
              totalDuration += Number(item.diff);
            }
          });
          if (!columnSums[column]) {
            columnSums[column] = 0;
          }
          columnSums[column] += totalDuration;
          timeSheets[index][column] = { data: parsedTimeSheet, total_duration: totalDuration };
          i++;
        }
      });
    });
    return { parsedTimeSheets: timeSheets, totalColckInDuration, columnSums };
  }

  parseScheduleSummaryIntoJsonArray(unParseScheduleSummary: any[], dateColumns: string[]): any {
    const scheduleSummary: any[] = unParseScheduleSummary;
    let totalWorkShiftDuration = 0;
    const columnSums: any = {};

    unParseScheduleSummary.forEach((row: any, index: number) => {
      let i = 0;
      if (row?.total_work_shift_duration) {
        totalWorkShiftDuration += Number(row.total_work_shift_duration);
      }
      Object.keys(row).forEach((column: string) => {
        if (dateColumns[i] === column) {
          const parsedScheduleSummary = row?.[column];
          let totalDuration = 0;
          parsedScheduleSummary.forEach((item: any) => {
            if (item?.diff) {
              totalDuration += Number(item.diff);
            }
          });
          if (!columnSums[column]) {
            columnSums[column] = 0;
          }
          columnSums[column] += totalDuration;
          scheduleSummary[index][column] = { data: parsedScheduleSummary, total_duration: totalDuration };
          i++;
        }
      });
    });
    return { parsedScheduleSummary: scheduleSummary, totalWorkShiftDuration, columnSums };
  }
}
