import { Component } from '@angular/core';

import { UtilService } from 'app/shared/service/util.service';
import { ReportService } from 'app/reports/report.service';
import { ScheduleReportService } from '../schedule-report.service';

import { IFilterDateRange, IReportFilterParam } from 'app/shared/forms/generic-filter-form/models/generic-filter.model';
import { ETimeType } from 'app/shared/pipes/time/time-type.enum';
import { EExportType } from 'app/reports/time-sheet/models/time-sheet-report.model';

@Component({
  selector: 'hrms-schedule-summary',
  templateUrl: './schedule-summary.component.html',
  styleUrls: ['./schedule-summary.component.scss'],
})
export class ScheduleSummaryComponent {
  EExportType = EExportType;
  filterParam?: IReportFilterParam;
  get eTimeType(): any {
    return ETimeType;
  }

  isLoading = false;

  scheduleSummary: any[] = [];
  totalWorkShiftDuration = 0;
  dateColumns: string[] = [];
  columnSums = {};

  constructor(
    private readonly reportService: ReportService,
    private readonly scheduleReportService: ScheduleReportService,
    private readonly utilService: UtilService
  ) {}

  filter(filterParam: IReportFilterParam): void {
    this.isLoading = true;
    this.filterParam = filterParam;
    const dateRange: IFilterDateRange = filterParam;
    this.dateColumns = this.utilService.generateDateColumnsByRange(dateRange);

    this.scheduleReportService.getScheduleSummary(filterParam).subscribe(
      (data: any) => {
        const unParseScheduleSummary = data.body;
        if (unParseScheduleSummary) {
          const parsedScheduleSummary = this.reportService.parseScheduleSummaryIntoJsonArray(unParseScheduleSummary, this.dateColumns);
          this.scheduleSummary = parsedScheduleSummary.parsedScheduleSummary;
          this.totalWorkShiftDuration = parsedScheduleSummary.totalWorkShiftDuration;
          this.columnSums = parsedScheduleSummary.columnSums;
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  export(exportType: string): void {
    if (this.filterParam !== undefined) {
      this.isLoading = true;

      this.scheduleReportService.printScheduleSummary(this.filterParam, exportType).subscribe((data: any) => {
        const url = window.URL.createObjectURL(data.body);
        window.open(url);
      });

      this.isLoading = false;
    }
  }

  resetScheduleSummary(): void {
    this.dateColumns = [];
    this.scheduleSummary = [];
  }
}
