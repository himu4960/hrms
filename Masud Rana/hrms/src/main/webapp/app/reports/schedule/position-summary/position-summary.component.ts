import { Component } from '@angular/core';

import { EExportType } from './../../time-sheet/models/time-sheet-report.model';
import { IFilterDateRange, IReportFilterParam } from 'app/shared/forms/generic-filter-form/models/generic-filter.model';

import { ScheduleReportService } from '../schedule-report.service';
import { UtilService } from 'app/shared/service/util.service';

import { ETimeType } from 'app/shared/pipes/time/time-type.enum';

export interface IPositionSummary {
  branch_id?: number | null;
  branch_name?: string | null;
  position_summary?: any[] | null;
}

@Component({
  selector: 'hrms-position-summary',
  templateUrl: './position-summary.component.html',
  styleUrls: ['./position-summary.component.scss'],
})
export class PositionSummaryComponent {
  EExportType = EExportType;
  filterParam?: IReportFilterParam;
  get eTimeType(): any {
    return ETimeType;
  }

  isLoading = false;

  positionSummary: IPositionSummary[] = [];
  totalWorkShiftDuration = 0;
  dateColumns: string[] = [];
  columnSums = {};

  constructor(private readonly scheduleReportService: ScheduleReportService, private readonly utilService: UtilService) {}

  filter(filterParam: IReportFilterParam): void {
    this.isLoading = true;
    this.filterParam = filterParam;
    this.dateColumns = [];
    this.positionSummary = [];
    const dateRange: IFilterDateRange = filterParam;
    this.dateColumns = this.utilService.generateDateColumnsByRange(dateRange);

    this.scheduleReportService.getPositionSummary(filterParam).subscribe(
      (data: any) => {
        let position_summary: any[] = [];
        const unformattedPositionSummary = data.body;
        const columnSums: any = {};

        this.totalWorkShiftDuration = 0;

        if (unformattedPositionSummary) {
          for (let i = 0; i < unformattedPositionSummary.length; i++) {
            const nextIndex = i + 1;
            const currentRowBranchId = unformattedPositionSummary[i].branch_id;
            const nextRowBranchId = unformattedPositionSummary[nextIndex]?.branch_id;
            const branchName = unformattedPositionSummary[i].branch_name;

            position_summary.push(unformattedPositionSummary[i]);
            if (currentRowBranchId !== nextRowBranchId) {
              this.positionSummary.push({ branch_id: currentRowBranchId, branch_name: branchName, position_summary: position_summary });
              position_summary = [];
            }

            this.totalWorkShiftDuration += Number(unformattedPositionSummary[i].total_work_shift);
            let dateColumnsIndex = 0;
            Object.keys(unformattedPositionSummary[i]).forEach((column: string) => {
              if (this.dateColumns[dateColumnsIndex] === column) {
                if (!columnSums[column]) {
                  columnSums[column] = 0;
                }
                columnSums[column] += Number(unformattedPositionSummary[i][column]);
                dateColumnsIndex++;
              }
            });
          }
        }

        this.columnSums = columnSums;
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  export(exportType: string): void {
    if (this.filterParam !== undefined) {
      this.isLoading = true;

      this.scheduleReportService.printPositionSummary(this.filterParam, exportType).subscribe((data: any) => {
        const url = window.URL.createObjectURL(data.body);
        window.open(url);
      });

      this.isLoading = false;
    }
  }

  resetPositionSummary(): void {
    this.dateColumns = [];
    this.positionSummary = [];
  }
}
