import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { ApplicationConfigService } from 'app/core/config/application-config.service';

import { IReportFilterParam } from 'app/shared/forms/generic-filter-form/models/generic-filter.model';

@Injectable({
  providedIn: 'root',
})
export class ScheduleReportService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('reports');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) { }

  getScheduleSummary(filterParam: IReportFilterParam): Observable<any> {
    return this.http.post(`${this.resourceUrl}/schedule-summary`, filterParam, { observe: 'response' });
  }

  printScheduleSummary(filterParam: IReportFilterParam, type: string): Observable<any> {
    return this.http.post(`${this.resourceUrl}/schedule-summary/export?type=${type}`, filterParam, {
      observe: 'response',
      responseType: 'blob',
    });
  }

  getPositionSummary(filterParam: IReportFilterParam): Observable<any> {
    return this.http.post(`${this.resourceUrl}/position-summary`, filterParam, { observe: 'response' });
  }

  printPositionSummary(filterParam: IReportFilterParam, type: string): Observable<any> {
    return this.http.post(`${this.resourceUrl}/position-summary/export?type=${type}`, filterParam, {
      observe: 'response',
      responseType: 'blob',
    });
  }

  getShiftsScheduled(filterParam: IReportFilterParam): any {
    return this.http.post(`${this.resourceUrl}/shifts-scheduled`, filterParam, { observe: 'response' });
  }

  getHoursScheduled(filterParam: IReportFilterParam): Observable<any> {
    return this.http.post(`${this.resourceUrl}/hours-scheduled`, filterParam, { observe: 'response' });
  }

  printHoursScheduled(filterParam: IReportFilterParam, type: string): Observable<any> {
    return this.http.post(`${this.resourceUrl}/hours-scheduled/export?type=${type}`, filterParam, {
      observe: 'response',
      responseType: 'blob',
    });
  }

  printShiftsScheduled(filterParam: IReportFilterParam, type: string): Observable<any> {
    return this.http.post(`${this.resourceUrl}/shifts-scheduled/export?type=${type}`, filterParam, {
      observe: 'response',
      responseType: 'blob',
    });
  }
}
