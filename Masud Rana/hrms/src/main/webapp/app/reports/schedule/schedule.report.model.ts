export interface IHoursScheduled {
  employee_id: number;
  employee_name: string;
  work_shift_hours: number;
}
