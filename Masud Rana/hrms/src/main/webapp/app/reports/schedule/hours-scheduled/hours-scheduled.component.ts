import { Component } from '@angular/core';
import { tap } from 'rxjs/operators';

import { ScheduleReportService } from '../schedule-report.service';

import { IReportFilterParam } from 'app/shared/forms/generic-filter-form/models/generic-filter.model';
import { IHoursScheduled } from '../schedule.report.model';
import { ETimeType } from 'app/shared/pipes/time/time-type.enum';
import { EExportType } from 'app/reports/time-sheet/models/time-sheet-report.model';

@Component({
  selector: 'hrms-hours-scheduled',
  templateUrl: './hours-scheduled.component.html',
  styleUrls: ['./hours-scheduled.component.scss'],
})
export class HoursScheduledComponent {
  EExportType = EExportType;
  get eTimeType(): any {
    return ETimeType;
  }

  isLoading = false;
  filterParam?: IReportFilterParam;
  hoursScheduled: IHoursScheduled[] = [];
  totalHoursScheduled = 0;

  constructor(private readonly scheduleReportService: ScheduleReportService) {}

  filter(filterParam: IReportFilterParam): void {
    this.isLoading = true;
    this.filterParam = filterParam;
    this.hoursScheduled = [];

    this.scheduleReportService
      .getHoursScheduled(filterParam)
      .pipe(
        tap((data: any) => {
          this.hoursScheduled = data.body;
          const hoursScheduledArray: IHoursScheduled[] = data.body;
          this.totalHoursScheduled = 0;
          hoursScheduledArray.map((hoursScheduled: IHoursScheduled) => {
            this.totalHoursScheduled += Number(hoursScheduled.work_shift_hours);
          });
        })
      )
      .subscribe(() => {
        this.isLoading = false;
      });
  }

  export(exportType: string): void {
    if (this.filterParam !== undefined) {
      this.isLoading = true;

      this.scheduleReportService.printHoursScheduled(this.filterParam, exportType).subscribe((data: any) => {
        const url = window.URL.createObjectURL(data.body);
        window.open(url);
      });

      this.isLoading = false;
    }
  }

  resetHoursScheduled(): void {
    this.hoursScheduled = [];
  }
}
