import { Component } from '@angular/core';

import { ReportService } from 'app/reports/report.service';
import { UtilService } from 'app/shared/service/util.service';
import { ScheduleReportService } from '../schedule-report.service';

import { IFilterDateRange, IReportFilterParam } from 'app/shared/forms/generic-filter-form/models/generic-filter.model';
import { ETimeType } from 'app/shared/pipes/time/time-type.enum';
import { EExportType } from 'app/reports/time-sheet/models/time-sheet-report.model';

@Component({
  selector: 'hrms-shifts-scheduled',
  templateUrl: './shifts-scheduled.component.html',
  styleUrls: ['./shifts-scheduled.component.scss'],
})
export class ShiftsScheduledComponent {
  EExportType = EExportType;
  filterParam?: IReportFilterParam;
  get eTimeType(): any {
    return ETimeType;
  }

  isLoading = false;

  shiftsScheduled: any[] = [];
  totalWorkShiftDuration = 0;
  dateColumns: string[] = [];

  constructor(
    private readonly reportService: ReportService,
    private readonly scheduleReportService: ScheduleReportService,
    private readonly utilService: UtilService
  ) {}

  filter(filterParam: IReportFilterParam): void {
    this.isLoading = true;
    this.filterParam = filterParam;
    const dateRange: IFilterDateRange = filterParam;
    this.dateColumns = this.utilService.generateDateColumnsByRange(dateRange);

    this.scheduleReportService.getShiftsScheduled(filterParam).subscribe(
      (data: any) => {
        const unParseShiftsScheduled = data.body;
        if (unParseShiftsScheduled) {
          const parseShiftsScheduled = this.reportService.parseScheduleSummaryIntoJsonArray(unParseShiftsScheduled, this.dateColumns);
          this.shiftsScheduled = parseShiftsScheduled.parsedScheduleSummary;
          this.totalWorkShiftDuration = parseShiftsScheduled.totalWorkShiftDuration;
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  export(exportType: string): void {
    if (this.filterParam !== undefined) {
      this.isLoading = true;

      this.scheduleReportService.printShiftsScheduled(this.filterParam, exportType).subscribe((data: any) => {
        const url = window.URL.createObjectURL(data.body);
        window.open(url);
      });

      this.isLoading = false;
    }
  }

  resetShiftsScheduled(): void {
    this.dateColumns = [];
    this.shiftsScheduled = [];
  }
}
