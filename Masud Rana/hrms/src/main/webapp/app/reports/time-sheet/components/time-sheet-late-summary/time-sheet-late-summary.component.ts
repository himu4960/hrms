import { Component } from '@angular/core';

import { IReportFilterParam } from 'app/shared/forms/generic-filter-form/models/generic-filter.model';

import { TimeSheetReportService } from '../../time-sheet-report.service';

import { EExportType, ITimeSheetLateSummary } from '../../models/time-sheet-report.model';

import { ETimeType } from 'app/shared/pipes/time/time-type.enum';

@Component({
  selector: 'hrms-time-sheet-late-summary',
  templateUrl: './time-sheet-late-summary.component.html',
  styleUrls: ['./time-sheet-late-summary.component.scss'],
})
export class TimeSheetLateSummaryComponent {
  EExportType = EExportType;

  get eTimeType(): any {
    return ETimeType;
  }

  filterParam: IReportFilterParam | undefined;

  isLoading = false;
  timeSheetlateSummary: ITimeSheetLateSummary[] = [];

  constructor(private readonly timeSheetReportService: TimeSheetReportService) {}

  filter(filterParam: IReportFilterParam): void {
    this.isLoading = true;
    this.filterParam = filterParam;

    this.timeSheetReportService.getTimeSheetLateSummary(filterParam).subscribe(
      (data: any) => {
        this.timeSheetlateSummary = data.body;
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  export(exportType: string): void {
    if (this.filterParam !== undefined) {
      this.isLoading = true;

      this.timeSheetReportService.printTimeSheetLateSummary(this.filterParam, exportType).subscribe((data: any) => {
        const url = window.URL.createObjectURL(data.body);
        window.open(url);
      });

      this.isLoading = false;
    }
  }

  resetTimeSheetLateSummary(): void {
    this.timeSheetlateSummary = [];
  }
}
