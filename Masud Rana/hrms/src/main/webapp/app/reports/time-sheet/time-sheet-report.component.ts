import { Component } from '@angular/core';

import { ReportService } from '../report.service';
import { TimeSheetReportService } from './time-sheet-report.service';
import { UtilService } from 'app/shared/service/util.service';

import { IFilterDateRange, IReportFilterParam } from 'app/shared/forms/generic-filter-form/models/generic-filter.model';

import { ETimeType } from 'app/shared/pipes/time/time-type.enum';
import { EExportType } from './models/time-sheet-report.model';

@Component({
  selector: 'hrms-time-sheet-report',
  templateUrl: './time-sheet-report.component.html',
  styleUrls: ['./time-sheet-report.component.scss'],
})
export class TimeSheetReportComponent {
  EExportType = EExportType;
  get eTimeType(): any {
    return ETimeType;
  }

  isLoading = false;
  filterParam: IReportFilterParam | undefined;

  timeSheets: any[] = [];

  dateColumns: string[] = [];

  constructor(
    private readonly reportService: ReportService,
    private readonly timeSheetReportService: TimeSheetReportService,
    private readonly utilService: UtilService
  ) {}

  filter(filterParam: IReportFilterParam): void {
    this.isLoading = true;
    this.filterParam = filterParam;
    const dateRange: IFilterDateRange = filterParam;
    this.dateColumns = this.utilService.generateDateColumnsByRange(dateRange);

    this.timeSheetReportService.getTimeSheet(filterParam).subscribe(
      (data: any) => {
        const unParsetimeSheets = data.body;
        if (unParsetimeSheets) {
          const parsedTimeSheetData = this.reportService.parseTimeSheetsIntoJsonArray(unParsetimeSheets, this.dateColumns);
          this.timeSheets = parsedTimeSheetData.parsedTimeSheets;
        }
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  export(exportType: string): void {
    if (this.filterParam !== undefined) {
      this.isLoading = true;

      this.timeSheetReportService.printTimeSheet(this.filterParam, exportType).subscribe((data: any) => {
        const url = window.URL.createObjectURL(data.body);
        window.open(url);
      });

      this.isLoading = false;
    }
  }

  resetTimeSheets(): void {
    this.dateColumns = [];
    this.timeSheets = [];
  }
}
