import { Component } from '@angular/core';

import { TimeSheetReportService } from '../../time-sheet-report.service';

import { IReportFilterParam } from 'app/shared/forms/generic-filter-form/models/generic-filter.model';
import { ITimeSheetAttendance } from '../../models/time-sheet-report.model';
import { EExportType } from './../../models/time-sheet-report.model';

import { ETimeType } from 'app/shared/pipes/time/time-type.enum';

@Component({
  selector: 'hrms-time-sheet-attendance',
  templateUrl: './time-sheet-attendance.component.html',
  styleUrls: ['./time-sheet-attendance.component.scss'],
})
export class TimeSheetAttendanceComponent {
  EExportType = EExportType;

  get eTimeType(): any {
    return ETimeType;
  }

  filterParam: IReportFilterParam | undefined;

  isLoading = false;
  timeSheetAttendance: ITimeSheetAttendance[] = [];

  constructor(private readonly timeSheetReportService: TimeSheetReportService) {}

  filter(filterParam: IReportFilterParam): void {
    this.isLoading = true;
    this.filterParam = filterParam;
    this.timeSheetReportService.getTimeSheetAttendance(filterParam).subscribe(
      (data: any) => {
        this.timeSheetAttendance = data.body;
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  export(exportType: string): void {
    if (this.filterParam !== undefined) {
      this.isLoading = true;
      this.timeSheetReportService.printTimeSheetAttendance(this.filterParam, exportType).subscribe((data: any) => {
        const url = window.URL.createObjectURL(data.body);
        window.open(url);
      });

      this.isLoading = false;
    }
  }

  resetTimeAttendance(): void {
    this.timeSheetAttendance = [];
  }
}
