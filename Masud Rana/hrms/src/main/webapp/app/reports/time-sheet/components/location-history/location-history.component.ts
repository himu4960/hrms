import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from 'app/store';

import { ITimeZone } from 'app/entities/time-zone/time-zone.model';
import { EExportType, ITimeSheetLocationHistory } from '../../models/time-sheet-report.model';
import { IReportFilterParam } from 'app/shared/forms/generic-filter-form/models/generic-filter.model';
import { ETimeType } from 'app/shared/pipes/time/time-type.enum';

import { selectApplicationSettings } from 'app/store/company-settings/company-settings.selectors';

import { TimeSheetReportService } from '../../time-sheet-report.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'hrms-location-history',
  templateUrl: './location-history.component.html',
  styleUrls: ['./location-history.component.scss'],
})
export class LocationHistoryComponent implements OnDestroy {
  EExportType = EExportType;

  get eTimeType(): any {
    return ETimeType;
  }

  isLoading = false;
  timeZone!: ITimeZone;
  filterParam: IReportFilterParam | undefined;
  timeSheetLocationHistory: ITimeSheetLocationHistory[] = [];

  private subscriptions: Subscription = new Subscription();

  constructor(private store: Store<AppState>, private readonly timeSheetReportService: TimeSheetReportService) {}

  ngOnInit(): void {
    this.subscriptions.add(
      this.store.select(selectApplicationSettings).subscribe(applicationSettings => {
        this.timeZone = applicationSettings.time_zone;
      })
    );
  }

  filter(filterParam: IReportFilterParam): void {
    this.isLoading = true;
    this.filterParam = filterParam;
    this.timeSheetReportService.getTimeSheetLocationHistory(filterParam).subscribe(
      (res: any) => {
        this.timeSheetLocationHistory = res.body ?? [];
        if (res.body.length) {
          for (let i = 0; i < res.body.length; i++) {
            if (res.body[i]?.clockin_location) {
              const clockInLocation = JSON.parse(res.body[i].clockin_location);
              if (clockInLocation?.address?.road && clockInLocation?.address?.neighbourhood) {
                this.timeSheetLocationHistory[
                  i
                ].clockin_location = `${clockInLocation?.address?.road}, ${clockInLocation?.address?.neighbourhood}`;
              } else {
                const address: string = clockInLocation?.display_name ? clockInLocation?.display_name : 'link';
                const splitedAddress = address.split(',', 2);
                this.timeSheetLocationHistory[i].clockin_location = splitedAddress[0];
              }
            }

            if (res.body[i]?.clockout_location) {
              const clockOutLocation = JSON.parse(res.body[i].clockout_location);
              if (clockOutLocation?.address?.road && clockOutLocation?.address?.neighbourhood) {
                this.timeSheetLocationHistory[
                  i
                ].clockout_location = `${clockOutLocation?.address?.road}, ${clockOutLocation?.address?.neighbourhood}`;
              } else {
                const address: string = clockOutLocation?.display_name ? clockOutLocation?.display_name : 'link';
                const splitedAddress = address.split(',', 2);
                this.timeSheetLocationHistory[i].clockin_location = splitedAddress[0];
              }
            }
          }
        }

        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  export(exportType: string): void {
    if (this.filterParam !== undefined) {
      this.isLoading = true;
      this.timeSheetReportService.printLocationHistory(this.filterParam, exportType).subscribe((data: any) => {
        const url = window.URL.createObjectURL(data.body);
        window.open(url);
      });

      this.isLoading = false;
    }
  }

  resetLocationHistory(): void {
    this.timeSheetLocationHistory = [];
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
