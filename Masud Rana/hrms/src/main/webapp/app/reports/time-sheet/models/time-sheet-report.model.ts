export interface ITimeSheetLateSummary {
  employee_id: number;
  employee_name: string;
  designation_name?: string | null;
  date: Date;
  shift_start: Date;
  actual_start?: Date | null;
  shift_end: Date;
  actual_end?: Date | null;
  late_status: string;
  late_by?: number | null;
}

export interface ITimeSheetAttendance {
  employee_id?: number;
  employee_name?: string | null;
  shifts: number;
  hours_scheduled: number;
  hours_clocked: number;
  late_count: number;
  absent_count: number;
  clock_vs_shift: number;
}

export interface ITimeSheetLocationHistory {
  date: string;
  employee_name: string;
  clock_start_time: string;
  clock_end_time: string;
  total_clock_duration: number;
  total_break_duration: number;
  branch_name: string;
  designation_name: string;
  time_clock_id: number;
  clockin_latitude: number;
  clockin_longitude: number;
  clockout_latitude: number;
  clockout_longitude: number;
  clockin_location: string;
  clockout_location: string;
}

export enum EExportType {
  PDF = 'pdf',
  XLSX = 'xlsx',
  CSV = 'csv',
}
