import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApplicationConfigService } from 'app/core/config/application-config.service';

import { IReportFilterParam } from 'app/shared/forms/generic-filter-form/models/generic-filter.model';
import { ITimeSheetAttendance, ITimeSheetLateSummary } from './models/time-sheet-report.model';

export type TimeSheetLateSummaryResponseType = HttpResponse<ITimeSheetLateSummary[]>;
export type TimeSheetAttendanceResponseType = HttpResponse<ITimeSheetAttendance[]>;

@Injectable({
  providedIn: 'root',
})
export class TimeSheetReportService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('reports');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  getTimeSheet(filterParam: IReportFilterParam): any {
    return this.http.post(`${this.resourceUrl}/time-sheets`, filterParam, { observe: 'response' });
  }

  getTimeSheetSummary(filterParam: IReportFilterParam): any {
    return this.http.post(`${this.resourceUrl}/time-sheets/summary`, filterParam, { observe: 'response' });
  }

  getTimeSheetLateSummary(filterParam: IReportFilterParam): Observable<TimeSheetLateSummaryResponseType> {
    return this.http.post<ITimeSheetLateSummary[]>(`${this.resourceUrl}/time-sheet/late-summary`, filterParam, { observe: 'response' });
  }

  getTimeSheetAttendance(filterParam: IReportFilterParam): Observable<TimeSheetAttendanceResponseType> {
    return this.http.post<ITimeSheetAttendance[]>(`${this.resourceUrl}/time-sheet/attendance`, filterParam, { observe: 'response' });
  }

  getTimeSheetLocationHistory(filterParam: IReportFilterParam): Observable<TimeSheetAttendanceResponseType> {
    return this.http.post<ITimeSheetAttendance[]>(`${this.resourceUrl}/location-history`, filterParam, { observe: 'response' });
  }

  printTimeSheetAttendance(filterParam: any, type: string): any {
    return this.http.post(`${this.resourceUrl}/time-sheet/attendance/export?type=${type}`, filterParam, {
      observe: 'response',
      responseType: 'blob',
    });
  }

  printLocationHistory(filterParam: any, type: string): any {
    return this.http.post(`${this.resourceUrl}/location-history/export?type=${type}`, filterParam, {
      observe: 'response',
      responseType: 'blob',
    });
  }

  printTimeSheetLateSummary(filterParam: any, type: string): any {
    return this.http.post(`${this.resourceUrl}/time-sheet/late-summary/export?type=${type}`, filterParam, {
      observe: 'response',
      responseType: 'blob',
    });
  }

  printTimeSheet(filterParam: any, type: string): any {
    return this.http.post(`${this.resourceUrl}/time-sheet/export?type=${type}`, filterParam, {
      observe: 'response',
      responseType: 'blob',
    });
  }

  printTimeSheetSummary(filterParam: any, type: string): any {
    return this.http.post(`${this.resourceUrl}/time-sheet-summary/export?type=${type}`, filterParam, {
      observe: 'response',
      responseType: 'blob',
    });
  }
}
