import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';

import { SharedModule } from 'app/shared/shared.module';
import { GenericFilterFormModule } from 'app/shared/forms/generic-filter-form/generic-filter-form.module';

import { TimeSheetSummaryComponent } from './time-sheet/components/time-sheet-summary/time-sheet-summary.component';
import { ReportsComponent } from './reports.component';
import { TimeSheetLateSummaryComponent } from './time-sheet/components/time-sheet-late-summary/time-sheet-late-summary.component';
import { TimeSheetAttendanceComponent } from './time-sheet/components/time-sheet-attendance/time-sheet-attendance.component';
import { ScheduleSummaryComponent } from './schedule/schedule-summary/schedule-summary.component';
import { PositionSummaryComponent } from './schedule/position-summary/position-summary.component';
import { ShiftsScheduledComponent } from './schedule/shifts-scheduled/shifts-scheduled.component';
import { HoursScheduledComponent } from './schedule/hours-scheduled/hours-scheduled.component';
import { TimeSheetReportComponent } from './time-sheet/time-sheet-report.component';
import { LocationHistoryComponent } from './time-sheet/components/location-history/location-history.component';

@NgModule({
  declarations: [
    ReportsComponent,
    TimeSheetReportComponent,
    TimeSheetSummaryComponent,
    TimeSheetLateSummaryComponent,
    TimeSheetAttendanceComponent,
    ScheduleSummaryComponent,
    PositionSummaryComponent,
    ShiftsScheduledComponent,
    HoursScheduledComponent,
    LocationHistoryComponent,
  ],
  imports: [
    SharedModule,
    GenericFilterFormModule,
    RouterModule.forChild([
      {
        path: '',
        redirectTo: '/reports/timesheets',
        pathMatch: 'full',
      },
      {
        path: '',
        component: ReportsComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: 'timesheets',
            component: TimeSheetReportComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'timesheets-summary',
            component: TimeSheetSummaryComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'timesheets-attendance',
            component: TimeSheetAttendanceComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'timesheets-late-summary',
            component: TimeSheetLateSummaryComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'schedule-summary',
            component: ScheduleSummaryComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'shifts-scheduled',
            component: ShiftsScheduledComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'position-summary',
            component: PositionSummaryComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'hours-scheduled',
            component: HoursScheduledComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'location-history',
            component: LocationHistoryComponent,
            canActivate: [AuthGuard],
          },
        ],
      },
    ]),
  ],
})
export class ReportsModule {}
