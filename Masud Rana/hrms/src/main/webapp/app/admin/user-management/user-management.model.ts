import { IUserCompany } from './../../core/auth/account.model';

export interface IUser {
  id?: number;
  username?: string;
  firstName?: string | null;
  lastName?: string | null;
  email?: string;
  activated?: boolean;
  langKey?: string;
  user_companies?: IUserCompany[];
  created_by?: string;
  created_at?: Date;
  updated_by?: string;
  updated_at?: Date;
}

export class User implements IUser {
  constructor(
    public id?: number,
    public username?: string,
    public firstName?: string | null,
    public lastName?: string | null,
    public email?: string,
    public activated?: boolean,
    public langKey?: string,
    public user_companies?: IUserCompany[],
    public created_by?: string,
    public created_at?: Date,
    public updated_by?: string,
    public updated_at?: Date
  ) {}
}
