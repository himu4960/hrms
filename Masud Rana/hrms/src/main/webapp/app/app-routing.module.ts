import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { errorRoute } from './layouts/error/error.route';
import { navbarRoute } from './layouts/navbar/navbar.route';

import { DEBUG_INFO_ENABLED } from 'app/app.constants';
import { RoleType } from './config/role.constants';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';
import { NotAuthGuard } from './core/auth/guards/not-auth-guard.service';

const LAYOUT_ROUTES = [navbarRoute, ...errorRoute];

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: 'admin',
          data: {
            user_companies: [RoleType.OWNER],
          },
          canActivate: [AuthGuard],
          loadChildren: () => import('./admin/admin-routing.module').then(m => m.AdminRoutingModule),
        },
        {
          path: 'account',
          canActivate: [AuthGuard],
          loadChildren: () => import('./account/account.module').then(m => m.AccountModule),
        },
        {
          path: 'login',
          canActivate: [NotAuthGuard],
          loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
        },
        {
          path: 'register',
          canActivate: [NotAuthGuard],
          loadChildren: () => import('./register/register.module').then(m => m.RegisterModule),
        },
        {
          path: 'forgot-password',
          canActivate: [NotAuthGuard],
          loadChildren: () => import('./forgot-password/forgot-password.module').then(m => m.ForgotPasswordModule),
        },
        { path: '', redirectTo: '/home', pathMatch: 'full' }, // redirect to `Login`
        {
          path: 'home',
          canActivate: [AuthGuard],
          loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
        },
        {
          path: 'employee',
          canActivate: [AuthGuard],
          loadChildren: () => import('./employee/module/employee.module').then(m => m.EmployeeModule),
        },
        {
          path: 'company',
          data: { pageTitle: 'hrmsApp.company.home.title' },
          loadChildren: () => import('./company/company.module').then(m => m.CompanyModule),
        },
        {
          path: 'training',
          canActivate: [AuthGuard],
          loadChildren: () => import('./training/training.module').then(m => m.TrainingModule),
        },
        {
          path: 'time-clock',
          canActivate: [AuthGuard],
          loadChildren: () => import('./time-clock/time-clock.module').then(m => m.TimeClockModule),
        },
        {
          path: 'leave',
          canActivate: [AuthGuard],
          loadChildren: () => import('./leave/leave.module').then(m => m.LeaveModule),
        },
        {
          path: 'shift-planning',
          canActivate: [AuthGuard],
          loadChildren: () => import('./shift-planning/shift-planning.module').then(m => m.ShiftPlanningModule),
        },
        {
          path: 'availability',
          canActivate: [AuthGuard],
          loadChildren: () => import('./availability/availability.module').then(m => m.AvailabilityModule),
        },
        {
          path: 'file-upload',
          canActivate: [AuthGuard],
          loadChildren: () => import('./file-upload/file-upload.module').then(m => m.FileUploadModule),
        },
        {
          path: 'payroll',
          canActivate: [AuthGuard],
          loadChildren: () => import('./payroll/payroll.module').then(m => m.PayrollModule),
        },
        {
          path: 'reports',
          canActivate: [AuthGuard],
          loadChildren: () => import('./reports/reports.module').then(m => m.ReportsModule),
        },
        ...LAYOUT_ROUTES,
      ],
      { enableTracing: DEBUG_INFO_ENABLED }
    ),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
