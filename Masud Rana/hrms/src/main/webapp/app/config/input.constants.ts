export const DATE_FORMAT = 'YYYY-MM-DD';
export const DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm';

export const SLASH_SEPARATED_DATE = 'YYYY/MM/DD';
export const HYPHEN_SEPARATED_DATE = 'YYYY-MM-DD';
export const COMMA_SEPARATED_DATE = 'MMM DD, YYYY';
