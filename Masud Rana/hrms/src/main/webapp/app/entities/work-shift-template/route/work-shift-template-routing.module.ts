import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WorkShiftTemplateComponent } from '../list/work-shift-template.component';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';

const workShiftTemplateRoute: Routes = [
  {
    path: '',
    component: WorkShiftTemplateComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(workShiftTemplateRoute)],
  exports: [RouterModule],
})
export class WorkShiftTemplateRoutingModule {}
