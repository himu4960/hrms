import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HttpResponse } from '@angular/common/http';

import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IWorkShiftTemplate } from '../work-shift-template.model';

import { WorkShiftTemplateService } from '../service/work-shift-template.service';

import { CreateWorkShiftTemplateComponent } from '../create/work-shift-template-create.component';

import { ETimeType } from 'app/shared/pipes/time/time-type.enum';

@Component({
  selector: 'hrms-work-shift-template',
  templateUrl: './work-shift-template.component.html',
})
export class WorkShiftTemplateComponent implements OnInit, OnDestroy {
  get eTimeType(): any {
    return ETimeType;
  }

  workShiftTemplates?: IWorkShiftTemplate[];
  workShiftTemplateId?: number;
  isLoading = false;

  @ViewChild('deleteModal') deleteModal!: NgbModal;

  private subscriptions: Subscription = new Subscription();

  constructor(protected workShiftTemplateService: WorkShiftTemplateService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;
    this.subscriptions.add(
      this.workShiftTemplateService.query().subscribe(
        (res: HttpResponse<IWorkShiftTemplate[]>) => {
          this.isLoading = false;
          this.workShiftTemplates = res.body ?? [];
        },
        () => {
          this.isLoading = false;
        }
      )
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  addTemplate(): void {
    const modelRef = this.modalService.open(CreateWorkShiftTemplateComponent, { size: 'md', backdrop: 'static' });
    modelRef.closed.subscribe(() => {
      this.loadAll();
    });
  }

  public showDeleteModal(workShiftTemplate: IWorkShiftTemplate): void {
    this.modalService.open(this.deleteModal, { size: 'md', backdrop: 'static' });
    this.workShiftTemplateId = workShiftTemplate?.id;
  }

  deleteWorkShiftTemplate(): void {
    if (this.workShiftTemplateId) {
      this.workShiftTemplateService.delete(this.workShiftTemplateId).subscribe(() => {
        this.loadAll();
        this.modalService.dismissAll();
      });
    }
  }

  trackId(index: number, item: IWorkShiftTemplate): number {
    return item.id!;
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
