import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { WorkShiftTemplateService } from '../service/work-shift-template.service';

import { WorkShiftTemplateComponent } from './work-shift-template.component';

describe('Component Tests', () => {
  describe('WorkShiftTemplate Management Component', () => {
    let comp: WorkShiftTemplateComponent;
    let fixture: ComponentFixture<WorkShiftTemplateComponent>;
    let service: WorkShiftTemplateService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [WorkShiftTemplateComponent],
      })
        .overrideTemplate(WorkShiftTemplateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WorkShiftTemplateComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(WorkShiftTemplateService);

      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.workShiftTemplates?.[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
