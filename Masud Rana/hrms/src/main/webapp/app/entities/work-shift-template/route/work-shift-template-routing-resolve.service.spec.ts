jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IWorkShiftTemplate, WorkShiftTemplate } from '../work-shift-template.model';

import { WorkShiftTemplateService } from '../service/work-shift-template.service';
import { WorkShiftTemplateRoutingResolveService } from './work-shift-template-routing-resolve.service';

describe('Service Tests', () => {
  describe('WorkShiftTemplate routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: WorkShiftTemplateRoutingResolveService;
    let service: WorkShiftTemplateService;
    let resultWorkShiftTemplate: IWorkShiftTemplate | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(WorkShiftTemplateRoutingResolveService);
      service = TestBed.inject(WorkShiftTemplateService);
      resultWorkShiftTemplate = undefined;
    });

    describe('resolve', () => {
      it('should return IWorkShiftTemplate returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultWorkShiftTemplate = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultWorkShiftTemplate).toEqual({ id: 123 });
      });

      it('should return new IWorkShiftTemplate if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultWorkShiftTemplate = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultWorkShiftTemplate).toEqual(new WorkShiftTemplate());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultWorkShiftTemplate = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultWorkShiftTemplate).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
