import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { WorkShiftTemplateRoutingModule } from './route/work-shift-template-routing.module';

import { CreateWorkShiftTemplateComponent } from './create/work-shift-template-create.component';

import { WorkShiftTemplateComponent } from './list/work-shift-template.component';

@NgModule({
  imports: [SharedModule, WorkShiftTemplateRoutingModule],
  declarations: [WorkShiftTemplateComponent, CreateWorkShiftTemplateComponent],
})
export class WorkShiftTemplateModule {}
