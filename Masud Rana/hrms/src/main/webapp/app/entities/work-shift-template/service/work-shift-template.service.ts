import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IWorkShiftTemplate, getWorkShiftTemplateIdentifier } from '../work-shift-template.model';

export type EntityResponseType = HttpResponse<IWorkShiftTemplate>;
export type EntityArrayResponseType = HttpResponse<IWorkShiftTemplate[]>;

@Injectable({ providedIn: 'root' })
export class WorkShiftTemplateService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('work-shift-templates');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IWorkShiftTemplate>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IWorkShiftTemplate[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  create(leaveType: IWorkShiftTemplate): Observable<EntityResponseType> {
    return this.http.post<IWorkShiftTemplate>(this.resourceUrl, leaveType, { observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
