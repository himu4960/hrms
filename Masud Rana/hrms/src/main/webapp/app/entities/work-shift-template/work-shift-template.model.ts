import { IBranch } from '../branch/branch.model';
import { IDesignation } from '../designation/designation.model';

export interface IWorkShiftTemplate {
  id?: number;
  branch_id?: number;
  designation_id?: number;
  start_time?: number;
  end_time?: number;
  total_work_shift_duration?: number;
  designation?: IDesignation;
  branch?: IBranch;
}

export class WorkShiftTemplate implements IWorkShiftTemplate {
  constructor(
    public id?: number,
    public branch_id?: number,
    public designation_id?: number,
    public start_time?: number,
    public end_time?: number,
    public total_work_shift_duration?: number,
    public designation?: IDesignation,
    public branch?: IBranch
  ) {}
}

export function getWorkShiftTemplateIdentifier(workShiftTemplate: IWorkShiftTemplate): number | undefined {
  return workShiftTemplate.id;
}
