import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IWorkShiftTemplate, WorkShiftTemplate } from '../work-shift-template.model';
import { WorkShiftTemplateService } from '../service/work-shift-template.service';

@Injectable({ providedIn: 'root' })
export class WorkShiftTemplateRoutingResolveService implements Resolve<IWorkShiftTemplate> {
  constructor(protected service: WorkShiftTemplateService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IWorkShiftTemplate> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((workShiftTemplate: HttpResponse<WorkShiftTemplate>) => {
          if (workShiftTemplate.body) {
            return of(workShiftTemplate.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new WorkShiftTemplate());
  }
}
