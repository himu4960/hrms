import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IWorkShiftTemplate } from '../work-shift-template.model';
import { IBranch } from 'app/entities/branch/branch.model';

import { WorkShiftTemplateService } from '../service/work-shift-template.service';
import { UtilService } from 'app/shared/service/util.service';

@Component({
  selector: 'hrms-work-shift-template-create',
  templateUrl: './work-shift-template-create.component.html',
})
export class CreateWorkShiftTemplateComponent implements OnInit, OnDestroy {
  isSaving = false;
  branches: IBranch[] = [];
  designations: any[] = [];
  branchId!: number;
  timeDifference = '00';

  workShiftTemplateForm = this.fb.group({
    id: [],
    branch_id: [null, [Validators.required]],
    designation_id: [null, [Validators.required]],
    start_time: [null, [Validators.required]],
    end_time: [null, [Validators.required]],
  });

  branchIdControl = this.workShiftTemplateForm.get('branch_id') as AbstractControl;
  designationIdControl = this.workShiftTemplateForm.get('designation_id') as AbstractControl;
  startTimeControl = this.workShiftTemplateForm.get('start_time') as AbstractControl;
  endTimeControl = this.workShiftTemplateForm.get('end_time') as AbstractControl;

  private subscriptions: Subscription = new Subscription();

  constructor(
    public activeModal: NgbActiveModal,
    private workShiftTemplateService: WorkShiftTemplateService,
    private utilService: UtilService,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.subscriptions.add(
      this.utilService.getBranches().subscribe((branches: IBranch[]) => {
        let branch_id: number;
        branches.map(branch => {
          if (branch) {
            if (branch_id !== branch.id) {
              this.designations.push({ branch_id: branch.id, branch_name: branch.name });
              branch_id = branch.id ? branch.id : branch_id;
            }
            if (branch?.designations?.length) {
              branch.designations.forEach(designation => {
                const designationOptions = {
                  branch_id: branch.id,
                  branch_name: branch.name,
                  designation_id: designation.id,
                  designation_name: designation.name,
                };
                this.designations.push(designationOptions);
              });
            }
          }
        });
      })
    );

    this.subscriptions.add(
      this.startTimeControl?.valueChanges.subscribe(startTime => {
        this.timeDifference = this.utilService.getTimeDifference(startTime, this.endTimeControl?.value);
      })
    );

    this.subscriptions.add(
      this.endTimeControl?.valueChanges.subscribe(endTime => {
        this.timeDifference = this.utilService.getTimeDifference(this.startTimeControl?.value, endTime);
      })
    );
  }

  save(): void {
    const designation = this.designationIdControl?.value;
    this.isSaving = true;
    const WorkShiftTemplate: IWorkShiftTemplate = {
      branch_id: designation.branch_id,
      designation_id: designation.designation_id,
      start_time: this.utilService.convertHoursToSeconds(this.startTimeControl?.value),
      end_time: this.utilService.convertHoursToSeconds(this.endTimeControl?.value),
    };
    this.subscribeToSaveResponse(this.workShiftTemplateService.create(WorkShiftTemplate));
  }

  cancel(): void {
    this.activeModal.close();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWorkShiftTemplate>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.cancel();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }
}
