import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';

import { of } from 'rxjs';

import { PermissionTypeDetailComponent } from './permission-type-detail.component';

describe('Component Tests', () => {
  describe('PermissionType Management Detail Component', () => {
    let comp: PermissionTypeDetailComponent;
    let fixture: ComponentFixture<PermissionTypeDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [PermissionTypeDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ permissionType: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(PermissionTypeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PermissionTypeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load permissionType on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.permissionType).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
