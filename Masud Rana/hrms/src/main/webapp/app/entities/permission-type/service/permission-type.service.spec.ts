import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IPermissionType, PermissionType } from '../permission-type.model';

import { PermissionTypeService } from './permission-type.service';

describe('Service Tests', () => {
  describe('PermissionType Service', () => {
    let service: PermissionTypeService;
    let httpMock: HttpTestingController;
    let elemDefault: IPermissionType;
    let expectedResult: IPermissionType | IPermissionType[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(PermissionTypeService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        name: 'AAAAAAA',
        key: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a PermissionType', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new PermissionType()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a PermissionType', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'BBBBBB',
            key: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a PermissionType', () => {
        const patchObject = Object.assign(
          {
            name: 'BBBBBB',
          },
          new PermissionType()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of PermissionType', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'BBBBBB',
            key: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a PermissionType', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addPermissionTypeToCollectionIfMissing', () => {
        it('should add a PermissionType to an empty array', () => {
          const permissionType: IPermissionType = { id: 123 };
          expectedResult = service.addPermissionTypeToCollectionIfMissing([], permissionType);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(permissionType);
        });

        it('should not add a PermissionType to an array that contains it', () => {
          const permissionType: IPermissionType = { id: 123 };
          const permissionTypeCollection: IPermissionType[] = [
            {
              ...permissionType,
            },
            { id: 456 },
          ];
          expectedResult = service.addPermissionTypeToCollectionIfMissing(permissionTypeCollection, permissionType);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a PermissionType to an array that doesn't contain it", () => {
          const permissionType: IPermissionType = { id: 123 };
          const permissionTypeCollection: IPermissionType[] = [{ id: 456 }];
          expectedResult = service.addPermissionTypeToCollectionIfMissing(permissionTypeCollection, permissionType);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(permissionType);
        });

        it('should add only unique PermissionType to an array', () => {
          const permissionTypeArray: IPermissionType[] = [{ id: 123 }, { id: 456 }, { id: 42272 }];
          const permissionTypeCollection: IPermissionType[] = [{ id: 123 }];
          expectedResult = service.addPermissionTypeToCollectionIfMissing(permissionTypeCollection, ...permissionTypeArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const permissionType: IPermissionType = { id: 123 };
          const permissionType2: IPermissionType = { id: 456 };
          expectedResult = service.addPermissionTypeToCollectionIfMissing([], permissionType, permissionType2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(permissionType);
          expect(expectedResult).toContain(permissionType2);
        });

        it('should accept null and undefined values', () => {
          const permissionType: IPermissionType = { id: 123 };
          expectedResult = service.addPermissionTypeToCollectionIfMissing([], null, permissionType, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(permissionType);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
