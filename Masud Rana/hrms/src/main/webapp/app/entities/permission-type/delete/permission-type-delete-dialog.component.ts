import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { EModalReasonType } from 'app/shared/enum';

import { IPermissionType } from '../permission-type.model';

import { PermissionTypeService } from '../service/permission-type.service';

@Component({
  templateUrl: './permission-type-delete-dialog.component.html',
})
export class PermissionTypeDeleteDialogComponent {
  permissionType?: IPermissionType;

  constructor(protected permissionTypeService: PermissionTypeService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.permissionTypeService.delete(id).subscribe(() => {
      this.activeModal.close(EModalReasonType.DELETED);
    });
  }
}
