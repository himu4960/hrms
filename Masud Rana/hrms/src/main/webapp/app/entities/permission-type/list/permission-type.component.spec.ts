import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { of } from 'rxjs';

import { PermissionTypeService } from '../service/permission-type.service';

import { PermissionTypeComponent } from './permission-type.component';

describe('Component Tests', () => {
  describe('PermissionType Management Component', () => {
    let comp: PermissionTypeComponent;
    let fixture: ComponentFixture<PermissionTypeComponent>;
    let service: PermissionTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [PermissionTypeComponent],
      })
        .overrideTemplate(PermissionTypeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PermissionTypeComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(PermissionTypeService);

      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.permissionTypes?.[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
