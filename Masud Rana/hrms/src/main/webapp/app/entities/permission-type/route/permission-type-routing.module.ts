import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';
import { PermissionTypeRoutingResolveService } from './permission-type-routing-resolve.service';

import { PermissionTypeComponent } from '../list/permission-type.component';
import { PermissionTypeDetailComponent } from '../detail/permission-type-detail.component';
import { PermissionTypeUpdateComponent } from '../update/permission-type-update.component';

const permissionTypeRoute: Routes = [
  {
    path: '',
    component: PermissionTypeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: ':id/view',
    component: PermissionTypeDetailComponent,
    resolve: {
      permissionType: PermissionTypeRoutingResolveService,
    },
    canActivate: [AuthGuard],
  },
  {
    path: 'new',
    component: PermissionTypeUpdateComponent,
    resolve: {
      permissionType: PermissionTypeRoutingResolveService,
    },
    canActivate: [AuthGuard],
  },
  {
    path: ':id/edit',
    component: PermissionTypeUpdateComponent,
    resolve: {
      permissionType: PermissionTypeRoutingResolveService,
    },
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(permissionTypeRoute)],
  exports: [RouterModule],
})
export class PermissionTypeRoutingModule {}
