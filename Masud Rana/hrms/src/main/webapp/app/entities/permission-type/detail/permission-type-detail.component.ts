import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPermissionType } from '../permission-type.model';

@Component({
  selector: 'hrms-permission-type-detail',
  templateUrl: './permission-type-detail.component.html',
})
export class PermissionTypeDetailComponent implements OnInit {
  permissionType: IPermissionType | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ permissionType }) => {
      this.permissionType = permissionType;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
