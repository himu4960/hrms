jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { of, Subject } from 'rxjs';

import { PermissionTypeService } from '../service/permission-type.service';

import { IPermissionType, PermissionType } from '../permission-type.model';

import { PermissionTypeUpdateComponent } from './permission-type-update.component';

describe('Component Tests', () => {
  describe('PermissionType Management Update Component', () => {
    let comp: PermissionTypeUpdateComponent;
    let fixture: ComponentFixture<PermissionTypeUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let permissionTypeService: PermissionTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [PermissionTypeUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(PermissionTypeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PermissionTypeUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      permissionTypeService = TestBed.inject(PermissionTypeService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const permissionType: IPermissionType = { id: 456 };

        activatedRoute.data = of({ permissionType });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(permissionType));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const permissionType = { id: 123 };
        spyOn(permissionTypeService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ permissionType });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: permissionType }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(permissionTypeService.update).toHaveBeenCalledWith(permissionType);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const permissionType = new PermissionType();
        spyOn(permissionTypeService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ permissionType });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: permissionType }));
        saveSubject.complete();

        // THEN
        expect(permissionTypeService.create).toHaveBeenCalledWith(permissionType);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const permissionType = { id: 123 };
        spyOn(permissionTypeService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ permissionType });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(permissionTypeService.update).toHaveBeenCalledWith(permissionType);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
