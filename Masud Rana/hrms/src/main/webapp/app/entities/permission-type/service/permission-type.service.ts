import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';

import { ApplicationConfigService } from 'app/core/config/application-config.service';

import { createRequestOption } from 'app/core/request/request-util';

import { IPermissionType, getPermissionTypeIdentifier } from '../permission-type.model';

export type EntityResponseType = HttpResponse<IPermissionType>;
export type EntityArrayResponseType = HttpResponse<IPermissionType[]>;

@Injectable({ providedIn: 'root' })
export class PermissionTypeService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('permission-types');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(permissionType: IPermissionType): Observable<EntityResponseType> {
    return this.http.post<IPermissionType>(this.resourceUrl, permissionType, { observe: 'response' });
  }

  update(permissionType: IPermissionType): Observable<EntityResponseType> {
    return this.http.put<IPermissionType>(`${this.resourceUrl}/${getPermissionTypeIdentifier(permissionType) as number}`, permissionType, {
      observe: 'response',
    });
  }

  partialUpdate(permissionType: IPermissionType): Observable<EntityResponseType> {
    return this.http.patch<IPermissionType>(
      `${this.resourceUrl}/${getPermissionTypeIdentifier(permissionType) as number}`,
      permissionType,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPermissionType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPermissionType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPermissionTypeToCollectionIfMissing(
    permissionTypeCollection: IPermissionType[],
    ...permissionTypesToCheck: (IPermissionType | null | undefined)[]
  ): IPermissionType[] {
    const permissionTypes: IPermissionType[] = permissionTypesToCheck.filter(isPresent);
    if (permissionTypes.length > 0) {
      const permissionTypeCollectionIdentifiers = permissionTypeCollection.map(
        permissionTypeItem => getPermissionTypeIdentifier(permissionTypeItem)!
      );
      const permissionTypesToAdd = permissionTypes.filter(permissionTypeItem => {
        const permissionTypeIdentifier = getPermissionTypeIdentifier(permissionTypeItem);
        if (permissionTypeIdentifier == null || permissionTypeCollectionIdentifiers.includes(permissionTypeIdentifier)) {
          return false;
        }
        permissionTypeCollectionIdentifiers.push(permissionTypeIdentifier);
        return true;
      });
      return [...permissionTypesToAdd, ...permissionTypeCollection];
    }
    return permissionTypeCollection;
  }
}
