jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';

import { of } from 'rxjs';

import { IPermissionType, PermissionType } from '../permission-type.model';
import { PermissionTypeService } from '../service/permission-type.service';

import { PermissionTypeRoutingResolveService } from './permission-type-routing-resolve.service';

describe('Service Tests', () => {
  describe('PermissionType routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: PermissionTypeRoutingResolveService;
    let service: PermissionTypeService;
    let resultPermissionType: IPermissionType | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(PermissionTypeRoutingResolveService);
      service = TestBed.inject(PermissionTypeService);
      resultPermissionType = undefined;
    });

    describe('resolve', () => {
      it('should return IPermissionType returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPermissionType = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultPermissionType).toEqual({ id: 123 });
      });

      it('should return new IPermissionType if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPermissionType = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultPermissionType).toEqual(new PermissionType());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultPermissionType = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultPermissionType).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
