import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { PermissionTypeComponent } from './list/permission-type.component';
import { PermissionTypeDetailComponent } from './detail/permission-type-detail.component';
import { PermissionTypeUpdateComponent } from './update/permission-type-update.component';
import { PermissionTypeDeleteDialogComponent } from './delete/permission-type-delete-dialog.component';

import { PermissionTypeRoutingModule } from './route/permission-type-routing.module';

@NgModule({
  imports: [SharedModule, PermissionTypeRoutingModule],
  declarations: [
    PermissionTypeComponent,
    PermissionTypeDetailComponent,
    PermissionTypeUpdateComponent,
    PermissionTypeDeleteDialogComponent,
  ],
  entryComponents: [PermissionTypeDeleteDialogComponent],
})
export class PermissionTypeModule {}
