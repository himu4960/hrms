import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { EModalReasonType } from 'app/shared/enum';

import { IPermissionType } from '../permission-type.model';

import { PermissionTypeService } from '../service/permission-type.service';

import { PermissionTypeDeleteDialogComponent } from '../delete/permission-type-delete-dialog.component';

@Component({
  selector: 'hrms-permission-type',
  templateUrl: './permission-type.component.html',
})
export class PermissionTypeComponent implements OnInit {
  permissionTypes?: IPermissionType[];
  isLoading = false;

  constructor(protected permissionTypeService: PermissionTypeService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.permissionTypeService.query().subscribe(
      (res: HttpResponse<IPermissionType[]>) => {
        this.isLoading = false;
        this.permissionTypes = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IPermissionType): number {
    return item.id!;
  }

  delete(permissionType: IPermissionType): void {
    const modalRef = this.modalService.open(PermissionTypeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.permissionType = permissionType;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.DELETED) {
        this.loadAll();
      }
    });
  }
}
