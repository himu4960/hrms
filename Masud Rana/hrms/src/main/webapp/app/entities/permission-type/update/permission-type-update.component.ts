import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IPermissionType, PermissionType } from '../permission-type.model';

import { PermissionTypeService } from '../service/permission-type.service';

@Component({
  selector: 'hrms-permission-type-update',
  templateUrl: './permission-type-update.component.html',
})
export class PermissionTypeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(50)]],
    key: [null, [Validators.maxLength(50)]],
  });

  constructor(
    protected permissionTypeService: PermissionTypeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ permissionType }) => {
      this.updateForm(permissionType);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const permissionType = this.createFromForm();
    if (permissionType.id !== undefined) {
      this.subscribeToSaveResponse(this.permissionTypeService.update(permissionType));
    } else {
      this.subscribeToSaveResponse(this.permissionTypeService.create(permissionType));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPermissionType>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(permissionType: IPermissionType): void {
    this.editForm.patchValue({
      id: permissionType.id,
      name: permissionType.name,
      key: permissionType.key,
    });
  }

  protected createFromForm(): IPermissionType {
    return {
      ...new PermissionType(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      key: this.editForm.get(['key'])!.value,
    };
  }
}
