import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';

import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPermissionType, PermissionType } from '../permission-type.model';

import { PermissionTypeService } from '../service/permission-type.service';

@Injectable({ providedIn: 'root' })
export class PermissionTypeRoutingResolveService implements Resolve<IPermissionType> {
  constructor(protected service: PermissionTypeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPermissionType> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((permissionType: HttpResponse<PermissionType>) => {
          if (permissionType.body) {
            return of(permissionType.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PermissionType());
  }
}
