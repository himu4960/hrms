jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { RoleService } from '../service/role.service';
import { ApplicationTypeService } from 'app/entities/application-type/service/application-type.service';

import { IRole } from '../role.model';
import { IApplicationType } from 'app/entities/application-type/application-type.model';

import { RoleUpdateComponent } from './role-update.component';

describe('Component Tests', () => {
  describe('Role Management Update Component', () => {
    let comp: RoleUpdateComponent;
    let fixture: ComponentFixture<RoleUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let applicationTypeService: ApplicationTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [RoleUpdateComponent],
        providers: [FormBuilder, provideMockStore(), ActivatedRoute],
      })
        .overrideTemplate(RoleUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RoleUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      TestBed.inject(RoleService);
      applicationTypeService = TestBed.inject(ApplicationTypeService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call ApplicationType query and add missing value', () => {
        const role: IRole = { id: 456 };
        const applicationType: IApplicationType = { id: 22242 };
        role.applicationType = applicationType;

        const applicationTypeCollection: IApplicationType[] = [{ id: 74126 }];
        spyOn(applicationTypeService, 'query').and.returnValue(of(new HttpResponse({ body: applicationTypeCollection })));
        const additionalApplicationTypes = [applicationType];
        const expectedCollection: IApplicationType[] = [...additionalApplicationTypes, ...applicationTypeCollection];
        spyOn(applicationTypeService, 'addApplicationTypeToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ role });
        comp.ngOnInit();

        expect(applicationTypeService.query).toHaveBeenCalled();
        expect(applicationTypeService.addApplicationTypeToCollectionIfMissing).toHaveBeenCalledWith(
          applicationTypeCollection,
          ...additionalApplicationTypes
        );
        expect(comp.applicationTypesSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const role: IRole = { id: 456 };
        const applicationType: IApplicationType = { id: 23116 };
        role.applicationType = applicationType;

        activatedRoute.data = of({ role });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(role));
        expect(comp.applicationTypesSharedCollection).toContain(applicationType);
      });
    });

    // describe('save', () => {
    //   it('Should call update service on save for existing entity', () => {
    //     // GIVEN
    //     const saveSubject = new Subject();
    //     const role = { id: 123 };
    //     spyOn(roleService, 'update').and.returnValue(saveSubject);
    //     spyOn(comp, 'previousState');
    //     activatedRoute.data = of({ role });
    //     comp.ngOnInit();

    //     // WHEN
    //     comp.save();
    //     expect(comp.isSaving).toEqual(true);
    //     saveSubject.next(new HttpResponse({ body: role }));
    //     saveSubject.complete();

    //     // THEN
    //     expect(comp.previousState).toHaveBeenCalled();
    //     expect(roleService.update).toHaveBeenCalledWith(role);
    //     expect(comp.isSaving).toEqual(false);
    //   });

    //   it('Should call create service on save for new entity', () => {
    //     // GIVEN
    //     const saveSubject = new Subject();
    //     const role = new Role();
    //     spyOn(roleService, 'create').and.returnValue(saveSubject);
    //     spyOn(comp, 'previousState');
    //     activatedRoute.data = of({ role });
    //     comp.ngOnInit();

    //     // WHEN
    //     comp.save();
    //     expect(comp.isSaving).toEqual(true);
    //     saveSubject.next(new HttpResponse({ body: role }));
    //     saveSubject.complete();

    //     // THEN
    //     expect(roleService.create).toHaveBeenCalledWith(role);
    //     expect(comp.isSaving).toEqual(false);
    //     expect(comp.previousState).toHaveBeenCalled();
    //   });

    //   it('Should set isSaving to false on error', () => {
    //     // GIVEN
    //     const saveSubject = new Subject();
    //     const role = { id: 123 };
    //     spyOn(roleService, 'update').and.returnValue(saveSubject);
    //     spyOn(comp, 'previousState');
    //     activatedRoute.data = of({ role });
    //     comp.ngOnInit();

    //     // WHEN
    //     comp.save();
    //     expect(comp.isSaving).toEqual(true);
    //     saveSubject.error('This is an error!');

    //     // THEN
    //     expect(roleService.update).toHaveBeenCalledWith(role);
    //     expect(comp.isSaving).toEqual(false);
    //     expect(comp.previousState).not.toHaveBeenCalled();
    //   });
    // });

    describe('Tracking relationships identifiers', () => {
      describe('trackApplicationTypeById', () => {
        it('Should return tracked ApplicationType primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackApplicationTypeById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
