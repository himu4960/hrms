import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoleComponent } from '../list/role.component';
import { RoleDetailComponent } from '../detail/role-detail.component';
import { RoleUpdateComponent } from '../update/role-update.component';

import { RoleRoutingResolveService } from './role-routing-resolve.service';
import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';
import { SuperAdminRoutePermissionGuard } from 'app/core/auth/guards/super-admin-route-permission.service';

const roleRoute: Routes = [
  {
    path: '',
    component: RoleComponent,
    canActivate: [AuthGuard, SuperAdminRoutePermissionGuard],
  },
  {
    path: ':id/view',
    component: RoleDetailComponent,
    resolve: {
      role: RoleRoutingResolveService,
    },
    canActivate: [AuthGuard, SuperAdminRoutePermissionGuard],
  },
  {
    path: 'new',
    component: RoleUpdateComponent,
    resolve: {
      role: RoleRoutingResolveService,
    },
    canActivate: [AuthGuard, SuperAdminRoutePermissionGuard],
  },
  {
    path: ':id/edit',
    component: RoleUpdateComponent,
    resolve: {
      role: RoleRoutingResolveService,
    },
    canActivate: [AuthGuard, SuperAdminRoutePermissionGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(roleRoute)],
  exports: [RouterModule],
})
export class RoleRoutingModule {}
