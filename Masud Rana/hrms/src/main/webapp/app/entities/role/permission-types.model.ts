export interface IPermissionType {
  id: number;
  name: string;
  key: string;
}

export class PermissionType implements IPermissionType {
  constructor(public id: number, public name: string, public key: string) {}
}
