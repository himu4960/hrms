import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { NgxWebstorageModule } from 'ngx-webstorage';

import { of } from 'rxjs';

import { AclService } from 'app/core/auth/acl.service';
import { RoleService } from '../service/role.service';

import { RoleComponent } from './role.component';

describe('RoleComponent', () => {
  describe('Role Component', () => {
    let comp: RoleComponent;
    let fixture: ComponentFixture<RoleComponent>;
    let service: RoleService;

    beforeEach(() => {
      const routerSpy = { navigate: jasmine.createSpy('navigate') };

      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, NgxWebstorageModule.forRoot()],
        declarations: [RoleComponent],
        providers: [AclService, { provide: Router, useValue: routerSpy }],
      })
        .overrideTemplate(RoleComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RoleComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(RoleService);
      TestBed.inject(AclService);

      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.roles?.[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
