import { IApplicationType } from 'app/entities/application-type/application-type.model';

export interface IRole {
  id?: number;
  name?: string;
  menus?: string;
  is_active?: boolean | null;
  is_deleted?: boolean | null;
  is_employee?: boolean | null;
  applicationType?: IApplicationType | null;
}

export class Role implements IRole {
  constructor(
    public id?: number,
    public name?: string,
    public menus?: string,
    public is_active?: boolean | null,
    public is_deleted?: boolean | null,
    public is_employee?: boolean | null,
    public applicationType?: IApplicationType | null
  ) {
    this.is_active = this.is_active ?? false;
    this.is_deleted = this.is_deleted ?? false;
    this.is_employee = this.is_employee ?? false;
  }
}

export interface IUpdateRole {
  employee_id?: number | null;
  role_id?: number | null;
}

export function getRoleIdentifier(role: IRole): number | undefined {
  return role.id;
}
