import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ITreeOptions, ITreeState, TreeComponent, TreeModel, TreeNode } from '@circlon/angular-tree-component';

import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IRole, Role } from '../role.model';
import { IApplicationType } from 'app/entities/application-type/application-type.model';
import { IMenu } from 'app/entities/menu/menu.model';
import { IPermissionType } from '../permission-types.model';

import { RoleService } from '../service/role.service';
import { ApplicationTypeService } from 'app/entities/application-type/service/application-type.service';
import { MenuService } from 'app/entities/menu/service/menu.service';

import { browserReloadMenuAction } from 'app/store/menu/menu.actions';

import { noWhitespaceValidator } from 'app/shared/validators/no-whitespace.validator';

@Component({
  selector: 'hrms-role-update',
  templateUrl: './role-update.component.html',
})
export class RoleUpdateComponent implements OnInit {
  @ViewChild(TreeComponent) tree!: TreeComponent;
  public treeNodes: any[] = [];
  state: ITreeState;

  options: ITreeOptions = {
    useCheckbox: true,
    actionMapping: {
      mouse: {
        checkboxClick: (rootTree: TreeModel, rootNode: TreeNode) => {
          const value = !rootNode.isSelected;
          const selectedLeadNodes = rootTree.selectedLeafNodeIds;
          setNodeSelected(rootTree, rootNode, value);
          rootTree.selectedLeafNodeIds = Object.assign({}, selectedLeadNodes, { [rootNode.id]: value });

          function setNodeSelected(tree: TreeModel, node: any, is_check: boolean): void {
            if (node.data.permission_type && node.data.permission_type.length > 0) {
              node.data.permission_type.forEach((permission: IPermissionType) => {
                if (is_check === true) {
                  node.data[permission.key] = is_check;
                } else {
                  node.data[permission.key] = is_check;
                }
                return permission;
              });
            }

            if (node.isSelectable()) {
              selectedLeadNodes[node.id] = is_check;
            } else {
              node.visibleChildren.forEach((child: any) => setNodeSelected(tree, child, value));
            }
          }
        },
      },
    },
  };

  isSaving = false;
  applicationTypesSharedCollection: IApplicationType[] = [];
  menus: any[] = [];
  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(50), noWhitespaceValidator]],
    menus: [],
    is_active: [],
    is_deleted: [],
    is_employee: [],
    applicationType: [],
  });

  constructor(
    protected roleService: RoleService,
    protected menuService: MenuService,
    protected applicationTypeService: ApplicationTypeService,
    protected activatedRoute: ActivatedRoute,
    protected store: Store,
    protected fb: FormBuilder
  ) {
    this.state = {
      expandedNodeIds: {},
    };
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ role }) => {
      const menuPermission: IMenu[] = role.menus ? JSON.parse(role.menus) : [];
      this.updateForm(role);

      this.loadRelationshipsOptions();

      this.menuService.queryPermission(menuPermission).subscribe((res: any) => {
        this.treeNodes = res.body ?? [];
        setTimeout(() => {
          menuPermission.forEach((menu: IMenu) => {
            if (menu.id) {
              const node = this.tree.treeModel.getNodeById(menu.id);
              if (node?.data?.can_view || node?.data?.can_add || node?.data?.can_edit) {
                node.setIsSelected(true);
              }
            }
          });
        }, 300);
      });
    });
  }

  previousState(): void {
    window.history.back();
  }

  public check(node: TreeNode, checked: boolean, key: string): void {
    this.updateChildNodeCheckbox(node, checked, key);
    this.updateParentNodeCheckbox(node.realParent, key);
  }

  save(): void {
    this.getSelectNode()
      .then((menus: IMenu[]) => {
        this.editForm.controls.menus.setValue(JSON.stringify(menus));
        this.isSaving = true;
        const role = this.createFromForm();
        if (role.id !== undefined) {
          this.subscribeToSaveResponse(this.roleService.update(role));
        } else {
          this.subscribeToSaveResponse(this.roleService.create(role));
        }
      })
      .catch();
  }

  trackApplicationTypeById(index: number, item: IApplicationType): number {
    return item.id!;
  }

  protected createRolePermission(menu: any): IMenu {
    const permission_type = menu?.permission_type?.filter((permission: IPermissionType) => menu[permission.key] === true);

    return {
      id: menu?.id,
      end_point: menu?.end_point,
      router_link: menu?.router_link,
      parent_id: menu?.parent_id,
      name: menu?.name,
      translate_name: menu?.translate_name,
      icon: menu?.icon,
      is_menu: menu?.is_menu,
      position: menu?.position ?? 0,
      can_view: menu?.can_view ?? false,
      can_add: menu?.can_add ?? false,
      can_edit: menu?.can_edit ?? false,
      can_delete: menu?.can_delete ?? false,
      can_change_password: menu?.can_change_password ?? false,
      can_approve: menu?.can_approve ?? false,
      can_reject: menu?.can_reject ?? false,
      can_publish: menu?.can_publish ?? false,
      permission_type: permission_type ?? [],
      children: [],
    };
  }

  protected updateChildNodeCheckbox(node: TreeNode, checked: boolean, key: string): void {
    if (node.data.permission_type && node.data.permission_type.length > 0) {
      node.data.permission_type.forEach((permission: IPermissionType) => {
        if (checked === true && node.data[key] === key) {
          node.data[key] = checked;
        } else {
          node.data[key] = checked;
        }
        return permission;
      });
    }

    node.children.forEach((child: TreeNode) => this.updateChildNodeCheckbox(child, checked, key));
  }

  protected updateParentNodeCheckbox(node: TreeNode, key: string): void {
    if (!node) {
      return;
    }

    for (const child of node.children) {
      if (child.data[key] === true) {
        const id = child.data.id;
        if (id) {
          const treeModel = this.tree.treeModel.getNodeById(id);
          treeModel.setIsSelected(true);
        }
      }
    }

    this.updateParentNodeCheckbox(node.parent, key);
  }

  protected findParentNode(menus: IMenu[], parentId: string[], menuId: string): string[] {
    const parentNode = menus.filter((r: IMenu) => r.id === parseInt(menuId, 10))[0];
    if (parentNode?.parent_id) {
      parentId.push(String(parentNode.parent_id));
      this.findParentNode(menus, parentId, String(parentNode.parent_id));
    }
    return parentId;
  }

  protected async nodeChildren(treeNode: any[], menus: IMenu[]): Promise<IMenu[]> {
    for (let j = 0; j < treeNode.length; j++) {
      const node = treeNode[j];
      node.permission = [];
      if (treeNode[j].children.length > 0) {
        menus.push(this.createRolePermission(node));
        const data = await this.nodeChildren(node.children, menus);
        menus.push(...data);
      } else {
        menus.push(this.createRolePermission(node));
      }
    }
    return menus;
  }

  protected async getSelectNode(): Promise<IMenu[]> {
    const selecteId: string[] = [];
    if (this.state?.selectedLeafNodeIds) {
      const data = JSON.parse(JSON.stringify(this.state.selectedLeafNodeIds));
      Object.keys(data).filter(key => {
        if (data[key]) {
          selecteId.push(key);
        }
      });
    }

    let menus: IMenu[] = [];
    for (let i = 0; i < this.treeNodes.length; i++) {
      const treeNode = this.treeNodes[i];
      if (treeNode.children.length > 0) {
        menus.push(this.createRolePermission(treeNode));
        const data = await this.nodeChildren(treeNode.children, []);
        menus.push(...data);
      } else {
        menus.push(this.createRolePermission(treeNode));
      }
    }
    menus = [...new Set(menus)];

    selecteId.forEach((id: string) => {
      const parentIds = this.findParentNode(menus, [], id);
      selecteId.push(...parentIds);
    });

    const uniqueSelecteId = [...new Set(selecteId)];

    return this.selectedMenus(menus, uniqueSelecteId);
  }

  protected selectedMenus(menus: IMenu[], selecteId: string[]): IMenu[] {
    return menus.filter((r: IMenu) => selecteId.includes(String(r.id)));
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRole>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
    this.isSaving = false;
  }

  protected onSaveFinalize(): void {
    this.store.dispatch(browserReloadMenuAction());
    this.isSaving = false;
  }

  protected updateForm(role: IRole): void {
    this.editForm.patchValue({
      id: role.id,
      name: role.name,
      menus: role.menus,
      is_active: role.is_active,
      is_deleted: role.is_deleted,
      is_employee: role.is_employee,
      applicationType: role.applicationType,
    });

    this.applicationTypesSharedCollection = this.applicationTypeService.addApplicationTypeToCollectionIfMissing(
      this.applicationTypesSharedCollection,
      role.applicationType
    );
  }

  protected loadRelationshipsOptions(): void {
    this.applicationTypeService
      .query()
      .pipe(map((res: HttpResponse<IApplicationType[]>) => res.body ?? []))
      .pipe(
        map((applicationTypes: IApplicationType[]) =>
          this.applicationTypeService.addApplicationTypeToCollectionIfMissing(applicationTypes, this.editForm.get('applicationType')!.value)
        )
      )
      .subscribe((applicationTypes: IApplicationType[]) => (this.applicationTypesSharedCollection = applicationTypes));
  }

  protected createFromForm(): IRole {
    return {
      ...new Role(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      menus: this.editForm.get(['menus'])!.value,
      is_active: this.editForm.get(['is_active'])!.value,
      is_deleted: this.editForm.get(['is_deleted'])!.value,
      is_employee: this.editForm.get(['is_employee'])!.value,
      applicationType: this.editForm.get(['applicationType'])!.value,
    };
  }
}
