import { NgModule } from '@angular/core';
import { TreeModule } from '@circlon/angular-tree-component';
import { SharedModule } from 'app/shared/shared.module';
import { RoleRoutingModule } from './route/role-routing.module';

import { RoleComponent } from './list/role.component';
import { RoleDetailComponent } from './detail/role-detail.component';
import { RoleUpdateComponent } from './update/role-update.component';
import { RoleDeleteDialogComponent } from './delete/role-delete-dialog.component';

@NgModule({
  imports: [SharedModule, TreeModule, RoleRoutingModule],
  declarations: [RoleComponent, RoleDetailComponent, RoleUpdateComponent, RoleDeleteDialogComponent],
  entryComponents: [RoleDeleteDialogComponent],
})
export class RoleModule {}
