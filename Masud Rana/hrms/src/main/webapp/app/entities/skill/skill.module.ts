import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { SkillRoutingModule } from './route/skill-routing.module';

import { SkillComponent } from './list/skill.component';
import { SkillDeleteDialogComponent } from './delete/skill-delete-dialog.component';

@NgModule({
  imports: [SharedModule, SkillRoutingModule],
  declarations: [SkillComponent, SkillDeleteDialogComponent],
  entryComponents: [SkillDeleteDialogComponent],
})
export class SkillModule {}
