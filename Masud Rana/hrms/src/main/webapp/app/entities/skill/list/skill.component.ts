import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { EModalReasonType } from 'app/shared/enum';

import { Skill, ISkill } from '../skill.model';

import { SkillService } from '../service/skill.service';

import { SkillDeleteDialogComponent } from '../delete/skill-delete-dialog.component';

import { noWhitespaceValidator } from 'app/shared/validators/no-whitespace.validator';

@Component({
  selector: 'hrms-employee-skill',
  templateUrl: './skill.component.html',
})
export class SkillComponent implements OnInit {
  skills?: ISkill[];
  isLoading = false;
  isSaving = false;
  isUpdating = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(255), noWhitespaceValidator]],
  });

  constructor(protected skillService: SkillService, protected modalService: NgbModal, protected fb: FormBuilder) {}

  loadAll(): void {
    this.isLoading = true;

    this.skillService.query().subscribe(
      (res: HttpResponse<ISkill[]>) => {
        this.isLoading = false;
        this.skills = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ISkill): number {
    return item.id!;
  }

  save(): void {
    this.isSaving = true;
    const skill = this.createFromForm();
    if (skill.id) {
      this.subscribeToSaveResponse(this.skillService.update(skill));
    } else {
      this.subscribeToSaveResponse(this.skillService.create(skill));
    }
  }

  delete(skill: ISkill): void {
    const modalRef = this.modalService.open(SkillDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.skill = skill;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.DELETED) {
        this.loadAll();
      }
    });
  }

  public subscribeToSaveResponse(result: Observable<HttpResponse<ISkill>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  public onSaveSuccess(): void {
    this.editForm.reset();
    this.loadAll();
  }

  public onSaveFinalize(): void {
    this.isSaving = false;
    this.isUpdating = false;
  }

  public onSaveError(): void {
    // Api for inheritance.
  }

  public updateForm(skill: ISkill): void {
    this.editForm.patchValue({
      id: skill.id,
      name: skill.name,
    });

    this.isUpdating = true;
  }

  public createFromForm(): ISkill {
    return {
      ...new Skill(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }
}
