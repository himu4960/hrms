export interface ISkill {
  id?: number;
  name?: string;
  checked?: boolean;
}

export class Skill implements ISkill {
  constructor(public id?: number, public company_id?: number, public name?: string) {}
}

export function getSkillIdentifier(skill: ISkill): number | undefined {
  return skill.id;
}
