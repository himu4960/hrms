import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';
import { SkillComponent } from '../list/skill.component';

const skillRoute: Routes = [
  {
    path: '',
    component: SkillComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(skillRoute)],
  exports: [RouterModule],
})
export class SkillRoutingModule {}
