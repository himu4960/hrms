import { ICustomFieldType } from './custom-field-type.model';

export interface ICustomField {
  id?: number;
  name?: string;
  custom_field_type_id?: number;
  permission_type?: string;
  value?: string | null;
  custom_field_type?: ICustomFieldType | null;
}

export class CustomField implements ICustomField {
  constructor(
    public id?: number,
    public name?: string,
    public custom_field_type_id?: number,
    public permission_type?: string,
    public value?: string | null
  ) {}
}

export function getCustomFieldIdentifier(customField: ICustomField): number | undefined {
  return customField.id;
}
