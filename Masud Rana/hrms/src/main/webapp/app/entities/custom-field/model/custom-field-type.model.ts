export interface ICustomFieldType {
  id?: number;
  name?: string;
  data_type?: string;
}

export class CustomFieldType implements ICustomFieldType {
  constructor(public id?: number, public name?: string, public data_type?: string) {}
}

export function getCustomFieldTypeIdentifier(customFieldType: ICustomFieldType): number | undefined {
  return customFieldType.id;
}
