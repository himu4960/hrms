import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, switchMap } from 'rxjs/operators';

import { CustomFieldService } from '../service/custom-field.service';

import { CustomField, ICustomField } from '../model/custom-field.model';
import { ICustomFieldType } from '../model/custom-field-type.model';

@Component({
  selector: 'hrms-custom-field-update',
  templateUrl: './custom-field-update.component.html',
})
export class CustomFieldUpdateComponent implements OnInit {
  isSaving = false;
  fieldTypes: ICustomFieldType[] = [];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(255)]],
    custom_field_type_id: [null, [Validators.required]],
    permission_type: [null, [Validators.required]],
    value: [null, [Validators.maxLength(800)]],
  });

  constructor(protected customFieldService: CustomFieldService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data
      .pipe(
        switchMap(({ customField }) => {
          this.updateForm(customField);
          return this.customFieldService.getCustomFieldType();
        })
      )
      .subscribe((fieldTypes: any) => {
        this.fieldTypes = fieldTypes.body ?? [];
      });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const customField = this.createFromForm();
    if (customField.id !== undefined) {
      this.subscribeToSaveResponse(this.customFieldService.update(customField));
    } else {
      this.subscribeToSaveResponse(this.customFieldService.create(customField));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICustomField>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(customField: ICustomField): void {
    this.editForm.patchValue({
      id: customField.id,
      name: customField.name,
      custom_field_type_id: customField.custom_field_type_id,
      permission_type: customField.permission_type,
      value: customField.value,
    });
  }

  protected createFromForm(): ICustomField {
    return {
      ...new CustomField(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      custom_field_type_id: this.editForm.get(['custom_field_type_id'])!.value,
      permission_type: this.editForm.get(['permission_type'])!.value,
      value: this.editForm.get(['value'])!.value,
    };
  }
}
