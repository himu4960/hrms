export interface IUser {
  id?: number;
  username?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  langKey?: string;
  password?: string;
  password_reset_at?: string;
  imageUrl?: string;
}

export class User implements IUser {
  constructor(
    public id: number,
    public username: string,
    public firstName: string,
    public lastName: string,
    public email: string,
    public langKey: string,
    public password: string,
    public password_reset_at: string,
    public imageUrl: string
  ) {}
}

export function getUserIdentifier(user: IUser): number | undefined {
  return user.id;
}
