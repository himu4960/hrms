import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { EModalReasonType } from 'app/shared/enum';

import { IDesignation, Designation } from '../designation.model';

import { DesignationService } from '../service/designation.service';

import { noWhitespaceValidator } from 'app/shared/validators/no-whitespace.validator';

@Component({
  selector: 'hrms-designation-update',
  templateUrl: './designation-update.component.html',
})
export class DesignationUpdateComponent implements OnInit {
  @Input() public designation: any;
  @Input() public branch_id: any;

  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(255), noWhitespaceValidator]],
  });

  constructor(protected designationService: DesignationService, protected fb: FormBuilder, protected activeModal: NgbActiveModal) {}

  ngOnInit(): void {
    this.updateForm(this.designation);
  }

  cancel(): void {
    this.activeModal.dismiss();
  }

  save(): void {
    this.isSaving = true;
    const designation = this.createFromForm();
    if (designation.id !== undefined) {
      this.designationService.update({ ...designation, branch_id: this.branch_id }).subscribe(() => {
        this.activeModal.close(EModalReasonType.UPDATED);
        this.isSaving = false;
      });
    } else {
      this.designationService.create({ ...designation, branch_id: this.branch_id }).subscribe(() => {
        this.activeModal.close(EModalReasonType.CREATED);
        this.isSaving = false;
      });
    }
  }

  protected updateForm(designation: IDesignation): void {
    this.editForm.patchValue({
      id: designation?.id,
      name: designation?.name,
    });
  }

  protected createFromForm(): IDesignation {
    return {
      ...new Designation(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }
}
