import { Branch } from '../branch/branch.model';
import { Company } from '../../company/company.model';
import { ISkill } from '../skill/skill.model';

export interface IDesignation {
  id?: number;
  name?: string;
  is_visible?: boolean;
  branch_id?: number;
  company?: Company;
  branch?: Branch;
  checked?: boolean;
  skills?: ISkill[];
}

export interface IOpenPositionModalArgs {
  designation?: IDesignation;
  branch_id?: number;
}

export class Designation implements IDesignation {
  constructor(public id?: number, public name?: string, public branch_id?: number, public company?: Company, public branch?: Branch) {}
}

export function getDesignationIdentifier(designation: IDesignation): number | undefined {
  return designation.id;
}
