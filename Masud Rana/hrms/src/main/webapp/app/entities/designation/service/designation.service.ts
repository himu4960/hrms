import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { IDesignation, getDesignationIdentifier } from '../designation.model';

export type EntityResponseType = HttpResponse<IDesignation>;
export type EntityArrayResponseType = HttpResponse<IDesignation[]>;

@Injectable({ providedIn: 'root' })
export class DesignationService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('designations');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(designation: IDesignation): Observable<EntityResponseType> {
    return this.http.post<IDesignation>(this.resourceUrl, designation, { observe: 'response' });
  }

  update(designation: IDesignation): Observable<EntityResponseType> {
    return this.http.put<IDesignation>(`${this.resourceUrl}/${getDesignationIdentifier(designation) as number}`, designation, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDesignation>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(): Observable<EntityArrayResponseType> {
    return this.http.get<IDesignation[]>(this.resourceUrl, { observe: 'response' });
  }

  getDesignationByBranchId(branch_id: number): Observable<EntityArrayResponseType> {
    return this.http.get<IDesignation[]>(`${this.resourceUrl}?branch_id=${branch_id}`, { observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addDesignationToCollectionIfMissing(
    designationCollection: IDesignation[],
    ...designationsToCheck: (IDesignation | null | undefined)[]
  ): IDesignation[] {
    const designations: IDesignation[] = designationsToCheck.filter(isPresent);
    if (designations.length > 0) {
      const designationCollectionIdentifiers = designationCollection.map(designationItem => getDesignationIdentifier(designationItem)!);
      const designationsToAdd = designations.filter(designationItem => {
        const designationIdentifier = getDesignationIdentifier(designationItem);
        if (designationIdentifier == null || designationCollectionIdentifiers.includes(designationIdentifier)) {
          return false;
        }
        designationCollectionIdentifiers.push(designationIdentifier);
        return true;
      });
      return [...designationsToAdd, ...designationCollection];
    }
    return designationCollection;
  }
}
