jest.mock('@ng-bootstrap/ng-bootstrap');
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { of, Subject } from 'rxjs';

import { Designation } from '../designation.model';

import { DesignationService } from '../service/designation.service';

import { DesignationUpdateComponent } from './designation-update.component';

describe('Component Tests', () => {
  describe('Designation Management Update Component', () => {
    let comp: DesignationUpdateComponent;
    let fixture: ComponentFixture<DesignationUpdateComponent>;
    let designationService: DesignationService;
    let mockActiveModal: NgbActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [DesignationUpdateComponent],
        providers: [FormBuilder, NgbActiveModal],
      })
        .overrideTemplate(DesignationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DesignationUpdateComponent);
      designationService = TestBed.inject(DesignationService);

      comp = fixture.componentInstance;
      mockActiveModal = TestBed.inject(NgbActiveModal);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const designation = { id: 123 };
        spyOn(designationService, 'update').and.returnValue(of(saveSubject));
        comp.ngOnInit();

        // WHEN
        comp.save();
        saveSubject.next(new HttpResponse({ body: designation }));
        saveSubject.complete();

        designationService.update(designation).subscribe(() => {
          mockActiveModal.close('updated');
        });

        // THEN
        expect(designationService.update).toHaveBeenCalledWith(designation);
        expect(mockActiveModal.close).toHaveBeenCalledWith('updated');
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const designation = new Designation();
        spyOn(designationService, 'create').and.returnValue(of(saveSubject));

        // WHEN
        comp.save();
        saveSubject.next(new HttpResponse({ body: designation }));
        saveSubject.complete();

        designationService.create(designation).subscribe(() => {
          mockActiveModal.close('created');
        });

        // THEN
        expect(designationService.create).toHaveBeenCalledWith(designation);
        expect(mockActiveModal.close).toHaveBeenCalledWith('created');
      });
    });
  });
});
