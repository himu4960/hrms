import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IWorkShift, WorkShift } from '../work-shift.model';
import { WorkShiftService } from '../service/work-shift.service';

@Injectable({ providedIn: 'root' })
export class WorkShiftRoutingResolveService implements Resolve<IWorkShift> {
  constructor(protected service: WorkShiftService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IWorkShift> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((workShift: HttpResponse<WorkShift>) => {
          if (workShift.body) {
            return of(workShift.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new WorkShift());
  }
}
