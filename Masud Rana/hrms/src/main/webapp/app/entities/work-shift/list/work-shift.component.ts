import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IWorkShift } from '../work-shift.model';
import { WorkShiftService } from '../service/work-shift.service';
import { WorkShiftDeleteDialogComponent } from '../delete/work-shift-delete-dialog.component';

@Component({
  selector: 'hrms-work-shift',
  templateUrl: './work-shift.component.html',
})
export class WorkShiftComponent implements OnInit {
  workShifts?: IWorkShift[];
  isLoading = false;

  constructor(protected workShiftService: WorkShiftService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.workShiftService.query().subscribe(
      (res: HttpResponse<IWorkShift[]>) => {
        this.isLoading = false;
        this.workShifts = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IWorkShift): number {
    return item.id!;
  }

  delete(workShift: IWorkShift): void {
    const modalRef = this.modalService.open(WorkShiftDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.workShift = workShift;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
