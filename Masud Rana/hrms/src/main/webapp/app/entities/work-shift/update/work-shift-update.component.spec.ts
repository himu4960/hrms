jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { WorkShiftService } from '../service/work-shift.service';
import { IWorkShift, WorkShift } from '../work-shift.model';

import { WorkShiftUpdateComponent } from './work-shift-update.component';

describe('Component Tests', () => {
  describe('WorkShift Management Update Component', () => {
    let comp: WorkShiftUpdateComponent;
    let fixture: ComponentFixture<WorkShiftUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let workShiftService: WorkShiftService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [WorkShiftUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(WorkShiftUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WorkShiftUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      workShiftService = TestBed.inject(WorkShiftService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const workShift: IWorkShift = { id: 456 };

        activatedRoute.data = of({ workShift });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(workShift));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const workShift = { id: 123 };
        spyOn(workShiftService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ workShift });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: workShift }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(workShiftService.update).toHaveBeenCalledWith(workShift);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const workShift = new WorkShift();
        spyOn(workShiftService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ workShift });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: workShift }));
        saveSubject.complete();

        // THEN
        expect(workShiftService.create).toHaveBeenCalledWith(workShift);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const workShift = { id: 123 };
        spyOn(workShiftService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ workShift });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(workShiftService.update).toHaveBeenCalledWith(workShift);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
