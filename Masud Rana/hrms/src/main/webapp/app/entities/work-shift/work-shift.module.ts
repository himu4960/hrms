import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { WorkShiftRoutingModule } from './route/work-shift-routing.module';

import { WorkShiftComponent } from './list/work-shift.component';
import { WorkShiftUpdateComponent } from './update/work-shift-update.component';
import { WorkShiftDeleteDialogComponent } from './delete/work-shift-delete-dialog.component';

@NgModule({
  imports: [SharedModule, WorkShiftRoutingModule],
  declarations: [WorkShiftComponent, WorkShiftUpdateComponent, WorkShiftDeleteDialogComponent],
  entryComponents: [WorkShiftDeleteDialogComponent],
})
export class WorkShiftModule {}
