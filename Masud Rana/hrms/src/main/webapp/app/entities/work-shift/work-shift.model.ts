import * as dayjs from 'dayjs';

import { IBranch } from '../branch/branch.model';
import { ICompany } from '../../company/company.model';
import { IDesignation } from '../designation/designation.model';

export interface IWorkShift {
  id?: number;
  company_id?: number;
  branch_id?: number;
  title?: string | null;
  designation_id?: number;
  is_published?: boolean | null;
  published_at?: dayjs.Dayjs | null;
  published_by?: number | null;
  start_date?: dayjs.Dayjs;
  end_date?: dayjs.Dayjs;
  start_time?: dayjs.Dayjs;
  end_time?: dayjs.Dayjs;
  total_work_shift_duration?: number;
  deleted_at?: dayjs.Dayjs | null;
  deleted_by?: number | null;
  designation?: IDesignation | null;
  company?: ICompany | null;
  branch?: IBranch | null;
}

export class WorkShift implements IWorkShift {
  constructor(
    public id?: number,
    public company_id?: number,
    public branch_id?: number,
    public title?: string | null,
    public designation_id?: number,
    public is_published?: boolean | null,
    public published_at?: dayjs.Dayjs | null,
    public published_by?: number | null,
    public start_date?: dayjs.Dayjs,
    public end_date?: dayjs.Dayjs,
    public start_time?: dayjs.Dayjs,
    public end_time?: dayjs.Dayjs,
    public total_work_shift_duration?: number,
    public deleted_at?: dayjs.Dayjs | null,
    public deleted_by?: number | null
  ) {
    this.is_published = this.is_published ?? false;
  }
}

export function getWorkShiftIdentifier(workShift: IWorkShift): number | undefined {
  return workShift.id;
}
