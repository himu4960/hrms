import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IWorkShift, WorkShift } from '../work-shift.model';
import { WorkShiftService } from '../service/work-shift.service';

@Component({
  selector: 'hrms-work-shift-update',
  templateUrl: './work-shift-update.component.html',
})
export class WorkShiftUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    company_id: [null, [Validators.required, Validators.max(11)]],
    branch_id: [null, [Validators.required, Validators.max(11)]],
    title: [null, [Validators.maxLength(255)]],
    designation_id: [null, [Validators.required, Validators.max(11)]],
    is_published: [],
    published_at: [],
    published_by: [],
    start_date: [null, [Validators.required]],
    end_date: [null, [Validators.required]],
    start_time: [null, [Validators.required]],
    end_time: [null, [Validators.required]],
    total_work_shift_duration: [null, [Validators.required]],
  });

  constructor(protected workShiftService: WorkShiftService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ workShift }) => {
      if (workShift.id === undefined) {
        const today = dayjs().startOf('day');
        workShift.start_date = today;
        workShift.end_date = today;
      }

      this.updateForm(workShift);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const workShift = this.createFromForm();
    if (workShift.id !== undefined) {
      this.subscribeToSaveResponse(this.workShiftService.update(workShift));
    } else {
      this.subscribeToSaveResponse(this.workShiftService.create(workShift));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWorkShift>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(workShift: IWorkShift): void {
    this.editForm.patchValue({
      id: workShift.id,
      company_id: workShift.company_id,
      branch_id: workShift.branch_id,
      title: workShift.title,
      designation_id: workShift.designation_id,
      is_published: workShift.is_published,
      published_at: workShift.published_at,
      published_by: workShift.published_by,
      start_time: workShift.start_time ? workShift.start_time.format(DATE_TIME_FORMAT) : null,
      end_time: workShift.end_time ? workShift.end_time.format(DATE_TIME_FORMAT) : null,
      start_date: workShift.start_date,
      end_date: workShift.end_date,
      total_work_shift_duration: workShift.total_work_shift_duration,
    });
  }

  protected createFromForm(): IWorkShift {
    return {
      ...new WorkShift(),
      id: this.editForm.get(['id'])!.value,
      company_id: this.editForm.get(['company_id'])!.value,
      branch_id: this.editForm.get(['branch_id'])!.value,
      title: this.editForm.get(['title'])!.value,
      designation_id: this.editForm.get(['designation_id'])!.value,
      is_published: this.editForm.get(['is_published'])!.value,
      published_at: this.editForm.get(['published_at'])!.value,
      published_by: this.editForm.get(['published_by'])!.value,
      start_time: this.editForm.get(['start_time'])!.value ? dayjs(this.editForm.get(['start_time'])!.value, DATE_TIME_FORMAT) : undefined,
      end_time: this.editForm.get(['end_time'])!.value ? dayjs(this.editForm.get(['end_time'])!.value, DATE_TIME_FORMAT) : undefined,
      start_date: this.editForm.get(['start_date'])!.value,
      end_date: this.editForm.get(['end_date'])!.value,
      total_work_shift_duration: this.editForm.get(['total_work_shift_duration'])!.value,
    };
  }
}
