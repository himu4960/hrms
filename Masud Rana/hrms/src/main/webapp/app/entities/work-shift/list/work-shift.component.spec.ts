import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { WorkShiftService } from '../service/work-shift.service';

import { WorkShiftComponent } from './work-shift.component';

describe('Component Tests', () => {
  describe('WorkShift Management Component', () => {
    let comp: WorkShiftComponent;
    let fixture: ComponentFixture<WorkShiftComponent>;
    let service: WorkShiftService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [WorkShiftComponent],
      })
        .overrideTemplate(WorkShiftComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(WorkShiftComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(WorkShiftService);

      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.workShifts?.[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
