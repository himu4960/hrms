import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { HYPHEN_SEPARATED_DATE } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IWorkShift, getWorkShiftIdentifier } from '../work-shift.model';

export type EntityResponseType = HttpResponse<IWorkShift>;
export type EntityArrayResponseType = HttpResponse<IWorkShift[]>;

@Injectable({ providedIn: 'root' })
export class WorkShiftService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('work-shifts');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(workShift: IWorkShift): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(workShift);
    return this.http
      .post<IWorkShift>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(workShift: IWorkShift): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(workShift);
    return this.http
      .put<IWorkShift>(`${this.resourceUrl}/${getWorkShiftIdentifier(workShift) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(workShift: IWorkShift): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(workShift);
    return this.http
      .patch<IWorkShift>(`${this.resourceUrl}/${getWorkShiftIdentifier(workShift) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IWorkShift>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IWorkShift[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addWorkShiftToCollectionIfMissing(
    workShiftCollection: IWorkShift[],
    ...workShiftsToCheck: (IWorkShift | null | undefined)[]
  ): IWorkShift[] {
    const workShifts: IWorkShift[] = workShiftsToCheck.filter(isPresent);
    if (workShifts.length > 0) {
      const workShiftCollectionIdentifiers = workShiftCollection.map(workShiftItem => getWorkShiftIdentifier(workShiftItem)!);
      const workShiftsToAdd = workShifts.filter(workShiftItem => {
        const workShiftIdentifier = getWorkShiftIdentifier(workShiftItem);
        if (workShiftIdentifier == null || workShiftCollectionIdentifiers.includes(workShiftIdentifier)) {
          return false;
        }
        workShiftCollectionIdentifiers.push(workShiftIdentifier);
        return true;
      });
      return [...workShiftsToAdd, ...workShiftCollection];
    }
    return workShiftCollection;
  }

  protected convertDateFromClient(workShift: IWorkShift): IWorkShift {
    return Object.assign({}, workShift, {
      published_at: workShift.published_at?.isValid() ? workShift.published_at.format(HYPHEN_SEPARATED_DATE) : undefined,
      start_date: workShift.start_date?.isValid() ? workShift.start_date.toJSON() : undefined,
      end_date: workShift.end_date?.isValid() ? workShift.end_date.toJSON() : undefined,
      start_time: workShift.start_time?.isValid() ? workShift.start_time.format(HYPHEN_SEPARATED_DATE) : undefined,
      end_time: workShift.end_time?.isValid() ? workShift.end_time.format(HYPHEN_SEPARATED_DATE) : undefined,
      deleted_at: workShift.deleted_at?.isValid() ? workShift.deleted_at.format(HYPHEN_SEPARATED_DATE) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.published_at = res.body.published_at ? dayjs(res.body.published_at) : undefined;
      res.body.start_date = res.body.start_date ? dayjs(res.body.start_date) : undefined;
      res.body.end_date = res.body.end_date ? dayjs(res.body.end_date) : undefined;
      res.body.start_time = res.body.start_time ? dayjs(res.body.start_time) : undefined;
      res.body.end_time = res.body.end_time ? dayjs(res.body.end_time) : undefined;
      res.body.deleted_at = res.body.deleted_at ? dayjs(res.body.deleted_at) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((workShift: IWorkShift) => {
        workShift.published_at = workShift.published_at ? dayjs(workShift.published_at) : undefined;
        workShift.start_date = workShift.start_date ? dayjs(workShift.start_date) : undefined;
        workShift.end_date = workShift.end_date ? dayjs(workShift.end_date) : undefined;
        workShift.start_time = workShift.start_time ? dayjs(workShift.start_time) : undefined;
        workShift.end_time = workShift.end_time ? dayjs(workShift.end_time) : undefined;
        workShift.deleted_at = workShift.deleted_at ? dayjs(workShift.deleted_at) : undefined;
      });
    }
    return res;
  }
}
