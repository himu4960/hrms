import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';
import { WorkShiftRoutingResolveService } from './work-shift-routing-resolve.service';

import { WorkShiftComponent } from '../list/work-shift.component';
import { WorkShiftUpdateComponent } from '../update/work-shift-update.component';

const workShiftRoute: Routes = [
  {
    path: '',
    component: WorkShiftComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'new',
    component: WorkShiftUpdateComponent,
    resolve: {
      workShift: WorkShiftRoutingResolveService,
    },
    canActivate: [AuthGuard],
  },
  {
    path: ':id/edit',
    component: WorkShiftUpdateComponent,
    resolve: {
      workShift: WorkShiftRoutingResolveService,
    },
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(workShiftRoute)],
  exports: [RouterModule],
})
export class WorkShiftRoutingModule {}
