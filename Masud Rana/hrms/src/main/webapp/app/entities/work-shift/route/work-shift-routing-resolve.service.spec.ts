jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IWorkShift, WorkShift } from '../work-shift.model';
import { WorkShiftService } from '../service/work-shift.service';

import { WorkShiftRoutingResolveService } from './work-shift-routing-resolve.service';

describe('Service Tests', () => {
  describe('WorkShift routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: WorkShiftRoutingResolveService;
    let service: WorkShiftService;
    let resultWorkShift: IWorkShift | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(WorkShiftRoutingResolveService);
      service = TestBed.inject(WorkShiftService);
      resultWorkShift = undefined;
    });

    describe('resolve', () => {
      it('should return IWorkShift returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultWorkShift = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultWorkShift).toEqual({ id: 123 });
      });

      it('should return new IWorkShift if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultWorkShift = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultWorkShift).toEqual(new WorkShift());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultWorkShift = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultWorkShift).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
