import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IWorkShift } from '../work-shift.model';
import { WorkShiftService } from '../service/work-shift.service';

@Component({
  templateUrl: './work-shift-delete-dialog.component.html',
})
export class WorkShiftDeleteDialogComponent {
  workShift?: IWorkShift;

  constructor(protected workShiftService: WorkShiftService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.workShiftService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
