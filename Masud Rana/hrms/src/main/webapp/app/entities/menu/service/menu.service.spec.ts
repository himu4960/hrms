import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IMenu, Menu } from '../menu.model';

import { MenuService } from './menu.service';

describe('Service Tests', () => {
  describe('Menu Service', () => {
    let service: MenuService;
    let httpMock: HttpTestingController;
    let elemDefault: IMenu;
    let expectedResult: IMenu | IMenu[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(MenuService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        name: 'Home',
        icon: 'fa-home',
        end_point: '/home',
        router_link: 'http://localhost:9000',
        permissions: 'can_add',
        parent_id: 0,
        position: 0,
        is_active: true,
        is_deleted: false,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Menu', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Menu()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Menu', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'About',
            icon: 'fa-about',
            end_point: '/about',
            router_link: 'http://localhost:9000/about',
            permissions: 'can_edit',
            parent_id: 1,
            position: 1,
            is_active: true,
            is_deleted: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Menu', () => {
        const patchObject = Object.assign(
          {
            icon: 'fa-icon',
            router_link: '/about',
            permissions: 'cab_view',
          },
          new Menu()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Menu', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'About',
            icon: 'fa-icon',
            end_point: '/about',
            router_link: '/about',
            permissions: 'cab_view',
            parent_id: 1,
            position: 1,
            is_active: true,
            is_deleted: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Menu', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
