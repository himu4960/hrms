import { NgModule } from '@angular/core';

import { MenuComponent } from './list/menu.component';
import { MenuDetailComponent } from './detail/menu-detail.component';
import { MenuUpdateComponent } from './update/menu-update.component';
import { MenuDeleteDialogComponent } from './delete/menu-delete-dialog.component';

import { SharedModule } from 'app/shared/shared.module';
import { MenuRoutingModule } from './route/menu-routing.module';
import { PermissionTypeRoutingModule } from '../permission-type/route/permission-type-routing.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
  imports: [NgMultiSelectDropDownModule.forRoot(), SharedModule, MenuRoutingModule, PermissionTypeRoutingModule],
  declarations: [MenuComponent, MenuDetailComponent, MenuUpdateComponent, MenuDeleteDialogComponent],
  entryComponents: [MenuDeleteDialogComponent],
})
export class MenuModule {}
