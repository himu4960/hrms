import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { createRequestOption } from 'app/core/request/request-util';

import { ApplicationConfigService } from 'app/core/config/application-config.service';

import { IPermissionType, PermissionType } from 'app/entities/role/permission-types.model';
import { IMenu, getMenuIdentifier } from '../menu.model';

export type EntityResponseType = HttpResponse<IMenu>;
export type EntityArrayResponseType = HttpResponse<IMenu[]>;

@Injectable({ providedIn: 'root' })
export class MenuService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('menus');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(menu: IMenu): Observable<EntityResponseType> {
    return this.http.post<IMenu>(this.resourceUrl, menu, { observe: 'response' });
  }

  update(menu: IMenu): Observable<EntityResponseType> {
    return this.http.put<IMenu>(`${this.resourceUrl}/${getMenuIdentifier(menu) as number}`, menu, { observe: 'response' });
  }

  partialUpdate(menu: IMenu): Observable<EntityResponseType> {
    return this.http.patch<IMenu>(`${this.resourceUrl}/${getMenuIdentifier(menu) as number}`, menu, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMenu>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMenu[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  queryPermission(menuList?: IMenu[]): Observable<EntityArrayResponseType> {
    return this.http
      .get<IMenu[]>(this.resourceUrl, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.buildMenuPermission(res, menuList)))
      .pipe(map((res: EntityArrayResponseType) => this.buildMenu(res)));
  }

  getMenuTree(menus: IMenu[]): IMenu[] {
    if (menus) {
      menus = this.createMenuTree(menus);
    }
    return menus;
  }

  protected buildMenuPermission(res: EntityArrayResponseType, menuList?: IMenu[]): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((response: any, index: number) => {
        if (response.permissions) {
          response.permission_type = JSON.parse(response.permissions);
          response.permission_type.forEach((permission: PermissionType) => {
            response[permission.key] = false;
          });
        }

        if (response.permissions) {
          const obj: any | undefined = menuList?.filter((menu: IMenu) => menu.id === response.id)[0];
          for (const key in obj) {
            const permission_type = response.permission_type?.filter((p: IPermissionType) => p.key === key)[0];
            if (key === permission_type?.key) {
              response[permission_type.key] = obj[key];
            }
          }
        }
      });
    }
    return res;
  }

  protected buildMenu(res: any): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((menus: IMenu) => {
        menus.children = [];
      });
      res.body = this.createMenuTree(res.body ?? []);
    }
    const respons: EntityArrayResponseType = res;
    return respons;
  }

  private createMenuTree: any = (input: any[]) => {
    const mapMenu = new Map(input.map(item => [item.id, item]));
    const tree: IMenu[] = [];
    for (const item of input) {
      if (mapMenu.has(item.parent_id)) {
        mapMenu.get(item.parent_id).children.push(mapMenu.get(item.id));
      } else {
        tree.push(mapMenu.get(item.id));
      }
    }
    return tree;
  };
}
