import { PermissionType } from '../role/permission-types.model';

export interface IMenu {
  id?: number;
  name?: string;
  translate_name?: string;
  icon?: string | null;
  end_point?: string;
  router_link?: string;
  settings_model_name?: string;
  settings_field_name?: string;
  is_for_employee?: boolean;
  permissions?: string | null;
  permission_type?: PermissionType[] | undefined;
  parent_id?: number | null;
  position?: number;
  is_menu?: boolean;
  can_add?: boolean;
  can_delete?: boolean;
  can_edit?: boolean;
  can_view?: boolean;
  can_change_password?: boolean;
  can_approve?: boolean;
  can_reject?: boolean;
  can_publish?: boolean;
  children?: IMenu[] | undefined;
  is_active?: boolean;
  is_deleted?: boolean;
  is_collapsed?: boolean;
}

export class Menu implements IMenu {
  constructor(
    public id?: number,
    public name?: string,
    public translate_name?: string,
    public icon?: string | null,
    public end_point?: string,
    public router_link?: string,
    public settings_model_name?: string,
    public settings_field_name?: string,
    public is_for_employee?: boolean,
    public permissions?: string | null,
    public permission_type?: PermissionType[] | undefined,
    public parent_id?: number | null,
    public position?: number,
    public is_menu?: boolean,
    public can_add?: boolean,
    public can_delete?: boolean,
    public can_edit?: boolean,
    public can_view?: boolean,
    public can_change_password?: boolean,
    public children?: IMenu[] | undefined,
    public is_active?: boolean,
    public is_deleted?: boolean
  ) {
    this.is_for_employee = this.is_for_employee ?? false;
    this.is_active = this.is_active ?? true;
    this.is_deleted = this.is_deleted ?? false;
    this.is_menu = this.is_menu ?? false;
    this.can_add = this.can_add ?? false;
    this.can_edit = this.can_edit ?? false;
    this.can_delete = this.can_delete ?? false;
    this.can_view = this.can_view ?? false;
    this.can_change_password = this.can_change_password ?? false;
  }
}

export function getMenuIdentifier(menu: IMenu): number | undefined {
  return menu.id;
}
