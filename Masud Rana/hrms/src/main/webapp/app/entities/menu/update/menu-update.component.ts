import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { finalize, switchMap } from 'rxjs/operators';

import { IDropdownSettings } from 'ng-multiselect-dropdown';

import { IMenu, Menu } from '../menu.model';

import { MenuService } from '../service/menu.service';
import { PermissionTypeService } from 'app/entities/permission-type/service/permission-type.service';

import { IpermissionTypeDto } from 'app/entities/permission-type/permission-type.dto';

import { noWhitespaceValidator } from 'app/shared/validators/no-whitespace.validator';

@Component({
  selector: 'hrms-menu-update',
  templateUrl: './menu-update.component.html',
})
export class MenuUpdateComponent implements OnInit {
  isSaving = false;
  dropdownList: IpermissionTypeDto[] = [];
  selectedItems: any[] = [];
  dropdownSettings: IDropdownSettings = {};

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(50)]],
    translate_name: [null, [Validators.required, Validators.maxLength(150)]],
    icon: [],
    end_point: [null, [Validators.maxLength(50), noWhitespaceValidator]],
    router_link: [null, [Validators.maxLength(50), noWhitespaceValidator]],
    permissions: [this.selectedItems],
    parent_id: [null, [Validators.maxLength(11)]],
    position: [null, [Validators.maxLength(4)]],
    is_menu: [null],
    is_active: [null],
    is_deleted: [null],
  });

  constructor(
    protected menuService: MenuService,
    protected permissionTypeService: PermissionTypeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'key',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
    };
  }

  ngOnInit(): void {
    this.activatedRoute.data
      .pipe(
        switchMap(({ menu }) => {
          this.updateForm(menu);
          return this.permissionTypeService.query();
        })
      )
      .subscribe((res: any) => {
        this.dropdownList = res.body;
      });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const menu = this.createFromForm();
    menu.permissions = JSON.stringify(menu.permissions);
    if (menu.id !== undefined) {
      this.subscribeToSaveResponse(this.menuService.update(menu));
    } else {
      this.subscribeToSaveResponse(this.menuService.create(menu));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMenu>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(menu: IMenu): void {
    this.editForm.patchValue({
      id: menu.id,
      name: menu.name,
      translate_name: menu.translate_name,
      icon: menu.icon,
      end_point: menu.end_point,
      router_link: menu.router_link,
      permissions: menu.permissions ? JSON.parse(menu.permissions) : menu.permissions,
      parent_id: menu.parent_id,
      position: menu.position,
      is_menu: menu.is_menu,
      is_active: menu.is_active,
      is_deleted: menu.is_deleted,
    });
  }

  protected createFromForm(): IMenu {
    return {
      ...new Menu(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      translate_name: this.editForm.get(['translate_name'])!.value,
      icon: this.editForm.get(['icon'])!.value,
      end_point: this.editForm.get(['end_point'])!.value,
      router_link: this.editForm.get(['router_link'])!.value,
      permissions: this.editForm.get(['permissions'])!.value,
      parent_id: this.editForm.get(['parent_id'])!.value,
      position: this.editForm.get(['position'])!.value,
      is_menu: this.editForm.get(['is_menu'])!.value,
      is_active: this.editForm.get(['is_active'])!.value,
      is_deleted: this.editForm.get(['is_deleted'])!.value,
    };
  }
}
