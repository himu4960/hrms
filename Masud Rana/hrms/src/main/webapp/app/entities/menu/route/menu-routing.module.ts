import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';
import { MenuRoutingResolveService } from './menu-routing-resolve.service';
import { SuperAdminRoutePermissionGuard } from 'app/core/auth/guards/super-admin-route-permission.service';

import { MenuComponent } from '../list/menu.component';
import { MenuDetailComponent } from '../detail/menu-detail.component';
import { MenuUpdateComponent } from '../update/menu-update.component';

const menuRoute: Routes = [
  {
    path: '',
    component: MenuComponent,
    canActivate: [AuthGuard, SuperAdminRoutePermissionGuard],
  },
  {
    path: ':id/view',
    component: MenuDetailComponent,
    resolve: {
      menu: MenuRoutingResolveService,
    },
    canActivate: [AuthGuard, SuperAdminRoutePermissionGuard],
  },
  {
    path: 'new',
    component: MenuUpdateComponent,
    resolve: {
      menu: MenuRoutingResolveService,
    },
    canActivate: [AuthGuard, SuperAdminRoutePermissionGuard],
  },
  {
    path: ':id/edit',
    component: MenuUpdateComponent,
    resolve: {
      menu: MenuRoutingResolveService,
    },
    canActivate: [AuthGuard, SuperAdminRoutePermissionGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(menuRoute)],
  exports: [RouterModule],
})
export class MenuRoutingModule {}
