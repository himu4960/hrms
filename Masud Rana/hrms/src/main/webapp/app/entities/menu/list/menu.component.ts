import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { EModalReasonType } from 'app/shared/enum';

import { IMenu } from '../menu.model';

import { MenuService } from '../service/menu.service';
import { AclService } from 'app/core/auth/acl.service';

import { MenuDeleteDialogComponent } from '../delete/menu-delete-dialog.component';

@Component({
  selector: 'hrms-menu',
  templateUrl: './menu.component.html',
})
export class MenuComponent implements OnInit {
  menus: any[] = [];
  isLoading = false;

  constructor(protected menuService: MenuService, public acl: AclService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.menuService.query().subscribe(
      (res: any) => {
        this.isLoading = false;

        this.menus = res.body ?? [];
        if (res.body.length) {
          for (let i = 0; i < res.body.length; i++) {
            const permission = JSON.parse(res.body[i].permissions);

            this.menus[i].permissions = permission;
          }
        }
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IMenu): number {
    return item.id!;
  }

  delete(menu: IMenu): void {
    const modalRef = this.modalService.open(MenuDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.menu = menu;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.DELETED) {
        this.loadAll();
      }
    });
  }
}
