jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { of } from 'rxjs';

import { MenuService } from '../service/menu.service';

import { IMenu } from '../menu.model';

import { MenuUpdateComponent } from './menu-update.component';

describe('Component Tests', () => {
  describe('Menu Management Update Component', () => {
    let comp: MenuUpdateComponent;
    let fixture: ComponentFixture<MenuUpdateComponent>;
    let activatedRoute: ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [MenuUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(MenuUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MenuUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      TestBed.inject(MenuService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const menu: IMenu = { id: 456 };

        activatedRoute.data = of({ menu });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(menu));
      });
    });

    // describe('save', () => {
    //   it('Should call update service on save for existing entity', () => {
    //     // GIVEN
    //     const saveSubject = new Subject();
    //     const menu = { id: 123 };
    //     spyOn(menuService, 'update').and.returnValue(saveSubject);
    //     spyOn(comp, 'previousState');
    //     activatedRoute.data = of({ menu });
    //     comp.ngOnInit();

    //     // WHEN
    //     comp.save();
    //     expect(comp.isSaving).toEqual(true);
    //     saveSubject.next(new HttpResponse({ body: menu }));
    //     saveSubject.complete();

    //     // THEN
    //     expect(comp.previousState).toHaveBeenCalled();
    //     expect(menuService.update).toHaveBeenCalledWith(menu);
    //     expect(comp.isSaving).toEqual(false);
    //   });

    //   it('Should call create service on save for new entity', () => {
    //     // GIVEN
    //     const saveSubject = new Subject();
    //     const menu = new Menu();
    //     spyOn(menuService, 'create').and.returnValue(saveSubject);
    //     spyOn(comp, 'previousState');
    //     activatedRoute.data = of({ menu });
    //     comp.ngOnInit();

    //     // WHEN
    //     comp.save();
    //     expect(comp.isSaving).toEqual(true);
    //     saveSubject.next(new HttpResponse({ body: menu }));
    //     saveSubject.complete();

    //     // THEN
    //     expect(menuService.create).toHaveBeenCalledWith(menu);
    //     expect(comp.isSaving).toEqual(false);
    //     expect(comp.previousState).toHaveBeenCalled();
    //   });

    //   it('Should set isSaving to false on error', () => {
    //     // GIVEN
    //     const saveSubject = new Subject();
    //     const menu = { id: 123 };
    //     spyOn(menuService, 'update').and.returnValue(saveSubject);
    //     spyOn(comp, 'previousState');
    //     activatedRoute.data = of({ menu });
    //     comp.ngOnInit();

    //     // WHEN
    //     comp.save();
    //     expect(comp.isSaving).toEqual(true);
    //     saveSubject.error('This is an error!');

    //     // THEN
    //     expect(menuService.update).toHaveBeenCalledWith(menu);
    //     expect(comp.isSaving).toEqual(false);
    //     expect(comp.previousState).not.toHaveBeenCalled();
    //   });
    // });
  });
});
