import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { IBreakRuleCondition } from '../break-rule-condition.model';

export type EntityResponseType = HttpResponse<IBreakRuleCondition>;
export type EntityArrayResponseType = HttpResponse<IBreakRuleCondition[]>;

@Injectable({ providedIn: 'root' })
export class BreakRuleConditionService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('break-rules/conditions');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
