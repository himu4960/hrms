import { ICompany } from '../../company/company.model';
import { IBreakRuleCondition } from './break-rule-condition.model';

export interface IBreakRule {
  id?: number;
  name?: string;
  company_id?: number;
  rule_description?: string;
  break_rule_conditions?: IBreakRuleCondition[];
  company?: ICompany | null;
}

export class BreakRule implements IBreakRule {
  constructor(
    public id?: number,
    public name?: string,
    public rule_description?: string,
    public break_rule_conditions?: IBreakRuleCondition[]
  ) {}
}

export function getBreakRuleIdentifier(breakRule: IBreakRule): number | undefined {
  return breakRule.id;
}
