import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMenu } from 'app/entities/menu/menu.model';
import { IBreakRule } from '../break-rule.model';

import { BreakRuleService } from '../service/break-rule.service';

import { BreakRuleDeleteDialogComponent } from '../delete/break-rule-delete-dialog.component';
import { BreakRuleUpdateComponent } from '../update/break-rule-update.component';

import { EModalReasonType } from 'app/shared/enum';
import { locationSideMenus } from 'app/shared/menus';
import { EBreakRuleConditionType } from '../enum/break-rule-condition.enum';
import { ETimeType } from './../../../shared/pipes/time/time-type.enum';

@Component({
  selector: 'hrms-break-rule',
  templateUrl: './break-rule.component.html',
})
export class BreakRuleComponent implements OnInit {
  public isCollapsed: boolean[] = [];
  breakRules?: IBreakRule[];
  isLoading = false;
  EBreakRuleConditionType = EBreakRuleConditionType;
  ETimeType = ETimeType;
  sideMenus: IMenu[] = locationSideMenus;

  constructor(protected breakRuleService: BreakRuleService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.breakRuleService.query().subscribe(
      (res: HttpResponse<IBreakRule[]>) => {
        this.isLoading = false;
        this.breakRules = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  createBreakRuleModal(breakRule?: IBreakRule): void {
    const modalRef = this.modalService.open(BreakRuleUpdateComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.breakRule = breakRule;
    modalRef.closed.subscribe(() => {
      this.loadAll();
    });
  }

  trackId(index: number, item: IBreakRule): number {
    return item.id!;
  }

  delete(breakRule: IBreakRule): void {
    const modalRef = this.modalService.open(BreakRuleDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.breakRule = breakRule;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.DELETED) {
        this.loadAll();
      }
    });
  }
}
