import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IBreakRule, BreakRule } from '../break-rule.model';

import { BreakRuleService } from './break-rule.service';

describe('Service Tests', () => {
  describe('BreakRule Service', () => {
    let service: BreakRuleService;
    let httpMock: HttpTestingController;
    let elemDefault: IBreakRule;
    let expectedResult: IBreakRule | IBreakRule[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(BreakRuleService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        name: 'AAAAAAA',
        rule_description: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a BreakRule', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new BreakRule()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a BreakRule', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'BBBBBB',
            rule_description: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a BreakRule', () => {
        const patchObject = Object.assign(
          {
            name: 'BBBBBB',
            rule_description: 'BBBBBB',
          },
          new BreakRule()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of BreakRule', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'BBBBBB',
            rule_description: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a BreakRule', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addBreakRuleToCollectionIfMissing', () => {
        it('should add a BreakRule to an empty array', () => {
          const breakRule: IBreakRule = { id: 123 };
          expectedResult = service.addBreakRuleToCollectionIfMissing([], breakRule);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(breakRule);
        });

        it('should not add a BreakRule to an array that contains it', () => {
          const breakRule: IBreakRule = { id: 123 };
          const breakRuleCollection: IBreakRule[] = [
            {
              ...breakRule,
            },
            { id: 456 },
          ];
          expectedResult = service.addBreakRuleToCollectionIfMissing(breakRuleCollection, breakRule);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a BreakRule to an array that doesn't contain it", () => {
          const breakRule: IBreakRule = { id: 123 };
          const breakRuleCollection: IBreakRule[] = [{ id: 456 }];
          expectedResult = service.addBreakRuleToCollectionIfMissing(breakRuleCollection, breakRule);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(breakRule);
        });

        it('should add only unique BreakRule to an array', () => {
          const breakRuleArray: IBreakRule[] = [{ id: 123 }, { id: 456 }, { id: 8918 }];
          const breakRuleCollection: IBreakRule[] = [{ id: 123 }];
          expectedResult = service.addBreakRuleToCollectionIfMissing(breakRuleCollection, ...breakRuleArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const breakRule: IBreakRule = { id: 123 };
          const breakRule2: IBreakRule = { id: 456 };
          expectedResult = service.addBreakRuleToCollectionIfMissing([], breakRule, breakRule2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(breakRule);
          expect(expectedResult).toContain(breakRule2);
        });

        it('should accept null and undefined values', () => {
          const breakRule: IBreakRule = { id: 123 };
          expectedResult = service.addBreakRuleToCollectionIfMissing([], null, breakRule, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(breakRule);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
