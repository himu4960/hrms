import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IBreakRule, BreakRule } from '../break-rule.model';
import { BreakRuleService } from '../service/break-rule.service';

@Injectable({ providedIn: 'root' })
export class BreakRuleRoutingResolveService implements Resolve<IBreakRule> {
  constructor(protected service: BreakRuleService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBreakRule> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((breakRule: HttpResponse<BreakRule>) => {
          if (breakRule.body) {
            return of(breakRule.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BreakRule());
  }
}
