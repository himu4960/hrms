import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IBreakRule, BreakRule } from '../break-rule.model';
import { IBreakRuleCondition } from '../break-rule-condition.model';

import { BreakRuleService } from '../service/break-rule.service';
import { CompanyService } from 'app/company/service/company.service';
import { BreakRuleConditionService } from '../service/break-rule-condition.service';

import { EBreakRuleConditionType } from '../enum/break-rule-condition.enum';

import { timeFormatValidator } from 'app/shared/validators/time-format.validator';
import { breakRuleConditionValidator } from 'app/shared/validators/break-rule-conditions.validator';
import { noWhitespaceValidator } from 'app/shared/validators/no-whitespace.validator';

@Component({
  selector: 'hrms-break-rule-update',
  templateUrl: './break-rule-update.component.html',
})
export class BreakRuleUpdateComponent implements OnInit {
  unPaidBreak = EBreakRuleConditionType.UNPAID_BREAK;
  paidBreak = EBreakRuleConditionType.PAID_BREAK;
  breakRule?: IBreakRule;

  breakRuleForm: FormGroup = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(255), noWhitespaceValidator]],
    rule_description: [null],
    break_rule_conditions: this.fb.array([this.newBreakRuleCondition()]),
  });

  constructor(
    protected breakRuleService: BreakRuleService,
    protected companyService: CompanyService,
    protected breakRuleConditionService: BreakRuleConditionService,
    protected fb: FormBuilder,
    public activeModal: NgbActiveModal
  ) {}

  ngOnInit(): void {
    if (this.breakRule !== undefined) {
      this.updateForm(this.breakRule);
    }
  }

  get breakRuleConditions(): FormArray {
    return this.breakRuleForm.get('break_rule_conditions') as FormArray;
  }

  cancel(): void {
    this.activeModal.dismiss();
  }

  newBreakRuleCondition(values?: any): FormGroup {
    return this.fb.group(
      {
        id: [values?.id],
        type: [values?.type || null, [Validators.required]],
        min_shift_length: [values?.min_shift_length || null, [Validators.required, timeFormatValidator()]],
        max_shift_length: [values?.max_shift_length || null, [Validators.required, timeFormatValidator()]],
        break_duration: [values?.break_duration || null, [Validators.required]],
        from: [values?.from || null, [Validators.required, timeFormatValidator()]],
        to: [values?.to || null, [Validators.required, timeFormatValidator()]],
        break_starts_at: [values?.break_starts_at || null, [Validators.required, timeFormatValidator()]],
      },
      {
        validators: breakRuleConditionValidator(),
      }
    );
  }

  addBreakRuleCondition(): void {
    this.breakRuleConditions.push(this.newBreakRuleCondition());
  }

  removeBreakRuleCondition(i: number, id: number): void {
    if (id) {
      this.breakRuleConditionService.delete(id).subscribe(() => {
        this.breakRuleConditions.removeAt(i);
      });
    } else {
      this.breakRuleConditions.removeAt(i);
    }
  }

  save(): void {
    if (this.breakRuleForm.value.id && this.breakRuleForm.value.id !== undefined) {
      this.subscribeToSaveResponse(this.breakRuleService.update(this.breakRuleForm.value));
    } else {
      this.subscribeToSaveResponse(this.breakRuleService.create(this.breakRuleForm.value));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBreakRule>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.activeModal.close();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    // Api for inheritance.
  }

  protected updateForm(breakRule: IBreakRule): void {
    this.breakRuleConditions.clear();
    breakRule.break_rule_conditions?.map((condition: IBreakRuleCondition) => {
      const { id, type, max_shift_length, min_shift_length, break_duration, break_starts_at, from, to } = condition;
      this.breakRuleConditions.push(
        this.newBreakRuleCondition({
          id,
          type,
          min_shift_length: this.breakRuleService.convertMinuteToTime(min_shift_length),
          max_shift_length: this.breakRuleService.convertMinuteToTime(max_shift_length),
          break_duration,
          break_starts_at: this.breakRuleService.convertMinuteToTime(break_starts_at),
          from: this.breakRuleService.convertMinuteToTime(from),
          to: this.breakRuleService.convertMinuteToTime(to),
        })
      );
    });
    this.breakRuleForm.patchValue({
      id: breakRule.id,
      name: breakRule.name,
      rule_description: breakRule.rule_description,
    });
  }

  protected createFromForm(): IBreakRule {
    return {
      ...new BreakRule(),
      id: this.breakRuleForm.get(['id'])!.value,
      name: this.breakRuleForm.get(['name'])!.value,
      rule_description: this.breakRuleForm.get(['rule_description'])!.value,
    };
  }
}
