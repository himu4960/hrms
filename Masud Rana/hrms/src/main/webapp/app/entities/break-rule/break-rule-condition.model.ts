import { IBreakRule } from 'app/entities/break-rule/break-rule.model';

export interface IBreakRuleCondition {
  id?: number;
  break_rule_id?: number;
  type?: string;
  min_shift_length?: number;
  max_shift_length?: number;
  break_duration?: number;
  from?: number;
  to?: number;
  break_starts_at?: number;
  breakRule?: IBreakRule | null;
  is_paid?: boolean;
}

export class BreakRuleCondition implements IBreakRuleCondition {
  constructor(
    public id?: number,
    public break_rule_id?: number,
    public type?: string,
    public min_shift_length?: number,
    public max_shift_length?: number,
    public break_duration?: number,
    public from?: number,
    public to?: number,
    public break_starts_at?: number,
    public breakRule?: IBreakRule | null,
    public is_paid?: boolean
  ) {}
}

export function getBreakRuleConditionIdentifier(breakRuleCondition: IBreakRuleCondition): number | undefined {
  return breakRuleCondition.id;
}
