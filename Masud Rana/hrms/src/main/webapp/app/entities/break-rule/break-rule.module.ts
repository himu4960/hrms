import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { BreakRuleRoutingModule } from './route/break-rule-routing.module';
import { CommonSidebarModule } from './../../shared/components/common-sidebar/common-sidebar.module';
import { EmployeeNavigationModule } from './../../employee/components/employee-navigation/employee-navigation.module';

import { BreakRuleComponent } from './list/break-rule.component';
import { BreakRuleDetailComponent } from './detail/break-rule-detail.component';
import { BreakRuleUpdateComponent } from './update/break-rule-update.component';
import { BreakRuleDeleteDialogComponent } from './delete/break-rule-delete-dialog.component';

@NgModule({
  imports: [SharedModule, BreakRuleRoutingModule, CommonSidebarModule, EmployeeNavigationModule],
  declarations: [BreakRuleComponent, BreakRuleDetailComponent, BreakRuleUpdateComponent, BreakRuleDeleteDialogComponent],
  entryComponents: [BreakRuleDeleteDialogComponent],
})
export class BreakRuleModule {}
