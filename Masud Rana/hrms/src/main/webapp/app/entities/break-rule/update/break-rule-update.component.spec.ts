jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';

import { BreakRuleService } from '../service/break-rule.service';

import { BreakRuleUpdateComponent } from './break-rule-update.component';
import { BreakRule } from '../break-rule.model';

describe('Component Tests', () => {
  describe('BreakRule Management Update Component', () => {
    let comp: BreakRuleUpdateComponent;
    let fixture: ComponentFixture<BreakRuleUpdateComponent>;
    let breakRuleService: BreakRuleService;
    let mockActiveModal: NgbActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [BreakRuleUpdateComponent],
        providers: [FormBuilder, NgbActiveModal],
      })
        .overrideTemplate(BreakRuleUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BreakRuleUpdateComponent);
      breakRuleService = TestBed.inject(BreakRuleService);

      comp = fixture.componentInstance;
      mockActiveModal = TestBed.inject(NgbActiveModal);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const breakRule = { id: 123 };
        spyOn(breakRuleService, 'update').and.returnValue(saveSubject);
        comp.ngOnInit();

        // WHEN
        comp.save();
        saveSubject.next(new HttpResponse({ body: breakRule }));
        saveSubject.complete();
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const breakRule = new BreakRule();
        spyOn(breakRuleService, 'create').and.returnValue(saveSubject);
        comp.ngOnInit();

        // WHEN
        comp.save();
        saveSubject.next(new HttpResponse({ body: breakRule }));
        saveSubject.complete();
      });
    });
  });
});
