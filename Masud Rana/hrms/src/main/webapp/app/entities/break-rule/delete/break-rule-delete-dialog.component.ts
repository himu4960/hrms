import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { EModalReasonType } from 'app/shared/enum';

import { IBreakRule } from '../break-rule.model';
import { BreakRuleService } from '../service/break-rule.service';

@Component({
  templateUrl: './break-rule-delete-dialog.component.html',
})
export class BreakRuleDeleteDialogComponent {
  breakRule?: IBreakRule;

  constructor(protected breakRuleService: BreakRuleService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.breakRuleService.delete(id).subscribe(() => {
      this.activeModal.close(EModalReasonType.DELETED);
    });
  }
}
