import { FormBuilder } from '@angular/forms';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IBreakRuleCondition } from '../break-rule-condition.model';

import { BreakRuleConditionService } from './break-rule-condition.service';

describe('Service Tests', () => {
  describe('BreakRuleCondition Service', () => {
    let service: BreakRuleConditionService;
    let httpMock: HttpTestingController;
    let expectedResult: IBreakRuleCondition | IBreakRuleCondition[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [FormBuilder],
      });
      expectedResult = null;
      service = TestBed.inject(BreakRuleConditionService);
      httpMock = TestBed.inject(HttpTestingController);
    });

    describe('Service methods', () => {
      it('should delete a BreakRuleCondition', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
