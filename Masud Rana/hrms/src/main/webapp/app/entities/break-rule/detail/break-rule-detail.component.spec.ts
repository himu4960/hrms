import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BreakRuleDetailComponent } from './break-rule-detail.component';

describe('Component Tests', () => {
  describe('BreakRule Management Detail Component', () => {
    let comp: BreakRuleDetailComponent;
    let fixture: ComponentFixture<BreakRuleDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [BreakRuleDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ breakRule: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(BreakRuleDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BreakRuleDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load breakRule on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.breakRule).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
