import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { BreakRuleService } from '../service/break-rule.service';

import { BreakRuleComponent } from './break-rule.component';

describe('Component Tests', () => {
  describe('BreakRule Management Component', () => {
    let comp: BreakRuleComponent;
    let fixture: ComponentFixture<BreakRuleComponent>;
    let service: BreakRuleService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [BreakRuleComponent],
      })
        .overrideTemplate(BreakRuleComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BreakRuleComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(BreakRuleService);

      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.breakRules?.[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
