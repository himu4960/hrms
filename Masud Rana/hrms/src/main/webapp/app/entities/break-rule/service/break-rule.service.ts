import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { createRequestOption } from 'app/core/request/request-util';

import { IBreakRule, getBreakRuleIdentifier } from '../break-rule.model';

import { ApplicationConfigService } from 'app/core/config/application-config.service';

import { EBreakRuleConditionType } from '../enum/break-rule-condition.enum';

export type EntityResponseType = HttpResponse<IBreakRule>;
export type EntityArrayResponseType = HttpResponse<IBreakRule[]>;

@Injectable({ providedIn: 'root' })
export class BreakRuleService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('break-rules');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(breakRule: IBreakRule): Observable<EntityResponseType> {
    const newBreakRule = this.newBreakRuleMapper(breakRule);
    return this.http.post<IBreakRule>(this.resourceUrl, newBreakRule, { observe: 'response' });
  }

  update(breakRule: IBreakRule): Observable<EntityResponseType> {
    const newBreakRule = this.newBreakRuleMapper(breakRule);
    return this.http.put<IBreakRule>(`${this.resourceUrl}/${getBreakRuleIdentifier(breakRule) as number}`, newBreakRule, {
      observe: 'response',
    });
  }

  partialUpdate(breakRule: IBreakRule): Observable<EntityResponseType> {
    return this.http.patch<IBreakRule>(`${this.resourceUrl}/${getBreakRuleIdentifier(breakRule) as number}`, breakRule, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBreakRule>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBreakRule[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addBreakRuleToCollectionIfMissing(
    breakRuleCollection: IBreakRule[],
    ...breakRulesToCheck: (IBreakRule | null | undefined)[]
  ): IBreakRule[] {
    const breakRules: IBreakRule[] = breakRulesToCheck.filter(isPresent);
    if (breakRules.length > 0) {
      const breakRuleCollectionIdentifiers = breakRuleCollection.map(breakRuleItem => getBreakRuleIdentifier(breakRuleItem)!);
      const breakRulesToAdd = breakRules.filter(breakRuleItem => {
        const breakRuleIdentifier = getBreakRuleIdentifier(breakRuleItem);
        if (breakRuleIdentifier == null || breakRuleCollectionIdentifiers.includes(breakRuleIdentifier)) {
          return false;
        }
        breakRuleCollectionIdentifiers.push(breakRuleIdentifier);
        return true;
      });
      return [...breakRulesToAdd, ...breakRuleCollection];
    }
    return breakRuleCollection;
  }

  convertMinuteToTime(value: number | undefined): string {
    if (value !== undefined) {
      let minutes: number | string = value % 60;
      let hours: number | string = (value - minutes) / 60;
      hours = hours < 10 ? `0${hours}` : hours;
      minutes = minutes < 10 ? `0${minutes}` : minutes;
      return `${hours}:${minutes}`;
    }
    return '';
  }

  convertTimeToMinute(value: any): number {
    if (value) {
      const hours = Number(value.split(':')[0]);
      const minutes = Number(value.split(':')[1]);
      return hours * 60 + minutes;
    }
    return 0;
  }

  private newBreakRuleMapper(newBreakRule: IBreakRule): IBreakRule {
    const newConditions = newBreakRule.break_rule_conditions?.map(condition => {
      const { id, type, min_shift_length, max_shift_length, break_duration, from, to, break_starts_at } = condition;
      return {
        id,
        type,
        min_shift_length: this.convertTimeToMinute(min_shift_length),
        max_shift_length: this.convertTimeToMinute(max_shift_length),
        break_duration,
        from: this.convertTimeToMinute(from),
        to: this.convertTimeToMinute(to),
        break_starts_at: this.convertTimeToMinute(break_starts_at),
        is_paid: type === EBreakRuleConditionType.PAID_BREAK ? true : false,
      };
    });

    return {
      ...newBreakRule,
      break_rule_conditions: newConditions,
    };
  }
}
