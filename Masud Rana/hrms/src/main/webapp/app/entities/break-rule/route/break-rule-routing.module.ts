import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BreakRuleComponent } from '../list/break-rule.component';
import { BreakRuleDetailComponent } from '../detail/break-rule-detail.component';
import { BreakRuleUpdateComponent } from '../update/break-rule-update.component';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';
import { RoutePermissionGuard } from 'app/core/auth/guards/route-permission.service';

import { BreakRuleRoutingResolveService } from './break-rule-routing-resolve.service';

const breakRuleRoute: Routes = [
  {
    path: '',
    component: BreakRuleComponent,
    canActivate: [AuthGuard, RoutePermissionGuard],
  },
  {
    path: ':id/view',
    component: BreakRuleDetailComponent,
    resolve: {
      breakRule: BreakRuleRoutingResolveService,
    },
    canActivate: [AuthGuard, RoutePermissionGuard],
  },
  {
    path: 'new',
    component: BreakRuleUpdateComponent,
    resolve: {
      breakRule: BreakRuleRoutingResolveService,
    },
    canActivate: [AuthGuard, RoutePermissionGuard],
  },
  {
    path: ':id/edit',
    component: BreakRuleUpdateComponent,
    resolve: {
      breakRule: BreakRuleRoutingResolveService,
    },
    canActivate: [AuthGuard, RoutePermissionGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(breakRuleRoute)],
  exports: [RouterModule],
})
export class BreakRuleRoutingModule {}
