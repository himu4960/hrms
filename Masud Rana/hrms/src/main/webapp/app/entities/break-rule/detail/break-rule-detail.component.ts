import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBreakRule } from '../break-rule.model';

@Component({
  selector: 'hrms-break-rule-detail',
  templateUrl: './break-rule-detail.component.html',
})
export class BreakRuleDetailComponent implements OnInit {
  breakRule: IBreakRule | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ breakRule }) => {
      this.breakRule = breakRule;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
