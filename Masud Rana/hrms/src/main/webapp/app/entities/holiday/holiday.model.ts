import * as dayjs from 'dayjs';

export interface IHoliday {
  id?: number;
  date?: number;
  title?: string;
  description?: string | null;
  code?: string;
}

export class Holiday implements IHoliday {
  constructor(public id?: number, public date?: number, public title?: string, public description?: string | null) {}
}

export function getHolidayIdentifier(holiday: IHoliday): number | undefined {
  return holiday.id;
}
