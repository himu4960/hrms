import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HolidayComponent } from '../list/holiday.component';
import { HolidayUpdateComponent } from '../update/holiday-update.component';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';

const holidayRoute: Routes = [
  {
    path: '',
    component: HolidayComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'new',
    component: HolidayUpdateComponent,
    canActivate: [AuthGuard],
  },
  {
    path: ':id/edit',
    component: HolidayUpdateComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(holidayRoute)],
  exports: [RouterModule],
})
export class HolidayRoutingModule {}
