import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as dayjs from 'dayjs';

import { IHoliday, Holiday } from '../holiday.model';

import { HolidayService } from '../service/holiday.service';
import { UtilService } from 'app/shared/service/util.service';

import { DAY_IN_MILLISECOND } from 'app/shared/constant/time.contstant';

@Component({
  selector: 'hrms-holiday-update',
  templateUrl: './holiday-update.component.html',
})
export class HolidayUpdateComponent implements OnInit {
  isSaving = false;

  holiday?: IHoliday;

  editForm = this.fb.group({
    id: [],
    start_date: ['', [Validators.required]],
    end_date: [''],
    title: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(255)]],
    description: [],
  });

  idControl = this.editForm.get('id');
  startDateControl = this.editForm.get('start_date');
  endDateControl = this.editForm.get('end_date');
  titleControl = this.editForm.get('title');
  descriptionControl = this.editForm.get('description');

  constructor(
    protected holidayService: HolidayService,
    protected activatedRoute: ActivatedRoute,
    private activeModal: NgbActiveModal,
    protected fb: FormBuilder,
    protected utilService: UtilService
  ) {}

  ngOnInit(): void {
    if (this.holiday) {
      this.updateForm(this.holiday);
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const holidays = this.createFromData();
    this.subscribeToSaveResponse(this.holidayService.create(holidays));
  }

  update(): void {
    this.isSaving = true;
    const holiday = this.editFromData();
    this.subscribeToSaveResponse(this.holidayService.update(holiday));
  }

  public cancel(): void {
    this.activeModal.dismiss();
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHoliday> | HttpResponse<IHoliday[]>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.activeModal.close();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(holiday: any): void {
    this.editForm.patchValue({
      id: holiday.id,
      title: holiday.title,
      start_date: holiday.date?.length ? dayjs(holiday.date[0]) : null,
      end_date: holiday.date?.length ? dayjs(holiday.date[holiday.date?.length - 1]) : null,
      description: holiday.description,
    });
  }

  protected createFromData(): IHoliday[] {
    const totalDays =
      this.utilService.getDateDifferenceInDays(new Date(this.endDateControl!.value), new Date(this.startDateControl!.value)) + 1;
    const holidayList = [];
    const code = this.utilService.generateRandomString(10);

    for (let i = 0; i < totalDays; i++) {
      const date = new Date(this.startDateControl!.value).getTime() + DAY_IN_MILLISECOND * i;
      holidayList.push({
        id: this.idControl!.value,
        date: date,
        title: this.titleControl!.value,
        description: this.descriptionControl!.value,
        code,
      });
    }
    return holidayList;
  }

  protected editFromData(): IHoliday {
    const date = this.editForm.get(['start_date'])!.value ? new Date(this.editForm.get(['start_date'])!.value).getTime() : undefined;
    return {
      ...new Holiday(),
      id: this.idControl!.value,
      date: date,
      title: this.titleControl!.value,
      description: this.descriptionControl!.value,
    };
  }
}
