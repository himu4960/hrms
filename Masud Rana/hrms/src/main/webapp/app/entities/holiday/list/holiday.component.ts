import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IHoliday } from '../holiday.model';
import { EModalReasonType } from 'app/shared/enum';

import { HolidayService } from '../service/holiday.service';

import { HolidayDeleteDialogComponent } from '../delete/holiday-delete-dialog.component';
import { HolidayUpdateComponent } from '../update/holiday-update.component';

@Component({
  selector: 'hrms-holiday',
  templateUrl: './holiday.component.html',
})
export class HolidayComponent implements OnInit {
  holidays!: any[];
  isLoading = false;

  constructor(protected holidayService: HolidayService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.holidayService.query().subscribe(
      (res: HttpResponse<IHoliday[]>) => {
        this.isLoading = false;
        const holidayList = res.body ?? [];
        const holidays: any[] = [];

        if (holidayList?.length) {
          let i = 0;
          let currentCode = null;
          let currentDates = [];

          for (let index = 0; index < holidayList.length; index++) {
            const holiday = holidayList[index];
            const { code, date } = holiday;

            if (code !== currentCode && currentCode !== null) {
              // add previous holidays with the same code to the list
              holidays[i] = { ...holidays[i], date: currentDates };
              i++;
              currentDates = [];
            }

            currentCode = code;
            currentDates.push(date);

            if (index === holidayList.length - 1) {
              // add last holiday with the same code to the list
              holidays[i] = { ...holiday, date: currentDates };
            } else {
              holidays[i] = { ...holiday };
            }
          }
        }

        // set the modified holidayList as the new value
        this.holidays = holidays;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IHoliday): number {
    return item.id!;
  }

  public showCreateModal(): void {
    const modalRef = this.modalService.open(HolidayUpdateComponent, { size: 'lg', backdrop: 'static' });
    modalRef.closed.subscribe(() => {
      this.loadAll();
    });
  }

  public showUpdateModal(holiday: IHoliday): void {
    const modalRef = this.modalService.open(HolidayUpdateComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.holiday = holiday;
    modalRef.closed.subscribe(() => {
      this.loadAll();
    });
  }

  delete(holiday: IHoliday): void {
    const modalRef = this.modalService.open(HolidayDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.holiday = holiday;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.DELETED) {
        this.loadAll();
      }
    });
  }
}
