import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { HolidayRoutingModule } from './route/holiday-routing.module';

import { HolidayComponent } from './list/holiday.component';
import { HolidayUpdateComponent } from './update/holiday-update.component';
import { HolidayDeleteDialogComponent } from './delete/holiday-delete-dialog.component';

@NgModule({
  imports: [SharedModule, HolidayRoutingModule],
  declarations: [HolidayComponent, HolidayUpdateComponent, HolidayDeleteDialogComponent],
  exports: [HolidayComponent],
  entryComponents: [HolidayDeleteDialogComponent],
})
export class HolidayModule {}
