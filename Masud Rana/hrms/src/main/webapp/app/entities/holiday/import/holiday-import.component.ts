import { Component, ElementRef, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import * as XLSX from 'xlsx';

import { IHoliday } from '../holiday.model';

import { ToastService } from 'app/shared/components/toast/toast.service';
import { HolidayService } from '../service/holiday.service';
import { UtilService } from 'app/shared/service/util.service';

@Component({
  selector: 'hrms-holiday-import',
  templateUrl: './holiday-import.component.html',
})
export class HolidayImportComponent {
  holidays: IHoliday[] = [];
  file: any;
  fileName = 'Choose file';

  @ViewChild('uploadHolidaytFile', { static: false }) fileUploader?: ElementRef;

  constructor(
    private holidayService: HolidayService,
    private utilService: UtilService,
    private translateService: TranslateService,
    private toastService: ToastService
  ) {}

  public onFileChange(event: any): void {
    const files = event.target.files;
    if (files.length) {
      this.fileName = files[0].name.split('.')[0];
      this.file = files[0];
    }
  }

  public uploadHolidaySheet(): void {
    if (this.file) {
      let errorCount = 0;
      const reader = new FileReader();
      reader.onload = (event: any) => {
        const wb = XLSX.read(event.target.result);
        const sheets = wb.SheetNames;

        if (sheets.length) {
          const rows = XLSX.utils.sheet_to_json(wb.Sheets[sheets[0]], { raw: false });

          this.holidays = rows.map((row: any) => {
            const { date, title, description } = row;
            const formattedDate = new Date(date).getTime();
            let holiday: IHoliday = {};
            if (formattedDate && title) {
              holiday = {
                date: formattedDate,
                title,
                description,
                code: this.utilService.generateRandomString(10),
              };
            } else {
              errorCount++;
            }
            return holiday;
          });
        }

        if (errorCount > 0) {
          this.toastService.showDangerToast(this.translateService.instant('timeClock.upload.validation.valid'));
          this.holidays = [];
        } else {
          this.holidayService.create(this.holidays).subscribe(res => {
            this.holidays = res.body ?? [];
            this.toastService.showSuccessToast(this.translateService.instant('timeClock.upload.validation.success'));
            if (this.fileUploader) {
              this.fileUploader.nativeElement.value = '';
              this.file = null;
            }
          });
        }
      };
      reader.readAsArrayBuffer(this.file);
    } else {
      this.toastService.showDangerToast(this.translateService.instant('timeClock.upload.validation.notFound'));
    }
  }
}
