import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IBranch, Branch } from '../branch.model';
import { ITimeZone } from '../../time-zone/time-zone.model';
import { IBreakRule } from '../../break-rule/break-rule.model';

import { BranchService } from '../service/branch.service';

import { noWhitespaceValidator } from 'app/shared/validators/no-whitespace.validator';
import { EModalReasonType } from 'app/shared/enum';

@Component({
  selector: 'hrms-branch-update-dialog',
  templateUrl: './branch-update-dialog.component.html',
})
export class BranchUpdateDialogComponent implements OnInit {
  branch?: IBranch;
  isSaving = false;
  timeZones: ITimeZone[] = [];
  breakRules: IBreakRule[] = [];

  addBranchForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(255), noWhitespaceValidator]],
    address: [null, [Validators.required, Validators.maxLength(255), noWhitespaceValidator]],
    phone: [null, [Validators.maxLength(20), noWhitespaceValidator]],
    time_zone: [],
    break_rule: [],
  });

  constructor(protected branchService: BranchService, protected fb: FormBuilder, public activeModal: NgbActiveModal) {}

  ngOnInit(): void {
    this.updateForm();
  }

  cancel(): void {
    this.activeModal.dismiss();
  }

  save(): void {
    this.isSaving = true;
    const branch = this.createFromForm();
    if (branch.id && branch.id !== undefined) {
      this.subscribeToSaveResponse(this.branchService.update(branch), EModalReasonType.UPDATED);
    } else {
      this.subscribeToSaveResponse(this.branchService.create(branch), EModalReasonType.CREATED);
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBranch>>, modalAction: string): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(modalAction),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(modalAction: string): void {
    this.activeModal.close(modalAction);
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(): void {
    if (this.branch !== undefined) {
      this.addBranchForm.patchValue({
        id: this.branch.id,
        name: this.branch.name,
        address: this.branch.address_line1,
        phone: this.branch.phone,
        time_zone: this.branch.time_zone,
        break_rule: this.branch.break_rule,
      });
    }
  }

  protected createFromForm(): IBranch {
    return {
      ...new Branch(),
      id: this.addBranchForm.get(['id'])!.value,
      name: this.addBranchForm.get(['name'])!.value,
      address_line1: this.addBranchForm.get(['address'])!.value,
      phone: this.addBranchForm.get(['phone'])!.value,
      time_zone: this.addBranchForm.get(['time_zone'])!.value,
      break_rule: this.addBranchForm.get(['break_rule'])!.value,
    };
  }
}
