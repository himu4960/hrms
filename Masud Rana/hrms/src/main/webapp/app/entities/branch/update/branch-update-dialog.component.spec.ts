jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { of, Subject } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Branch } from '../branch.model';

import { BranchService } from '../service/branch.service';

import { BranchUpdateDialogComponent } from './branch-update-dialog.component';

describe('Component Tests', () => {
  describe('Branch Management Update Component', () => {
    let comp: BranchUpdateDialogComponent;
    let fixture: ComponentFixture<BranchUpdateDialogComponent>;
    let branchService: BranchService;
    let mockActiveModal: NgbActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [BranchUpdateDialogComponent],
        providers: [FormBuilder, NgbActiveModal],
      })
        .overrideTemplate(BranchUpdateDialogComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BranchUpdateDialogComponent);
      mockActiveModal = TestBed.inject(NgbActiveModal);
      branchService = TestBed.inject(BranchService);

      comp = fixture.componentInstance;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const branch = { id: 123 };
        spyOn(branchService, 'update').and.returnValue(saveSubject);

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: branch }));
        saveSubject.complete();
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const branch = new Branch();
        spyOn(branchService, 'create').and.returnValue(saveSubject);

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: branch }));
        saveSubject.complete();

        // THEN
        expect(branchService.create).toHaveBeenCalled();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
