import { IMenu } from 'app/entities/menu/menu.model';
import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBranch } from '../branch.model';
import { ITimeZone } from './../../time-zone/time-zone.model';
import { IBreakRule } from './../../break-rule/break-rule.model';
import { IOpenPositionModalArgs } from './../../designation/designation.model';
import { IDesignation } from 'app/entities/designation/designation.model';

import { EModalReasonType } from 'app/shared/enum';

import { locationSideMenus } from 'app/shared/menus';

import { BranchService } from '../service/branch.service';
import { BreakRuleService } from './../../break-rule/service/break-rule.service';
import { TimeZoneService } from 'app/entities/time-zone/service/time-zone.service';
import { DesignationService } from './../../designation/service/designation.service';

import { BranchDeleteDialogComponent } from '../delete/branch-delete-dialog.component';
import { DesignationUpdateComponent } from './../../designation/update/designation-update.component';
import { DesignationDeleteDialogComponent } from './../../designation/delete/designation-delete-dialog.component';
import { BranchUpdateDialogComponent } from '../update/branch-update-dialog.component';
import { AssociateSkillDialogComponent } from '../associate-skill/associate-skill-dialog.component';

@Component({
  selector: 'hrms-branch',
  templateUrl: './branch.component.html',
})
export class BranchComponent implements OnInit {
  branches: IBranch[] = [];
  isSaving = false;
  isLoading = false;
  designation: any;
  timeZones: ITimeZone[] = [];
  breakRules: IBreakRule[] = [];
  sideMenus: IMenu[] = locationSideMenus;

  constructor(
    protected branchService: BranchService,
    protected designationService: DesignationService,
    protected modalService: NgbModal,
    protected timeZoneService: TimeZoneService,
    protected breakRuleService: BreakRuleService
  ) {}

  loadAll(): void {
    this.isLoading = true;

    this.branchService.query().subscribe(
      (res: HttpResponse<IBranch[]>) => {
        this.isLoading = false;
        this.branches = res.body ?? [];
        this.loadRelationshipsOptions();
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IBranch): number {
    return item.id!;
  }

  openAssociateSkillModal(designation: IDesignation): void {
    const modalRef = this.modalService.open(AssociateSkillDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.designation = designation;
    modalRef.componentInstance.selectedSkills = designation?.skills ?? [];
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.UPDATED) {
        this.loadAll();
      }
    });
  }

  openAddPositionModal(args: IOpenPositionModalArgs): void {
    const designation = args.designation;
    const branch_id = args.branch_id;

    const modalRef = this.modalService.open(DesignationUpdateComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.designation = designation;
    modalRef.componentInstance.branch_id = branch_id;
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.CREATED || reason === EModalReasonType.UPDATED) {
        this.loadAll();
      }
    });
  }

  visiblePosition(designation: IDesignation): void {
    this.designationService.update(designation).subscribe();
  }

  deletePosition(designation: IDesignation): void {
    const modalRef = this.modalService.open(DesignationDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.designation = designation;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.DELETED) {
        this.loadAll();
      }
    });
  }

  addOrUpdate(branch?: IBranch): void {
    const modalRef = this.modalService.open(BranchUpdateDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.branch = branch;
    modalRef.componentInstance.isSaving = false;
    modalRef.componentInstance.timeZones = this.timeZones;
    modalRef.componentInstance.breakRules = this.breakRules;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.CREATED || reason === EModalReasonType.UPDATED) {
        this.loadAll();
      }
    });
  }

  delete(branch?: IBranch): void {
    const modalRef = this.modalService.open(BranchDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.branch = branch;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.DELETED) {
        this.loadAll();
      }
    });
  }

  protected loadRelationshipsOptions(): void {
    this.timeZoneService
      .query()
      .pipe(map((res: HttpResponse<ITimeZone[]>) => res.body ?? []))
      .subscribe((timeZone: ITimeZone[]) => (this.timeZones = timeZone));

    this.breakRuleService
      .query()
      .pipe(map((res: HttpResponse<IBreakRule[]>) => res.body ?? []))
      .subscribe((breakRule: IBreakRule[]) => (this.breakRules = breakRule));
  }
}
