import { NgModule } from '@angular/core';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { SharedModule } from 'app/shared/shared.module';
import { BranchRoutingModule } from './route/branch-routing.module';
import { EmployeeModule } from './../../employee/module/employee.module';
import { CommonSidebarModule } from './../../shared/components/common-sidebar/common-sidebar.module';
import { EmployeeNavigationModule } from './../../employee/components/employee-navigation/employee-navigation.module';

import { BranchComponent } from './list/branch.component';
import { BranchUpdateDialogComponent } from './update/branch-update-dialog.component';
import { BranchDeleteDialogComponent } from './delete/branch-delete-dialog.component';
import { DesignationUpdateComponent } from '../designation/update/designation-update.component';
import { DesignationDeleteDialogComponent } from '../designation/delete/designation-delete-dialog.component';
import { BranchCardComponent } from './list/card/branch-card.component';
import { AssociateSkillDialogComponent } from './associate-skill/associate-skill-dialog.component';

@NgModule({
  imports: [NgMultiSelectDropDownModule, SharedModule, BranchRoutingModule, EmployeeModule, CommonSidebarModule, EmployeeNavigationModule],
  declarations: [
    BranchComponent,
    BranchCardComponent,
    BranchUpdateDialogComponent,
    BranchDeleteDialogComponent,
    DesignationUpdateComponent,
    DesignationDeleteDialogComponent,
    AssociateSkillDialogComponent,
  ],
  entryComponents: [BranchDeleteDialogComponent],
})
export class BranchModule {}
