import { IBreakRule } from 'app/entities/break-rule/break-rule.model';
import { ITimeZone } from 'app/entities/time-zone/time-zone.model';
import { ICompany } from 'app/company/company.model';
import { IEmployee } from 'app/employee/models/employee.model';

export interface IBranch {
  id?: number;
  name?: string;
  phone?: string | null;
  email?: string | null;
  address_line1?: string | null;
  address_line2?: string | null;
  address_city?: string | null;
  address_province?: string | null;
  address_country?: string | null;
  address_latitude?: number | null;
  address_longitude?: number | null;
  is_active?: boolean | null;
  is_primary?: boolean | null;
  break_rule_id?: number;
  time_zone_id?: number;
  break_rule?: IBreakRule | null;
  time_zone?: ITimeZone | null;
  designations?: any[];
  company?: ICompany | null;
  employees?: IEmployee[] | null;
}

export class Branch implements IBranch {
  constructor(
    public id?: number,
    public name?: string,
    public phone?: string | null,
    public email?: string | null,
    public address_line1?: string | null,
    public address_line2?: string | null,
    public address_city?: string | null,
    public address_province?: string | null,
    public address_country?: string | null,
    public address_latitude?: number | null,
    public address_longitude?: number | null,
    public is_active?: boolean | null,
    public is_primary?: boolean | null,
    public break_rule_id?: number,
    public time_zone_id?: number,
    public break_rule?: IBreakRule | null,
    public time_zone?: ITimeZone | null,
    public designations?: any[]
  ) {
    this.is_active = this.is_active ?? false;
  }
}

export function getBranchIdentifier(branch: IBranch): number | undefined {
  return branch.id;
}
