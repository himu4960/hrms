import { Component, Input, Output, EventEmitter } from '@angular/core';

import { IBranch } from '../../branch.model';
import { IDesignation } from './../../../designation/designation.model';

@Component({
  selector: 'hrms-branch-card',
  templateUrl: './branch-card.component.html',
})
export class BranchCardComponent {
  @Input() branches: IBranch[] = [];
  @Input() is_primary = false;
  @Output() addOrUpdateEmitter = new EventEmitter<IBranch>();
  @Output() deleteEmitter = new EventEmitter<IBranch>();
  @Output() deletePositionEmitter = new EventEmitter<IDesignation>();
  @Output() updatePositionEmitter = new EventEmitter<IDesignation>();
  @Output() openAddPositionModalEmitter = new EventEmitter<any>();
  @Output() openAssociateSkillModalEmitter = new EventEmitter<any>();

  designation: any;

  trackId(index: number, item: IBranch): number {
    return item.id!;
  }

  addOrUpdate(branch: IBranch): void {
    this.addOrUpdateEmitter.emit(branch);
  }

  delete(branch: IBranch): void {
    this.deleteEmitter.emit(branch);
  }

  visiblePosition(e: any, designation: IDesignation): void {
    const is_visible = e.target.checked;
    this.updatePositionEmitter.emit({ ...designation, is_visible });
  }

  deletePosition(designation: IDesignation): void {
    this.deletePositionEmitter.emit(designation);
  }

  openAddPositionModal(designation?: IDesignation, branch_id?: number): void {
    this.openAddPositionModalEmitter.emit({ designation, branch_id });
  }

  openAssociateSkillModal(designation: IDesignation): void {
    this.openAssociateSkillModalEmitter.emit(designation);
  }
}
