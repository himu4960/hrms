import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { HttpResponse } from '@angular/common/http';

import { EModalReasonType } from 'app/shared/enum';

import { ISkill } from 'app/entities/skill/skill.model';
import { IDesignation } from './../../designation/designation.model';

import { SkillService } from 'app/entities/skill/service/skill.service';
import { DesignationService } from './../../designation/service/designation.service';

@Component({
  templateUrl: './associate-skill-dialog.component.html',
})
export class AssociateSkillDialogComponent implements OnInit {
  designation?: IDesignation;
  selectedSkills: ISkill[] = [];
  skillsDropdownList: ISkill[] = [];
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true,
  };

  constructor(
    private readonly skillService: SkillService,
    private readonly designationService: DesignationService,
    private readonly activeModal: NgbActiveModal
  ) {}

  ngOnInit(): void {
    this.loadSkills();
  }

  public loadSkills(): void {
    this.skillService.query().subscribe((res: HttpResponse<ISkill[]>) => {
      this.skillsDropdownList = res.body ?? [];
    });
  }

  cancel(): void {
    this.activeModal.dismiss();
  }

  save(): void {
    this.designationService.update({ ...this.designation, skills: this.selectedSkills }).subscribe(
      () => {
        this.activeModal.close(EModalReasonType.UPDATED);
      },
      () => {
        this.cancel();
      }
    );
  }
}
