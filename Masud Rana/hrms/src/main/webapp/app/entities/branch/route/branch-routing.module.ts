import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BranchComponent } from '../list/branch.component';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';
import { RoutePermissionGuard } from 'app/core/auth/guards/route-permission.service';

const branchRoute: Routes = [
  {
    path: '',
    component: BranchComponent,
    canActivate: [AuthGuard, RoutePermissionGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(branchRoute)],
  exports: [RouterModule],
})
export class BranchRoutingModule {}
