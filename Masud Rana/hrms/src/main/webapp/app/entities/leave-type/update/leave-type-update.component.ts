import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { noWhitespaceValidator } from 'app/shared/validators/no-whitespace.validator';

import { ILeaveType, LeaveType } from '../leave-type.model';

import { LeaveTypeService } from '../service/leave-type.service';

@Component({
  selector: 'hrms-leave-type-update',
  templateUrl: './leave-type-update.component.html',
})
export class LeaveTypeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(255), noWhitespaceValidator]],
    is_partial_day_request_allowed: [],
    max_days_allowed: [null, [Validators.required]],
    is_max_employee_allowed_enabled: [],
    max_employee_allowed: [],
    is_leave_in_advance_days_enabled: [],
    leave_in_advance_days: [],
    auto_assign_new_employee_enabled: [],
    description: [],
  });

  isMaxEmployeeAllowedEnabled = this.editForm.get('is_max_employee_allowed_enabled')!.value;
  isLeaveAdvanceDaysEnabled = this.editForm.get('is_leave_in_advance_days_enabled')!.value;

  constructor(protected leaveTypeService: LeaveTypeService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ leaveType }) => {
      this.updateForm(leaveType);
    });

    this.editForm.get('is_max_employee_allowed_enabled')!.valueChanges.subscribe(value => {
      this.isMaxEmployeeAllowedEnabled = value;
      this.handleValidator(this.isMaxEmployeeAllowedEnabled, 'max_employee_allowed');
    });

    this.editForm.get('is_leave_in_advance_days_enabled')!.valueChanges.subscribe(value => {
      this.isLeaveAdvanceDaysEnabled = value;
      this.handleValidator(this.isLeaveAdvanceDaysEnabled, 'leave_in_advance_days');
    });
  }

  public handleValidator(isEnabled: boolean, formControlName: string): void {
    if (isEnabled) {
      this.editForm.controls[formControlName].setValidators([Validators.required]);
      this.editForm.controls[formControlName].updateValueAndValidity();
    } else {
      this.editForm.controls[formControlName].clearValidators();
      this.editForm.controls[formControlName].updateValueAndValidity();
    }
  }

  public previousState(): void {
    window.history.back();
  }

  public save(): void {
    this.isSaving = true;
    const leaveType = this.createFromForm();
    if (leaveType.id !== undefined) {
      this.subscribeToSaveResponse(this.leaveTypeService.update(leaveType));
    } else {
      this.subscribeToSaveResponse(this.leaveTypeService.create(leaveType));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILeaveType>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(leaveType: ILeaveType): void {
    this.editForm.patchValue({
      id: leaveType.id,
      name: leaveType.name,
      is_partial_day_request_allowed: leaveType.is_partial_day_request_allowed,
      max_days_allowed: leaveType.max_days_allowed,
      is_max_employee_allowed_enabled: leaveType.is_max_employee_allowed_enabled,
      max_employee_allowed: leaveType.max_employee_allowed,
      is_leave_in_advance_days_enabled: leaveType.is_leave_in_advance_days_enabled,
      leave_in_advance_days: leaveType.leave_in_advance_days,
      auto_assign_new_employee_enabled: leaveType.auto_assign_new_employee_enabled,
      description: leaveType.description,
    });
    this.isMaxEmployeeAllowedEnabled = leaveType?.is_max_employee_allowed_enabled ?? false;
    this.isLeaveAdvanceDaysEnabled = leaveType?.is_leave_in_advance_days_enabled ?? false;

    this.handleValidator(this.isMaxEmployeeAllowedEnabled, 'max_employee_allowed');
    this.handleValidator(this.isLeaveAdvanceDaysEnabled, 'leave_in_advance_days');
  }

  protected createFromForm(): ILeaveType {
    return {
      ...new LeaveType(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      is_partial_day_request_allowed: this.editForm.get(['is_partial_day_request_allowed'])!.value,
      max_days_allowed: this.editForm.get(['max_days_allowed'])!.value,
      is_max_employee_allowed_enabled: this.editForm.get(['is_max_employee_allowed_enabled'])!.value,
      max_employee_allowed: this.editForm.get(['max_employee_allowed'])!.value,
      is_leave_in_advance_days_enabled: this.editForm.get(['is_leave_in_advance_days_enabled'])!.value,
      leave_in_advance_days: this.editForm.get(['leave_in_advance_days'])!.value,
      auto_assign_new_employee_enabled: this.editForm.get(['auto_assign_new_employee_enabled'])!.value,
      description: this.editForm.get(['description'])!.value,
    };
  }
}
