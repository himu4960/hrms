import { ICompany } from '../../company/company.model';

export interface ILeaveType {
  id?: number;
  company_id?: number;
  name?: string;
  is_partial_day_request_allowed?: boolean | null;
  max_days_allowed?: number;
  is_max_employee_allowed_enabled?: boolean | null;
  max_employee_allowed?: number;
  is_leave_in_advance_days_enabled?: boolean | null;
  leave_in_advance_days?: number;
  auto_assign_new_employee_enabled?: boolean | null;
  description?: string | null;
  company?: ICompany | null;
}

export class LeaveType implements ILeaveType {
  constructor(
    public id?: number,
    public company_id?: number,
    public name?: string,
    public is_partial_day_request_allowed?: boolean | null,
    public max_days_allowed?: number,
    public is_max_employee_allowed_enabled?: boolean | null,
    public max_employee_allowed?: number,
    public is_leave_in_advance_days_enabled?: boolean | null,
    public leave_in_advance_days?: number,
    public auto_assign_new_employee_enabled?: boolean | null,
    public description?: string | null,
    public company?: ICompany | null
  ) {
    this.is_partial_day_request_allowed = this.is_partial_day_request_allowed ?? true;
    this.is_max_employee_allowed_enabled = this.is_max_employee_allowed_enabled ?? false;
    this.is_leave_in_advance_days_enabled = this.is_leave_in_advance_days_enabled ?? false;
    this.auto_assign_new_employee_enabled = this.auto_assign_new_employee_enabled ?? false;
  }
}

export function getLeaveTypeIdentifier(leaveType: ILeaveType): number | undefined {
  return leaveType.id;
}
