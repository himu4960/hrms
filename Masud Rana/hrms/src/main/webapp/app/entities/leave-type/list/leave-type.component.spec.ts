import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { LeaveTypeService } from '../service/leave-type.service';

import { LeaveTypeComponent } from './leave-type.component';

describe('Component Tests', () => {
  describe('LeaveType Management Component', () => {
    let comp: LeaveTypeComponent;
    let fixture: ComponentFixture<LeaveTypeComponent>;
    let service: LeaveTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [LeaveTypeComponent],
      })
        .overrideTemplate(LeaveTypeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(LeaveTypeComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(LeaveTypeService);

      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.leaveTypes?.[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
