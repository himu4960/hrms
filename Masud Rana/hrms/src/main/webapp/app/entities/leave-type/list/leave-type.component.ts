import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ILeaveType } from '../leave-type.model';
import { LeaveTypeService } from '../service/leave-type.service';
import { LeaveTypeDeleteDialogComponent } from '../delete/leave-type-delete-dialog.component';

@Component({
  selector: 'hrms-leave-type',
  templateUrl: './leave-type.component.html',
})
export class LeaveTypeComponent implements OnInit {
  leaveTypes?: ILeaveType[];
  isLoading = false;

  constructor(protected leaveTypeService: LeaveTypeService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.leaveTypeService.query().subscribe(
      (res: HttpResponse<ILeaveType[]>) => {
        this.isLoading = false;
        this.leaveTypes = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ILeaveType): number {
    return item.id!;
  }

  delete(leaveType: ILeaveType): void {
    const modalRef = this.modalService.open(LeaveTypeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.leaveType = leaveType;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
