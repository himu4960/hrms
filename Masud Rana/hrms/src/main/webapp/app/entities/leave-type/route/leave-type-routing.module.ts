import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';
import { LeaveTypeRoutingResolveService } from './leave-type-routing-resolve.service';

import { LeaveTypeComponent } from '../list/leave-type.component';
import { LeaveTypeUpdateComponent } from '../update/leave-type-update.component';

const leaveTypeRoute: Routes = [
  {
    path: '',
    component: LeaveTypeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'new',
    component: LeaveTypeUpdateComponent,
    resolve: {
      leaveType: LeaveTypeRoutingResolveService,
    },
    canActivate: [AuthGuard],
  },
  {
    path: ':id/edit',
    component: LeaveTypeUpdateComponent,
    resolve: {
      leaveType: LeaveTypeRoutingResolveService,
    },
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(leaveTypeRoute)],
  exports: [RouterModule],
})
export class LeaveTypeRoutingModule {}
