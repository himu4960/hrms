import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'role',
        data: { pageTitle: 'hrmsApp.role.home.title' },
        loadChildren: () => import('./role/role.module').then(m => m.RoleModule),
      },
      {
        path: 'branch',
        data: { pageTitle: 'hrmsApp.branch.home.title' },
        loadChildren: () => import('./branch/branch.module').then(m => m.BranchModule),
      },
      {
        path: 'menu',
        data: { pageTitle: 'hrmsApp.menu.home.title' },
        loadChildren: () => import('./menu/menu.module').then(m => m.MenuModule),
      },
      {
        path: 'permission-type',
        data: { pageTitle: 'hrmsApp.permissionType.home.title' },
        loadChildren: () => import('./permission-type/permission-type.module').then(m => m.PermissionTypeModule),
      },
      {
        path: 'skill',
        data: { pageTitle: 'hrmsApp.skill.home.title' },
        loadChildren: () => import('./skill/skill.module').then(m => m.SkillModule),
      },
      {
        path: 'break-rule',
        data: { pageTitle: 'hrmsApp.breakRule.home.title' },
        loadChildren: () => import('./break-rule/break-rule.module').then(m => m.BreakRuleModule),
      },
      {
        path: 'custom-field',
        data: { pageTitle: 'hrmsApp.customField.home.title' },
        loadChildren: () => import('./custom-field/custom-field.module').then(m => m.CustomFieldModule),
      },
      {
        path: 'work-shift',
        data: { pageTitle: 'hrmsApp.workShift.home.title' },
        loadChildren: () => import('./work-shift/work-shift.module').then(m => m.WorkShiftModule),
      },
      {
        path: 'leave-type',
        data: { pageTitle: 'leaveType.home.title' },
        loadChildren: () => import('./leave-type/leave-type.module').then(m => m.LeaveTypeModule),
      },
      {
        path: 'work-shift-template',
        data: { pageTitle: 'hrmsApp.workShiftTemplate.home.title' },
        loadChildren: () => import('./work-shift-template/work-shift-template.module').then(m => m.WorkShiftTemplateModule),
      },
      {
        path: 'holiday',
        data: { pageTitle: 'hrmsApp.holiday.home.title' },
        loadChildren: () => import('./holiday/holiday.module').then(m => m.HolidayModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
