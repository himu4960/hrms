export interface ICompanyType {
  id?: number;
  name?: string | null;
  is_active?: boolean | null;
  is_deleted?: boolean | null;
}

export class CompanyType implements ICompanyType {
  constructor(public id?: number, public name?: string | null, public is_active?: boolean | null, public is_deleted?: boolean | null) {
    this.is_active = this.is_active ?? false;
    this.is_deleted = this.is_deleted ?? false;
  }
}

export function getCompanyTypeIdentifier(companyType: ICompanyType): number | undefined {
  return companyType.id;
}
