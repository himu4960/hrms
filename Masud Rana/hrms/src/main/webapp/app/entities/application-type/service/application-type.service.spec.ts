import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IApplicationType } from '../application-type.model';

import { ApplicationTypeService } from './application-type.service';

describe('Service Tests', () => {
  describe('ApplicationType Service', () => {
    let service: ApplicationTypeService;
    let httpMock: HttpTestingController;
    let elemDefault: IApplicationType;
    let expectedResult: IApplicationType | IApplicationType[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(ApplicationTypeService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        name: 'AAAAAAA',
        is_active: false,
        is_deleted: false,
      };
    });

    describe('Service methods', () => {
      it('should return a list of ApplicationType', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'BBBBBB',
            is_active: true,
            is_deleted: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
