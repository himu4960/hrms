export interface IApplicationType {
  id?: number;
  name?: string;
  is_active?: boolean | null;
  is_deleted?: boolean | null;
}

export class ApplicationType implements IApplicationType {
  constructor(public id?: number, public name?: string, public is_active?: boolean | null, public is_deleted?: boolean | null) {
    this.is_active = this.is_active ?? false;
    this.is_deleted = this.is_deleted ?? false;
  }
}

export function getApplicationTypeIdentifier(applicationType: IApplicationType): number | undefined {
  return applicationType.id;
}
