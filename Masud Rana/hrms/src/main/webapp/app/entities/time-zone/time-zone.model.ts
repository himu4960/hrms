export interface ITimeZone {
  id: number;
  name: string;
  abbr: string;
  offset: string;
  description: string;
  is_active?: boolean;
  is_deleted?: boolean;
  is_default?: boolean;
}

export class TimeZone implements ITimeZone {
  constructor(
    public id: number,
    public name: string,
    public abbr: string,
    public offset: string,
    public description: string,
    public is_active?: boolean,
    public is_deleted?: boolean,
    public is_default?: boolean
  ) {
    this.is_active = this.is_active ?? false;
    this.is_deleted = this.is_deleted ?? false;
    this.is_default = this.is_default ?? false;
  }
}

export function getTimeZoneIdentifier(timeZone: ITimeZone): number | undefined {
  return timeZone.id;
}
