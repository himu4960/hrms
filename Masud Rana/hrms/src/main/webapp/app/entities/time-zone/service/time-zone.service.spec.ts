import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ITimeZone } from '../time-zone.model';

import { TimeZoneService } from './time-zone.service';

describe('Service Tests', () => {
  describe('TimeZone Service', () => {
    let service: TimeZoneService;
    let httpMock: HttpTestingController;
    let elemDefault: ITimeZone;
    let expectedResult: ITimeZone | ITimeZone[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(TimeZoneService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        name: 'AAAAAAA',
        abbr: 'AAAAAAA',
        offset: 'AAAAAAA',
        description: 'AAAAAAA',
        is_active: false,
        is_deleted: false,
        is_default: false,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a TimeZone', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(elemDefault).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a TimeZone', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'BBBBBB',
            code: 'BBBBBB',
            iso_code: 'BBBBBB',
            is_active: true,
            is_deleted: true,
            is_default: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a TimeZone', () => {
        const patchObject = Object.assign({
          ...elemDefault,
          name: 'BBBBBB',
          abbr: 'BBBBBB',
          offset: 'BBBBBB',
          description: 'BBBBBB',
          is_active: true,
          is_default: true,
        });

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of TimeZone', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            name: 'BBBBBB',
            abbr: 'BBBBBB',
            offset: 'BBBBBB',
            description: 'BBBBBB',
            is_active: true,
            is_deleted: true,
            is_default: true,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a TimeZone', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addTimeZoneToCollectionIfMissing', () => {
        it('should add a TimeZone to an empty array', () => {
          const timeZone: ITimeZone = { ...elemDefault, id: 123 };
          expectedResult = service.addTimeZoneToCollectionIfMissing([], timeZone);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(timeZone);
        });

        it('should not add a TimeZone to an array that contains it', () => {
          const timeZone: ITimeZone = { ...elemDefault, id: 123 };
          const timeZoneCollection: ITimeZone[] = [
            {
              ...timeZone,
            },
            { ...elemDefault, id: 456 },
          ];
          expectedResult = service.addTimeZoneToCollectionIfMissing(timeZoneCollection, timeZone);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a TimeZone to an array that doesn't contain it", () => {
          const timeZone: ITimeZone = { ...elemDefault, id: 123 };
          const timeZoneCollection: ITimeZone[] = [{ ...elemDefault, id: 456 }];
          expectedResult = service.addTimeZoneToCollectionIfMissing(timeZoneCollection, timeZone);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(timeZone);
        });

        it('should add only unique TimeZone to an array', () => {
          const timeZoneArray: ITimeZone[] = [
            { ...elemDefault, id: 123 },
            { ...elemDefault, id: 456 },
            { ...elemDefault, id: 16656 },
          ];
          const timeZoneCollection: ITimeZone[] = [{ ...elemDefault, id: 123 }];
          expectedResult = service.addTimeZoneToCollectionIfMissing(timeZoneCollection, ...timeZoneArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const timeZone: ITimeZone = { ...elemDefault, id: 123 };
          const timeZone2: ITimeZone = { ...elemDefault, id: 456 };
          expectedResult = service.addTimeZoneToCollectionIfMissing([], timeZone, timeZone2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(timeZone);
          expect(expectedResult).toContain(timeZone2);
        });

        it('should accept null and undefined values', () => {
          const timeZone: ITimeZone = { ...elemDefault, id: 123 };
          expectedResult = service.addTimeZoneToCollectionIfMissing([], null, timeZone, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(timeZone);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
