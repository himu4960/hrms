import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ITimeZone, getTimeZoneIdentifier } from '../time-zone.model';

export type EntityResponseType = HttpResponse<ITimeZone>;
export type EntityArrayResponseType = HttpResponse<ITimeZone[]>;

@Injectable({ providedIn: 'root' })
export class TimeZoneService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('time-zones');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(timeZone: ITimeZone): Observable<EntityResponseType> {
    return this.http.post<ITimeZone>(this.resourceUrl, timeZone, { observe: 'response' });
  }

  update(timeZone: ITimeZone): Observable<EntityResponseType> {
    return this.http.put<ITimeZone>(`${this.resourceUrl}/${getTimeZoneIdentifier(timeZone) as number}`, timeZone, { observe: 'response' });
  }

  partialUpdate(timeZone: ITimeZone): Observable<EntityResponseType> {
    return this.http.patch<ITimeZone>(`${this.resourceUrl}/${getTimeZoneIdentifier(timeZone) as number}`, timeZone, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITimeZone>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITimeZone[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addTimeZoneToCollectionIfMissing(timeZoneCollection: ITimeZone[], ...timeZonesToCheck: (ITimeZone | null | undefined)[]): ITimeZone[] {
    const timeZones: ITimeZone[] = timeZonesToCheck.filter(isPresent);
    if (timeZones.length > 0) {
      const timeZoneCollectionIdentifiers = timeZoneCollection.map(timeZoneItem => getTimeZoneIdentifier(timeZoneItem)!);
      const timeZonesToAdd = timeZones.filter(timeZoneItem => {
        const timeZoneIdentifier = getTimeZoneIdentifier(timeZoneItem);
        if (timeZoneIdentifier == null || timeZoneCollectionIdentifiers.includes(timeZoneIdentifier)) {
          return false;
        }
        timeZoneCollectionIdentifiers.push(timeZoneIdentifier);
        return true;
      });
      return [...timeZonesToAdd, ...timeZoneCollection];
    }
    return timeZoneCollection;
  }
}
