import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { ForgotPasswordComponent } from './forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ForgotPasswordComponent,
      },
      {
        path: 'reset/:id/:token',
        component: ResetPasswordComponent,
      },
    ]),
  ],
  declarations: [ForgotPasswordComponent, ResetPasswordComponent],
})
export class ForgotPasswordModule {}
