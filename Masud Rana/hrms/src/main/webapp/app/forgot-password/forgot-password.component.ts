import { Component, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { ForgotPasswordService } from './forgot-password.service';

@Component({
  selector: 'hrms-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements AfterViewInit {
  @ViewChild('email', { static: false })
  email?: ElementRef;

  success = false;
  isSaving = false;
  resetRequestForm = this.fb.group({
    email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
  });

  constructor(private forgotPasswordService: ForgotPasswordService, private fb: FormBuilder) {}

  ngAfterViewInit(): void {
    if (this.email) {
      this.email.nativeElement.focus();
    }
  }

  sendOneTimeLink(): void {
    this.isSaving = true;
    this.forgotPasswordService.sendOneTimeLink(this.resetRequestForm.get(['email'])!.value).subscribe(
      () => {
        this.success = true;
        this.isSaving = false;
      },
      () => {
        this.isSaving = false;
      }
    );
  }
}
