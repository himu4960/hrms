import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApplicationConfigService } from 'app/core/config/application-config.service';

@Injectable({ providedIn: 'root' })
export class ForgotPasswordService {
  resourceUrl = this.applicationConfigService.getEndpointFor('forgot-password');
  constructor(private http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  sendOneTimeLink(email: string): Observable<{}> {
    return this.http.get(`${this.resourceUrl}?email=${email}`, { observe: 'response' });
  }

  verifyToken(id: number, token: string): Observable<any> {
    return this.http.get(`${this.resourceUrl}/reset/${id}/${token}`, { observe: 'response' });
  }

  resetPassword(id: number, token: string, newPassword: string): Observable<any> {
    return this.http.post(`${this.resourceUrl}/reset/${id}/${token}`, { newPassword }, { observe: 'response' });
  }
}
