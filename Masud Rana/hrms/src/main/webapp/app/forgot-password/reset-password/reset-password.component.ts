import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ForgotPasswordService } from '../forgot-password.service';

import { noWhitespaceValidator } from 'app/shared/validators/no-whitespace.validator';

@Component({
  selector: 'hrms-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['../forgot-password.component.scss'],
})
export class ResetPasswordComponent {
  id!: number;
  token!: string;
  successToken = false;
  loading = true;
  isSaving = false;
  resetSuccess = false;

  passNotMatchedError = false;

  resetRequestForm = this.fb.group({
    newPassword: [null, [Validators.required, noWhitespaceValidator]],
    confirmPassword: [null, [Validators.required, noWhitespaceValidator]],
  });

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private readonly forgotPasswordService: ForgotPasswordService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const id = params['id'];
      const token = params['token'];
      if (id && token) {
        this.id = id;
        this.token = token;
        this.verifyToken(id, token);
      } else {
        this.router.navigateByUrl('404');
      }
    });
  }

  verifyToken(id: number, token: string): void {
    this.forgotPasswordService.verifyToken(id, token).subscribe(
      () => {
        this.successToken = true;
        this.loading = false;
      },
      () => {
        this.loading = false;
        this.router.navigateByUrl('404');
      }
    );
  }

  resetPassword(): void {
    const value = this.resetRequestForm.value;
    if (value.newPassword === value.confirmPassword) {
      if (this.id !== undefined && this.token !== undefined) {
        this.isSaving = true;
        this.passNotMatchedError = false;
        this.forgotPasswordService.resetPassword(this.id, this.token, value.newPassword).subscribe(
          () => {
            this.isSaving = false;
            this.resetSuccess = true;
          },
          () => {
            this.isSaving = false;
          }
        );
      } else {
        this.router.navigateByUrl('404');
      }
    } else {
      this.passNotMatchedError = true;
    }
  }
}
