import { ElementRef } from '@angular/core';
import { ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { of, throwError } from 'rxjs';

import { ForgotPasswordComponent } from './forgot-password.component';
import { ForgotPasswordService } from './forgot-password.service';

describe('Component Tests', () => {
  describe('ForgotPasswordComponent', () => {
    let fixture: ComponentFixture<ForgotPasswordComponent>;
    let comp: ForgotPasswordComponent;

    beforeEach(() => {
      fixture = TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [ForgotPasswordComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ForgotPasswordComponent, '')
        .createComponent(ForgotPasswordComponent);
      comp = fixture.componentInstance;
    });

    it('sets focus after the view has been initialized', () => {
      const node = {
        focus: jest.fn(),
      };
      comp.email = new ElementRef(node);

      comp.ngAfterViewInit();

      expect(node.focus).toHaveBeenCalled();
    });

    it('notifies of success upon successful sendOneTimeLink', inject([ForgotPasswordService], (service: ForgotPasswordService) => {
      spyOn(service, 'sendOneTimeLink').and.returnValue(of({}));
      comp.resetRequestForm.patchValue({
        email: 'user@domain.com',
      });

      comp.sendOneTimeLink();

      expect(service.sendOneTimeLink).toHaveBeenCalledWith('user@domain.com');
      expect(comp.success).toBe(true);
    }));

    it('no notification of success upon error response', inject([ForgotPasswordService], (service: ForgotPasswordService) => {
      spyOn(service, 'sendOneTimeLink').and.returnValue(
        throwError({
          status: 503,
          data: 'something else',
        })
      );
      comp.resetRequestForm.patchValue({
        email: 'user@domain.com',
      });
      comp.sendOneTimeLink();

      expect(service.sendOneTimeLink).toHaveBeenCalledWith('user@domain.com');
      expect(comp.success).toBe(false);
    }));
  });
});
