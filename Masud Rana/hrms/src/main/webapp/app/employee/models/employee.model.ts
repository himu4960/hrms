import { IBranch } from 'app/entities/branch/branch.model';
import { IDesignation } from 'app/entities/designation/designation.model';
import { ISkill } from 'app/entities/skill/skill.model';

export interface IEmployeeDesignation {
  id?: number;
  employee_id?: number;
  designation_id: number;
  designation?: IDesignation;
}

export interface IEmployeeSkill {
  employee_id?: number;
  skill_id?: number;
}

export interface IEmployeeDesignationDelete {
  employee_id: number;
  designation_id: number;
}

export interface IEmployeeWithCount {
  employees: IEmployee[];
  count: number;
}

export interface IEmployeeActivation {
  employee_id: number;
  is_active: boolean;
}

export interface IDropdownEmployee {
  id?: number;
  name?: string;
}

export interface IEmployee {
  id?: number;
  code?: string;
  first_name?: string | null;
  middle_name?: string | null;
  last_name?: string | null;
  dob?: number | null;
  email?: string | null;
  personal_email?: string | null;
  phone?: string | null;
  telephone?: string | null;
  nationality?: string | null;
  photo_url?: string | null;
  gender_id?: number | null;
  religion_id?: number | null;
  marital_status_id?: number | null;
  blood_group_id?: number | null;
  language_code?: number | null;
  is_active?: boolean | null;
  is_deleted?: boolean | null;
  invitation_accepted?: boolean | null;
  invitation_token?: string | null;
  invitation_expiry?: string | null;
  branch_id?: number | null;
  branch?: IBranch;
  employee_designations?: IEmployeeDesignation[];
  skills?: ISkill[];
  join_date?: number | null;
}

export interface IEmployeeList {
  id?: number;
  code?: string | null;
  first_name?: string | null;
  last_name?: string | null;
  email?: string | null;
  phone?: string | null;
  invitation_accepted?: boolean | null;
  role_name?: string | null;
  photo_url?: string | null;
  is_active?: boolean | null;
}

export class Employee implements IEmployee {
  constructor(
    public id?: number,
    public code?: string,
    public first_name?: string | null,
    public middle_name?: string | null,
    public last_name?: string | null,
    public dob?: number | null,
    public email?: string | null,
    public personal_email?: string | null,
    public phone?: string | null,
    public telephone?: string | null,
    public nationality?: string | null,
    public photo_url?: string | null,
    public gender_id?: number | null,
    public religion_id?: number | null,
    public marital_status_id?: number | null,
    public blood_group_id?: number | null,
    public language_code?: number | null,
    public is_active?: boolean | null,
    public is_deleted?: boolean | null,
    public invitation_accepted?: boolean | null,
    public invitation_token?: string | null,
    public invitation_expiry?: string | null,
    public branch_id?: number | null,
    public employee_designation?: IEmployeeDesignation[],
    public skills?: ISkill[]
  ) {}
}

export function getEmployeeIdentifier(employee: IEmployee): number | undefined {
  return employee.id;
}
