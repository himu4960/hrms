import { OwnRoutePermissionGuard } from './../core/auth/guards/own-route-permission.service';
import { RoutePermissionGuard } from 'app/core/auth/guards/route-permission.service';
import { AddEmployeeComponent } from './components/add-employee/add-employee.component';
import { EditEmployeeComponent } from './components/edit-employee/edit-employee.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { EmployeeOnlineComponent } from './components/employee-online/employee-online.component';
import { EmployeeOverviewComponent } from './components/employee-overview/employee-overview.component';
import { InactiveEmployeeComponent } from './components/inactive-employee/inactive-employee.component';
import { PasswordManagementComponent } from './components/password-management/password-management.component';
import { PermissionsManagementComponent } from './components/permissions-management/permissions-management.component';
import { EmployeeComponent } from './employee.component';

import { EmployeeRoutingResolveService } from './service/employee-routing-resolve.service';
import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';

export const employeeRoutes = [
  {
    path: '',
    component: EmployeeComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: EmployeeListComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'add-employee',
        component: AddEmployeeComponent,
        canActivate: [AuthGuard, RoutePermissionGuard],
      },
      {
        path: 'details/:id',
        component: EmployeeOverviewComponent,
        resolve: {
          data: EmployeeRoutingResolveService,
        },
        canActivate: [AuthGuard],
      },
      {
        path: 'edit/:id',
        component: EditEmployeeComponent,
        resolve: {
          data: EmployeeRoutingResolveService,
        },
        canActivate: [AuthGuard, OwnRoutePermissionGuard],
      },
      {
        path: 'online',
        component: EmployeeOnlineComponent,
        resolve: {
          data: EmployeeRoutingResolveService,
        },
        canActivate: [AuthGuard],
      },
      {
        path: 'inactive',
        component: InactiveEmployeeComponent,
        resolve: {
          data: EmployeeRoutingResolveService,
        },
        canActivate: [AuthGuard],
      },
      {
        path: 'password/:id',
        component: PasswordManagementComponent,
        resolve: {
          data: EmployeeRoutingResolveService,
        },
        canActivate: [AuthGuard, OwnRoutePermissionGuard],
      },
      {
        path: 'permissions/:id',
        component: PermissionsManagementComponent,
        resolve: {
          data: EmployeeRoutingResolveService,
        },
        canActivate: [AuthGuard, RoutePermissionGuard],
      },
    ],
  },
];
