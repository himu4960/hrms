import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Pagination } from 'app/core/request/request.model';
import { ICompany } from './../../company/company.model';

import { isPresent } from 'app/core/util/operators';
import { createRequestOption } from 'app/core/request/request-util';

import {
  getEmployeeIdentifier,
  IEmployee,
  IEmployeeActivation,
  IEmployeeDesignation,
  IEmployeeDesignationDelete,
  IEmployeeSkill,
  IEmployeeWithCount,
} from '../models/employee.model';
import { IBirthday } from './../../home/components/birthday/birthday.model';
import { IAnniversary } from './../../home/components/anniversary/anniversary.model';
import { IAddEmployee, IEmployeeFilter } from '../components/add-employee/add-employee.model';

import { ApplicationConfigService } from '../../core/config/application-config.service';

export type EntityResponseType = HttpResponse<IEmployee>;
export type EmployeeWithCountResponseType = HttpResponse<IEmployeeWithCount>;
export type EmployeeDesignationResponseType = HttpResponse<IEmployeeDesignation>;
export type EntityArrayResponseType = HttpResponse<IEmployee[]>;
export type AnniversaryArrayResponseType = HttpResponse<IAnniversary[]>;
export type BirthdayArrayResponseType = HttpResponse<IBirthday[]>;

@Injectable({ providedIn: 'root' })
export class EmployeeService {
  public resourceUrl = this.applicationConfigService.getEndpointFor(`employees`);
  public designationUrl = this.applicationConfigService.getEndpointFor(`designations`);

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  getEmployeesAnniversary(month?: number): Observable<AnniversaryArrayResponseType> {
    if (month !== undefined) {
      return this.http.get<IAnniversary[]>(`${this.resourceUrl}/anniversary?month=${month}`, { observe: 'response' });
    }
    return this.http.get<IAnniversary[]>(`${this.resourceUrl}/anniversary`, { observe: 'response' });
  }

  getEmployeesBirthday(month?: number): Observable<BirthdayArrayResponseType> {
    if (month !== undefined) {
      return this.http.get<IBirthday[]>(`${this.resourceUrl}/birthday?month=${month}`, { observe: 'response' });
    }
    return this.http.get<IBirthday[]>(`${this.resourceUrl}/birthday`, { observe: 'response' });
  }

  sendInvitation(employee_id: number, company: ICompany): Observable<EntityResponseType> {
    return this.http.post<any>(`${this.resourceUrl}/register/invite/send-invite/${employee_id}`, company, { observe: 'response' });
  }

  filter(employeeFilter: IEmployeeFilter, req?: Pagination): Observable<any> {
    const options = createRequestOption(req);
    return this.http.post<any>(`${this.resourceUrl}/filter`, employeeFilter, { params: options, observe: 'response' });
  }

  getBranchWiseDesignation(branch_id: number): Observable<any> {
    return this.http.get<any>(`${this.designationUrl}/visible/${branch_id}`, { observe: 'response' });
  }

  getBranchWiseDesignations(branch_id: number): Observable<any> {
    return this.http.get<any>(`${this.designationUrl}/visible/${branch_id}`, { observe: 'response' });
  }

  create(employees: IEmployee[]): Observable<EntityResponseType> {
    return this.http.post<IEmployee>(this.resourceUrl, employees, { observe: 'response' });
  }

  update(employee: IEmployee): Observable<EntityResponseType> {
    return this.http.put<IEmployee>(`${this.resourceUrl}/${employee.id}`, employee, { observe: 'response' });
  }

  changeBranch(employee: IEmployee): Observable<EntityResponseType> {
    return this.http.put<IEmployee>(
      `${this.resourceUrl}/change-branch/${employee.id}`,
      { branch_id: employee.branch_id },
      { observe: 'response' }
    );
  }

  toggleActivation(employeeActivation: IEmployeeActivation): Observable<any> {
    return this.http.put(`${this.resourceUrl}/activation`, employeeActivation, { observe: 'response' });
  }

  assignDesignation(employeeDesignation: IEmployeeDesignation): Observable<EmployeeDesignationResponseType> {
    return this.http.post<IEmployeeDesignation>(`${this.resourceUrl}/designation/assign`, employeeDesignation, { observe: 'response' });
  }

  assignSkill(employeeSkill: IEmployeeSkill): Observable<EntityResponseType> {
    return this.http.post<IEmployeeDesignation>(`${this.resourceUrl}/skill/assign`, employeeSkill, { observe: 'response' });
  }

  unassignSkill(employeeSkill: IEmployeeSkill): Observable<{}> {
    const { employee_id, skill_id } = employeeSkill;
    return this.http.request('delete', `${this.resourceUrl}/skill/unassign`, {
      body: { employee_id: employee_id, skill_id: skill_id },
    });
  }

  unassignDesignation(employeeDesignation: IEmployeeDesignationDelete): Observable<any> {
    const { employee_id, designation_id } = employeeDesignation;
    return this.http.request('delete', `${this.resourceUrl}/designation/unassign`, {
      body: { employee_id: employee_id, designation_id: designation_id },
      observe: 'response',
    });
  }

  partialUpdate(employee: IEmployee): Observable<EntityResponseType> {
    return this.http.patch<IEmployee>(`${this.resourceUrl}/${getEmployeeIdentifier(employee) as number}`, employee, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IEmployee>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findAll(): Observable<EntityArrayResponseType> {
    return this.http.get<IEmployee[]>(this.resourceUrl, { observe: 'response' });
  }

  findAllInactiveEmployees(req?: any): Observable<EmployeeWithCountResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEmployeeWithCount>(`${this.resourceUrl}/inactive`, { params: options, observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEmployee[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  uploadFile(file: any, id: any): Observable<any> {
    const formData = new FormData();
    formData.append('file', file, file.name);
    formData.append('id', id);
    return this.http.post(`${this.resourceUrl}/image/upload`, formData);
  }

  addEmployeeToCollectionIfMissing(employeeCollection: IEmployee[], ...employeesToCheck: (IEmployee | null | undefined)[]): IEmployee[] {
    const employees: IEmployee[] = employeesToCheck.filter(isPresent);
    if (employees.length > 0) {
      const employeeCollectionIdentifiers = employeeCollection.map(employeeItem => getEmployeeIdentifier(employeeItem)!);
      const employeesToAdd = employees.filter(employeeItem => {
        const employeeIdentifier = getEmployeeIdentifier(employeeItem);
        if (employeeIdentifier == null || employeeCollectionIdentifiers.includes(employeeIdentifier)) {
          return false;
        }
        employeeCollectionIdentifiers.push(employeeIdentifier);
        return true;
      });
      return [...employeesToAdd, ...employeeCollection];
    }
    return employeeCollection;
  }

  public findTodayBirthdays(birthdays?: IBirthday[]): IBirthday[] {
    const currentDate = new Date(Date.now()).getUTCDate();
    return (
      birthdays
        ?.map(employee => {
          const birthday_date = new Date(Number(employee.dob)).getUTCDate();
          const birthday_month = new Date(Number(employee.dob)).getUTCMonth();
          if (currentDate === birthday_date) {
            return { ...employee, birthday_date, birthday_month, is_today_birthday: true };
          }
          return { ...employee, birthday_date, birthday_month, is_today_birthday: false };
        })
        .sort((a: IBirthday, b: IBirthday) => Number(b.is_today_birthday) - Number(a.is_today_birthday)) ?? []
    );
  }

  public findTodayAnniversaries(anniversaries?: IAnniversary[]): IAnniversary[] {
    const currentDate = new Date(Date.now()).getUTCDate();
    return (
      anniversaries
        ?.map(employee => {
          const anniversary_date = new Date(Number(employee.join_date)).getUTCDate();
          const anniversary_month = new Date(Number(employee.join_date)).getUTCMonth();
          if (currentDate === anniversary_date) {
            return { ...employee, anniversary_date, anniversary_month, is_today_anniversary: true };
          }
          return { ...employee, anniversary_date, anniversary_month, is_today_anniversary: false };
        })
        .sort((a: IAnniversary, b: IAnniversary) => Number(b.is_today_anniversary) - Number(a.is_today_anniversary)) ?? []
    );
  }

  export(type: string): any {
    return this.http.post(`${this.resourceUrl}/export?type=${type}`, '', {
      observe: 'response',
      responseType: 'blob',
    });
  }
}
