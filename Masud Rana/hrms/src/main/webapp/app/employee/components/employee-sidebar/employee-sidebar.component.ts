import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from 'app/store';

import { EPaginationPageSize } from 'app/shared/enum/pagination.enum';

import { PermissionService } from './../../../shared/service/permission.service';

import { employeeFilterAction } from './../../../store/employee/employee.actions';

import { IBranch } from './../../../entities/branch/branch.model';

@Component({
  selector: 'hrms-employee-sidebar',
  templateUrl: './employee-sidebar.component.html',
  styleUrls: ['./employee-sidebar.component.scss'],
})
export class EmployeeSidebarComponent implements OnInit {
  isEmployee = true;
  pageSize = EPaginationPageSize.PAGE_SIZE_20;
  page = 0;
  @Input() totalEmployee = 0;
  @Input() notActivatedEmployeeCount = 0;
  @Input() disabledEmployeeCount = 0;
  @Input() branches: IBranch[] = [];

  constructor(private store: Store<AppState>, private readonly permissionService: PermissionService) {}

  ngOnInit(): void {
    this.isEmployee = this.permissionService.isEmployee();
  }

  public filterFromSidebar(filter: any): void {
    this.store.dispatch(
      employeeFilterAction({
        filter: {
          ...filter,
          designations: [],
          skills: [],
        },
        pagination: {
          page: this.page,
          size: this.pageSize,
        },
      })
    );
  }
}
