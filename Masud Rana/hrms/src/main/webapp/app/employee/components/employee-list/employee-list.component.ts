import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Store } from '@ngrx/store';
import { AppState } from 'app/store';

import { ISkill } from 'app/entities/skill/skill.model';
import { IBranch } from 'app/entities/branch/branch.model';
import { IEmployeeData } from 'app/shared/models/common.model';
import { IDesignation } from 'app/entities/designation/designation.model';
import { IEmployee, IEmployeeList } from 'app/employee/models/employee.model';
import { EExportType } from 'app/reports/time-sheet/models/time-sheet-report.model';
import { IEmployeeWithCount } from './../../models/employee.model';

import { EmployeeService } from 'app/employee/service/employee.service';
import { PermissionService } from 'app/shared/service/permission.service';
import { BranchService } from 'app/entities/branch/service/branch.service';
import { SkillService } from 'app/entities/skill/service/skill.service';
import { CommonService } from 'app/shared/service/common.service';

import { selectApplicationSettings } from 'app/store/company-settings/company-settings.selectors';
import { selectEmployeeData } from 'app/store/employee/employee.selectors';

import { ApplicationSettings } from 'app/store/states/application-settings.interface';

import { EPaginationPageSize } from 'app/shared/enum/pagination.enum';
import { RoleType } from './../../../shared/enum/role-types.enum';

@Component({
  selector: 'hrms-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss'],
})
export class EmployeeListComponent implements OnInit, OnDestroy {
  isLoading = true;
  isEmployee = true;
  employees: IEmployeeList[] = [];
  branches: IBranch[] = [];
  designations: IDesignation[] = [];
  skills: ISkill[] = [];
  applicationSettings!: ApplicationSettings;
  totalEmployeeCount = 0;
  notActivatedEmployeeCount = 0;
  disabledEmployeeCount = 0;
  filteredSkills: number[] = [];
  filteredDesignations: number[] = [];
  selectedBranch: string | null = null;

  pageSize = EPaginationPageSize.PAGE_SIZE_20;
  page = 0;

  employeeFilterForm: FormGroup = this.fb.group({
    search_text: [],
    branch_id: [],
  });

  params = {};

  get eRoleType(): any {
    return RoleType;
  }

  private subscriptions: Subscription = new Subscription();

  constructor(
    private store: Store<AppState>,
    protected employeeService: EmployeeService,
    protected permissionService: PermissionService,
    protected branchService: BranchService,
    protected skillService: SkillService,
    protected modalService: NgbModal,
    protected commonService: CommonService,
    protected fb: FormBuilder,
    protected route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getEmployeeData();

    this.subscriptions.add(
      this.store.select(selectApplicationSettings).subscribe((applicationSettings: ApplicationSettings) => {
        this.applicationSettings = applicationSettings;
      })
    );

    this.subscriptions.add(
      this.route.queryParams.subscribe(params => {
        this.params = params;
      })
    );
  }

  transition(page: number): void {
    this.page = page - 1;
    this.filter();
  }

  searchFilter(): void {
    this.page = 0;
  }

  public getEmployeeData(): void {
    this.isLoading = true;
    this.subscriptions.add(
      this.store.select(selectEmployeeData).subscribe(
        (data: IEmployeeData) => {
          if (data.employees) {
            this.isEmployee = this.permissionService.isEmployee();
            this.employees = data.employees;
            this.branches = data.branches;
            this.skills = data?.skills?.map((skill: ISkill) => ({ ...skill, checked: false })) ?? [];
            this.designations = data?.designations?.map(designation => ({ ...designation, checked: false })) ?? [];
            this.totalEmployeeCount = data.total_employee_count;
          }
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      )
    );
  }

  public changeBranch(branchId?: number): void {
    this.employeeFilterForm.get('branch_id')!.setValue(branchId);
    const findBranch = this.branches.find((branch: IBranch) => branch.id === Number(branchId));
    this.selectedBranch = findBranch?.name ?? null;
    this.designations = findBranch?.designations ?? [];
  }

  public filter(): void {
    this.isLoading = true;
    const formValue = this.employeeFilterForm.value;
    this.employeeService
      .filter(
        { ...formValue, ...this.params, designations: this.filteredDesignations, skills: this.filteredSkills },
        { page: this.page, size: this.pageSize }
      )
      .subscribe(
        (res: HttpResponse<IEmployeeWithCount>) => {
          this.designations = this.designations.map((designation: IDesignation) => {
            if (designation?.id && this.filteredDesignations.includes(designation.id)) {
              return {
                ...designation,
                checked: true,
              };
            } else {
              return {
                ...designation,
                checked: false,
              };
            }
          });
          this.skills = this.skills.map((skill: ISkill) => {
            if (skill?.id && this.filteredSkills.includes(skill.id)) {
              return {
                ...skill,
                checked: true,
              };
            } else {
              return {
                ...skill,
                checked: false,
              };
            }
          });
          this.employees = res.body?.employees ?? [];
          this.totalEmployeeCount = res.body?.count ?? 0;
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  public handleDesignation(e: any): void {
    if (e.target.checked) {
      if (!this.filteredDesignations.includes(Number(e.target.value))) {
        this.filteredDesignations.push(Number(e.target.value));
      }
    } else {
      const designationToRemove = this.filteredDesignations.findIndex(id => id === Number(e.target.value));
      this.filteredDesignations.splice(designationToRemove, 1);
    }
  }

  public handleSkill(e: any): void {
    if (e.target.checked) {
      if (!this.filteredDesignations.includes(Number(e.target.value))) {
        this.filteredSkills.push(Number(e.target.value));
      }
    } else {
      const skillToRemove = this.filteredSkills.findIndex(id => id === Number(e.target.value));
      this.filteredSkills.splice(skillToRemove, 1);
    }
  }

  public loadAll(): void {
    this.employeeService.query().subscribe(
      (res: HttpResponse<IEmployee[]>) => {
        this.employees = res.body ?? [];
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  public exportExcel(): void {
    this.isLoading = true;

    this.employeeService.export(EExportType.XLSX).subscribe((data: any) => {
      const url = window.URL.createObjectURL(data.body);
      window.open(url);
    });

    this.isLoading = false;
  }

  public exportCsv(): void {
    this.isLoading = true;

    this.employeeService.export(EExportType.CSV).subscribe((data: any) => {
      const url = window.URL.createObjectURL(data.body);
      window.open(url);
    });

    this.isLoading = false;
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
