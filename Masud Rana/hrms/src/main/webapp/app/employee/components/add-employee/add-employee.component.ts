import { Component } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { AppState } from 'app/store';

import { employeeAction } from './../../../store/employee/employee.actions';

import { EmployeeService } from '../../service/employee.service';
import { ToastService } from './../../../shared/components/toast/toast.service';

import { noWhitespaceValidator } from 'app/shared/validators/no-whitespace.validator';

@Component({
  selector: 'hrms-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss'],
})
export class AddEmployeeComponent {
  isSaving = false;
  employeesForm: FormGroup = this.fb.group({
    employees: this.fb.array([this.newEmployee()]),
  });

  constructor(
    protected fb: FormBuilder,
    protected employeeService: EmployeeService,
    protected toastService: ToastService,
    private translateService: TranslateService,
    private store: Store<AppState>
  ) {}

  previousState(): void {
    this.toastService.showSuccessToast(this.translateService.instant('hrmsApp.employee.sentEmail.success'));
    this.store.dispatch(employeeAction());
    window.history.back();
  }

  get employees(): FormArray {
    return this.employeesForm.get('employees') as FormArray;
  }

  newEmployee(): FormGroup {
    return this.fb.group({
      first_name: [null, [Validators.required, Validators.maxLength(255), noWhitespaceValidator]],
      last_name: [null, [Validators.required, Validators.maxLength(255), noWhitespaceValidator]],
      email: [null, [Validators.required, Validators.email, noWhitespaceValidator]],
    });
  }

  addEmployee(): void {
    this.employees.push(this.newEmployee());
  }

  removeEmployee(i: number): void {
    this.employees.removeAt(i);
  }

  save(): void {
    this.isSaving = true;
    this.subscribeToSaveResponse(this.employeeService.create(this.employeesForm.value.employees));
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<any>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
    this.isSaving = false;
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }
}
