import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IEmployee } from 'app/employee/models/employee.model';
import { EmployeeService } from 'app/employee/service/employee.service';

import { EModalReasonType } from 'app/shared/enum';

@Component({
  templateUrl: './employee-delete-dialog.component.html',
})
export class EmployeeDeleteDialogComponent {
  isSaving = false;
  employee?: IEmployee;

  constructor(protected employeeService: EmployeeService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.isSaving = true;
    this.employeeService.delete(id).subscribe(() => {
      this.isSaving = false;
      this.activeModal.close(EModalReasonType.DELETED);
    });
  }
}
