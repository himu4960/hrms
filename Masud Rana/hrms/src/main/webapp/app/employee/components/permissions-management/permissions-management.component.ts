import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { AppState } from 'app/store';
import { Store } from '@ngrx/store';

import { selectCurrentCompany } from 'app/store/user/user.selectors';

import { AccountService } from 'app/core/auth/account.service';
import { UtilService } from 'app/shared/service/util.service';

import { IEmployee } from 'app/employee/models/employee.model';
import { IRole, IUpdateRole } from 'app/entities/role/role.model';
import { PermissionService } from 'app/shared/service/permission.service';

@Component({
  selector: 'hrms-permissions-management',
  templateUrl: './permissions-management.component.html',
  styleUrls: ['./permissions-management.component.scss'],
})
export class PermissionsManagementComponent implements OnInit, OnDestroy {
  employeeDetails: IEmployee | null = null;
  hasPermission = false;
  isOwner = false;
  isEmployee = true;
  employeeId!: number;
  roles: IRole[] = [];
  role_id = null;
  role_name!: string;

  private subscriptions: Subscription = new Subscription();

  constructor(
    private store: Store<AppState>,
    private utilService: UtilService,
    private activatedRoute: ActivatedRoute,
    private accountService: AccountService,
    private permissionService: PermissionService
  ) {}

  ngOnInit(): void {
    const userType = this.permissionService.getUserType();
    this.isEmployee = this.permissionService.isEmployee();
    const userPermissionLavel = this.permissionService.getPermissionRoleTypeCode(userType);
    this.subscriptions.add(
      this.activatedRoute.data
        .pipe(
          switchMap((routerResolved: any) => {
            this.employeeDetails = routerResolved.data?.employee;
            this.role_id = routerResolved.data?.role.id;
            this.isOwner = routerResolved.data?.is_owner;
            this.role_name = routerResolved.data?.role.name;
            this.hasPermission = userPermissionLavel >= this.permissionService.getPermissionRoleTypeCode(this.role_name);
            return this.utilService.getRoles();
          })
        )
        .subscribe((roles: IRole[]) => {
          roles.map(role => {
            if (role?.name) {
              const employeePermissionLavel = this.permissionService.getPermissionRoleTypeCode(role.name);
              if (userPermissionLavel >= employeePermissionLavel) {
                this.roles.push(role);
              }
            }
          });
        })
    );

    this.subscriptions.add(
      this.store.select(selectCurrentCompany).subscribe(selectedCompany => {
        if (selectedCompany?.employee?.id) {
          this.employeeId = selectedCompany?.employee?.id;
        }
      })
    );
  }

  updateRole(): void {
    const role: IUpdateRole = {
      employee_id: this.employeeDetails?.id,
      role_id: this.role_id,
    };
    this.accountService.updateRole(role).subscribe();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
