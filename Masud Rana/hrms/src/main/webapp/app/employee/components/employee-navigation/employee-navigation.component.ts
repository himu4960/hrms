import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { PermissionService } from 'app/shared/service/permission.service';

@Component({
  selector: 'hrms-employee-navigation',
  templateUrl: './employee-navigation.component.html',
  styleUrls: ['./employee-navigation.component.scss'],
})
export class EmployeeNavigationComponent implements OnInit {
  @Input() routerURL = '';
  @Output() eventEmitter = new EventEmitter<any>();
  isEmployee = true;

  constructor(private permissionService: PermissionService) {}

  ngOnInit(): void {
    this.isEmployee = this.permissionService.isEmployee();
  }

  public onEmit(): void {
    this.eventEmitter.emit();
  }
}
