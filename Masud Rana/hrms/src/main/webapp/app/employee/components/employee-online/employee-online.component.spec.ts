import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeOnlineComponent } from './employee-online.component';

import { EmployeeOnlineService } from '../../../shared/components/socket/employee-online.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('EmployeeOnlineComponent', () => {
  let component: EmployeeOnlineComponent;
  let fixture: ComponentFixture<EmployeeOnlineComponent>;
  let fakeService: {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [{ provide: EmployeeOnlineService, useValue: fakeService }],
      declarations: [EmployeeOnlineComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeOnlineComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
