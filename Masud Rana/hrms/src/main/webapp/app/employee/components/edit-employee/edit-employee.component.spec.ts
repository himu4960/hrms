jest.mock('@angular/router');

import { StoreModule } from '@ngrx/store';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import { EditEmployeeComponent } from './edit-employee.component';

import { EmployeeService } from 'app/employee/service/employee.service';

import { IEmployee } from 'app/employee/models/employee.model';

describe('Component Tests', () => {
  describe('Profile edit Component', () => {
    let comp: EditEmployeeComponent;
    let fixture: ComponentFixture<EditEmployeeComponent>;
    let activatedRoute: ActivatedRoute;
    let employeeService: EmployeeService;

    let localStorageMock: Partial<LocalStorageService>;
    let sessionStorageMock: Partial<SessionStorageService>;

    let fakeService: {};

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, StoreModule.forRoot({})],
        declarations: [EditEmployeeComponent],
        providers: [
          FormBuilder,
          ActivatedRoute,
          { provide: LocalStorageService, useValue: localStorageMock },
          { provide: SessionStorageService, useValue: sessionStorageMock },
          { provide: TranslateService, useValue: fakeService },
        ],
      })
        .overrideTemplate(EditEmployeeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EditEmployeeComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      employeeService = TestBed.inject(EmployeeService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update emplyeeForm', () => {
        const employee: IEmployee = { id: 456 };
        activatedRoute.data = of({ employee });
        spyOn(comp, 'fetchData');
        comp.ngOnInit();

        expect(comp.fetchData).toHaveBeenCalled();
      });
    });

    describe('update', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const employee = { id: 123 };
        spyOn(employeeService, 'update').and.returnValue(saveSubject);
        activatedRoute.data = of({ employee });
        comp.ngOnInit();

        // WHEN
        comp.update();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: employee }));
        saveSubject.complete();
      });
    });
  });
});
