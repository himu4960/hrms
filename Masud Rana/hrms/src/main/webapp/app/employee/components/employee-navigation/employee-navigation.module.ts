import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from 'app/shared/shared.module';

import { EmployeeNavigationComponent } from './employee-navigation.component';

@NgModule({
  imports: [SharedModule, RouterModule, NgbDropdownModule],
  declarations: [EmployeeNavigationComponent],
  exports: [EmployeeNavigationComponent],
})
export class EmployeeNavigationModule {}
