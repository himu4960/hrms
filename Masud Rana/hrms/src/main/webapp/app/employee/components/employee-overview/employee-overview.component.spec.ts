jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';

import { StoreModule } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

import { ToastService } from 'app/shared/components/toast/toast.service';

import { EmployeeOverviewComponent } from './employee-overview.component';

describe('Component Tests', () => {
  describe('Employee overview Component', () => {
    let comp: EmployeeOverviewComponent;
    let fixture: ComponentFixture<EmployeeOverviewComponent>;

    let localStorageMock: Partial<LocalStorageService>;
    let sessionStorageMock: Partial<SessionStorageService>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, StoreModule.forRoot({})],
        declarations: [EmployeeOverviewComponent],
        providers: [
          { provide: LocalStorageService, useValue: localStorageMock },
          { provide: SessionStorageService, useValue: sessionStorageMock },
          { provide: TranslateService, useValue: {} },
          ActivatedRoute,
          ToastService,
        ],
      })
        .overrideTemplate(EmployeeOverviewComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EmployeeOverviewComponent);
      TestBed.inject(ActivatedRoute);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should load emplyeeDetails', () => {
        spyOn(comp, 'loadEmployeeDetails');

        comp.ngOnInit();
        expect(comp.loadEmployeeDetails).toHaveBeenCalled();
      });
    });
  });
});
