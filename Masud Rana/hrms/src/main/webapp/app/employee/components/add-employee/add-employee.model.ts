export interface IAddEmployee {
  firstName?: string;
  lastName?: string;
  email?: string;
}

export interface IEmployeeFilter {
  search_text: string | null;
  branch_id: number | null;
  is_active?: boolean | null;
  invitation_accepted?: boolean | null;
  designations: number[];
  skills: number[];
}

export class AddEmployee implements IAddEmployee {
  constructor(public firstName?: string, public lastName?: string, public email?: string) {}
}
