import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { LocalStorageService, NgxWebstorageModule } from 'ngx-webstorage';
import { of } from 'rxjs';

import { PermissionService } from 'app/shared/service/permission.service';
import { CommonService } from 'app/shared/service/common.service';

import { EmployeeListComponent } from './employee-list.component';

describe('EmployeeListComponent', () => {
  describe('List employee Component', () => {
    let comp: EmployeeListComponent;
    let fixture: ComponentFixture<EmployeeListComponent>;
    let commonService: CommonService;
    let localStorageService: LocalStorageService;
    const fakeService = {};

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, NgxWebstorageModule.forRoot()],
        providers: [
          FormBuilder,
          provideMockStore(),
          { provide: PermissionService, useValue: fakeService },
          {
            provide: ActivatedRoute,
            useValue: { queryParams: of({ key: 'ABC123' }) },
          },
        ],
        declarations: [EmployeeListComponent],
      })
        .overrideTemplate(EmployeeListComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EmployeeListComponent);
      TestBed.inject(LocalStorageService);
      comp = fixture.componentInstance;
      commonService = TestBed.inject(CommonService);
      localStorageService = TestBed.inject(LocalStorageService);

      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(commonService, 'getEmployeeData').and.returnValue(
        of(
          new HttpResponse({
            body: [{ id: 123 }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();
      expect(comp).toBeTruthy();
    });
  });
});
