import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { EmployeeOnlineComponent } from './employee-online.component';

@NgModule({
  imports: [SharedModule],
  declarations: [EmployeeOnlineComponent],
  exports: [EmployeeOnlineComponent],
})
export class EmployeeOnlineModule {}
