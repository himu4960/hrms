import { Component, OnInit } from '@angular/core';

import { EmployeeOnlineService } from '../../../shared/components/socket/employee-online.service';

import { ITimeClock } from './../../../time-clock/models/time-clock.model';

import { ETimeClockEventType } from './../../../time-clock/time-clock-event-type.enum';

@Component({
  selector: 'hrms-employee-online',
  templateUrl: './employee-online.component.html',
  styleUrls: ['./employee-online.component.scss'],
})
export class EmployeeOnlineComponent implements OnInit {
  ETimeClockEventType = ETimeClockEventType;
  isLoading = false;
  onlineEmployees: any = [];

  constructor(private employeeOnlineService: EmployeeOnlineService) {}

  ngOnInit(): void {
    this.employeeOnlineService.setLoadedOnline();
    this.employeeOnlineService.getAllOnlineEmployees().subscribe((timeClocks: ITimeClock[]) => {
      this.onlineEmployees = this.checkIsOnBreak(timeClocks);
    });
  }

  public checkIsOnBreak(timeClocks: ITimeClock[]): any {
    return timeClocks.map((timeClock: ITimeClock) => {
      let isOnBreak = false;

      const timeClockEvents = timeClock.time_clock_events?.map(event => {
        if (event?.event_type === ETimeClockEventType.BREAK && event?.break_start_time && !event?.break_end_time) {
          isOnBreak = true;
        }
        return event;
      });

      return { ...timeClock, time_clock_events: timeClockEvents, isOnBreak };
    });
  }
}
