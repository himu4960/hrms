import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { AppState } from 'app/store';

import { IEmployee, IEmployeeWithCount } from './../../models/employee.model';
import { Pagination } from './../../../core/request/request.model';
import { ICompany } from '../../../company/company.model';

import { EModalReasonType } from 'app/shared/enum';
import { EPaginationPageSize } from 'app/shared/enum/pagination.enum';

import { EmployeeService } from 'app/employee/service/employee.service';
import { ToastService } from './../../../shared/components/toast/toast.service';

import { EmployeeDeleteDialogComponent } from './delete/employee-delete-dialog.component';

import { employeeFilterAction } from 'app/store/employee/employee.actions';
import { employeeAction } from 'app/store/employee/employee.actions';

import { selectCurrentCompany } from './../../../store/user/user.selectors';

@Component({
  selector: 'hrms-inactive-employee',
  templateUrl: './inactive-employee.component.html',
  styleUrls: ['./inactive-employee.component.scss'],
})
export class InactiveEmployeeComponent implements OnInit, OnDestroy {
  isLoading = true;
  employees: IEmployee[] = [];
  pageSize = EPaginationPageSize.PAGE_SIZE_20;
  page = 0;
  totalEmployeeCount = 0;
  selectedCompany!: ICompany;

  private subscriptions: Subscription = new Subscription();

  constructor(
    private readonly employeeService: EmployeeService,
    protected modalService: NgbModal,
    private readonly toastService: ToastService,
    private readonly translateService: TranslateService,
    private store: Store<AppState>
  ) {}

  ngOnInit(): void {
    this.loadInactiveEmployees();
    this.subscriptions.add(
      this.store.select(selectCurrentCompany).subscribe(res => {
        this.selectedCompany = res?.company;
      })
    );
  }

  transition(page: number): void {
    this.page = page - 1;
    this.loadInactiveEmployees({
      page: this.page,
      size: this.pageSize,
    });
  }

  public loadInactiveEmployees(pagination?: Pagination): void {
    this.isLoading = true;
    this.employeeService.findAllInactiveEmployees(pagination).subscribe(
      (res: HttpResponse<IEmployeeWithCount>) => {
        this.employees = res.body?.employees ?? [];
        this.totalEmployeeCount = res.body?.count ?? 0;
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  public sendInvitation(employee_id?: number): void {
    if (employee_id) {
      this.isLoading = true;
      this.employeeService.sendInvitation(employee_id, this.selectedCompany).subscribe(
        () => {
          this.toastService.showSuccessToast(this.translateService.instant('hrmsApp.employee.sentEmail.success'));
          this.isLoading = false;
        },
        () => {
          this.toastService.showSuccessToast(this.translateService.instant('hrmsApp.employee.sentEmail.error'));
          this.isLoading = false;
        }
      );
    }
  }

  public delete(employee: IEmployee): void {
    const modalRef = this.modalService.open(EmployeeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.employee = employee;
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.DELETED) {
        this.loadInactiveEmployees();
        this.store.dispatch(employeeAction());
      }
    });
  }

  public filterFromSidebar(filter: any): void {
    this.store.dispatch(
      employeeFilterAction({
        filter: {
          ...filter,
          designations: [],
          skills: [],
        },
        pagination: {
          page: this.page,
          size: this.pageSize,
        },
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
