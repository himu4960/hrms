import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { combineLatest, Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';

import { AppState } from 'app/store';
import { selectApplicationSettings } from 'app/store/company-settings/company-settings.selectors';
import { selectCurrentCompany } from 'app/store/user/user.selectors';

import { IRole } from 'app/entities/role/role.model';
import { IEmployee } from 'app/employee/models/employee.model';
import { ApplicationSettings } from 'app/store/states/application-settings.interface';

import { PermissionService } from 'app/shared/service/permission.service';
import { EmployeeService } from 'app/employee/service/employee.service';
import { ToastService } from 'app/shared/components/toast/toast.service';

import { RoleType } from 'app/shared/enum';

@Component({
  selector: 'hrms-employee-details-navigation',
  templateUrl: './employee-details-navigation.component.html',
  styleUrls: ['./employee-details-navigation.component.scss'],
})
export class EmployeeDetailsNavigationComponent implements OnInit, OnDestroy {
  isLoading = false;

  employeeDetails: IEmployee | null = null;
  role: IRole | null = null;
  applicationSettings!: ApplicationSettings;
  employeeId = null;
  isEmployee = true;

  get roleType(): any {
    return RoleType;
  }

  @ViewChild('fileUpload', { static: false }) public fileUpload: ElementRef | undefined;

  private subscriptions: Subscription = new Subscription();

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private toastService: ToastService,
    private permissionService: PermissionService,
    private employeeService: EmployeeService
  ) {}

  ngOnInit(): void {
    this.loadAllData();
  }

  loadAllData(): void {
    this.isLoading = true;
    this.subscriptions.add(
      this.store
        .select(selectApplicationSettings)
        .pipe(
          switchMap((res: ApplicationSettings) => {
            if (res !== null) {
              this.applicationSettings = res;
            }
            this.isEmployee = this.permissionService.isEmployee();
            const userCompany$ = this.activatedRoute.data;
            const selectedCompany$ = this.store.select(selectCurrentCompany);
            return combineLatest([userCompany$, selectedCompany$]).pipe(
              tap(([userCompany, selectedCompany]: any) => {
                this.employeeId = selectedCompany?.employee?.id;
                this.employeeDetails = userCompany?.data?.employee || null;
                this.role = userCompany.data?.role || null;
                this.isLoading = false;
              })
            );
          })
        )
        .subscribe()
    );
  }

  showFileDialog(): boolean {
    if (this.fileUpload) {
      this.fileUpload.nativeElement.click();
    }
    return false;
  }

  public onFileChange(event: any, id: number | undefined): void {
    this.isLoading = true;
    const file = event.target.files[0];
    if (file && id) {
      this.employeeService.uploadFile(file, id).subscribe((employeeDetails: IEmployee) => {
        this.employeeDetails = employeeDetails;
        this.toastService.showSuccessToast(this.translateService.instant('hrmsApp.employee.imageUpload.success'));
        this.isLoading = false;
      });
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
