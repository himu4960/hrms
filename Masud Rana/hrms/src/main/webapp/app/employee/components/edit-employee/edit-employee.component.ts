import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';

import { combineLatest, Observable } from 'rxjs';
import { finalize, switchMap, tap } from 'rxjs/operators';

import { Store } from '@ngrx/store';
import { AppState } from 'app/store';

import { selectCurrentCompany } from 'app/store/user/user.selectors';
import { selectLanguage } from 'app/store/language/language.selectors';

import { IDesignation } from 'app/entities/designation/designation.model';
import { Employee, IEmployee, IEmployeeDesignation } from 'app/employee/models/employee.model';
import { IEmployeeSkill } from './../../models/employee.model';
import { ISkill } from './../../../entities/skill/skill.model';
import { IBranch } from './../../../entities/branch/branch.model';

import { EmployeeService } from 'app/employee/service/employee.service';
import { UtilService } from 'app/shared/service/util.service';
import { PermissionService } from 'app/shared/service/permission.service';
import { ToastService } from './../../../shared/components/toast/toast.service';

import { ApplicationSettings } from 'app/store/states/application-settings.interface';

import { noWhitespaceValidator } from 'app/shared/validators/no-whitespace.validator';

import { employeeAction } from './../../../store/employee/employee.actions';

@Component({
  selector: 'hrms-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.scss'],
})
export class EditEmployeeComponent implements OnInit {
  public isCollapsed: boolean[] = [];
  isLoading = true;
  isSaving = false;
  isEmployee = true;
  employeeId = null;
  languageCode = 'en';
  birthdatePicker: string | null = null;
  joinDatePicker: string | null = null;
  totalDesignationCount = 0;

  genders: any;
  bloodGroups: any;
  religions: any;
  maritalStatuses: any;
  branches: any;
  selectedBranch?: IBranch;
  languages: any;

  skills: ISkill[] = [];
  applicationSettings!: ApplicationSettings;

  employee_skill_ids: number[] = [];

  employeeDetails: IEmployee | null = null;

  employeeForm: FormGroup = this.fb.group({
    id: [null],
    code: [null, [Validators.maxLength(100)]],
    first_name: [null, [Validators.required, Validators.maxLength(50), noWhitespaceValidator]],
    middle_name: [null, [Validators.maxLength(50)]],
    last_name: [null, [Validators.maxLength(50), noWhitespaceValidator]],
    birth_day: [null],
    email: [null, [Validators.required, Validators.maxLength(100), noWhitespaceValidator]],
    personal_email: [null, [Validators.maxLength(100)]],
    phone: [null, [Validators.maxLength(30)]],
    nationality: [null, [Validators.maxLength(100)]],
    gender_id: [null],
    religion_id: [null],
    marital_status_id: [null],
    blood_group_id: [null],
    branch_id: [null, [Validators.required]],
    language_code: [null],
    join_date: [null],
  });

  file!: any;

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private utilService: UtilService,
    private employeeService: EmployeeService,
    private permissionService: PermissionService,
    private fb: FormBuilder,
    private toastService: ToastService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.fetchData();
  }

  public fetchData(): void {
    this.activatedRoute.data
      .pipe(
        switchMap((userCompany: any) => {
          this.isEmployee = this.permissionService.isEmployee();

          const employeeDetails = userCompany.data?.employee;
          this.employeeDetails = employeeDetails;
          this.birthdatePicker = this.employeeDetails?.dob ? this.utilService.format(Number(this.employeeDetails.dob)) : null;

          this.joinDatePicker = this.employeeDetails?.join_date ? this.utilService.format(Number(this.employeeDetails.join_date)) : null;

          this.employeeForm.patchValue({
            id: this.employeeDetails?.id,
            code: this.employeeDetails?.code,
            first_name: this.employeeDetails?.first_name,
            middle_name: this.employeeDetails?.middle_name,
            last_name: this.employeeDetails?.last_name,
            birth_day: this.employeeDetails?.dob,
            email: this.employeeDetails?.email,
            personal_email: this.employeeDetails?.personal_email,
            phone: this.employeeDetails?.phone,
            nationality: this.employeeDetails?.nationality,
            gender_id: this.employeeDetails?.gender_id,
            religion_id: this.employeeDetails?.religion_id,
            marital_status_id: this.employeeDetails?.marital_status_id,
            blood_group_id: this.employeeDetails?.blood_group_id,
            branch_id: this.employeeDetails?.branch?.id,
            language_code: this.languageCode,
            join_date: this.employeeDetails?.join_date,
          });

          const employee_designation_ids = employeeDetails?.employee_designations?.map(
            (designation: IEmployeeDesignation) => designation.designation_id
          );

          this.employee_skill_ids = employeeDetails?.skills.map((skill: ISkill) => skill.id);

          const genders$ = this.utilService.getGendersByLanguage(this.languageCode);
          const bloodGroups$ = this.utilService.getBloodGroupsByLanguage(this.languageCode);
          const religions$ = this.utilService.getReligionsByLanguage(this.languageCode);
          const maritalStatuses$ = this.utilService.getMaritalStatusesByLanguage(this.languageCode);
          const branches$ = this.utilService.getVisibleDesignations();
          const languages$ = this.store.select(selectLanguage);
          const skills$ = this.utilService.getDesignationBasedSkills({ employee_id: this.employeeDetails?.id });
          const applicationSettings$ = this.store.select(selectCurrentCompany);
          const selectedCompany$ = this.store.select(selectCurrentCompany);

          return combineLatest([
            genders$,
            bloodGroups$,
            religions$,
            maritalStatuses$,
            branches$,
            languages$,
            skills$,
            applicationSettings$,
            selectedCompany$,
          ]).pipe(
            tap(
              ([
                genders,
                bloodGroups,
                religions,
                maritalStatuses,
                branches,
                languages,
                skills,
                applicationSettings,
                selectedCompany,
              ]: any) => {
                this.genders = genders;
                this.bloodGroups = bloodGroups;
                this.religions = religions;
                this.maritalStatuses = maritalStatuses;
                this.languages = languages;
                this.branches = branches.map((branch: IBranch) => {
                  branch.designations?.map((designation: IDesignation) => {
                    designation.checked = employee_designation_ids?.includes(designation.id);
                    return designation;
                  });
                  return branch;
                });

                this.selectedBranch = this.branches.find((branch: IBranch) => branch.id === this.employeeDetails?.branch?.id);
                this.totalDesignationCount = this.selectedBranch?.designations?.length ?? 0;
                this.skills = this.skillsMapper(skills);
                this.applicationSettings = applicationSettings;
                this.employeeId = selectedCompany?.employee?.id;
              }
            )
          );
        })
      )
      .subscribe();
    this.isLoading = false;
  }

  public onBranchChange(): void {
    const branch_id = this.employeeForm.get('branch_id')!.value;
    this.employeeService
      .getBranchWiseDesignations(branch_id)
      .pipe(
        switchMap((res: HttpResponse<IBranch>) => {
          this.selectedBranch = res.body ?? undefined;
          this.totalDesignationCount = this.selectedBranch?.designations?.length ?? 0;
          return this.utilService.getDesignationBasedSkills({ employee_id: this.employeeDetails?.id, branch_id });
        })
      )
      .subscribe((skills: ISkill[]) => {
        this.skills = this.skillsMapper(skills);
        this.changeBranch();
      });
  }

  public onDesignationChange(e: any, employeeDesignation: IEmployeeDesignation): void {
    if (employeeDesignation?.employee_id && employeeDesignation?.designation_id) {
      if (e.target.checked) {
        this.employeeService
          .assignDesignation({ employee_id: employeeDesignation.employee_id, designation_id: employeeDesignation.designation_id })
          .subscribe((res: HttpResponse<any>) => {
            const skills = res.body;
            this.skills = skills;
          });
      } else {
        this.employeeService
          .unassignDesignation({ employee_id: employeeDesignation.employee_id, designation_id: employeeDesignation.designation_id })
          .subscribe((res: HttpResponse<any>) => {
            this.skills = res.body;
          });
      }
    }
  }

  public onSkillChange(e: any, employeeSkill: IEmployeeSkill): void {
    if (employeeSkill?.employee_id && employeeSkill?.skill_id) {
      if (e.target.checked) {
        this.employeeService.assignSkill(employeeSkill).subscribe();
      } else {
        this.employeeService.unassignSkill(employeeSkill).subscribe();
      }
    }
  }

  public onBirthDateChange(e: any): void {
    const value = e.target.value;
    this.employeeForm.patchValue({
      birth_day: new Date(value).getTime(),
    });
  }

  public onJoinDateChange(e: any): void {
    const value = e.target.value;
    this.employeeForm.patchValue({
      join_date: new Date(value).getTime(),
    });
  }

  public update(): void {
    this.isSaving = true;
    const employee = this.createFrom();
    if (employee.id) {
      this.subscribeToSaveResponse(this.employeeService.update(employee));
    }
  }

  public changeBranch(): void {
    this.isSaving = true;
    const employee = this.createFrom();
    if (employee.id) {
      this.subscribeToSaveResponse(this.employeeService.changeBranch(employee));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmployee>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isLoading = false;
    this.store.dispatch(employeeAction());
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected createFrom(): IEmployee {
    const employee = {
      ...new Employee(),
      id: this.employeeForm.get(['id'])?.value,
      code: this.employeeForm.get(['code'])?.value,
      phone: this.employeeForm.get(['phone'])?.value,
      email: this.employeeForm.get(['email'])?.value,
      first_name: this.employeeForm.get(['first_name'])?.value,
      middle_name: this.employeeForm.get(['middle_name'])?.value,
      last_name: this.employeeForm.get(['last_name'])?.value,
      dob: this.employeeForm.get(['birth_day'])?.value,
      personal_email: this.employeeForm.get(['personal_email'])?.value,
      nationality: this.employeeForm.get(['nationality'])?.value,
      gender_id: this.employeeForm.get(['gender_id'])?.value,
      religion_id: this.employeeForm.get(['religion_id'])?.value,
      marital_status_id: this.employeeForm.get(['marital_status_id'])?.value,
      blood_group_id: this.employeeForm.get(['blood_group_id'])?.value,
      branch_id: this.employeeForm.get(['branch_id'])?.value,
      join_date: this.employeeForm.get(['join_date'])?.value,
    };
    return employee;
  }

  private skillsMapper(skills: ISkill[]): ISkill[] {
    return skills.map((skill: ISkill) => {
      if (skill?.id) {
        skill.checked = this.employee_skill_ids.includes(skill.id);
      }
      return skill;
    });
  }
}
