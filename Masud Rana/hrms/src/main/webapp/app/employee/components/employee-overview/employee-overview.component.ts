import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { combineLatest, Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

import { AppState } from 'app/store';

import { selectApplicationSettings } from 'app/store/company-settings/company-settings.selectors';
import { selectCurrentCompany } from 'app/store/user/user.selectors';

import { IRole } from 'app/entities/role/role.model';
import { IWorkShift } from 'app/shift-planning/models/work-shift.model';
import { IEmployee, IEmployeeActivation } from 'app/employee/models/employee.model';
import { ApplicationSettings } from 'app/store/states/application-settings.interface';
import { ICompany } from 'app/company/company.model';

import { employeeAction } from 'app/store/employee/employee.actions';

import { PermissionService } from 'app/shared/service/permission.service';
import { EmployeeService } from 'app/employee/service/employee.service';
import { WorkShiftService } from 'app/shift-planning/service/work-shift.service';
import { ToastService } from 'app/shared/components/toast/toast.service';

@Component({
  selector: 'hrms-employee-overview',
  templateUrl: './employee-overview.component.html',
  styleUrls: ['./employee-overview.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class EmployeeOverviewComponent implements OnInit {
  employeeDetails: IEmployee | null = null;
  isLoading = false;
  applicationSettings!: ApplicationSettings;
  upcomingShifts: IWorkShift[] = [];
  role: IRole | null = null;
  employeeId = null;
  isOwner = false;
  isEmployee = true;
  isActive = false;
  action: any;
  selectedCompany!: ICompany;

  @ViewChild('adminActionModal') adminActionModal!: NgbModal;

  private subscriptions: Subscription = new Subscription();

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private workShiftService: WorkShiftService,
    private employeeService: EmployeeService,
    private permissionService: PermissionService,
    private readonly toastService: ToastService,
    private readonly translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.loadEmployeeDetails();
  }

  public sendInvitation(employee_id?: number): void {
    if (employee_id) {
      this.isLoading = true;
      this.employeeService.sendInvitation(employee_id, this.selectedCompany).subscribe(
        () => {
          this.toastService.showSuccessToast(this.translateService.instant('hrmsApp.employee.sentEmail.success'));
          this.isLoading = false;
        },
        () => {
          this.toastService.showSuccessToast(this.translateService.instant('hrmsApp.employee.sentEmail.error'));
          this.isLoading = false;
        }
      );
    }
  }

  loadEmployeeDetails(): void {
    this.isLoading = true;
    this.subscriptions.add(
      this.activatedRoute.data
        .pipe(
          switchMap(userCompany => {
            this.employeeDetails = userCompany?.data?.employee || null;
            this.role = userCompany.data?.role || null;
            this.isOwner = userCompany.data?.is_owner;
            this.isActive = userCompany.data?.is_active;
            this.isEmployee = this.permissionService.isEmployee();
            const applicationSettings$ = this.store.select(selectApplicationSettings);
            const employeeId: number = userCompany?.data?.employee?.id;
            const upcomingShifts$ = this.workShiftService.getUpcomingShiftsByEmployeeId(employeeId, false);
            const selectedCompany$ = this.store.select(selectCurrentCompany);
            return combineLatest([applicationSettings$, selectedCompany$, upcomingShifts$]).pipe(
              tap(([applicationSettings, selectedCompany, upcomingShifts]: any) => {
                if (applicationSettings !== null) {
                  this.applicationSettings = applicationSettings;
                }
                this.upcomingShifts = upcomingShifts.body;
                this.selectedCompany = selectedCompany;
                this.employeeId = selectedCompany?.employee?.id;
              })
            );
          })
        )
        .subscribe()
    );
  }

  showAdminActionModal(id: any, activeStatus: any): void {
    this.action = { id, activeStatus };
    this.modalService.open(this.adminActionModal, { size: 'md', backdrop: 'static' });
  }

  changeActiveStatus(employeeId: any, activeStatus: any): void {
    if (employeeId) {
      this.isLoading = true;
      const employeeActivation: IEmployeeActivation = {
        employee_id: employeeId,
        is_active: !activeStatus,
      };
      this.employeeService.toggleActivation(employeeActivation).subscribe(res => {
        this.isLoading = false;
        this.store.dispatch(employeeAction());
        this.employeeDetails = res.body.employee;
        this.isOwner = res.body.is_owner;
        this.isActive = res.body.is_active;
        this.modalService.dismissAll();
      });
    }
  }
}
