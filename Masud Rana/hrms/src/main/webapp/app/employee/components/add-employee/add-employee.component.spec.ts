jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { of, Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import { EmployeeService } from '../../service/employee.service';

import { AddEmployeeComponent } from './add-employee.component';
import { Employee } from '../../models/employee.model';

describe('ListComponent', () => {
  describe('Employee Update Component', () => {
    let comp: AddEmployeeComponent;
    let fixture: ComponentFixture<AddEmployeeComponent>;
    let activatedRoute: ActivatedRoute;
    let employeeService: EmployeeService;

    let fakeService: {};

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, StoreModule.forRoot({})],
        declarations: [AddEmployeeComponent],
        providers: [{ provide: TranslateService, useValue: fakeService }, FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(AddEmployeeComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AddEmployeeComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      employeeService = TestBed.inject(EmployeeService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      describe('save', () => {
        it('Should call create service on save for new entity', () => {
          // GIVEN
          const saveSubject = new Subject();
          const employee = new Employee();
          spyOn(employeeService, 'create').and.returnValue(saveSubject);
          spyOn(comp, 'previousState');
          activatedRoute.data = of({ employee });

          // WHEN
          comp.save();
          expect(comp.isSaving).toEqual(true);
          saveSubject.next(new HttpResponse({ body: employee }));
          saveSubject.complete();

          // THEN
          expect(comp.isSaving).toEqual(false);
          expect(comp.previousState).toHaveBeenCalled();
        });
      });
    });
  });
});
