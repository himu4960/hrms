import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';
import { combineLatest, of, Subscription } from 'rxjs';

import { Store } from '@ngrx/store';
import { AppState } from 'app/store';

import { selectCurrentCompany } from 'app/store/user/user.selectors';

import { PermissionService } from 'app/shared/service/permission.service';

import { ApplicationSettings } from 'app/store/states/application-settings.interface';
import { IEmployee } from 'app/employee/models/employee.model';
import { selectApplicationSettings } from 'app/store/company-settings/company-settings.selectors';

@Component({
  selector: 'hrms-password-management',
  templateUrl: './password-management.component.html',
  styleUrls: ['./password-management.component.scss'],
})
export class PasswordManagementComponent implements OnInit {
  applicationSettings!: ApplicationSettings;
  employeeDetails: IEmployee | null = null;
  employeeId = null;
  isEmployee = true;

  private subscriptions: Subscription = new Subscription();

  constructor(private store: Store<AppState>, private activatedRoute: ActivatedRoute, private permissionService: PermissionService) {}

  ngOnInit(): void {
    this.subscriptions.add(
      this.store
        .select(selectApplicationSettings)
        .pipe(
          switchMap((res: ApplicationSettings) => {
            if (res !== null) {
              this.applicationSettings = res;
            }
            this.isEmployee = this.permissionService.isEmployee();
            const employeeDetails$ = this.activatedRoute.data;
            const selectedCompany$ = this.store.select(selectCurrentCompany);
            return combineLatest([employeeDetails$, selectedCompany$]).pipe(
              tap(([employeeDetails, selectedCompany]: any) => {
                this.employeeId = selectedCompany?.employee?.id;
                this.employeeDetails = employeeDetails?.EmployeeOverview || null;
              })
            );
          })
        )
        .subscribe()
    );
  }
}
