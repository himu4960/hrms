import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { PasswordResetFinishComponent } from 'app/account/password-reset/finish/password-reset-finish.component';
import { PasswordStrengthBarComponent } from 'app/account/password/password-strength-bar/password-strength-bar.component';
import { PasswordComponent } from 'app/account/password/password.component';

@NgModule({
  declarations: [PasswordComponent, PasswordStrengthBarComponent, PasswordResetFinishComponent],
  imports: [SharedModule],
  exports: [PasswordComponent, PasswordStrengthBarComponent, PasswordResetFinishComponent],
})
export class PasswordManagementModule {}
