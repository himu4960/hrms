import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { PasswordManagementModule } from './password-management.module';
import { EmployeeOnlineModule } from '../components/employee-online/employee-online.module';
import { EmployeeNavigationModule } from '../components/employee-navigation/employee-navigation.module';
import { UpcomingShiftsModule } from 'app/home/components/upcoming-shifts/upcoming-shifts.module';

import { employeeRoutes } from '../routes';

import { EmployeeListComponent } from '../components/employee-list/employee-list.component';
import { AddEmployeeComponent } from '../components/add-employee/add-employee.component';
import { EmployeeOverviewComponent } from '../components/employee-overview/employee-overview.component';
import { EditEmployeeComponent } from '../components/edit-employee/edit-employee.component';
import { PasswordManagementComponent } from '../components/password-management/password-management.component';
import { PermissionsManagementComponent } from '../components/permissions-management/permissions-management.component';
import { InactiveEmployeeComponent } from '../components/inactive-employee/inactive-employee.component';
import { EmployeeComponent } from '../employee.component';
import { EmployeeSidebarComponent } from '../components/employee-sidebar/employee-sidebar.component';
import { EmployeeDeleteDialogComponent } from '../components/inactive-employee/delete/employee-delete-dialog.component';
import { EmployeeDetailsNavigationComponent } from '../components/employee-overview/employee-details-navigation/employee-details-navigation.component';

import { DisableLinkDirective } from 'app/shared/directives/disable-link.directive';

@NgModule({
  imports: [
    FormsModule,
    SharedModule,
    PasswordManagementModule,
    EmployeeOnlineModule,
    EmployeeNavigationModule,
    UpcomingShiftsModule,
    RouterModule.forChild(employeeRoutes),
  ],
  declarations: [
    EmployeeListComponent,
    AddEmployeeComponent,
    EmployeeOverviewComponent,
    EditEmployeeComponent,
    PasswordManagementComponent,
    PermissionsManagementComponent,
    DisableLinkDirective,
    InactiveEmployeeComponent,
    EmployeeSidebarComponent,
    EmployeeComponent,
    EmployeeDeleteDialogComponent,
    EmployeeDetailsNavigationComponent,
  ],
  entryComponents: [EmployeeDeleteDialogComponent],
})
export class EmployeeModule {}
