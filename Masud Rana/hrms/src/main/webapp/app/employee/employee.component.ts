import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { AppState } from 'app/store';

import { selectEmployeeData } from './../store/employee/employee.selectors';

import { employeeAction } from './../store/employee/employee.actions';

import { IEmployeeData } from 'app/shared/models/common.model';
import { IBranch } from 'app/entities/branch/branch.model';

@Component({
  selector: 'hrms-employee',
  templateUrl: './employee.component.html',
})
export class EmployeeComponent implements OnInit, OnDestroy {
  branches: IBranch[] = [];
  totalEmployee = 0;
  notActivatedEmployeeCount = 0;
  disabledEmployeeCount = 0;

  private subscriptions: Subscription = new Subscription();
  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store.dispatch(employeeAction());
    this.loadEmployeeCommonData();
  }

  public loadEmployeeCommonData(): void {
    this.subscriptions.add(
      this.store.select(selectEmployeeData).subscribe((data: IEmployeeData) => {
        this.branches = data.branches || [];
        this.totalEmployee = data.total_employee_count;
        this.notActivatedEmployeeCount = data.not_activated;
        this.disabledEmployeeCount = data.disabled;
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
