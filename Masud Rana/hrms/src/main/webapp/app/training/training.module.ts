import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';

import { TrainingComponent } from './training.component';

@NgModule({
  declarations: [TrainingComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: TrainingComponent,
        canActivate: [AuthGuard],
      },
    ]),
  ],
})
export class TrainingModule {}
