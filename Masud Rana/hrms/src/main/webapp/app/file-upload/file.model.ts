export interface IFile {
  id: number;
  name: string;
  mimetype: string;
  orginal_filename: string;
  path_url: string;
  file_size: number;
  download_count: number;
  created_at: number;
  updated_at: number;
}
