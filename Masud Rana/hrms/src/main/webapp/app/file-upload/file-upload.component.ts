import { Component, ElementRef, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { AppState } from 'app/store';

import { IMenu } from 'app/entities/menu/menu.model';
import { IUser } from '../entities/user/user.model';

import { selectCurrentCompany, selectUser } from 'app/store/user/user.selectors';

import { UserCompany } from '../store/states/user.interface';

import { FileService } from './file.service';
import { ToastService } from '../shared/components/toast/toast.service';

import { noWhitespaceValidator } from 'app/shared/validators/no-whitespace.validator';

import { locationSideMenus } from 'app/shared/menus';

@Component({
  selector: 'hrms-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
})
export class FileUploadComponent implements OnInit, OnDestroy {
  @ViewChild('uploadFile', { static: false }) fileUploader?: ElementRef;
  isSaving = false;
  sideMenus: IMenu[] = locationSideMenus;
  fileName = 'No File Chosen';
  fileUploadForm = this.fb.group({
    name: [null, [Validators.required, Validators.maxLength(150), noWhitespaceValidator]],
    file: [null],
    branch_id: [null],
    created_by: [null],
  });

  private subscriptions: Subscription = new Subscription();
  constructor(
    private fb: FormBuilder,
    private fileUploadService: FileService,
    private store: Store<AppState>,
    private toastService: ToastService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.subscriptions.add(
      this.store.select(selectCurrentCompany).subscribe((res: UserCompany) => {
        if (res !== null) {
          if (res?.employee !== undefined) {
            this.fileUploadForm.patchValue({
              branch_id: res.employee.branch_id,
            });
          }
        }
      })
    );

    this.subscriptions.add(
      this.store.select(selectUser).subscribe((res: IUser) => {
        if (res !== null) {
          this.fileUploadForm.patchValue({
            created_by: res.id,
          });
        }
      })
    );
  }

  public onUpload(): void {
    this.isSaving = true;
    const file = this.fileUploadForm.value;

    if (file.name && file.file) {
      this.fileUploadService.onUpload(file.file, file.name, file.branch_id, file.created_by).subscribe(
        (res: any) => {
          this.isSaving = false;
          if (this.fileUploader) {
            this.toastService.showSuccessToast(this.translateService.instant('file.upload.success'));
            this.fileUploader.nativeElement.value = '';
          }
          this.fileUploadForm.reset();
        },
        () => {
          this.isSaving = false;
        }
      );
    } else {
      this.isSaving = false;
      this.toastService.showDangerToast(this.translateService.instant('file.upload.error'));
    }
  }

  public chooseFileHandler(e: any): void {
    const file = e.target.files[0];
    const name = file.name.split('.')[0];
    this.fileName = name;

    this.fileUploadForm.patchValue({
      name,
      file,
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
