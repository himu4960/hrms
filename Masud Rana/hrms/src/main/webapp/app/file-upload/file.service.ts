import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IFile } from './file.model';

import { ApplicationConfigService } from '../core/config/application-config.service';

export type IFileResponseType = HttpResponse<IFile>;
export type IFileArrayResponseType = HttpResponse<IFile[]>;

@Injectable({ providedIn: 'root' })
export class FileService {
  public resourceUrl = this.applicationConfigService.getEndpointFor(`files`);
  constructor(private http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  onUpload(file: any, name: string, branch_id: string, creator: string): Observable<any> {
    const formData = new FormData();
    formData.append('file', file, file.name);
    formData.append('name', name);
    branch_id && formData.append('branch_id', branch_id);
    creator && formData.append('created_by', creator);
    return this.http.post(`${this.resourceUrl}/upload`, formData);
  }

  findAll(): Observable<IFileArrayResponseType> {
    return this.http.get<IFile[]>(this.resourceUrl, {
      observe: 'response',
    });
  }

  onDownload(file: IFile): Observable<IFileResponseType> {
    return this.http.post<IFile>(`${this.resourceUrl}/download`, file, {
      observe: 'response',
    });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, {
      observe: 'response',
    });
  }
}
