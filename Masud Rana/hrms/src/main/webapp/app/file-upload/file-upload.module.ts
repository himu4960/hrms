import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { CommonSidebarModule } from 'app/shared/components/common-sidebar/common-sidebar.module';

import { FileUploadComponent } from './file-upload.component';

@NgModule({
  imports: [
    SharedModule,
    CommonSidebarModule,
    RouterModule.forChild([
      {
        path: '',
        component: FileUploadComponent,
      },
    ]),
  ],
  declarations: [FileUploadComponent],
  exports: [FileUploadComponent],
})
export class FileUploadModule {}
