import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { EmployeeConfirmationService } from './employee-confirmation.service';

@Component({
  selector: 'hrms-employee-confirmation',
  templateUrl: './employee-confirmation.component.html',
  styleUrls: ['./employee-confirmation.component.scss'],
})
export class EmployeeConfirmationComponent {
  token = '';
  isSuccess = false;
  isError = false;
  successMessage: any = null;
  error: any = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private employeeConfirmationService: EmployeeConfirmationService,
    private translateService: TranslateService
  ) {
    this.route.params.subscribe(params => {
      const token = params['token'];
      if (token) {
        this.token = token;
      } else {
        this.router.navigateByUrl('404');
      }
    });
  }

  backToLogin(): void {
    this.router.navigate(['/login']);
  }

  confirm(): void {
    this.employeeConfirmationService.confirm(this.token).subscribe(res => {
      if (res) {
        this.isSuccess = true;
        this.successMessage = this.translateService.instant('hrmsApp.employee.alert.success.hasBeenConfirmed');
      }
    });
  }
}
