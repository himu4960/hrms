import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EmployeeConfirmationService {
  public resourceUrl = this.applicationConfigService.getEndpointFor(`register`);
  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  confirm(token: string): Observable<any> {
    return this.http.get<any>(`${this.resourceUrl}/employee/confirm/${token}`);
  }
}
