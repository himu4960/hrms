import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';

import { EmployeeConfirmationComponent } from './employee-confirmation.component';

@NgModule({
  declarations: [EmployeeConfirmationComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: EmployeeConfirmationComponent,
      },
    ]),
  ],
})
export class EmployeeConfirmationModule {}
