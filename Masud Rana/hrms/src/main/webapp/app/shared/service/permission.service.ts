import { Injectable } from '@angular/core';

import { AuthServerProvider } from 'app/core/auth/auth-jwt.service';
import { StateStorageService } from 'app/core/auth/state-storage.service';
import { permissionRoleTypeCode, RoleType } from 'app/shared/enum';
import { UserCompany } from 'app/store/states/user.interface';

@Injectable({
  providedIn: 'root',
})
export class PermissionService {
  constructor(private storageService: StateStorageService, private authServerProvider: AuthServerProvider) {}

  getUserType(): string {
    const selectedCompany: UserCompany = this.storageService.getCurrentCompany();
    return selectedCompany.role.name;
  }

  getPermissionRoleTypeCode(roleName: string): number {
    const permissionCode = Object.values(permissionRoleTypeCode).find(
      value => this.getKeyByValue(permissionRoleTypeCode, value) === roleName
    );
    return permissionCode ? permissionCode : 0;
  }

  isEmployee(): boolean {
    const jwtToken = this.authServerProvider.getToken();
    const decodedJwtToken = this.authServerProvider.decodeJwtToken(jwtToken);
    return decodedJwtToken?.is_employee;
  }

  isOwner(): boolean {
    return this.getUserType() === RoleType.OWNER;
  }

  isSuperAdmin(): boolean {
    return this.getUserType() === RoleType.SUPER_ADMIN;
  }

  getKeyByValue(object: any, value: any): any {
    return Object.keys(object).find(key => object[key] === value);
  }
}
