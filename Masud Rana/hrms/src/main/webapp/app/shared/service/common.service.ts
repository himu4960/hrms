import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { exhaustMap } from 'rxjs/operators';

import { ITimeSheetLateSummary } from 'app/reports/time-sheet/models/time-sheet-report.model';
import { IEmployeeData, IHomeData } from '../models/common.model';

import { ETimeSheetStatus } from '../enum/time-sheet.enum';

import { EmployeeService } from 'app/employee/service/employee.service';
import { ApplicationConfigService } from './../../core/config/application-config.service';

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('common-data');

  constructor(
    protected http: HttpClient,
    private applicationConfigService: ApplicationConfigService,
    private employeeService: EmployeeService
  ) {}

  getDashboardData(): Observable<IHomeData> {
    return this.http.get(`${this.resourceUrl}/home`, { observe: 'response' }).pipe(
      exhaustMap((res: HttpResponse<any>) => {
        const homeData = {
          all_shift_count: res.body?.all_shift_count ?? 0,
          unpublished_shift_count: res.body?.unpublished_shift_count ?? 0,
          leave_request_count: res.body?.leave_request_count ?? 0,
          time_sheet_count: res.body?.time_sheet_count ?? 0,
          on_leaves: res.body?.on_leaves ?? [],
          anniversaries: this.employeeService.findTodayAnniversaries(res.body?.anniversaries),
          announcements: res.body?.announcements ?? [],
          birthdays: this.employeeService.findTodayBirthdays(res.body?.birthdays),
          files: res.body?.files ?? [],
          lates: res.body?.lates?.filter((late: ITimeSheetLateSummary) => late.late_status === ETimeSheetStatus.YES) ?? [],
          upcoming_shifts: res.body?.upcoming_shifts ?? [],
        };

        return of(homeData);
      })
    );
  }

  getEmployeeData(): Observable<IEmployeeData> {
    return this.http.get(`${this.resourceUrl}/employee`, { observe: 'response' }).pipe(
      exhaustMap((res: HttpResponse<any>) => {
        const employeeData = {
          employees: res.body?.employees ?? [],
          branches: res.body?.branches ?? [],
          skills: res.body?.skills ?? [],
          total_employee_count: res.body?.total_employee_count ?? 0,
          not_activated: res.body?.not_activated ?? 0,
          disabled: res.body?.disabled ?? 0,
        };

        return of(employeeData);
      })
    );
  }
}
