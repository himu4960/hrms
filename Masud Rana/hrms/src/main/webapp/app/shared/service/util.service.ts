import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import * as Moment from 'moment';
import { DateRange, extendMoment } from 'moment-range';
import * as dayjs from 'dayjs';
import * as weekday from 'dayjs/plugin/weekday';
import * as duration from 'dayjs/plugin/duration';
dayjs.extend(weekday);
dayjs.extend(duration);

import { HYPHEN_SEPARATED_DATE } from './../../config/input.constants';

import { ApplicationConfigService } from 'app/core/config/application-config.service';

import { IDateBetween } from './../../time-clock/models/time-clock.model';
import { IFilterDateRange } from '../forms/generic-filter-form/models/generic-filter.model';
import { ILanguage } from '../models/utils.model';

import { ELocalStorageKeys } from 'app/core/auth/state-storage.service';

import { createRequestOption } from 'app/core/request/request-util';
import { DEFAULT_LANGUAGE_CODE } from 'app/app.constants';
@Injectable({
  providedIn: 'root',
})
export class UtilService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('');
  public moment = extendMoment(Moment);

  constructor(
    protected http: HttpClient,
    private applicationConfigService: ApplicationConfigService,
    private translateService: TranslateService
  ) {}

  getActiveLanguages(): Observable<any> {
    return this.http.get(`${this.resourceUrl}languages`);
  }

  setDefaultlanguage(): void {
    this.getActiveLanguages().subscribe(
      (languages: ILanguage[]) => {
        const defaultLanguage: ILanguage | undefined = languages.find(language => language.is_default);
        if (defaultLanguage?.code) {
          this.translateService.setDefaultLang(defaultLanguage.code);
          this.translateService.use(defaultLanguage.code);
        }
      },
      () => {
        this.translateService.setDefaultLang(DEFAULT_LANGUAGE_CODE);
        this.translateService.use(DEFAULT_LANGUAGE_CODE);
      }
    );
  }

  getAppSetupData(): Observable<any> {
    return this.http.get(`${this.resourceUrl}auth/setup`);
  }

  getRoles(): Observable<any> {
    return this.http.get(`${this.resourceUrl}roles`);
  }

  getTimeZones(): Observable<any> {
    return this.http.get(`${this.resourceUrl}time-zones`);
  }

  getTimeZoneById(id: number): Observable<any> {
    return this.http.get(`${this.resourceUrl}time-zones/${id}`);
  }

  getEmployees(): Observable<any> {
    return this.http.get(`${this.resourceUrl}employees/dropdown`);
  }

  getEmployeeById(id: number | undefined): Observable<any> {
    return this.http.get(`${this.resourceUrl}employees/${id}`);
  }

  getBranches(): Observable<any> {
    return this.http.get(`${this.resourceUrl}branches`);
  }

  getVisibleDesignations(): Observable<any> {
    return this.http.get(`${this.resourceUrl}designations/visible`);
  }

  getSkills(): Observable<any> {
    return this.http.get(`${this.resourceUrl}skills`);
  }

  getDesignationBasedSkills(req?: any): Observable<any> {
    const options = createRequestOption(req);
    return this.http.get(`${this.resourceUrl}designations/skills`, { params: options });
  }

  getDesignations(): Observable<any> {
    return this.http.get(`${this.resourceUrl}designations`);
  }

  getDesignationsByEmployeeId(employeeId: number | undefined): Observable<any> {
    return this.http.get(`${this.resourceUrl}designations/employee/${employeeId}`);
  }

  getLeaveType(): Observable<any> {
    return this.http.get(`${this.resourceUrl}leave-types`);
  }

  getGendersByLanguage(langCode: string): Observable<any> {
    return this.http.get(`${this.resourceUrl}genders/${langCode}`);
  }

  getMaritalStatusesByLanguage(langCode: string): any {
    return this.http.get(`${this.resourceUrl}marital-statuses/${langCode}`);
  }

  getReligionsByLanguage(langCode: string): Observable<any> {
    return this.http.get(`${this.resourceUrl}religions/${langCode}`);
  }

  getBloodGroupsByLanguage(langCode: string): Observable<any> {
    return this.http.get(`${this.resourceUrl}blood-groups/${langCode}`);
  }

  getGeoLocation(ip: string): Observable<any> {
    return this.http.get(`${this.resourceUrl}geo-location/${ip}`);
  }

  getTimeFromSecond(second: number): string {
    const hours = Math.floor(second / 3600);
    const minutes = Math.floor((second - hours * 3600) / 60);

    if (!hours) {
      return `${minutes}m`;
    }

    return `${hours}h ${minutes}m`;
  }

  convertHoursToSeconds(hour: string): number {
    const [hours, minutes] = hour.split(':');
    return Number(hours) * 60 * 60 + Number(minutes) * 60;
  }

  convertTimeStampToLocalTime(timeStampValue: number, timeZone: string): string {
    return new Date(Number(timeStampValue)).toLocaleString('en-US', { timeZone: timeZone });
  }

  convertSecondsIntoHoursAndMinutes(seconds: number): string {
    return this.moment(seconds * 1000)
      .toISOString()
      .substring(11, 16);
  }

  convertYearMonthDayIntoDate(fullYear: { year: number; month: number; day: number }): Date {
    const date = new Date();
    date.setFullYear(fullYear.year, fullYear.month - 1, fullYear.day);
    return date;
  }

  /**
   * @param date
   * @returns YYYY-MM-DD
   */
  format(date?: any): string {
    if (date && date !== undefined) {
      return dayjs(date).format(HYPHEN_SEPARATED_DATE);
    }
    return '';
  }

  formatTimeTo24Hours(time?: string | null): string | null {
    if (time && time !== undefined) {
      return dayjs(time, 'h:mm A', 'en', true).isValid() ? dayjs(time, 'h:mm A').format('HH:mm:ss') : null;
    }
    return null;
  }

  isTwoDatesAreSame(pastDay: number, clockStartDate: any): boolean {
    const formattedPastDate = this.format(dayjs().add(pastDay, 'day'));
    const formattedClockStartDate = dayjs(clockStartDate).format(HYPHEN_SEPARATED_DATE);
    return formattedPastDate <= formattedClockStartDate;
  }

  /**
   * @param date
   * @returns HH:mm:ss
   */
  public getTimeDifferenceFromCurrentTime(value?: Date | null): string {
    if (value && value !== undefined) {
      const startTime = dayjs(value);
      const currentTime = dayjs();
      const diff = dayjs.duration(currentTime.diff(startTime));
      return diff.format('HH:mm:ss').toString();
    }
    return '00:00:00';
  }

  /**
   * @param date
   * @returns HH:mm 10:30
   */
  formatTime(date?: string | null | number): string {
    if (date && date !== undefined) {
      return this.moment(date).format('HH:mm');
    }
    return '';
  }

  combineDateAndTime(date?: string | null, time?: string | null): string {
    if (date && time) {
      return this.moment(`${date} ${time}`).toISOString();
    }
    return '';
  }

  combineFromDatePicker(start_date: string, start_time: string): number {
    const start_date_formatted = this.format(start_date);
    return new Date(this.combineDateAndTime(start_date_formatted.toString(), start_time?.toString())).getTime();
  }

  getTimeDifference(startTime: string, endTime: string): string {
    const timeDiff = this.convertHoursToSeconds(endTime) - this.convertHoursToSeconds(startTime);
    return `${this.convertSecondsIntoHoursAndMinutes(timeDiff)}`;
  }

  getDateDifferenceInDays(newDate: Date, oldDate: Date): number {
    return Math.round((newDate.getTime() - oldDate.getTime()) / (1000 * 3600 * 24));
  }

  /**
   * @param date only takes the date number, ex: 24, 01, 30
   * @param time only takes 17:00, 17:00 format
   * @returns example: Sun Oct 16 2022 09:00:11 GMT+0600 (Bangladesh Standard Time)
   */
  getFormattedDateToCreateWorkShift(date: Date, time: string): any {
    const formattedDate = this.moment();
    formattedDate.set('year', date.getFullYear());
    formattedDate.set('month', date.getMonth());
    formattedDate.set('date', date.getDate());
    formattedDate.set('hour', parseInt(time.split(':')[0], 10));
    formattedDate.set('minute', parseInt(time.split(':')[1], 10));
    return formattedDate;
  }

  /**
   * @param startDate
   * @param endDate
   * @returns DateRange
   */
  getDateRange(startDate: any, endDate: any): DateRange {
    return this.moment.range(startDate, endDate);
  }

  /**
   * @param date takes 2022-10-16
   * @param time takes 09:30 format
   * @returns example: Sun Oct 16 2022 09:00:11 GMT+0600 (Bangladesh Standard Time)
   */
  getDateByDateAndTime(date: string, time: string): any {
    return this.moment(`${date} ${time}`);
  }

  /**
   * @param date
   * @returns example: Sun Oct 16 2022 09:00:11 GMT+0600 (Bangladesh Standard Time)
   */
  getDateByDate(date: string): any {
    return this.moment(`${date}`);
  }

  /**
   * Takes Current Date (Today) and Start Date (Future Date)
   * Then converts both into milliseconds to calculate time
   * @param currentDate
   * @param startDate
   * @returns days difference as number
   */
  getDayDifference(currentDate: Date, startDate: Date): number {
    const difference = startDate.getTime() - currentDate.getTime();
    const TotalDays = Math.ceil(difference / (1000 * 3600 * 24));
    return TotalDays;
  }

  /**
   * Takes Current Date (Today) and Day Count
   * @param currentDate
   * @param days
   * @returns future date as Date
   */
  getFutureDateAfterDays(currentDate: Date, days: number): Date {
    const futureDate = new Date();
    futureDate.setDate(currentDate.getDate() + days);
    return futureDate;
  }

  generateDateColumnsByRange(dateRange: IFilterDateRange): string[] {
    const dateColumns: string[] = [];
    const startDate = new Date(dateRange.start_date);
    startDate.setDate(startDate.getDate() - 1);
    const endDate = new Date(dateRange.end_date);
    endDate.setDate(endDate.getDate() + 1);
    const totalDays = this.getDateDifferenceInDays(endDate, new Date(dateRange.start_date));

    for (let i = 0; i < totalDays; i++) {
      startDate.setDate(startDate.getDate() + 1);
      const currentDate = this.format(startDate);
      dateColumns.push(currentDate);
    }
    return dateColumns;
  }

  hasKeys(obj: any): boolean {
    if (!obj) {
      return false;
    }
    return Object.keys(obj).length === 0;
  }

  isObject(obj: any): boolean {
    if (!obj) {
      return false;
    }
    return Object.getPrototypeOf(obj) === Object.prototype;
  }

  // ############ Filter by days ############## //

  public getToday(): number {
    return new Date(this.format(dayjs())).getTime();
  }

  public getLastDayDate(day: number): number {
    return new Date(this.format(dayjs().add(day, 'day'))).getTime();
  }

  public getThisWeekDate(): number {
    const this_week_first_day = dayjs().weekday(0);
    return new Date(this.format(this_week_first_day)).getTime();
  }

  public getLastWeekDate(): IDateBetween {
    const start_date = new Date(this.format(dayjs().weekday(-7))).getTime();
    const end_date = new Date(this.format(dayjs().weekday(-1))).getTime();

    return { start_date, end_date };
  }

  public generateRandomString(length: number): string {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
    }

    return result;
  }
}
