import { NgModule } from '@angular/core';

import { SharedLibsModule } from './shared-libs.module';

import { TranslateDirective } from './directives/language/translate.directive';
import { HasAnyAuthorityDirective } from './directives/auth/has-any-authority.directive';
import { SortByDirective } from './directives/sort/sort-by.directive';
import { SortDirective } from './directives/sort/sort.directive';
import { InputTrimDirective } from './directives/input-trim.directive';

import { AlertComponent } from './components/alert/alert.component';
import { AlertErrorComponent } from './components/alert/alert-error.component';
import { ItemCountComponent } from './components/pagination/item-count.component';
import { ToastComponent } from './components/toast/toast.component';
import { FullScreenSpinnerComponent } from './components/full-screen-spinner/full-screen-spinner.component';
import { TransparentSpinnerComponent } from './components/transparent-spinner/transparent-spinner.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';

import { DurationPipe } from './pipes/date/duration.pipe';
import { FormatTimePipe } from './pipes/time/format-time.pipe';
import { ConvertHoursMinutesPipe } from './pipes/time/convert-hours-minutes.pipe';
import { FormatMediumDatePipe } from './pipes/date/format-medium-date.pipe';
import { FormatMediumDatetimePipe } from './pipes/date/format-medium-datetime.pipe';
import { FindLanguageFromKeyPipe } from './pipes/languages/find-language-from-key.pipe';
import { SecondsToTimePipe } from 'app/shared/pipes/time-value/time-value.pipe';
import { MinutesToDaysPipe } from 'app/shared/pipes/time-value/minutes-to-days.pipe';

import { CommonService } from './service/common.service';

@NgModule({
  imports: [SharedLibsModule],
  declarations: [
    TransparentSpinnerComponent,
    FullScreenSpinnerComponent,
    FindLanguageFromKeyPipe,
    TranslateDirective,
    AlertComponent,
    AlertErrorComponent,
    HasAnyAuthorityDirective,
    DurationPipe,
    FormatMediumDatetimePipe,
    FormatMediumDatePipe,
    SortByDirective,
    SortDirective,
    ItemCountComponent,
    SecondsToTimePipe,
    MinutesToDaysPipe,
    FormatTimePipe,
    ConvertHoursMinutesPipe,
    ToastComponent,
    InputTrimDirective,
    SidebarComponent,
  ],
  providers: [CommonService],
  exports: [
    TransparentSpinnerComponent,
    FullScreenSpinnerComponent,
    SharedLibsModule,
    FindLanguageFromKeyPipe,
    TranslateDirective,
    AlertComponent,
    AlertErrorComponent,
    HasAnyAuthorityDirective,
    DurationPipe,
    FormatMediumDatetimePipe,
    FormatMediumDatePipe,
    SortByDirective,
    SortDirective,
    ItemCountComponent,
    SecondsToTimePipe,
    MinutesToDaysPipe,
    FormatTimePipe,
    ConvertHoursMinutesPipe,
    ToastComponent,
    InputTrimDirective,
    SidebarComponent,
  ],
})
export class SharedModule {}
