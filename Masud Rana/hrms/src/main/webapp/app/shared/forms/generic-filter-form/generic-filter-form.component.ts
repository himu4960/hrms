import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import { combineLatest, Observable, of, Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

import { AppState } from 'app/store';
import { UtilService } from 'app/shared/service/util.service';

import { IEmployee } from 'app/employee/models/employee.model';
import { IBranch } from 'app/entities/branch/branch.model';
import { IDesignation } from 'app/entities/designation/designation.model';
import { IFilterDateRange, IReportFilterParam } from './models/generic-filter.model';

import { dateValidator } from 'app/shared/validators/start-end-date.validator';
import { PermissionService } from 'app/shared/service/permission.service';

import { GenericFilterService } from './generic-filter.service';

import { ETimeType } from 'app/shared/pipes/time/time-type.enum';
import { EFilterDurationType } from './generic-filter.enum';
import { selectShiftPlanningSettings } from 'app/store/company-settings/company-settings.selectors';
import { ShiftPlanningSettings } from 'app/store/states/shift-planning-settings.interface';
import { selectCurrentCompany, selectUser } from 'app/store/user/user.selectors';
import { UserCompany } from 'app/store/states/user.interface';

@Component({
  selector: 'hrms-generic-filter-form',
  templateUrl: './generic-filter-form.component.html',
  styleUrls: ['./generic-filter-form.component.scss'],
})
export class GenericFilterFormComponent implements OnInit, OnDestroy {
  @Input() isLoading = false;
  @Output() filterApply = new EventEmitter<any>();
  @Output() filterReset = new EventEmitter<any>();

  get eFilterDurationType(): any {
    return EFilterDurationType;
  }
  get eTimeType(): any {
    return ETimeType;
  }

  branches: IBranch[] = [];
  employees: IEmployee[] = [];
  designations: IDesignation[] = [];

  duration_type = EFilterDurationType.LAST_WEEK;
  shiftPlanningSettings!: ShiftPlanningSettings;
  employeeId!: number | undefined;
  isEmployee = true;

  filterForm: FormGroup = this.fb.group(
    {
      start_date: [],
      end_date: [],
      branch_id: [],
      employee_id: [],
      designation_id: [],
      duration_type: [this.duration_type, Validators.required],
    },
    { validators: dateValidator }
  );

  employees$!: Observable<any>;

  public filterDateControl = this.filterForm.get(['duration_type']);
  private subscriptions: Subscription = new Subscription();

  constructor(
    private readonly genericFilterService: GenericFilterService,
    private fb: FormBuilder,
    private utilService: UtilService,
    private store: Store<AppState>,
    private permissionService: PermissionService
  ) {}

  ngOnInit(): void {
    this.subscriptions.add(
      this.store.select(selectShiftPlanningSettings).subscribe((res: ShiftPlanningSettings) => {
        if (res !== null) {
          this.shiftPlanningSettings = res;
        }
      })
    );

    this.subscriptions.add(
      this.store.select(selectCurrentCompany).subscribe((currentCompany: UserCompany) => {
        if (currentCompany !== null) {
          this.employeeId = currentCompany?.employee?.id;
        }
      })
    );

    this.filterApplyClick();

    this.loadPage().subscribe(() => {
      this.isLoading = false;
    });
  }

  loadPage(): Observable<any> {
    this.isLoading = true;
    return this.utilService.getBranches().pipe(
      switchMap((res: any) => {
        this.branches = res;
        this.isEmployee = this.permissionService.isEmployee();

        if (!this.isEmployee) {
          this.employees$ = this.utilService.getEmployees();
        } else {
          this.employees$ = of(null);
        }
        const designation$ = this.utilService.getDesignations();

        return combineLatest([this.employees$, designation$]).pipe(
          tap(([employees, designations]) => {
            this.employees = employees;
            this.designations = designations;
          })
        );
      })
    );
  }

  filterApplyClick(): void {
    this.duration_type = Number(this.filterForm.value.duration_type);
    const start_date: Date = this.filterForm.get(['start_date'])!.value;
    const end_date: Date = this.filterForm.get(['end_date'])!.value;

    const filterRange: IFilterDateRange = this.genericFilterService.getFilterRange(this.duration_type, start_date, end_date);
    let filterParam: IReportFilterParam = {
      start_date: filterRange.start_date,
      end_date: filterRange.end_date,
    };

    if (this.isEmployee) {
      filterParam = {
        ...filterParam,
        employee_id: this.employeeId,
      };
    } else {
      filterParam = {
        ...filterParam,
        branch_id: this.filterForm.get(['branch_id'])!.value,
        employee_id: this.filterForm.get(['employee_id'])!.value,
        designation_id: this.filterForm.get(['designation_id'])!.value,
      };
    }
    this.filterApply.emit(filterParam);
  }

  resetFilters(): void {
    this.filterForm.reset();
    this.filterForm.get(['duration_type'])?.patchValue(EFilterDurationType.LAST_WEEK);
    this.filterReset.emit();
  }

  changeFilter(value: any): void {
    this.duration_type = Number(value);
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
