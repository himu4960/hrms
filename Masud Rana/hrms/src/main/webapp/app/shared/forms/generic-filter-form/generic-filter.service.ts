import { Injectable } from '@angular/core';
import { UtilService } from 'app/shared/service/util.service';

import { EFilterDurationType } from './generic-filter.enum';

@Injectable({
  providedIn: 'root',
})
export class GenericFilterService {
  constructor(private readonly utilService: UtilService) {}

  getFilterRange(filterType: number, startDate: Date, endDate: Date): any {
    const variableToday = new Date();

    const week_start_date = new Date(variableToday.setDate(variableToday.getDate() - variableToday.getDay()));

    if (filterType === EFilterDurationType.THIS_WEEK) {
      const this_week_start_date = new Date(variableToday.setDate(variableToday.getDate() - variableToday.getDay()));
      const this_week_end_date = new Date(week_start_date.setDate(week_start_date.getDate() + 6));
      startDate = new Date(this.utilService.format(this_week_start_date));
      endDate = new Date(this.utilService.format(this_week_end_date));
    } else if (filterType === EFilterDurationType.LAST_WEEK) {
      const last_week_start_date = new Date(week_start_date.setDate(week_start_date.getDate() - 13));
      const last_week_end_date = new Date(week_start_date.setDate(week_start_date.getDate() + 6));
      startDate = new Date(this.utilService.format(last_week_start_date));
      endDate = new Date(this.utilService.format(last_week_end_date));
    } else if (filterType === EFilterDurationType.TODAY) {
      const today = new Date();
      startDate = new Date(this.utilService.format(today));
      endDate = new Date(this.utilService.format(new Date(new Date().setTime(new Date().getTime() + 1))));
    } else if (filterType === EFilterDurationType.LAST_7_DAYS) {
      const last_7_days = new Date(new Date().setDate(new Date().getDate() - 6));
      startDate = new Date(this.utilService.format(last_7_days));
      endDate = new Date(this.utilService.format(new Date()));
    } else if (filterType === EFilterDurationType.YESTERDAY) {
      const variableYesterday = new Date(new Date().setDate(new Date().getDate() - 1));
      const yesterday = new Date(new Date().setDate(new Date().getDate() - 1));
      startDate = new Date(this.utilService.format(yesterday));
      endDate = new Date(this.utilService.format(new Date(variableYesterday.setTime(variableYesterday.getTime() + 1))));
    } else {
      startDate = new Date(this.utilService.format(startDate));
      endDate = new Date(this.utilService.format(endDate));
    }
    return { start_date: startDate, end_date: endDate };
  }
}
