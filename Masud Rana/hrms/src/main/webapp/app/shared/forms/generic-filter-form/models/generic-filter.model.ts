export interface IReportFilterParam {
  start_date: Date;
  end_date: Date;
  branch_id?: number | null;
  employee_id?: number | null;
  designation_id?: number | null;
}

export interface IFilterDateRange {
  start_date: Date;
  end_date: Date;
}

export enum EFilterDurationType {
  TODAY = 0,
  YESTERDAY = 1,
  LAST_7_DAYS = 7,
  THIS_WEEK = 6,
  LAST_WEEK = 14,
  ALL_TIME = -1,
}
