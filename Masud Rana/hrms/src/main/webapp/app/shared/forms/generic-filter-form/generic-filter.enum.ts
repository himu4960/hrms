export enum EFilterDurationType {
  TODAY = 0,
  YESTERDAY = 1,
  LAST_7_DAYS = 7,
  THIS_WEEK = 6,
  LAST_WEEK = 14,
  ALL_TIME = -1,
}
