import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { GenericFilterFormComponent } from './generic-filter-form.component';

@NgModule({
  declarations: [GenericFilterFormComponent],
  imports: [SharedModule],
  exports: [GenericFilterFormComponent],
})
export class GenericFilterFormModule {}
