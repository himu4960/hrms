import { IMenu } from 'app/entities/menu/menu.model';

export const sideMenus: IMenu[] = [
  {
    name: 'Time Clock Overview',
    router_link: '/time-clock/overview',
  },
  {
    name: 'Add Clock Time',
    router_link: '/time-clock/add',
    settings_model_name: 'timeClock',
    settings_field_name: 'timesheet_employee_can_manually_add',
    is_for_employee: true,
  },
  {
    name: 'Upload Time Sheet',
    router_link: '/time-clock/upload',
    settings_model_name: 'timeClock',
    settings_field_name: 'timesheet_employee_can_import',
    is_for_employee: true,
  },
  {
    name: 'Manage Time Sheets',
    router_link: '/time-clock/manage',
  },
  {
    name: 'Time Sheets',
    router_link: '/reports/time-sheets',
  },
  {
    name: 'Time Sheets Summary',
    router_link: '/time-sheets-summary',
  },
  {
    name: 'Time Sheet Attendance',
    router_link: '/time-sheets-attendance',
  },

  {
    name: 'Leave Overview',
    router_link: '/leave/overview',
  },
  {
    name: 'Leave Management',
    router_link: '/leave/management',
  },
  {
    name: 'Schedule',
    router_link: '/reports/schedule',
  },
  {
    name: 'Schedule Summary',
    router_link: '/reports/schedule-summary',
  },
  {
    name: 'Shift Schedule',
    router_link: '/reports/shifts-scheduled',
  },
  {
    name: 'position Summary',
    router_link: '/reports/position-summary',
  },
  {
    name: 'Hours Scheduled',
    router_link: '/reports/hours-scheduled',
  },
  {
    name: 'Time Sheets',
    router_link: '/reports/timesheets',
  },
  {
    name: 'Time Sheets',
    router_link: '/reports/timesheet',
  },
  {
    name: 'Time Sheets Summary',
    router_link: '/reports/timesheets-summary',
  },
  {
    name: 'Time Sheets Attendance',
    router_link: '/reports/timesheets-attendance',
  },
  {
    name: 'Time Sheets Late Summary',
    router_link: '/reports/timesheets-late-summary',
  },
  {
    name: 'Location History',
    router_link: '/reports/location-history',
  },
];

export const locationSideMenus: IMenu[] = [
  {
    name: 'All Locations',
    router_link: '/branch',
    translate_name: 'hrmsApp.branch.sidebar.allBranches.label',
    is_for_employee: true,
  },
  {
    name: 'Break Rule',
    router_link: '/break-rule',
    translate_name: 'hrmsApp.branch.sidebar.breakRule.label',
    is_for_employee: false,
  },
  {
    name: 'File Upload',
    router_link: '/file-upload',
    translate_name: 'hrmsApp.branch.sidebar.fileUpload.label',
    is_for_employee: false,
  },
];
