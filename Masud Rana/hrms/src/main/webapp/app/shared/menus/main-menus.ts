import { IMenu } from 'app/entities/menu/menu.model';

export const mainMenus: IMenu[] = [
  {
    name: 'Home',
    router_link: '/home',
  },
  {
    name: 'Employees',
    router_link: '/employee',
    settings_model_name: 'application',
    settings_field_name: 'employee_can_view_staff_gallery',
    is_for_employee: true,
  },
  {
    name: 'Shift Planning',
    router_link: '/shift-planning',
    settings_model_name: 'shiftPlanning',
    settings_field_name: 'work_unit_module_enabled',
  },
  {
    name: 'Leave',
    router_link: '/leave',
    settings_model_name: 'leaveAvailability',
    settings_field_name: 'leave_module_enabled',
  },
  {
    name: 'Time Clock',
    router_link: '/time-clock',
    settings_model_name: 'timeClock',
    settings_field_name: 'time_clock_enabled',
  },
  {
    name: 'Report',
    router_link: '/reports',
    settings_model_name: 'application',
    settings_field_name: 'employee_can_view_report',
    is_for_employee: true,
  },
  {
    name: 'Role',
    router_link: '/role',
  },
  {
    name: 'Menu',
    router_link: '/menu',
  },
];
