import { IWorkShift } from 'app/shift-planning/models/work-shift.model';
import { IFile } from 'app/file-upload/file.model';
import { IAnnouncement } from 'app/announcement/announcement.model';
import { IEmployee } from 'app/employee/models/employee.model';
import { IBranch } from 'app/entities/branch/branch.model';
import { IDesignation } from 'app/entities/designation/designation.model';
import { ISkill } from 'app/entities/skill/skill.model';
import { IAnniversary } from 'app/home/components/anniversary/anniversary.model';
import { IBirthday } from 'app/home/components/birthday/birthday.model';
import { ILeave } from 'app/leave/model/leave.model';
import { ITimeSheetLateSummary } from 'app/reports/time-sheet/models/time-sheet-report.model';

export interface IHomeData {
  all_shift_count: number;
  leave_request_count: number;
  unpublished_shift_count: number;
  time_sheet_count: number;
  anniversaries: IAnniversary[];
  announcements: IAnnouncement[];
  birthdays: IBirthday[];
  files: IFile[];
  lates: ITimeSheetLateSummary[];
  on_leaves: ILeave[];
  upcoming_shifts: IWorkShift[];
}

export interface IEmployeeData {
  employees: IEmployee[];
  branches: IBranch[];
  skills: ISkill[];
  designations?: IDesignation[];
  total_employee_count: number;
  not_activated: number;
  disabled: number;
}
