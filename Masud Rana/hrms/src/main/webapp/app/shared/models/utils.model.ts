import { CompanyType } from './../../entities/company-type/company-type.model';
import { ApplicationType } from './../../entities/application-type/application-type.model';

export interface ILanguage {
  code: string;
  name: string;
  is_default: boolean;
}

export interface IAppSetupData {
  application_types: ApplicationType[];
  company_types: CompanyType[];
}
