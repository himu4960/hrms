export enum EPermission {
  CAN_ADD = 'can_add',
  CAN_EDIT = 'can_edit',
  CAN_VIEW = 'can_view',
  CAN_DELETE = 'can_delete',
  CAN_APPROVE = 'can_approve',
  CAN_REJECT = 'can_reject',
  CAN_PUBLISH = 'can_publish',
  CAN_CHANGE_PASSWORD = 'can_change_password',
}
