export enum RoleType {
  SUPER_ADMIN = 'SUPER_ADMIN',
  OWNER = 'OWNER',
  MANAGER = 'MANAGER',
  EMPLOYEE = 'EMPLOYEE',
}

export const permissionRoleTypeCode = {
  SUPER_ADMIN: 400,
  OWNER: 300,
  MANAGER: 200,
  EMPLOYEE: 100,
};
