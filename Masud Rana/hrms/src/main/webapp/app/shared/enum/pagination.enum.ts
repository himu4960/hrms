export enum EPaginationPageSize {
  PAGE_SIZE_05 = 5,
  PAGE_SIZE_10 = 10,
  PAGE_SIZE_20 = 20,
  PAGE_SIZE_30 = 30,
  PAGE_SIZE_40 = 40,
  PAGE_SIZE_50 = 50,
  PAGE_SIZE_100 = 100,
}
