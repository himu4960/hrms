import { Component } from '@angular/core';

@Component({
  selector: 'hrms-full-screen-spinner',
  templateUrl: './full-screen-spinner.component.html',
  styleUrls: ['./full-screen-spinner.component.scss'],
})
export class FullScreenSpinnerComponent {}
