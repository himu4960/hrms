import { Component } from '@angular/core';

@Component({
  selector: 'hrms-transparent-spinner',
  templateUrl: './transparent-spinner.component.html',
  styleUrls: ['./transparent-spinner.component.scss'],
})
export class TransparentSpinnerComponent {}
