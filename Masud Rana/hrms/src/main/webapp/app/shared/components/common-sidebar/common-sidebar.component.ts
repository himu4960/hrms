import { Component, Input } from '@angular/core';

@Component({
  selector: 'hrms-common-sidebar',
  templateUrl: './common-sidebar.component.html',
})
export class CommonSidebarComponent {
  @Input() sideMenus?: any[];
}
