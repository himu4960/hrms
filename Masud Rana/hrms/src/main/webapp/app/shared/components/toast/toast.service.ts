import { Injectable, TemplateRef } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ToastService {
  toasts: any[] = [];

  showSuccessToast(textOrTpl: string | TemplateRef<any>): void {
    this.toasts.push({ textOrTpl, classname: 'bg-success text-light rounded-8 fs-md', delay: 5000, icon: 'hazira-icon-ok' });
  }

  showDangerToast(textOrTpl: string | TemplateRef<any>): void {
    this.toasts.push({ textOrTpl, classname: 'bg-danger text-light rounded-8 fs-md', delay: 5000, icon: 'hazira-icon-cross' });
  }

  showWarningToast(textOrTpl: string | TemplateRef<any>): void {
    this.toasts.push({ textOrTpl, classname: 'bg-warning text-light rounded-8 fs-md', delay: 5000, icon: 'icon-warning1' });
  }

  remove(toast: any): void {
    this.toasts = this.toasts.filter(t => t !== toast);
  }

  clear(): void {
    this.toasts.splice(0, this.toasts.length);
  }
}
