import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from './../../shared.module';
import { CommonSidebarComponent } from './common-sidebar.component';

@NgModule({
  imports: [CommonModule, RouterModule, SharedModule],
  declarations: [CommonSidebarComponent],
  exports: [CommonSidebarComponent],
})
export class CommonSidebarModule {}
