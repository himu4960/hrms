import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { Store } from '@ngrx/store';
import { Socket } from 'ngx-socket-io';

import { AppState } from 'app/store';

import { selectState } from './../../../store/user/user.selectors';

import { AuthServerProvider } from 'app/core/auth/auth-jwt.service';

import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EmployeeOnlineService {
  constructor(private socket: Socket, private authServerProvider: AuthServerProvider, private store: Store<AppState>) {}

  connect(): Observable<any> {
    return this.store.select(selectState).pipe(
      switchMap(state => {
        const token = state?.jwtToken ?? this.authServerProvider.getAuthenticationToken();
        const socketUrl = environment.URL;

        this.socket = new Socket({
          url: socketUrl,
          options: {
            extraHeaders: {
              Authorization: token,
            },
          },
        });

        return of(this.socket.connect());
      }),
      switchMap(() => this.socket.fromEvent<any>('connection'))
    );
  }

  setLoadedOnline(): void {
    this.socket.emit('fetchOnline');
  }

  setClockIn(timeClockId: number): void {
    this.socket.emit('clockIn', timeClockId);
  }

  setClockOut(timeClockId: number): void {
    this.socket.emit('clockOut', timeClockId);
  }

  getAllOnlineEmployees(): Observable<any> {
    return this.socket.fromEvent<any>('getAllOnline');
  }

  getOnClockIn(): Observable<any> {
    return this.socket.fromEvent<any>('onClockIn');
  }

  getOnClockOut(): Observable<any> {
    return this.socket.fromEvent<any>('onClockOut');
  }

  disconnect(): void {
    this.socket.disconnect();
  }
}
