import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'hrms-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss'],
})
export class ConfirmDialogComponent {
  message!: string;
  data!: any;

  constructor(public activeModal: NgbActiveModal) {}

  confirm(): void {
    this.activeModal.close();
  }
}
