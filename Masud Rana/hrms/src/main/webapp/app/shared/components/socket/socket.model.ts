export interface ISocketConnected {
  client_id: string;
  message: string;
}
