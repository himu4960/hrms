import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { AppState } from 'app/store';

import { IMenu } from 'app/entities/menu/menu.model';

import { selectCurrentRoute } from 'app/store/route/router.selector';
import { selectSideMenu } from 'app/store/side-menu/side.menu.selectors';

import { browserReloadSideMenuAction } from 'app/store/side-menu/side.menu.actions';

@Component({
  selector: 'hrms-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit, OnDestroy {
  sideMenus: IMenu[] = [];
  sideMenuId?: number;

  private subscriptions: Subscription = new Subscription();

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.subscriptions.add(
      this.store.select(selectSideMenu).subscribe((res: any) => {
        this.sideMenus = res || [];
      })
    );
    this.subscriptions.add(
      this.store.select(selectCurrentRoute).subscribe((route: any) => {
        if (route) {
          this.store.dispatch(browserReloadSideMenuAction());
        }
      })
    );
  }

  toggleCollapse(menu: any): void {
    if (menu.id === this.sideMenuId) {
      menu.is_collapsed = !menu.is_collapsed;
    }

    if (menu.id !== this.sideMenuId) {
      menu.is_collapsed = false;
      this.sideMenuId = menu.id;
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
