import { Component, HostBinding, TemplateRef } from '@angular/core';
import { ToastService } from './toast.service';

@Component({
  selector: 'hrms-app-toasts',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
})
export class ToastComponent {
  @HostBinding('class') baseClasses = 'toast-container position-fixed bottom-0 p-3';
  @HostBinding('style') baseStyles = 'z-index: 999999; bottom: 0; left: 50%; transform: translateX(-50%);';

  constructor(public toastService: ToastService) {}

  isTemplate(toast: { textOrTpl: any }): any {
    return toast.textOrTpl instanceof TemplateRef;
  }
}
