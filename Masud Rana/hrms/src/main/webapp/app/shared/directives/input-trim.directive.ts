import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[hrmsInputTrim]',
})
export class InputTrimDirective {
  constructor(private el: ElementRef) { }

  @HostListener('blur') onBlur(): void {
    const value = this.el.nativeElement.value;
    const valueTrim = value.trim();
    if (value !== valueTrim) {
      this.el.nativeElement.value = valueTrim;
    }
  }
}
