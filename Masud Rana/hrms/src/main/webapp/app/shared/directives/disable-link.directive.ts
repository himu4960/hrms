import { Directive, Input, Optional } from '@angular/core';
import { RouterLink, RouterLinkWithHref } from '@angular/router';

@Directive({
  selector: '[routerLink][hrmsDisableLink]',
})
export class DisableLinkDirective {
  @Input() hrmsDisableLink = false;

  constructor(@Optional() routerLink: RouterLink, @Optional() routerLinkWithHref: RouterLinkWithHref) {
    const link = routerLink || routerLinkWithHref;
    const onClick = link.onClick;

    link.onClick = (...args) => {
      if (this.hrmsDisableLink) {
        return routerLinkWithHref ? false : true;
      } else {
        return onClick.apply(link, args);
      }
    };
  }
}
