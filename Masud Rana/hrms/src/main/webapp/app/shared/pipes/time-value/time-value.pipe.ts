import { Pipe, PipeTransform } from '@angular/core';
import { timeValues } from './time-value.mapper';

@Pipe({
  name: 'timeValue',
})
export class SecondsToTimePipe implements PipeTransform {
  transform(value: any): string {
    const time = timeValues.find(x => x.value === value)?.name;
    return time ? time : '';
  }
}
