import { Pipe, PipeTransform } from '@angular/core';

import * as Moment from 'moment';
import { extendMoment } from 'moment-range';

@Pipe({
  name: 'convertHoursMinutes',
})
export class ConvertHoursMinutesPipe implements PipeTransform {
  public moment = extendMoment(Moment);
  transform(value: number | undefined | null): string {
    if (value && value !== undefined) {
      return this.convertSecondIntoHoursAndMinutes(value);
    }
    return '';
  }

  convertSecondIntoHoursAndMinutes(seconds: number): string {
    return this.moment(seconds * 1000)
      .toISOString()
      .substring(11, 16);
  }
}
