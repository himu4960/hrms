import { Pipe, PipeTransform } from '@angular/core';

import { DEFAULT_WORK_HOUR_IN_MINS } from 'app/shared/constant/time.contstant';

@Pipe({
  name: 'minutesToDays',
})
export class MinutesToDaysPipe implements PipeTransform {
  transform(value: any): string {
    const isPositive = value >= 0;
    const absValue = Math.abs(value);
    const days = Math.floor(absValue / DEFAULT_WORK_HOUR_IN_MINS);
    const hours = Math.floor((absValue % DEFAULT_WORK_HOUR_IN_MINS) / 60);
    if (days && hours) {
      return isPositive ? `${days} days, ${hours} hours` : `-${days} days, ${hours} hours(Extra)`;
    } else if (days) {
      return isPositive ? `${days} days` : `-${days} days(Extra)`;
    } else {
      return isPositive ? `${hours} hours` : `-${hours} hours(Extra)`;
    }
  }
}
