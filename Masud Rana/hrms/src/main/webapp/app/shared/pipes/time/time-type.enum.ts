export enum ETimeType {
  MINUTE = 'minute',
  SECOND = 'second',
}
