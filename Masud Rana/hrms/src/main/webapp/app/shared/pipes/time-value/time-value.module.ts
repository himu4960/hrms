import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SecondsToTimePipe } from './time-value.pipe';
import { MinutesToDaysPipe } from './minutes-to-days.pipe';

@NgModule({
  declarations: [SecondsToTimePipe, MinutesToDaysPipe],
  imports: [CommonModule],
})
export class TimeValueModule {}
