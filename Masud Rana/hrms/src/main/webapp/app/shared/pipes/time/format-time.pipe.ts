import { Pipe, PipeTransform } from '@angular/core';

import { ETimeType } from './time-type.enum';

@Pipe({
  name: 'formatTime',
})
export class FormatTimePipe implements PipeTransform {
  transform(value: number | undefined | null, type: string): string {
    if (value && value !== undefined) {
      if (type === ETimeType.MINUTE) {
        return this.getTimeFromMinute(value);
      }
      if (type === ETimeType.SECOND) {
        return this.getTimeFromSecond(value);
      }
    }
    return '';
  }

  getTimeFromMinute(minute: number): string {
    const minutes = minute % 60;
    const hours = (minute - minutes) / 60;
    if (minutes) {
      return `${hours}h ${minutes}m`;
    }
    return `${hours}h`;
  }

  getTimeFromSecond(second: number): string {
    const hours = Math.floor(second / 3600);
    const minutes = Math.floor((second - hours * 3600) / 60);

    if (!hours && !minutes) {
      return `${Math.floor(second)} Sec`;
    }

    if (!hours) {
      return `${minutes}m`;
    }

    return `${hours}h ${minutes}m`;
  }
}
