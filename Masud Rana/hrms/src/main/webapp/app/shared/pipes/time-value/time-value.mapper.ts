export interface ITimeSecond {
  name: string;
  value: number;
}

export class TimeSecond implements ITimeSecond {
  constructor(public name: string, public value: number) {
    this.name = name;
    this.value = value;
  }
}

export const timeValues: TimeSecond[] = [
  { name: '12:00 AM', value: 0 },
  { name: '12:15 AM', value: 900 },
  { name: '12:30 AM', value: 1800 },
  { name: '12:45 AM', value: 2700 },

  { name: '1:00 AM', value: 3600 },
  { name: '1:15 AM', value: 4500 },
  { name: '1:30 AM', value: 5400 },
  { name: '1:45 AM', value: 6300 },

  { name: '2:00 AM', value: 7200 },
  { name: '2:15 AM', value: 8100 },
  { name: '2:30 AM', value: 9000 },
  { name: '2:45 AM', value: 9900 },

  { name: '3:00 AM', value: 10800 },
  { name: '3:15 AM', value: 11700 },
  { name: '3:30 AM', value: 12600 },
  { name: '3:45 AM', value: 13500 },

  { name: '4:00 AM', value: 14400 },
  { name: '4:15 AM', value: 15300 },
  { name: '4:30 AM', value: 16200 },
  { name: '4:45 AM', value: 17100 },

  { name: '5:00 AM', value: 18000 },
  { name: '5:15 AM', value: 18900 },
  { name: '5:30 AM', value: 19800 },
  { name: '5:45 AM', value: 20700 },

  { name: '6:00 AM', value: 21600 },
  { name: '6:15 AM', value: 22500 },
  { name: '6:30 AM', value: 23400 },
  { name: '6:45 AM', value: 24300 },

  { name: '7:00 AM', value: 25200 },
  { name: '7:15 AM', value: 26100 },
  { name: '7:30 AM', value: 27000 },
  { name: '7:45 AM', value: 27900 },

  { name: '8:00 AM', value: 28800 },
  { name: '8:15 AM', value: 29700 },
  { name: '8:30 AM', value: 30600 },
  { name: '8:45 AM', value: 31500 },

  { name: '9:00 AM', value: 32400 },
  { name: '9:15 AM', value: 33300 },
  { name: '9:30 AM', value: 34200 },
  { name: '9:45 AM', value: 35100 },

  { name: '10:00 AM', value: 36000 },
  { name: '10:15 AM', value: 36900 },
  { name: '10:30 AM', value: 37800 },
  { name: '10:45 AM', value: 38700 },

  { name: '11:00 AM', value: 39600 },
  { name: '11:15 AM', value: 40500 },
  { name: '11:30 AM', value: 41400 },
  { name: '11:45 AM', value: 42300 },

  { name: '12:00 PM', value: 43200 },
  { name: '12:15 PM', value: 44100 },
  { name: '12:30 PM', value: 44500 },
  { name: '12:45 PM', value: 45900 },

  { name: '1:00 PM', value: 46800 },
  { name: '1:15 PM', value: 47700 },
  { name: '1:30 PM', value: 48600 },
  { name: '1:45 PM', value: 49500 },

  { name: '2:00 PM', value: 50400 },
  { name: '2:15 PM', value: 51300 },
  { name: '2:30 PM', value: 52200 },
  { name: '2:45 PM', value: 53100 },

  { name: '3:00 PM', value: 54000 },
  { name: '3:15 PM', value: 54900 },
  { name: '3:30 PM', value: 55800 },
  { name: '3:45 PM', value: 56700 },

  { name: '4:00 PM', value: 57600 },
  { name: '4:15 PM', value: 58500 },
  { name: '4:30 PM', value: 59400 },
  { name: '4:45 PM', value: 60300 },

  { name: '5:00 PM', value: 61200 },
  { name: '5:15 PM', value: 62100 },
  { name: '5:30 PM', value: 63000 },
  { name: '5:45 PM', value: 63900 },

  { name: '6:00 PM', value: 64800 },
  { name: '6:15 PM', value: 65700 },
  { name: '6:30 PM', value: 66600 },
  { name: '6:45 PM', value: 67500 },

  { name: '7:00 PM', value: 68400 },
  { name: '7:15 PM', value: 69300 },
  { name: '7:30 PM', value: 70200 },
  { name: '7:45 PM', value: 71100 },

  { name: '8:00 PM', value: 72000 },
  { name: '8:15 PM', value: 72900 },
  { name: '8:30 PM', value: 73800 },
  { name: '8:45 PM', value: 74700 },

  { name: '9:00 PM', value: 75600 },
  { name: '9:15 PM', value: 76500 },
  { name: '9:30 PM', value: 77400 },
  { name: '9:45 PM', value: 78300 },

  { name: '10:00 PM', value: 79200 },
  { name: '10:15 PM', value: 80100 },
  { name: '10:30 PM', value: 81000 },
  { name: '10:45 PM', value: 81900 },

  { name: '11:00 PM', value: 82800 },
  { name: '11:15 PM', value: 83700 },
  { name: '11:30 PM', value: 84600 },
  { name: '11:45 PM', value: 85500 },
];
