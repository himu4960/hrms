import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

import { ITimeClockEventForm } from './../../time-clock/models/time-clock-event.model';

export function timeClockEventValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const timeClockEvent: ITimeClockEventForm = control.value;
    const { break_start_date, break_start_time, break_end_date, break_end_time } = timeClockEvent;

    const break_start_time_seconds = new Date(`${break_start_time} ${break_start_date}`).getTime();
    const break_end_time_seconds = new Date(`${break_end_time} ${break_end_date}`).getTime();

    if (break_start_time_seconds > break_end_time_seconds) {
      const err = {
        break_end_time: true,
      };
      control.get('break_end_time')?.setErrors(err);
      return err;
    }

    control.get('break_end_time')?.setErrors(null);
    return null;
  };
}
