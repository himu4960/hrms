import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function breakRuleConditionValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const min_shift_length = convertTimeToMinute(control.value.min_shift_length);
    const max_shift_length = convertTimeToMinute(control.value.max_shift_length);
    const break_duration = control.value.break_duration;
    const break_starts_at = convertTimeToMinute(control.value.break_starts_at);
    const from = convertTimeToMinute(control.value.from);
    const to = convertTimeToMinute(control.value.to);

    if (min_shift_length >= max_shift_length) {
      const err = {
        min_shift_must_less_than_max_shift: true,
      };
      control.get('min_shift_length')?.setErrors(err);
      return err;
    }

    if (min_shift_length <= break_duration) {
      const err = {
        break_duration_must_less_than_min_shift: true,
      };
      control.get('break_duration')?.setErrors(err);
      return err;
    }

    if (min_shift_length <= from) {
      const err = {
        from_must_less_than_min_shift: true,
      };
      control.get('from')?.setErrors(err);
      return err;
    }

    if (max_shift_length < to) {
      const err = {
        to_must_less_than_max_shift: true,
      };
      control.get('to')?.setErrors(err);
      return err;
    }

    if (from > to) {
      const err = {
        to_must_greater_than_from: true,
      };
      control.get('to')?.setErrors(err);
      return err;
    }

    if (from > break_starts_at) {
      const err = {
        break_starts_at_must_greater_than_from: true,
      };
      control.get('break_starts_at')?.setErrors(err);
      return err;
    }

    if (min_shift_length <= break_starts_at) {
      const err = {
        break_starts_at_must_less_than_min_shift: true,
      };
      control.get('break_starts_at')?.setErrors(err);
      return err;
    }

    if (from > break_starts_at) {
      const err = {
        break_starts_at_must_greater_than_from: true,
      };
      control.get('break_starts_at')?.setErrors(err);
      return err;
    }

    control.get('min_shift_length')?.setErrors(null);
    control.get('max_shift_length')?.setErrors(null);
    control.get('break_duration')?.setErrors(null);
    control.get('break_starts_at')?.setErrors(null);
    control.get('from')?.setErrors(null);
    control.get('to')?.setErrors(null);
    return null;
  };
}

export function convertTimeToMinute(value: any): any {
  if (!value) {
    return null;
  }
  const hours = Number(value.split(':')[0]);
  const minutes = Number(value.split(':')[1]);
  return hours * 60 + minutes;
}
