import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function timeFormatValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }

    const hasHHMMSSformat = /^([0-1]\d|2[0-3]):(([0-5]\d|2[0-9]))(?::([0-5]?\d))?$/.test(control.value);

    return !hasHHMMSSformat ? { timeFormatHHMMSS: true } : null;
  };
}
