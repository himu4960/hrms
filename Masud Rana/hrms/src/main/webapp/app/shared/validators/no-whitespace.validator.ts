import { FormControl } from '@angular/forms';

export function noWhitespaceValidator(control: FormControl): any {
  if (control.value) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }
  return null;
}
