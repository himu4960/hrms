import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export const dateValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const startDate: AbstractControl | null = control.get('start_date');
  const endDate: AbstractControl | null = control.get('end_date');
  const startTime: AbstractControl | null = control.get('start_time');
  const endTime: AbstractControl | null = control.get('end_time');
  const is_partial_day_allowed: AbstractControl | null = control.get('is_partial_day_allowed');

  if (is_partial_day_allowed?.value === 1) {
    return startTime?.value !== null && endTime?.value !== null && startTime?.value <= endTime?.value ? null : { timeValid: true };
  } else if (is_partial_day_allowed?.value === 0) {
    return startDate?.value !== null && endDate?.value !== null && startDate?.value <= endDate?.value ? null : { dateValid: true };
  } else {
    return startDate?.value !== null && endDate?.value !== null && startDate?.value <= endDate?.value ? null : { dateValid: true };
  }
};
