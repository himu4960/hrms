import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

import { ITimeClockForm } from './../../time-clock/models/time-clock.model';

export function timeClockValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const timeClock: ITimeClockForm = control.value;
    const { clock_end_date, clock_end_time, clock_start_date, clock_start_time } = timeClock;
    const clock_start_time_seconds = new Date(`${clock_start_time} ${clock_start_date}`).getTime();
    const clock_end_time_seconds = new Date(`${clock_end_time} ${clock_end_date}`).getTime();

    if (clock_start_time_seconds > clock_end_time_seconds) {
      const err = {
        clock_end_time: true,
        clock_end_date: true,
      };
      control.get('clock_end_time')?.setErrors(err);
      control.get('clock_end_date')?.setErrors(err);
      return err;
    }

    control.get('clock_end_time')?.setErrors(null);
    control.get('clock_end_date')?.setErrors(null);
    return null;
  };
}
