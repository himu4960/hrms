import { IEmployee } from 'app/employee/models/employee.model';
import { IDesignation } from 'app/entities/designation/designation.model';
import { ILeave } from './../../leave/model/leave.model';
import { IDropdownEmployee } from './../../employee/models/employee.model';

export interface IShiftPlanningFilterParams {
  branch_id?: number;
  designation_ids?: number[];
  start_date?: string;
  end_date?: string;
  employee_ids?: number[];
  skill_ids?: number[] | undefined;
}

export interface IShiftPlanning {
  id: number;
  code?: string | null;
  first_name?: string | null;
  middle_name?: string | null;
  last_name?: string | null;
  dob?: string | null;
  email?: string | null;
  personal_email?: string | null;
  phone?: string | null;
  telephone?: string | null;
  nationality?: string | null;
  photo_url?: string | null;
  gender_id?: number | null;
  religion_id?: number | null;
  marital_status_id?: number | null;
  blood_group_id?: number | null;
  branch_id?: number | null;
  is_active?: boolean | null;
  is_deleted?: boolean | null;
  invitation_accepted?: boolean | null;
  join_date?: string | null;
  invitation_token?: string | null;
  invitation_expiry?: string | null;
  leaves: ILeave[];
  employees: IEmployee[];
}

export interface ICreateManyWorkShift {
  title?: string;
  start_date?: number;
  end_date?: number;
  start_time?: number;
  end_time?: number;
  employees?: IDropdownEmployee[];
  total_work_shift_duration?: number;
}

export interface IWorkShift {
  id?: number;
  branch_id?: number;
  title?: string | null;
  designation_id?: number;
  is_published?: boolean | null;
  published_at?: number | null;
  published_by?: number | null;
  start_date?: number | null;
  end_date?: number | null;
  start_time?: number;
  end_time?: number;
  total_work_shift_duration?: number;
  deleted_at?: number | null;
  deleted_by?: number | null;
  employee_id?: number;
  skills?: string;
  note?: string;
  designation?: IDesignation;
}

export interface IWorkShiftEmployee {
  id: number;
  company_id: number;
  branch_id: number;
  workshift_id: number;
  employee_id: number;
  designation_id: number;
  work_shift: IWorkShift;
}

export class WorkShift implements IWorkShift {
  constructor(
    public id?: number,
    public branch_id?: number,
    public title?: string | null,
    public designation_id?: number,
    public is_published?: boolean | null,
    public published_at?: number | null,
    public published_by?: number | null,
    public start_date?: number,
    public end_date?: number,
    public start_time?: number,
    public end_time?: number,
    public total_work_shift_duration?: number,
    public deleted_at?: number | null,
    public deleted_by?: number | null
  ) {
    this.is_published = this.is_published ?? false;
  }
}

export function getWorkShiftIdentifier(workShift: IWorkShift): number | undefined {
  return workShift.id;
}
