export interface ICalendarResource {
  id: number | undefined;
  title: string;
  eventColor?: string;
}

export interface ICalendarEvent {
  resourceId: number;
  title?: string;
  start: string;
  end: string;
}
