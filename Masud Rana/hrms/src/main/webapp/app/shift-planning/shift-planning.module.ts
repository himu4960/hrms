import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';

import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import interactionPlugin from '@fullcalendar/interaction'; // a plugin

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';

import { ConfirmDialogModule } from 'app/shared/components/confirm-dialog/confirm-dialog.module';
import { SharedModule } from 'app/shared/shared.module';

import { ShiftPlanningComponent } from './shift-planning.component';
import { CreateWorkShiftComponent } from './components/create-work-shift/create-work-shift.component';
import { UpdateWorkShiftComponent } from './components/update-work-shift/update-work-shift.component';
import { ShiftPlanningFiltersComponent } from './components/shift-planning-filters/shift-planning-filters.component';
import { CreateManyWorkShiftComponent } from './components/create-many-work-shift/create-many-work-shift.component';

FullCalendarModule.registerPlugins([
  // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin,
]);

@NgModule({
  declarations: [
    ShiftPlanningComponent,
    CreateWorkShiftComponent,
    UpdateWorkShiftComponent,
    ShiftPlanningFiltersComponent,
    CreateManyWorkShiftComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ConfirmDialogModule,
    RouterModule.forChild([
      {
        path: '',
        component: ShiftPlanningComponent,
        canActivate: [AuthGuard],
      },
    ]),
    FullCalendarModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgbDatepickerModule,
  ],
  entryComponents: [CreateWorkShiftComponent, UpdateWorkShiftComponent, CreateManyWorkShiftComponent],
})
export class ShiftPlanningModule {}
