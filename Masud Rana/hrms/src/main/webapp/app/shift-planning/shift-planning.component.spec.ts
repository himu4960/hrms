import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { provideMockStore } from '@ngrx/store/testing';
import { TranslateService } from '@ngx-translate/core';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';

import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import interactionPlugin from '@fullcalendar/interaction'; // a plugin
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { ShiftPlanningComponent } from './shift-planning.component';
import { ShiftPlanningFiltersComponent } from './components/shift-planning-filters/shift-planning-filters.component';

import { PermissionService } from 'app/shared/service/permission.service';
import { UtilService } from 'app/shared/service/util.service';

import { SharedModule } from 'app/shared/shared.module';

describe('ShiftPlanningComponent', () => {
  let component: ShiftPlanningComponent;
  let fixture: ComponentFixture<ShiftPlanningComponent>;
  let fakeService: {};

  FullCalendarModule.registerPlugins([
    // register FullCalendar plugins
    dayGridPlugin,
    interactionPlugin,
  ]);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        FullCalendarModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        NgbDatepickerModule,
        FormsModule,
        NgMultiSelectDropDownModule,
        SharedModule,
      ],
      declarations: [ShiftPlanningComponent, ShiftPlanningFiltersComponent],
      providers: [
        provideMockStore(),
        { provide: PermissionService, useValue: fakeService },
        UtilService,
        { provide: TranslateService, useValue: fakeService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftPlanningComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
