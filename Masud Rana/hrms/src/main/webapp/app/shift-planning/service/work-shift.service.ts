import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { createRequestOption } from 'app/core/request/request-util';

import {
  getWorkShiftIdentifier,
  ICreateManyWorkShift,
  IShiftPlanning,
  IShiftPlanningFilterParams,
  IWorkShift,
} from '../models/work-shift.model';

import { ApplicationConfigService } from 'app/core/config/application-config.service';

export type EntityResponseType = HttpResponse<IWorkShift>;
export type EntityArrayResponseType = HttpResponse<IWorkShift[]>;
export type ShiftPlanningResponseType = HttpResponse<IShiftPlanning>;
export type ShiftPlanningArrayResponseType = HttpResponse<IShiftPlanning[]>;

@Injectable({ providedIn: 'root' })
export class WorkShiftService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('work-shifts');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(workShift: IWorkShift): Observable<EntityResponseType> {
    return this.http.post<IWorkShift>(this.resourceUrl, workShift, { observe: 'response' });
  }

  createMany(manyWorkShift: ICreateManyWorkShift): Observable<any> {
    return this.http.post<IWorkShift[]>(`${this.resourceUrl}/many`, manyWorkShift, { observe: 'response' });
  }

  update(workShift: IWorkShift, id: number): Observable<EntityResponseType> {
    return this.http.put<IWorkShift>(`${this.resourceUrl}/${id}`, workShift, {
      observe: 'response',
    });
  }

  partialUpdate(workShift: IWorkShift): Observable<EntityResponseType> {
    return this.http.patch<IWorkShift>(`${this.resourceUrl}/${getWorkShiftIdentifier(workShift) as number}`, workShift, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IWorkShift>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getShiftPlanning(shiftPlanningFilterParams?: IShiftPlanningFilterParams): Observable<ShiftPlanningResponseType> {
    if (shiftPlanningFilterParams) {
      return this.http.post<IShiftPlanning>(`${this.resourceUrl}/filter`, shiftPlanningFilterParams, { observe: 'response' });
    }
    return this.http.post<IShiftPlanning>(`${this.resourceUrl}/filter`, {}, { observe: 'response' });
  }

  getUpcomingShifts(isAll: boolean = false): Observable<ShiftPlanningResponseType> {
    return this.http.get<IShiftPlanning>(`${this.resourceUrl}/upcoming?isAll=${isAll}`, { observe: 'response' });
  }

  getUpcomingShiftsByEmployeeId(employeeId: number, isAll: boolean = false): Observable<ShiftPlanningResponseType> {
    return this.http.get<IShiftPlanning>(`${this.resourceUrl}/upcoming/${employeeId}?isAll=${isAll}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IWorkShift[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  deleteByDate(startDate: number | null, endDate: number | null): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}`, { body: { start_date: startDate, end_date: endDate }, observe: 'response' });
  }

  publish(ids: number[]): Observable<HttpResponse<{}>> {
    return this.http.put(`${this.resourceUrl}/publish`, ids, { observe: 'response' });
  }

  addWorkShiftToCollectionIfMissing(
    workShiftCollection: IWorkShift[],
    ...workShiftsToCheck: (IWorkShift | null | undefined)[]
  ): IWorkShift[] {
    const workShifts: IWorkShift[] = workShiftsToCheck.filter(isPresent);
    if (workShifts.length > 0) {
      const workShiftCollectionIdentifiers = workShiftCollection.map(workShiftItem => getWorkShiftIdentifier(workShiftItem)!);
      const workShiftsToAdd = workShifts.filter(workShiftItem => {
        const workShiftIdentifier = getWorkShiftIdentifier(workShiftItem);
        if (workShiftIdentifier == null || workShiftCollectionIdentifiers.includes(workShiftIdentifier)) {
          return false;
        }
        workShiftCollectionIdentifiers.push(workShiftIdentifier);
        return true;
      });
      return [...workShiftsToAdd, ...workShiftCollection];
    }
    return workShiftCollection;
  }

  protected convertHoursToSeconds(duration: string): number {
    const [hours, minutes, seconds] = duration.split(':');
    return Number(hours) * 60 * 60 + Number(minutes) * 60 + Number(seconds);
  }
}
