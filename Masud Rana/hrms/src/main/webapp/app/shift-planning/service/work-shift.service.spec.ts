import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IWorkShift, WorkShift } from '../models/work-shift.model';

import { WorkShiftService } from './work-shift.service';

describe('Service Tests', () => {
  describe('WorkShift Service', () => {
    let service: WorkShiftService;
    let httpMock: HttpTestingController;
    let elemDefault: IWorkShift;
    let expectedResult: IWorkShift | IWorkShift[] | boolean | null;
    let currentDate: number;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(WorkShiftService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = Date.now();

      elemDefault = {
        id: 0,
        branch_id: 0,
        title: 'AAAAAAA',
        designation_id: 0,
        is_published: false,
        published_at: currentDate,
        published_by: 0,
        start_date: currentDate,
        end_date: currentDate,
        start_time: 123,
        end_time: 456,
        total_work_shift_duration: 0,
        deleted_at: currentDate,
        deleted_by: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            published_at: currentDate,
            start_date: currentDate,
            end_date: currentDate,
            start_time: currentDate,
            end_time: currentDate,
            deleted_at: currentDate,
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a WorkShift', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            published_at: currentDate,
            start_date: currentDate,
            end_date: currentDate,
            start_time: currentDate,
            end_time: currentDate,
            deleted_at: currentDate,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            published_at: currentDate,
            start_date: currentDate,
            end_date: currentDate,
            start_time: currentDate,
            end_time: currentDate,
            deleted_at: currentDate,
          },
          returnedFromService
        );

        service.create(new WorkShift()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a WorkShift', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            company_id: 1,
            branch_id: 1,
            title: 'BBBBBB',
            designation_id: 1,
            is_published: true,
            published_at: currentDate,
            published_by: 1,
            start_date: currentDate,
            end_date: currentDate,
            start_time: currentDate,
            end_time: currentDate,
            total_work_shift_duration: 1,
            deleted_at: currentDate,
            deleted_by: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            published_at: currentDate,
            start_date: currentDate,
            end_date: currentDate,
            start_time: currentDate,
            end_time: currentDate,
            deleted_at: currentDate,
          },
          returnedFromService
        );

        service.update(expected, 1).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a WorkShift', () => {
        const patchObject = Object.assign(
          {
            company_id: 1,
            branch_id: 1,
            title: 'BBBBBB',
            designation_id: 1,
            is_published: true,
            end_date: currentDate,
            start_time: currentDate,
            total_work_shift_duration: 1,
            deleted_at: currentDate,
          },
          new WorkShift()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            published_at: currentDate,
            start_date: currentDate,
            end_date: currentDate,
            start_time: currentDate,
            end_time: currentDate,
            deleted_at: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of WorkShift', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            company_id: 1,
            branch_id: 1,
            title: 'BBBBBB',
            designation_id: 1,
            is_published: true,
            published_at: currentDate,
            published_by: 1,
            start_date: currentDate,
            end_date: currentDate,
            start_time: currentDate,
            end_time: currentDate,
            total_work_shift_duration: 1,
            deleted_at: currentDate,
            deleted_by: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            published_at: currentDate,
            start_date: currentDate,
            end_date: currentDate,
            start_time: currentDate,
            end_time: currentDate,
            deleted_at: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a WorkShift', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addWorkShiftToCollectionIfMissing', () => {
        it('should add a WorkShift to an empty array', () => {
          const workShift: IWorkShift = { id: 123 };
          expectedResult = service.addWorkShiftToCollectionIfMissing([], workShift);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(workShift);
        });

        it('should not add a WorkShift to an array that contains it', () => {
          const workShift: IWorkShift = { id: 123 };
          const workShiftCollection: IWorkShift[] = [
            {
              ...workShift,
            },
            { id: 456 },
          ];
          expectedResult = service.addWorkShiftToCollectionIfMissing(workShiftCollection, workShift);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a WorkShift to an array that doesn't contain it", () => {
          const workShift: IWorkShift = { id: 123 };
          const workShiftCollection: IWorkShift[] = [{ id: 456 }];
          expectedResult = service.addWorkShiftToCollectionIfMissing(workShiftCollection, workShift);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(workShift);
        });

        it('should add only unique WorkShift to an array', () => {
          const workShiftArray: IWorkShift[] = [{ id: 123 }, { id: 456 }, { id: 9286 }];
          const workShiftCollection: IWorkShift[] = [{ id: 123 }];
          expectedResult = service.addWorkShiftToCollectionIfMissing(workShiftCollection, ...workShiftArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const workShift: IWorkShift = { id: 123 };
          const workShift2: IWorkShift = { id: 456 };
          expectedResult = service.addWorkShiftToCollectionIfMissing([], workShift, workShift2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(workShift);
          expect(expectedResult).toContain(workShift2);
        });

        it('should accept null and undefined values', () => {
          const workShift: IWorkShift = { id: 123 };
          expectedResult = service.addWorkShiftToCollectionIfMissing([], null, workShift, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(workShift);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
