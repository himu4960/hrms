import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { of, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { NgbCalendar, NgbDate, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { TranslateService } from '@ngx-translate/core';

import { CalendarOptions, DatesSetArg, EventClickArg, EventContentArg, EventDef, FullCalendarComponent } from '@fullcalendar/angular';
import resourceTimelinePlugin from '@fullcalendar/resource-timeline';
import { DateClickArg } from '@fullcalendar/interaction';

import { CreateWorkShiftComponent } from './components/create-work-shift/create-work-shift.component';
import { UpdateWorkShiftComponent } from './components/update-work-shift/update-work-shift.component';
import { CreateManyWorkShiftComponent } from './components/create-many-work-shift/create-many-work-shift.component';

import { ILeave } from '../leave/model/leave.model';
import { Employee } from 'app/employee/models/employee.model';
import { IHoliday } from 'app/entities/holiday/holiday.model';
import { IShiftPlanningFilterParams, IWorkShift } from './models/work-shift.model';

import { EModalReasonType } from 'app/shared/enum/modal-reason-type.enum';

import { AppState } from 'app/store';

import { User, UserCompany } from 'app/store/states/user.interface';
import { ShiftPlanningSettings } from 'app/store/states/shift-planning-settings.interface';
import { LeaveAvailabilitySettings } from 'app/store/states/leave-availability-settings.interface';

import { selectCurrentCompany, selectUser } from 'app/store/user/user.selectors';
import { selectLeaveAvailabilitySettings, selectShiftPlanningSettings } from 'app/store/company-settings/company-settings.selectors';

import { UtilService } from 'app/shared/service/util.service';
import { WorkShiftService } from './service/work-shift.service';
import { ToastService } from 'app/shared/components/toast/toast.service';
import { PermissionService } from 'app/shared/service/permission.service';

export enum ELeaveStatus {
  PENDING = 'Pending',
  APPROVED = 'Approved',
  REJECTED = 'Rejected',
}

export enum EResourceTimeline {
  DAY = 'resourceTimelineDay',
  WEEK = 'resourceTimelineWeek',
  MONTH = 'resourceTimelineMonth',
  CUSTOM = 'resourceTimeline',
}

@Component({
  selector: 'hrms-shift-planning',
  templateUrl: './shift-planning.component.html',
  styleUrls: ['./shift-planning.component.scss'],
})
export class ShiftPlanningComponent implements OnInit, OnDestroy {
  @ViewChild('calendar') calendarComponent!: FullCalendarComponent;

  isAllowEmployeeToAddWorkShift = false;

  isLoading = false;
  isEmployee = true;
  isWorkShiftExist = false;
  calendarStartDateIsTaken = false;

  startDate!: string | undefined;
  endDate!: string | undefined;

  resources: any[] = [];
  events: any[] = [];
  unPublishedIds: any[] = [];
  holiDays: IHoliday[] = [];
  weekendDays: number[] = [];
  markDisabled: any;

  shiftPlanningSettings!: ShiftPlanningSettings;
  leaveSettings!: LeaveAvailabilitySettings;

  user!: User;
  currentCompany!: UserCompany;
  datesSet!: DatesSetArg;

  employeeId!: any;
  calendarStartDate!: Date;
  minDate: any;

  calendarOptions: CalendarOptions = {
    plugins: [resourceTimelinePlugin],
    dateClick: this.handleDateClick.bind(this), // bind is important!
    eventClick: this.handleEventClick.bind(this),
    eventContent: this.handleEventContent.bind(this),
    datesSet: this.handleDateChanged.bind(this),
    resourceLabelContent: this.handleResourceRender.bind(this),
    timeZone: 'local',
    initialView: EResourceTimeline.MONTH,
    aspectRatio: 2,
    headerToolbar: {
      left: 'prev,next',
      center: 'title',
      right: `${EResourceTimeline.DAY},${EResourceTimeline.WEEK},${EResourceTimeline.MONTH}`,
    },
    views: {
      resourceTimelineMonth: {
        slotLabelFormat: [{ weekday: 'short', day: '2-digit' }],
      },
    },
    editable: false,
    resourceAreaHeaderContent: 'Employees',
    resourceAreaWidth: '25%',
    eventResourceEditable: false,
    expandRows: false,
    eventStartEditable: false,
  };

  isFirstFilterEmit = false;

  filterParams: IShiftPlanningFilterParams = {
    branch_id: 0,
    designation_ids: [],
    start_date: '',
    end_date: '',
    employee_ids: [],
    skill_ids: [],
  };

  @ViewChild('deleteModal') deleteModal!: ShiftPlanningComponent;

  private subscriptions: Subscription = new Subscription();

  constructor(
    protected modalService: NgbModal,
    private workShiftService: WorkShiftService,
    private store: Store<AppState>,
    private permissionService: PermissionService,
    private utilService: UtilService,
    private toastService: ToastService,
    private translateService: TranslateService,
    private calendar: NgbCalendar
  ) {
    const current = new Date();
    this.minDate = {
      year: current.getFullYear(),
      month: current.getMonth() + 1,
      day: current.getDate(),
    };
  }

  ngOnInit(): void {
    this.getInitialStore();
  }

  public getInitialStore(): void {
    this.subscriptions.add(
      this.store
        .select(selectUser)
        .pipe(
          switchMap((user: User) => {
            this.user = user;
            return this.store.select(selectCurrentCompany);
          }),
          switchMap((company: UserCompany) => {
            this.currentCompany = company;
            this.employeeId = this.currentCompany?.employee?.id;
            if (company?.employee?.branch_id) {
              this.filterParams = {
                ...this.filterParams,
                branch_id: company.employee.branch_id,
              };
            }
            return this.store.select(selectShiftPlanningSettings);
          }),
          switchMap((shiftPlanningSettings: ShiftPlanningSettings) => {
            if (shiftPlanningSettings !== null) {
              this.shiftPlanningSettings = shiftPlanningSettings;
              this.isAllowEmployeeToAddWorkShift = shiftPlanningSettings.employee_can_add_work_units;

              this.calendarOptions.firstDay = this.shiftPlanningSettings.shift_planning_start_day;
            }
            return this.store.select(selectLeaveAvailabilitySettings);
          })
        )
        .subscribe((leaveAvailabilitySettings: LeaveAvailabilitySettings) => {
          this.isEmployee = this.permissionService.isEmployee();
          if (leaveAvailabilitySettings !== null) {
            this.leaveSettings = leaveAvailabilitySettings;
            this.weekendDays = JSON.parse(leaveAvailabilitySettings?.deduct_weekends_from_leaves);
            this.disableWeekendAndHolidays();
          }
        })
    );
  }

  disableWeekendAndHolidays(): void {
    const holiDays: IHoliday[] = this.holiDays;
    const weekends: number[] = this.weekendDays;
    const index = weekends ? weekends.indexOf(0) : null;
    if (index && index > -1) {
      // ngbDatePicker starts from Monday = 1 to Sunday = 7
      // whereas new Date() starts from Sunday = 0 to Saturday = 6
      weekends[index] = 7;
    }

    this.markDisabled = (date: NgbDate) => {
      let calenderDate: number;
      if (weekends?.length) {
        calenderDate = this.calendar.getWeekday(date);
        if (calenderDate === 7) {
          calenderDate = 0;
        }
      }
      if (holiDays.length) {
        return (
          holiDays.findIndex(holiDay => {
            const holiDayDate = holiDay?.date ? new Date(holiDay?.date) : undefined;
            let isDisable = false;
            if (holiDayDate) {
              if (weekends?.length) {
                isDisable =
                  (holiDayDate.getDate() === date.day &&
                    holiDayDate.getMonth() + 1 === date.month &&
                    holiDayDate.getFullYear() === date.year) ||
                  weekends.includes(calenderDate);
              } else {
                isDisable =
                  holiDayDate.getDate() === date.day &&
                  holiDayDate.getMonth() + 1 === date.month &&
                  holiDayDate.getFullYear() === date.year;
              }
            }
            return isDisable;
          }) !== -1
        );
      }

      return false;
    };
  }

  handleDateClick(event: DateClickArg): void {
    const resourceEmployeeId = event.resource?.extendedProps.employeeId;
    if (!this.shiftPlanningSettings.past_date_shifts_allowed && event.date < new Date()) {
      this.toastService.showWarningToast(this.translateService.instant('hrmsApp.workShift.past_date_shifts_not_allowed'));
    } else if (!this.isEmployee || (this.employeeId === resourceEmployeeId && this.shiftPlanningSettings.employee_can_add_work_units)) {
      this.createWorkShift(event);
    }
  }

  createWorkShift(event: DateClickArg): void {
    const modalRef = this.modalService.open(CreateWorkShiftComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.eventDate = event.date;
    modalRef.componentInstance.userData = event?.resource?._resource;
    modalRef.componentInstance.existingWorkShifts = this.getWorkShiftsByDate(event.date, event?.resource?._resource.id);
    modalRef.componentInstance.isPublished = this.shiftPlanningSettings.use_draft_publish_schedule_method;
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.CREATED) {
        this.buildPayload();
      }
    });
  }

  createManyWorkShifts(): void {
    const modalRef = this.modalService.open(CreateManyWorkShiftComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.minDate = this.minDate;
    modalRef.componentInstance.markDisabled = this.markDisabled;
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.CREATED) {
        this.buildPayload();
      }
    });
  }

  getWorkShiftsByDate(date: Date, eventResourceId: any): any {
    const calendarClickDate = new Date(date).getDate();
    const workShifts = this.events.filter(event => {
      const eventDate = new Date(event.start).getDate();
      return calendarClickDate === eventDate && +event.resourceId === +eventResourceId;
    });
    return workShifts;
  }

  handleEventClick(event: EventClickArg): void {
    const workShiftData = event.event._def;
    const resourceEmployeeId = workShiftData?.extendedProps?.employeeId;
    if (!this.isEmployee || (this.employeeId === resourceEmployeeId && workShiftData?.extendedProps?.dataForUpdate)) {
      this.updateWorkShift(workShiftData);
    }
  }

  updateWorkShift(workShiftData: EventDef): void {
    const modalRef = this.modalService.open(UpdateWorkShiftComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.workShiftData = workShiftData;
    if (!this.isEmployee || (this.isEmployee && this.shiftPlanningSettings.employee_can_drop_shifts)) {
      modalRef.componentInstance.hideDeleteButton = true;
    }

    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.UPDATED || EModalReasonType.DELETED) {
        this.buildPayload();
      }
    });
  }

  handleDateChanged(datesSet: DatesSetArg): void {
    this.datesSet = datesSet;
    if (this.permissionService?.isEmployee() && this.shiftPlanningSettings.employee_can_view_shifts_in_advance > 1) {
      if (!this.calendarStartDateIsTaken) {
        this.calendarStartDate = new Date(datesSet.startStr);
        this.calendarStartDateIsTaken = true;
      }
      const targetDate = this.utilService.getFutureDateAfterDays(
        this.calendarStartDate,
        this.shiftPlanningSettings.employee_can_view_shifts_in_advance - 1
      );
      this.setStartAndEndDateForViewInAdvance(this.calendarStartDate.toISOString(), targetDate.toISOString());
    } else {
      this.setStartAndEndDate(datesSet);
    }
    this.buildPayload();
  }

  setStartAndEndDateForViewInAdvance(startDate: string, endDate: string): void {
    this.startDate = startDate;
    this.endDate = endDate;
  }

  setStartAndEndDate(datesSet: DatesSetArg): void {
    this.startDate = datesSet?.startStr;
    this.endDate = datesSet?.endStr;
  }

  buildPayload(): void {
    this.filterParams.start_date = this.startDate;
    this.filterParams.end_date = this.endDate;
    this.loadShiftPlanning(this.filterParams);
  }

  loadShiftPlanning(paylod: IShiftPlanningFilterParams): void {
    this.isLoading = true;
    this.subscriptions.add(
      this.workShiftService.getShiftPlanning(paylod).subscribe(
        (response: any) => {
          const data = response?.body;
          this.events = [];
          this.resources = [];
          this.unPublishedIds = [];
          this.isWorkShiftExist = false;

          if (data instanceof Array && data.length) {
            if (!this.isEmployee || (this.isEmployee && this.shiftPlanningSettings.employee_can_see_co_worker_shifts)) {
              data.forEach(employee => {
                this.createShiftPlanningEvents(employee);
              });
            } else {
              const employeeData = data.find((employee: Employee) => employee.id === this.employeeId);
              this.createShiftPlanningEvents(employeeData);
            }
          }
          this.calendarOptions['events'] = this.events;
          this.calendarOptions['resources'] = this.resources;
          this.isLoading = false;
        },
        () => {
          this.isLoading = false;
        }
      )
    );
  }

  createShiftPlanningEvents(employee: any): void {
    const resource = this.buildResource(employee);
    this.resources.push(resource);

    let tempEvents: any[] = [];

    if (employee?.work_shifts instanceof Array && employee?.work_shifts.length) {
      this.isWorkShiftExist = true;
      employee?.work_shifts.forEach((workShift: IWorkShift) => {
        let title = workShift?.title;

        if (
          (!this.isEmployee || (this.shiftPlanningSettings.employee_can_release_shifts && employee?.id === this.employeeId)) &&
          !workShift.is_published
        ) {
          this.unPublishedIds.push(workShift?.id);
          if (!workShift?.title) {
            title = `Work Shift ${workShift.id}`;
            title = title.concat(' unPublished');
          }
        } else {
          title = workShift?.title ? workShift?.title : `Work Shift ${workShift.id}`;
        }

        const event = this.buildEventForWorkshift(employee?.id, employee.branch_id, title, workShift);
        if (tempEvents.length < 1) {
          tempEvents.push(event);
        } else {
          tempEvents = this.checkConflicts(tempEvents, event);
        }
      });

      tempEvents.forEach(event => {
        this.events.push(event);
      });
    }

    if (
      employee?.leaves instanceof Array &&
      employee?.leaves.length &&
      (!this.isEmployee || (this.isEmployee && this.shiftPlanningSettings.employee_can_see_co_worker_vacations))
    ) {
      employee?.leaves.forEach((leave: ILeave) => {
        if (this.shiftPlanningSettings.show_pending_leave_request && leave.status === ELeaveStatus.PENDING) {
          const event = this.buildEventForLeave(employee?.id, leave);
          this.events.push(event);
        } else {
          if (leave.status === ELeaveStatus.APPROVED) {
            const event = this.buildEventForLeave(employee?.id, leave);
            this.events.push(event);
          }
        }
      });
    }
  }

  onTimeRangeEmit(filter: any): void {
    const from = this.utilService.convertYearMonthDayIntoDate(filter.from);
    const to = this.utilService.convertYearMonthDayIntoDate(filter.to);
    to.setDate(to.getDate() + 1);

    this.calendarOptions.initialView = EResourceTimeline.CUSTOM;
    this.calendarOptions.visibleRange = {
      start: this.utilService.format(from),
      end: this.utilService.format(to),
    };
    this.calendarComponent.ngAfterViewInit();
  }

  onFiltersEmit(filters: IShiftPlanningFilterParams): void {
    if (!this.isFirstFilterEmit) {
      this.filterParams = filters;
      this.isFirstFilterEmit = true;
    } else {
      this.filterParams = filters;
      this.buildPayload();
    }
  }

  buildResource(employee: any): any {
    if (this.shiftPlanningSettings.show_employee_skills_in_scheduler) {
      let skills = '';
      let title = `${employee?.first_name} ${employee?.last_name}`;
      if (employee?.skills instanceof Array && employee?.skills.length) {
        employee?.skills.forEach((skill: { id: number; name: string }) => {
          skills = skills.concat(`${skill?.name}, `);
        });
        title = title.concat(`(Skills): ${skills}`);
      }
      return {
        id: employee?.id,
        title: title,
        branchId: employee?.branch_id,
        employeeId: employee?.id,
        photo_url: employee?.photo_url,
        employeeDesignations: employee.employee_designations,
      };
    }
    return {
      id: employee?.id,
      title: `${employee?.first_name} ${employee?.last_name}`,
      branchId: employee?.branch_id,
      employeeId: employee?.id,
      photo_url: employee?.photo_url,
      employeeDesignations: employee.employee_designations,
    };
  }

  buildEventForWorkshift(employeeId: any, employeeBranchId: any, title: any, workShift: IWorkShift): any {
    const startDate = workShift.start_date ? new Date(Number(workShift.start_date)) : null;
    const endDate = workShift.end_date ? new Date(Number(workShift.end_date)) : null;

    return {
      resourceId: employeeId,
      title: title,
      start: startDate,
      end: endDate,
      workShiftId: workShift.id,
      employeeId: employeeId,
      dataForUpdate: {
        title: title,
        start_date: startDate,
        end_date: endDate,
        start_time: workShift.start_time,
        end_time: workShift.end_time,
        designation_id: 1,
        branch_id: employeeBranchId,
        skills: workShift.skills,
        note: workShift.note,
      },
    };
  }

  buildEventForLeave(employeeId: any, leave: ILeave): any {
    return {
      resourceId: employeeId,
      title: 'Leave',
      start: leave?.start_date,
      end: leave?.end_date,
      leaveId: leave.id,
    };
  }

  checkConflicts(workShifts: any[], workShiftToAdd: any): any[] {
    let newWorkShiftToAdd = {};
    let newWorkshifts: any[] = [];

    newWorkshifts = workShifts.map((workShift: any) => {
      const workShiftStartDate = this.utilService.getDateByDate(workShift['start']);
      const workShiftEndDate = this.utilService.getDateByDate(workShift['end']);

      const eventStartDate = this.utilService.getDateByDate(workShiftToAdd['start']);
      const eventEndTimeDate = this.utilService.getDateByDate(workShiftToAdd['end']);

      const workShiftRange = this.utilService.getDateRange(workShiftStartDate, workShiftEndDate);
      const eventRange = this.utilService.getDateRange(eventStartDate, eventEndTimeDate);

      if (workShiftRange.overlaps(eventRange)) {
        if (!workShift['title'].includes('*')) {
          newWorkShiftToAdd = {
            ...workShiftToAdd,
            title: workShiftToAdd['title'].concat(' *'),
          };
          return {
            ...workShift,
            title: workShift['title'].concat(' *'),
          };
        } else {
          newWorkShiftToAdd = {
            ...workShiftToAdd,
            title: workShiftToAdd['title'].concat(' *'),
          };
          return workShift;
        }
      }
      newWorkShiftToAdd = {
        ...workShiftToAdd,
      };
      return workShift;
    });

    newWorkshifts.push(newWorkShiftToAdd);
    return newWorkshifts;
  }

  handleEventContent(eventContent: EventContentArg): any {
    const italicEl = document.createElement('i');
    if (eventContent.event.extendedProps.isUrgent) {
      italicEl.innerHTML = `${eventContent.event._def.title}`;
    } else {
      italicEl.innerHTML = `${eventContent.event._def.title}`;
    }
    const arrayOfDomNodes = [italicEl];
    return { domNodes: arrayOfDomNodes };
  }

  handleResourceRender(resourceLabelContent: any): any {
    const extendedProps = resourceLabelContent?.resource?._resource?.extendedProps;
    const designations = extendedProps?.employeeDesignations ?? [];

    const designation = designations.length ? designations[0]?.designation?.name : '';
    const photo_url = extendedProps?.photo_url;
    const h6El = document.createElement('h6');
    h6El.innerHTML = `
    <div class="row align-items-center w-100 m-0 p-0">
      <div class="col-12 col-lg-3">
        <img class="img-square-md img-rounded" src=${photo_url ? photo_url : '../../../../content/images/person.png'} />
      </div>
      <div class="col-12 col-lg-9">
        <div>
            <h6 class="text-primary font-weight-bold shift-planning-resource-h6 fs-default">${resourceLabelContent.fieldValue}</h6>
            <p class="m-0 font-weight-bold text-dark fs-xs shift-planning-resource-p">${designation}</p>
        </div>
      </div>
    </div>`;
    const arrayOfDomNodes = [h6El];
    return { domNodes: arrayOfDomNodes };
  }

  publish(): void {
    this.workShiftService.publish(this.unPublishedIds).subscribe(() => {
      this.buildPayload();
      this.unPublishedIds = [];
    });
  }

  public showDeleteModal(): void {
    this.modalService.open(this.deleteModal, { size: 'md', backdrop: 'static' });
  }

  deleteWorkShiftByDate(): void {
    const startDate = this.startDate ? new Date(this.startDate).getTime() : null;
    const endDate = this.endDate ? new Date(this.endDate).getTime() : null;
    this.workShiftService.deleteByDate(startDate, endDate).subscribe(() => {
      this.buildPayload();
      this.isWorkShiftExist = false;
      this.modalService.dismissAll();
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
