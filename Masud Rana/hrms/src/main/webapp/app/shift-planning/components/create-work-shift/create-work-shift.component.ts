import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Store } from '@ngrx/store';

import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { AppState } from 'app/store';
import { ShiftPlanningSettings } from 'app/store/states/shift-planning-settings.interface';
import { selectShiftPlanningSettings } from 'app/store/company-settings/company-settings.selectors';
import { EModalReasonType } from 'app/shared/enum';

import { ConfirmDialogComponent } from 'app/shared/components/confirm-dialog/confirm-dialog.component';
import { scheduleWithinTimes } from 'app/account/admin-settings/components/resources/shift-planning-settings';

import { IBranch } from '../../../entities/branch/branch.model';
import { IWorkShift } from '../../models/work-shift.model';
import { IWorkShiftTemplate } from 'app/entities/work-shift-template/work-shift-template.model';

import { UtilService } from 'app/shared/service/util.service';
import { WorkShiftService } from '../../service/work-shift.service';
import { WorkShiftTemplateService } from 'app/entities/work-shift-template/service/work-shift-template.service';

@Component({
  selector: 'hrms-create-work-shift',
  templateUrl: './create-work-shift.component.html',
  styleUrls: ['./create-work-shift.component.scss'],
})
export class CreateWorkShiftComponent implements OnInit, OnDestroy {
  isLoading = false;
  eventDate!: Date;
  startDateTimeFromEvent!: Date;
  branches: IBranch[] = [];
  branchId!: number;
  designationId!: number;
  userData!: any;
  isPublished!: boolean;
  employeeDesignations!: [];
  existingWorkShifts!: [];
  workShiftTemplates!: IWorkShiftTemplate[];
  conflictedWorkShifts = [];
  scheduleWithinTimes = scheduleWithinTimes;
  selectedTemplateIndex!: number;
  message = '';

  shiftPlanningSettings!: ShiftPlanningSettings;
  createWorkShiftForm = new FormGroup({});

  private subscriptions: Subscription = new Subscription();

  constructor(
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private store: Store<AppState>,
    private workShiftService: WorkShiftService,
    private workShiftTemplateService: WorkShiftTemplateService,
    private utilService: UtilService
  ) {}

  ngOnInit(): void {
    this.branchId = this.userData['extendedProps']['branchId'];
    this.startDateTimeFromEvent = new Date(this.eventDate);
    const startDateFromEvent = this.utilService.format(this.eventDate);
    const calenderClickedStartTime = this.utilService.formatTime(this.eventDate.toISOString());

    const calendarClickedDateTime = new Date(this.eventDate.setHours(this.eventDate.getHours() + 8)).toISOString();
    const endDateFromEvent = this.utilService.format(calendarClickedDateTime);
    const calenderClickedEndTime = this.utilService.formatTime(calendarClickedDateTime);

    this.createWorkShiftForm = this.fb.group({
      title: new FormControl(''),
      designation_id: new FormControl('', [Validators.required]),
      start_date: new FormControl(startDateFromEvent, [Validators.required]),
      end_date: new FormControl(endDateFromEvent, [Validators.required]),
      start_time: new FormControl(calenderClickedStartTime, [Validators.required]),
      end_time: new FormControl(calenderClickedEndTime, [Validators.required]),
      timeDifference: new FormControl(''),
    });

    const designationControl = this.createWorkShiftForm.get('designation_id') as AbstractControl;
    const startDateControl = this.createWorkShiftForm.get('start_date') as AbstractControl;
    const startTimeControl = this.createWorkShiftForm.get('start_time') as AbstractControl;
    const endTimeControl = this.createWorkShiftForm.get('end_time') as AbstractControl;

    this.subscriptions.add(
      this.store.select(selectShiftPlanningSettings).subscribe((shiftPlanningSettings: ShiftPlanningSettings) => {
        this.shiftPlanningSettings = shiftPlanningSettings;
      })
    );

    this.getBranches();
    this.employeeDesignations = this.userData['extendedProps']['employeeDesignations'];

    this.setTimeDifference(startDateControl.value, startTimeControl.value, endTimeControl.value);

    startTimeControl?.valueChanges.subscribe(newStartTime => {
      this.setTimeDifference(startDateControl.value, newStartTime, endTimeControl.value);
      if (this.shiftPlanningSettings.employee_cannot_be_scheduled_within_time > this.scheduleWithinTimes[0]?.value) {
        this.checkEmployeeCanNotBeScheduledWithinTime(newStartTime);
      }
    });

    endTimeControl?.valueChanges.subscribe(endtime => {
      this.setTimeDifference(startDateControl.value, startTimeControl.value, endtime);
    });

    designationControl?.valueChanges
      .pipe(
        switchMap(designationId => {
          this.designationId = designationId;
          return this.workShiftTemplateService.query();
        })
      )
      .subscribe((res: HttpResponse<IWorkShiftTemplate[]>) => {
        const workShiftTemplates = res.body;
        if (workShiftTemplates?.length) {
          this.workShiftTemplates = workShiftTemplates?.filter(
            workShiftTemplate => workShiftTemplate.branch_id === this.branchId && workShiftTemplate.designation_id === this.designationId
          );
        }
      });
  }

  setTimeDifference(startDate: string, startTime: string, endTime: string): void {
    const formatedStartDate = this.utilService.combineDateAndTime(this.utilService.format(startDate), startTime);
    const timeDifference = this.utilService.getTimeDifference(startTime, endTime);
    const endDate = new Date(formatedStartDate).setSeconds(
      new Date(formatedStartDate).getSeconds() + this.utilService.convertHoursToSeconds(timeDifference)
    );
    this.createWorkShiftForm.get('timeDifference')?.patchValue(timeDifference);
    this.createWorkShiftForm.get('end_date')?.patchValue(this.utilService.format(new Date(endDate)));
  }

  cancel(): void {
    this.activeModal.dismiss();
  }

  create(): void {
    if (!this.shiftPlanningSettings.shift_overlapping_disallowed) {
      this.message = '';
      this.checkConflicts();
      if (this.conflictedWorkShifts.length) {
        const modalRef = this.modalService.open(ConfirmDialogComponent, { size: 'lg', backdrop: 'static' });
        modalRef.componentInstance.message = 'This shift will conflict with other work shifts!';
        modalRef.closed.subscribe(() => {
          this.workShiftService.create(this.buildWorkshiftPayload()).subscribe(() => {
            this.activeModal.close(EModalReasonType.CREATED);
          });
        });
      } else {
        this.workShiftService.create(this.buildWorkshiftPayload()).subscribe(() => {
          this.activeModal.close(EModalReasonType.CREATED);
        });
      }
    } else {
      this.message = 'Shift Overlappling is not allowed!';
    }
  }

  checkEmployeeCanNotBeScheduledWithinTime(startTime: string): void {
    if (this.existingWorkShifts.length) {
      this.existingWorkShifts.sort((a: any, b: any) => a?.start_time - b?.start_time);
      const lastWorkShift: any = this.existingWorkShifts[this.existingWorkShifts.length - 1];
      const workShiftEndTime = lastWorkShift.dataForUpdate.end_time as number;
      const currentStartTime = this.utilService.convertHoursToSeconds(startTime);
      const totalTime = workShiftEndTime + parseInt(this.getSecondsBySettingsValue(), 10);
      if (currentStartTime < totalTime) {
        this.message = `Can not be scheduled before ${this.utilService.convertSecondsIntoHoursAndMinutes(totalTime)}`;
      } else {
        this.message = '';
        this.createWorkShiftForm.setErrors({ invalid: false });
      }
    }
  }

  getSecondsBySettingsValue(): any {
    const resourceItem = this.scheduleWithinTimes.find(
      item => item.value === this.shiftPlanningSettings.employee_cannot_be_scheduled_within_time
    );
    return resourceItem?.seconds;
  }

  buildWorkshiftPayload(): IWorkShift {
    const { start_date, end_date, start_time, end_time, designation_id } = this.createWorkShiftForm.value;

    const startDate = this.utilService.getFormattedDateToCreateWorkShift(new Date(start_date), start_time);
    const endDate = this.utilService.getFormattedDateToCreateWorkShift(new Date(end_date), end_time);

    const startTimeInNumber = this.utilService.convertHoursToSeconds(start_time) ? this.utilService.convertHoursToSeconds(start_time) : 1;
    const endTimeInNumber = this.utilService.convertHoursToSeconds(end_time) ? this.utilService.convertHoursToSeconds(end_time) : 1;

    const workShift: IWorkShift = {
      employee_id: this.userData['extendedProps']['employeeId'],
      branch_id: this.branchId,
      designation_id: designation_id,
      start_date: new Date(startDate).getTime(),
      end_date: new Date(endDate).getTime(),
      start_time: startTimeInNumber,
      end_time: endTimeInNumber,
    };

    if (!this.isPublished) {
      workShift.is_published = true;
    }

    return workShift;
  }

  checkConflicts(): void {
    this.conflictedWorkShifts = [];
    const { start_time, end_time } = this.createWorkShiftForm.value;

    if (this.existingWorkShifts.length) {
      this.existingWorkShifts.forEach(workshift => {
        const workShiftStartDate = this.utilService.getDateByDate(workshift['start']);
        const workShiftEndDate = this.utilService.getDateByDate(workshift['end']);

        const convertedEventDate = this.utilService.format(this.eventDate);

        const eventStartDate = this.utilService.getDateByDateAndTime(convertedEventDate, start_time);
        const eventEndDate = this.utilService.getDateByDateAndTime(convertedEventDate, end_time);

        const workShiftDateRange = this.utilService.getDateRange(workShiftStartDate, workShiftEndDate);
        const eventDateRange = this.utilService.getDateRange(eventStartDate, eventEndDate);

        if (workShiftDateRange.overlaps(eventDateRange)) {
          this.conflictedWorkShifts.push(workshift);
        }
      });
    }
  }

  chooseTemplate(workShiftTemplate: IWorkShiftTemplate, index: number): void {
    const { start_date } = this.createWorkShiftForm.value;
    this.selectedTemplateIndex = index;
    if (workShiftTemplate?.start_time && workShiftTemplate?.end_time) {
      const start_time = this.utilService.convertSecondsIntoHoursAndMinutes(workShiftTemplate.start_time);
      const end_time = this.utilService.convertSecondsIntoHoursAndMinutes(workShiftTemplate.end_time);
      this.createWorkShiftForm.get('start_time')?.patchValue(start_time);
      this.createWorkShiftForm.get('end_time')?.patchValue(end_time);
      this.setTimeDifference(start_date, start_time, end_time);
    }
  }

  getBranches(): void {
    this.isLoading = true;
    this.utilService.getBranches().subscribe((branch: IBranch[]) => {
      this.isLoading = false;
      this.branches = branch ?? [];
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
