import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Subscription } from 'rxjs';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

import { AppState } from 'app/store';
import { selectShiftPlanningSettings } from 'app/store/company-settings/company-settings.selectors';
import { ShiftPlanningSettings } from 'app/store/states/shift-planning-settings.interface';
import { EModalReasonType } from 'app/shared/enum';
import { UtilService } from 'app/shared/service/util.service';
import { PermissionService } from 'app/shared/service/permission.service';
import { IWorkShift } from 'app/shift-planning/models/work-shift.model';
import { ISkill } from 'app/entities/skill/skill.model';

import { WorkShiftService } from '../../service/work-shift.service';

export enum EAllowedUsersForNote {
  MANAGERS_AND_SUPERVISORS = 1,
  MANAGERS_AND_SUPERVISORS_AND_SCHEDULER = 2,
  ALL_USERS = 3,
}

@Component({
  selector: 'hrms-update-work-shift',
  templateUrl: './update-work-shift.component.html',
  styleUrls: ['./update-work-shift.component.scss'],
})
export class UpdateWorkShiftComponent implements OnInit, OnDestroy {
  isEmployee = true;
  workShiftData!: any;
  dataForUpdate!: any;
  hideDeleteButton = false;
  isNotePermitted = false;
  shiftPlanningSettings!: ShiftPlanningSettings;
  isAllowEmployeeToAddWorkShift = false;

  selectedSkills: ISkill[] = [];
  dropdownList: ISkill[] = [];
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    selectAllText: 'Select All',
    unSelectAllText: 'Unselect All',
    itemsShowLimit: 3,
    allowSearchFilter: true,
  };

  updateWorkShiftForm = this.fb.group({
    title: new FormControl(''),
    designation_id: new FormControl('', [Validators.required]),
    start_date: new FormControl('', [Validators.required]),
    end_date: new FormControl('', [Validators.required]),
    start_time: new FormControl('', [Validators.required]),
    end_time: new FormControl('', [Validators.required]),
    branch_id: new FormControl(''),
    employee_id: new FormControl(''),
    timeDifference: new FormControl(''),
    skills: new FormControl([]),
    note: new FormControl(''),
  });

  startDateControl = this.updateWorkShiftForm.get('start_date') as AbstractControl;
  endDateControl = this.updateWorkShiftForm.get('end_date') as AbstractControl;
  startTimeControl = this.updateWorkShiftForm.get('start_time') as AbstractControl;
  endTimeControl = this.updateWorkShiftForm.get('end_time') as AbstractControl;
  skillsControl = this.updateWorkShiftForm.get('skills') as AbstractControl;
  noteControl = this.updateWorkShiftForm.get('note') as AbstractControl;

  private subscriptions: Subscription = new Subscription();

  constructor(
    public activeModal: NgbActiveModal,
    protected fb: FormBuilder,
    private workShiftService: WorkShiftService,
    private utilService: UtilService,
    private store: Store<AppState>,
    private permissionService: PermissionService
  ) {}

  ngOnInit(): void {
    this.subscriptions.add(
      this.store.select(selectShiftPlanningSettings).subscribe((settings: ShiftPlanningSettings) => {
        this.shiftPlanningSettings = settings;
        this.isAllowEmployeeToAddWorkShift = settings.employee_can_add_work_units;
        this.checkNotePermission();
        this.isEmployee = this.permissionService.isEmployee();
      })
    );

    this.getSkills();

    this.dataForUpdate = this.workShiftData?.extendedProps?.dataForUpdate;
    this.updateWorkShiftForm.patchValue(this.dataForUpdate);
    this.patchForm();

    this.setTimeDifference(
      this.startDateControl?.value,
      this.endDateControl?.value,
      this.startTimeControl?.value,
      this.endTimeControl?.value
    );

    this.startDateControl?.valueChanges.subscribe(startDate => {
      this.setTimeDifference(startDate, this.endDateControl?.value, this.startTimeControl?.value, this.endTimeControl?.value);
    });

    this.endDateControl?.valueChanges.subscribe(endDate => {
      this.setTimeDifference(this.startDateControl?.value, endDate, this.startTimeControl?.value, this.endTimeControl?.value);
    });

    this.startTimeControl?.valueChanges.subscribe(startTime => {
      this.setTimeDifference(this.startDateControl?.value, this.endDateControl?.value, startTime, this.endTimeControl?.value);
    });

    this.endTimeControl?.valueChanges.subscribe(endTime => {
      this.setTimeDifference(this.startDateControl?.value, this.endDateControl?.value, this.startTimeControl?.value, endTime);
    });
  }

  setTimeDifference(startDate: string, endDate: string, startTime: string, endTime: string): void {
    const formatedStartDate = this.utilService.combineDateAndTime(this.utilService.format(startDate), startTime);
    const formatedEndDate = this.utilService.combineDateAndTime(this.utilService.format(endDate), endTime);
    const timeDifference = (new Date(formatedEndDate).getTime() - new Date(formatedStartDate).getTime()) / 1000;

    this.updateWorkShiftForm.get('timeDifference')?.patchValue(this.utilService.getTimeFromSecond(timeDifference));
  }

  checkNotePermission(): void {
    if (this.isEmployee && this.shiftPlanningSettings.notes_permissions_allowed_users === EAllowedUsersForNote.ALL_USERS) {
      this.isNotePermitted = true;
    } else if (!this.isEmployee) {
      this.isNotePermitted = true;
    }
  }

  cancel(): void {
    this.activeModal.dismiss();
  }

  patchForm(): void {
    const startDate: any = this.dataForUpdate.start_date ? this.utilService.format(this.dataForUpdate.start_date) : null;
    const endDate = this.dataForUpdate.end_date ? this.utilService.format(this.dataForUpdate.end_date) : null;
    const startTime = this.utilService.convertSecondsIntoHoursAndMinutes(this.dataForUpdate?.start_time);
    const endTime = this.utilService.convertSecondsIntoHoursAndMinutes(this.dataForUpdate?.end_time);

    this.startDateControl?.patchValue(startDate);
    this.endDateControl?.patchValue(endDate);
    this.startTimeControl?.patchValue(startTime);
    this.endTimeControl?.patchValue(endTime);
    this.skillsControl?.patchValue(JSON.parse(this.dataForUpdate?.skills));
    this.noteControl?.patchValue(this.dataForUpdate?.note);
  }

  update(): void {
    const id = this.workShiftData?.extendedProps?.workShiftId;
    const workShift = this.buildWorkshiftPayload();
    this.workShiftService.update(workShift, id).subscribe(() => {
      this.activeModal.close(EModalReasonType.UPDATED);
    });
  }

  delete(): void {
    const id = this.workShiftData?.extendedProps?.workShiftId;
    this.workShiftService.delete(id).subscribe(() => {
      this.activeModal.close(EModalReasonType.DELETED);
    });
  }

  buildWorkshiftPayload(): IWorkShift {
    const { title, start_date, end_date, start_time, end_time, designation_id } = this.updateWorkShiftForm.value;
    const startDate = this.utilService.getFormattedDateToCreateWorkShift(new Date(start_date), start_time);
    const endDate = this.utilService.getFormattedDateToCreateWorkShift(new Date(end_date), end_time);
    let skills: ISkill[] = [];
    if (this.skillsControl?.value) {
      skills = this.skillsControl?.value.map((item: any) => item.id);
    }

    const startTimeInNumber = this.utilService.convertHoursToSeconds(start_time) ? this.utilService.convertHoursToSeconds(start_time) : 1;
    const endTimeInNumber = this.utilService.convertHoursToSeconds(end_time) ? this.utilService.convertHoursToSeconds(end_time) : 1;

    const workShift: IWorkShift = {
      title: title,
      designation_id: designation_id,
      start_date: new Date(startDate).getTime(),
      end_date: new Date(endDate).getTime(),
      start_time: startTimeInNumber,
      end_time: endTimeInNumber,
      skills: JSON.stringify(skills),
      note: this.noteControl?.value,
    };

    return workShift;
  }

  getSkills(): any {
    this.utilService.getSkills().subscribe((skills: ISkill[]) => {
      this.dropdownList = skills;
      const skillSet = new Set(JSON.parse(this.dataForUpdate?.skills));
      this.selectedSkills = this.dropdownList.filter((item: any) => skillSet.has(item.id));
    });
  }

  getFormattedDate(date: string[], time: string): Date {
    const formattedDate = new Date(parseInt(date[0], 10), parseInt(date[1], 10) - 1, parseInt(date[2], 10));
    formattedDate.setHours(parseInt(time.split(':')[0], 10), parseInt(time.split(':')[1], 10));
    return formattedDate;
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
