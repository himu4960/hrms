import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';

import { NgbCalendar, NgbDate, NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { DatesSetArg } from '@fullcalendar/angular';

import { Subscription } from 'rxjs';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

import { AppState } from 'app/store';
import { loadBranchesAction, loadSkillsAction } from 'app/store/company-properties/company-properties.actions';
import { selecteBranches, selectSkills } from 'app/store/company-properties/company-properties.selectors';
import { ISkill } from 'app/entities/skill/skill.model';
import { IBranch } from 'app/entities/branch/branch.model';
import { UtilService } from 'app/shared/service/util.service';
import { IEmployee } from 'app/employee/models/employee.model';
import { IDesignation } from 'app/entities/designation/designation.model';

@Component({
  selector: 'hrms-shift-planning-filters',
  templateUrl: './shift-planning-filters.component.html',
  styleUrls: ['./shift-planning-filters.component.scss'],
})
export class ShiftPlanningFiltersComponent implements OnInit, OnChanges, OnDestroy {
  @Input() shiftPlanningDates!: DatesSetArg;
  @Output() timeRange = new EventEmitter<any>();
  @Output() filters = new EventEmitter<any>();

  hoveredDate: NgbDate | null = null;
  fromDate: NgbDate;
  toDate: NgbDate | null = null;

  @ViewChild('datePicker') datePicker!: NgbDatepicker;

  branchId!: number | undefined;

  selectedSkills: ISkill[] = [];
  skillsDropdownList: ISkill[] = [];
  selectedDesignations: ISkill[] = [];
  designationDropdownList: ISkill[] = [];
  selectedEmployees: IEmployee[] = [];
  employeeDropdownList: IEmployee[] = [];
  branches: IBranch[] = [];

  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    allowSearchFilter: true,
  };

  private subscriptions: Subscription = new Subscription();

  constructor(private calendar: NgbCalendar, private store: Store<AppState>, private utilService: UtilService) {
    this.fromDate = this.calendar.getToday();
    this.toDate = null;
  }

  ngOnChanges(): void {
    if (this.shiftPlanningDates !== null) {
      const startDate = this.shiftPlanningDates?.start;
      const endDate = this.shiftPlanningDates?.end;

      this.datePicker?.navigateTo({ year: startDate.getFullYear(), month: startDate.getMonth() + 1 });
      this.setFromDateAndToDateForDatePicker(startDate, endDate);
    }
  }

  ngOnInit(): void {
    this.store.dispatch(loadSkillsAction());
    this.store.dispatch(loadBranchesAction());

    this.subscriptions.add(
      this.store.select(selectSkills).subscribe(skills => {
        this.skillsDropdownList = skills;
      })
    );

    this.subscriptions.add(
      this.store.select(selecteBranches).subscribe(branches => {
        if (branches !== null) {
          this.branches = branches;
          if (this.branches.length) {
            this.branchId = this.branches[0].id;
            this.getEmployees(this.branchId);
            const designations: IDesignation[] | undefined = this.branches[0]?.designations;
            if (designations?.length) {
              this.designationDropdownList = designations;
            }
            this.onSearch();
          }
        }
      })
    );
  }

  onDateSelection(date: NgbDate): void {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
    if (this.fromDate && this.toDate) {
      this.timeRange.emit({
        branch_id: this.branchId,
        designation_ids: this.selectedDesignations.map(designation => designation.id),
        skill_ids: this.selectedSkills.map(skill => skill.id),
        employee_ids: this.selectedEmployees.map(employee => employee.id),
        from: this.fromDate,
        to: this.toDate,
      });
    }
  }

  setFromDateAndToDateForDatePicker(startDate: Date, endDate: Date): void {
    const newEndDate = new Date();
    newEndDate.setFullYear(endDate?.getFullYear(), endDate?.getMonth(), endDate?.getDate() - 1);
    this.fromDate = this.getYearMonthDayObject(startDate);
    this.toDate = this.getYearMonthDayObject(newEndDate);
  }

  getYearMonthDayObject(date: Date): any {
    return {
      year: new Date(date).getFullYear(),
      month: new Date(date).getMonth() + 1,
      day: new Date(date).getDate(),
    };
  }

  isHovered(date: NgbDate): any {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate): any {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate): any {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onFiltersChange(event?: any): void {
    const branchId = parseInt(event?.target?.value, 10);
    this.branchId = branchId;
    this.getEmployees(branchId);
    const designations: IDesignation[] | undefined = this.branches.find(branch => branch.id === branchId)?.designations;
    if (designations?.length) {
      this.designationDropdownList = designations;
    } else {
      this.designationDropdownList = [];
    }
    this.onSearch();
  }

  getEmployees(branchId: number | undefined): void {
    this.subscriptions.add(
      this.utilService.getEmployees().subscribe((response: any[]) => {
        this.employeeDropdownList = response
          .filter((employee: IEmployee) => employee?.branch?.id === branchId)
          .map((filteredEmployee: IEmployee) => {
            return { id: filteredEmployee?.id, name: `${filteredEmployee?.first_name} ${filteredEmployee?.last_name}` };
          });
      })
    );
  }

  onSearch(): void {
    const filters = {
      branch_id: this.branchId,
      designation_ids: this.selectedDesignations.map(designation => designation.id),
      skill_ids: this.selectedSkills.map(skill => skill.id),
      employee_ids: this.selectedEmployees.map(employee => employee.id),
    };
    this.filters.emit(filters);
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
