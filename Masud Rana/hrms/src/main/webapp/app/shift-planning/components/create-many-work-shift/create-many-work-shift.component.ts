import { switchMap } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { of, Subscription } from 'rxjs';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

import { TranslateService } from '@ngx-translate/core';

import { EModalReasonType } from 'app/shared/enum/modal-reason-type.enum';

import { IEmployee } from 'app/employee/models/employee.model';
import { ICreateManyWorkShift } from './../../models/work-shift.model';
import { IDropdownEmployee } from './../../../employee/models/employee.model';

import { UtilService } from 'app/shared/service/util.service';
import { ToastService } from 'app/shared/components/toast/toast.service';
import { WorkShiftService } from 'app/shift-planning/service/work-shift.service';

@Component({
  selector: 'hrms-create-many-work-shift',
  templateUrl: './create-many-work-shift.component.html',
})
export class CreateManyWorkShiftComponent implements OnInit, OnDestroy {
  combinedStartDate = '';
  combinedEndDate = '';
  minDate: any;
  markDisabled: any;
  employeesDropdownList: IDropdownEmployee[] = [];
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    selectAllText: 'Select All',
    unSelectAllText: 'Unselect All',
    itemsShowLimit: 3,
    allowSearchFilter: true,
  };

  isLoading = false;
  message!: string;

  createWorkShiftForm = this.fb.group({
    title: new FormControl(''),
    employees: new FormControl([]),
    start_date: new FormControl('', [Validators.required]),
    end_date: new FormControl('', [Validators.required]),
    start_time: new FormControl('', [Validators.required]),
    end_time: new FormControl('', [Validators.required]),
    timeDifference: new FormControl(''),
  });

  startDateControl = this.createWorkShiftForm.get('start_date') as AbstractControl;
  endDateControl = this.createWorkShiftForm.get('end_date') as AbstractControl;
  startTimeControl = this.createWorkShiftForm.get('start_time') as AbstractControl;
  endTimeControl = this.createWorkShiftForm.get('end_time') as AbstractControl;

  private subscriptions: Subscription = new Subscription();

  constructor(
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private utilService: UtilService,
    private workShiftService: WorkShiftService,
    private toastService: ToastService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.getAllEmployees();

    this.setTimeDifference(this.startDateControl.value, this.startTimeControl.value, this.endDateControl.value, this.endTimeControl.value);

    this.startTimeControl?.valueChanges.subscribe(newStartTime => {
      this.setTimeDifference(this.startDateControl.value, newStartTime, this.endDateControl.value, this.endTimeControl.value);
    });

    this.endTimeControl?.valueChanges.subscribe(endtime => {
      this.setTimeDifference(this.startDateControl.value, this.startTimeControl.value, this.endDateControl.value, endtime);
    });
  }

  public setTimeDifference(startDate: string, startTime: string, endDate: string, endTime: string): void {
    this.combinedStartDate = this.utilService.combineDateAndTime(this.utilService.format(startDate), startTime);
    this.combinedEndDate = this.utilService.combineDateAndTime(this.utilService.format(endDate), endTime);
    const timeDifference = startTime && endTime && this.utilService.getTimeDifference(startTime, endTime);
    this.createWorkShiftForm.get('timeDifference')?.patchValue(timeDifference);
  }

  public getAllEmployees(): void {
    this.subscriptions.add(
      this.utilService
        .getEmployees()
        .pipe(
          switchMap((employees: IEmployee[]) => {
            const employeesForDropDown = employees.map(employee => ({
              id: employee.id,
              name: `${employee.first_name} ${employee.last_name}`,
            }));
            return of(employeesForDropDown);
          })
        )
        .subscribe(response => {
          this.employeesDropdownList = response;
        })
    );
  }

  public cancel(): void {
    this.activeModal.dismiss();
  }

  public create(): void {
    this.workShiftService.createMany(this.buildWorkshiftPayload()).subscribe(
      () => {
        this.toastService.showSuccessToast(this.translateService.instant('hrmsApp.workShift.toastMessages.queue.success'));
        this.activeModal.close(EModalReasonType.CREATED);
      },
      () => {
        this.toastService.showSuccessToast(this.translateService.instant('hrmsApp.workShift.toastMessages.queue.error'));
      }
    );
  }

  buildWorkshiftPayload(): ICreateManyWorkShift {
    const { start_date, end_date, start_time, end_time, title, employees } = this.createWorkShiftForm.value;

    const startDate = this.utilService.getFormattedDateToCreateWorkShift(new Date(start_date), start_time);
    const endDate = this.utilService.getFormattedDateToCreateWorkShift(new Date(end_date), end_time);

    const startTimeInNumber = this.utilService.convertHoursToSeconds(start_time) ? this.utilService.convertHoursToSeconds(start_time) : 1;
    const endTimeInNumber = this.utilService.convertHoursToSeconds(end_time) ? this.utilService.convertHoursToSeconds(end_time) : 1;

    const workShift: ICreateManyWorkShift = {
      employees,
      title,
      start_date: new Date(startDate).getTime(),
      end_date: new Date(endDate).getTime(),
      start_time: startTimeInNumber,
      end_time: endTimeInNumber,
    };

    return workShift;
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
