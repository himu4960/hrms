import { Route, Routes } from '@angular/router';

import { RegisterComponent } from './register.component';
import { RegisterCompanyComponent } from './register-company/register-company.component';
import { RegisterEmployeeComponent } from './register-employee/register-employee.component';

const REGISTER_ROUTE: Routes = [
  {
    path: '',
    component: RegisterComponent,
    data: {
      pageTitle: 'register.title',
    },
  },
  {
    path: 'employee/:token',
    component: RegisterEmployeeComponent,
    data: {
      pageTitle: 'register.title',
    },
  },
  {
    path: 'finish',
    component: RegisterCompanyComponent,
    data: {
      pageTitle: 'register.title',
    },
  },
  {
    path: 'confirm',
    loadChildren: () =>
      import('../account/registration-confirmation/registration-confirmation.module').then(m => m.RegistrationConfirmationModule),
  },
  {
    path: 'employee/confirm/:token',
    loadChildren: () => import('../employee-confirmation/employee-confirmation.module').then(m => m.EmployeeConfirmationModule),
  },
];

export const registerState: Routes = [
  {
    path: '',
    children: REGISTER_ROUTE,
  },
];
