import { IUser } from './../entities/user/user.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApplicationConfigService } from 'app/core/config/application-config.service';

import { IRegisterEmployee, IRegisterUser, Registration } from './register.model';
import { IEmployee } from 'app/employee/models/employee.model';

export type EntityResponseType = HttpResponse<IUser>;
export type EmployeeResponseType = HttpResponse<IEmployee>;

@Injectable({ providedIn: 'root' })
export class RegisterService {
  public resourceUrl = this.applicationConfigService.getEndpointFor(`register`);
  public employeeResourceUrl = this.applicationConfigService.getEndpointFor(`employees`);
  constructor(private http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  save(registration: Registration): Observable<{}> {
    return this.http.post(`${this.resourceUrl}/company`, registration);
  }

  registerUser(registration: IRegisterUser): Observable<EntityResponseType> {
    return this.http.post<IUser>(this.resourceUrl, registration, { observe: 'response' });
  }

  verifyToken(token: string): Observable<EmployeeResponseType> {
    return this.http.get(`${this.employeeResourceUrl}/register/invite/check?token=${token}`, { observe: 'response' });
  }

  registerEmployee(registration: IRegisterEmployee): Observable<EntityResponseType> {
    return this.http.post<IUser>(`${this.employeeResourceUrl}/invite/register`, registration, { observe: 'response' });
  }
}
