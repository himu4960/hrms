import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { IVerifiedEmployee } from '../register.model';

import { RegisterService } from '../register.service';

@Component({
  selector: 'hrms-register-employee',
  templateUrl: './register-employee.component.html',
  styleUrls: ['../register.component.scss'],
})
export class RegisterEmployeeComponent implements OnInit {
  doNotMatch = false;
  error = false;
  success = false;
  successToken = false;
  loading = true;
  isSaving = false;
  token: string | null = null;
  registerForm = this.fb.group({
    username: ['', [Validators.required]],
    firstName: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    email: [{ value: '', disabled: true }, [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    phone: [
      '',
      [
        Validators.required,
        Validators.minLength(11),
        Validators.maxLength(13),
        Validators.pattern('^[+]?([0-9][s]?|[0-9]?)([(][0-9]{3}[)][s]?|[0-9]{3}[-s.]?)[0-9]{3}[-s.]?[0-9]{4,6}$'),
      ],
    ],
    password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
    confirmPassword: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
    company_id: [null, [Validators.required]],
    branch_id: [null, [Validators.required]],
    employee_id: [null, [Validators.required]],
  });

  constructor(private fb: FormBuilder, private registerService: RegisterService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const token = params['token'];
      if (token) {
        this.token = token;
        this.verifyToken(token);
      } else {
        this.router.navigateByUrl('404');
      }
    });
  }

  register(): void {
    if (this.registerForm.get(['password'])!.value !== this.registerForm.get(['confirmPassword'])!.value) {
      this.doNotMatch = true;
    } else {
      this.isSaving = true;
      const username = this.registerForm.get(['username'])!.value;
      const firstName = this.registerForm.get(['firstName'])!.value;
      const lastName = this.registerForm.get(['lastName'])!.value;
      const email = this.registerForm.get(['email'])!.value;
      const phone = this.registerForm.get(['phone'])!.value;
      const passwordForm = this.registerForm.get(['password'])!.value;
      const company_id = this.registerForm.get(['company_id'])!.value;
      const branch_id = this.registerForm.get(['branch_id'])!.value;
      const employee_id = this.registerForm.get(['employee_id'])!.value;

      this.registerService
        .registerEmployee({ username, firstName, lastName, email, phone, password: passwordForm, company_id, branch_id, employee_id })
        .subscribe(
          () => {
            this.success = true;
            this.isSaving = false;
            this.error = false;
          },
          () => {
            this.success = false;
            this.isSaving = false;
            this.error = true;
          }
        );
    }
  }

  verifyToken(token: string): void {
    this.registerService.verifyToken(token).subscribe(
      (res: HttpResponse<IVerifiedEmployee>) => {
        this.successToken = true;
        this.loading = false;
        if (res.body) {
          const employee = res.body;
          this.registerForm.patchValue({
            firstName: employee.first_name,
            lastName: employee.last_name,
            email: employee.email,
            branch_id: employee.branch_id,
            company_id: employee.company_id,
            employee_id: employee.id,
          });
        }
      },
      () => {
        this.loading = false;
        this.router.navigateByUrl('404');
      }
    );
  }
}
