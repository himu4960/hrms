jest.mock('@ngx-translate/core');

import { ComponentFixture, TestBed, waitForAsync, inject, tick, fakeAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of, throwError } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageService, NgxWebstorageModule } from 'ngx-webstorage';

import { RegisterService } from './register.service';
import { RegisterComponent } from './register.component';

describe('Component Tests', () => {
  describe('RegisterComponent', () => {
    let fixture: ComponentFixture<RegisterComponent>;
    let comp: RegisterComponent;
    let localStorageService: LocalStorageService;

    beforeEach(
      waitForAsync(() => {
        TestBed.configureTestingModule({
          imports: [HttpClientTestingModule, RouterTestingModule, NgxWebstorageModule.forRoot()],
          declarations: [RegisterComponent],
          providers: [FormBuilder, TranslateService],
        })
          .overrideTemplate(RegisterComponent, '')
          .compileComponents();
      })
    );

    beforeEach(() => {
      fixture = TestBed.createComponent(RegisterComponent);
      comp = fixture.componentInstance;
      localStorageService = TestBed.inject(LocalStorageService);
      TestBed.inject(TranslateService);
    });

    it('should success to true after creating an account', inject(
      [RegisterService, TranslateService],
      fakeAsync((service: RegisterService, mockLanguageService: TranslateService) => {
        spyOn(service, 'registerUser').and.returnValue(of({}));
        mockLanguageService.currentLang = 'en';
        comp.registerUserForm.patchValue({
          fullName: 'Testing Testing',
          email: 'test@test.com',
          langKey: 'en',
        });

        comp.register();
        tick();

        expect(service.registerUser).toHaveBeenCalledWith({
          fullName: 'Testing Testing',
          email: 'test@test.com',
          langKey: 'en',
        });
        expect(comp.success).toBe(true);
        expect(comp.errorEmailExists).toBe(false);
        expect(comp.error).toBe(false);
      })
    ));

    it('should notify of email existence upon 400/email address already in use', inject(
      [RegisterService],
      fakeAsync((service: RegisterService) => {
        spyOn(service, 'registerUser').and.returnValue(
          throwError({
            status: 302,
          })
        );
        comp.registerUserForm.patchValue({
          fullName: 'Testing Testing',
          email: 'test@test.com',
        });

        comp.register();
        tick();

        expect(comp.errorEmailExists).toBe(true);
        expect(comp.error).toBe(false);
      })
    ));
  });
});
