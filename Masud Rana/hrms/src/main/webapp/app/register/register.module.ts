import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from './../shared/shared.module';
import { PasswordManagementModule } from './../employee/module/password-management.module';

import { RegisterComponent } from './register.component';
import { RegisterCompanyComponent } from './register-company/register-company.component';
import { RegisterEmployeeComponent } from './register-employee/register-employee.component';

import { registerState } from './register.route';

@NgModule({
  imports: [SharedModule, PasswordManagementModule, RouterModule.forChild(registerState)],
  declarations: [RegisterComponent, RegisterCompanyComponent, RegisterEmployeeComponent],
})
export class RegisterModule {}
