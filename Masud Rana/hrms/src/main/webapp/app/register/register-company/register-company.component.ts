import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { of, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { IAppSetupData } from './../../shared/models/utils.model';
import { CompanyType } from './../../entities/company-type/company-type.model';
import { ApplicationType } from './../../entities/application-type/application-type.model';

import { RegisterService } from '../register.service';
import { UtilService } from './../../shared/service/util.service';
import { StateStorageService } from './../../core/auth/state-storage.service';

@Component({
  selector: 'hrms-register-company',
  templateUrl: './register-company.component.html',
  styleUrls: ['../register.component.scss'],
})
export class RegisterCompanyComponent implements OnInit, OnDestroy {
  timer = 5;
  loading = true;
  isSaving = false;

  applicationTypes: ApplicationType[] = [];
  companyTypes: CompanyType[] = [];

  doNotMatch = false;
  error = false;
  errorCompanyExists = false;
  errorUserExists = false;
  success = false;

  registerForm = this.fb.group({
    name: ['', [Validators.required]],
    phone: [
      '',
      [
        Validators.required,
        Validators.minLength(11),
        Validators.maxLength(13),
        Validators.pattern('^[+]?([0-9][s]?|[0-9]?)([(][0-9]{3}[)][s]?|[0-9]{3}[-s.]?)[0-9]{3}[-s.]?[0-9]{4,6}$'),
      ],
    ],
    password: ['', [Validators.required, Validators.maxLength(50)]],
    confirmPassword: ['', [Validators.required, Validators.maxLength(50)]],
    application_type_id: ['', [Validators.required]],
    company_type_id: ['', [Validators.required]],
  });

  private subscriptions: Subscription = new Subscription();

  constructor(
    private registerService: RegisterService,
    private fb: FormBuilder,
    private utilService: UtilService,
    private stateStorageService: StateStorageService,
    private router: Router,
    private location: Location
  ) {}

  previousState(): void {
    this.location.back();
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.utilService
        .getAppSetupData()
        .pipe(
          switchMap((appSetupData: IAppSetupData) => {
            this.applicationTypes = appSetupData.application_types;
            this.companyTypes = appSetupData.company_types;
            return of(appSetupData);
          }),
          switchMap(() => {
            const userFormData = this.stateStorageService.getRegisteredUserFormData();
            this.registerForm.patchValue({
              ...userFormData,
            });
            this.registerForm.valueChanges.subscribe(() => {
              this.stateStorageService.setRegisteredUserFormData(this.registerForm.value);
            });
            return of(null);
          })
        )
        .subscribe()
    );

    if (!this.stateStorageService.getRegisteredUser()) {
      this.router.navigateByUrl('/register');
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  register(): void {
    this.isSaving = true;
    this.doNotMatch = false;
    this.error = false;
    this.errorCompanyExists = false;
    this.errorUserExists = false;

    const password = this.registerForm.get(['password'])!.value;

    if (password !== this.registerForm.get(['confirmPassword'])!.value) {
      this.doNotMatch = true;
      this.isSaving = false;
    } else {
      const name = this.registerForm.get(['name'])!.value;
      const phone = this.registerForm.get(['phone'])!.value;
      const application_type_id = Number(this.registerForm.get(['application_type_id'])!.value);
      const company_type_id = Number(this.registerForm.get(['company_type_id'])!.value);
      const user_id = this.stateStorageService.getRegisteredUser();

      this.registerService.save({ user_id, name, phone, password, application_type_id, company_type_id }).subscribe(
        () => {
          this.isSaving = false;
          this.success = true;
          this.stateStorageService.removeRegisteredUser();
          this.stateStorageService.removeRegisteredUserFormData();
        },
        response => this.processError(response)
      );
    }
  }

  private processError(response: HttpErrorResponse): void {
    this.isSaving = false;
    if (response.status === 302) {
      this.errorCompanyExists = true;
    } else {
      this.error = true;
    }
  }
}
