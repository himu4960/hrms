import { Component, AfterViewInit, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { IUser } from './../admin/user-management/user-management.model';

import { RegisterService } from './register.service';
import { StateStorageService } from './../core/auth/state-storage.service';

@Component({
  selector: 'hrms-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements AfterViewInit {
  @ViewChild('login', { static: false })
  login?: ElementRef;
  loading = false;

  error = false;
  errorEmailExists = false;
  success = false;

  registerUserForm = this.fb.group({
    fullName: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
  });

  constructor(
    private translateService: TranslateService,
    private registerService: RegisterService,
    private fb: FormBuilder,
    private router: Router,
    private stateStorageService: StateStorageService
  ) {}

  ngAfterViewInit(): void {
    if (this.login) {
      this.login.nativeElement.focus();
    }
  }

  register(): void {
    this.loading = true;
    this.errorEmailExists = false;

    const fullName = this.registerUserForm.get(['fullName'])!.value;
    const email = this.registerUserForm.get(['email'])!.value;
    this.registerService.registerUser({ fullName, email, langKey: this.translateService.currentLang }).subscribe(
      (response: HttpResponse<IUser>) => {
        this.loading = false;
        this.success = true;
        if (response.body?.id !== undefined) {
          this.stateStorageService.setRegisteredUser(response.body.id);
          this.router.navigateByUrl('/register/finish');
        }
      },
      err => this.processError(err)
    );
  }

  private processError(response: HttpErrorResponse): void {
    this.loading = false;
    if (response.status === 302) {
      this.errorEmailExists = true;
    } else {
      this.error = true;
    }
  }
}
