export interface IRegisterUser {
  fullName: string;
  email: string;
  langKey: string;
}

export interface IRegisterCompany {
  user_id?: number;
  name: string;
  phone: string;
  password: string;
  company_type_id: number;
  application_type_id: number;
}

export class Registration {
  constructor(
    public user_id: number,
    public name: string,
    public phone: string,
    public password: string,
    public company_type_id: number,
    public application_type_id: number
  ) {}
}

export interface IVerifiedEmployee {
  id?: number;
  first_name?: string | null;
  middle_name?: string | null;
  last_name?: string | null;
  email?: string | null;
  phone?: string | null;
  invitation_accepted?: boolean | null;
  invitation_token?: string | null;
  invitation_expiry?: string | null;
  branch_id?: number | null;
  company_id?: number | null;
}

export interface IRegisterEmployee {
  username?: string;
  phone?: string;
  password?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  company_id?: number;
  branch_id?: number;
  employee_id?: number;
}

export class RegistrationEmployee {
  constructor(
    public username: string,
    public phone?: string,
    public password?: string,
    public firstName?: string,
    public lastName?: string,
    public email?: string,
    public company_id?: number,
    public branch_id?: number,
    public employee_id?: number
  ) {}
}
