import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { of, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { Store } from '@ngrx/store';

import { AppState } from 'app/store';

import { updateShiftPlanningSettingsAction } from 'app/store/company-settings/company-settings.actions';
import { selectShiftPlanningSettings } from 'app/store/company-settings/company-settings.selectors';
import { ShiftPlanningSettings } from 'app/store/states/shift-planning-settings.interface';

import {
  allowedUsers,
  dropTimeLimits,
  scheduleWithinTimes,
  requiredSkills,
  schedules,
  shifts,
  startDays,
  workingDaysInARow,
} from '../resources/shift-planning-settings';

import { PermissionService } from 'app/shared/service/permission.service';

@Component({
  selector: 'hrms-company-settings-shift-planning',
  templateUrl: './company-settings-shift-planning.component.html',
  styleUrls: ['./company-settings-shift-planning.component.scss'],
})
export class CompanySettingsShiftPlanningComponent implements OnInit, OnDestroy {
  shiftPlanningSettings!: ShiftPlanningSettings;
  isEmployee = true;
  isAllowEmployeeToAddWorkShift = false;

  startDays = startDays;
  allowedUsers = allowedUsers;
  requiredSkills = requiredSkills;
  dropTimeLimits = dropTimeLimits;
  workingDaysInARow = workingDaysInARow;
  schedules = schedules;
  shifts = shifts;
  eachMinutes = scheduleWithinTimes;

  shiftPlanningSettingsForm = this.fb.group({
    employee_can_add_work_units: new FormControl(''),
    employee_can_be_scheduled_to_immediate_shifts: new FormControl(''),
    employee_can_drop_shifts: new FormControl(''),
    employee_can_release_shifts: new FormControl(''),
    employee_can_see_assigned_staffs: new FormControl(''),
    employee_can_see_co_worker_shifts: new FormControl(''),
    employee_can_see_co_worker_vacations: new FormControl(''),
    employee_can_see_own_work_report: new FormControl(''),
    employee_can_trade_shifts: new FormControl(''),
    employee_can_view_all_schedules: new FormControl(''),
    employee_can_view_shifts_in_advance: new FormControl(''),
    employee_cannot_be_scheduled_within_time: new FormControl(''),
    employee_pick_up_shifts_with_overtime_enabled: new FormControl(''),
    employee_requested_open_shifts_auto_approved: new FormControl(''),
    longer_than_24hours_shifts_allowed: new FormControl(''),
    past_date_shifts_allowed: new FormControl(''),
    max_working_days_in_a_row: new FormControl(''),
    notes_permissions_allowed_users: new FormControl(''),
    on_call_management_enabled: new FormControl(''),
    required_skills_on_shift: new FormControl(''),
    same_day_trades_allowed: new FormControl(''),
    schedule_automatic_conflicts_allowed: new FormControl(''),
    shift_acknowledgment_enabled: new FormControl(''),
    shift_loading_indicator_enabled: new FormControl(''),
    shift_notes_visibility_allowed_users: new FormControl(''),
    shift_overlapping_disallowed: new FormControl(''),
    shift_planning_start_day: new FormControl(''),
    shift_planning_tasks_enabled: new FormControl(''),
    shift_trade_release_drop_time_limit: new FormControl(''),
    shift_trades_between_positions_disabled: new FormControl(''),
    shift_trades_manager_must_release_after_request: new FormControl(''),
    shift_trades_manager_must_release_before_request: new FormControl(''),
    show_12am_as_midnight: new FormControl(''),
    show_costing_data_in_scheduler: new FormControl(''),
    show_employee_skills_in_scheduler: new FormControl(''),
    show_locations_in_shifts: new FormControl(''),
    show_overnight_shifts: new FormControl(''),
    show_pending_leave_request: new FormControl(''),
    sorting_by_address_enabled: new FormControl(''),
    use_draft_publish_schedule_method: new FormControl(''),
    use_shift_approval: new FormControl(''),
    work_unit_module_enabled: new FormControl(''),
    work_units_submitted_by_employee_auto_approved: new FormControl(''),
    work_units_submitted_by_scheduler_auto_approved: new FormControl(''),
  });

  private subscriptions: Subscription = new Subscription();

  constructor(protected fb: FormBuilder, private store: Store<AppState>, private permissionService: PermissionService) {}

  ngOnInit(): void {
    this.subscriptions.add(
      this.store
        .select(selectShiftPlanningSettings)
        .pipe(
          switchMap((shiftPlanningSettings: ShiftPlanningSettings) => {
            if (shiftPlanningSettings !== null) {
              this.shiftPlanningSettings = shiftPlanningSettings;
              this.isAllowEmployeeToAddWorkShift = shiftPlanningSettings.employee_can_add_work_units;
              this.shiftPlanningSettingsForm.patchValue(this.shiftPlanningSettings);
            }
            return this.shiftPlanningSettingsForm.get('employee_can_add_work_units')!.valueChanges;
          }),
          switchMap(value => {
            this.isAllowEmployeeToAddWorkShift = value;
            if (!value) {
              this.shiftPlanningSettingsForm.get('employee_can_drop_shifts')?.patchValue(false);
              this.shiftPlanningSettingsForm.get('employee_can_release_shifts')?.patchValue(false);
            }
            return of(null);
          })
        )
        .subscribe(() => {
          this.isEmployee = this.permissionService.isEmployee();
        })
    );
  }

  onSave(): void {
    this.store.dispatch(updateShiftPlanningSettingsAction({ data: this.shiftPlanningSettingsForm.value }));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
