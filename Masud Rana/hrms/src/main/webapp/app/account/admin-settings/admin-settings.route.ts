import { Route } from '@angular/router';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';
import { RoutePermissionGuard } from 'app/core/auth/guards/route-permission.service';

export const newSettingsRoute: Route = {
  path: '',
  data: {
    pageTitle: 'global.menu.account.adminSettings',
  },
  loadChildren: () => import('./admin-settings.module').then(m => m.AdminSettingsModule),
  canActivate: [AuthGuard, RoutePermissionGuard],
};
