export interface IStartDays {
  name: string;
  value: number;
  translationName: string;
}

export const startDays: IStartDays[] = [
  {
    name: 'Sunday',
    value: 0,
    translationName: 'settings.selectBox.weekName.sunday',
  },
  {
    name: 'Monday',
    value: 1,
    translationName: 'settings.selectBox.weekName.monday',
  },
  {
    name: 'Tuesday',
    value: 2,
    translationName: 'settings.selectBox.weekName.tuesday',
  },
  {
    name: 'Wednesday',
    value: 3,
    translationName: 'settings.selectBox.weekName.wednesday',
  },
  {
    name: 'Thursday',
    value: 4,
    translationName: 'settings.selectBox.weekName.thursday',
  },
  {
    name: 'Friday',
    value: 5,
    translationName: 'settings.selectBox.weekName.friday',
  },
  {
    name: 'Saturday',
    value: 6,
    translationName: 'settings.selectBox.weekName.saturday',
  },
];

export const allowedUsers = [
  {
    name: 'Managers And Supervisors',
    value: 1,
    translationName: 'shiftPlanningSettings.selectBox.allowedUsers.managerAndSupervisor',
  },
  {
    name: 'Managers, Supervisors And Scheduler',
    value: 2,
    translationName: 'shiftPlanningSettings.selectBox.allowedUsers.managerAndSupervisorAndScheduler',
  },
  {
    name: 'All Users',
    value: 3,
    translationName: 'shiftPlanningSettings.selectBox.allowedUsers.allUser',
  },
];

export const requiredSkills = [
  {
    name: 'Off',
    value: 0,
    translationName: 'settings.selectBox.requiredSkill.off',
  },
  {
    name: 'Any',
    value: 1,
    translationName: 'settings.selectBox.requiredSkill.any',
  },
  {
    name: 'All',
    value: 2,
    translationName: 'settings.selectBox.requiredSkill.all',
  },
];

export const dropTimeLimits = [
  {
    name: 'Off',
    value: 0,
  },
  {
    name: '12 Hours',
    value: 12,
  },
  {
    name: '24 Hours',
    value: 24,
  },
  {
    name: '48 Hours',
    value: 48,
  },
  {
    name: '7 Days',
    value: 7,
  },
  {
    name: '14 Days',
    value: 14,
  },
];

export const scheduleWithinTimes = [
  {
    name: 'None',
    value: 0,
    seconds: 0,
  },
  {
    name: '15 Minutes',
    value: 1,
    seconds: 900,
  },
  {
    name: '30 Minutes',
    value: 2,
    seconds: 1800,
  },
  {
    name: '45 Minutes',
    value: 3,
    seconds: 2700,
  },
  {
    name: '1 Hour',
    value: 4,
    seconds: 3600,
  },
  {
    name: '1 Hour 15 Minutes',
    value: 5,
    seconds: 4500,
  },
  {
    name: '1 Hour 30 Minutes',
    value: 6,
    seconds: 5400,
  },
  {
    name: '1 Hour 45 Minutes',
    value: 7,
    seconds: 6300,
  },
  {
    name: '2 Hours',
    value: 8,
    seconds: 7200,
  },
  {
    name: '3 Hours',
    value: 9,
    seconds: 10800,
  },
  {
    name: '4 Hours',
    value: 10,
    seconds: 14400,
  },
  {
    name: '5 Hours',
    value: 11,
    seconds: 18000,
  },
  {
    name: '6 Hours',
    value: 12,
    seconds: 21600,
  },
  {
    name: '7 Hours',
    value: 13,
    seconds: 25200,
  },
  {
    name: '8 Hours',
    value: 14,
    seconds: 28800,
  },
  {
    name: '9 Hours',
    value: 15,
    seconds: 32400,
  },
  {
    name: '10 Hours',
    value: 16,
    seconds: 36000,
  },
  {
    name: '11 Hours',
    value: 17,
    seconds: 39600,
  },
  {
    name: '12 Hours',
    value: 18,
    seconds: 43200,
  },
];

export const shifts = [
  {
    name: 'All Shift',
    value: 1,
    translationName: 'settings.selectBox.shifts.allShift',
  },
  {
    name: '1 Week',
    value: 7,
    translationName: 'settings.selectBox.shifts.week1',
  },
  {
    name: '2 Weeks',
    value: 14,
    translationName: 'settings.selectBox.shifts.week2',
  },
  {
    name: '3 Weeks',
    value: 21,
    translationName: 'settings.selectBox.shifts.week3',
  },
  {
    name: '1 Month',
    value: 30,
    translationName: 'settings.selectBox.shifts.month1',
  },
  {
    name: '2 Months',
    value: 60,
    translationName: 'settings.selectBox.shifts.month2',
  },
  {
    name: '3 Months',
    value: 90,
    translationName: 'settings.selectBox.shifts.month3',
  },
  {
    name: '4 Months',
    value: 120,
    translationName: 'settings.selectBox.shifts.month4',
  },
  {
    name: '5 Months',
    value: 150,
    translationName: 'settings.selectBox.shifts.month5',
  },
];

export const schedules = [
  {
    name: 'None',
    value: 0,
  },
  {
    name: 'Hours Only',
    value: 1,
  },
  {
    name: 'Hours And Staff Count',
    value: 2,
  },
  {
    name: 'Hours And Staff Cost',
    value: 3,
  },
];

export const workingDaysInARow = [
  {
    name: 'Unlimited',
    value: 0,
  },
  {
    name: '1 Day',
    value: 1,
  },
  {
    name: '2 Days',
    value: 2,
  },
  {
    name: '3 Days',
    value: 3,
  },
  {
    name: '4 Days',
    value: 4,
  },
  {
    name: '5 Days',
    value: 5,
  },
  {
    name: '6 Days',
    value: 6,
  },
  {
    name: '7 Days',
    value: 7,
  },
  {
    name: '8 Days',
    value: 8,
  },
  {
    name: '9 Days',
    value: 9,
  },
  {
    name: '10 Days',
    value: 10,
  },
  {
    name: '11 Days',
    value: 11,
  },
  {
    name: '12 Days',
    value: 12,
  },
  {
    name: '13 Days',
    value: 13,
  },
  {
    name: '14 Days',
    value: 14,
  },
  {
    name: '15 Days',
    value: 15,
  },
  {
    name: '16 Days',
    value: 16,
  },
  {
    name: '17 Days',
    value: 17,
  },
  {
    name: '18 Days',
    value: 18,
  },
  {
    name: '19 Days',
    value: 19,
  },
  {
    name: '20 Days',
    value: 20,
  },
  {
    name: '21 Days',
    value: 21,
  },
  {
    name: '22 Days',
    value: 22,
  },
  {
    name: '23 Days',
    value: 23,
  },
  {
    name: '24 Days',
    value: 24,
  },
  {
    name: '25 Days',
    value: 25,
  },
  {
    name: '26 Days',
    value: 26,
  },
  {
    name: '27 Days',
    value: 27,
  },
  {
    name: '28 Days',
    value: 28,
  },
  {
    name: '29 Days',
    value: 29,
  },
  {
    name: '30 Days',
    value: 30,
  },
];
