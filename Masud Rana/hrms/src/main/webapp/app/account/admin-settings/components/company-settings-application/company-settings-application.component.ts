import { HttpResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';

import { Store } from '@ngrx/store';

import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { LANGUAGES } from 'app/config/language.constants';
import { timeCountableOptions } from '../resources/time-clock-settings';
import { ITimeZone } from 'app/entities/time-zone/time-zone.model';
import { Country } from 'app/core/auth/country-timezone.model';

import { AppState } from 'app/store';
import { updateApplicationSettingsAction } from 'app/store/company-settings/company-settings.actions';
import { ApplicationSettings } from 'app/store/states/application-settings.interface';
import { selectApplicationSettings } from 'app/store/company-settings/company-settings.selectors';

import { CountryTimezoneService } from 'app/core/auth/country-timezone.service';
import { TimeZoneService } from 'app/entities/time-zone/service/time-zone.service';

@Component({
  selector: 'hrms-company-settings-application',
  templateUrl: './company-settings-application.component.html',
  styleUrls: ['./company-settings-application.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CompanySettingsApplicationComponent implements OnInit, OnDestroy {
  applicationSettings!: ApplicationSettings;

  countries: Country[] = [];
  timeZones: ITimeZone[] = [];
  languages = LANGUAGES;
  localeDateFormats = ['mm/dd/yy', 'mm-dd-yy'];
  localeWeekNumbers = [0, 1, 2, 3];
  autoLogoutTimeOptions = timeCountableOptions;

  appSettingsForm = this.fb.group({
    locale_country_id: new FormControl(''),
    locale_timezone_id: new FormControl(''),
    locale_default_lang_code: new FormControl(''),
    office_working_hours: new FormControl(''),
    locale_date_format: new FormControl(''),
    locale_week_number: new FormControl(''),
    notification_email_enabled: new FormControl(''),
    notification_sms_enabled: new FormControl(''),
    notification_mobile_push_enabled: new FormControl(''),
    notification_force_email_enabled: new FormControl(''),
    notification_birthday_enabled: new FormControl(''),
    notification_birthday_card_enabled: new FormControl(''),
    training_module_enabled: new FormControl(''),
    training_download_pdf_enabled: new FormControl(''),
    employee_name_format: new FormControl(''),
    employee_can_view_report: new FormControl(''),
    employee_can_edit_profile: new FormControl(''),
    employee_can_view_staff_gallery: new FormControl(''),
    employee_can_view_staff_details: new FormControl(''),
    employee_mgnt_can_view_eid: new FormControl(''),
    employee_can_view_whos_on: new FormControl(''),
    employee_can_request_account: new FormControl(''),
    employee_security_question: new FormControl(''),
    employee_security_answer: new FormControl(''),
    employee_can_view_uploaded_files: new FormControl(''),
    comm_message_wall_enabled: new FormControl(''),
    comm_employee_can_post_msg: new FormControl(''),
    comm_employee_can_comment: new FormControl(''),
    comm_messaging_module_enabled: new FormControl(''),
    comm_employee_can_send_private_msg: new FormControl(''),
    comm_employee_can_reply_received_msg: new FormControl(''),
    comm_employee_can_ping_users: new FormControl(''),
    auth_show_logo: new FormControl(''),
    auth_auto_logout_time: new FormControl(''),
    auth_password_expiry_time: new FormControl(''),
  });

  private subscriptions: Subscription = new Subscription();

  constructor(
    protected timeZoneService: TimeZoneService,
    protected fb: FormBuilder,
    private store: Store<AppState>,
    private countryTimeZoneService: CountryTimezoneService
  ) {
    this.timeZoneService
      .query()
      .pipe(map((res: HttpResponse<ITimeZone[]>) => res.body ?? []))
      .subscribe((timeZone: ITimeZone[]) => (this.timeZones = timeZone));
    this.countryTimeZoneService.getAllCountries().subscribe((countries: Country[]) => {
      this.countries = countries;
    });
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.store.select(selectApplicationSettings).subscribe((res: ApplicationSettings) => {
        if (res !== null) {
          this.applicationSettings = res;
          this.appSettingsForm.patchValue(this.applicationSettings);
        }
      })
    );
  }

  onSave(): void {
    this.store.dispatch(updateApplicationSettingsAction({ data: this.appSettingsForm.value }));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
