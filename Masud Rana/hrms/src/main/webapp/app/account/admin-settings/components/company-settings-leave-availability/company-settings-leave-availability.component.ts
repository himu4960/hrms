import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';

import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDropdownSettings } from 'ng-multiselect-dropdown';

import { AppState } from 'app/store';
import { selectLeaveAvailabilitySettings } from 'app/store/company-settings/company-settings.selectors';
import { LeaveAvailabilitySettings } from 'app/store/states/leave-availability-settings.interface';

import { updateLeaveAvailablilitiySettingsAction } from 'app/store/company-settings/company-settings.actions';
import { IStartDays, startDays } from '../resources/shift-planning-settings';

import { LeaveTypeService } from 'app/entities/leave-type/service/leave-type.service';

import { LeaveTypeDeleteDialogComponent } from 'app/entities/leave-type/delete/leave-type-delete-dialog.component';

import { ILeaveType } from 'app/entities/leave-type/leave-type.model';
import { EModalReasonType } from 'app/shared/enum/modal-reason-type.enum';

@Component({
  selector: 'hrms-company-settings-leave-availability',
  templateUrl: './company-settings-leave-availability.component.html',
  styleUrls: ['./company-settings-leave-availability.component.scss'],
})
export class CompanySettingsLeaveAvailabilityComponent implements OnInit, OnDestroy {
  leaveAvaiabilitySettings!: LeaveAvailabilitySettings;
  selectedDays: IStartDays[] = [];
  dropdownList: IStartDays[] = startDays;
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'value',
    textField: 'name',
    selectAllText: 'Select All',
    unSelectAllText: 'Unselect All',
    itemsShowLimit: 3,
    allowSearchFilter: false,
  };

  leaveAvailabilitySettingsForm = this.fb.group({
    leave_module_enabled: new FormControl(''),
    deduct_weekends_from_leaves: new FormControl(''),
    employees_can_book_vacation: new FormControl(''),
    show_deleted_employees_in_leave_approvals: new FormControl(''),
    schedulers_should_receive_leave_notifications: new FormControl(''),
    allow_schedulers_to_approve_past_leave_requests: new FormControl(''),
    leave_must_be_booked_in_days_advance: new FormControl(),
    max_staff_can_be_booked_at_once: new FormControl(),
    employees_can_cancel_previously_approved_leave_requests: new FormControl(''),
    availability_module_enabled: new FormControl(''),
    employees_can_modify_unavailability: new FormControl(''),
    availability_must_be_approved_by_management: new FormControl(''),
    allow_schedulers_to_approve_availability: new FormControl(''),
  });

  leaveTypes?: ILeaveType[];
  isLoading = false;

  deductWeekendsFromLeaves = this.leaveAvailabilitySettingsForm.get('deduct_weekends_from_leaves') as FormControl;

  private subscriptions: Subscription = new Subscription();

  constructor(
    protected fb: FormBuilder,
    private store: Store<AppState>,
    protected leaveTypeService: LeaveTypeService,
    protected modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.loadLeaveType();
    this.subscriptions.add(
      this.store
        .select(selectLeaveAvailabilitySettings)
        .pipe(
          map((response: LeaveAvailabilitySettings) => {
            return {
              ...response,
              deduct_weekends_from_leaves: JSON.parse(response.deduct_weekends_from_leaves),
            };
          })
        )
        .subscribe((res: LeaveAvailabilitySettings) => {
          if (res !== null) {
            this.leaveAvaiabilitySettings = res;
            const daysSet = new Set(this.leaveAvaiabilitySettings.deduct_weekends_from_leaves);
            this.selectedDays = this.dropdownList.filter((item: any) => {
              if (daysSet.has(item.value)) {
                return item;
              }
            });
            this.leaveAvailabilitySettingsForm.patchValue({
              ...this.leaveAvaiabilitySettings,
              deduct_weekends_from_leaves: this.selectedDays,
            });
          }
        })
    );
  }

  loadLeaveType(): void {
    this.isLoading = true;

    this.leaveTypeService.query().subscribe(
      (res: HttpResponse<ILeaveType[]>) => {
        this.isLoading = false;
        this.leaveTypes = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  deleteLeaveType(leaveType: ILeaveType): void {
    const modalRef = this.modalService.open(LeaveTypeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.leaveType = leaveType;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.DELETED) {
        this.loadLeaveType();
      }
    });
  }

  onSave(): void {
    this.store.dispatch(updateLeaveAvailablilitiySettingsAction({ data: this.buildPayload() }));
  }

  buildPayload(): any {
    const days = this.deductWeekendsFromLeaves?.value.map((item: any) => item.value);
    return {
      ...this.leaveAvailabilitySettingsForm.value,
      deduct_weekends_from_leaves: JSON.stringify(days),
    };
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
