import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApplicationConfigService } from 'app/core/config/application-config.service';

@Injectable({ providedIn: 'root' })
export class SettingsService {
  constructor(private http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  getApplicationSettings(): Observable<{}> {
    return this.http.get(this.applicationConfigService.getEndpointFor('company-settings/application'));
  }

  getShiftPlanningSettings(): Observable<{}> {
    return this.http.get(this.applicationConfigService.getEndpointFor('company-settings/shift-planning'));
  }

  getTimeClocksSettings(): Observable<{}> {
    return this.http.get(this.applicationConfigService.getEndpointFor('company-settings/time-clocks'));
  }

  getLeaveAvailabilitySettings(): Observable<{}> {
    return this.http.get(this.applicationConfigService.getEndpointFor('company-settings/leave-availability'));
  }

  updateApplicationSettings(payload: any): Observable<{}> {
    return this.http.put(this.applicationConfigService.getEndpointFor('company-settings/application'), payload);
  }

  updateShiftPlanningSettings(payload: any): Observable<{}> {
    return this.http.put(this.applicationConfigService.getEndpointFor('company-settings/shift-planning'), payload);
  }

  updateTimeClocksSettings(payload: any): Observable<{}> {
    return this.http.put(this.applicationConfigService.getEndpointFor('company-settings/time-clocks'), payload);
  }

  updateLeaveAvailabilitySettings(payload: any): Observable<{}> {
    return this.http.put(this.applicationConfigService.getEndpointFor('company-settings/leave-availability'), payload);
  }
}
