import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { SharedModule } from './../../shared/shared.module';
import { HolidayModule } from 'app/entities/holiday/holiday.module';

import { AdminSettingsComponent } from './admin-settings.component';
import { CompanySettingsApplicationComponent } from './components/company-settings-application/company-settings-application.component';
import { CompanySettingsShiftPlanningComponent } from './components/company-settings-shift-planning/company-settings-shift-planning.component';
import { CompanySettingsTimeClockComponent } from './components/company-settings-time-clock/company-settings-time-clock.component';
import { CompanySettingsLeaveAvailabilityComponent } from './components/company-settings-leave-availability/company-settings-leave-availability.component';
import { SettingsSidebarComponent } from './components/settings-sidebar/settings-sidebar.component';
import { HolidayImportComponent } from 'app/entities/holiday/import/holiday-import.component';

@NgModule({
  imports: [
    SharedModule,
    HolidayModule,
    NgMultiSelectDropDownModule.forRoot(),
    RouterModule.forChild([
      {
        path: 'admin-settings',
        redirectTo: '/account/admin-settings/application',
        pathMatch: 'full',
      },
      {
        path: 'admin-settings',
        component: AdminSettingsComponent,
        children: [
          {
            path: 'application',
            component: CompanySettingsApplicationComponent,
          },
          {
            path: 'shift-planning',
            component: CompanySettingsShiftPlanningComponent,
          },
          {
            path: 'time-clock',
            component: CompanySettingsTimeClockComponent,
          },
          {
            path: 'leave-availability',
            component: CompanySettingsLeaveAvailabilityComponent,
          },
          {
            path: 'holiday-import',
            component: HolidayImportComponent,
          },
        ],
      },
    ]),
  ],
  declarations: [
    HolidayImportComponent,
    AdminSettingsComponent,
    SettingsSidebarComponent,
    CompanySettingsTimeClockComponent,
    CompanySettingsApplicationComponent,
    CompanySettingsShiftPlanningComponent,
    CompanySettingsLeaveAvailabilityComponent,
  ],
  exports: [
    HolidayImportComponent,
    AdminSettingsComponent,
    SettingsSidebarComponent,
    CompanySettingsTimeClockComponent,
    CompanySettingsApplicationComponent,
    CompanySettingsShiftPlanningComponent,
    CompanySettingsLeaveAvailabilityComponent,
  ],
})
export class AdminSettingsModule {}
