import { ESettingsTimeClockCount } from 'app/store/states/time-clocks-settings.interface';

export const resolution = {
  name: 'SMALL',
  value: '320X240',
};

export const roundings = [
  {
    name: 'DONT_ROUND',
    value: -1,
  },
  {
    name: 'MIN_5',
    value: 5,
  },
  {
    name: 'MIN_10',
    value: 10,
  },
  {
    name: 'MIN_15',
    value: 15,
  },
  {
    name: 'MIN_30',
    value: 30,
  },
  {
    name: 'MIN_60',
    value: 60,
  },
];

export const shiftBuffers = [
  {
    name: 'ANYTIME',
    value: -1,
  },
  {
    name: 'NONE',
    value: 0,
  },
  {
    name: 'MIN_5',
    value: 5,
  },
  {
    name: 'MIN_10',
    value: 10,
  },
  {
    name: 'MIN_15',
    value: 15,
  },
  {
    name: 'MIN_30',
    value: 30,
  },
  {
    name: 'MIN_60',
    value: 60,
  },
  {
    name: 'MIN_90',
    value: 90,
  },
  {
    name: 'HOUR_2',
    value: 120,
  },
  {
    name: 'HOUR_3',
    value: 180,
  },
  {
    name: 'HOUR_6',
    value: 360,
  },
];

export const pastDays = [
  {
    name: 'Anytime',
    value: 0,
    translationName: 'timeClockSettings.selectBox.days.anytime',
  },
  {
    name: '1 day',
    value: -1,
    translationName: 'timeClockSettings.selectBox.days.DAY_1',
  },
  {
    name: '3 days',
    value: -3,
    translationName: 'timeClockSettings.selectBox.days.DAY_3',
  },
  {
    name: '5 days',
    value: -5,
    translationName: 'timeClockSettings.selectBox.days.DAY_5',
  },
  {
    name: '10 days',
    value: -10,
    translationName: 'timeClockSettings.selectBox.days.DAY_10',
  },
  {
    name: '15 days',
    value: -15,
    translationName: 'timeClockSettings.selectBox.days.DAY_15',
  },
  {
    name: '30 days',
    value: -30,
    translationName: 'timeClockSettings.selectBox.days.DAY_30',
  },
  {
    name: '60 days',
    value: -60,
    translationName: 'timeClockSettings.selectBox.days.DAY_60',
  },
];

export const directions = [
  {
    name: 'DOWN',
    value: -1,
  },
  {
    name: 'NEAREST',
    value: 0,
  },
  {
    name: 'UP',
    value: 1,
  },
];

export const lateReminders = [
  {
    name: 'BEFORE_15_MIN_SHIFT_START',
    value: -15,
    translationName: 'timeClockSettings.selectBox.reminder.BEFORE_15_MIN_SHIFT_START',
  },
  {
    name: 'BEGINNING_SHIFT',
    value: 0,
    translationName: 'timeClockSettings.selectBox.reminder.BEGINNING_SHIFT',
  },
  {
    name: 'AFTER_15_MIN_SHIFT_START',
    value: 15,
    translationName: 'timeClockSettings.selectBox.reminder.AFTER_15_MIN_SHIFT_START',
  },
  {
    name: 'AFTER_30_MIN_SHIFT_START',
    value: 30,
    translationName: 'timeClockSettings.selectBox.reminder.AFTER_30_MIN_SHIFT_START',
  },
  {
    name: 'AFTER_45_MIN_SHIFT_START',
    value: 45,
    translationName: 'timeClockSettings.selectBox.reminder.AFTER_45_MIN_SHIFT_START',
  },
  {
    name: 'AFTER_1_HOUR_SHIFT_START',
    value: 60,
    translationName: 'timeClockSettings.selectBox.reminder.AFTER_1_HOUR_SHIFT_START',
  },
];

export const timeCountableOptions = [
  {
    name: 'OFF',
    value: ESettingsTimeClockCount.OFF,
    translationName: 'timeClockSettings.selectBox.minutes.OFF',
  },
  {
    name: '10 Minutes',
    value: ESettingsTimeClockCount.MIN_10,
    translationName: 'timeClockSettings.selectBox.minutes.MIN_10',
  },
  {
    name: '15 Minutes',
    value: ESettingsTimeClockCount.MIN_15,
    translationName: 'timeClockSettings.selectBox.minutes.MIN_15',
  },
  {
    name: '20 Minutes',
    value: ESettingsTimeClockCount.MIN_20,
    translationName: 'timeClockSettings.selectBox.minutes.MIN_20',
  },
  {
    name: '25 Minutes',
    value: ESettingsTimeClockCount.MIN_25,
    translationName: 'timeClockSettings.selectBox.minutes.MIN_25',
  },
  {
    name: '30 Minutes',
    value: ESettingsTimeClockCount.MIN_30,
    translationName: 'timeClockSettings.selectBox.minutes.MIN_30',
  },
];
