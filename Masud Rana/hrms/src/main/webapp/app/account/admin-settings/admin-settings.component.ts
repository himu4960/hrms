import { Component } from '@angular/core';

@Component({
  selector: 'hrms-admin-settings',
  templateUrl: './admin-settings.component.html',
  styleUrls: ['./admin-settings.component.scss'],
})
export class AdminSettingsComponent {}
