import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'app/store';

import { ESettingsTimeClockCount, TimeClockSettings } from 'app/store/states/time-clocks-settings.interface';

import { updateTimeClockSettingsAction } from 'app/store/company-settings/company-settings.actions';

import { selectTimeClockSettings } from 'app/store/company-settings/company-settings.selectors';

import {
  directions,
  lateReminders,
  pastDays,
  resolution,
  roundings,
  shiftBuffers,
  timeCountableOptions,
} from '../resources/time-clock-settings';

@Component({
  selector: 'hrms-company-settings-time-clock',
  templateUrl: './company-settings-time-clock.component.html',
  styleUrls: ['./company-settings-time-clock.component.scss'],
})
export class CompanySettingsTimeClockComponent implements OnInit, OnDestroy {
  timeClockSettings!: TimeClockSettings;

  resolution = resolution;
  roundings = roundings;
  shiftBuffers = shiftBuffers;
  directions = directions;
  lateReminders = lateReminders;
  pastDays = pastDays;
  timeCountableOptions = timeCountableOptions;

  timeClockSettingsForm = this.fb.group({
    enable_auto_punch_out: new FormControl(''),
    allow_employee_to_work_after_shift: new FormControl(''),
    late_countable_time_start: new FormControl(''),
    pre_shift_punch_in_enabled_time: new FormControl(''),
    allow_clock_in_after_timeframe: new FormControl(''),
    allow_clock_in_before_timeframe: new FormControl(''),
    allow_clock_out_after_timeframe: new FormControl(''),
    allow_clock_out_before_timeframe: new FormControl(''),
    allow_webcam_mobile: new FormControl(''),
    auto_clock_out_hour: new FormControl(''),
    break_button_enabled: new FormControl(''),
    clock_in_rounding_direction: new FormControl(''),
    clock_out_notes_required: new FormControl(''),
    clock_out_rounding_direction: new FormControl(''),
    employee_should_use_webcam: new FormControl(''),
    gps_data_required: new FormControl(''),
    lock_timeclocking_to_locations: new FormControl(''),
    notification_late_reminder_for_managers: new FormControl(''),
    notification_late_reminder_for_schedulers: new FormControl(''),
    notification_not_clocked_out_reminder_for_managers: new FormControl(''),
    notification_not_clocked_out_reminder_for_schedulers: new FormControl(''),
    pre_clock_enabled: new FormControl(''),
    pre_clock_mandatory: new FormControl(''),
    restrict_employee_clock_in_out: new FormControl(''),
    restrict_overlapping_timesheets: new FormControl(''),
    round_clock_in_times_to_nearest: new FormControl(''),
    round_clock_out_times_to_nearest: new FormControl(''),
    round_totals_to_nearest: new FormControl(''),
    scheduled_details_enabled: new FormControl(''),
    should_have_position_set: new FormControl(''),
    should_have_remote_site_set: new FormControl(''),
    show_address_under_time_clock: new FormControl(''),
    show_break_times_details: new FormControl(''),
    time_clock_enabled: new FormControl(''),
    time_clock_tips_enabled: new FormControl(''),
    timesheet_allow_employee_to_add_past_x_days: new FormControl(''),
    timesheet_empl_can_edit: new FormControl(''),
    timesheet_employee_can_edit_without_reason: new FormControl(''),
    timesheet_employee_can_import: new FormControl(''),
    timesheet_employee_can_manually_add: new FormControl(''),
    timesheet_employee_can_view_notes: new FormControl(''),
    timesheet_notes_mandatory: new FormControl(''),
    timesheet_prevent_employee_to_edit_approved_time: new FormControl(''),
    webcam_resolution: new FormControl(''),
  });

  isEnabledAutoPunchOut = this.timeClockSettingsForm.get('enable_auto_punch_out')!.value;

  get ESettingsTimeClockCount(): any {
    return ESettingsTimeClockCount;
  }

  private subscriptions: Subscription = new Subscription();

  constructor(protected fb: FormBuilder, private store: Store<AppState>) {}

  ngOnInit(): void {
    this.subscriptions.add(
      this.timeClockSettingsForm.get('enable_auto_punch_out')!.valueChanges.subscribe(value => {
        this.isEnabledAutoPunchOut = value;
      })
    );

    this.subscriptions.add(
      this.store.select(selectTimeClockSettings).subscribe((res: TimeClockSettings) => {
        if (res !== null) {
          this.timeClockSettings = res;
          this.timeClockSettingsForm.patchValue(this.timeClockSettings);
        }
      })
    );
  }

  onSave(): void {
    this.store.dispatch(updateTimeClockSettingsAction({ data: this.timeClockSettingsForm.value }));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
