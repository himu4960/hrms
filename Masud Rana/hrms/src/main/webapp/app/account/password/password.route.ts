import { Route } from '@angular/router';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';
import { PasswordComponent } from './password.component';

export const passwordRoute: Route = {
  path: 'password',
  component: PasswordComponent,
  data: {
    pageTitle: 'global.menu.account.password',
  },
  canActivate: [AuthGuard],
};
