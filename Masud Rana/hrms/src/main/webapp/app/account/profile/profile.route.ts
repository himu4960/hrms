import { Route } from '@angular/router';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';
import { ProfileComponent } from './profile.component';

export const settingsRoute: Route = {
  path: 'profile',
  component: ProfileComponent,
  data: {
    pageTitle: 'global.menu.account.settings',
  },
  canActivate: [AuthGuard],
};
