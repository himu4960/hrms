import { Routes } from '@angular/router';

import { activateRoute } from './activate/activate.route';
import { passwordRoute } from './password/password.route';
import { passwordResetFinishRoute } from './password-reset/finish/password-reset-finish.route';
import { settingsRoute } from './profile/profile.route';
import { newSettingsRoute } from './admin-settings/admin-settings.route';
import { selectCompanyRoute } from './company-selection/company-selection.route';

const ACCOUNT_ROUTES = [activateRoute, passwordRoute, passwordResetFinishRoute, settingsRoute, newSettingsRoute, selectCompanyRoute];

export const accountState: Routes = [
  {
    path: '',
    children: ACCOUNT_ROUTES,
  },
];
