import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { IPassword, PasswordResetFinishService } from './password-reset-finish.service';

@Component({
  selector: 'hrms-reset-password',
  templateUrl: './password-reset-finish.component.html',
})
export class PasswordResetFinishComponent {
  @Input() isPasswordChangeRequest = false;
  doNotMatch = false;
  error = false;
  success = false;

  passwordForm = this.fb.group({
    currentPassword: [''],
    newPassword: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
    confirmPassword: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
  });

  constructor(private passwordResetFinishService: PasswordResetFinishService, private fb: FormBuilder) {}

  resetPassword(): void {
    this.doNotMatch = false;
    this.error = false;

    const currentPassword = this.passwordForm.get(['currentPassword'])!.value;
    const newPassword = this.passwordForm.get(['newPassword'])!.value;
    const confirmPassword = this.passwordForm.get(['confirmPassword'])!.value;

    if (this.isPasswordChangeRequest && newPassword === confirmPassword) {
      const password: IPassword = { currentPassword, newPassword };
      this.passwordResetFinishService.changePassword(password).subscribe(
        () => (this.success = true),
        () => (this.error = true)
      );
    } else if (newPassword === confirmPassword) {
      this.passwordResetFinishService.resetPassword({ newPassword: newPassword }).subscribe(
        () => (this.success = true),
        () => (this.error = true)
      );
    } else {
      this.doNotMatch = true;
    }
  }
}
