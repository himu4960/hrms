import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApplicationConfigService } from 'app/core/config/application-config.service';

export interface IPassword {
  currentPassword: string;
  newPassword: string;
}

@Injectable({ providedIn: 'root' })
export class PasswordResetFinishService {
  constructor(private http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  changePassword(password: IPassword): Observable<{}> {
    return this.http.post(this.applicationConfigService.getEndpointFor('account/change-password'), password);
  }

  resetPassword(password: { newPassword: string }): Observable<{}> {
    return this.http.post(this.applicationConfigService.getEndpointFor('account/reset-password'), password);
  }
}
