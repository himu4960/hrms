import { ComponentFixture, TestBed, inject, tick, fakeAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, throwError } from 'rxjs';

import { PasswordResetFinishComponent } from './password-reset-finish.component';
import { PasswordResetFinishService } from './password-reset-finish.service';

describe('Component Tests', () => {
  describe('PasswordResetFinishComponent', () => {
    let fixture: ComponentFixture<PasswordResetFinishComponent>;
    let comp: PasswordResetFinishComponent;

    beforeEach(() => {
      fixture = TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [PasswordResetFinishComponent],
        providers: [
          FormBuilder,
          {
            provide: ActivatedRoute,
            useValue: { queryParams: of({ key: 'XYZPDQ' }) },
          },
        ],
      })
        .overrideTemplate(PasswordResetFinishComponent, '')
        .createComponent(PasswordResetFinishComponent);
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(PasswordResetFinishComponent);
      comp = fixture.componentInstance;
    });

    it('should ensure the two passwords entered match', () => {
      comp.passwordForm.patchValue({
        newPassword: 'password',
        confirmPassword: 'non-matching',
      });

      comp.resetPassword();

      expect(comp.doNotMatch).toBe(true);
    });

    it('should update success to true after resetting password', inject(
      [PasswordResetFinishService],
      fakeAsync((service: PasswordResetFinishService) => {
        spyOn(service, 'resetPassword').and.returnValue(of({}));
        comp.passwordForm.patchValue({
          newPassword: 'password',
          confirmPassword: 'password',
        });

        comp.resetPassword();
        tick();

        expect(comp.success).toBe(true);
      })
    ));

    it('should notify of generic error', inject(
      [PasswordResetFinishService],
      fakeAsync((service: PasswordResetFinishService) => {
        spyOn(service, 'resetPassword').and.returnValue(throwError('ERROR'));
        comp.passwordForm.patchValue({
          newPassword: 'password',
          confirmPassword: 'password',
        });

        comp.resetPassword();
        tick();

        expect(comp.success).toBe(false);
        expect(comp.error).toBe(true);
      })
    ));
  });
});
