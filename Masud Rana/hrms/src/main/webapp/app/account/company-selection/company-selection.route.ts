import { Route } from '@angular/router';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';

import { CompanySelectionComponent } from './company-selection.component';

export const selectCompanyRoute: Route = {
  path: 'select-company',
  component: CompanySelectionComponent,
  data: {
    pageTitle: 'global.menu.account.selectComapny',
  },
  canActivate: [AuthGuard],
};
