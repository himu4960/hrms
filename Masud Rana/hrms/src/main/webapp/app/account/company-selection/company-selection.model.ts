export interface ICompanySelectionPayload {
  company_id: number;
  user_id: number;
}
