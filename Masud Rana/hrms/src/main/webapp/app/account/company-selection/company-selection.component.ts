import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { Subscription } from 'rxjs';

import { AppState } from 'app/store';

import { loadCompanySelectAction } from 'app/store/company-settings/company-settings.actions';
import { currentCompanyAction, selectCompanyAction } from 'app/store/user/user.actions';

import { User, UserCompany } from 'app/store/states/user.interface';

import { selectUser } from 'app/store/user/user.selectors';

import { StateStorageService } from 'app/core/auth/state-storage.service';
import { ToastService } from 'app/shared/components/toast/toast.service';
import { RegistrationConfirmationService } from '../registration-confirmation/registration-confirmation.service';
import { RoleType } from 'app/shared/enum';

@Component({
  selector: 'hrms-company-selection',
  templateUrl: './company-selection.component.html',
  styleUrls: ['./company-selection.component.scss'],
})
export class CompanySelectionComponent implements OnInit, OnDestroy {
  isSentConfirmation = false;
  companies!: UserCompany[];
  defaultLogoUrl = 'content/images/dummy-logo.png';

  get roleType(): any {
    return RoleType;
  }

  private subscriptions: Subscription = new Subscription();

  constructor(
    private store: Store<AppState>,
    private storageService: StateStorageService,
    private router: Router,
    private toastService: ToastService,
    private registrationConfirmationService: RegistrationConfirmationService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.getUserCompanies();
  }

  selectCompany(userCompany: UserCompany): void {
    const company_id = userCompany?.company?.id;
    const employee_invitation_accepted = userCompany?.employee?.invitation_accepted ?? null;
    const user_company_is_active = userCompany?.is_active ?? false;
    const user_id = this.storageService.getUserData()?.id;
    if (company_id && user_id && employee_invitation_accepted && user_company_is_active) {
      this.store.dispatch(loadCompanySelectAction({ company_id, user_id }));
      this.store.dispatch(currentCompanyAction({ data: this.getSelectedCompany(company_id) }));
      this.store.dispatch(selectCompanyAction({ data: true }));

      this.storageService.setCurrentCompany(this.getSelectedCompany(company_id));
    } else {
      if (!employee_invitation_accepted) {
        this.toastService.showDangerToast(this.translateService.instant('login.messages.error.confirmAccount'));
      }

      if (!user_company_is_active) {
        this.toastService.showDangerToast(this.translateService.instant('login.messages.error.disabledCompanyAccount'));
      }
    }
  }

  sendConfirmation(invitation_token?: string | null): void {
    if (invitation_token && invitation_token !== undefined) {
      this.registrationConfirmationService.sendConfirmationEmail(invitation_token).subscribe(() => {
        this.isSentConfirmation = true;
        this.toastService.showSuccessToast(this.translateService.instant('login.messages.emailAlreadySent'));
      });
    }
  }

  getUserCompanies(): void {
    this.subscriptions.add(
      this.store.select(selectUser).subscribe((user: User) => {
        if (user) {
          this.companies = user?.user_companies;
          if (user?.user_companies?.length === 1) {
            this.selectCompany(user?.user_companies[0]);
          }
        } else {
          this.companies = this.storageService.getUserData()?.user_companies ?? [];
        }
      })
    );
  }

  getSelectedCompany(id: number): UserCompany {
    return this.companies.find(userCompany => userCompany.company.id === id) as UserCompany;
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
