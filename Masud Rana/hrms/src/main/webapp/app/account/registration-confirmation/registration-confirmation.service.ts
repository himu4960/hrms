import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApplicationConfigService } from '../../core/config/application-config.service';

@Injectable({
  providedIn: 'root',
})
export class RegistrationConfirmationService {
  public resourceUrl = this.applicationConfigService.getEndpointFor(`register`);
  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  confirmAccount(token: string): Observable<any> {
    return this.http.get<any>(`${this.resourceUrl}/confirm/${token}`);
  }

  sendConfirmationEmail(token: string): Observable<any> {
    return this.http.get<any>(`${this.resourceUrl}/send/${token}`);
  }
}
