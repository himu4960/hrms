import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { UtilService } from 'app/shared/service/util.service';
import { RegistrationConfirmationService } from './registration-confirmation.service';

@Component({
  selector: 'hrms-registration-confirmation',
  templateUrl: './registration-confirmation.component.html',
  styleUrls: ['./registration-confirmation.component.scss'],
})
export class RegistrationConfirmationComponent {
  token = '';
  isSuccess = false;
  isError = false;
  successMessage: any = null;
  error: any = null;

  constructor(
    private route: ActivatedRoute,
    private utilService: UtilService,
    private router: Router,
    private registrationConfirmationService: RegistrationConfirmationService,
    private translateService: TranslateService
  ) {
    this.route.queryParams.subscribe(params => {
      if (!this.utilService.hasKeys(params) && params?.token) {
        this.token = params.token;
      } else {
        router.navigateByUrl('/login');
      }
    });
  }

  previousState(): void {
    this.router.navigateByUrl('/login');
  }

  confirmAccount(): void {
    this.registrationConfirmationService.confirmAccount(this.token).subscribe(
      res => {
        this.isSuccess = true;
        this.successMessage = this.translateService.instant('hrmsApp.employee.alert.success.hasBeenConfirmed');
      },
      err => {
        this.error = err.error;
        this.isError = true;
      }
    );
  }

  sendConfirmationEmail(): void {
    this.registrationConfirmationService.sendConfirmationEmail(this.token).subscribe(
      res => {
        this.isSuccess = true;
        this.successMessage = this.translateService.instant('hrmsApp.employee.alert.success.emailSent');
      },
      err => {
        this.isError = true;
        this.error = err.error;
      }
    );
  }
}
