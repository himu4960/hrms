import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';

import { RegistrationConfirmationComponent } from './registration-confirmation.component';

@NgModule({
  declarations: [RegistrationConfirmationComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: RegistrationConfirmationComponent,
      },
    ]),
  ],
})
export class RegistrationConfirmationModule {}
