import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { accountState } from './account.route';

import { ActivateComponent } from './activate/activate.component';
import { ProfileComponent } from './profile/profile.component';
import { CompanySelectionComponent } from './company-selection/company-selection.component';

import { SharedModule } from 'app/shared/shared.module';
import { AdminSettingsModule } from './admin-settings/admin-settings.module';
import { PasswordManagementModule } from 'app/employee/module/password-management.module';

@NgModule({
  imports: [
    SharedModule,
    AdminSettingsModule,
    PasswordManagementModule,
    RouterModule.forChild(accountState),
    NgMultiSelectDropDownModule.forRoot(),
  ],
  declarations: [ActivateComponent, ProfileComponent, CompanySelectionComponent],
})
export class AccountModule {}
