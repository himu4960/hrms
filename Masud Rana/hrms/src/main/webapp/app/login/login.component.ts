import { Component, ViewChild, AfterViewInit, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { AppState } from 'app/store';

import { loginUserAction } from 'app/store/user/user.actions';

import { selectState } from './../store/user/user.selectors';

@Component({
  selector: 'hrms-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, AfterViewInit {
  loading = false;
  @ViewChild('username', { static: false })
  username?: ElementRef;

  authenticationError = false;

  loginForm = this.fb.group({
    username: [null, [Validators.required]],
    password: [null, [Validators.required]],
    rememberMe: [false],
  });

  private subscriptions: Subscription = new Subscription();

  constructor(private fb: FormBuilder, private store: Store<AppState>) {}

  ngOnInit(): void {
    this.subscriptions.add(
      this.store.select(selectState).subscribe((res: any) => {
        if (res !== null) {
          this.loading = res.loading;
        }
      })
    );
  }

  ngAfterViewInit(): void {
    if (this.username) {
      this.username.nativeElement.focus();
    }
  }

  login(): void {
    this.loading = true;
    const userData = {
      username: this.loginForm.get('username')!.value,
      password: this.loginForm.get('password')!.value,
      rememberMe: this.loginForm.get('rememberMe')!.value,
    };
    this.store.dispatch(
      loginUserAction({
        data: userData,
      })
    );
  }
}
