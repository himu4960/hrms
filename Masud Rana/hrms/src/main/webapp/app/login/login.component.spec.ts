jest.mock('@angular/router');
jest.mock('app/core/auth/account.service');
jest.mock('app/login/login.service');

import { ComponentFixture, TestBed, waitForAsync, inject, fakeAsync, tick } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { of } from 'rxjs';

import { AccountService } from 'app/core/auth/account.service';

import { LoginService } from './login.service';
import { LoginComponent } from './login.component';
import { provideMockStore } from '@ngrx/store/testing';

describe('Component Tests', () => {
  describe('LoginComponent', () => {
    let comp: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;

    beforeEach(
      waitForAsync(() => {
        TestBed.configureTestingModule({
          declarations: [LoginComponent],
          providers: [
            FormBuilder,
            AccountService,
            provideMockStore(),
            Router,
            {
              provide: LoginService,
              useValue: {
                login: jest.fn(() => of({})),
              },
            },
          ],
        })
          .overrideTemplate(LoginComponent, '')
          .compileComponents();
      })
    );

    beforeEach(() => {
      fixture = TestBed.createComponent(LoginComponent);
      comp = fixture.componentInstance;
      TestBed.inject(Router);
      TestBed.inject(LoginService);
      TestBed.inject(AccountService);
    });

    it('should authenticate the user and navigate to home page', inject(
      [],
      fakeAsync(() => {
        // GIVEN
        const credentials = {
          username: 'admin',
          password: 'admin',
          rememberMe: true,
        };

        comp.loginForm.patchValue(credentials);

        // WHEN/
        comp.login();
        tick(); // simulate async

        // THEN
        expect(comp.authenticationError).toEqual(false);
      })
    ));
  });
});
