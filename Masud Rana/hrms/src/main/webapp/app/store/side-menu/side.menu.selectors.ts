import { createFeatureSelector, createSelector } from '@ngrx/store';

import { IMenu } from 'app/entities/menu/menu.model';

import { State, sideMenuFeatureKey } from './side.menu.reducers';

export const selectSideMenuState = createFeatureSelector<State>(sideMenuFeatureKey);

export const selectSideMenu = createSelector(selectSideMenuState, (state: State) => state.sideMenu as IMenu);
