import { Injectable } from '@angular/core';

import { select, Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { of } from 'rxjs';
import { mergeMap, withLatestFrom } from 'rxjs/operators';

import { PermissionService } from 'app/shared/service/permission.service';
import { StateStorageService } from 'app/core/auth/state-storage.service';

import { falseAction } from '../company-settings/company-settings.actions';
import { browserReloadSideMenuAction, loadSideMenuAction } from './side.menu.actions';
import { selectAllSettings } from '../company-settings/company-settings.selectors';

import { IMenu } from 'app/entities/menu/menu.model';

import { sideMenus } from 'app/shared/menus';

import { AppState } from '..';
import { selectCurrentRoute } from '../route/router.selector';

@Injectable()
export class SideMenuEffects {
  loadMenu$ = createEffect(() =>
    this.actions$.pipe(
      ofType(browserReloadSideMenuAction),
      withLatestFrom(this.store.pipe(select(selectCurrentRoute))),
      mergeMap(([response, routeResponse]: any[]) => {
        if (response !== null) {
          const acl = this.storageService.getMenu();
          const id = this.findById(acl, { url: routeResponse.url });

          const findSideMenu = this.findByChild(acl, id, []);

          const permission = this.menuAndSettingsPermission(findSideMenu);
          const treeMenus = this.createMenuTree(permission);
          return of(loadSideMenuAction({ data: treeMenus }));
        }
        return of(falseAction());
      })
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private permissionService: PermissionService,
    private storageService: StateStorageService
  ) {}

  private menuAndSettingsPermission(response: IMenu[]): IMenu[] {
    let settings: any = {};
    const menus: IMenu[] = sideMenus;

    this.store.select(selectAllSettings).subscribe(allSettings => {
      settings = allSettings;
    });
    return menus
      .filter((menu: IMenu) => {
        if (settings && menu.settings_model_name && menu.settings_field_name) {
          const settings_enabled = settings[`${menu?.settings_model_name}`][`${menu?.settings_field_name}`];
          if (this.permissionService.isEmployee()) {
            return menu && settings_enabled && this.permissionService.isEmployee();
          } else if (menu.is_for_employee) {
            return true;
          } else {
            return menu && settings_enabled;
          }
        } else {
          return menu;
        }
      })
      .map((menu: IMenu) => {
        return this.findOne(response, menu.router_link) || {};
      })
      .filter((menu: IMenu) => menu?.id);
  }

  private findOne(response: any[], value?: string): IMenu {
    return response?.find((r: IMenu) => r.router_link === value);
  }

  private findById(acl: IMenu[], menuOptions?: IMenuOptions): any {
    const currentMenu = acl?.find((menu: IMenu) => {
      if (menuOptions?.url) {
        return menu?.router_link === menuOptions?.url;
      } else {
        return menu?.id === menuOptions?.parentId;
      }
    });

    if (currentMenu?.parent_id) {
      return this.findById(acl, { parentId: currentMenu?.parent_id });
    } else {
      const id = currentMenu?.id;
      const parent_id = currentMenu?.parent_id ?? null;
      return parent_id ?? id;
    }
  }

  private findByChild(acl: IMenu[], id?: number, node?: IMenu[]): any {
    const menus = acl?.filter((menu: IMenu) => menu?.parent_id === id);
    for (const menu of menus) {
      node?.push({ ...menu });
      this.findByChild(acl, menu.id, node);
    }
    return node;
  }

  private createMenuTree: any = (input: any[]) => {
    const mapMenu = new Map(input.map(item => [item.id, item]));
    const tree: IMenu[] = [];
    for (const item of input) {
      if (mapMenu.has(item.parent_id)) {
        mapMenu.get(item.parent_id).children.push(mapMenu.get(item.id));
      } else {
        tree.push(mapMenu.get(item.id));
      }
    }
    return tree;
  };
}

interface IMenuOptions {
  url?: string;
  parentId?: number;
}
