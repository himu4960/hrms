import { createReducer, on } from '@ngrx/store';
import { logoutUserAction } from '../user/user.actions';

import { browserReloadSideMenuAction, loadSideMenuAction } from './side.menu.actions';

export const sideMenuFeatureKey = 'side-menu';

export interface State {
  sideMenu: any;
}

export const initialState: State = {
  sideMenu: null,
};

export const reducer = createReducer(
  initialState,

  // STORE MENU DATA
  on(loadSideMenuAction, browserReloadSideMenuAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      sideMenu: data,
    };
  }),
  // LOGOUT ACTION
  on(logoutUserAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      sideMenu: null,
    };
  })
);
