import { createAction, props } from '@ngrx/store';

/** ........... MENU .......... */

export const loadSideMenuAction = createAction('[SIDE MENU Component] Load SIDE MENU', props<{ data: any }>());

export const browserReloadSideMenuAction = createAction('[Side Main Component] Browser Reload Side Menu Action');
