export interface TimeClockSettings {
  enable_auto_punch_out: boolean;
  allow_employee_to_work_after_shift: number;
  late_countable_time_start: number;
  pre_shift_punch_in_enabled_time: number;
  time_clock_enabled: boolean;
  employee_should_use_webcam: boolean;
  allow_webcam_mobile: boolean;
  webcam_resolution: any;
  gps_data_required: boolean;
  show_address_under_time_clock: boolean;
  pre_clock_enabled: boolean;
  pre_clock_mandatory: boolean;
  restrict_employee_clock_in_out: boolean;
  lock_timeclocking_to_locations: boolean;
  should_have_position_set: boolean;
  should_have_remote_site_set: boolean;
  clock_out_notes_required: boolean;
  break_button_enabled: boolean;
  auto_clock_out_hour: number;
  time_clock_tips_enabled: boolean;
  scheduled_details_enabled: boolean;
  show_break_times_details: boolean;
  restrict_overlapping_timesheets: boolean;
  round_totals_to_nearest: number;
  round_clock_in_times_to_nearest: number;
  clock_in_rounding_direction: number;
  round_clock_out_times_to_nearest: number;
  clock_out_rounding_direction: number;
  allow_clock_in_before_timeframe: number;
  allow_clock_in_after_timeframe: number;
  allow_clock_out_before_timeframe: number;
  allow_clock_out_after_timeframe: number;
  timesheet_employee_can_import: boolean;
  timesheet_employee_can_manually_add: boolean;
  timesheet_empl_can_edit: boolean;
  timesheet_prevent_employee_to_edit_approved_time: boolean;
  timesheet_allow_employee_to_add_past_x_days: number;
  timesheet_employee_can_view_notes: boolean;
  timesheet_notes_mandatory: boolean;
  timesheet_employee_can_edit_without_reason: boolean;
  notification_late_reminder_for_schedulers: number;
  notification_late_reminder_for_managers: number;
  notification_not_clocked_out_reminder_for_schedulers: number;
  notification_not_clocked_out_reminder_for_managers: number;
  is_active: boolean;
  is_deleted: boolean;
}

export enum ESettingsTimeClockCount {
  OFF = 0,
  MIN_10 = 600 * 1000,
  MIN_15 = 900 * 1000,
  MIN_20 = 1200 * 1000,
  MIN_25 = 1500 * 1000,
  MIN_30 = 1800 * 1000,
}
