import { IEmployee } from 'app/employee/models/employee.model';

export interface User {
  id: number;
  created_at: Date;
  updated_at: Date;
  updated_by?: any;
  created_by?: any;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  langKey: string;
  imageUrl?: any;
  user_companies: UserCompany[];
}

export interface UserCompany {
  is_owner: boolean;
  is_active: boolean;
  company: Company;
  employee: IEmployee;
  role: Role;
}

export interface Company {
  id: number;
  created_at: Date;
  updated_at: Date;
  updated_by?: any;
  created_by?: any;
  name: string;
  email: string;
  phone: string;
  address?: any;
  logo_url_large?: any;
  logo_url_medium?: any;
  logo_url_small?: any;
  status: boolean;
  company_type_id: number;
  application_type_id: number;
}

export interface Role {
  id: number;
  name: string;
}
