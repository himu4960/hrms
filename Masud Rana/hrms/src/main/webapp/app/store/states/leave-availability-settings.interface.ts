export interface LeaveAvailabilitySettings {
  id: number;
  leave_module_enabled: boolean;
  deduct_weekends_from_leaves: any;
  employees_can_book_vacation: boolean;
  show_deleted_employees_in_leave_approvals: boolean;
  schedulers_should_receive_leave_notifications: boolean;
  allow_schedulers_to_approve_past_leave_requests: boolean;
  leave_must_be_booked_in_days_advance: number;
  max_staff_can_be_booked_at_once: number;
  employees_can_cancel_previously_approved_leave_requests: boolean;
  availability_module_enabled: boolean;
  employees_can_modify_unavailability: boolean;
  availability_must_be_approved_by_management: boolean;
  allow_schedulers_to_approve_availability: boolean;
}
