import { createAction, props } from '@ngrx/store';

export const employeeAction = createAction('[Employee Component] Load Employee Common Data Action');
export const employeeSuccessAction = createAction('[Employee Effect] Load Employee Common Data Success Action', props<{ data: any }>());

export const employeeFilterAction = createAction('[Employee Component] Employee Filter Action', props<{ filter: any; pagination?: any }>());
export const employeeFilterSuccessAction = createAction('[Employee Effect] Employee Filter Success Action', props<{ data: any }>());
