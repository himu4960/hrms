import { createFeatureSelector, createSelector } from '@ngrx/store';

import { State, employeeFeatureKey } from './employee.reducers';

export const selectEmployeeState = createFeatureSelector<State>(employeeFeatureKey);

export const selectEmployeeData = createSelector(selectEmployeeState, (state: State) => state);
