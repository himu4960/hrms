import { createReducer, on } from '@ngrx/store';

import { employeeAction, employeeFilterSuccessAction, employeeSuccessAction } from './employee.actions';

import { IEmployeeData } from './../../shared/models/common.model';

export const employeeFeatureKey = 'employees';

export interface State {
  employees: any;
  branches: any;
  designations: any;
  skills: any;
  total_employee_count: any;
  not_activated: any;
  disabled: any;
}

export const initialState: State = {
  employees: null,
  branches: null,
  designations: null,
  skills: null,
  total_employee_count: null,
  not_activated: null,
  disabled: null,
};

export const reducer = createReducer(
  initialState,

  // STORE EMPLOYEE DATA
  on(employeeSuccessAction, (state: State, action: any) => {
    const data: IEmployeeData = action?.data;
    return {
      ...state,
      employees: data?.employees ?? [],
      branches: data?.branches ?? [],
      designations: data?.branches[0]?.designations ?? [],
      skills: data?.skills ?? [],
      total_employee_count: data?.total_employee_count ?? 0,
      not_activated: data?.not_activated ?? 0,
      disabled: data?.disabled ?? 0,
    };
  }),

  on(employeeFilterSuccessAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      employees: data,
    };
  })
);
