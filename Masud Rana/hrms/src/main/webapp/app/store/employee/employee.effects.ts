import { IEmployeeWithCount } from './../../employee/models/employee.model';
import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';

import { of } from 'rxjs';
import { map, mergeMap, withLatestFrom } from 'rxjs/operators';

import { AppState } from '..';

import { IEmployee } from 'app/employee/models/employee.model';

import { CommonService } from './../../shared/service/common.service';
import { EmployeeService } from './../../employee/service/employee.service';

import { falseAction } from './../company-settings/company-settings.actions';
import { employeeAction, employeeFilterAction, employeeFilterSuccessAction, employeeSuccessAction } from './employee.actions';

import { selectEmployeeData } from './employee.selectors';

@Injectable()
export class EmployeeEffects {
  employeeData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(employeeAction),
      withLatestFrom(this.store.pipe(select(selectEmployeeData))),
      mergeMap((res: any[]) => {
        if (res[1] !== null) {
          return this.commonService.getEmployeeData().pipe(
            map((employeeData: any) => {
              return employeeSuccessAction({ data: employeeData });
            })
          );
        }
        return of(falseAction());
      })
    )
  );

  employeeFilteredData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(employeeFilterAction),
      withLatestFrom(this.store.pipe(select(selectEmployeeData))),
      mergeMap((filterRes: any[]) => {
        const filter = filterRes[0]?.filter;
        const pagination = filterRes[0]?.pagination;
        if (filterRes[1] !== null) {
          return this.employeeService.filter(filter, pagination).pipe(
            map((employeeRes: HttpResponse<IEmployeeWithCount>) => {
              return employeeFilterSuccessAction({ data: employeeRes.body?.employees });
            })
          );
        }
        return of(falseAction());
      })
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private commonService: CommonService,
    private employeeService: EmployeeService
  ) {}
}
