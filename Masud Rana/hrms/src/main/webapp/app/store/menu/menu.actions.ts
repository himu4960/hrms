import { createAction, props } from '@ngrx/store';

/** ........... MENU .......... */

export const loadMenuAction = createAction('[MENU Component] Load MENU', props<{ data: any }>());

export const browserReloadMenuAction = createAction('[Main Component] Browser Reload Menu Action');
