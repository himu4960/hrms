import { Injectable } from '@angular/core';

import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { AccountService } from 'app/core/auth/account.service';
import { PermissionService } from 'app/shared/service/permission.service';

import { falseAction } from '../company-settings/company-settings.actions';
import { browserReloadMenuAction, loadMenuAction } from './menu.actions';
import { selectAllSettings } from '../company-settings/company-settings.selectors';

import { IMenu } from 'app/entities/menu/menu.model';

import { mainMenus } from 'app/shared/menus';

import { AppState } from '..';

@Injectable()
export class MenuEffects {
  loadMenu$ = createEffect(() =>
    this.actions$.pipe(
      ofType(browserReloadMenuAction),
      mergeMap((res: any) => {
        if (res !== null) {
          return this.accountService.getRoleMenu().pipe(
            map((response: any) => {
              const permission = this.menuAndSettingsPermission(response);
              return loadMenuAction({ data: permission });
            })
          );
        }
        return of(falseAction());
      })
    )
  );

  constructor(
    private actions$: Actions,
    private accountService: AccountService,
    private store: Store<AppState>,
    private permissionService: PermissionService
  ) {}

  private menuAndSettingsPermission(response: IMenu[]): IMenu[] {
    let settings: any = {};
    const menus: IMenu[] = mainMenus;

    this.store.select(selectAllSettings).subscribe(allSettings => {
      settings = allSettings;
    });
    return menus
      .filter((menu: IMenu) => {
        if (menu.settings_model_name && menu.settings_field_name) {
          const settings_enabled = settings[`${menu.settings_model_name}`][`${menu.settings_field_name}`];
          if (this.permissionService.isEmployee()) {
            return menu && settings_enabled && this.permissionService.isEmployee();
          } else if (menu.is_for_employee) {
            return true;
          } else {
            return menu && settings_enabled;
          }
        } else {
          return menu;
        }
      })
      .map((menu: IMenu) => {
        return this.findOne(response, menu.router_link) || {};
      })
      .filter((menu: IMenu) => menu?.id);
  }

  private findOne(response: any[], value?: string): IMenu {
    return response?.find((r: IMenu) => r.router_link === value);
  }
}
