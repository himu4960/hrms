import { createFeatureSelector, createSelector } from '@ngrx/store';

import { IMenu } from 'app/entities/menu/menu.model';

import { State, menuFeatureKey } from './menu.reducers';

export const selectMenuState = createFeatureSelector<State>(menuFeatureKey);

export const selectMenu = createSelector(selectMenuState, (state: State) => state.menu as IMenu);
