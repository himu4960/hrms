import { createReducer, on } from '@ngrx/store';
import { logoutUserAction } from '../user/user.actions';

import { browserReloadMenuAction, loadMenuAction } from './menu.actions';

export const menuFeatureKey = 'menu';

export interface State {
  menu: any;
}

export const initialState: State = {
  menu: null,
};

export const reducer = createReducer(
  initialState,

  // STORE MENU DATA
  on(loadMenuAction, browserReloadMenuAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      menu: data,
    };
  }),
  // LOGOUT ACTION
  on(logoutUserAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      menu: null,
    };
  })
);
