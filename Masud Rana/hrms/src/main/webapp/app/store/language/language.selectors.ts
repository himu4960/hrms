import { createFeatureSelector, createSelector } from '@ngrx/store';

import { State, languageFeatureKey } from './language.reducers';

export const selectLanguageState = createFeatureSelector<State>(languageFeatureKey);

export const selectLanguage = createSelector(selectLanguageState, (state: State) => state.language);
