import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { map, mergeMap, catchError, withLatestFrom } from 'rxjs/operators';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';

import { AppState } from '..';

import { DEFAULT_LANGUAGE_CODE } from 'app/app.constants';

import { ILanguage } from 'app/shared/models/utils.model';

import { UtilService } from 'app/shared/service/util.service';

import { falseAction } from '../company-settings/company-settings.actions';
import { languageAction, languageErrorAction, languageSuccessAction } from './language.actions';

import { selectLanguage } from './language.selectors';

@Injectable()
export class LanguageEffects {
  languageUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(languageAction),
      withLatestFrom(this.store.pipe(select(selectLanguage))),
      mergeMap((res: any[]) => {
        if (!res[1].length) {
          return this.utilService.getActiveLanguages().pipe(
            map(
              languages => {
                const defaultLanguage: ILanguage | undefined = languages.find((language: ILanguage) => language.is_default);
                if (defaultLanguage?.code) {
                  this.translateService.setDefaultLang(defaultLanguage.code);
                  this.translateService.use(defaultLanguage.code);
                }

                return languageSuccessAction({ data: languages });
              },
              catchError(() => {
                this.translateService.setDefaultLang(DEFAULT_LANGUAGE_CODE);
                this.translateService.use(DEFAULT_LANGUAGE_CODE);
                return of(languageErrorAction());
              })
            )
          );
        }
        return of(falseAction());
      })
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private utilService: UtilService,
    private translateService: TranslateService
  ) {}
}
