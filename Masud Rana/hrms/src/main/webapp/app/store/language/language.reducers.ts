import { createReducer, on } from '@ngrx/store';

import { languageAction, languageErrorAction, languageSuccessAction } from './language.actions';

import { ILanguage } from 'app/shared/models/utils.model';

export const languageFeatureKey = 'languages';

export interface State {
  loading: boolean;
  language: ILanguage[];
}

export const initialState: State = {
  loading: false,
  language: [],
};

export const reducer = createReducer(
  initialState,

  // STORE LANGUAGE DATA
  on(languageSuccessAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      loading: false,
      language: data,
    };
  }),
  on(languageErrorAction, (state: State) => {
    return {
      ...state,
      loading: false,
    };
  }),
  on(languageAction, (state: State) => {
    return {
      ...state,
      loading: true,
    };
  })
);
