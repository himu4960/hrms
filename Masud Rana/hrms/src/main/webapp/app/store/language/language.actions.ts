import { createAction, props } from '@ngrx/store';

import { ILanguage } from 'app/shared/models/utils.model';

export const languageAction = createAction('[Main Component] Language Action');
export const languageSuccessAction = createAction('[Language Effects] Language Success Action', props<{ data: ILanguage }>());
export const languageErrorAction = createAction('[Language Effects] Language Error Action');
