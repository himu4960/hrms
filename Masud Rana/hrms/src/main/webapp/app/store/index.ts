import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import { ActionReducerMap, MetaReducer } from '@ngrx/store';

import { CompanySettingsEffects } from './company-settings/company-settings.effects';
import { CompanyPropertiesEffects } from './company-properties/company-properties.effects';
import { UserEffects } from './user/user.effects';
import { MenuEffects } from './menu/menu.effects';
import { SideMenuEffects } from './side-menu/side.menu.effects';
import { EmployeeEffects } from './employee/employee.effects';
import { LanguageEffects } from './language/language.effects';

import * as companySettings from './company-settings/company-settings.reducers';
import * as companyProperties from './company-properties/company-properties.reducers';
import * as user from './user/user.reducers';
import * as language from './language/language.reducers';
import * as employee from './employee/employee.reducers';
import * as menu from './menu/menu.reducers';
import * as sideMenu from './side-menu/side.menu.reducers';

import { environment } from 'app/environments/environment';

export interface AppState {
  router: RouterReducerState<any>;
  [language.languageFeatureKey]: language.State;
  [user.userFeatureKey]: user.State;
  [employee.employeeFeatureKey]: employee.State;
  [menu.menuFeatureKey]: menu.State;
  [sideMenu.sideMenuFeatureKey]: sideMenu.State;
  [companySettings.companySettingsFeatureKey]: companySettings.State;
  [companyProperties.companyPropertiesFeatureKey]: companyProperties.State;
}

export const reducers: ActionReducerMap<AppState> = {
  router: routerReducer,
  [language.languageFeatureKey]: language.reducer,
  [user.userFeatureKey]: user.reducer,
  [employee.employeeFeatureKey]: employee.reducer,
  [menu.menuFeatureKey]: menu.reducer,
  [sideMenu.sideMenuFeatureKey]: sideMenu.reducer,
  [companySettings.companySettingsFeatureKey]: companySettings.reducer,
  [companyProperties.companyPropertiesFeatureKey]: companyProperties.reducer,
};

export const effects: any[] = [
  CompanySettingsEffects,
  CompanyPropertiesEffects,
  UserEffects,
  MenuEffects,
  SideMenuEffects,
  EmployeeEffects,
  LanguageEffects,
];

export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
