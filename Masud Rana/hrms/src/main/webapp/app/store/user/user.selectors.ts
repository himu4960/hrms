import { createFeatureSelector, createSelector } from '@ngrx/store';

import { State, userFeatureKey } from './user.reducers';
import { User, UserCompany } from '../states/user.interface';

export const selectUserState = createFeatureSelector<State>(userFeatureKey);

export const selectState = createSelector(selectUserState, (state: State) => state);
export const selectUser = createSelector(selectUserState, (state: State) => state.user as User);
export const selectCurrentCompany = createSelector(selectUserState, (state: State) => state.selectedCompany as UserCompany);
