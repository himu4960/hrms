import { createReducer, on } from '@ngrx/store';

import {
  browserReloadCurrentCompanyAction,
  browserReloadJwtTokenAction,
  browserReloadUserDataAction,
  currentCompanyAction,
  jwtTokenAction,
  loginUserErrorAction,
  loginUserSuccessAction,
  logoutUserAction,
} from './user.actions';

import { User } from '../states/user.interface';

export const userFeatureKey = 'user';

export interface State {
  loading: boolean;
  user: User | null;
  selectedCompany: any;
  jwtToken: string | null;
}

export const initialState: State = {
  loading: false,
  user: null,
  selectedCompany: null,
  jwtToken: null,
};

export const reducer = createReducer(
  initialState,

  // STORE USER DATA
  on(loginUserSuccessAction, browserReloadUserDataAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      loading: false,
      user: data,
    };
  }),
  on(loginUserErrorAction, (state: State) => {
    return {
      ...state,
      loading: false,
      user: null,
      selectedCompany: null,
      jwtToken: null,
    };
  }),
  on(logoutUserAction, (state: State) => {
    return {
      ...state,
      loading: false,
      user: null,
      selectedCompany: null,
      jwtToken: null,
    };
  }),
  on(currentCompanyAction, browserReloadCurrentCompanyAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      selectedCompany: data,
    };
  }),
  on(jwtTokenAction, browserReloadJwtTokenAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      jwtToken: data,
    };
  })
);
