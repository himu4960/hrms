import { createAction, props } from '@ngrx/store';

export const loginUserAction = createAction('[Login Component] Login User Action', props<{ data: any }>());
export const loginUserSuccessAction = createAction('[User Effects] Login User Success Action', props<{ data: any }>());
export const loginUserErrorAction = createAction('[User Effects] Login User Error Action');
export const logoutUserAction = createAction('[Logout Component] Log Out Action');
export const selectCompanyAction = createAction('[Main Component] User Has Company Action', props<{ data: any }>());
export const currentCompanyAction = createAction('[Company Selection Component] Current Company Action', props<{ data: any }>());
export const jwtTokenAction = createAction('[Main Component] JWT Token Action', props<{ data: any }>());
export const browserReloadJwtTokenAction = createAction('[Main Component] Browser Reload JWT Token Action', props<{ data: any }>());
export const browserReloadUserDataAction = createAction('[Main Component] Browser Reload Action', props<{ data: any }>());
export const browserReloadCurrentCompanyAction = createAction(
  '[Main Component] Browser Reload Current Company Action',
  props<{ data: any }>()
);
