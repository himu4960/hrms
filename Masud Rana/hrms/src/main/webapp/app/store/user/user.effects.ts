import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';

import { of } from 'rxjs';
import { catchError, map, mergeMap, withLatestFrom } from 'rxjs/operators';

import { AppState } from '..';

import { Login } from 'app/login/login.model';

import { StateStorageService } from 'app/core/auth/state-storage.service';
import { LoginService } from 'app/login/login.service';

import { User } from '../states/user.interface';

import { falseAction } from '../company-settings/company-settings.actions';
import { loginUserAction, loginUserErrorAction, loginUserSuccessAction } from './user.actions';

import { selectUser } from './user.selectors';

@Injectable()
export class UserEffects {
  loginUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginUserAction),
      withLatestFrom(this.store.pipe(select(selectUser))),
      mergeMap((res: any[]) => {
        const credentials: Login = { ...res[0]?.data };

        if (res[1] === null) {
          return this.loginService.login(credentials).pipe(
            map((userData: any) => {
              this.storageService.storeUserData(userData);
              if (this.checkIfUserHasCompany(this.storageService.getUserData())) {
                this.router.navigate(['/account/select-company']);
              } else {
                this.storageService.setRegisteredUser(userData.id);
                this.router.navigate(['/register/finish']);
              }
              return loginUserSuccessAction({ data: userData });
            }),
            catchError(() => of(loginUserErrorAction()))
          );
        }
        return of(falseAction());
      })
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private loginService: LoginService,
    private storageService: StateStorageService,
    private router: Router
  ) {}

  checkIfUserHasCompany(userData: User | null): boolean {
    if (userData?.user_companies?.length) {
      return true;
    }
    return false;
  }
}
