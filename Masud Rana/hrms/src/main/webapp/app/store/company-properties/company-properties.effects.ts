import { Injectable } from '@angular/core';

import { select, Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { of } from 'rxjs';
import { mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';

import { UtilService } from 'app/shared/service/util.service';

import { AppState } from '..';
import { falseAction } from '../company-settings/company-settings.actions';
import { selecteBranches, selectSkills } from './company-properties.selectors';
import { loadBranchesAction, loadBranchesSuccessAction, loadSkillsAction, loadSkillsSuccessAction } from './company-properties.actions';

@Injectable()
export class CompanyPropertiesEffects {
  // LOAD => GET Requests
  loadCompanySkills$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadSkillsAction),
      withLatestFrom(this.store.pipe(select(selectSkills))),
      mergeMap((mergedResponse: any[]) => {
        if (mergedResponse[1] === null) {
          return this.utileService.getSkills().pipe(switchMap((skills: any) => [loadSkillsSuccessAction({ data: skills })]));
        }
        return of(falseAction());
      })
    )
  );

  loadCompanyBranches$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadBranchesAction),
      withLatestFrom(this.store.pipe(select(selecteBranches))),
      mergeMap((mergedResponse: any[]) => {
        if (mergedResponse[1] === null) {
          return this.utileService.getBranches().pipe(switchMap((branches: any) => [loadBranchesSuccessAction({ data: branches })]));
        }
        return of(falseAction());
      })
    )
  );

  constructor(private actions$: Actions, private store: Store<AppState>, private utileService: UtilService) {}
}
