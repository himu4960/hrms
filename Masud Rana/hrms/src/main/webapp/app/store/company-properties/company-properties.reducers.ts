import { createReducer, on } from '@ngrx/store';

import { loadBranchesSuccessAction, loadSkillsSuccessAction } from './company-properties.actions';

export const companyPropertiesFeatureKey = 'companyProperties';

export interface State {
  skills: any;
  branches: any;
}

export const initialState: State = {
  skills: null,
  branches: null,
};

export const reducer = createReducer(
  initialState,

  // LOAD SUCCESS ACTIONS
  on(loadSkillsSuccessAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      skills: data,
    };
  }),
  on(loadBranchesSuccessAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      branches: data,
    };
  })
);
