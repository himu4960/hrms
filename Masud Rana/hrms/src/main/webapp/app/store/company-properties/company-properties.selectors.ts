import { createFeatureSelector, createSelector } from '@ngrx/store';

import { IBranch } from 'app/entities/branch/branch.model';
import { ISkill } from 'app/entities/skill/skill.model';

import { companyPropertiesFeatureKey, State } from './company-properties.reducers';

export const selectCompanyPropertiesState = createFeatureSelector<State>(companyPropertiesFeatureKey);

export const selectSkills = createSelector(selectCompanyPropertiesState, (state: State) => state.skills as ISkill[]);

export const selecteBranches = createSelector(selectCompanyPropertiesState, (state: State) => state.branches as IBranch[]);
