import { createAction, props } from '@ngrx/store';

export const loadSkillsAction = createAction('[Any Component] Load Skills Action');
export const loadBranchesAction = createAction('[Any Component] Load Branches Action');

export const loadSkillsSuccessAction = createAction('[Company Peroperties Effect] Load Skills Success Action', props<{ data: any }>());
export const loadBranchesSuccessAction = createAction('[Company Peroperties Effect] Load Branches Success Action', props<{ data: any }>());
