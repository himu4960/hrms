import { createFeatureSelector, createSelector } from '@ngrx/store';

import { ApplicationSettings } from '../states/application-settings.interface';
import { LeaveAvailabilitySettings } from '../states/leave-availability-settings.interface';
import { ShiftPlanningSettings } from '../states/shift-planning-settings.interface';
import { TimeClockSettings } from '../states/time-clocks-settings.interface';

import { companySettingsFeatureKey, State } from './company-settings.reducers';

export const selectCompanySettingsState = createFeatureSelector<State>(companySettingsFeatureKey);

export const selectApplicationSettings = createSelector(
  selectCompanySettingsState,
  (state: State) => (state?.application as ApplicationSettings) ?? null
);

export const selectShiftPlanningSettings = createSelector(
  selectCompanySettingsState,
  (state: State) => state.shiftPlanning as ShiftPlanningSettings
);

export const selectTimeClockSettings = createSelector(selectCompanySettingsState, (state: State) => state.timeClock as TimeClockSettings);

export const selectLeaveAvailabilitySettings = createSelector(
  selectCompanySettingsState,
  (state: State) => state.leaveAvailability as LeaveAvailabilitySettings
);

export const selectAllSettings = createSelector(selectCompanySettingsState, (state: State) => state);
