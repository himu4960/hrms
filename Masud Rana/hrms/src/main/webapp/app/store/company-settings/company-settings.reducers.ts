import { createReducer, on } from '@ngrx/store';
import { logoutUserAction } from '../user/user.actions';
import {
  browserReloadCompanySettingsAction,
  loadApplicationSettingsSuccessAction,
  loadCompanySettingsSuccessAction,
  loadLeaveAvailablilitiySettingsSuccessAction,
  loadShiftPlanningSettingsSuccessAction,
  loadTimeClockSettingsSuccessAction,
  updateApplicationSettingsSuccessAction,
  updateLeaveAvailablilitiySettingsSuccessAction,
  updateShiftPlanningSettingsSuccessAction,
  updateTimeClockSettingsSuccessAction,
} from './company-settings.actions';

export const companySettingsFeatureKey = 'companySettings';

export interface State {
  application: any;
  shiftPlanning: any;
  timeClock: any;
  leaveAvailability: any;
}

export const initialState: State = {
  application: null,
  shiftPlanning: null,
  timeClock: null,
  leaveAvailability: null,
};

export const reducer = createReducer(
  initialState,

  // LOAD ACTIONS
  on(loadApplicationSettingsSuccessAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      application: data,
    };
  }),
  on(loadShiftPlanningSettingsSuccessAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      shiftPlanning: data,
    };
  }),
  on(loadTimeClockSettingsSuccessAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      timeClock: data,
    };
  }),
  on(loadLeaveAvailablilitiySettingsSuccessAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      leaveAvailability: data,
    };
  }),

  // UPLOAD ACTIONS
  on(updateApplicationSettingsSuccessAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      application: data,
    };
  }),
  on(updateShiftPlanningSettingsSuccessAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      shiftPlanning: data,
    };
  }),
  on(updateTimeClockSettingsSuccessAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      timeClock: data,
    };
  }),
  on(updateLeaveAvailablilitiySettingsSuccessAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      leaveAvailability: data,
    };
  }),

  on(loadCompanySettingsSuccessAction, browserReloadCompanySettingsAction, (state: State, action: any) => {
    const data = action?.data;
    return {
      ...state,
      application: data?.settings_application,
      shiftPlanning: data?.settings_shift_planning,
      timeClock: data?.settings_time_clock,
      leaveAvailability: data?.settings_leave_availability,
    };
  }),

  // LOGOUT ACTIONS
  on(logoutUserAction, (state: State) => {
    return {
      ...state,
      application: null,
      shiftPlanning: null,
      timeClock: null,
      leaveAvailability: null,
    };
  })
);
