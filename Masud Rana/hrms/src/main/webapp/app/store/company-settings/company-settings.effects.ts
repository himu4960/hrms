import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { select, Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { of } from 'rxjs';
import { catchError, map, mergeMap, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { SettingsService } from 'app/account/admin-settings/settings.service';

import { AppState } from '..';
import {
  falseAction,
  loadApplicationSettingsSuccessAction,
  loadCompanySelectAction,
  loadLeaveAvailablilitiySettingsSuccessAction,
  loadShiftPlanningSettingsSuccessAction,
  loadTimeClockSettingsSuccessAction,
  updateApplicationSettingsAction,
  updateApplicationSettingsSuccessAction,
  updateLeaveAvailablilitiySettingsAction,
  updateLeaveAvailablilitiySettingsSuccessAction,
  updateShiftPlanningSettingsAction,
  updateShiftPlanningSettingsSuccessAction,
  updateTimeClockSettingsAction,
  updateTimeClockSettingsSuccessAction,
} from './company-settings.actions';
import { browserReloadMenuAction } from '../menu/menu.actions';

import {
  selectApplicationSettings,
  selectLeaveAvailabilitySettings,
  selectShiftPlanningSettings,
  selectTimeClockSettings,
} from './company-settings.selectors';
import { selectMenu } from '../menu/menu.selectors';
import { jwtTokenAction } from '../user/user.actions';

import { ApplicationSettings } from '../states/application-settings.interface';
import { ShiftPlanningSettings } from '../states/shift-planning-settings.interface';
import { TimeClockSettings } from '../states/time-clocks-settings.interface';
import { LeaveAvailabilitySettings } from '../states/leave-availability-settings.interface';

import { AccountService } from 'app/core/auth/account.service';
import { StateStorageService } from 'app/core/auth/state-storage.service';

@Injectable()
export class CompanySettingsEffects {
  // LOAD => GET Requests
  loadCompanyDetails$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadCompanySelectAction),
      withLatestFrom(
        this.store.pipe(select(selectTimeClockSettings)),
        this.store.pipe(select(selectApplicationSettings)),
        this.store.pipe(select(selectShiftPlanningSettings)),
        this.store.pipe(select(selectMenu))
      ),
      mergeMap(([response, companyId]: any[]) => {
        if (companyId === null) {
          const { company_id, user_id } = response;
          return this.accountService.changeBusiness({ company_id, user_id }).pipe(
            map((res: any) => {
              const { settings } = res;
              this.storageService.setCompanySettings(settings);
              return { settings, token: res.token };
            }),
            switchMap(({ settings, token }: any) => [
              browserReloadMenuAction(),
              jwtTokenAction({ data: token }),
              loadTimeClockSettingsSuccessAction({ data: settings['settings_time_clock'] }),
              loadApplicationSettingsSuccessAction({ data: settings['settings_application'] }),
              loadShiftPlanningSettingsSuccessAction({ data: settings['settings_shift_planning'] }),
              loadLeaveAvailablilitiySettingsSuccessAction({ data: settings['settings_leave_availability'] }),
            ]),
            tap(res => {
              this.router.navigate(['/home']);
              return of(res);
            }),
            catchError(error => {
              return of(falseAction());
            })
          );
        }
        return of(falseAction());
      })
    )
  );

  // UPDATE => PUT Requests

  updateApplicationSettings$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateApplicationSettingsAction),
      withLatestFrom(this.store.pipe(select(selectApplicationSettings))),
      switchMap(([response, companyId]: any[]) => {
        const appSettingsToUpdate = { ...response?.data };

        return this.settingsService.updateApplicationSettings(appSettingsToUpdate).pipe(
          map(settingApplication => {
            const updatedApplicationSettings = { ...settingApplication };
            const settings = this.storageService.getCompanySettings();
            this.storageService.setCompanySettings({ ...settings, settings_application: updatedApplicationSettings });
            return updatedApplicationSettings as ApplicationSettings;
          }),
          switchMap((dataToStore: any) => [updateApplicationSettingsSuccessAction({ data: dataToStore }), browserReloadMenuAction()])
        );
      })
    )
  );

  updateShiftPlanningSettings$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateShiftPlanningSettingsAction),
      withLatestFrom(this.store.pipe(select(selectShiftPlanningSettings))),
      switchMap(([response, companyId]: any) => {
        const shiftPlanningSettingsToUpdate = { ...response?.data };
        return this.settingsService.updateShiftPlanningSettings(shiftPlanningSettingsToUpdate).pipe(
          map(() => {
            const updatedShiftPlanningSettings = { ...shiftPlanningSettingsToUpdate };
            const settings = this.storageService.getCompanySettings();
            this.storageService.setCompanySettings({
              ...settings,
              settings_shift_planning: updatedShiftPlanningSettings,
            });
            return updatedShiftPlanningSettings as ShiftPlanningSettings;
          }),
          switchMap((dataToStore: any) => [updateShiftPlanningSettingsSuccessAction({ data: dataToStore }), browserReloadMenuAction()])
        );
      })
    )
  );

  updateTimeClockSettings$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateTimeClockSettingsAction),
      withLatestFrom(this.store.pipe(select(selectTimeClockSettings))),
      switchMap(([response, companyId]: any[]) => {
        const timeClocksSettingsToUpdate = { ...response?.data };
        return this.settingsService.updateTimeClocksSettings(timeClocksSettingsToUpdate).pipe(
          map(() => {
            const updatedExistingSettings = { ...timeClocksSettingsToUpdate };
            const settings = this.storageService.getCompanySettings();
            this.storageService.setCompanySettings({ ...settings, settings_time_clock: updatedExistingSettings });
            return updatedExistingSettings as TimeClockSettings;
          }),
          switchMap((dataToStore: any) => {
            return [updateTimeClockSettingsSuccessAction({ data: dataToStore }), browserReloadMenuAction()];
          }),
          catchError(error => {
            return of(falseAction());
          })
        );
      })
    )
  );

  updateLeaveAvailabilitiesSettings$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateLeaveAvailablilitiySettingsAction),
      withLatestFrom(this.store.pipe(select(selectLeaveAvailabilitySettings))),
      switchMap(([response, companyId]: any[]) => {
        const leaveAvailabilitySettingsToUpdate = { ...response?.data };
        return this.settingsService.updateLeaveAvailabilitySettings(leaveAvailabilitySettingsToUpdate).pipe(
          map(() => {
            const updatedExistingSettings = { ...leaveAvailabilitySettingsToUpdate };
            const settings = this.storageService.getCompanySettings();
            this.storageService.setCompanySettings({ ...settings, settings_leave_availability: updatedExistingSettings });
            return updatedExistingSettings as LeaveAvailabilitySettings;
          }),
          switchMap((dataToStore: any) => {
            return [updateLeaveAvailablilitiySettingsSuccessAction({ data: dataToStore }), browserReloadMenuAction()];
          }),
          catchError(error => {
            return of(falseAction());
          })
        );
      })
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private settingsService: SettingsService,
    private accountService: AccountService,
    private storageService: StateStorageService,
    private router: Router
  ) {}
}
