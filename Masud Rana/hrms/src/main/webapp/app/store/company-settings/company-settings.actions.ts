import { createAction, props } from '@ngrx/store';
import { ICompanySelectionPayload } from './../../account/company-selection/company-selection.model';

/** ........... APPLICATION .......... */
export const loadApplicationSettingsSuccessAction = createAction('[Application Effect] Load Application Success', props<{ data: any }>());
export const updateApplicationSettingsAction = createAction('[Application Component] Update Application', props<{ data: any }>());
export const updateApplicationSettingsSuccessAction = createAction(
  '[Application Effect] Update Application Success',
  props<{ data: any }>()
);

/** ........... SHIFTPLANNING.......... */
export const loadShiftPlanningSettingsSuccessAction = createAction(
  '[ShiftPlanning Effect] Load ShiftPlanning Success',
  props<{ data: any }>()
);
export const updateShiftPlanningSettingsAction = createAction('[ShiftPlanning Component] Update ShiftPlanning', props<{ data: any }>());
export const updateShiftPlanningSettingsSuccessAction = createAction(
  '[ShiftPlanning Effect] Update ShiftPlanning Success',
  props<{ data: any }>()
);

/** ........... COMPANYSELECTION .......... */
export const loadCompanySelectAction = createAction('[Company Select Component] Load Company Details', props<ICompanySelectionPayload>());
export const loadCompanyDetailsSuccess = createAction('[Company Select Effect] Load Company Success Details', props<{ data: any }>());
export const loadCompanySettingsSuccessAction = createAction(
  '[Company Settings Effect] Load Company Settings Success',
  props<{ data: any }>()
);

/** ........... TIMECLOCKS .......... */
export const loadTimeClockSettingsSuccessAction = createAction('[TimeClocks Effect] Load TimeClocks Success', props<{ data: any }>());
export const updateTimeClockSettingsAction = createAction('[TimeClocks Component] Update TimeClocks', props<{ data: any }>());
export const updateTimeClockSettingsSuccessAction = createAction('[TimeClocks Effect] Update TimeClocks Success', props<{ data: any }>());

export const browserReloadCompanySettingsAction = createAction(
  '[Main Component] Browser Reload Company Settings Action',
  props<{ data: any }>()
);

/** ........... LEAVE AVAILABILITY .......... */
export const loadLeaveAvailablilitiySettingsSuccessAction = createAction(
  '[Leave Availability Effect] Load Leave Availability',
  props<{ data: any }>()
);
export const updateLeaveAvailablilitiySettingsAction = createAction(
  '[Leave Availability Component] Update Leave Availability',
  props<{ data: any }>()
);
export const updateLeaveAvailablilitiySettingsSuccessAction = createAction(
  '[Leave Availability Effect] Update Leave Availability Success',
  props<{ data: any }>()
);

/** ...........  FALSE ACTION .......... */
export const falseAction = createAction('[False Component] False Action ');
