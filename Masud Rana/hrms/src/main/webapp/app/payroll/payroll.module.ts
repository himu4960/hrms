import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';

import { PayrollComponent } from './payroll.component';

@NgModule({
  declarations: [PayrollComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: PayrollComponent,
        canActivate: [AuthGuard],
      },
    ]),
  ],
})
export class PayrollModule {}
