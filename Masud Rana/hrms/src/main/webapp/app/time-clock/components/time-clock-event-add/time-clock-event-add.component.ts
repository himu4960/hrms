import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AppState } from 'app/store';
import { Store } from '@ngrx/store';

import { UserCompany } from 'app/store/states/user.interface';
import { TimeClockSettings } from '../../../store/states/time-clocks-settings.interface';

import { selectCurrentCompany } from 'app/store/user/user.selectors';
import { selectTimeClockSettings } from 'app/store/company-settings/company-settings.selectors';

import { ITimeClock, ITimeClockFilterParams } from '../../models/time-clock.model';
import { ITimeClockEvent } from '../../models/time-clock-event.model';
import { IDesignation } from 'app/entities/designation/designation.model';

import { ETimeClockEventType } from '../../time-clock-event-type.enum';

import { TimeClockService } from '../../service/time-clock.service';
import { UtilService } from 'app/shared/service/util.service';

@Component({
  selector: 'hrms-time-clock-event-add',
  templateUrl: './time-clock-event-add.component.html',
  styleUrls: ['./time-clock-event-add.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class TimeClockEventAddComponent implements OnInit, OnDestroy {
  @Input() timeClock?: ITimeClock;
  @Input() isEditMode!: boolean;

  @Output() timeClockChanged = new EventEmitter<ITimeClock>();
  @Output() timeClockEditEvent = new EventEmitter<ITimeClockEvent>();
  @Output() breakStarted = new EventEmitter<boolean>();

  timeClockSettings!: TimeClockSettings;

  isOpenPositionField = false;
  isBreakStart = false;

  filterParams?: ITimeClockFilterParams;

  lastTimeClockEvent?: ITimeClockEvent;
  designations: IDesignation[] = [];
  timeClockBreakForm = this.fb.group({
    id: [],
    break_start_time: [Date.now(), [Validators.required]],
    created_at: [Date.now(), [Validators.required]],
    time_clock_id: [this.timeClock?.id, [Validators.required]],
  });
  timeClockNoteForm = this.fb.group({
    id: [],
    note: [null, [Validators.required]],
    time_clock_id: [this.timeClock?.id, [Validators.required]],
    created_at: [Date.now(), [Validators.required]],
  });
  timeClockPositionForm = this.fb.group({
    id: [],
    new_position_id: [this.designations[0], [Validators.required]],
    created_at: [new Date().toISOString(), [Validators.required]],
  });

  private subscriptions: Subscription = new Subscription();

  constructor(
    private store: Store<AppState>,
    protected fb: FormBuilder,
    private timeClockService: TimeClockService,
    private utilService: UtilService
  ) {}

  ngOnInit(): void {
    this.subscriptions.add(
      this.store.select(selectTimeClockSettings).subscribe((res: TimeClockSettings) => {
        if (res !== null) {
          this.timeClockSettings = res;
        }
      })
    );

    this.subscriptions.add(
      this.store
        .select(selectCurrentCompany)
        .pipe(
          switchMap((res: UserCompany) => {
            if (res !== null) {
              this.filterParams = {
                branch_id: res.employee.branch_id,
                employee_id: res.employee.id,
              };
            }
            return this.utilService.getDesignationsByEmployeeId(res.employee.id);
          })
        )
        .subscribe(designations => {
          if (designations && designations.length > 0) {
            this.designations = designations;
            this.filterParams = {
              ...this.filterParams,
              designation_id: designations[0].id,
            };
          }
        })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  public addNote(): void {
    if (this.timeClockNoteForm.value.note && this.timeClock?.id !== undefined) {
      if (this.isEditMode) {
        this.timeClockEditEvent.emit({
          ...this.timeClockNoteForm.value,
          time_clock_id: this.timeClock?.id,
          event_type: ETimeClockEventType.NOTE,
          created_at: new Date().toISOString(),
        });
      } else {
        this.timeClockService.addNote({ ...this.timeClockNoteForm.value, time_clock_id: this.timeClock?.id }).subscribe(() => {
          this.timeClockChanged.emit();
        });
      }
      this.timeClockNoteForm.reset();
    }
  }

  public breakStart(): void {
    if (this.timeClock?.id !== undefined) {
      if (this.isEditMode) {
        this.timeClockEditEvent.emit({
          ...this.timeClockBreakForm.value,
          event_type: ETimeClockEventType.BREAK,
          break_start_time: Date.now(),
          break_end_time: Date.now(),
        });
      } else {
        this.timeClockService.breakStart(this.timeClock.id).subscribe(res => {
          if (res?.body?.break_start_time) {
            this.isBreakStart = true;
            this.breakStarted.emit(this.isBreakStart);
            this.lastTimeClockEvent = res?.body;
            this.timeClockChanged.emit();
          }
        });
      }
      this.timeClockBreakForm.reset();
    }
  }

  public breakOut(lastTimeClockEvent?: ITimeClockEvent): void {
    if (lastTimeClockEvent?.id) {
      this.timeClockService.breakOut(lastTimeClockEvent.id).subscribe(res => {
        if (res?.body?.break_end_time) {
          this.isBreakStart = false;
          this.breakStarted.emit(this.isBreakStart);
          this.lastTimeClockEvent = res?.body;
          this.timeClockChanged.emit();
        }
      });
    }
  }

  public addPosition(): void {
    if (this.timeClock?.id) {
      if (this.isEditMode) {
        this.timeClockEditEvent.emit({
          ...this.timeClockPositionForm.value,
          time_clock_id: this.timeClock.id,
          event_type: ETimeClockEventType.POSITION,
          new_position_id: Number(this.timeClockPositionForm.value.new_position_id),
        });
        this.openPositionField(false);
      } else {
        this.timeClockService
          .addPosition({
            new_position_id: Number(this.timeClockPositionForm.value.new_position_id),
            time_clock_id: this.timeClock?.id,
            created_at: Date.now(),
          })
          .subscribe(() => {
            this.timeClockChanged.emit();
            this.openPositionField(false);
          });
      }
    }
  }

  public openPositionField(isOpen: boolean): void {
    this.isOpenPositionField = isOpen;
  }

  public loadTimeClockEvents(time_clock_events: any[]): void {
    time_clock_events.forEach(event => {
      if (event.event_type === ETimeClockEventType.BREAK) {
        if (!event.break_end_time) {
          this.lastTimeClockEvent = event;
          this.isBreakStart = true;
          this.breakStarted.emit(this.isBreakStart);
        }
      }
    });
  }
}
