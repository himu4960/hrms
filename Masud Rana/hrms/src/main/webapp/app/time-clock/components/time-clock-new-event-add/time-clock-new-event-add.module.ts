import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { TimeClockNewEventAddComponent } from './time-clock-new-event-add.component';

@NgModule({
  imports: [SharedModule],
  declarations: [TimeClockNewEventAddComponent],
  exports: [TimeClockNewEventAddComponent],
})
export class TimeClockNewEventAddModule {}
