import { Component } from '@angular/core';

@Component({
  selector: 'hrms-time-clock-statistics',
  templateUrl: './time-clock-statistics.component.html',
  styleUrls: ['./time-clock-statistics.component.scss'],
})
export class TimeClockStatisticsComponent {}
