import { Component, ElementRef, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { AppState } from 'app/store';
import { Subscription } from 'rxjs';
import * as XLSX from 'xlsx';
import * as dayjs from 'dayjs';
import * as customParseFormat from 'dayjs/plugin/customParseFormat';
dayjs.extend(customParseFormat);

import { UserCompany } from './../../../store/states/user.interface';

import { selectCurrentCompany } from 'app/store/user/user.selectors';

import { IImportTimeSheet } from '../../models/time-clock.model';
import { IEmployee } from './../../../employee/models/employee.model';

import { UtilService } from '../../../shared/service/util.service';
import { ToastService } from '../../../shared/components/toast/toast.service';
import { TimeClockService } from '../../service/time-clock.service';
import { EmployeeService } from './../../../employee/service/employee.service';
import { PermissionService } from 'app/shared/service/permission.service';

@Component({
  selector: 'hrms-time-sheet-import',
  templateUrl: './time-sheet-import.component.html',
  styleUrls: ['./time-sheet-import.component.scss'],
})
export class TimeSheetImportComponent {
  @ViewChild('uploadTimeSheetFile', { static: false }) fileUploader?: ElementRef;
  isEmployee = true;
  employees: IEmployee[] = [];
  employee?: IEmployee;
  timeClocks: any = [];
  file: any;
  fileName = 'No file Chosen';

  private subscriptions: Subscription = new Subscription();

  constructor(
    private store: Store<AppState>,
    private utilService: UtilService,
    private toastService: ToastService,
    private timeClockService: TimeClockService,
    private employeeService: EmployeeService,
    private permissionService: PermissionService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.isEmployee = this.permissionService.isEmployee();

    if (!this.isEmployee) {
      this.employeeService.findAll().subscribe(res => {
        this.employees = res.body ? res.body : [];
      });
    } else {
      this.subscriptions.add(
        this.store.select(selectCurrentCompany).subscribe((res: UserCompany) => {
          if (res !== null) {
            if (res?.employee !== undefined) {
              this.employee = res.employee;
            }
          }
        })
      );
    }
  }

  public onFileChange(event: any): void {
    const files = event.target.files;
    const name = files[0].name;
    this.fileName = name;
    if (files.length) {
      this.file = files[0];
    }
  }

  public uploadTimeSheet(): void {
    if (this.file) {
      let errorCount = 0;
      let findDesignation: any = null;
      const reader = new FileReader();
      reader.onload = (event: any) => {
        const wb = XLSX.read(event.target.result);
        const sheets = wb.SheetNames;

        if (sheets.length) {
          const rows = XLSX.utils.sheet_to_json(wb.Sheets[sheets[0]], {
            raw: false,
          });

          this.timeClocks = rows.map((row: any) => {
            const { clockin, clockout, date, email, position, notes } = row;

            const formattedClockIn = this.utilService.formatTimeTo24Hours(clockin);
            const formattedClockOut = this.utilService.formatTimeTo24Hours(clockout);
            const formattedDate = this.utilService.format(date);

            if (!this.isEmployee) {
              const findEmployee = this.employees.find(employee => employee.email === email);

              if (findEmployee) {
                findDesignation = findEmployee.employee_designations?.find(
                  (designation: any) => designation?.designation?.name?.toLowerCase() === position?.toLowerCase()
                );
              } else {
                errorCount++;
              }
            } else {
              if (this.employee) {
                findDesignation = this.employee.employee_designations?.find(
                  (designation: any) => designation?.designation?.name?.toLowerCase() === position?.toLowerCase()
                );
              } else {
                errorCount++;
              }
            }

            if (!formattedClockIn || !formattedClockOut || !formattedDate || !findDesignation) {
              errorCount++;
            }

            const newTimeClock: IImportTimeSheet = {
              clock_start_time: new Date(this.utilService.combineDateAndTime(formattedDate, formattedClockIn)).getTime(),
              clock_end_time: new Date(this.utilService.combineDateAndTime(formattedDate, formattedClockOut)).getTime(),
              notes,
              email,
              position,
            };
            return newTimeClock;
          });
        }

        if (errorCount > 0) {
          this.toastService.showDangerToast(this.translateService.instant('timeClock.upload.validation.valid'));
          this.timeClocks = [];
        } else {
          this.timeClockService.timeSheetImport(this.timeClocks).subscribe(() => {
            this.toastService.showSuccessToast(this.translateService.instant('timeClock.upload.validation.success'));
            this.fileName = 'No file Chosen';
            if (this.fileUploader) {
              this.fileUploader.nativeElement.value = '';
              this.timeClocks = [];
              this.file = null;
            }
          });
        }
      };
      reader.readAsArrayBuffer(this.file);
    } else {
      this.toastService.showDangerToast(this.translateService.instant('timeClock.upload.validation.notFound'));
    }
  }
}
