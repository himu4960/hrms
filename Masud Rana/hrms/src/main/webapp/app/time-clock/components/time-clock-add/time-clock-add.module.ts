import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { TimeClockEventAddModule } from '../time-clock-event-add/time-clock-event-add.module';
import { TimeClockNewEventAddModule } from '../time-clock-new-event-add/time-clock-new-event-add.module';

import { TimeClockAddComponent } from './time-clock-add.component';

@NgModule({
  imports: [SharedModule, TimeClockNewEventAddModule, TimeClockEventAddModule],
  declarations: [TimeClockAddComponent],
  exports: [TimeClockAddComponent],
})
export class TimeClockAddModule {}
