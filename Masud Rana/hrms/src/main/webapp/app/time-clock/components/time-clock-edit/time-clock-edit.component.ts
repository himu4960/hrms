import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'app/store';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { finalize } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';

import { EModalReasonType } from './../../../shared/enum/modal-reason-type.enum';

import { UserCompany } from '../../../store/states/user.interface';
import { TimeClockSettings } from './../../../store/states/time-clocks-settings.interface';
import { ApplicationSettings } from 'app/store/states/application-settings.interface';

import { ITimeClock, ITimeClockForm, ITimeClockFilterParams } from './../../models/time-clock.model';
import { IDesignation } from '../../../entities/designation/designation.model';
import { ITimeClockEvent, ITimeClockEventForm } from './../../models/time-clock-event.model';
import { ITimeZone } from 'app/entities/time-zone/time-zone.model';

import { selectCurrentCompany } from '../../../store/user/user.selectors';
import { selectApplicationSettings, selectTimeClockSettings } from './../../../store/company-settings/company-settings.selectors';

import { ETimeClockEventType } from './../../time-clock-event-type.enum';
import { ETimeType } from './../../../shared/pipes/time/time-type.enum';

import { UtilService } from './../../../shared/service/util.service';
import { TimeClockService } from './../../service/time-clock.service';
import { PermissionService } from './../../../shared/service/permission.service';

import { timeClockValidator } from '../../../shared/validators/time-clock.validator';
import { timeClockEventValidator } from '../../../shared/validators/time-clock-event.validator';

@Component({
  templateUrl: './time-clock-edit.component.html',
  styleUrls: ['./time-clock-edit.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TimeClockEditComponent implements OnInit {
  errorMessage = null;
  isError = false;

  ETimeClockEventType = ETimeClockEventType;
  ETimeType = ETimeType;

  isEmployee = true;

  timeClockSettings!: TimeClockSettings;
  timeClock!: ITimeClock;
  timeZone!: ITimeZone;
  designations: IDesignation[] = [];
  isEditMode = true;

  isLoading = false;

  filterParams?: ITimeClockFilterParams;

  editTimeClockForm = this.fb.group(
    {
      id: [],
      clock_start_time: [null, [Validators.required, Validators.maxLength(255)]],
      clock_start_date: [null, [Validators.required, Validators.maxLength(255)]],

      clock_end_time: [null, [Validators.required, Validators.maxLength(255)]],
      clock_end_date: [null, [Validators.required, Validators.maxLength(255)]],
      notes: [null],
      time_clock_events: this.fb.array([]),
    },
    {
      validators: timeClockValidator(),
    }
  );

  public clock_end_time_control = this.editTimeClockForm.get('clock_end_time');
  public notes_control = this.editTimeClockForm.get('notes');

  private subscriptions: Subscription = new Subscription();

  constructor(
    private store: Store<AppState>,
    protected fb: FormBuilder,
    public activeModal: NgbActiveModal,
    private utilService: UtilService,
    private timeClockService: TimeClockService,
    private permissionService: PermissionService
  ) {}

  ngOnInit(): void {
    this.isEmployee = this.permissionService?.isEmployee();
    this.getSettingsFromStore();
    this.updateForm();
    this.checkTimeClockSettingsAndAddValidator();
  }

  get timeClockEvents(): FormArray {
    return this.editTimeClockForm.get('time_clock_events') as FormArray;
  }

  public checkTimeClockSettingsAndAddValidator(): void {
    if (!this.timeClockSettings?.timesheet_employee_can_edit_without_reason) {
      this.notes_control?.addValidators(Validators.required);
    } else {
      this.notes_control?.clearValidators();
    }
  }

  public timeClockEventForm(values: any): FormGroup {
    return this.fb.group(
      {
        id: [values?.id ?? null],
        time_clock_id: [values?.time_clock_id ?? null],
        event_type: [values?.event_type ?? null, [Validators.required]],
        break_start_time: [values?.break_start_time ?? null],
        break_end_time: [values?.break_end_time ?? null],
        break_start_date: [values?.break_start_date ?? null],
        break_end_date: [values?.break_end_date ?? null],
        total_break_duration: [values?.total_break_duration ?? null],
        new_position_id: [values?.new_designation?.id ?? null],
        note: [values?.note ?? null],
        created_at_time: [values?.created_at_time ?? null],
        created_at_date: [values?.created_at_date ?? null],
      },
      {
        validators: timeClockEventValidator(),
      }
    );
  }

  public pushTimeClockEvent(timeClockEvent: ITimeClockEvent): void {
    const { break_start_time, break_end_time, new_position_id, created_at, note, event_type } = timeClockEvent;
    const breakStart: string = break_start_time ? this.utilService.convertTimeStampToLocalTime(break_start_time, this.timeZone?.abbr) : '';
    const breakEnd: string = break_end_time ? this.utilService.convertTimeStampToLocalTime(break_end_time, this.timeZone?.abbr) : '';

    this.timeClockEvents.push(
      this.timeClockEventForm({
        event_type,
        new_designation: { id: new_position_id },
        note,

        break_start_time: this.utilService.formatTime(breakStart),
        break_start_date: this.utilService.format(breakStart),

        break_end_time: this.utilService.formatTime(breakEnd),
        break_end_date: this.utilService.format(breakEnd),

        created_at_time: this.utilService.formatTime(created_at),
        created_at_date: this.utilService.format(created_at),
      })
    );
  }

  public update(): void {
    const newTimeClock = this.timeClockMapping(this.editTimeClockForm.value);
    this.timeClockService.editMode({ ...newTimeClock, ...this.filterParams }).subscribe(
      () => {
        this.errorMessage = null;
        this.activeModal.close(EModalReasonType.UPDATED);
      },
      (err: HttpErrorResponse) => {
        this.errorMessage = err.error.message;
      }
    );
  }

  public cancel(): void {
    this.activeModal.dismiss();
  }

  public removeTimeClockEvent(i: number): void {
    this.timeClockEvents.removeAt(i);
  }

  public deleteTimeClock(id?: number): void {
    if (id !== undefined) {
      this.timeClockService.delete(id).subscribe(() => {
        this.activeModal.close(EModalReasonType.DELETED);
      });
    }
  }

  protected timeClockMapping(timeClockForm: ITimeClockForm): ITimeClock {
    const { clock_start_date, clock_end_time, clock_end_date, clock_start_time, time_clock_events, id, notes } = timeClockForm;
    const time_clock_id = id;
    const newTimeClockEvents = time_clock_events?.map((timeClockEvent: ITimeClockEventForm) => {
      const {
        event_type,
        break_start_time,
        break_start_date,
        break_end_time,
        break_end_date,
        created_at_date,
        created_at_time,
        note,
        new_position_id,
      } = timeClockEvent;
      return {
        id: timeClockEvent.id,
        event_type,
        time_clock_id,
        break_start_time:
          break_start_date && break_start_time
            ? new Date(this.utilService.combineDateAndTime(break_start_date.toString(), break_start_time?.toString())).getTime()
            : null,
        break_end_time:
          break_end_date && break_end_time
            ? new Date(this.utilService.combineDateAndTime(break_end_date.toString(), break_end_time?.toString())).getTime()
            : null,
        created_at: new Date(this.utilService.combineDateAndTime(created_at_date, created_at_time)).getTime(),
        note,
        new_position_id,
      };
    });
    return {
      id,
      clock_start_time:
        clock_start_date && clock_start_time
          ? new Date(this.utilService.combineDateAndTime(clock_start_date.toString(), clock_start_time?.toString())).getTime()
          : null,
      clock_end_time:
        clock_end_date && clock_end_time
          ? new Date(this.utilService.combineDateAndTime(clock_end_date.toString(), clock_end_time?.toString())).getTime()
          : null,
      time_clock_events: newTimeClockEvents ?? [],
      notes,
    };
  }

  protected getSettingsFromStore(): void {
    this.subscriptions.add(
      this.store.select(selectTimeClockSettings).subscribe((res: TimeClockSettings) => {
        if (res !== null) {
          this.timeClockSettings = res;
        }
      })
    );

    this.subscriptions.add(
      this.store.select(selectCurrentCompany).subscribe((res: UserCompany) => {
        if (res !== null) {
          if (res?.employee !== undefined) {
            this.filterParams = {
              branch_id: res.employee.branch_id,
              employee_id: res.employee.id,
            };
          }
        }
      })
    );

    this.subscriptions.add(
      this.store.select(selectApplicationSettings).subscribe((settings: ApplicationSettings) => {
        if (settings) {
          this.timeZone = settings.time_zone;
        }
      })
    );
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITimeClock>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isLoading = false;
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isLoading = false;
  }

  protected updateForm(): void {
    if (this?.timeClock && this.timeClock?.time_clock_events !== undefined) {
      const clockStart: any = this.timeClock.clock_start_time
        ? this.utilService.convertTimeStampToLocalTime(this.timeClock.clock_start_time, this.timeZone?.abbr)
        : '';
      const clockEnd: any = this.timeClock.clock_end_time
        ? this.utilService.convertTimeStampToLocalTime(this.timeClock.clock_end_time, this.timeZone?.abbr)
        : '';

      this.timeClock.time_clock_events?.map((event: ITimeClockEvent) => {
        const { id, event_type, note, new_designation, break_start_time, break_end_time, total_break_duration, created_at } = event;

        const breakStart: string = break_start_time
          ? this.utilService.convertTimeStampToLocalTime(break_start_time, this.timeZone?.abbr)
          : '';
        const breakEnd: string = break_end_time ? this.utilService.convertTimeStampToLocalTime(break_end_time, this.timeZone?.abbr) : '';

        this.timeClockEvents.push(
          this.timeClockEventForm({
            id,
            time_clock_id: this.timeClock.id,
            event_type,
            note,
            new_designation,
            total_break_duration,

            break_start_time: this.utilService.formatTime(breakStart),
            break_start_date: this.utilService.format(breakStart),

            break_end_time: this.utilService.formatTime(breakEnd),
            break_end_date: this.utilService.format(breakEnd),

            created_at_time: this.utilService.formatTime(created_at),
            created_at_date: this.utilService.format(created_at),
          })
        );
      });
      this.editTimeClockForm.patchValue({
        id: this.timeClock.id,
        clock_start_time: this.utilService.formatTime(clockStart),
        clock_start_date: this.utilService.format(clockStart),

        clock_end_time: this.utilService.formatTime(clockEnd),
        clock_end_date: this.utilService.format(clockEnd),

        notes: this.timeClock?.notes,
      });
    }
  }
}
