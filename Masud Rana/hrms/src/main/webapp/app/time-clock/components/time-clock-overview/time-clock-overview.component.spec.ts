import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import { SharedModule } from 'app/shared/shared.module';

import { TimeClockService } from '../../service/time-clock.service';
import { PermissionService } from '../../../shared/service/permission.service';

import { TimeClockOverviewComponent } from './time-clock-overview.component';

describe('TimeClockOverviewComponent', () => {
  let component: TimeClockOverviewComponent;
  let fixture: ComponentFixture<TimeClockOverviewComponent>;
  let service: TimeClockService;
  let fakeService: {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, SharedModule, StoreModule.forRoot({})],
      declarations: [TimeClockOverviewComponent],
      providers: [
        FormBuilder,
        provideMockStore(),
        { provide: PermissionService, useValue: fakeService },
        { provide: TranslateService, useValue: fakeService },
      ],
    })
      .overrideTemplate(TimeClockOverviewComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeClockOverviewComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(TimeClockService);

    const headers = new HttpHeaders().append('link', 'link;link');
    spyOn(service, 'query').and.returnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
