import { FormBuilder } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { SocketIoModule } from 'ngx-socket-io';
import { TranslateService } from '@ngx-translate/core';

import { SharedModule } from '../../../shared/shared.module';
import { TimeClockEventAddModule } from '../time-clock-event-add/time-clock-event-add.module';

import { TimeClockAddComponent } from './time-clock-add.component';

import { PermissionService } from './../../../shared/service/permission.service';
import { EmployeeOnlineService } from '../../../shared/components/socket/employee-online.service';

describe('TimeClockAddComponent', () => {
  let component: TimeClockAddComponent;
  let fixture: ComponentFixture<TimeClockAddComponent>;
  let fakeService: {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, SharedModule, TimeClockEventAddModule],
      providers: [
        FormBuilder,
        provideMockStore(),
        { provide: PermissionService, useValue: fakeService },
        { provide: EmployeeOnlineService, useValue: fakeService },
        { provide: TranslateService, useValue: fakeService },
      ],
      declarations: [TimeClockAddComponent],
    })
      .overrideTemplate(TimeClockAddComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeClockAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
