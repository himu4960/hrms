import { StoreModule } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

import { SharedModule } from 'app/shared/shared.module';

import { TimeClockEventAddComponent } from './time-clock-event-add.component';

describe('TimeClockAddComponent', () => {
  let component: TimeClockEventAddComponent;
  let fixture: ComponentFixture<TimeClockEventAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, SharedModule, CommonModule, StoreModule.forRoot({}), TranslateModule.forRoot({})],
      providers: [FormBuilder],
      declarations: [TimeClockEventAddComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeClockEventAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
