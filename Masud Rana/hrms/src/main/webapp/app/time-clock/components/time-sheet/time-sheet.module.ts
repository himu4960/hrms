import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { TimeClockEditModule } from '../time-clock-edit/time-clock-edit.module';

import { TimeSheetComponent } from './time-sheet.component';

@NgModule({
  imports: [SharedModule, TimeClockEditModule],
  declarations: [TimeSheetComponent],
  exports: [TimeSheetComponent],
})
export class TimeSheetModule {}
