import { FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';
import { provideMockStore } from '@ngrx/store/testing';
import { TranslateService } from '@ngx-translate/core';

import { ITimeClock } from './../../models/time-clock.model';

import { TimeClockEditComponent } from './time-clock-edit.component';

import { PermissionService } from './../../../shared/service/permission.service';

describe('TimeClockEditComponent', () => {
  let component: TimeClockEditComponent;
  let fixture: ComponentFixture<TimeClockEditComponent>;
  let fakeService: {};

  const mockTimeClock: ITimeClock = {
    clock_start_time: Date.now(),
    time_clock_events: [],
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CommonModule, StoreModule.forRoot({})],
      declarations: [TimeClockEditComponent],
      providers: [
        FormBuilder,
        NgbActiveModal,
        provideMockStore(),
        { provide: PermissionService, useValue: fakeService },
        { provide: TranslateService, useValue: fakeService },
      ],
    })
      .overrideTemplate(TimeClockEditComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeClockEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    TestBed.inject(NgbActiveModal);
  });

  it('should create', () => {
    component.timeClock = mockTimeClock;
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });
});
