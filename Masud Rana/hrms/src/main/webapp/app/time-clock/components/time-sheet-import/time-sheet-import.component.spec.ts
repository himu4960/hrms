import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { provideMockStore } from '@ngrx/store/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

import { SharedModule } from 'app/shared/shared.module';

import { TimeSheetImportComponent } from './time-sheet-import.component';

import { PermissionService } from './../../../shared/service/permission.service';
import { TimeClockService } from './../../service/time-clock.service';

describe('TimeSheetComponent', () => {
  let component: TimeSheetImportComponent;
  let fixture: ComponentFixture<TimeSheetImportComponent>;
  let service: TimeClockService;
  const mockTranslateService = {
    get: (key: any) => of(key),
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, SharedModule, TranslateModule.forRoot()],
      providers: [
        provideMockStore(),
        {
          provide: PermissionService,
          useValue: {
            isEmployee: jasmine.createSpy('isEmployee'),
          },
        },
        { provide: TranslateService, useValue: mockTranslateService },
      ],
      declarations: [TimeSheetImportComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeSheetImportComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(TimeClockService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
