import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { TimeClockEventAddModule } from '../time-clock-event-add/time-clock-event-add.module';

import { TimeClockEditComponent } from '../time-clock-edit/time-clock-edit.component';

@NgModule({
  imports: [SharedModule, TimeClockEventAddModule],
  declarations: [TimeClockEditComponent],
  entryComponents: [TimeClockEditComponent],
  exports: [TimeClockEditComponent],
})
export class TimeClockEditModule {}
