import { switchMap } from 'rxjs/operators';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { AppState } from 'app/store';
import { Store } from '@ngrx/store';
import { of, Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as dayjs from 'dayjs';
import * as weekday from 'dayjs/plugin/weekday';
dayjs.extend(weekday);

import { selectCurrentCompany } from '../../../store/user/user.selectors';
import { selectTimeClockSettings } from './../../../store/company-settings/company-settings.selectors';

import { UserCompany } from '../../../store/states/user.interface';
import { TimeClockSettings } from './../../../store/states/time-clocks-settings.interface';

import { ITimeSheet, ITimeSheetFilter, ITimeClockFilterParams } from './../../models/time-clock.model';
import { IDesignation } from './../../../entities/designation/designation.model';
import { ITimeZone } from '../../../entities/time-zone/time-zone.model';
import { Pagination } from 'app/core/request/request.model';

import { TimeClockEditComponent } from './../time-clock-edit/time-clock-edit.component';

import { ETimeSheetFilterType, ETimeSheetApproveType, ETimeClockEventType } from './../../time-clock-event-type.enum';
import { EModalReasonType } from './../../../shared/enum/modal-reason-type.enum';
import { EPaginationPageSize } from './../../../shared/enum/pagination.enum';
import { ETimeType } from './../../../shared/pipes/time/time-type.enum';

import { TimeClockService } from './../../service/time-clock.service';
import { UtilService } from '../../../shared/service/util.service';
import { PermissionService } from './../../../shared/service/permission.service';

@Component({
  selector: 'hrms-time-sheet',
  templateUrl: './time-sheet.component.html',
  styleUrls: ['./time-sheet.component.scss'],
})
export class TimeSheetComponent implements OnInit, OnDestroy {
  isLoading = false;
  isEmployee = true;
  employeeCanViewNotes = false;
  timeClockSettings?: TimeClockSettings;
  pageSize = EPaginationPageSize.PAGE_SIZE_20;
  page = 0;
  predicate!: string;
  ascending!: boolean;
  @Input() totalItems = 0;
  @Input() timeSheet?: ITimeSheet;
  @Input() timeZone!: ITimeZone;
  @Input() designations?: IDesignation[];
  @Output() timeSheetLoaded = new EventEmitter<any>();

  ETimeSheetFilterType = ETimeSheetFilterType;
  ETimeSheetApproveType = ETimeSheetApproveType;
  ETimeClockEventType = ETimeClockEventType;
  ETimeType = ETimeType;

  filterTimeSheetForm = this.fb.group({
    filter_date: [ETimeSheetFilterType.TODAY, [Validators.required]],
    is_approved: [ETimeSheetApproveType.ALL, [Validators.required]],
    page_size: [EPaginationPageSize.PAGE_SIZE_20, [Validators.required]],
  });

  dateForm = this.fb.group({
    selected_date: [null, [Validators.required]],
  });

  filterParams?: ITimeClockFilterParams;

  get ePaginationPageSize(): any {
    return EPaginationPageSize;
  }

  private subscriptions: Subscription = new Subscription();

  constructor(
    private store: Store<AppState>,
    protected fb: FormBuilder,
    private timeClockService: TimeClockService,
    private utilService: UtilService,
    protected modalService: NgbModal,
    protected permissionService: PermissionService
  ) {}

  ngOnInit(): void {
    this.isEmployee = this.permissionService?.isEmployee();
    this.getTimeClockSettingsFromStore();
    this.getFilterParamsFromStore();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  transition(page: number): void {
    this.page = page - 1;
    this.filterTimeSheet({
      page: this.page,
      size: this.pageSize,
    });
  }

  public changePageSize(): void {
    const size = this.filterTimeSheetForm.get('page_size')!.value;
    this.pageSize = Number(size) ?? 0;
    this.filterTimeSheet({
      page: 0,
      size: this.pageSize,
    });
  }

  public filterTimeSheet(req?: Pagination): void {
    this.isLoading = true;
    const filter_date = Number(this.filterTimeSheetForm.value.filter_date);
    const is_approved = Number(this.filterTimeSheetForm.value.is_approved);

    let options: ITimeSheetFilter = {
      ...this.filterParams,
    };

    if (is_approved === ETimeSheetApproveType.APPROVE) {
      options = {
        ...options,
        is_approved: true,
      };
    }

    if (is_approved === ETimeSheetApproveType.UNAPPROVE) {
      options = {
        ...options,
        is_approved: false,
      };
    }

    if (filter_date === ETimeSheetFilterType.THIS_WEEK) {
      options = { ...options, filter_start_date: this.utilService.getThisWeekDate(), filter_end_date: new Date(Date.now()).getTime() };
    } else if (filter_date === ETimeSheetFilterType.LAST_WEEK) {
      options = {
        ...options,
        filter_start_date: this.utilService.getLastWeekDate().start_date,
        filter_end_date: this.utilService.getLastWeekDate().end_date,
      };
    } else if (filter_date === ETimeSheetFilterType.ALL_TIME) {
      options = { ...options };
    } else if (filter_date === ETimeSheetFilterType.TODAY || filter_date === ETimeSheetFilterType.YESTERDAY) {
      options = {
        ...options,
        filter_start_date: this.utilService.getLastDayDate(filter_date),
        filter_end_date: this.utilService.getLastDayDate(filter_date),
      };
    } else {
      options = {
        ...options,
        filter_start_date: this.utilService.getLastDayDate(filter_date),
        filter_end_date: new Date(Date.now()).getTime(),
      };
    }

    this.timeClockService.filter({ ...options }, req).subscribe(
      res => {
        const newTimeClocks = this.timeClockService.timeClockSettingMapper(res.body?.time_clocks, this.timeClockSettings, this.isEmployee);
        this.timeSheet = { ...res.body, time_clocks: newTimeClocks } ?? this.timeSheet;
        this.totalItems = Number(res.body?.count);
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  public editTimeClock(timeClockId?: number): void {
    if (timeClockId !== undefined) {
      this.timeClockService.findOne(timeClockId).subscribe((res: any) => {
        if (res.body) {
          const modalRef = this.modalService.open(TimeClockEditComponent, { size: 'lg', backdrop: 'static' });
          modalRef.componentInstance.designations = this.designations;
          modalRef.componentInstance.timeClock = res.body;
          modalRef.componentInstance.isEditMode = true;
          modalRef.closed.subscribe(reason => {
            if (reason === EModalReasonType.UPDATED || reason === EModalReasonType.DELETED) {
              this.timeSheetLoaded.emit();
            }
          });
        }
      });
    }
  }

  protected getFilterParamsFromStore(): void {
    this.subscriptions.add(
      this.store
        .select(selectCurrentCompany)
        .pipe(
          switchMap((res: UserCompany) => {
            if (res !== null) {
              if (res?.employee !== undefined) {
                this.filterParams = {
                  branch_id: res.employee.branch_id,
                  employee_id: res.employee.id,
                };
              }
            }
            return of(null);
          })
        )
        .subscribe(() => {
          this.filterTimeSheet();
        })
    );
  }

  protected getTimeClockSettingsFromStore(): void {
    this.subscriptions.add(
      this.store.select(selectTimeClockSettings).subscribe((res: TimeClockSettings) => {
        if (res !== null) {
          this.timeClockSettings = res;
        }
      })
    );
  }
}
