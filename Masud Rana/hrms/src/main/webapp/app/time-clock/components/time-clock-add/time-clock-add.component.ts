import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'app/store';
import { finalize, switchMap } from 'rxjs/operators';
import { Observable, of, Subscription, timer } from 'rxjs';

import { selectCurrentCompany } from './../../../store/user/user.selectors';
import { selectApplicationSettings, selectTimeClockSettings } from './../../../store/company-settings/company-settings.selectors';

import { TimeClockSettings } from './../../../store/states/time-clocks-settings.interface';

import { ITimeClock, ITimeClockLocation, ITimeSheet } from '../../models/time-clock.model';
import { ITimeClockEvent } from '../../models/time-clock-event.model';
import { IGeoLocation, ITimeClockFilterParams } from './../../models/time-clock.model';
import { IDesignation } from './../../../entities/designation/designation.model';
import { ITimeZone } from '../../../entities/time-zone/time-zone.model';

import { ETimeClockEventType } from '../../time-clock-event-type.enum';
import { ETimeType } from './../../../shared/pipes/time/time-type.enum';

import { TimeClockService } from './../../service/time-clock.service';
import { ToastService } from './../../../shared/components/toast/toast.service';
import { UtilService } from './../../../shared/service/util.service';
import { PermissionService } from './../../../shared/service/permission.service';
import { EmployeeOnlineService } from '../../../shared/components/socket/employee-online.service';

@Component({
  selector: 'hrms-time-clock-add',
  templateUrl: './time-clock-add.component.html',
  styleUrls: ['./time-clock-add.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TimeClockAddComponent implements OnInit, OnDestroy {
  clockInTimer?: string | null;
  isEmployee = true;
  isEmployeeRestricted = false;
  timeClockSettings!: TimeClockSettings;
  timeZone!: ITimeZone;

  @Output() timeSheetLoaded = new EventEmitter<ITimeSheet>();
  @Output() timeClockLoaded = new EventEmitter<ITimeClock>();

  ETimeClockEventType = ETimeClockEventType;
  ETimeType = ETimeType;
  isEditMode = false;
  noteEventCount = 0;
  positionEventCount = 0;
  isLoading = false;
  isClockIn = false;
  isPreClockIn = false;
  isPreClockMandatory = false;
  isOpenPositionField = false;
  isBreakStart = false;
  filterParams?: ITimeClockFilterParams;
  requestIp: any = '';
  timeClockLocation!: ITimeClockLocation;
  geoLocation!: IGeoLocation;

  timeClock?: ITimeClock;
  lastTimeClockEvent?: ITimeClockEvent;

  designations: IDesignation[] = [];

  currentDate = Date.now();

  timeClockNoteForm = this.fb.group({
    note: [null, [Validators.required]],
    time_clock_id: [this.timeClock?.id, [Validators.required]],
  });

  timeClockPositionForm = this.fb.group({
    new_position_id: [this.designations[0], [Validators.required]],
  });

  private subscriptionTimer: any;
  private isSubcribedTimer = false;
  private subscriptions: Subscription = new Subscription();

  constructor(
    private store: Store<AppState>,
    public toastService: ToastService,
    protected fb: FormBuilder,
    private timeClockService: TimeClockService,
    private utilService: UtilService,
    private permissionService: PermissionService,
    private employeeOnlineService: EmployeeOnlineService
  ) {}

  ngOnInit(): void {
    this.initiateStore();
    this.getOnClockIn();
  }

  public getOnClockIn(): void {
    this.subscriptions.add(
      this.employeeOnlineService?.getOnClockIn().subscribe(() => {
        this.checkPreClockIn();
      })
    );
  }

  public initiateStore(): void {
    this.subscriptions.add(
      this.store
        .select(selectTimeClockSettings)
        .pipe(
          switchMap(timeClockSettings => {
            if (timeClockSettings !== null) {
              this.timeClockSettings = timeClockSettings;
            }
            return this.store.select(selectApplicationSettings);
          }),
          switchMap(applicationSettings => {
            if (applicationSettings) {
              this.timeZone = applicationSettings.time_zone;
            }
            return this.store.select(selectCurrentCompany);
          }),
          switchMap(currentCompany => {
            if (currentCompany !== null) {
              if (currentCompany?.employee !== undefined) {
                this.filterParams = {
                  branch_id: currentCompany.employee.branch_id,
                  employee_id: currentCompany.employee.id,
                };
              }
            }
            return of(currentCompany);
          })
        )
        .subscribe(res => {
          if (res !== null && this.timeZone) {
            this.loadTimeClock();
            this.checkIfEmployeeIsRestricted();
            this.isEmployee = this.permissionService?.isEmployee();
          }
        })
    );
  }

  public startClockInTimer(): void {
    if (!this.isSubcribedTimer) {
      const clockStartTime = this.timeClock?.clock_start_time ? new Date(Number(this.timeClock.clock_start_time)) : null;
      this.subscriptionTimer = timer(0, 1000).subscribe(() => {
        this.clockInTimer = this.utilService.getTimeDifferenceFromCurrentTime(clockStartTime);
        this.isSubcribedTimer = true;
      });
    }
  }

  public stopClockInTimer(): void {
    if (this.isSubcribedTimer) {
      this.subscriptionTimer.unsubscribe();
      this.isSubcribedTimer = false;
      this.clockInTimer = null;
    }
  }

  public checkIfEmployeeIsRestricted(): void {
    if (this.isEmployee && this.timeClockSettings?.restrict_employee_clock_in_out) {
      this.isEmployeeRestricted = true;
    }
  }

  public changeIsBreakStart(isBreakStart: boolean): void {
    this.isBreakStart = isBreakStart;
    this.setLoadedOnline();
  }

  public updateTimeClock(): void {
    if (this.timeClock) {
      this.timeClockService.update(this.timeClock);
    }
  }

  public createTimeClock(): void {
    this.isLoading = true;
    navigator.geolocation.getCurrentPosition(
      position => {
        this.timeClockLocation = {
          clockin_latitude: position?.coords.latitude,
          clockin_longitude: position?.coords.longitude,
        };
        this.subscribeToSaveResponse(
          this.timeClockService.create({ ...this.filterParams, ...this.timeClockLocation, pre_clock_time: Date.now() })
        );
      },
      error => {
        this.toastService.showDangerToast(error.message);
        this.isLoading = false;
      },
      { enableHighAccuracy: true }
    );
  }

  public checkClockInValidation(): boolean {
    if (this.timeClock && this.utilService.hasKeys(this.timeClock) && this.utilService.isObject(this.timeClock)) {
      return true;
    }
    if (this.timeClock && !this.timeClock.clock_end_time) {
      return false;
    }
    return true;
  }

  public clockIn(): void {
    this.isLoading = true;
    navigator.geolocation.getCurrentPosition(
      position => {
        this.timeClockLocation = {
          clockin_latitude: position?.coords.latitude,
          clockin_longitude: position?.coords.longitude,
        };
        if (position) {
          const isValid = this.checkClockInValidation();
          if (isValid) {
            this.subscribeToSaveResponse(
              this.timeClockService.create({ ...this.filterParams, ...this.timeClockLocation, clock_start_time: Date.now() })
            );
          } else {
            if (this.timeClock !== undefined) {
              this.timeClockService
                .update({ id: this.timeClock.id, ...this.timeClockLocation, clock_start_time: Date.now() })
                .subscribe(res => {
                  this.loadTimeClock();
                  this.setLoadedOnline();
                  this.isLoading = false;
                });
            }
          }
        } else {
          this.toastService.showDangerToast('Please turn on your location before Punch In');
          this.isLoading = false;
        }
      },
      error => {
        this.toastService.showDangerToast(error.message);
        this.isLoading = false;
      },
      { enableHighAccuracy: true }
    );
  }

  public setLoadedOnline(): void {
    this.employeeOnlineService.setLoadedOnline();
  }

  public setOnline(timeClockId?: number): void {
    if (timeClockId && timeClockId !== undefined) {
      this.employeeOnlineService.setClockIn(timeClockId);
    }
  }

  public setOffline(timeClockId?: number): void {
    if (timeClockId && timeClockId !== undefined) {
      this.employeeOnlineService.setClockOut(timeClockId);
    }
  }

  public showDangerToast(text: string): void {
    this.toastService.showDangerToast(text);
  }

  public checkPreClockIn(): void {
    if (this.timeClockSettings?.pre_clock_mandatory) {
      this.isPreClockMandatory = true;
      if (this?.timeClock && this.timeClock?.pre_clock_time && !this.timeClock?.clock_end_time) {
        this.isPreClockMandatory = false;
        this.isPreClockIn = true;
        if (!this.timeClock?.clock_start_time) {
          this.isClockIn = false;
        } else {
          this.checkClockIn();
        }
      } else {
        this.isPreClockIn = false;
        this.checkClockIn();
      }
    } else {
      if (this?.timeClock && this.timeClock?.pre_clock_time && !this.timeClock?.clock_end_time) {
        this.isPreClockMandatory = false;
        this.isPreClockIn = true;
        this.checkClockIn();
      } else {
        this.isPreClockIn = false;
        this.checkClockIn();
      }
    }
  }

  public checkClockIn(): void {
    if (this?.timeClock && this.timeClock?.clock_start_time && !this.timeClock?.clock_end_time) {
      this.isClockIn = true;
      this.startClockInTimer();
    } else {
      this.isClockIn = false;
      this.stopClockInTimer();
    }
  }

  public checkClockOutSettingsValidation(): boolean {
    let noteEventCount = 0;
    let positionEventCount = 0;
    const events: ITimeClockEvent[] = this.timeClock?.time_clock_events ?? [];

    if (this.timeClockSettings?.clock_out_notes_required || this.timeClockSettings?.should_have_position_set) {
      events.map((timeClockEvent: ITimeClockEvent) => {
        if (timeClockEvent.note) {
          noteEventCount += 1;
        }

        if (timeClockEvent.new_designation) {
          positionEventCount += 1;
        }
      });

      if (
        this.timeClockSettings?.clock_out_notes_required &&
        this.timeClockSettings?.should_have_position_set &&
        noteEventCount < 1 &&
        positionEventCount < 1
      ) {
        this.showDangerToast('Please add note and position!');
        return false;
      }

      if (this.timeClockSettings?.clock_out_notes_required && noteEventCount < 1) {
        this.showDangerToast('Please add note!');
        return false;
      }

      if (this.timeClockSettings?.should_have_position_set && positionEventCount < 1) {
        this.showDangerToast('Please add position!');
        return false;
      }
    }
    return true;
  }

  public clockOut(): void {
    this.isLoading = true;
    navigator.geolocation.getCurrentPosition(
      position => {
        this.timeClockLocation = {
          clockout_latitude: position?.coords.latitude,
          clockout_longitude: position?.coords.longitude,
        };
        if (position) {
          if (this.timeClock?.id !== undefined) {
            const isValid = this.checkClockOutSettingsValidation();

            if (isValid) {
              this.subscribeToSaveResponse(this.timeClockService.clockOut(this.timeClock.id, { ...this.timeClockLocation }), true);
            }
          }
        } else {
          this.toastService.showDangerToast('Please turn on your location before Punch Out');
          this.isLoading = false;
        }
      },
      error => {
        this.toastService.showDangerToast(error.message);
        this.isLoading = false;
      },
      { enableHighAccuracy: true }
    );
  }

  public deleteTimeClock(time_clock_id?: number): void {
    if (time_clock_id !== undefined) {
      this.timeClockService.delete(time_clock_id).subscribe(() => {
        this.loadTimeClock();
      });
    }
  }

  public loadTimeClock(): void {
    if (this.filterParams !== undefined) {
      this.timeClockService.findForEmployee(this.filterParams).subscribe((res: HttpResponse<ITimeClock>) => {
        this.timeClock = { ...res['body'] };
        this.timeClockLoaded.emit(this.timeClock);
        this.requestIp = this.timeClock?.request_ip;
        this.checkPreClockIn();
        this.loadTimeClockEvents(this.timeClock.time_clock_events ?? []);
        setTimeout(() => {
          this.getGeoLocation();
        }, 500);
      });
    }
  }

  public loadTimeClockEvents(time_clock_events: any[]): void {
    time_clock_events.forEach(event => {
      if (event.event_type === ETimeClockEventType.BREAK) {
        if (!event.break_end_time) {
          this.lastTimeClockEvent = event;
          this.isBreakStart = true;
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  protected getGeoLocation(): void {
    if (this.timeClockSettings.show_address_under_time_clock) {
      this.subscriptions.add(
        this.utilService.getGeoLocation(this.requestIp).subscribe(geoLocation => {
          this.geoLocation = geoLocation;
        })
      );
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITimeClock>>, isClockOut?: boolean): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      response => {
        const responseBody: ITimeClock = { ...response['body'] };
        this.requestIp = responseBody?.request_ip;
        if (!isClockOut) {
          this.getGeoLocation();
        }
        this.onSaveSuccess(isClockOut, response.body?.id);
        this.isLoading = false;
      },
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(isClockOut?: boolean, timeClockId?: number): void {
    this.loadTimeClock();
    this.setLoadedOnline();
    if (isClockOut) {
      this.isClockIn = false;
      this.timeSheetLoaded.emit();
      this.stopClockInTimer();
      this.setOffline(timeClockId);
    } else {
      this.setOnline(timeClockId);
    }
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isLoading = false;
  }
}
