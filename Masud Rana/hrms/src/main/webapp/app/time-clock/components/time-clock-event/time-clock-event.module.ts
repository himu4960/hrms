import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { TimeClockEventComponent } from './time-clock-event.component';

@NgModule({
  imports: [SharedModule],
  declarations: [TimeClockEventComponent],
  exports: [TimeClockEventComponent],
})
export class TimeClockEventModule {}
