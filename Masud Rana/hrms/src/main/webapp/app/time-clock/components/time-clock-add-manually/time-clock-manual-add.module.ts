import { NgModule } from '@angular/core';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../../../shared/shared.module';
import { TimeClockManualAddComponent } from './time-clock-manual-add.component';

@NgModule({
  imports: [SharedModule, NgbDatepickerModule],
  declarations: [TimeClockManualAddComponent],
  exports: [TimeClockManualAddComponent],
})
export class TimeClockManualAddModule {}
