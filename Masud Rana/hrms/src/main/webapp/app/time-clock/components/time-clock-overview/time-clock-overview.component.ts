import { HttpResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AppState } from 'app/store';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { ITimeClock, ITimeClockFilterParams, ITimeSheet } from '../../models/time-clock.model';
import { IDesignation } from '../../../entities/designation/designation.model';
import { ITimeZone } from '../../../entities/time-zone/time-zone.model';
import { Pagination } from 'app/core/request/request.model';

import { UserCompany } from '../../../store/states/user.interface';
import { TimeClockSettings } from '../../../store/states/time-clocks-settings.interface';
import { ApplicationSettings } from '../../../store/states/application-settings.interface';

import { selectApplicationSettings, selectTimeClockSettings } from 'app/store/company-settings/company-settings.selectors';
import { selectCurrentCompany } from '../../../store/user/user.selectors';

import { TimeClockService } from '../../service/time-clock.service';
import { PermissionService } from '../../../shared/service/permission.service';

@Component({
  selector: 'hrms-time-clock-overview',
  templateUrl: './time-clock-overview.component.html',
  styleUrls: ['./time-clock-overview.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TimeClockOverviewComponent implements OnInit, OnDestroy {
  isEmployee = true;
  totalItems = 0;
  allowTimeSheetManual = true;
  allowTimeSheetImport = true;
  timeClock!: ITimeClock;
  timeZone!: ITimeZone;
  isLoading = true;
  timeClockSettings!: TimeClockSettings;
  designations: IDesignation[] = [];
  filterParams?: ITimeClockFilterParams;
  timeSheet: any = {
    time_clocks: [],
    total_duration: 0,
    total_break_duration: 0,
    total_without_break_duration: 0,
  };

  private subscriptions: Subscription = new Subscription();

  constructor(private store: Store<AppState>, private timeClockService: TimeClockService, private permissionService: PermissionService) {}

  ngOnInit(): void {
    this.isEmployee = this.permissionService.isEmployee();
    this.getTimeClockSettingsFromStore();
    this.getFilterParamsFromStore();
    this.checkIfEmployeeAllowTimeSheetAddManual();
    this.checkIfEmployeeAllowTimeSheetImport();
  }

  public checkIfEmployeeAllowTimeSheetImport(): void {
    if (this.isEmployee && !this.timeClockSettings.timesheet_employee_can_import) {
      this.allowTimeSheetImport = false;
    }
  }

  public checkIfEmployeeAllowTimeSheetAddManual(): void {
    if (this.isEmployee && !this.timeClockSettings.timesheet_employee_can_manually_add) {
      this.allowTimeSheetManual = false;
    }
  }

  public loadTimeSheet(req?: Pagination): void {
    if (this.filterParams !== undefined) {
      this.timeClockService.findTimeSheetForEmployee(this.filterParams, req).subscribe((res: HttpResponse<ITimeSheet>) => {
        const newTimeClocks = this.timeClockService.timeClockSettingMapper(res.body?.time_clocks, this.timeClockSettings, this.isEmployee);
        this.timeSheet = { ...res.body, time_clocks: newTimeClocks } ?? this.timeSheet;
        this.totalItems = Number(res.body?.count);
      });
    }
  }

  public loadedTimeClockEvents(event: ITimeClock): void {
    this.timeClock = event;
  }

  public getTimeSheet(timeSheet: ITimeSheet): void {
    this.timeSheet = timeSheet;
  }

  public getDesignations(designations: IDesignation[]): void {
    this.designations = designations;
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  protected getTimeClockSettingsFromStore(): void {
    this.subscriptions.add(
      this.store.select(selectTimeClockSettings).subscribe((res: TimeClockSettings) => {
        if (res !== null) {
          this.timeClockSettings = res;
        }
      })
    );

    this.subscriptions.add(
      this.store.select(selectApplicationSettings).subscribe((settings: ApplicationSettings) => {
        if (settings) {
          this.timeZone = settings.time_zone;
        }
      })
    );
  }

  protected getFilterParamsFromStore(): void {
    this.subscriptions.add(
      this.store.select(selectCurrentCompany).subscribe((res: UserCompany) => {
        if (res !== null) {
          if (res?.employee !== undefined) {
            this.filterParams = {
              branch_id: res.employee.branch_id,
              employee_id: res.employee.id,
            };
          }
          if (res.employee.employee_designations !== undefined && res.employee.employee_designations.length > 0) {
            this.designations = res.employee.employee_designations.map(designation => {
              return {
                id: designation.designation_id,
                name: designation.designation?.name,
              };
            });
          }
        }
      })
    );
  }
}
