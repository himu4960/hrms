import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AppState } from 'app/store';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as dayjs from 'dayjs';
import * as weekday from 'dayjs/plugin/weekday';
dayjs.extend(weekday);

import { EModalReasonType } from 'app/shared/enum';
import { ETimeType } from './../../../shared/pipes/time/time-type.enum';
import { ETimeSheetApproveType } from './../../time-clock-event-type.enum';

import { ITimeSheetManage } from './../../models/time-clock.model';
import { IDesignation } from 'app/entities/designation/designation.model';
import { IEmployee } from 'app/employee/models/employee.model';
import { IBranch } from 'app/entities/branch/branch.model';
import { EFilterDurationType, IFilterDateRange } from './../../../shared/forms/generic-filter-form/models/generic-filter.model';

import { TimeClockService } from '../../service/time-clock.service';
import { UtilService } from 'app/shared/service/util.service';
import { GenericFilterService } from './../../../shared/forms/generic-filter-form/generic-filter.service';

import { TimeClockEditComponent } from '../time-clock-edit/time-clock-edit.component';
import { selectApplicationSettings } from 'app/store/company-settings/company-settings.selectors';
import { ApplicationSettings } from 'app/store/states/application-settings.interface';
import { ITimeZone } from 'app/entities/time-zone/time-zone.model';

@Component({
  selector: 'hrms-time-sheet-manage',
  templateUrl: './time-sheet-manage.component.html',
  styleUrls: ['./time-sheet-manage.component.scss'],
})
export class TimeSheetManageComponent implements OnInit {
  timeZone!: ITimeZone;
  timeSheets: any[] = [];
  ETimeType = ETimeType;
  ETimeSheetApproveType = ETimeSheetApproveType;
  EFilterDurationType = EFilterDurationType;

  duration_type = EFilterDurationType.ALL_TIME;

  designations: IDesignation[] = [];
  branches: IBranch[] = [];
  employees: IEmployee[] = [];

  filterTimeSheetManageForm = this.fb.group({
    filter_date: [],
    start_date: ['2022-10-09', [Validators.required]],
    end_date: [this.utilService.format(new Date().toISOString()), [Validators.required]],
    is_approved: [],
    employee_id: [],
    designation_id: [],
    skill_id: [],
  });

  constructor(
    private store: Store<AppState>,
    protected fb: FormBuilder,
    private timeClockService: TimeClockService,
    private utilService: UtilService,
    protected modalService: NgbModal,
    protected genericFilterService: GenericFilterService
  ) {}

  ngOnInit(): void {
    this.timeSheetManageFilter();
    this.loadPage().subscribe();
  }

  loadPage(): Observable<any> {
    return this.store.select(selectApplicationSettings).pipe(
      switchMap((res: ApplicationSettings) => {
        const employees$ = this.utilService.getEmployees();
        const branches$ = this.utilService.getBranches();
        const designation$ = this.utilService.getDesignations();
        this.timeZone = res?.time_zone;
        return combineLatest([employees$, designation$, branches$]).pipe(
          tap(([employees, designations, branches]) => {
            this.branches = branches;
            this.employees = employees;
            this.designations = designations;
          })
        );
      })
    );
  }

  public timeSheetManageFilter(): void {
    const { start_date, end_date, is_approved, employee_id, designation_id, skill_id, filter_date } = this.filterTimeSheetManageForm.value;

    let filterParams = {};

    if (filter_date >= 0) {
      const filterRange: IFilterDateRange = this.genericFilterService.getFilterRange(filter_date, start_date, end_date);
      this.filterTimeSheetManageForm.patchValue({
        start_date: this.utilService.format(filterRange.start_date),
        end_date: this.utilService.format(filterRange.end_date),
      });
      filterParams = {
        ...filterParams,
        start_date: this.utilService.format(filterRange.start_date),
        end_date: this.utilService.format(filterRange.end_date),
      };
    } else {
      filterParams = {
        ...filterParams,
        start_date,
        end_date,
      };
    }

    if (is_approved >= 0) {
      filterParams = {
        ...filterParams,
        is_approved,
      };
    }

    if (employee_id) {
      filterParams = {
        ...filterParams,
        employee_id,
      };
    }

    if (designation_id) {
      filterParams = {
        ...filterParams,
        designation_id,
      };
    }

    if (skill_id) {
      filterParams = {
        ...filterParams,
        skill_id,
      };
    }

    this.timeClockService.timeSheetManageFilter(filterParams).subscribe(res => {
      const timeSheets: ITimeSheetManage[] = res.body;
      if (timeSheets.length) {
        this.timeSheets = timeSheets.map(timeSheet => {
          return { ...timeSheet, clock_start_date: this.utilService.format(timeSheet.clock_start_time) };
        });
      } else {
        this.timeSheets = [];
      }
    });
  }

  public editTimeClock(timeClockId?: number): void {
    if (timeClockId !== undefined) {
      this.timeClockService.findOne(timeClockId).subscribe((res: any) => {
        if (res.body) {
          const modalRef = this.modalService.open(TimeClockEditComponent, { size: 'lg', backdrop: 'static' });
          modalRef.componentInstance.designations = this.designations;
          modalRef.componentInstance.timeClock = res.body;
          modalRef.componentInstance.isEditMode = true;
          modalRef.closed.subscribe(reason => {
            if (reason === EModalReasonType.UPDATED || reason === EModalReasonType.DELETED) {
              this.timeSheetManageFilter();
            }
          });
        }
      });
    }
  }

  public handleApprove(time_clock_id: number, is_approved: boolean): void {
    this.timeClockService.update({ id: time_clock_id, is_approved }).subscribe(() => {
      this.timeSheetManageFilter();
    });
  }
}
