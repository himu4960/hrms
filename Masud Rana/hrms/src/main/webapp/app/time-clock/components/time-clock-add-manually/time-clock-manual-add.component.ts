import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { AppState } from 'app/store';
import { Observable, Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import * as dayjs from 'dayjs';

import { UserCompany } from './../../../store/states/user.interface';
import { TimeClockSettings } from './../../../store/states/time-clocks-settings.interface';

import { selectCurrentCompany } from './../../../store/user/user.selectors';
import { selectTimeClockSettings } from './../../../store/company-settings/company-settings.selectors';

import { IDesignationForEmployee, ITimeClock } from '../../models/time-clock.model';
import { IEmployee, IEmployeeDesignation } from './../../../employee/models/employee.model';
import { IDesignation } from 'app/entities/designation/designation.model';

import { UtilService } from '../../../shared/service/util.service';
import { EmployeeService } from './../../../employee/service/employee.service';
import { PermissionService } from '../../../shared/service/permission.service';
import { TimeClockService } from '../../service/time-clock.service';
import { ToastService } from './../../../shared/components/toast/toast.service';

@Component({
  selector: 'hrms-time-clock-manual-add',
  templateUrl: './time-clock-manual-add.component.html',
  styleUrls: ['./time-clock-manual-add.component.scss'],
})
export class TimeClockManualAddComponent implements OnInit {
  isSaving = false;
  isEmployee = true;
  timeClockSettings!: TimeClockSettings;
  isLoading = false;
  onlyClockedIn = false;
  employees: IEmployee[] = [];
  designations: any = [];
  selectedEmployee?: IEmployee;
  selectedDesignation?: IDesignation;

  addTimeClockForm = this.fb.group({
    id: [],
    clock_start_time: [null, [Validators.required]],
    clock_start_date: [null, [Validators.required]],
    clock_end_time: [null, [Validators.required]],
    clock_end_date: [null, [Validators.required]],
    branch_id: [null, [Validators.required]],
    designation_id: [null, [Validators.required]],
    employee_id: [null, [Validators.required]],
    notes: [],
    tips: [],
  });

  public clock_start_time_control = this.addTimeClockForm.get('clock_start_time');
  public clock_end_time_control = this.addTimeClockForm.get('clock_end_time');
  public clock_end_date_control = this.addTimeClockForm.get('clock_end_date');

  private subscriptions: Subscription = new Subscription();

  constructor(
    private store: Store<AppState>,
    protected fb: FormBuilder,
    private employeeService: EmployeeService,
    public permissionService: PermissionService,
    private timeClockService: TimeClockService,
    private utilService: UtilService,
    public toastService: ToastService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.isEmployee = this.permissionService?.isEmployee();
    this.subscriptions.add(
      this.store.select(selectTimeClockSettings).subscribe((res: TimeClockSettings) => {
        if (res !== null) {
          this.timeClockSettings = res;
        }
      })
    );

    if (!this.isEmployee) {
      this.loadEmployees();
    } else {
      this.subscriptions.add(
        this.store.select(selectCurrentCompany).subscribe((res: UserCompany) => {
          if (res !== null) {
            this.selectedEmployee = res.employee;
            this.designations = this.employeeDesignationMapper(res.employee?.employee_designations);
            this.addTimeClockForm.patchValue({
              employee_id: res.employee.id,
              branch_id: res.employee.branch?.id ?? res.employee?.branch_id,
            });
          }
        })
      );
    }

    this.addTimeClockForm.patchValue({
      ...this.addTimeClockForm,
      clock_start_time: this.utilService.formatTime(dayjs().toISOString()),
      clock_end_time: this.onlyClockedIn ? null : this.utilService.formatTime(dayjs().toISOString()),
    });
  }

  addTimeClock(): void {
    this.isSaving = true;
    const timeClock = this.addTimeClockForm.value;
    const { clock_start_time, clock_start_date, clock_end_time, clock_end_date, branch_id, designation_id, employee_id, notes } = timeClock;

    const newTimeClock: ITimeClock = {
      clock_start_time:
        clock_start_date && clock_start_time
          ? new Date(this.utilService.combineDateAndTime(clock_start_date.toString(), clock_start_time?.toString())).getTime()
          : null,
      clock_end_time:
        clock_end_date && clock_end_time
          ? new Date(this.utilService.combineDateAndTime(clock_end_date.toString(), clock_end_time?.toString())).getTime()
          : null,
      branch_id,
      designation_id,
      employee_id,
      notes,
    };
    this.subscribeToSaveResponse(this.timeClockService.create(newTimeClock));
  }

  loadEmployeeDesignations(employeeId?: number): void {
    if (employeeId !== undefined) {
      this.selectedEmployee = this.employees.find(employee => employee.id === Number(employeeId));
      if (this.selectedEmployee !== undefined) {
        this.designations = this.employeeDesignationMapper(this.selectedEmployee?.employee_designations);
        this.selectedDesignation = this.designations.length ? this.designations[0] : null;
        this.addTimeClockForm.patchValue({
          ...this.addTimeClockForm,
          branch_id: this.selectedEmployee.branch?.id,
          employee_id: this.selectedEmployee.id,
        });
      }
    }
  }

  onChangeDesignation(designation: IDesignation): void {
    this.selectedDesignation = designation;
  }

  employeeDesignationMapper(employee_designatios?: IEmployeeDesignation[]): IDesignationForEmployee[] {
    if (employee_designatios !== undefined && employee_designatios.length > 0) {
      return employee_designatios.map(employee_designation => ({
        id: Number(employee_designation.designation?.id),
        name: employee_designation.designation?.name,
      }));
    }
    return [];
  }

  startDateSelect(e: NgbDate): void {
    const date = this.utilService.format(new Date(e.year, e.month - 1, e.day).toISOString());
    this.addTimeClockForm.patchValue({
      ...this.addTimeClockForm,
      clock_start_date: date,
    });
  }

  endDateSelect(e: NgbDate): void {
    const date = this.utilService.format(new Date(e.year, e.month - 1, e.day).toISOString());
    this.addTimeClockForm.patchValue({
      ...this.addTimeClockForm,
      clock_end_date: date,
    });
  }

  changeOnlyClockedIn(value: boolean): void {
    this.onlyClockedIn = value;
    if (this.onlyClockedIn) {
      this.clock_end_date_control?.clearValidators();
    } else {
      this.clock_end_date_control?.addValidators([Validators.required]);
    }
    this.clock_end_date_control?.updateValueAndValidity();
  }

  loadEmployees(): void {
    this.utilService.getEmployees().subscribe(res => {
      if (res) {
        this.employees = res;
      }
    });
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITimeClock>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isLoading = false;
    this.isSaving = false;
    this.toastService.showSuccessToast(this.translateService.instant('timeClock.manual.add'));
    this.addTimeClockForm.reset();
  }

  protected onSaveError(): void {
    // Api for inheritance.
    this.isSaving = false;
  }

  protected onSaveFinalize(): void {
    this.isLoading = false;
  }
}
