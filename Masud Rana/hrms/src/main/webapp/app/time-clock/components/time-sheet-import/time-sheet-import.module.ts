import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { TimeSheetImportComponent } from './time-sheet-import.component';

@NgModule({
  imports: [SharedModule],
  declarations: [TimeSheetImportComponent],
  exports: [TimeSheetImportComponent],
})
export class TimeSheetImportModule {}
