import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { TimeClockStatisticsComponent } from './time-clock-statistics.component';

@NgModule({
  imports: [SharedModule],
  declarations: [TimeClockStatisticsComponent],
  exports: [TimeClockStatisticsComponent],
})
export class TimeClockStatisticsModule {}
