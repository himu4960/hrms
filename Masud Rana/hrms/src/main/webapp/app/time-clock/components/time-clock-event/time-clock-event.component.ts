import { Component, Input } from '@angular/core';

import { ITimeClock } from './../../models/time-clock.model';

import { ETimeClockEventType } from './../../time-clock-event-type.enum';
import { ETimeType } from './../../../shared/pipes/time/time-type.enum';
import { ITimeZone } from '../../../entities/time-zone/time-zone.model';

@Component({
  selector: 'hrms-time-clock-event',
  templateUrl: './time-clock-event.component.html',
  styleUrls: ['./time-clock-event.component.scss'],
})
export class TimeClockEventComponent {
  @Input() timeClock!: ITimeClock;
  @Input() timeZone!: ITimeZone;
  ETimeType = ETimeType;
  ETimeClockEventType = ETimeClockEventType;
}
