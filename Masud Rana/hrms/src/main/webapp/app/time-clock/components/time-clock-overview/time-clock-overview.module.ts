import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { TimeSheetModule } from '../time-sheet/time-sheet.module';
import { TimeClockAddModule } from '../time-clock-add/time-clock-add.module';
import { TimeClockEventModule } from '../time-clock-event/time-clock-event.module';

import { TimeClockOverviewComponent } from './time-clock-overview.component';

@NgModule({
  imports: [
    SharedModule,
    TimeClockAddModule,
    TimeClockEventModule,
    TimeSheetModule,
    RouterModule.forChild([
      {
        path: '',
        component: TimeClockOverviewComponent,
      },
    ]),
  ],
  declarations: [TimeClockOverviewComponent],
  exports: [TimeClockOverviewComponent, RouterModule],
})
export class TimeClockOverviewModule {}
