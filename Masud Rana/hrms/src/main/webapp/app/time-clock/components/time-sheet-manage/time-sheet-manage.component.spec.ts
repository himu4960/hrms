import { StoreModule } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from 'app/shared/shared.module';

import { TimeSheetManageComponent } from './time-sheet-manage.component';

describe('TimeSheetComponent', () => {
  let component: TimeSheetManageComponent;
  let fixture: ComponentFixture<TimeSheetManageComponent>;

  let fakeService: {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, SharedModule, StoreModule.forRoot({}), TranslateModule.forRoot({})],
      providers: [FormBuilder],
      declarations: [TimeSheetManageComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeSheetManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
