import { StoreModule } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

import { SharedModule } from 'app/shared/shared.module';

import { TimeClockNewEventAddComponent } from './time-clock-new-event-add.component';

describe('TimeClockNewEventAddComponent', () => {
  let component: TimeClockNewEventAddComponent;
  let fixture: ComponentFixture<TimeClockNewEventAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, SharedModule, CommonModule, StoreModule.forRoot({}), TranslateModule.forRoot({})],
      providers: [FormBuilder],
      declarations: [TimeClockNewEventAddComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeClockNewEventAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
