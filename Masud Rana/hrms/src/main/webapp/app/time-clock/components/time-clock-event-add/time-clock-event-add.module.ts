import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { TimeClockEventAddComponent } from './time-clock-event-add.component';

@NgModule({
  imports: [SharedModule],
  declarations: [TimeClockEventAddComponent],
  exports: [TimeClockEventAddComponent],
})
export class TimeClockEventAddModule {}
