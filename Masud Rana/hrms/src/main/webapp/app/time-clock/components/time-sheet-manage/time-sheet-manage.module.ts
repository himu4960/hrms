import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { TimeClockEditModule } from '../time-clock-edit/time-clock-edit.module';

import { TimeSheetManageComponent } from './time-sheet-manage.component';

@NgModule({
  imports: [SharedModule, TimeClockEditModule],
  declarations: [TimeSheetManageComponent],
  exports: [TimeSheetManageComponent],
})
export class TimeSheetManageModule {}
