import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TranslateService } from '@ngx-translate/core';

import { ITimeClock, TimeClock } from '../models/time-clock.model';

import { TimeClockService } from './time-clock.service';

describe('Service Tests', () => {
  describe('TimeClock Service', () => {
    let service: TimeClockService;
    let httpMock: HttpTestingController;
    let elemDefault: ITimeClock;
    let expectedResult: ITimeClock | ITimeClock[] | boolean | null;
    let currentDate: number;

    let fakeService: {};

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [{ provide: TranslateService, useValue: fakeService }],
      });
      expectedResult = null;
      service = TestBed.inject(TimeClockService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = Date.now();

      elemDefault = {
        id: 0,
        is_approved: false,
        approved_by: 0,
        approved_at: currentDate,
        approved_note: 'AAAAAAA',
        clock_start_time: currentDate,
        clock_end_time: currentDate,
        total_clock_duration: 0,
        total_break_duration: 0,
        total_break_taken: 0,
        notes: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            approved_at: currentDate,
            clock_start_time: currentDate,
            clock_end_time: currentDate,
          },
          elemDefault
        );

        service.findOne(123).subscribe((resp: any) => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a TimeClock', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            approved_at: currentDate,
            clock_start_time: currentDate,
            clock_end_time: currentDate,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            approved_at: currentDate,
            clock_start_time: currentDate,
            clock_end_time: currentDate,
          },
          returnedFromService
        );

        service.create(new TimeClock()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a TimeClock', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            is_approved: true,
            approved_by: 1,
            approved_at: currentDate,
            approved_note: 'BBBBBB',
            clock_start_time: currentDate,
            clock_end_time: currentDate,
            total_clock_duration: 1,
            total_break_duration: 1,
            total_break_taken: 1,
            notes: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            approved_at: currentDate,
            clock_start_time: currentDate,
            clock_end_time: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of TimeClock', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            is_approved: true,
            approved_by: 1,
            approved_at: currentDate,
            approved_note: 'BBBBBB',
            clock_start_time: currentDate,
            clock_end_time: currentDate,
            total_clock_duration: 1,
            total_break_duration: 1,
            total_break_taken: 1,
            notes: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            approved_at: currentDate,
            clock_start_time: currentDate,
            clock_end_time: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a TimeClock', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
