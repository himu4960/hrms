import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';

import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { UtilService } from './../../shared/service/util.service';

import { createRequestOption } from 'app/core/request/request-util';

import {
  ITimeClockFilterForEmployee,
  ITimeSheet,
  ITimeSheetFilter,
  ITimeSheetManageFilterParams,
  IImportTimeSheet,
} from './../models/time-clock.model';
import { ITimeClock, getTimeClockIdentifier } from '../models/time-clock.model';
import { ITimeClockEvent } from '../models/time-clock-event.model';
import { TimeClockSettings } from 'app/store/states/time-clocks-settings.interface';
import { Pagination } from 'app/core/request/request.model';

export type TimeClockEventResponseType = HttpResponse<ITimeClockEvent>;
export type TimeClockEventArrayResponseType = HttpResponse<ITimeClockEvent[]>;
export type TimeClockResponseType = HttpResponse<ITimeClock>;
export type TimeClockArrayResponseType = HttpResponse<ITimeClock[]>;
export type TimeSheetResponseType = HttpResponse<ITimeSheet>;

@Injectable({ providedIn: 'root' })
export class TimeClockService {
  public resourceUrlTimeClock = this.applicationConfigService.getEndpointFor('time-clocks');
  public resourceUrlTimeClockEvent = this.applicationConfigService.getEndpointFor('time-clock-events');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService, private utilService: UtilService) {}

  timeSheetManageFilter(filterTimeSheet: ITimeSheetManageFilterParams): Observable<HttpResponse<any>> {
    return this.http.post(`${this.resourceUrlTimeClock}/time-sheet/manage`, filterTimeSheet, { observe: 'response' });
  }

  timeSheetImport(importTimeSheet: IImportTimeSheet[]): Observable<HttpResponse<any>> {
    return this.http.post(`${this.resourceUrlTimeClock}/time-sheet/import`, importTimeSheet, { observe: 'response' });
  }

  filter(filterTimeSheet: ITimeSheetFilter, req?: Pagination): Observable<TimeSheetResponseType> {
    const options = createRequestOption(req);
    return this.http.post<ITimeSheet>(`${this.resourceUrlTimeClock}/time-sheet/filter`, filterTimeSheet, {
      params: options,
      observe: 'response',
    });
  }

  create(timeClock: ITimeClock): Observable<TimeClockResponseType> {
    return this.http.post<ITimeClock>(this.resourceUrlTimeClock, timeClock, { observe: 'response' });
  }

  breakStart(time_clock_id: number): Observable<TimeClockEventResponseType> {
    return this.http.post<ITimeClock>(`${this.resourceUrlTimeClockEvent}/break`, { time_clock_id }, { observe: 'response' });
  }

  breakOut(id: number): Observable<TimeClockEventResponseType> {
    return this.http.put<ITimeClock>(`${this.resourceUrlTimeClockEvent}/${id}/break`, {}, { observe: 'response' });
  }

  clockOut(time_clock_id: number, timeClock: ITimeClock): Observable<TimeClockResponseType> {
    return this.http.put<ITimeClock>(`${this.resourceUrlTimeClock}/${time_clock_id}/out`, timeClock, { observe: 'response' });
  }

  addNote(timeClockEvent: ITimeClockEvent): Observable<TimeClockEventResponseType> {
    return this.http.post<ITimeClock>(`${this.resourceUrlTimeClockEvent}/note`, timeClockEvent, { observe: 'response' });
  }

  addPosition(timeClockEvent: ITimeClockEvent): Observable<TimeClockEventResponseType> {
    return this.http.post<ITimeClock>(`${this.resourceUrlTimeClockEvent}/position`, timeClockEvent, { observe: 'response' });
  }

  update(timeClock: ITimeClock): Observable<TimeClockResponseType> {
    return this.http.put<ITimeClock>(`${this.resourceUrlTimeClock}/${getTimeClockIdentifier(timeClock) as number}`, timeClock, {
      observe: 'response',
    });
  }

  editMode(timeClock: ITimeClock): Observable<TimeClockResponseType> {
    return this.http.put<ITimeClock>(`${this.resourceUrlTimeClock}/edit/${getTimeClockIdentifier(timeClock) as number}`, timeClock, {
      observe: 'response',
    });
  }

  findOne(id: number): Observable<TimeClockResponseType> {
    return this.http.get<ITimeClock>(`${this.resourceUrlTimeClock}/${id}`, { observe: 'response' });
  }

  findTimeSheetForEmployee(filterParams: ITimeClockFilterForEmployee, req?: Pagination): Observable<TimeSheetResponseType> {
    const options = createRequestOption(req);
    return this.http.post<ITimeSheet>(`${this.resourceUrlTimeClock}/time-sheet`, filterParams, { params: options, observe: 'response' });
  }

  findForEmployee(filterParams: ITimeClockFilterForEmployee): Observable<TimeClockResponseType> {
    const { employee_id } = filterParams;
    const url = `${this.resourceUrlTimeClock}?employee_id=${employee_id}`;
    return this.http.get<ITimeClock>(url, {
      observe: 'response',
    });
  }

  query(req?: any): Observable<TimeClockArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITimeClock[]>(`${this.resourceUrlTimeClock}/all`, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrlTimeClock}/${id}`, { observe: 'response' });
  }

  public timeClockSettingMapper(timeClocks?: ITimeClock[], timeClockSettings?: TimeClockSettings, isEmployee?: boolean): ITimeClock[] {
    if (timeClocks !== undefined && timeClockSettings !== undefined && isEmployee !== undefined) {
      return timeClocks.map((timeClock: ITimeClock) => {
        const pastDay = timeClockSettings.timesheet_allow_employee_to_add_past_x_days;
        if (isEmployee && pastDay) {
          if (!this.utilService.isTwoDatesAreSame(pastDay, timeClock.clock_start_time)) {
            return {
              ...timeClock,
              editable: false,
            };
          }
        }
        return {
          ...timeClock,
          editable: true,
        };
      });
    }
    return [];
  }

  addTimeClockToCollectionIfMissing(
    timeClockCollection: ITimeClock[],
    ...timeClocksToCheck: (ITimeClock | null | undefined)[]
  ): ITimeClock[] {
    const timeClocks: ITimeClock[] = timeClocksToCheck.filter(isPresent);
    if (timeClocks.length > 0) {
      const timeClockCollectionIdentifiers = timeClockCollection.map(timeClockItem => getTimeClockIdentifier(timeClockItem)!);
      const timeClocksToAdd = timeClocks.filter(timeClockItem => {
        const timeClockIdentifier = getTimeClockIdentifier(timeClockItem);
        if (timeClockIdentifier == null || timeClockCollectionIdentifiers.includes(timeClockIdentifier)) {
          return false;
        }
        timeClockCollectionIdentifiers.push(timeClockIdentifier);
        return true;
      });
      return [...timeClocksToAdd, ...timeClockCollection];
    }
    return timeClockCollection;
  }
}
