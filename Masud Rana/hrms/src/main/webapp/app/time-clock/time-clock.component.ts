import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { AppState } from 'app/store';

import { TimeClockSettings } from 'app/store/states/time-clocks-settings.interface';

import { selectTimeClockSettings } from 'app/store/company-settings/company-settings.selectors';

import { PermissionService } from './../shared/service/permission.service';

@Component({
  selector: 'hrms-time-clock',
  templateUrl: './time-clock.component.html',
  styleUrls: ['./time-clock.component.scss'],
})
export class TimeClockComponent implements OnInit {
  timeClockSettings?: TimeClockSettings;
  isEmployee = true;

  private subscriptions: Subscription = new Subscription();

  constructor(private permissionService: PermissionService, private store: Store<AppState>) {}

  ngOnInit(): void {
    this.isEmployee = this.permissionService.isEmployee();
    this.getTimeClockSettings();
  }

  public getTimeClockSettings(): void {
    this.subscriptions.add(
      this.store.select(selectTimeClockSettings).subscribe((res: TimeClockSettings) => {
        if (res !== null) {
          this.timeClockSettings = res;
        }
      })
    );
  }
}
