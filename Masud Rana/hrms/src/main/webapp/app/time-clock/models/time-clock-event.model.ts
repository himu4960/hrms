import { IDesignation } from 'app/entities/designation/designation.model';

export interface ITimeClockEventForm {
  id?: number;
  time_clock_id?: number | null;
  event_type?: string | null;
  break_start_time?: number | null;
  break_end_time?: number | null;
  break_start_date?: number | null;
  break_end_date?: number | null;
  total_break_duration?: number | null;
  new_position_id?: number | null;
  note?: string | null;
  created_at_time?: string | null;
  created_at_date?: string | null;
}

export interface ITimeClockEvent {
  id?: number;
  event_type?: string | null;
  break_start_time?: number | null;
  break_end_time?: number | null;
  total_break_duration?: number | null;
  prev_position_id?: number | null;
  new_position_id?: number | null;
  note?: string | null;
  time_clock_id?: number | null;
  new_designation?: IDesignation | null;
  created_at?: number | null;
}

export class TimeClockEvent implements ITimeClockEvent {
  constructor(
    public id?: number,
    public time_clock_id?: number | null,
    public event_type?: string | null,
    public break_start_time?: number | null,
    public break_end_time?: number | null,
    public total_break_duration?: number | null,
    public prev_position_id?: number | null,
    public new_position_id?: number | null,
    public new_designation?: IDesignation | null,
    public note?: string | null,
    public created_at?: number | null
  ) {}
}

export function getTimeClockEventIdentifier(timeClockEvent: ITimeClockEvent): number | undefined {
  return timeClockEvent.id;
}
