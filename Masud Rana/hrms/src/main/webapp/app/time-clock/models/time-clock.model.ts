import { TimeClockEvent, ITimeClockEvent, ITimeClockEventForm } from './time-clock-event.model';

export interface ITimeClockFilterParams {
  branch_id?: number | null;
  employee_id?: number | null;
  designation_id?: number | null;
}
export interface ITimeClockLocation {
  clockin_latitude?: number | null;
  clockin_longitude?: number | null;
  clockout_latitude?: number | null;
  clockout_longitude?: number | null;
}

export interface ITimeSheetManage {
  clock_start_time: string;
  clock_end_time: string;
  designation_name: string;
  employee_name: string;
  end_date: string;
  start_date: string;
  time_clock_id: number;
  total_break_duration: number;
  total_clock_duration: number;
  is_approved: boolean;
}

export interface IDateBetween {
  start_date: number;
  end_date: number;
}

export interface ITimeClockFilterForEmployee {
  employee_id?: number | null;
  designation_id?: number | null;
}

export interface ITimeSheetFilter {
  is_approved?: boolean | null;
  filter_start_date?: number | null;
  filter_end_date?: number | null;
  employee_id?: number | null;
  designation_id?: number | null;
}

export interface ITimeSheetManageFilterParams {
  start_date?: string | null;
  end_date?: string | null;
  designation_id?: number | null;
  skill_id?: number | null;
  employee_id?: number | null;
  is_approved?: boolean | null;
}

export interface IDesignationForEmployee {
  id?: number;
  name?: string;
}

export interface IImportTimeSheet {
  email: string;
  clock_start_time: number;
  clock_end_time: number;
  notes: string;
  position: string;
}
export interface ITimeClock {
  id?: number;
  is_approved?: boolean | null;
  approved_by?: number | null;
  approved_at?: number | null;
  approved_note?: string | null;
  clock_start_time?: number | null;
  clock_end_time?: number | null;
  pre_clock_time?: number | null;
  total_clock_duration?: number | null;
  total_break_duration?: number | null;
  total_break_taken?: number | null;
  notes?: string | null;
  branch_id?: number | null;
  employee_id?: number | null;
  designation_id?: number | null;
  request_ip?: string | null;
  clockin_latitude?: number | null;
  clockin_longitude?: number | null;
  clockout_latitude?: number | null;
  clockout_longitude?: number | null;
  time_clock_events?: ITimeClockEvent[];
  editable?: boolean | null;
}

export interface ITimeClockForm {
  id?: number;
  is_approved?: boolean | null;
  approved_by?: number | null;
  approved_at?: number | null;
  approved_note?: string | null;
  clock_start_time?: number | null;
  clock_start_date?: number | null;

  clock_end_time?: number | null;
  clock_end_date?: number | null;

  pre_clock_time?: number | null;

  notes?: string | null;
  branch_id?: number | null;
  employee_id?: number | null;
  designation_id?: number | null;
  time_clock_events?: ITimeClockEventForm[];
}

export interface ITimeSheet {
  time_clocks: ITimeClock[];
  total_duration?: number;
  total_break_duration?: number;
  total_without_break_duration?: number;
  count?: number;
}

export class TimeClock implements ITimeClock {
  constructor(
    public id?: number,
    public is_approved?: boolean | null,
    public approved_by?: number | null,
    public approved_at?: number | null,
    public approved_note?: string | null,
    public clock_start_time?: number | null,
    public clock_end_time?: number | null,
    public pre_clock_time?: number | null,
    public total_clock_duration?: number | null,
    public total_break_duration?: number | null,
    public total_break_taken?: number | null,
    public notes?: string | null,
    public branch_id?: number | null,
    public employee_id?: number | null,
    public designation_id?: number | null,
    public time_clock_events?: TimeClockEvent[]
  ) {
    this.is_approved = this.is_approved ?? false;
  }
}

export interface IGeoLocation {
  id?: number;
  ip?: string;
  hostname?: any;
  type: string;
  continent_code: string;
  continent_name: string;
  country_code: string;
  country_name: string;
  region_code: string;
  region_name: string;
  city: string;
  zip: string;
  latitude: number;
  longitude: number;
  geoname_id: number;
  capital: string;
  country_flag: string;
  country_flag_emoji: string;
  country_flag_emoji_unicode: string;
  calling_code: string;
  is_eu: boolean;
  time_zone_id?: any;
  time_zone_current_time?: any;
  time_zone_gmt_offset?: any;
  security_is_proxy?: any;
  security_proxy_type?: any;
  security_is_crawler?: any;
  security_crawler_name?: any;
  security_crawler_type?: any;
  security_is_tor?: any;
  security_threat_level?: any;
  security_threat_types?: any;
}

export function getTimeClockIdentifier(timeClock: ITimeClock): number | undefined {
  return timeClock.id;
}
