export enum ETimeClockEventType {
  BREAK = 'break',
  POSITION = 'position',
  NOTE = 'note',
}

export enum ETimeSheetFilterType {
  TODAY = 0,
  YESTERDAY = -1,
  LAST_7_DAYS = -7,
  THIS_WEEK = 6,
  LAST_WEEK = 14,
  ALL_TIME = 100,
}
export enum ETimeSheetApproveType {
  ALL = 2,
  APPROVE = 1,
  UNAPPROVE = 0,
}
