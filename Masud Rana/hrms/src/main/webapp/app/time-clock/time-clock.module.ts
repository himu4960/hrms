import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { TimeSheetModule } from './components/time-sheet/time-sheet.module';
import { TimeClockAddModule } from './components/time-clock-add/time-clock-add.module';
import { TimeSheetManageModule } from './components/time-sheet-manage/time-sheet-manage.module';
import { TimeClockManualAddModule } from './components/time-clock-add-manually/time-clock-manual-add.module';
import { TimeSheetImportModule } from './components/time-sheet-import/time-sheet-import.module';
import { TimeClockStatisticsModule } from './components/time-clock-statistics/time-clock-statistics.module';
import { TimeClockEventModule } from './components/time-clock-event/time-clock-event.module';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';

import { TimeClockComponent } from './time-clock.component';
import { TimeClockManualAddComponent } from './components/time-clock-add-manually/time-clock-manual-add.component';
import { TimeSheetImportComponent } from './components/time-sheet-import/time-sheet-import.component';
import { TimeSheetManageComponent } from './components/time-sheet-manage/time-sheet-manage.component';
@NgModule({
  imports: [
    SharedModule,
    TimeClockAddModule,
    TimeClockEventModule,
    TimeClockStatisticsModule,
    TimeSheetModule,
    TimeSheetManageModule,
    TimeClockManualAddModule,
    TimeSheetImportModule,
    RouterModule.forChild([
      {
        path: '',
        redirectTo: '/time-clock/overview',
        pathMatch: 'full',
      },
      {
        path: '',
        component: TimeClockComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: 'overview',
            loadChildren: () => import('./components/time-clock-overview/time-clock-overview.module').then(m => m.TimeClockOverviewModule),
          },
          {
            path: 'add',
            component: TimeClockManualAddComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'upload',
            component: TimeSheetImportComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'manage',
            component: TimeSheetManageComponent,
            canActivate: [AuthGuard],
          },
        ],
      },
    ]),
  ],
  declarations: [TimeClockComponent],
  exports: [RouterModule],
})
export class TimeClockModule {}
