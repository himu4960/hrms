export const environment = {
  production: false,
  BASE_API_END_POINT: '/api/',
  URL: 'http://localhost:8081',
};
