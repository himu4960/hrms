import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';

import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { VERSION } from 'app/app.constants';
import { RoleType } from 'app/config/role.constants';

import { AppState } from 'app/store';
import { logoutUserAction } from 'app/store/user/user.actions';
import { LocalStorageService } from 'ngx-webstorage';

import { IEmployee } from './../../employee/models/employee.model';

import { selectCurrentRoute } from 'app/store/route/router.selector';
import { selectLanguage } from 'app/store/language/language.selectors';
import { selectCurrentCompany, selectUser } from './../../store/user/user.selectors';

import { mainMenus } from 'app/shared/menus';
import { RouterStateUrl } from 'app/store/route/custom-route-serializer';

import { ELocalStorageKeys, StateStorageService } from 'app/core/auth/state-storage.service';
import { AccountService } from 'app/core/auth/account.service';
import { PermissionService } from 'app/shared/service/permission.service';
import { EmployeeOnlineService } from 'app/shared/components/socket/employee-online.service';

import { IMenu } from 'app/entities/menu/menu.model';
import { ILanguage } from 'app/shared/models/utils.model';
import { IUser } from './../../entities/user/user.model';

import { LANGUAGES } from 'app/config/language.constants';

@Component({
  selector: 'hrms-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit, OnDestroy {
  inProduction?: boolean;
  isNavbarCollapsed = true;
  languages: ILanguage[] = [];
  activeLanguage: ILanguage | null | undefined;
  openAPIEnabled?: boolean;
  version = '';
  ownerRole = [RoleType.OWNER];
  menus: IMenu[] = mainMenus;
  tittle!: any;
  employee: IEmployee | null = null;
  user: IUser | null = null;
  languagesCode = LANGUAGES;

  private subscriptions: Subscription = new Subscription();

  constructor(
    private translateService: TranslateService,
    private localStorage: LocalStorageService,
    private accountService: AccountService,
    private store: Store<AppState>,
    private storageService: StateStorageService,
    private router: Router,
    private permissionService: PermissionService,
    private employeeOnlineService: EmployeeOnlineService
  ) {
    if (VERSION) {
      this.version = VERSION.toLowerCase().startsWith('v') ? VERSION : 'v' + VERSION;
    }
  }

  get isCompanyAuthenticated(): boolean {
    return this.storageService.getCurrentCompany()?.company && true;
  }

  get isEmployee(): boolean {
    return this.permissionService.isEmployee();
  }

  ngOnInit(): void {
    this.loadAll();
  }

  loadAll(): void {
    this.subscriptions.add(
      this.store
        .select(selectCurrentCompany)
        .pipe(
          switchMap(currentCompany => {
            if (currentCompany !== null) {
              this.employee = currentCompany?.employee ?? null;
            }
            return this.store.select(selectUser);
          })
        )
        .subscribe(user => {
          if (user) {
            this.user = user;
          } else {
            this.user = this.storageService.getUserData();
          }
        })
    );

    this.subscriptions.add(
      this.store
        .select(selectCurrentRoute)
        .pipe(
          switchMap((route: RouterStateUrl) => {
            if (route !== null) {
              const menu = this.menus.find(menuItem => route.url === menuItem.router_link);
              this.tittle = menu?.name;
            }
            return this.store.select(selectLanguage);
          })
        )
        .subscribe((languages: ILanguage[]) => {
          this.languages = languages;
          if (!this.activeLanguage) {
            this.activeLanguage = languages.find(language => language.is_default);
          }
        })
    );
  }

  changeLanguage(selectedLanguage: ILanguage): void {
    if (selectedLanguage?.code) {
      this.translateService.use(selectedLanguage.code).subscribe(() => {
        this.activeLanguage = selectedLanguage;
        this.localStorage.store(ELocalStorageKeys.LANGUAGE, selectedLanguage.code);
        this.loadAll();
      });
    }
  }

  collapseNavbar(): void {
    this.isNavbarCollapsed = true;
  }

  isAuthenticated(): boolean {
    return this.accountService?.isAuthenticated();
  }

  logout(): void {
    this.employeeOnlineService.disconnect();
    this.collapseNavbar();
    this.storageService.clearStorage();
    this.store.dispatch(logoutUserAction());
    this.router.navigate(['/login']);
  }

  toggleNavbar(): void {
    this.isNavbarCollapsed = !this.isNavbarCollapsed;
  }

  getImageUrl(): string {
    return this.isAuthenticated() ? this.accountService.getImageUrl() : '';
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
