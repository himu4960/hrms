import { Component, Input } from '@angular/core';

@Component({
  selector: 'hrms-left-sidebar',
  templateUrl: './left-sidebar.component.html',
  styleUrls: ['./left-sidebar.component.scss'],
})
export class LeftSidebarComponent {
  @Input() menus: any = [];
}
