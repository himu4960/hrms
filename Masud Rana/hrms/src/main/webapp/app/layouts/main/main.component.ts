import { Component, OnInit, ViewChild } from '@angular/core';
import { Event, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { AppState } from 'app/store';

import { browserReloadMenuAction } from './../../store/menu/menu.actions';
import { browserReloadJwtTokenAction } from './../../store/user/user.actions';
import { browserReloadUserDataAction, browserReloadCurrentCompanyAction } from 'app/store/user/user.actions';
import { browserReloadCompanySettingsAction } from 'app/store/company-settings/company-settings.actions';

import { StateStorageService } from 'app/core/auth/state-storage.service';
import { AuthServerProvider } from 'app/core/auth/auth-jwt.service';
import { languageAction } from 'app/store/language/language.actions';

@Component({
  selector: 'hrms-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  @ViewChild('progressBar') progressBar: any;

  constructor(
    private store: Store<AppState>,
    private storageService: StateStorageService,
    private router: Router,
    private authServerProvider: AuthServerProvider
  ) {
    this.router.events.subscribe((event: Event) => {
      switch (true) {
        case event instanceof NavigationStart: {
          this.progressBar.start();
          break;
        }

        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.progressBar.complete();
          break;
        }
        default: {
          break;
        }
      }
    });
  }

  ngOnInit(): void {
    this.store.dispatch(languageAction());
    if (this.storageService.getUserData() && this.storageService.getCompanySettings()) {
      this.store.dispatch(browserReloadJwtTokenAction({ data: this.authServerProvider.getAuthenticationToken() }));
      this.store.dispatch(browserReloadCurrentCompanyAction({ data: this.storageService.getCurrentCompany() }));
      this.store.dispatch(browserReloadUserDataAction({ data: this.storageService.getUserData() }));
      this.store.dispatch(browserReloadCompanySettingsAction({ data: this.storageService.getCompanySettings() }));
      this.store.dispatch(browserReloadMenuAction());
    }
  }
}
