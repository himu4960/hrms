import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'hrms-page-ribbon',
  template: `
    <div class="ribbon" *ngIf="ribbonEnv$ | async as ribbonEnv">
      <a href="" hrmsTranslate="global.ribbon.{{ ribbonEnv }}">{{ ribbonEnv }}</a>
    </div>
  `,
  styleUrls: ['./page-ribbon.component.scss'],
})
export class PageRibbonComponent {
  ribbonEnv$?: Observable<string | undefined>;
}
