import { Component, OnDestroy, OnInit } from '@angular/core';
import { of, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { AppState } from 'app/store';

import { IMenu } from 'app/entities/menu/menu.model';

import { selectMenu } from 'app/store/menu/menu.selectors';

import { EmployeeOnlineService } from 'app/shared/components/socket/employee-online.service';

import { ISocketConnected } from 'app/shared/components/socket/socket.model';

@Component({
  selector: 'hrms-main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.scss'],
})
export class MainNavigationComponent implements OnInit, OnDestroy {
  menus?: IMenu[];
  private subscriptions: Subscription = new Subscription();

  constructor(private store: Store<AppState>, private employeeOnlineService: EmployeeOnlineService) {}

  ngOnInit(): void {
    this.subscriptions.add(
      this.store.select(selectMenu).subscribe((res: any) => {
        this.menus = res || [];
      })
    );

    this.subscriptions.add(
      this.employeeOnlineService
        .connect()
        .pipe(
          switchMap((connectionResponse: ISocketConnected) => {
            if (connectionResponse.client_id) {
              return of(connectionResponse.client_id);
            }
            return of(null);
          }),
          switchMap(clientId => {
            if (!clientId) {
              return of(this.employeeOnlineService.disconnect());
            }
            return of(null);
          })
        )
        .subscribe()
    );
  }

  ngOnDestroy(): void {
    this.employeeOnlineService.disconnect();
    this.subscriptions.unsubscribe();
  }
}
