import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs';

import { createRequestOption } from 'app/core/request/request-util';

import { ApplicationConfigService } from 'app/core/config/application-config.service';

import { ILeave } from '../model/leave.model';
import { ILeaveData, ILeaveParams } from '../model/leave-balance.model';

export type EntityResponseType = HttpResponse<ILeave>;
export type EntityArrayResponseType = HttpResponse<ILeave[]>;

@Injectable({
  providedIn: 'root',
})
export class LeaveService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('leaves');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(leave: ILeave): Observable<EntityResponseType> {
    return this.http.post<ILeave>(this.resourceUrl, leave, { observe: 'response' });
  }

  requestByEmployee(leave: ILeave): Observable<EntityResponseType> {
    return this.http.post<ILeave>(`${this.resourceUrl}/request`, leave, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ILeave[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  getOnLeaves(): Observable<EntityArrayResponseType> {
    return this.http.get<ILeave[]>(`${this.resourceUrl}/on-leaves`, { observe: 'response' });
  }

  filter(filterParam: any): Observable<any> {
    return this.http.post<EntityArrayResponseType>(`${this.resourceUrl}/filter`, filterParam, { observe: 'response' });
  }

  getLeaveData(leaveParams: ILeaveParams): Observable<HttpResponse<ILeaveData>> {
    return this.http.post<any>(`${this.resourceUrl}/data`, leaveParams, { observe: 'response' });
  }

  approveLeave(leaveId: number, approved_note: string): Observable<EntityResponseType> {
    return this.http.put(`${this.resourceUrl}/approve/${leaveId}`, { approved_note }, { observe: 'response' });
  }

  rejectLeave(leaveId: number, rejected_note: string): Observable<EntityResponseType> {
    return this.http.put(`${this.resourceUrl}/reject/${leaveId}`, { rejected_note }, { observe: 'response' });
  }

  cancelLeave(leaveId: number): Observable<EntityResponseType> {
    return this.http.put<any>(`${this.resourceUrl}/cancel/${leaveId}`, null, { observe: 'response' });
  }

  reviewRequest(leaveId: number): Observable<EntityResponseType> {
    return this.http.put<any>(`${this.resourceUrl}/review/request/${leaveId}`, null, { observe: 'response' });
  }

  reviewReject(leaveId: number): Observable<EntityResponseType> {
    return this.http.put<any>(`${this.resourceUrl}/review/reject/${leaveId}`, null, { observe: 'response' });
  }

  reviewApproved(leaveId: number): Observable<EntityResponseType> {
    return this.http.put<any>(`${this.resourceUrl}/review/approve/${leaveId}`, null, { observe: 'response' });
  }
}
