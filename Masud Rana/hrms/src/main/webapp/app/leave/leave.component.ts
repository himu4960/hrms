import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { PermissionService } from 'app/shared/service/permission.service';

@Component({
  selector: 'hrms-leave',
  templateUrl: './leave.component.html',
  styleUrls: ['./leave.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LeaveComponent implements OnInit {
  isEmployee = true;

  constructor(private permissionService: PermissionService) {}

  ngOnInit(): void {
    this.isEmployee = this.permissionService?.isEmployee();
  }
}
