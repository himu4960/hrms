import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { ConfirmDialogModule } from 'app/shared/components/confirm-dialog/confirm-dialog.module';

import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';

import { LeaveComponent } from './leave.component';
import { RequestLeaveComponent } from './components/request-leave/request-leave.component';
import { LeaveManagementComponent } from './components/leave-management/leave-management.component';
import { LeaveOverviewComponent } from './components/leave-overview/leave-overview.component';
import { LeaveTableComponent } from './components/leave-table/leave-table.component';
import { ConfirmLeaveRequestDialogComponent } from './components/confirm/confirm-leave-request-dialog.component';

@NgModule({
  declarations: [
    LeaveComponent,
    LeaveOverviewComponent,
    RequestLeaveComponent,
    LeaveManagementComponent,
    LeaveTableComponent,
    ConfirmLeaveRequestDialogComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    ConfirmDialogModule,
    RouterModule.forChild([
      {
        path: '',
        redirectTo: '/leave/overview',
        pathMatch: 'full',
      },
      {
        path: '',
        component: LeaveComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: 'overview',
            component: LeaveOverviewComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'management',
            component: LeaveManagementComponent,
            canActivate: [AuthGuard],
          },
        ],
      },
    ]),
  ],
})
export class LeaveModule {}
