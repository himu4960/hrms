import { HttpResponse } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { ITimeZone } from 'app/entities/time-zone/time-zone.model';
import { ILeaveBalance, ILeaveData, ILeaveParams } from 'app/leave/model/leave-balance.model';
import { ILeave } from 'app/leave/model/leave.model';

import { EModalReasonType } from 'app/shared/enum';

import { LeaveService } from 'app/leave/service/leave.service';
import { PermissionService } from 'app/shared/service/permission.service';

import { AppState } from 'app/store';

import { selectApplicationSettings, selectLeaveAvailabilitySettings } from 'app/store/company-settings/company-settings.selectors';
import { selectCurrentCompany } from 'app/store/user/user.selectors';

import { ApplicationSettings } from 'app/store/states/application-settings.interface';
import { LeaveAvailabilitySettings } from 'app/store/states/leave-availability-settings.interface';
import { UserCompany } from 'app/store/states/user.interface';

import { ConfirmLeaveRequestDialogComponent } from '../confirm/confirm-leave-request-dialog.component';
import { RequestLeaveComponent } from '../request-leave/request-leave.component';

@Component({
  selector: 'hrms-leave-overview',
  templateUrl: './leave-overview.component.html',
  styleUrls: ['./leave-overview.component.scss'],
})
export class LeaveOverviewComponent {
  isLoading = false;
  isEmployee = true;
  pendingLeaves: ILeave[] = [];
  upcomingLeaves: ILeave[] = [];
  leaveBalances: ILeaveBalance[] = [];

  timeZone!: ITimeZone;

  filterParams: ILeaveParams = {};
  action: any;

  leaveSettings!: LeaveAvailabilitySettings;
  employeeCanRequestLeave = true;

  private subscriptions: Subscription = new Subscription();

  constructor(
    private modalService: NgbModal,
    private store: Store<AppState>,
    private permissionService: PermissionService,
    private leaveService: LeaveService
  ) {}

  ngOnInit(): void {
    this.isEmployee = this.permissionService?.isEmployee();
    this.getFilterParamsFromStore();

    this.subscriptions.add(
      this.store.select(selectLeaveAvailabilitySettings).subscribe((res: LeaveAvailabilitySettings) => {
        if (res !== null) {
          this.leaveSettings = res;
          this.employeeCanBookLeaveRequest();
        }
      })
    );

    this.subscriptions.add(
      this.store.select(selectApplicationSettings).subscribe((settings: ApplicationSettings) => {
        if (settings) {
          this.timeZone = settings.time_zone;
        }
      })
    );

    this.loadLeaveData();
  }

  public loadLeaveData(): void {
    if (this.filterParams?.employee_id) {
      this.filterParams = {
        ...this.filterParams,
      };

      this.leaveService.getLeaveData(this.filterParams).subscribe((res: HttpResponse<ILeaveData>) => {
        if (res.body) {
          this.leaveBalances = res.body.leave_balances;
          this.pendingLeaves = res.body.pending_leaves;
          this.upcomingLeaves = res.body.upcoming_leaves;
        }
      });
    }
  }

  public employeeCanBookLeaveRequest(): void {
    if (this.isEmployee) {
      this.employeeCanRequestLeave = this.leaveSettings.employees_can_book_vacation;
    }
  }

  public showLeaveRequestModal(): void {
    const modalRef = this.modalService.open(RequestLeaveComponent, { size: 'lg', backdrop: 'static' });
    modalRef.closed.subscribe(() => {
      this.loadLeaveData();
    });
  }

  public showCancelModal(id: any): void {
    const modalRef = this.modalService.open(ConfirmLeaveRequestDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.leaveId = id;
    modalRef.componentInstance.title = 'leaves.modal.cancel.title';
    modalRef.componentInstance.bodyText = 'leaves.modal.cancel.description';
    modalRef.componentInstance.successButtonLabel = 'global.action.delete';
    modalRef.componentInstance.successButtonClass = 'btn-danger';
    modalRef.componentInstance.modalType = EModalReasonType.CANCEL;
    modalRef.closed.subscribe(() => {
      this.loadLeaveData();
    });
  }

  public showRequestModal(id: any): void {
    const modalRef = this.modalService.open(ConfirmLeaveRequestDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.leaveId = id;
    modalRef.componentInstance.title = 'leaves.modal.request.title';
    modalRef.componentInstance.bodyText = 'leaves.modal.request.description';
    modalRef.componentInstance.successButtonLabel = 'global.action.request';
    modalRef.componentInstance.successButtonClass = 'btn-primary';
    modalRef.componentInstance.modalType = EModalReasonType.REQUESTED;
    modalRef.closed.subscribe(() => {
      this.loadLeaveData();
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  protected getFilterParamsFromStore(): void {
    this.subscriptions.add(
      this.store.select(selectCurrentCompany).subscribe((res: UserCompany) => {
        if (res !== null) {
          if (res?.employee !== undefined) {
            this.filterParams = {
              employee_id: res.employee.id,
            };
          }
        }
      })
    );
  }
}
