jest.mock('@angular/router');

import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { provideMockStore } from '@ngrx/store/testing';
import { of, Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Leave } from 'app/leave/model/leave.model';
import { LeaveService } from 'app/leave/service/leave.service';

import { RequestLeaveComponent } from './request-leave.component';
import { PermissionService } from 'app/shared/service/permission.service';

describe('RequestLeaveComponent', () => {
  let component: RequestLeaveComponent;
  let fixture: ComponentFixture<RequestLeaveComponent>;
  let activatedRoute: ActivatedRoute;
  let leaveService: LeaveService;
  let fakeService: {};
  let mockActiveModal: NgbActiveModal;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [RequestLeaveComponent],
      providers: [
        NgbActiveModal,
        FormBuilder,
        ActivatedRoute,
        provideMockStore(),
        { provide: PermissionService, useValue: fakeService },
        { provide: TranslateService, useValue: fakeService },
      ],
    })
      .overrideTemplate(RequestLeaveComponent, '')
      .compileComponents();
    mockActiveModal = TestBed.inject(NgbActiveModal);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestLeaveComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    leaveService = TestBed.inject(LeaveService);
    component = fixture.componentInstance;
  });

  it('Should call create service on save for new entity', () => {
    // GIVEN
    const saveSubject = new Subject();
    const leave = new Leave();
    spyOn(leaveService, 'create').and.returnValue(saveSubject);
    spyOn(component, 'previousState');
    activatedRoute.data = of({ leave });
    component.ngOnInit();

    expect(component.isSaving).toEqual(false);

    // WHEN
    component.leaveRequest();
    expect(component.isSaving).toEqual(true);
    saveSubject.next(new HttpResponse({ body: leave }));
    saveSubject.complete();

    // THEN
    expect(component.isSaving).toEqual(true);
  });
});
