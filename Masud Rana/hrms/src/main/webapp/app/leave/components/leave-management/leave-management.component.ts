import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { AppState } from 'app/store';

import { IBranch } from 'app/entities/branch/branch.model';
import { ELeaveStatusType, ILeave } from 'app/leave/model/leave.model';
import { ILeaveParams } from 'app/leave/model/leave-balance.model';
import { ITimeZone } from 'app/entities/time-zone/time-zone.model';

import { LeaveService } from './../../service/leave.service';
import { UtilService } from 'app/shared/service/util.service';

import { dateValidator } from 'app/shared/validators/start-end-date.validator';

import { selectCurrentCompany } from 'app/store/user/user.selectors';

import { UserCompany } from 'app/store/states/user.interface';
import { ApplicationSettings } from 'app/store/states/application-settings.interface';

import { selectApplicationSettings } from 'app/store/company-settings/company-settings.selectors';

import { ConfirmLeaveRequestDialogComponent } from '../confirm/confirm-leave-request-dialog.component';

import { EModalReasonType } from 'app/shared/enum';

@Component({
  selector: 'hrms-leave-management',
  templateUrl: './leave-management.component.html',
  styleUrls: ['./leave-management.component.scss'],
})
export class LeaveManagementComponent implements OnInit, OnDestroy {
  branches: IBranch[] = [];
  requestedLeaves: any[] = [];
  leaveApplication?: any;
  designations: any = [];
  filterParams: ILeaveParams = {};
  timeZone!: ITimeZone;

  approved_note = '';
  rejected_note = '';

  filterForm: FormGroup = this.fb.group(
    {
      start_date: [null],
      end_date: [null],
      branch_id: [null],
      designation_id: [null],
      is_approved: [false],
    },
    { validators: dateValidator }
  );

  get eLeaveStatusType(): any {
    return ELeaveStatusType;
  }

  @ViewChild('leaveApplicationViewModal') leaveApplicationViewModal!: NgbModal;

  private subscriptions: Subscription = new Subscription();

  constructor(
    private utilService: UtilService,
    private fb: FormBuilder,
    private leaveService: LeaveService,
    private store: Store<AppState>,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.getBranch();
    this.getFilterParamsFromStore();
    this.filterForm.get('branch_id')?.valueChanges.subscribe((branchId: number) => {
      const branch = this.branches.find(data => data.id === branchId);
      this.designations = branch?.designations ?? [];
      if (this.designations instanceof Array && this.designations.length) {
        this.filterForm.get('designation_id')?.patchValue(this.designations[0].id);
      }
    });

    this.subscriptions.add(
      this.store.select(selectApplicationSettings).subscribe((settings: ApplicationSettings) => {
        if (settings) {
          this.timeZone = settings.time_zone;
        }
      })
    );

    this.filter();
  }

  public getBranch(): void {
    this.utilService.getBranches().subscribe((branches: IBranch[]) => {
      if (branches !== undefined) {
        this.branches = branches;
      }
    });
  }

  public filter(): void {
    const start_date = this.filterForm.value.start_date ? new Date(this.filterForm.value.start_date).toISOString() : null;
    const end_date = this.filterForm.value.end_date ? new Date(this.filterForm.value.end_date).toISOString() : null;
    this.filterParams = {
      ...this.filterParams,
      ...this.filterForm.value,
      start_date,
      end_date,
    };

    this.leaveService.filter(this.filterParams).subscribe((res: any) => {
      this.requestedLeaves = res.body ?? [];
    });
  }

  public resetFilters(): void {
    this.filterForm.reset();
  }

  public showRejectModal(id: any, isRequestRejected = false): void {
    const modalRef = this.modalService.open(ConfirmLeaveRequestDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.leaveId = id;
    modalRef.componentInstance.title = 'leaves.modal.reject.title';
    modalRef.componentInstance.bodyText = 'leaves.modal.reject.description';
    modalRef.componentInstance.successButtonLabel = 'global.action.reject';
    modalRef.componentInstance.successButtonClass = 'btn-danger';
    if (isRequestRejected) {
      modalRef.componentInstance.modalType = EModalReasonType.REQUEST_REJECTED;
    } else {
      modalRef.componentInstance.modalType = EModalReasonType.REJECTED;
    }
    modalRef.closed.subscribe(() => {
      this.filter();
    });
  }

  public showApproveModal(id: any, isRequested = false): void {
    const modalRef = this.modalService.open(ConfirmLeaveRequestDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.leaveId = id;
    modalRef.componentInstance.title = 'leaves.modal.approve.title';
    modalRef.componentInstance.bodyText = 'leaves.modal.approve.description';
    modalRef.componentInstance.successButtonLabel = 'global.action.approve';
    modalRef.componentInstance.successButtonClass = 'btn-primary';
    if (isRequested) {
      modalRef.componentInstance.modalType = EModalReasonType.REQUEST_APPROVED;
    } else {
      modalRef.componentInstance.modalType = EModalReasonType.APPROVED;
    }
    modalRef.closed.subscribe(() => {
      this.filter();
    });
  }

  public showLeaveApplicationViewModal(leave: any): void {
    this.leaveApplication = leave;
    this.modalService.open(this.leaveApplicationViewModal, { size: 'lg', backdrop: 'static' });
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  protected getFilterParamsFromStore(): void {
    this.subscriptions.add(
      this.store.select(selectCurrentCompany).subscribe((res: UserCompany) => {
        if (res !== null) {
          if (res?.employee !== undefined) {
            this.filterParams = {
              employee_id: res.employee.id,
            };
          }
        }
      })
    );
  }
}
