import { HttpResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { finalize, switchMap } from 'rxjs/operators';

import { NgbActiveModal, NgbCalendar, NgbDate } from '@ng-bootstrap/ng-bootstrap';

import { AppState } from 'app/store';

import { IEmployee } from 'app/employee/models/employee.model';
import { ILeaveType } from 'app/entities/leave-type/leave-type.model';
import { ILeave, Leave } from 'app/leave/model/leave.model';
import { ITimeSecond, timeValues } from '../../../shared/pipes/time-value/time-value.mapper';

import { LeaveService } from 'app/leave/service/leave.service';
import { PermissionService } from 'app/shared/service/permission.service';
import { HolidayService } from 'app/entities/holiday/service/holiday.service';
import { UtilService } from 'app/shared/service/util.service';

import { dateValidator } from 'app/shared/validators/start-end-date.validator';

import { LeaveAvailabilitySettings } from 'app/store/states/leave-availability-settings.interface';
import { UserCompany } from 'app/store/states/user.interface';

import { selectLeaveAvailabilitySettings } from 'app/store/company-settings/company-settings.selectors';
import { selectCurrentCompany } from 'app/store/user/user.selectors';

import { ADD_24_HOURS } from 'app/shared/constant/time.contstant';
import { IHoliday } from 'app/entities/holiday/holiday.model';

@Component({
  selector: 'hrms-request-leave',
  templateUrl: './request-leave.component.html',
  styleUrls: ['./request-leave.component.scss'],
})
export class RequestLeaveComponent implements OnInit, OnDestroy {
  isSaving = false;
  isEnabledPartial = true;
  isPartialDayAllowed = false;
  isSelectedLeaveType = false;
  isEmployee = true;
  employees: IEmployee[] = [];
  leaveTypes: ILeaveType[] = [];
  leaveTimes: ITimeSecond[] = timeValues;
  weekendDays: number[] = [];
  holiDays: IHoliday[] = [];
  leaveSettings!: LeaveAvailabilitySettings;
  advanceLeaveBookDayErrorMessage = '';

  minDate: any;

  leaveRequestForm: FormGroup = this.fb.group(
    {
      id: [],
      employee_id: [null, Validators.required],
      leave_type_id: [null, Validators.required],
      is_partial_day_allowed: [0],
      start_date: [null, Validators.required],
      end_date: [],
      start_time: [],
      end_time: [],
      comments: [null, [Validators.required, Validators.maxLength(500)]],
    },
    { validators: dateValidator }
  );

  public employeeIdControl = this.leaveRequestForm.get(['employee_id']);
  public leaveTypeIdControl = this.leaveRequestForm.get(['leave_type_id']);
  public startDateControl = this.leaveRequestForm.get(['start_date']);
  public endDateControl = this.leaveRequestForm.get(['end_date']);
  public startTimeControl = this.leaveRequestForm.get(['start_time']);
  public endTimeControl = this.leaveRequestForm.get(['end_time']);
  public is_partial_day_allowed = this.leaveRequestForm.get(['is_partial_day_allowed']);
  public commentsControl = this.leaveRequestForm.get(['comments']);

  markDisabled: any;
  private subscriptions: Subscription = new Subscription();

  constructor(
    private fb: FormBuilder,
    private permissionService: PermissionService,
    private utilService: UtilService,
    private leaveService: LeaveService,
    private holidayService: HolidayService,
    private store: Store<AppState>,
    private calendar: NgbCalendar,
    private activeModal: NgbActiveModal
  ) {
    const current = new Date();
    this.minDate = {
      year: current.getFullYear(),
      month: current.getMonth() + 1,
      day: current.getDate(),
    };
  }

  ngOnInit(): void {
    this.utilService
      .getEmployees()
      .pipe(
        switchMap((emplyees: IEmployee[]) => {
          this.isEmployee = this.permissionService.isEmployee();
          this.employees = emplyees;
          if (this.isEmployee) {
            this.employeeIdControl?.removeValidators(Validators.required);
            this.leaveRequestForm.controls['employee_id'].updateValueAndValidity();
            this.updateFormForEmployee();
          }
          return this.utilService.getLeaveType();
        })
      )
      .subscribe((leaveTypes: ILeaveType[]) => {
        this.leaveTypes = leaveTypes;
      });

    this.subscriptions.add(
      this.store.select(selectLeaveAvailabilitySettings).subscribe((res: LeaveAvailabilitySettings) => {
        if (res !== null) {
          this.leaveSettings = res;
          this.weekendDays = JSON.parse(res?.deduct_weekends_from_leaves);
          this.disableWeekendAndHolidays();
        }
      })
    );

    this.subscriptions.add(
      this.holidayService.query().subscribe(res => {
        this.holiDays = res.body ?? [];
        this.disableWeekendAndHolidays();
      })
    );

    this.subscriptions.add(
      this.startDateControl?.valueChanges.subscribe(startDate => {
        const selectedLeaveType = this.leaveTypes.find((leaveType: ILeaveType) => leaveType.id === Number(this.leaveTypeIdControl?.value));
        const advanceDays = selectedLeaveType?.leave_in_advance_days ?? 0;
        const dateDiff = this.utilService.getDayDifference(new Date(), new Date(startDate));
        if (selectedLeaveType?.is_leave_in_advance_days_enabled && advanceDays > 0 && dateDiff < advanceDays) {
          this.startDateControl?.setErrors({ incorrect: true });
          this.advanceLeaveBookDayErrorMessage = `Leave Must Be Booked ${advanceDays} Days In Advance`;
        } else {
          this.advanceLeaveBookDayErrorMessage = '';
        }
      })
    );

    this.changeLeaveType();
  }

  public updateFormForEmployee(): void {
    this.subscriptions.add(
      this.store.select(selectCurrentCompany).subscribe((res: UserCompany) => {
        if (res !== null) {
          this.leaveRequestForm.patchValue({
            employee_id: res.employee.id,
          });
        }
      })
    );
  }

  public cancel(): void {
    this.activeModal.dismiss();
  }

  public handleEnabledPartial(event: any): void {
    if (event.target.value) {
      const leaveTypeId = event.target.value;
      this.isSelectedLeaveType = true;
      const selectedLeaveType = this.leaveTypes.find((leaveType: ILeaveType) => leaveType.id === parseInt(leaveTypeId, 10));
      this.isEnabledPartial = selectedLeaveType?.is_partial_day_request_allowed ?? true;
    }
  }

  leaveRequest(): void {
    this.isSaving = true;
    const leave: ILeave = this.createFrom();

    if (this.weekendDays?.length) {
      leave.weekend_day = this.weekendDays;
    }

    if (this.isEmployee) {
      this.subscribeToSaveResponse(this.leaveService.requestByEmployee(leave));
    } else {
      this.subscribeToSaveResponse(this.leaveService.create(leave));
    }
  }

  changeLeaveType(): void {
    this.subscriptions.add(
      this.is_partial_day_allowed?.valueChanges.subscribe(value => {
        this.startDateControl?.reset('');
        this.isPartialDayAllowed = value;
      })
    );
  }

  previousState(): void {
    this.activeModal.close();
  }

  disableWeekendAndHolidays(): void {
    const holiDays: IHoliday[] = this.holiDays;
    const weekends: number[] = this.weekendDays;
    const index = weekends ? weekends.indexOf(0) : null;
    if (index && index > -1) {
      // ngbDatePicker starts from Monday = 1 to Sunday = 7
      // whereas new Date() starts from Sunday = 0 to Saturday = 6
      weekends[index] = 7;
    }

    this.markDisabled = (date: NgbDate) => {
      let calenderDate = this.calendar.getWeekday(date);
      if (calenderDate === 7) {
        calenderDate = 0;
      }
      if (holiDays.length) {
        return (
          holiDays.findIndex(holiDay => {
            const holiDayDate = holiDay?.date ? new Date(holiDay?.date) : undefined;
            let isDisable = false;
            if (holiDayDate) {
              if (weekends?.length) {
                isDisable =
                  (holiDayDate.getDate() === date.day &&
                    holiDayDate.getMonth() + 1 === date.month &&
                    holiDayDate.getFullYear() === date.year) ||
                  weekends.includes(calenderDate);
              } else {
                isDisable =
                  holiDayDate.getDate() === date.day &&
                  holiDayDate.getMonth() + 1 === date.month &&
                  holiDayDate.getFullYear() === date.year;
              }
            }
            return isDisable;
          }) !== -1
        );
      }

      if (!holiDays.length && weekends?.length) {
        return weekends.includes(calenderDate);
      }

      return false;
    };
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILeave>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected createFrom(): ILeave {
    const start_date = this.utilService.combineFromDatePicker(this.startDateControl!.value, this.startTimeControl!.value);
    const end_date = this.utilService.combineFromDatePicker(
      this.endDateControl?.value ?? this.startDateControl!.value,
      this.endTimeControl!.value
    );

    return {
      ...new Leave(),
      id: this.leaveRequestForm.get(['id'])!.value,
      employee_id: this.employeeIdControl!.value,
      leave_type_id: this.leaveTypeIdControl!.value,
      is_partial_day_allowed: this.isPartialDayAllowed,
      start_date: this.isPartialDayAllowed ? start_date : new Date(this.startDateControl!.value).getTime(),
      end_date: this.isPartialDayAllowed ? end_date : new Date(this.endDateControl!.value).getTime() + ADD_24_HOURS,
      start_time: start_date,
      end_time: end_date,
      comments: this.leaveRequestForm.get(['comments'])!.value,
    };
  }
}
