import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { LeaveService } from 'app/leave/service/leave.service';
import { EModalReasonType } from 'app/shared/enum';

@Component({
  selector: 'hrms-confirm-leave-request-dialog',
  templateUrl: './confirm-leave-request-dialog.component.html',
})
export class ConfirmLeaveRequestDialogComponent {
  title = '';
  bodyText = '';
  leaveId?: number;
  note = '';
  successButtonLabel = '';
  successButtonClass = 'btn-success';
  modalType = EModalReasonType.APPROVED;

  get eModalReasonType(): any {
    return EModalReasonType;
  }

  constructor(private readonly activeModal: NgbActiveModal, private leaveService: LeaveService) {}

  public cancel(): void {
    this.activeModal.dismiss();
  }

  public confirm(): void {
    if (this.leaveId !== undefined) {
      if (this.modalType === EModalReasonType.REJECTED) {
        this.leaveService.rejectLeave(this.leaveId, this.note).subscribe(() => {
          this.activeModal.close(EModalReasonType.REJECTED);
        });
      }

      if (this.modalType === EModalReasonType.APPROVED) {
        this.leaveService.approveLeave(this.leaveId, this.note).subscribe(() => {
          this.activeModal.close(EModalReasonType.REJECTED);
        });
      }

      if (this.modalType === EModalReasonType.CANCEL) {
        this.leaveService.cancelLeave(this.leaveId).subscribe(() => {
          this.activeModal.close(EModalReasonType.CANCEL);
        });
      }

      if (this.modalType === EModalReasonType.REQUESTED) {
        this.leaveService.reviewRequest(this.leaveId).subscribe(() => {
          this.activeModal.close(EModalReasonType.REQUESTED);
        });
      }

      if (this.modalType === EModalReasonType.REQUEST_REJECTED) {
        this.leaveService.reviewReject(this.leaveId).subscribe(() => {
          this.activeModal.close(EModalReasonType.REQUEST_REJECTED);
        });
      }

      if (this.modalType === EModalReasonType.REQUEST_APPROVED) {
        this.leaveService.reviewApproved(this.leaveId).subscribe(() => {
          this.activeModal.close(EModalReasonType.REQUEST_APPROVED);
        });
      }
    }
  }
}
