import { Component, EventEmitter, Input, Output } from '@angular/core';

import { ELeaveStatusType } from 'app/leave/model/leave.model';
import { LeaveAvailabilitySettings } from 'app/store/states/leave-availability-settings.interface';

@Component({
  selector: 'hrms-leave-table',
  templateUrl: './leave-table.component.html',
})
export class LeaveTableComponent {
  @Input() rows: any[] = [];
  @Input() timeZone: any = null;
  @Input() leaveSettings!: LeaveAvailabilitySettings;
  @Input() actionButtonLabel = 'global.action.cancel';
  @Input() actionButtonClass = 'btn-danger';
  @Output() showCancelModalEmitter = new EventEmitter<number>();

  get eLeaveStatusType(): any {
    return ELeaveStatusType;
  }

  public showCancelModal(leave_id: number): void {
    this.showCancelModalEmitter.emit(leave_id);
  }
}
