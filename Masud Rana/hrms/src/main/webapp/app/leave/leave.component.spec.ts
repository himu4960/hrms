jest.mock('@angular/router');

import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';

import { LeaveComponent } from './leave.component';
import { LeaveService } from 'app/leave/service/leave.service';
import { provideMockStore } from '@ngrx/store/testing';
import { PermissionService } from 'app/shared/service/permission.service';

describe('LeaveComponent', () => {
  let component: LeaveComponent;
  let fixture: ComponentFixture<LeaveComponent>;
  let leaveService: LeaveService;
  let fakeService: {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [LeaveComponent],
      providers: [
        provideMockStore(),
        FormBuilder,
        { provide: PermissionService, useValue: fakeService },
        { provide: TranslateService, useValue: fakeService },
      ],
    })
      .overrideTemplate(LeaveComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveComponent);
    leaveService = TestBed.inject(LeaveService);
    component = fixture.componentInstance;
  });

  it('Should call query service for load leave', () => {
    // GIVEN
    const saveSubject = new Subject();
    spyOn(leaveService, 'query').and.returnValue(saveSubject);

    component.ngOnInit();
    // WHEN
    saveSubject.next(new HttpResponse());
    saveSubject.complete();
  });
});
