import { IEmployee } from './../../employee/models/employee.model';
import { ILeaveType } from './../../entities/leave-type/leave-type.model';
import { ICompany } from './../../company/company.model';
import { ILeave } from './leave.model';

export interface ILeaveParams {
  employee_id?: number | null;
  branch_id?: number | null;
  leave_type_id?: number | null;
  designation_id?: number | null;
  status?: string | null;
  start_date?: string | null;
  end_date?: string | null;
}

export interface ILeaveBalance {
  id?: number;
  company_id?: number | null;
  employee_id?: number | null;
  leave_type_id?: number | null;
  allocated_leave_mins?: number | false;
  remaining_leave_mins?: number | null;
  company?: ICompany;
  employee?: IEmployee;
  leaveType?: ILeaveType;
}

export interface ILeaveData {
  pending_leaves: ILeave[];

  upcoming_leaves: ILeave[];

  leave_balances: ILeave[];
}

export class Leave implements ILeaveBalance {
  constructor(
    public id?: number,
    public employee_id?: number | null,
    public leave_type_id?: number | null,
    public allocated_leave_mins?: number | false,
    public remaining_leave_mins?: number | null,
    public company?: ICompany,
    public employee?: IEmployee,
    public leaveType?: ILeaveType
  ) {}
}

export function getLeaveIdentifier(leaveBalance: ILeaveBalance): number | undefined {
  return leaveBalance.id;
}
