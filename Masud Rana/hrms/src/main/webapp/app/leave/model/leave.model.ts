import { ICompany } from 'app/company/company.model';
import { IBranch } from 'app/entities/branch/branch.model';
import { ILeaveType } from 'app/entities/leave-type/leave-type.model';
import { IEmployee } from 'app/employee/models/employee.model';

export enum ELeaveStatusType {
  PENDING = 'Pending',
  APPROVED = 'Approved',
  REJECTED = 'Rejected',
  REQUESTED = 'Requested',
  REQUEST_APPROVED = 'Request Approved',
  REQUEST_REJECTED = 'Request Rejected',
}

export interface ILeave {
  id?: number;
  employee_id?: number | null;
  leave_type_id?: number | null;
  weekend_day?: number[];
  is_partial_day_allowed?: boolean | false;
  start_date?: number | null;
  end_date?: number | null;
  start_time?: number | null;
  end_time?: number | null;
  total_leave_duration_mins?: number | null;
  comments?: string | null;
  status?: string | null;
  is_approved?: boolean | null;
  approved_note?: string | null;
  is_rejected?: boolean | null;
  rejected_note?: string | null;
  is_deleted?: boolean | null;
  branch_id?: number | null;
  company_id?: number | null;
  company?: ICompany;
  branch?: IBranch;
  employee?: IEmployee;
  leaveType?: ILeaveType;
}

export class Leave implements ILeave {
  constructor(
    public id?: number,
    public employee_id?: number | null,
    public leave_type_id?: number | null,
    public weekend_day?: number[],
    public is_partial_day_allowed?: boolean | false,
    public start_date?: number | null,
    public end_date?: number | null,
    public start_time?: number | null,
    public end_time?: number | null,
    public total_leave_duration_mins?: number | null,
    public comments?: string | null,
    public status?: string | null,
    public is_approved?: boolean | null,
    public approved_note?: string | null,
    public is_rejected?: boolean | null,
    public rejected_note?: string | null,
    public is_deleted?: boolean | null,
    public branch_id?: number | null,
    public company_id?: number | null,
    public company?: ICompany,
    public branch?: IBranch,
    public employee?: IEmployee,
    public leaveType?: ILeaveType
  ) {
    this.is_deleted = this.is_deleted ?? false;
    this.is_approved = this.is_approved ?? false;
    this.is_rejected = this.is_rejected ?? false;
  }
}

export function getLeaveIdentifier(leave: ILeave): number | undefined {
  return leave.id;
}
