import { Component, Input, ViewEncapsulation } from '@angular/core';
import { Store } from '@ngrx/store';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { of, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { AppState } from 'app/store';

import { IAnnouncement } from './announcement.model';
import { ITimeZone } from 'app/entities/time-zone/time-zone.model';
import { EModalReasonType } from 'app/shared/enum';
import { ApplicationSettings } from 'app/store/states/application-settings.interface';

import { AnnouncementService } from './announcement.service';
import { PermissionService } from 'app/shared/service/permission.service';

import { AnnouncementUpdateDialogComponent } from './update/announcement-update-dialog.component';
import { AnnouncementDeleteDialogComponent } from './delete/announcement-delete-dialog.component';
import { AnnouncementViewDialogComponent } from './view/announcement-view-dialog.component';

import { selectApplicationSettings } from 'app/store/company-settings/company-settings.selectors';

@Component({
  selector: 'hrms-announcement',
  templateUrl: './announcement.component.html',
  styleUrls: ['./announcement.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AnnouncementComponent {
  @Input() announcements: IAnnouncement[] = [];
  timeZone!: ITimeZone;

  private subscriptions: Subscription = new Subscription();

  constructor(
    private store: Store<AppState>,
    private announcementService: AnnouncementService,
    protected modalService: NgbModal,
    private permissionService: PermissionService
  ) {}

  get isEmployee(): boolean {
    return this.permissionService.isEmployee();
  }

  public loadAll(): void {
    this.subscriptions.add(
      this.store
        .select(selectApplicationSettings)
        .pipe(
          switchMap((settings: ApplicationSettings) => {
            if (settings) {
              this.timeZone = settings.time_zone;
              return this.announcementService.getAll();
            }
            return of(null);
          })
        )
        .subscribe(res => {
          if (res?.body) {
            this.announcements = res.body;
          }
        })
    );
  }

  public view(announcement?: IAnnouncement): void {
    const modalRef = this.modalService.open(AnnouncementViewDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.announcement = announcement;
  }

  public update(announcement?: IAnnouncement): void {
    const modalRef = this.modalService.open(AnnouncementUpdateDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.announcement = announcement;
    modalRef.componentInstance.isEmployee = this.isEmployee;
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.CREATED || reason === EModalReasonType.UPDATED) {
        this.loadAll();
      }
    });
  }

  public delete(id?: number | null): void {
    const modalRef = this.modalService.open(AnnouncementDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.announcementId = id;
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.DELETED) {
        this.loadAll();
      }
    });
  }
}
