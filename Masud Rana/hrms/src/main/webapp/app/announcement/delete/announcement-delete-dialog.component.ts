import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { AnnouncementService } from '../announcement.service';

import { EModalReasonType } from 'app/shared/enum';

@Component({
  templateUrl: './announcement-delete-dialog.component.html',
})
export class AnnouncementDeleteDialogComponent {
  announcementId?: number;
  isSaving = false;

  constructor(public activeModal: NgbActiveModal, private announcementService: AnnouncementService, protected fb: FormBuilder) {}

  public cancel(): void {
    this.activeModal.dismiss();
  }

  public delete(): void {
    if (this.announcementId !== undefined) {
      this.announcementService.delete(this.announcementId).subscribe(() => {
        this.activeModal.close(EModalReasonType.DELETED);
      });
    }
  }
}
