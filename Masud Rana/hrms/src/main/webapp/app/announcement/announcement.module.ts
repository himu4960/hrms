import { NgModule } from '@angular/core';

import { SharedModule } from './../shared/shared.module';

import { AnnouncementComponent } from '../announcement/announcement.component';
import { AnnouncementUpdateDialogComponent } from './update/announcement-update-dialog.component';
import { AnnouncementDeleteDialogComponent } from './delete/announcement-delete-dialog.component';
import { AnnouncementViewDialogComponent } from './view/announcement-view-dialog.component';

@NgModule({
  imports: [SharedModule],
  declarations: [
    AnnouncementComponent,
    AnnouncementUpdateDialogComponent,
    AnnouncementDeleteDialogComponent,
    AnnouncementViewDialogComponent,
  ],
  exports: [AnnouncementComponent, AnnouncementUpdateDialogComponent, AnnouncementDeleteDialogComponent, AnnouncementViewDialogComponent],
})
export class AnnouncementModule {}
