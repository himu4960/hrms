import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IAnnouncement } from '../announcement.model';

@Component({
  templateUrl: './announcement-view-dialog.component.html',
})
export class AnnouncementViewDialogComponent {
  announcement?: IAnnouncement;

  constructor(public activeModal: NgbActiveModal) {}

  public cancel(): void {
    this.activeModal.dismiss();
  }
}
