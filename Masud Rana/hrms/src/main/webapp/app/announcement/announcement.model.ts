export interface IAnnouncement {
  id?: number | null;
  title?: string | null;
  description?: string | null;
  branch_id?: number | null;
  created_at?: string | null;
  updated_at?: string | null;
}

export class Announcement implements IAnnouncement {
  constructor(public id?: number, public title?: string, public description?: string, public branch_id?: number) {}
}
