import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { UtilService } from 'app/shared/service/util.service';
import { ApplicationConfigService } from 'app/core/config/application-config.service';

import { IAnnouncement } from './announcement.model';

export type AnnouncementResponseType = HttpResponse<IAnnouncement>;
export type AnnouncementArrayResponseType = HttpResponse<IAnnouncement[]>;

@Injectable({
  providedIn: 'root',
})
export class AnnouncementService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('announcements');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService, private utilService: UtilService) {}

  getAll(): Observable<AnnouncementArrayResponseType> {
    return this.http.get<IAnnouncement[]>(this.resourceUrl, { observe: 'response' });
  }

  create(announcement: IAnnouncement): Observable<AnnouncementResponseType> {
    return this.http.post<IAnnouncement>(this.resourceUrl, announcement, { observe: 'response' });
  }

  update(announcement: IAnnouncement): Observable<AnnouncementResponseType> {
    return this.http.put<IAnnouncement>(`${this.resourceUrl}/${announcement.id}`, announcement, { observe: 'response' });
  }

  delete(announcementId: number): Observable<AnnouncementResponseType> {
    return this.http.delete(`${this.resourceUrl}/${announcementId}`, { observe: 'response' });
  }
}
