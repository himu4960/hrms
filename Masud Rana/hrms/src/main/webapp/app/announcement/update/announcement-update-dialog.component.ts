import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Announcement, IAnnouncement } from '../announcement.model';
import { AnnouncementService } from '../announcement.service';

import { EModalReasonType } from 'app/shared/enum';

import { noWhitespaceValidator } from 'app/shared/validators/no-whitespace.validator';

@Component({
  templateUrl: './announcement-update-dialog.component.html',
})
export class AnnouncementUpdateDialogComponent implements OnInit {
  textAreaMaxLength = 255;
  textCount = 0;
  announcement?: IAnnouncement;
  isEmployee = false;
  isSaving = false;

  addAnnouncementForm = this.fb.group({
    id: [],
    title: [null, [Validators.required, Validators.maxLength(100), noWhitespaceValidator]],
    description: [null],
    branch_id: [null],
  });

  constructor(public activeModal: NgbActiveModal, private announcementService: AnnouncementService, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.updateForm();
  }

  valueChange(): void {
    const value = this.addAnnouncementForm.get('description')!.value;
    this.textCount = value.length;
  }

  public updateForm(): void {
    if (this.announcement !== undefined) {
      this.addAnnouncementForm.patchValue({
        id: this.announcement.id,
        title: this.announcement.title,
        description: this.announcement.description,
        branch_id: this.announcement.branch_id,
      });
    }
  }

  public cancel(): void {
    this.activeModal.dismiss();
  }

  public save(): void {
    this.isSaving = true;
    const announcementData = this.createFromForm();

    if (announcementData.id !== undefined) {
      this.subscribeToSaveResponse(this.announcementService.create(announcementData), EModalReasonType.CREATED);
    } else {
      this.subscribeToSaveResponse(this.announcementService.update(announcementData), EModalReasonType.UPDATED);
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAnnouncement>>, modalAction: string): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(modalAction),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(modalAction: string): void {
    this.activeModal.close(modalAction);
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected createFromForm(): IAnnouncement {
    return {
      ...new Announcement(),
      id: this.addAnnouncementForm.get(['id'])!.value,
      title: this.addAnnouncementForm.get(['title'])!.value,
      description: this.addAnnouncementForm.get(['description'])!.value,
      branch_id: this.addAnnouncementForm.get(['branch_id'])!.value,
    };
  }
}
