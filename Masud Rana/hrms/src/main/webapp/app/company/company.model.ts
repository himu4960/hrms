import { ICompanyType } from 'app/entities/company-type/company-type.model';
import { IApplicationType } from 'app/entities/application-type/application-type.model';

export interface ICompany {
  id?: number;
  name?: string;
  phone?: string;
  email?: string;
  address?: string;
  logo_url_large?: string | null;
  logo_url_medium?: string | null;
  logo_url_small?: string | null;
  status?: boolean | null;
  companyType?: ICompanyType | null;
  applicationType?: IApplicationType | null;
}

export class Company implements ICompany {
  constructor(
    public id?: number,
    public name?: string,
    public phone?: string,
    public email?: string,
    public address?: string,
    public logo_url_large?: string | null,
    public logo_url_medium?: string | null,
    public logo_url_small?: string | null,
    public status?: boolean | null,
    public companyType?: ICompanyType | null,
    public applicationType?: IApplicationType | null
  ) {
    this.status = this.status ?? false;
  }
}

export function getCompanyIdentifier(company: ICompany): number | undefined {
  return company.id;
}
