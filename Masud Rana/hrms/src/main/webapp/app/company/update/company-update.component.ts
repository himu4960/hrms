import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICompany, Company } from '../company.model';
import { ICompanyType } from 'app/entities/company-type/company-type.model';
import { IApplicationType } from 'app/entities/application-type/application-type.model';

import { CompanyService } from '../service/company.service';
import { CompanyTypeService } from 'app/entities/company-type/service/company-type.service';
import { ApplicationTypeService } from 'app/entities/application-type/service/application-type.service';
import { ToastService } from 'app/shared/components/toast/toast.service';

@Component({
  selector: 'hrms-company-update',
  templateUrl: './company-update.component.html',
})
export class CompanyUpdateComponent implements OnInit {
  isSaving = false;
  company!: ICompany;

  companyTypesSharedCollection: ICompanyType[] = [];
  applicationTypesSharedCollection: IApplicationType[] = [];
  file!: any;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(255)]],
    phone: [null, [Validators.required, Validators.maxLength(20)]],
    email: [null, [Validators.required, Validators.maxLength(255)]],
    address: [null, [Validators.required, Validators.maxLength(500)]],
    logo_url_large: [],
    logo_url_medium: [],
    logo_url_small: [],
    status: [true],
    companyType: [],
    applicationType: [],
  });

  constructor(
    protected companyService: CompanyService,
    protected companyTypeService: CompanyTypeService,
    protected applicationTypeService: ApplicationTypeService,
    private toastService: ToastService,
    private translateService: TranslateService,
    protected fb: FormBuilder,
    protected activeModal: NgbActiveModal
  ) {}

  ngOnInit(): void {
    if (this.company !== undefined) {
      this.updateForm(this.company);
    }
    this.loadRelationshipsOptions();
  }

  cancel(): void {
    this.activeModal.dismiss();
  }

  save(): void {
    this.isSaving = true;
    const company = this.createFromForm();
    if (company.id && company.id !== undefined) {
      this.subscribeToSaveResponse(this.companyService.update(company));
    } else {
      this.subscribeToSaveResponse(this.companyService.create(company));
    }
  }

  trackCompanyTypeById(index: number, item: ICompanyType): number {
    return item.id!;
  }

  trackApplicationTypeById(index: number, item: IApplicationType): number {
    return item.id!;
  }

  onFileChange(event: any): void {
    this.file = event.target.files[0];
  }

  onUpload(): void {
    this.isSaving = true;
    const companyId = this.createFromForm().id;
    if (this.file && companyId) {
      this.companyService.uploadLogo(this.file, companyId).subscribe(
        () => {
          this.isSaving = false;
          this.toastService.showSuccessToast(this.translateService.instant('hrmsApp.company.logoUpload.success'));
          this.activeModal.close();
        },
        () => {
          this.isSaving = false;
        }
      );
    } else {
      this.isSaving = false;
      this.toastService.showDangerToast(this.translateService.instant('hrmsApp.company.logoUpload.fail'));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICompany>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    !this.isSaving && this.activeModal.close();
  }

  protected onSaveError(): void {
    // Api for inheritance.
    this.isSaving = false;
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(company: ICompany): void {
    this.editForm.patchValue({
      id: company.id,
      name: company.name,
      phone: company.phone,
      email: company.email,
      address: company.address,
      logo_url_large: company.logo_url_large,
      logo_url_medium: company.logo_url_medium,
      logo_url_small: company.logo_url_small,
      status: company.status,
      companyType: company.companyType,
      applicationType: company.applicationType,
    });

    this.companyTypesSharedCollection = this.companyTypeService.addCompanyTypeToCollectionIfMissing(
      this.companyTypesSharedCollection,
      company.companyType
    );
    this.applicationTypesSharedCollection = this.applicationTypeService.addApplicationTypeToCollectionIfMissing(
      this.applicationTypesSharedCollection,
      company.applicationType
    );
  }

  protected loadRelationshipsOptions(): void {
    this.companyTypeService
      .query()
      .pipe(map((res: HttpResponse<ICompanyType[]>) => res.body ?? []))
      .pipe(
        map((companyTypes: ICompanyType[]) =>
          this.companyTypeService.addCompanyTypeToCollectionIfMissing(companyTypes, this.editForm.get('companyType')!.value)
        )
      )
      .subscribe((companyTypes: ICompanyType[]) => (this.companyTypesSharedCollection = companyTypes));

    this.applicationTypeService
      .query()
      .pipe(map((res: HttpResponse<IApplicationType[]>) => res.body ?? []))
      .pipe(
        map((applicationTypes: IApplicationType[]) =>
          this.applicationTypeService.addApplicationTypeToCollectionIfMissing(applicationTypes, this.editForm.get('applicationType')!.value)
        )
      )
      .subscribe((applicationTypes: IApplicationType[]) => (this.applicationTypesSharedCollection = applicationTypes));
  }

  protected createFromForm(): ICompany {
    return {
      ...new Company(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      phone: this.editForm.get(['phone'])!.value,
      email: this.editForm.get(['email'])!.value,
      address: this.editForm.get(['address'])!.value,
      logo_url_large: this.editForm.get(['logo_url_large'])!.value,
      logo_url_medium: this.editForm.get(['logo_url_medium'])!.value,
      logo_url_small: this.editForm.get(['logo_url_small'])!.value,
      status: this.editForm.get(['status'])!.value,
      companyType: this.editForm.get(['companyType'])!.value,
      applicationType: this.editForm.get(['applicationType'])!.value,
    };
  }
}
