jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';

import { CompanyService } from '../service/company.service';
import { ApplicationTypeService } from 'app/entities/application-type/service/application-type.service';
import { CompanyTypeService } from 'app/entities/company-type/service/company-type.service';

import { Company } from '../company.model';

import { CompanyUpdateComponent } from './company-update.component';

describe('Component Tests', () => {
  describe('Company Management Update Component', () => {
    let comp: CompanyUpdateComponent;
    let fixture: ComponentFixture<CompanyUpdateComponent>;
    let companyService: CompanyService;
    let companyTypeService: CompanyTypeService;
    let applicationTypeService: ApplicationTypeService;
    let mockActiveModal: NgbActiveModal;

    let fakeService: {};

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [CompanyUpdateComponent],
        providers: [FormBuilder, NgbActiveModal, { provide: TranslateService, useValue: fakeService }],
      })
        .overrideTemplate(CompanyUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CompanyUpdateComponent);
      companyService = TestBed.inject(CompanyService);
      companyTypeService = TestBed.inject(CompanyTypeService);
      applicationTypeService = TestBed.inject(ApplicationTypeService);
      mockActiveModal = TestBed.inject(NgbActiveModal);

      comp = fixture.componentInstance;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const company = { id: 123 };
        spyOn(companyService, 'update').and.returnValue(saveSubject);

        // WHEN
        comp.save();
        saveSubject.next(new HttpResponse({ body: company }));
        saveSubject.complete();
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const company = new Company();
        spyOn(companyService, 'create').and.returnValue(saveSubject);
        comp.ngOnInit();

        // WHEN
        comp.save();
        saveSubject.next(new HttpResponse({ body: company }));
        saveSubject.complete();

        // THEN
        expect(companyService.create).toHaveBeenCalled();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackCompanyTypeById', () => {
        it('Should return tracked CompanyType primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackCompanyTypeById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackApplicationTypeById', () => {
        it('Should return tracked ApplicationType primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackApplicationTypeById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
