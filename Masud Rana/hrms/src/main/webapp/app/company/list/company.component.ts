import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { EModalReasonType } from 'app/shared/enum';

import { ICompany } from '../company.model';
import { IUserCompany } from 'app/core/auth/account.model';

import { CompanyService } from '../service/company.service';
import { PermissionService } from 'app/shared/service/permission.service';

import { CompanyDeleteDialogComponent } from '../delete/company-delete-dialog.component';
import { CompanyUpdateComponent } from '../update/company-update.component';

@Component({
  selector: 'hrms-company',
  templateUrl: './company.component.html',
})
export class CompanyComponent implements OnInit {
  userCompanies?: IUserCompany[];
  isLoading = false;

  constructor(protected companyService: CompanyService, protected modalService: NgbModal, protected permissionService: PermissionService) {}

  get isOwner(): boolean {
    return this.permissionService.isOwner() || this.permissionService.isSuperAdmin();
  }

  loadAll(): void {
    this.isLoading = true;

    this.companyService.query().subscribe(
      (res: HttpResponse<IUserCompany[]>) => {
        this.isLoading = false;
        this.userCompanies = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IUserCompany): number {
    return item.company_id!;
  }

  createCompanyModal(company?: ICompany): void {
    const modalRef = this.modalService.open(CompanyUpdateComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.company = company;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(() => {
      this.loadAll();
    });
  }

  delete(company: ICompany): void {
    const modalRef = this.modalService.open(CompanyDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.company = company;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === EModalReasonType.DELETED) {
        this.loadAll();
      }
    });
  }
}
