import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CompanyComponent } from '../list/company.component';
import { CompanyDetailComponent } from '../detail/company-detail.component';
import { CompanyUpdateComponent } from '../update/company-update.component';

import { CompanyRoutingResolveService } from './company-routing-resolve.service';
import { AuthGuard } from 'app/core/auth/guards/auth-guard.service';

const companyRoute: Routes = [
  {
    path: '',
    component: CompanyComponent,
    canActivate: [AuthGuard],
  },
  {
    path: ':id/view',
    component: CompanyDetailComponent,
    resolve: {
      company: CompanyRoutingResolveService,
    },
    canActivate: [AuthGuard],
  },
  {
    path: 'new',
    component: CompanyUpdateComponent,
    resolve: {
      company: CompanyRoutingResolveService,
    },
    canActivate: [AuthGuard],
  },
  {
    path: ':id/edit',
    component: CompanyUpdateComponent,
    resolve: {
      company: CompanyRoutingResolveService,
    },
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(companyRoute)],
  exports: [RouterModule],
})
export class CompanyRoutingModule {}
