SELECT
    hrms_time_clocks.id AS time_clock_id,
    CONCAT(
        hrms_employees.first_name,
        hrms_employees.last_name
    ) AS employee_name,
    hrms_designations.name AS designation_name,
    clock_start_time,
    TIME_FORMAT(clock_start_time, "%h: %i %p") AS clock_in,
    DATE_FORMAT(clock_start_time, "%Y-%m-%d") AS start_date,
    TIME_FORMAT(clock_end_time, "%h: %i %p") AS clock_out,
    DATE_FORMAT(clock_end_time, "%Y-%m-%d") AS end_date,
    total_clock_duration,
    total_break_duration
FROM
    hrms_time_clocks
    INNER JOIN hrms_employees ON hrms_employees.id = hrms_time_clocks.employee_id
    INNER JOIN hrms_designations ON hrms_designations.id = hrms_time_clocks.designation_id
WHERE
    1
    AND clock_start_time BETWEEN '2022-10-19'
    AND '2022-10-30'
GROUP BY
    start_date,
    employee_name
ORDER BY
    start_date,
    employee_name