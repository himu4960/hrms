SELECT pvt.`employee_id`,
    CONCAT(
        hrms_employees.first_name,
        hrms_employees.last_name
    ) AS 'Employee Name',
    SUM(pvt.`total_clock_duration`) AS total_clock_in,
    IF(
        COUNT(pvt.`2022-10-11`) = 0,
        JSON_ARRAY(),
        JSON_ARRAYAGG(pvt.`2022-10-11`)
    ) AS '2022-10-11',
    IF(
        COUNT(pvt.`2022-10-12`) = 0,
        JSON_ARRAY(),
        JSON_ARRAYAGG(pvt.`2022-10-12`)
    ) AS '2022-10-12'
FROM (
        SELECT tcs.`employee_id`,
            tcs.`total_clock_duration`,
            DATE_FORMAT(tcs.`clock_start_time`, "%Y-%m-%d") AS start_date,
            CASE
                WHEN tcs.start_date = '2022-10-11' THEN JSON_OBJECT(
                    'start',
                    tcs.start_time,
                    'end',
                    tcs.end_time,
                    'diff',
                    tcs.total_clock_duration
                )
            END AS "2022-10-11",
            CASE
                WHEN tcs.start_date = '2022-10-12' THEN JSON_OBJECT(
                    'start',
                    tcs.start_time,
                    'end',
                    tcs.end_time,
                    'diff',
                    tcs.total_clock_duration
                )
            END AS "2022-10-12"
        FROM (
                SELECT `employee_id`,
                    `clock_start_time`,
                    TIME_FORMAT(`clock_start_time`, "%h: %i %p") AS start_time,
                    DATE_FORMAT(`clock_start_time`, "%Y-%m-%d") AS start_date,
                    TIME_FORMAT(`clock_end_time`, "%h: %i %p") AS end_time,
                    DATE_FORMAT(`clock_end_time`, "%Y-%m-%d") AS end_date,
                    `total_clock_duration`
                FROM `hrms_time_clocks`
                WHERE 1
                    AND clock_start_time BETWEEN '2022-10-11' AND '2022-10-13'
            ) AS tcs
        ORDER BY `employee_id`,
            `clock_start_time`
    ) AS pvt
    INNER JOIN hrms_employees ON hrms_employees.id = pvt.employee_id
GROUP BY pvt.`employee_id`
