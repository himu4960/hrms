SELECT CONCAT(employees.first_name, employees.last_name) AS employee_name,
    COUNT(work_shifts.id) AS shifts,
    SUM(work_shifts.total_work_shift_duration) AS 'hours_scheduled',
    (
        CASE
            WHEN ISNULL(SUM(time_clocks.total_clock_duration)) THEN 0
            ELSE SUM(time_clocks.total_clock_duration)
        END
    ) AS 'hours_clocked',
    (
        CASE
            WHEN ISNULL(
                SUM (
                    work_shifts.start_date < time_clocks.clock_start_time
                    AND DATE_FORMAT(work_shifts.start_date, "%Y-%m-%d") = DATE_FORMAT(time_clocks.clock_start_time, "%Y-%m-%d")
                )
            ) THEN 0
            ELSE SUM (
                work_shifts.start_date < time_clocks.clock_start_time
                AND DATE_FORMAT(work_shifts.start_date, "%Y-%m-%d") = DATE_FORMAT(time_clocks.clock_start_time, "%Y-%m-%d")
            )
        END
    ) AS late_count,
    (
        CASE
            WHEN ISNULL(SUM (ISNULL(time_clocks.clock_start_time))) THEN 0
            ELSE SUM (ISNULL(time_clocks.clock_start_time))
        END
    ) AS absent_count,
    (
        CASE
            WHEN ISNULL(
                (
                    SUM(time_clocks.total_clock_duration) / SUM(work_shifts.total_work_shift_duration)
                ) * 100
            ) THEN 0
            ELSE (
                SUM(time_clocks.total_clock_duration) / SUM(work_shifts.total_work_shift_duration)
            ) * 100
        END
    ) AS 'clock_vs_shift'
FROM hrms_workshifts work_shifts
    LEFT JOIN hrms_time_clocks time_clocks ON time_clocks.work_shift_id = work_shifts.id
    INNER JOIN hrms_work_shift_employees work_shift_employees ON work_shift_employees.work_shift_id = work_shifts.id
    INNER JOIN hrms_employees employees ON employees.id = work_shift_employees.employee_id
WHERE 1
    AND work_shifts.company_id = 1
    AND work_shifts.start_date BETWEEN '2022-10-16T06:02:38.121Z' AND '2022-10-22T06:02:38.121Z'
GROUP BY work_shift_employees.employee_id