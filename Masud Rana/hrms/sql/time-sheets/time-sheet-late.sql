SELECT employees.id AS 'employee_id',
    designations.name AS 'designation_name',
    CONCAT(employees.first_name, employees.last_name) AS 'employee_name',
    work_shifts.start_date AS date,
    TIME_FORMAT(work_shifts.start_date, "%h: %i %p") AS 'shift_start',
    IF(
        ISNULL(TIME_FORMAT(clock_start_time, "%h: %i %p")),
        '',
        TIME_FORMAT(clock_start_time, "%h: %i %p")
    ) AS 'actual_start',
    TIME_FORMAT(work_shifts.end_date, "%h: %i %p") AS 'shift_end',
    IF(
        ISNULL(TIME_FORMAT(clock_end_time, "%h: %i %p")),
        '',
        TIME_FORMAT(clock_end_time, "%h: %i %p")
    ) AS 'actual_end',
    (
        CASE
            WHEN work_shifts.start_date >= clock_start_time
            AND DATE_FORMAT(work_shifts.start_date, "%Y-%m-%d") = DATE_FORMAT(clock_start_time, "%Y-%m-%d") THEN 'No'
            WHEN work_shifts.start_date < clock_start_time
            AND DATE_FORMAT(work_shifts.start_date, "%Y-%m-%d") = DATE_FORMAT(clock_start_time, "%Y-%m-%d") THEN 'Yes'
            ELSE 'Absent'
        END
    ) AS "late_status",
    IF(
        work_shifts.start_date < clock_start_time
        AND DATE_FORMAT(work_shifts.start_date, "%Y-%m-%d") = DATE_FORMAT(clock_start_time, "%Y-%m-%d"),
        TIMESTAMPDIFF(SECOND, work_shifts.start_date, clock_start_time),
        ''
    ) AS 'late_by'
FROM hrms_workshifts work_shifts
    LEFT JOIN hrms_time_clocks time_clocks ON time_clocks.work_shift_id = work_shifts.id
    INNER JOIN hrms_work_shift_employees work_shift_employees ON work_shift_employees.work_shift_id = work_shifts.id
    INNER JOIN hrms_designations designations ON designations.id = work_shifts.designation_id
    INNER JOIN hrms_employees employees ON employees.id = work_shift_employees.employee_id
WHERE 1
    AND work_shifts.start_date BETWEEN '2022-10-16' AND '2022-10-22'
    AND work_shifts.company_id = 1
ORDER BY employees.id