SELECT pvt.employee_id,
    pvt.employee_name,
    SUM(pvt.total_work_shift_duration) AS total_work_shift_duration,
    IF(
        COUNT(pvt.`2022-11-11`) = 0,
        JSON_ARRAY(),
        JSON_ARRAYAGG(pvt.`2022-11-11`)
    ) AS '2022-11-11',
    IF(
        COUNT(pvt.`2022-11-12`) = 0,
        JSON_ARRAY(),
        JSON_ARRAYAGG(pvt.`2022-11-12`)
    ) AS '2022-11-12'
FROM (
        SELECT work_shift.employee_id,
            work_shift.employee_name,
            work_shift.total_work_shift_duration,
            DATE_FORMAT(work_shift.start_date, "%Y-%m-%d") AS start_date,
            CASE
                WHEN work_shift.start_date = '2022-11-11' THEN JSON_OBJECT(
                    'start',
                    work_shift.start_time,
                    'end',
                    work_shift.end_time,
                    'designation',
                    work_shift.designation_name
                )
            END AS '2022-11-11',
            CASE
                WHEN work_shift.start_date = '2022-11-12' THEN JSON_OBJECT(
                    'start',
                    work_shift.start_time,
                    'end',
                    work_shift.end_time,
                    'designation',
                    work_shift.designation_name
                )
            END AS '2022-11-12'
        FROM (
                SELECT hrms_employees.id AS employee_id,
                    CONCAT(
                        hrms_employees.first_name,
                        hrms_employees.last_name
                    ) AS 'employee_name',
                    TIME_FORMAT(start_date, "%h: %i %p") AS start_time,
                    DATE_FORMAT(start_date, "%Y-%m-%d") AS start_date,
                    TIME_FORMAT(end_date, "%h: %i %p") AS end_time,
                    DATE_FORMAT(end_date, "%Y-%m-%d") AS end_date,
                    total_work_shift_duration,
                    hrms_designations.name AS designation_name
                FROM hrms_workshifts
                    INNER JOIN hrms_work_shift_employees work_shift_employees ON work_shift_employees.work_shift_id = hrms_workshifts.id
                    INNER JOIN hrms_employees ON hrms_employees.id = work_shift_employees.employee_id
                    INNER JOIN hrms_designations ON hrms_designations.id = hrms_workshifts.designation_id
                WHERE 1
                    AND start_date BETWEEN '2022-11-11' AND '2022-11-13'
                    AND hrms_workshifts.company_id = 1
            ) AS work_shift
        ORDER BY start_date
    ) AS pvt
GROUP BY pvt.employee_id
