SELECT branches.id AS branch_id,
    branches.name AS branch_name,
    designations.name AS designation_name,
    SUM(total_work_shift_duration) AS total_work_shift,
    SUM(
        CASE
            WHEN DATE_FORMAT(start_date, "%Y-%m-%d") = '2022-10-17' THEN total_work_shift_duration
            ELSE 0
        END
    ) AS '2022-10-17',
    SUM(
        CASE
            WHEN DATE_FORMAT(start_date, "%Y-%m-%d") = '2022-10-18' THEN total_work_shift_duration
            ELSE 0
        END
    ) AS '2022-10-18',
    SUM(
        CASE
            WHEN DATE_FORMAT(start_date, "%Y-%m-%d") = '2022-10-19' THEN total_work_shift_duration
            ELSE 0
        END
    ) AS '2022-10-19',
    SUM(
        CASE
            WHEN DATE_FORMAT(start_date, "%Y-%m-%d") = '2022-10-20' THEN total_work_shift_duration
            ELSE 0
        END
    ) AS '2022-10-20'
FROM hrms_workshifts work_shift
    INNER JOIN hrms_branches branches ON branches.id = work_shift.branch_id
    INNER JOIN hrms_designations designations ON designations.id = work_shift.designation_id
WHERE 1
    AND start_date BETWEEN '2022-10-17' AND '2022-10-21'
    AND work_shift.company_id = 1
GROUP BY work_shift.branch_id,
    work_shift.designation_id
