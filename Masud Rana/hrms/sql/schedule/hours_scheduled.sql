SELECT
    employees.id AS employee_id,
    CONCAT(
        employees.first_name,
        employees.last_name
    ) AS employee_name,
    SUM(
        work_shifts.total_work_shift_duration
    ) AS work_shift_hours
FROM
    hrms_workshifts work_shifts
INNER JOIN hrms_work_shift_employees work_shift_employees ON
    work_shift_employees.work_shift_id = work_shifts.id
INNER JOIN hrms_employees employees ON
    employees.id = work_shift_employees.employee_id
GROUP BY
    employees.id
ORDER BY
    employees.id
