import { Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OnGatewayConnection, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';

import { JWTService } from '../service/jwt.service';

import { TimeClockRepository } from '../repository/time-clock.repository';

@WebSocketGateway({
    cors: {
        origin: [...process.env.CORS_URL.split(',')],
    },
})
export class SocketGateway implements OnGatewayConnection {
    @WebSocketServer()
    private server: Server;
    private Logger = new Logger('AppGateway');
    private connectedClients = new Map<string, Socket>();

    constructor(
        @InjectRepository(TimeClockRepository) private timeClockRepository: TimeClockRepository,
        private jwtService: JWTService,
    ) {}

    async handleConnection(socket: Socket) {
        try {
            const token = this.sanitizeToken(socket.handshake.headers.authorization);
            const decoded: any = await this.jwtService.decodedJWTToken(token);
            const { company_id } = decoded;

            if (!company_id) {
                this.disconnectClientById(socket.id);
            }
            this.Logger.log(`New client connected...: ${socket.id}`);

            this.connectedClients.set(socket.id, socket);

            socket.join(company_id);
            return socket.to(company_id).emit('connection', {
                client_id: socket.id,
                message: `Successfully Connected ${socket.id}`,
            });
        } catch (error) {
            return this.disconnectClientById(socket.id);
        }
    }

    handleDisconnect(client: Socket) {
        this.connectedClients.delete(client.id);
        this.disconnectClientById(client.id);
        this.Logger.log(`Client disconnected: ${client.id}`);
    }

    disconnectClientById(id: string) {
        const client = this.connectedClients.get(id);

        if (client) {
            client.disconnect();
            this.Logger.log(`Client with ID ${id} disconnected`);
        } else {
            this.Logger.log(`No client found with ID ${id}`);
        }
    }

    @SubscribeMessage('fetchOnline')
    async getAllOnline(socket: Socket): Promise<any> {
        const token = this.sanitizeToken(socket.handshake.headers.authorization);
        const decoded: any = await this.jwtService.decodedJWTToken(token);
        const allTimeClocks = await this.timeClockRepository
            .createQueryBuilder('timeClock')
            .leftJoinAndSelect('timeClock.employee', 'employee')
            .leftJoinAndSelect('timeClock.designation', 'designation')
            .leftJoinAndSelect('timeClock.time_clock_events', 'time_clock_events')
            .where('timeClock.is_running = :is_running', { is_running: true })
            .andWhere('timeClock.clock_end_time is null')
            .andWhere('timeClock.company_id = :company_id', { company_id: decoded.company_id })
            .select([
                'timeClock.id',
                'timeClock.pre_clock_time',
                'timeClock.clock_start_time',
                'timeClock.clock_end_time',
                'timeClock.is_running',
            ])
            .addSelect([
                'employee.id',
                'employee.first_name',
                'employee.last_name',
                'employee.photo_url',
                'time_clock_events.id',
                'time_clock_events.event_type',
                'time_clock_events.break_start_time',
                'time_clock_events.break_end_time',
                'time_clock_events.total_break_duration',
                'designation.id',
                'designation.name',
            ])
            .getMany();

        return this.server.to(decoded.company_id).emit('getAllOnline', allTimeClocks);
    }

    @SubscribeMessage('clockIn')
    async getOnClockIn(socket: Socket, timeClockId: number): Promise<any> {
        const token = this.sanitizeToken(socket.handshake.headers.authorization);
        const decoded: any = await this.jwtService.decodedJWTToken(token);
        const timeClock = await this.timeClockRepository
            .createQueryBuilder('timeClock')
            .leftJoinAndSelect('timeClock.employee', 'employee')
            .leftJoinAndSelect('timeClock.designation', 'designation')
            .leftJoinAndSelect('timeClock.time_clock_events', 'time_clock_events')
            .where('timeClock.id = :id', { id: timeClockId })
            .andWhere('timeClock.is_running = :is_running', { is_running: true })
            .andWhere('timeClock.clock_end_time is null')
            .andWhere('timeClock.company_id = :company_id', { company_id: decoded.company_id })
            .select([
                'timeClock.id',
                'timeClock.pre_clock_time',
                'timeClock.clock_start_time',
                'timeClock.clock_end_time',
                'timeClock.is_running',
            ])
            .addSelect([
                'employee.id',
                'employee.first_name',
                'employee.last_name',
                'time_clock_events.id',
                'time_clock_events.event_type',
                'time_clock_events.break_start_time',
                'time_clock_events.break_end_time',
                'time_clock_events.total_break_duration',
                'designation.id',
                'designation.name',
            ])
            .getOne();
        return this.server.to(decoded.company_id).emit('onClockIn', timeClock);
    }

    @SubscribeMessage('clockOut')
    async getOnClockOut(socket: Socket, timeClockId: number): Promise<any> {
        const token = this.sanitizeToken(socket.handshake.headers.authorization);
        const decoded: any = await this.jwtService.decodedJWTToken(token);
        return this.server.to(decoded.company_id).emit('onClockOut', timeClockId);
    }

    private sanitizeToken(token: any) {
        return token?.replace(/['"]+/g, '');
    }
}
