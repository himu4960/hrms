import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
    HttpStatus,
    ForbiddenException,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { BRANCH_RESPONSE } from './../../shared/constant/response-message';

import { BranchDTO } from '../../service/dto/branch/branch.dto';
import { ResponseDTO } from './../../service/dto/response.dto';
import { BranchResponseDTO } from './../../service/dto/branch/branch-response.dto';

import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';

import { UtilService } from './../../shared/util.service';
import { BranchService } from '../../service/branch.service';
import { JWTService } from './../../service/jwt.service';

@Controller('api/branches')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Branches')
export class BranchController {
    logger = new Logger('BranchController');

    constructor(
        private readonly branchService: BranchService,
        private readonly jwtService: JWTService,
        private readonly utilService: UtilService,
    ) {}

    @Get('/')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: BranchResponseDTO,
        isArray: true,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async getAll(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const results = await this.branchService.findAll(decoded.company_id);
            return this.utilService.getSuccessResponse(results, BRANCH_RESPONSE.READ.SUCCESS, 'BRANCH.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(this.utilService.getErrorResponse(error.message, 'BRANCH.NOT_RETRIEVED'));
        }
    }

    @Get('/:id')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: BranchResponseDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async getOne(@Param('id') id: number): Promise<ResponseDTO> {
        try {
            const findBranch = await this.branchService.findById(id);
            return this.utilService.getSuccessResponse(findBranch, BRANCH_RESPONSE.READ.SUCCESS, 'BRANCH.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(BRANCH_RESPONSE.READ.ERROR, 'BRANCH.NOT_RETRIEVED'),
            );
        }
    }

    @PostMethod('/')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Create branch' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: BranchResponseDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() branchDTO: BranchDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const created = await this.branchService.save(
                { ...branchDTO, company_id: decoded.company_id },
                req.user?.id,
            );
            HeaderUtil.addEntityCreatedHeaders(req.res, 'Branch', created.id);
            return this.utilService.getSuccessResponse(created, BRANCH_RESPONSE.CREATE.SUCCESS, 'BRANCH.CREATED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(BRANCH_RESPONSE.CREATE.ERROR, 'BRANCH.NOT_CREATED'),
            );
        }
    }

    @Put('/:id')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update branch with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: BranchResponseDTO,
    })
    async putId(@Req() req: Request, @Body() branchDTO: BranchDTO): Promise<ResponseDTO> {
        try {
            HeaderUtil.addEntityCreatedHeaders(req.res, 'Branch', branchDTO.id);
            const updatedResult = await this.branchService.update(branchDTO, req.user?.id);
            return this.utilService.getSuccessResponse(updatedResult, BRANCH_RESPONSE.UPDATE.SUCCESS, 'BRANCH.UPDATED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(BRANCH_RESPONSE.UPDATE.ERROR, 'BRANCH.NOT_UPDATED'),
            );
        }
    }

    @Delete('/:id')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Delete branch' })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'The record has been successfully deleted.',
        type: BranchResponseDTO,
    })
    async deleteById(@Param('id') id: number): Promise<ResponseDTO> {
        try {
            const deletedResult = await this.branchService.deleteById(id);
            if (deletedResult) {
                return this.utilService.getSuccessResponse(
                    deletedResult,
                    BRANCH_RESPONSE.DELETE.SUCCESS,
                    'BRANCH.DELETED',
                );
            }
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(BRANCH_RESPONSE.DELETE.ERROR, 'BRANCH.NOT_DELETED'),
            );
        }
    }
}
