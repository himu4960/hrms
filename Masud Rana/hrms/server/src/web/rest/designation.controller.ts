import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
    Query,
    HttpStatus,
    HttpException,
    ForbiddenException,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { DESIGNATION_RESPONSE, SKILLS_RESPONSE } from './../../shared/constant/response-message';

import { BranchResponseDTO } from './../../service/dto/branch/branch-response.dto';
import { ResponseDTO } from './../../service/dto/response.dto';
import { SkillDTO } from './../../service/dto/skill.dto';
import { DesignationDTO } from '../../service/dto/designation/designation.dto';
import { DesignationCreateDTO } from './../../service/dto/designation/designation-create.dto';
import { DesignationUpdateDTO } from './../../service/dto/designation/designation-update.dto';
import { DesignationResponseDTO } from './../../service/dto/designation/designation-response.dto';

import { DesignationService } from '../../service/designation.service';
import { JWTService } from './../../service/jwt.service';

import { PageRequest, Page } from '../../domain/base/pagination.entity';

import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { AuthGuard, RolesGuard } from '../../security';

import { UtilService } from './../../shared/util.service';

@Controller('api/designations')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Designations')
export class DesignationController {
    logger = new Logger('DesignationController');

    constructor(
        private readonly designationService: DesignationService,
        private readonly jwtService: JWTService,
        private readonly utilService: UtilService,
    ) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: DesignationResponseDTO,
    })
    async getAll(@Req() req: Request, @Query('branch_id') branch_id?: number): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
            const [results, count] = await this.designationService.findAndCount(
                {
                    skip: +pageRequest.page * pageRequest.size,
                    take: +pageRequest.size,
                    order: pageRequest.sort.asOrder(),
                },
                decoded.company_id,
                Number(branch_id),
            );
            HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
            return this.utilService.getSuccessResponse(
                results,
                DESIGNATION_RESPONSE.READ.SUCCESS,
                'DESIGNATION.RETRIEVED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(DESIGNATION_RESPONSE.READ.SUCCESS, 'DESIGNATION.NOT_RETRIEVED'),
                HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Get('/visible')
    @ApiResponse({
        status: 200,
        description: 'List all visible designations',
        type: BranchResponseDTO,
        isArray: true,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async getVisibleDesignations(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const results = await this.designationService.getVisibleDesignations(decoded.company_id);
            return this.utilService.getSuccessResponse(
                results,
                DESIGNATION_RESPONSE.READ.SUCCESS,
                'DESIGNATION.RETRIEVED',
            );
        } catch (error) {
            throw new ForbiddenException(this.utilService.getErrorResponse(error.message, 'DESIGNATION.NOT_RETRIEVED'));
        }
    }

    @Get('/skills')
    @ApiResponse({
        status: 200,
        description: 'List all skills records',
        type: SkillDTO,
    })
    async getDesignationBasedSkills(
        @Query('employee_id') employee_id: any,
        @Query('branch_id') branch_id: any,
        @Req() req: Request,
    ): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const results = await this.designationService.getDesignationBasedSkills(
                decoded.company_id,
                employee_id,
                branch_id,
            );
            return this.utilService.getSuccessResponse(results, SKILLS_RESPONSE.READ.SUCCESS, 'SKILLS.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(this.utilService.getErrorResponse(error.message, 'SKILLS.NOT_RETRIEVE'));
        }
    }

    @Get('/visible/:branch_id')
    @ApiResponse({
        status: 200,
        description: 'Get specific branch wise visible designations',
        type: BranchResponseDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async getVisibleDesignationOne(@Param('branch_id') branch_id: number, @Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const results = await this.designationService.getVisibleDesignationOne(decoded.company_id, branch_id);
            return this.utilService.getSuccessResponse(
                results,
                DESIGNATION_RESPONSE.READ.SUCCESS,
                'DESIGNATION.RETRIEVED',
            );
        } catch (error) {
            throw new ForbiddenException(this.utilService.getErrorResponse(error.message, 'DESIGNATION.NOT_RETRIEVED'));
        }
    }

    @Get('/employee/:employee_id')
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: DesignationResponseDTO,
    })
    async getByEmployeeId(@Param('employee_id') employee_id: number, @Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const options = {
                where: {
                    employee_id: employee_id,
                },
            };
            const findDesignation = await this.designationService.findByEmployeeId(options, decoded.company_id);
            return this.utilService.getSuccessResponse(
                findDesignation,
                DESIGNATION_RESPONSE.READ.SUCCESS,
                'DESIGNATION.RETRIEVED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'DESIGNATION.NOT_RETRIEVED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Get('/:id')
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: DesignationResponseDTO,
    })
    async getOne(@Param('id') id: number): Promise<ResponseDTO> {
        try {
            const findDesignation = await this.designationService.findById(id);
            return this.utilService.getSuccessResponse(
                findDesignation,
                DESIGNATION_RESPONSE.READ.SUCCESS,
                'DESIGNATION.RETRIEVED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(DESIGNATION_RESPONSE.READ.ERROR, 'DESIGNATION.NOT_RETRIEVED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @PostMethod('/')
    @ApiOperation({ title: 'Create designation' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: DesignationDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() designationDTO: DesignationCreateDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const created = await this.designationService.save(designationDTO, decoded.company_id, req.user?.id);
            HeaderUtil.addEntityCreatedHeaders(req.res, 'Designation', created.id);
            return this.utilService.getSuccessResponse(
                created,
                DESIGNATION_RESPONSE.CREATE.SUCCESS,
                'DESIGNATION.CREATED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(DESIGNATION_RESPONSE.CREATE.ERROR, 'DESIGNATION.NOT_CREATED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Put('/')
    @ApiOperation({ title: 'Update designation' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: DesignationDTO,
    })
    async put(@Req() req: Request, @Body() designationDTO: DesignationUpdateDTO): Promise<ResponseDTO> {
        try {
            HeaderUtil.addEntityCreatedHeaders(req.res, 'Designation', designationDTO.id);
            const updatedResult = await this.designationService.update(designationDTO, req.user?.id);
            return this.utilService.getSuccessResponse(
                updatedResult,
                DESIGNATION_RESPONSE.UPDATE.SUCCESS,
                'DESIGNATION.UPDATED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(DESIGNATION_RESPONSE.UPDATE.ERROR, 'DESIGNATION.NOT_UPDATED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Put('/:id')
    @ApiOperation({ title: 'Update designation with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: DesignationDTO,
    })
    async putId(@Req() req: Request, @Body() designationDTO: DesignationUpdateDTO): Promise<ResponseDTO> {
        try {
            HeaderUtil.addEntityCreatedHeaders(req.res, 'Designation', designationDTO.id);
            const updatedResult = await this.designationService.update(designationDTO, req.user?.id);
            return this.utilService.getSuccessResponse(
                updatedResult,
                DESIGNATION_RESPONSE.UPDATE.SUCCESS,
                'DESIGNATION.UPDATED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(DESIGNATION_RESPONSE.UPDATE.ERROR, 'DESIGNATION.NOT_UPDATED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Delete('/:id')
    @ApiOperation({ title: 'Delete designation' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Param('id') id: number): Promise<ResponseDTO> {
        try {
            const deletedResult = await this.designationService.deleteById(id);
            return this.utilService.getSuccessResponse(
                deletedResult,
                DESIGNATION_RESPONSE.DELETE.SUCCESS,
                'DESIGNATION.DELETED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(DESIGNATION_RESPONSE.DELETE.ERROR, 'DESIGNATION.NOT_DELETED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }
}
