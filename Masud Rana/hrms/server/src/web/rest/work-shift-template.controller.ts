import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    UseGuards,
    Req,
    UseInterceptors,
    HttpException,
    HttpStatus,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { WorkShiftTemplateDTO } from '../../service/dto/work-shift-template.dto';
import { ResponseDTO } from '../../service/dto/response.dto';

import { WorkShiftTemplateService } from '../../service/work-shift-template.service';
import { JWTService } from '../../service/jwt.service';
import { UtilService } from '../../shared/util.service';

import { PageRequest, Page } from '../../domain/base/pagination.entity';
import { AuthGuard, RolesGuard } from '../../security';

import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { WORK_SHIFT_TEMPLATE_RESPONSE } from '../../shared/constant/response-message';

@Controller('api/work-shift-templates')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Workshift Templates')
export class WorkShiftTemplateController {
    logger = new Logger('WorkShiftTemplateController');

    constructor(
        private readonly workShiftTemplateService: WorkShiftTemplateService,
        private utilService: UtilService,
        private readonly jwtService: JWTService,
    ) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: WorkShiftTemplateDTO,
    })
    async getAll(@Req() req: Request): Promise<WorkShiftTemplateDTO[]> {
        const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
        const [results, count] = await this.workShiftTemplateService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: WorkShiftTemplateDTO,
    })
    async getOne(@Param('id') id: number): Promise<WorkShiftTemplateDTO> {
        return await this.workShiftTemplateService.findById(id);
    }

    @PostMethod('/')
    @ApiOperation({ title: 'Create workShiftTemplate' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: WorkShiftTemplateDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() workShiftTemplateDTO: WorkShiftTemplateDTO): Promise<WorkShiftTemplateDTO> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const created = await this.workShiftTemplateService.save(
            { ...workShiftTemplateDTO, company_id: decoded.company_id },
            req.user?.id,
        );
        HeaderUtil.addEntityCreatedHeaders(req.res, 'WorkShiftTemplate', created.id);
        return created;
    }

    @Delete('/:id')
    @ApiOperation({ title: 'Delete workShiftTemplate' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: number): Promise<ResponseDTO> {
        HeaderUtil.addEntityDeletedHeaders(req.res, 'WorkShiftTemplate', id);
        try {
            const response = await this.workShiftTemplateService.deleteById(id);
            return this.utilService.getSuccessResponse(
                response,
                WORK_SHIFT_TEMPLATE_RESPONSE.DELETE.SUCCESS,
                'DELETE_SUCCESS',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(
                    WORK_SHIFT_TEMPLATE_RESPONSE.DELETE.ERROR,
                    'WORK_SHIFT_TEMPLATE.NOT_DELETED',
                ),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }
}
