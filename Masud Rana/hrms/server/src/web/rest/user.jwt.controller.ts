import { ResponseDTO } from './../../service/dto/response.dto';
import { Response, Request } from 'express';
import { Body, Controller, Logger, Post, Res, Req, UseInterceptors, ForbiddenException } from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { UserLoginDTO } from '../../service/dto/user-login.dto';

import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { AuthService } from '../../service/auth.service';
import { UtilService } from '../../shared/util.service';

import { LOGIN_RESPONSE } from './../../shared/constant/response-message';

@Controller('api')
@UseInterceptors(LoggingInterceptor)
@ApiUseTags('User Login')
export class UserJWTController {
    logger = new Logger('UserJWTController');

    constructor(private readonly authService: AuthService, private readonly utilService: UtilService) { }

    @Post('/login')
    @ApiOperation({ title: 'Authorization api retrieving token' })
    @ApiResponse({
        status: 201,
        description: 'Authorized',
    })
    async authorize(@Body() user: UserLoginDTO, @Res() res: Response): Promise<any> {
        try {
            const jwt = await this.authService.login(user);
            if (jwt && jwt.token && jwt.refresh_token) {
                res.setHeader('Authorization', 'Bearer ' + jwt.token);
                return res.json(this.utilService.getSuccessResponse(jwt, LOGIN_RESPONSE.SUCCESS, 'USER.SUCCESS'));
            } else {
                const errorResponse = {
                    code: jwt.code,
                    message: jwt.message,
                    translateCode: 'USER.NOT_LOGIN',
                };
                throw new ForbiddenException(errorResponse);
            }
        } catch (error) {
            const errorResponse = {
                data: { code: error.response?.code, token: error.response?.token },
                message: error.message,
                translateCode: 'USER.NOT_LOGIN',
            };
            throw new ForbiddenException(errorResponse);
        }
    }
}
