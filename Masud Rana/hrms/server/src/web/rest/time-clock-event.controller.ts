import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { TimeClockEventDTO } from '../../service/dto/time-clock/time-clock-event.dto';
import { TimeClockEventNoteDTO } from './../../service/dto/time-clock/time-clock-event-note.dto';
import { TimeClockEventBreakDTO } from './../../service/dto/time-clock/time-clock-event-break.dto';
import { TimeClockEventPositionDTO } from './../../service/dto/time-clock/time-clock-event-position.dto';
import { TimeClockEventPositionUpdateDTO } from '../../service/dto/time-clock/time-clock-event-position-update.dto';

import { PageRequest, Page } from '../../domain/base/pagination.entity';

import { AuthGuard, RolesGuard } from '../../security';

import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { JWTService } from './../../service/jwt.service';
import { TimeClockEventsService } from './../../service/time-clock-events.service';

@Controller('api/time-clock-events')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Time Clock Events')
export class TimeClockEventsController {
    logger = new Logger('TimeClockEventsController');
    constructor(
        private readonly timeClockEventService: TimeClockEventsService,
        private readonly jwtService: JWTService,
    ) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: TimeClockEventDTO,
    })
    async getAll(@Req() req: Request): Promise<TimeClockEventDTO[]> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
        const [results, count] = await this.timeClockEventService.findAndCount(
            {
                skip: +pageRequest.page * pageRequest.size,
                take: +pageRequest.size,
                order: pageRequest.sort.asOrder(),
            },
            decoded.company_id,
        );
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: TimeClockEventDTO,
    })
    async getOne(@Param('id') id: number): Promise<TimeClockEventDTO> {
        return await this.timeClockEventService.findById(id);
    }

    @PostMethod('/break')
    @ApiOperation({ title: 'Create timeClockEvent break start' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: TimeClockEventDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async startBreakTimeEvent(
        @Req() req: Request,
        @Body() timeClockEventDTO: TimeClockEventBreakDTO,
    ): Promise<TimeClockEventDTO> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const created = await this.timeClockEventService.startBreakTimeEvent(
            { ...timeClockEventDTO, created_by: req.user?.id },
            decoded.company_id,
        );
        HeaderUtil.addEntityCreatedHeaders(req.res, 'TimeClockEvent', created?.id);
        return created;
    }

    @Put('/:id/break')
    @ApiOperation({ title: 'Update timeClockEvent with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: TimeClockEventDTO,
    })
    async endBreakTimeEvent(@Req() req: Request, @Param('id') id: number): Promise<TimeClockEventDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'TimeClockEvent', id);
        return await this.timeClockEventService.endBreakTimeEvent({
            id,
            created_by: req.user?.id,
        });
    }

    @PostMethod('/position')
    @ApiOperation({ title: 'Create timeClockEvent position' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: TimeClockEventDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async addPositionEvent(
        @Req() req: Request,
        @Body() timeClockEventDTO: TimeClockEventPositionDTO,
    ): Promise<TimeClockEventDTO> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const created = await this.timeClockEventService.addPositionEvent(
            { ...timeClockEventDTO, created_by: req.user?.id },
            decoded.company_id,
        );
        HeaderUtil.addEntityCreatedHeaders(req.res, 'TimeClockEvent', created?.id);
        return created;
    }

    @Put('/:id/position')
    @ApiOperation({ title: 'Update timeClockEvent position with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: TimeClockEventDTO,
    })
    async updatePositionEvent(
        @Req() req: Request,
        @Param('id') id: number,
        @Body() timeClockEventDTO: TimeClockEventPositionUpdateDTO,
    ): Promise<TimeClockEventDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'TimeClockEvent', timeClockEventDTO.id);
        return await this.timeClockEventService.updatePositionEvent({
            ...timeClockEventDTO,
            id,
            created_by: req.user?.id,
        });
    }

    @PostMethod('/note')
    @ApiOperation({ title: 'Create timeClockEvent note' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: TimeClockEventDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async addNoteEvent(
        @Req() req: Request,
        @Body() timeClockEventDTO: TimeClockEventNoteDTO,
    ): Promise<TimeClockEventDTO> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const created = await this.timeClockEventService.addNoteEvent(
            { ...timeClockEventDTO, created_by: req.user?.id },
            decoded.company_id,
        );
        HeaderUtil.addEntityCreatedHeaders(req.res, 'TimeClockEvent', created?.id);
        return created;
    }

    @Put('/:id/note/')
    @ApiOperation({ title: 'Update timeClockEvent note with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: TimeClockEventDTO,
    })
    async updateNoteEvent(
        @Req() req: Request,
        @Param('id') id: number,
        @Body() timeClockEventDTO: TimeClockEventNoteDTO,
    ): Promise<TimeClockEventDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'TimeClockEvent', timeClockEventDTO.id);
        return await this.timeClockEventService.updateNoteEvent({
            ...timeClockEventDTO,
            id,
            created_by: req.user?.id,
        });
    }

    @Delete('/:id')
    @ApiOperation({ title: 'Delete timeClockEvent' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Param('id') id: number): Promise<TimeClockEventDTO> {
        return await this.timeClockEventService.deleteById(id);
    }
}
