import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
    HttpException,
    HttpStatus,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { HolidayDTO } from '../../service/dto/holiday.dto';
import { ResponseDTO } from 'src/service/dto/response.dto';

import { HolidayService } from '../../service/holiday.service';
import { UtilService } from '../../shared/util.service';

import { AuthGuard } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { HOLIDAY_RESPONSE } from '../../shared/constant/response-message';

@Controller('api/holidays')
@UseGuards(AuthGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Holidays')
export class HolidayController {
    logger = new Logger('HolidayController');

    constructor(private readonly holidayService: HolidayService, private readonly utilService: UtilService) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: HolidayDTO,
    })
    async getAll(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const [results, count] = await this.holidayService.findAndCount({});
            return this.utilService.getSuccessResponse(
                results,
                HOLIDAY_RESPONSE.READ.SUCCESS,
                'DATA_RETRIEVED_SUCCESS',
            );
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'DATA_NOT_RETRIEVED');
        }
    }

    @Get('/:id')
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: HolidayDTO,
    })
    async getOne(@Param('id') id: number): Promise<ResponseDTO> {
        try {
            const results = await this.holidayService.findById(id);
            return this.utilService.getSuccessResponse(
                results,
                HOLIDAY_RESPONSE.READ.SUCCESS,
                'DATA_RETRIEVED_SUCCESS',
            );
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'DATA_NOT_RETRIEVED');
        }
    }

    @PostMethod('/')
    @ApiOperation({ title: 'Create holidays' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: HolidayDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() holidayDTO: HolidayDTO[]): Promise<ResponseDTO> {
        try {
            const response = await this.holidayService.save(holidayDTO, req.user?.id);
            return this.utilService.getSuccessResponse(response, HOLIDAY_RESPONSE.CREATE.SUCCESS, 'CREATE_SUCCESS');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'CREATE_NOT_SUCCESS');
        }
    }

    @Put('/')
    @ApiOperation({ title: 'Update holiday' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: HolidayDTO,
    })
    async put(@Req() req: Request, @Body() holidayDTO: HolidayDTO): Promise<ResponseDTO> {
        try {
            HeaderUtil.addEntityCreatedHeaders(req.res, 'Holiday', holidayDTO.id);
            const response = await this.holidayService.update(holidayDTO, req.user?.updated_by);
            return this.utilService.getSuccessResponse(response, HOLIDAY_RESPONSE.UPDATE.SUCCESS, 'UPDATE_SUCCESS');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'UPDATE_NOT_SUCCESS');
        }
    }

    @Put('/:id')
    @ApiOperation({ title: 'Update holiday with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: HolidayDTO,
    })
    async putId(@Req() req: Request, @Body() holidayDTO: HolidayDTO): Promise<ResponseDTO> {
        try {
            HeaderUtil.addEntityCreatedHeaders(req.res, 'Holiday', holidayDTO.id);
            const response = await this.holidayService.update(holidayDTO, req.user?.id);
            return this.utilService.getSuccessResponse(response, HOLIDAY_RESPONSE.UPDATE.SUCCESS, 'UPDATE_SUCCESS');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'UPDATE_NOT_SUCCESS');
        }
    }

    @Delete('/:id')
    @ApiOperation({ title: 'Delete holiday' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: number): Promise<ResponseDTO> {
        HeaderUtil.addEntityDeletedHeaders(req.res, 'Holiday', id);
        try {
            const response = await this.holidayService.deleteById(id);
            return this.utilService.getSuccessResponse(response, HOLIDAY_RESPONSE.DELETE.SUCCESS, 'DELETE_SUCCESS');
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(HOLIDAY_RESPONSE.DELETE.ERROR, 'HOLIDAY.NOT_DELETED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }
}
