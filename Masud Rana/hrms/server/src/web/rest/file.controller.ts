import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    UseGuards,
    Req,
    UseInterceptors,
    UploadedFile,
    ForbiddenException,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';

import { FILE_RESPONSE } from './../../shared/constant/response-message';

import { FileDTO } from '../../service/dto/file.dto';
import { ResponseDTO } from 'src/service/dto/response.dto';

import { FileService } from '../../service/file.service';
import { UtilService } from './../../shared/util.service';
import { JWTService } from './../../service/jwt.service';

import { PageRequest, Page } from '../../domain/base/pagination.entity';

import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/files')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('files')
export class FileController {
    logger = new Logger('FileController');

    constructor(
        private readonly fileService: FileService,
        private readonly utilService: UtilService,
        private readonly jwtService: JWTService,
    ) {}

    @Get('/')
    @Roles(RoleType.SUPER_ADMIN, RoleType.OWNER, RoleType.MANAGER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: FileDTO,
    })
    async getAll(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
            const [results, count] = await this.fileService.findAndCount(
                {
                    skip: +pageRequest.page * pageRequest.size,
                    take: +pageRequest.size,
                    order: pageRequest.sort.asOrder(),
                },
                decoded.company_id,
            );
            HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
            return this.utilService.getSuccessResponse(results, FILE_RESPONSE.READ.SUCCESS, 'FILE.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(FILE_RESPONSE.READ.ERROR, 'FILE.NOT_RETRIEVED'),
            );
        }
    }

    @PostMethod('/upload')
    @Roles(RoleType.SUPER_ADMIN, RoleType.OWNER, RoleType.MANAGER)
    @ApiOperation({ title: 'Create file' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: FileDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @UseInterceptors(FileInterceptor('file'))
    async post(
        @Req() req: Request,
        @UploadedFile() file: Express.Multer.File,
        @Body() fileDTO: any,
    ): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const created = await this.fileService.save(
                file,
                {
                    ...fileDTO,
                    file_size: file.size,
                    mimetype: file.mimetype,
                    orginal_filename: file.originalname,
                },
                decoded.company_id,
            );
            return this.utilService.getSuccessResponse(created, FILE_RESPONSE.CREATE.SUCCESS, 'FILE.UPLOADED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(FILE_RESPONSE.CREATE.ERROR, 'FILE.NOT_UPLOADED'),
            );
        }
    }

    @PostMethod('/download')
    @Roles(RoleType.SUPER_ADMIN, RoleType.OWNER, RoleType.MANAGER)
    @ApiOperation({ title: 'Create file' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: FileDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @UseInterceptors(FileInterceptor('file'))
    async download(@Body() fileDTO: any): Promise<any> {
        try {
            const created = await this.fileService.download(fileDTO);
            return this.utilService.getSuccessResponse(created, FILE_RESPONSE.DOWNLOAD.SUCCESS, 'FILE.DOWNLOADED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(FILE_RESPONSE.DOWNLOAD.ERROR, 'FILE.NOT_DOWNLOADED'),
            );
        }
    }

    @Delete('/:id')
    @Roles(RoleType.SUPER_ADMIN, RoleType.OWNER, RoleType.MANAGER)
    @ApiOperation({ title: 'Delete file' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: number): Promise<ResponseDTO> {
        try {
            await this.fileService.deleteById(id);
            return this.utilService.getSuccessResponse(null, FILE_RESPONSE.DELETE.SUCCESS, 'FILE.DELETED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(FILE_RESPONSE.DELETE.ERROR, 'FILE.NOT_DELETED'),
            );
        }
    }
}
