import {
    ClassSerializerInterceptor,
    Controller,
    Get,
    Logger,
    Param,
    Req,
    UseInterceptors,
    ForbiddenException,
} from '@nestjs/common';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

import { TIME_ZONE_RESPONSE } from './../../shared/constant/response-message';

import { TimeZoneDTO } from '../../service/dto/time-zone.dto';
import { ResponseDTO } from 'src/service/dto/response.dto';

import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { UtilService } from './../../shared/util.service';
import { TimeZoneService } from '../../service/time-zone.service';

@Controller('api/time-zones')
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiUseTags('Time Zones')
export class TimeZoneController {
    logger = new Logger('TimeZoneController');

    constructor(private readonly timeZoneService: TimeZoneService, private readonly utilService: UtilService) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List all active time zone records',
        type: TimeZoneDTO,
    })
    async getActiveTimeZones(): Promise<ResponseDTO> {
        try {
            const data = await this.timeZoneService.getActiveTimeZones();
            return this.utilService.getSuccessResponse(data, TIME_ZONE_RESPONSE.READ.SUCCESS, 'TIMEZONE.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(TIME_ZONE_RESPONSE.READ.ERROR, 'TIMEZONE.NOT_RETRIEVED'),
            );
        }
    }

    @Get('/all')
    @ApiResponse({
        status: 200,
        description: 'List all time zone records',
        type: TimeZoneDTO,
    })
    async getAll(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const data = await this.timeZoneService.getAll();
            return this.utilService.getSuccessResponse(data, TIME_ZONE_RESPONSE.READ.SUCCESS, 'TIMEZONE.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(TIME_ZONE_RESPONSE.READ.ERROR, 'TIMEZONE.NOT_RETRIEVED'),
            );
        }
    }

    @Get('/:id')
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: TimeZoneDTO,
    })
    async getOne(@Param('id') id: number): Promise<ResponseDTO> {
        try {
            const data = await this.timeZoneService.getTimeZoneById(id);
            return this.utilService.getSuccessResponse(data, TIME_ZONE_RESPONSE.READ.SUCCESS, 'TIMEZONE.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(TIME_ZONE_RESPONSE.READ.ERROR, 'TIMEZONE.NOT_RETRIEVED'),
            );
        }
    }
}
