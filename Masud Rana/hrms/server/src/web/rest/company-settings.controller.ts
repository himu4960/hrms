import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Get,
    Logger,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
    HttpException,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { SettingsTimeClockDTO } from '../../service/dto/company-settings/settings-time-clock.dto';
import { SettingsTimeClockUpdateDTO } from '../../service/dto/company-settings/settings-time-clock-update.dto';
import { SettingsApplicationUpdateDTO } from './../../service/dto/company-settings/settings-application-update.dto';
import { SettingsApplicationDTO } from './../../service/dto/company-settings/settings-application.dto';
import { SettingsShiftPlanningDTO } from './../../service/dto/company-settings/settings-shift-planning.dto';
import { SettingsShiftPlanningUpdateDTO } from './../../service/dto/company-settings/settings-shift-planning-update.dto';
import { SettingsLeaveAvailabilityDTO } from './../../service/dto/company-settings/settings-leave-availability.dto';

import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';

import { CompanySettingsService } from '../../service/company-settings.service';
import { JWTService } from './../../service/jwt.service';
import { UtilService } from '../../shared/util.service';

import { ResponseDTO } from 'src/service/dto/response.dto';
import { APPLICATION_SETTING_RESPONSE, SHIFT_PLANNING_SETTING_RESPONSE } from '../../shared/constant/response-message';

@Controller('api/company-settings')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Company Settings')
export class CompanySettingsController {
    logger = new Logger('CompanySettingsController');

    constructor(
        private readonly companySettingsService: CompanySettingsService,
        private readonly jwtService: JWTService,
        private readonly utilService: UtilService
    ) { }

    @Get('/application')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Get application with company id' })
    @ApiResponse({
        status: 200,
        description: 'Application record',
        type: SettingsApplicationDTO,
    })
    async getApplicationByCompanyId(@Req() req: Request): Promise<SettingsApplicationDTO> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        return await this.companySettingsService.getApplicationByCompanyId(decoded.company_id);
    }

    @Put('/application')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update application with company id' })
    @ApiResponse({
        status: 201,
        description: 'Application has been successfully updated.',
        type: SettingsApplicationUpdateDTO,
    })
    async updateApplicationByCompanyId(
        @Req() req: Request,
        @Body() applicationDTO: SettingsApplicationUpdateDTO,
    ): Promise<ResponseDTO> {
        try {
            HeaderUtil.addEntityCreatedHeaders(req.res, 'Application', applicationDTO.id);
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);

            const updatedResult = await this.companySettingsService.updateApplicationByCompanyId(
                applicationDTO,
                decoded.company_id,
                req.user?.id,
            );

            if (updatedResult) {
                return this.utilService.getSuccessResponse(updatedResult, APPLICATION_SETTING_RESPONSE.UPDATE.SUCCESS, 'APPLICATION_SETTING.UPDATE');
            }

            return this.utilService.getErrorResponse(APPLICATION_SETTING_RESPONSE.UPDATE.ERROR, 'APPLICATION_SETTING.NOT_UPDATE');
        }
        catch (error) {
            throw new HttpException(error.response, error.status);
        }
    }

    @Get('/shift-planning')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Get shift planning with company id' })
    @ApiResponse({
        status: 200,
        description: 'Shift planning record',
        type: SettingsShiftPlanningDTO,
    })
    async getShiftPlanningByCompanyId(@Req() req: Request): Promise<SettingsShiftPlanningDTO> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        return await this.companySettingsService.getShiftPlanningByCompanyId(decoded.company_id);
    }

    @Put('/shift-planning')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update shift planning with company id' })
    @ApiResponse({
        status: 201,
        description: 'Shift planning has been successfully updated.',
        type: SettingsShiftPlanningUpdateDTO,
    })
    async updateShiftPlanningByCompanyId(
        @Req() req: Request,
        @Body() shiftPlanningDTO: SettingsShiftPlanningUpdateDTO,
    ): Promise<ResponseDTO> {
        try {
            HeaderUtil.addEntityCreatedHeaders(req.res, 'Shift Planning', shiftPlanningDTO.id);
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const updatedResult = await this.companySettingsService.updateShiftPlanningByCompanyId(
                shiftPlanningDTO,
                decoded.company_id,
                req.user?.id,
            );

            if (updatedResult) {
                return this.utilService.getSuccessResponse(updatedResult, SHIFT_PLANNING_SETTING_RESPONSE.UPDATE.SUCCESS, 'SHIFT_PLANNING_SETTING.UPDATE');
            }
            return this.utilService.getErrorResponse(SHIFT_PLANNING_SETTING_RESPONSE.UPDATE.ERROR, 'SHIFT_PLANNING_SETTING.NOT_UPDATE');
        } catch (error) {
            throw new HttpException(error.response, error.status);
        }
    }

    @Get('/time-clocks')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Get timeClock with company id' })
    @ApiResponse({
        status: 200,
        description: 'Time clock record',
        type: SettingsTimeClockDTO,
    })
    async getTimeClockByCompanyId(@Req() req: Request): Promise<SettingsTimeClockDTO> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        return await this.companySettingsService.getTimeClockByCompanyId(decoded.company_id);
    }

    @Put('/time-clocks')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update timeClock with company id' })
    @ApiResponse({
        status: 201,
        description: 'Time clock has been successfully updated.',
        type: SettingsTimeClockDTO,
    })
    async updateTimeClockByCompanyId(
        @Req() req: Request,
        @Body() timeClockDTO: SettingsTimeClockUpdateDTO,
    ): Promise<ResponseDTO> {
        try {
            HeaderUtil.addEntityCreatedHeaders(req.res, 'TimeClock', timeClockDTO.id);
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const updatedResult = await this.companySettingsService.updateTimeClockByCompanyId(
                timeClockDTO,
                decoded.company_id,
                req.user?.id,
            );
            if (updatedResult) {
                const updatedResponse = {
                    data: updatedResult,
                    message: `Time clock setting updated successfully`,
                };
                return updatedResponse;
            }
            const errorResponse = {
                message: `Time clock setting cannot be updated`,
            };
            return errorResponse;
        } catch (error) {
            throw new HttpException(error.response, error.status);
        }
    }

    @Get('/leave-availability')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Get leave availability with company id' })
    @ApiResponse({
        status: 200,
        description: 'Leave availability record',
        type: SettingsLeaveAvailabilityDTO,
    })
    async getLeaveAvailabilityByCompanyId(@Req() req: Request): Promise<SettingsLeaveAvailabilityDTO> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        return await this.companySettingsService.getLeaveAvailabilityByCompanyId(decoded.company_id);
    }

    @Put('/leave-availability')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update Leave availability with company id' })
    @ApiResponse({
        status: 201,
        description: 'Leave availability has been successfully updated.',
        type: SettingsLeaveAvailabilityDTO,
    })
    async updateLeaveAvailabilityByCompanyId(
        @Req() req: Request,
        @Body() leaveAvailabilityDTO: SettingsLeaveAvailabilityDTO,
    ): Promise<ResponseDTO> {
        try {
            HeaderUtil.addEntityCreatedHeaders(req.res, 'TimeClock', leaveAvailabilityDTO.id);
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const updatedResult = await this.companySettingsService.updateLeaveAvailabilityByCompanyId(
                leaveAvailabilityDTO,
                decoded.company_id,
                req.user?.id,
            );
            if (updatedResult) {
                const updatedResponse = {
                    data: updatedResult,
                    message: `Leave availability setting updated successfully`,
                };
                return updatedResponse;
            }
            const errorResponse = {
                message: `Leave availability setting cannot be updated`,
            };
            return errorResponse;
        } catch (error) {
            throw new HttpException(error.response, error.status);
        }
    }
}
