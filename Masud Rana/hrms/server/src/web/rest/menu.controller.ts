import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';

import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';

import { MenuDTO } from '../../service/dto/menu.dto';
import { MenuService } from '../../service/menu.service';

@Controller('api/menus')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Menu')
export class MenuController {
    logger = new Logger('MenuController');

    constructor(private readonly menuService: MenuService) { }

    @Get('/')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: MenuDTO,
    })
    async getAll(): Promise<MenuDTO[]> {
        const [results, count] = await this.menuService.findAndCount();
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: MenuDTO,
    })
    async getOne(@Param('id') id: number): Promise<MenuDTO> {
        return await this.menuService.findById(id);
    }

    @PostMethod('/')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Create menu' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: MenuDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() menuDTO: MenuDTO): Promise<MenuDTO> {
        const created = await this.menuService.save(menuDTO, req.user?.id);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Menu', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update menu' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: MenuDTO,
    })
    async put(@Req() req: Request, @Body() menuDTO: MenuDTO): Promise<MenuDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Menu', menuDTO.id);
        return await this.menuService.update(menuDTO, req.user?.id);
    }

    @Put('/:id')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update menu with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: MenuDTO,
    })
    async putId(@Req() req: Request, @Body() menuDTO: MenuDTO): Promise<MenuDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Menu', menuDTO.id);
        return await this.menuService.update(menuDTO, req.user?.id);
    }

    @Delete('/:id')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Delete menu' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: number): Promise<MenuDTO> {
        return await this.menuService.deleteById(id);
    }
}
