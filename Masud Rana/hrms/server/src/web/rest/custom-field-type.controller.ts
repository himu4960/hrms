import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { CustomFieldTypeDTO } from '../../service/dto/custom-field-type.dto';

import { CustomFieldTypeService } from '../../service/custom-field-type.service';

import { PageRequest, Page } from '../../domain/base/pagination.entity';

import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/custom-field-types')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Custom Field Types')
export class CustomFieldTypeController {
    logger = new Logger('CustomFieldTypeController');

    constructor(private readonly customFieldTypeService: CustomFieldTypeService) {}

    @Get('/')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: CustomFieldTypeDTO,
    })
    async getAll(@Req() req: Request): Promise<CustomFieldTypeDTO[]> {
        const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
        const [results, count] = await this.customFieldTypeService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: CustomFieldTypeDTO,
    })
    async getOne(@Param('id') id: number): Promise<CustomFieldTypeDTO> {
        return await this.customFieldTypeService.findById(id);
    }

    @PostMethod('/')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Create customFieldType' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: CustomFieldTypeDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() customFieldTypeDTO: CustomFieldTypeDTO): Promise<CustomFieldTypeDTO> {
        const created = await this.customFieldTypeService.save(customFieldTypeDTO);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'CustomFieldType', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update customFieldType' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: CustomFieldTypeDTO,
    })
    async put(@Req() req: Request, @Body() customFieldTypeDTO: CustomFieldTypeDTO): Promise<CustomFieldTypeDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'CustomFieldType', customFieldTypeDTO.id);
        return await this.customFieldTypeService.update(customFieldTypeDTO);
    }

    @Put('/:id')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update customFieldType with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: CustomFieldTypeDTO,
    })
    async putId(@Req() req: Request, @Body() customFieldTypeDTO: CustomFieldTypeDTO): Promise<CustomFieldTypeDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'CustomFieldType', customFieldTypeDTO.id);
        return await this.customFieldTypeService.update(customFieldTypeDTO);
    }

    @Delete('/:id')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Delete customFieldType' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: number): Promise<CustomFieldTypeDTO> {
        return await this.customFieldTypeService.deleteById(id);
    }
}
