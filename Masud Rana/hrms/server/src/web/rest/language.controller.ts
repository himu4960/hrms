import { ClassSerializerInterceptor, Controller, Get, Logger, Query, UseInterceptors } from '@nestjs/common';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

import { LanguageDTO } from '../../service/dto/language.dto';

import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { LanguageService } from '../../service/language.service';

@Controller('api/languages')
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiUseTags('Languages')
export class LanguageController {
    logger = new Logger('LanguageController');

    constructor(private readonly languageService: LanguageService) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List all active records',
        type: LanguageDTO,
    })
    async getActiveLanguages(): Promise<LanguageDTO[]> {
        return await this.languageService.getActiveLanguages();
    }

    @Get('/all')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: LanguageDTO,
    })
    async getAll(): Promise<LanguageDTO[]> {
        return await this.languageService.getAll();
    }
}
