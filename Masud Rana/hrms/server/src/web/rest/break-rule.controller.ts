import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';

import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { BreakRuleDTO } from '../../service/dto/break-rule/break-rule.dto';
import { BreakRuleUpdateDTO } from './../../service/dto/break-rule/break-rule-update.dto';
import { BreakRuleCreateDTO } from './../../service/dto/break-rule/break-rule-create.dto';

import { PageRequest, Page } from '../../domain/base/pagination.entity';

import { BreakRuleService } from '../../service/break-rule.service';
import { JWTService } from './../../service/jwt.service';

@Controller('api/break-rules')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Break Rules')
export class BreakRuleController {
    logger = new Logger('BreakRuleController');

    constructor(private readonly breakRuleService: BreakRuleService, private readonly jwtService: JWTService) {}

    @Get('/')
    @Roles(RoleType.OWNER)
    @ApiResponse({
        status: 200,
        description: 'List all break rule records',
        type: BreakRuleDTO,
    })
    async getAll(@Req() req: Request): Promise<BreakRuleDTO[]> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
        const [results, count] = await this.breakRuleService.breakRuleFindAndCount(
            {
                skip: +pageRequest.page * pageRequest.size,
                take: +pageRequest.size,
                order: pageRequest.sort.asOrder(),
            },
            decoded.company_id,
        );
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.OWNER)
    @ApiResponse({
        status: 200,
        description: 'The found break rule record',
        type: BreakRuleDTO,
    })
    async getOne(@Param('id') id: number): Promise<BreakRuleDTO> {
        return await this.breakRuleService.breakRuleFindById(id);
    }

    @PostMethod('/')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Create breakRule' })
    @ApiResponse({
        status: 201,
        description: 'Break rule has been successfully created.',
        type: BreakRuleDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() breakRuleDTO: BreakRuleCreateDTO): Promise<BreakRuleDTO> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const created = await this.breakRuleService.breakRuleSave(breakRuleDTO, decoded.company_id, req.user?.id);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'BreakRule', created.id);
        return created;
    }

    @Put('/:break_rule_id')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update breakRule with id' })
    @ApiResponse({
        status: 200,
        description: 'Break rule has been successfully updated.',
        type: BreakRuleUpdateDTO,
    })
    async putId(
        @Req() req: Request,
        @Param('break_rule_id') break_rule_id: number,
        @Body() breakRuleDTO: BreakRuleUpdateDTO,
    ): Promise<BreakRuleDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'BreakRule', breakRuleDTO.id);
        return await this.breakRuleService.breakRuleUpdate(break_rule_id, breakRuleDTO, req.user?.id);
    }

    @Delete('/:id')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Delete breakRule' })
    @ApiResponse({
        status: 204,
        description: 'Break rule has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: number): Promise<BreakRuleDTO> {
        return await this.breakRuleService.breakRuleDeleteById(id);
    }

    @Delete('/conditions/:break_rule_id')
    @Roles(RoleType.OWNER)
    @ApiResponse({
        status: 200,
        description: 'Break rule condition has been successfully deleted',
        type: BreakRuleDTO,
    })
    async breakRuleConditionDeleteById(@Param('break_rule_id') break_rule_id: number): Promise<void> {
        return await this.breakRuleService.breakRuleConditionDeleteById(break_rule_id);
    }
}
