import { GenderDTO } from './../../service/dto/gender/gender.dto';
import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { GenderAllDTO } from './../../service/dto/gender/gender-all.dto';
import { GenderUpdateDTO } from './../../service/dto/gender/gender-update.dto';
import { GenderCreateDTO } from './../../service/dto/gender/gender-create.dto';
import { GenderGetByLangCodeDTO } from './../../service/dto/gender/gender-get-by-langcode.dto';

import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { GenderService } from '../../service/gender.service';

@Controller('api/genders')
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiUseTags('Gender')
export class GenderController {
    logger = new Logger('GenderController');

    constructor(private readonly genderService: GenderService) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: GenderAllDTO,
    })
    async getAll(@Req() req: Request) {
        return this.genderService.findAll();
    }

    @Get('/:langCode')
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: GenderGetByLangCodeDTO,
    })
    async getOne(@Param('langCode') langCode: string) {
        return await this.genderService.findByLangCode(langCode);
    }

    @PostMethod('/')
    @ApiOperation({ title: 'Create gender' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: GenderCreateDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() createGender: GenderCreateDTO) {
        const created = await this.genderService.save(createGender, req.user?.id);
        return created;
    }

    @Put('/:id')
    @ApiOperation({ title: 'Update gender with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: GenderUpdateDTO,
    })
    async putId(@Req() req: Request, @Param('id') id: number, @Body() updateGender: GenderUpdateDTO) {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Gender', id);
        return await this.genderService.update(id, updateGender, req.user?.id);
    }

    @Delete('/:id')
    @ApiOperation({ title: 'Delete gender' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: number): Promise<GenderDTO> {
        return await this.genderService.deleteById(id);
    }
}
