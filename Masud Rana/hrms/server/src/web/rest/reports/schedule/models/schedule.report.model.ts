export interface IHoursScheduled {
    employee_id: string;
    employee_name: string;
    work_shift_hours: string;
}
