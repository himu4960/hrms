import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Logger,
    Post as PostMethod,
    UseGuards,
    Req,
    UseInterceptors,
    Query,
    Res,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse } from '@nestjs/swagger';
import { Response } from 'express';

import dayjs from 'dayjs';

import { Request } from '../../../../client/request';
import { AuthGuard, Roles, RolesGuard, RoleType } from '../../../../security';
import { LoggingInterceptor } from '../../../../client/interceptors/logging.interceptor';

import { REPORT_RESPONSE } from './../../../../shared/constant/response-message';

import { JWTService } from '../../../../service/jwt.service';
import { UtilService } from './../../../../shared/util.service';
import { ScheduleReportService } from '../../../../service/reports/schedule.report.service';
import { DateTimeConvertService } from './../../../../shared/convert/date-time-convert.service';
import { ExportService } from './../../../../shared/export/export.service';
import { TimeZoneService } from '../../../../service/time-zone.service';

import { ReportFilterDTO } from '../../../../service/dto/reports/report.dto';
import { EExportType } from '../time-clock/models/time-sheet-report.model';
import { IHoursScheduled } from './models/schedule.report.model';
import { ResponseDTO } from './../../../../service/dto/response.dto';


@Controller('api/reports')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Report')
export class ScheduleReportController {
    logger = new Logger('ScheduleReportController');

    constructor(private readonly jwtService: JWTService,
        private readonly scheduleReportService: ScheduleReportService,
        private readonly exportService: ExportService,
        private readonly dateTimeConvertService: DateTimeConvertService,
        private readonly timeZoneService: TimeZoneService,
        private readonly utilService: UtilService) { }

    @PostMethod('/schedule-summary')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'List all records',
    })
    async getScheduleSummary(@Req() req: Request, @Body() filterDTO: ReportFilterDTO): Promise<any> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const timeZone = await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid));
        return this.scheduleReportService.getScheduleSummary(filterDTO, decoded.company_id, timeZone);
    }

    @PostMethod('/position-summary')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'List all records',
    })
    async getPositionSummary(@Req() req: Request, @Body() filterDTO: ReportFilterDTO): Promise<any> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const timeZone = await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid));
        return this.scheduleReportService.getPositionSummary(filterDTO, decoded.company_id, timeZone);
    }

    @PostMethod('/hours-scheduled')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'List all records',
    })
    async getHoursScheduled(@Req() req: Request, @Body() filterDTO: ReportFilterDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const response: IHoursScheduled[] = await this.scheduleReportService.getHoursScheduled(filterDTO, decoded.company_id);
            if (response.length) {
                return this.utilService.getSuccessResponse(response, REPORT_RESPONSE.READ.SUCCESS, REPORT_RESPONSE.READ.SUCCESS);
            } else {
                return this.utilService.getSuccessResponse(response, REPORT_RESPONSE.READ.NOT_FOUND, REPORT_RESPONSE.READ.NOT_FOUND);
            }
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, REPORT_RESPONSE.READ.ERROR);
        }
    }

    @PostMethod('/hours-scheduled/export')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'List all records',
    })
    async exportHoursScheduled(@Query('type') exportType: string, @Req() req: Request, @Res() res: Response, @Body() filterDTO: ReportFilterDTO): Promise<any> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const today = this.dateTimeConvertService.format(dayjs());
        try {
            if (exportType === EExportType.PDF) {
                const doc = await this.scheduleReportService.exportPdfHoursScheduled(filterDTO, decoded.company_id);
                const buffer = []
                doc.on('data', buffer.push.bind(buffer))
                doc.on('end', () => {
                    const data = Buffer.concat(buffer)
                    res.set({
                        'Content-Type': 'application/pdf',
                        'Content-Disposition': `attachment; filename=${today}_hours_scheduled.pdf`,
                        'Content-Length': data.length,
                    })

                    res.end(data);
                })
                return this.utilService.getSuccessResponse(
                    null,
                    REPORT_RESPONSE.PDF_EXPORT_SUCCESS,
                    'EXPORTED',
                );
            }
            else if (exportType === EExportType.XLSX) {
                const result = await this.scheduleReportService.exportXLSXHoursScheduled(
                    filterDTO,
                    decoded.company_id,
                    res,
                );
                return this.utilService.getSuccessResponse(
                    null,
                    REPORT_RESPONSE.XLSX_EXPORT_SUCCESS,
                    'EXPORTED',
                );
            }
            return this.utilService.getErrorResponse(REPORT_RESPONSE.WARNING, 'NOT_EXPORTED');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'NOT_EXPORTED');
        }
    }

    @PostMethod('/position-summary/export')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'List all records',
    })
    async exportPositionSummary(@Query('type') exportType: string, @Req() req: Request, @Res() res: Response, @Body() filterDTO: ReportFilterDTO): Promise<any> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const timeZone = await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid));
        const today = this.dateTimeConvertService.format(dayjs());
        try {
            if (exportType === EExportType.PDF) {
                const doc = await this.scheduleReportService.exportPdfPositionSummary(filterDTO, decoded.company_id, timeZone);
                const buffer = []
                doc.on('data', buffer.push.bind(buffer))
                doc.on('end', () => {
                    const data = Buffer.concat(buffer)
                    res.set({
                        'Content-Type': 'application/pdf',
                        'Content-Disposition': `attachment; filename=${today}_position_summary.pdf`,
                        'Content-Length': data.length,
                    })

                    res.end(data);
                })
                return this.utilService.getSuccessResponse(
                    null,
                    REPORT_RESPONSE.PDF_EXPORT_SUCCESS,
                    'EXPORTED',
                );
            }
            else if (exportType === EExportType.XLSX) {
                filterDTO.is_export = true;
                const result = await this.scheduleReportService.exportXLSXPositionSummary(
                    filterDTO,
                    decoded.company_id,
                    res,
                    timeZone
                );
                return this.utilService.getSuccessResponse(
                    null,
                    REPORT_RESPONSE.XLSX_EXPORT_SUCCESS,
                    'EXPORTED',
                );
            }
            return this.utilService.getErrorResponse(REPORT_RESPONSE.WARNING, 'NOT_EXPORTED');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'NOT_EXPORTED');
        }
    }

    @PostMethod('/shifts-scheduled')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'List all records',
    })
    async getShiftsScheduled(@Req() req: Request, @Body() filterDTO: ReportFilterDTO): Promise<any> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const timeZone = await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid));
            const response: any[] = await this.scheduleReportService.getShiftsScheduled(filterDTO, decoded.company_id, timeZone);
            if (response.length) {
                return this.utilService.getSuccessResponse(response, REPORT_RESPONSE.READ.SUCCESS, REPORT_RESPONSE.READ.SUCCESS);
            } else {
                return this.utilService.getSuccessResponse(response, REPORT_RESPONSE.READ.NOT_FOUND, REPORT_RESPONSE.READ.NOT_FOUND);
            }
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, REPORT_RESPONSE.READ.ERROR);
        }
    }

    @PostMethod('/schedule-summary/export')
    @ApiResponse({
        status: 200,
        description: 'Successfully Exported',
    })
    async exportScheduleSummary(
        @Query('type') exportType: string,
        @Req() req: Request,
        @Res() res: Response,
        @Body() filterDTO: ReportFilterDTO,
    ): Promise<ResponseDTO> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const timeZone = await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid));
        const today = this.dateTimeConvertService.format(dayjs());
        try {
            if (exportType === EExportType.PDF) {
                const doc = await this.scheduleReportService.exportPdfScheduleSummary(filterDTO, decoded.company_id, timeZone);
                const buffer = []
                doc.on('data', buffer.push.bind(buffer))
                doc.on('end', () => {
                    const data = Buffer.concat(buffer)
                    res.set({
                        'Content-Type': 'application/pdf',
                        'Content-Disposition': `attachment; filename=${today}_schedule_summary.pdf`,
                        'Content-Length': data.length,
                    })

                    res.end(data);
                });
                return this.utilService.getSuccessResponse(
                    null,
                    REPORT_RESPONSE.PDF_EXPORT_SUCCESS,
                    'EXPORTED',
                );
            } else if (exportType === EExportType.XLSX) {
                const scheduleSummary = await this.scheduleReportService.getScheduleSummary(filterDTO, decoded.company_id, timeZone);
                const result = await this.exportService.exportXLSXScheduleSummary(
                    scheduleSummary,
                    'Schedule Summary',
                    res,
                );
                return this.utilService.getSuccessResponse(
                    null,
                    REPORT_RESPONSE.XLSX_EXPORT_SUCCESS,
                    'EXPORTED',
                );
            }
            return this.utilService.getErrorResponse(REPORT_RESPONSE.WARNING, 'NOT_EXPORTED');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'NOT_EXPORTED');
        }
    }

    @PostMethod('/shifts-scheduled/export')
    @ApiResponse({
        status: 200,
        description: 'Successfully Exported',
    })
    async exportShiftsScheduled(
        @Query('type') exportType: string,
        @Req() req: Request,
        @Res() res: Response,
        @Body() filterDTO: ReportFilterDTO,
    ): Promise<ResponseDTO> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const timeZone = await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid));
        const today = this.dateTimeConvertService.format(dayjs());
        try {
            if (exportType === EExportType.PDF) {
                const doc = await this.scheduleReportService.exportPdfShiftsScheduled(filterDTO, decoded.company_id, timeZone);
                const buffer = []
                doc.on('data', buffer.push.bind(buffer))
                doc.on('end', () => {
                    const data = Buffer.concat(buffer)
                    res.set({
                        'Content-Type': 'application/pdf',
                        'Content-Disposition': `attachment; filename=${today}_shifts_scheduled.pdf`,
                        'Content-Length': data.length,
                    })

                    res.end(data);
                });
                return this.utilService.getSuccessResponse(
                    null,
                    REPORT_RESPONSE.PDF_EXPORT_SUCCESS,
                    'EXPORTED',
                );
            } else if (exportType === EExportType.XLSX) {
                const result = await this.scheduleReportService.exportXLSXShiftsScheduled(
                    filterDTO,
                    decoded.company_id,
                    res,
                    timeZone
                );
                return this.utilService.getSuccessResponse(
                    null,
                    REPORT_RESPONSE.XLSX_EXPORT_SUCCESS,
                    'EXPORTED',
                );
            }
            return this.utilService.getErrorResponse(REPORT_RESPONSE.WARNING, 'NOT_EXPORTED');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'NOT_EXPORTED');
        }
    }
}
