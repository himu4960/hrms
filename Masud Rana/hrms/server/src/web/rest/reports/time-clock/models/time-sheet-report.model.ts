export interface ITimeSheetLateSummary {
    employee_id: string;
    employee_name: string;
    designation_name: string;
    date: string;
    shift_start: string;
    actual_start: string;
    shift_end: string;
    actual_end: string;
    late_status: string;
    late_by: string;
}

export interface ITimeSheetAttendance {
    employee_id: string;
    employee_name: string;
    shifts: string;
    hours_scheduled: string;
    hours_clocked: string;
    late_count: string;
    absent_count: string;
    clock_vs_shift: string;
}

export interface ITimeSheetAttendanceExport {
    employee_name?: string | null;
    shifts?: string | null;
    hours_scheduled?: string | null;
    hours_clocked?: string | null;
    late_count?: string | null;
    absent_count?: string | null;
    clock_vs_shift?: string | null;
}

export interface ITimeSheetLateSummaryeExport {
    employee_name?: string | null;
    designation_name?: string | null;
    date?: string | null;
    shift_start?: string | null;
    actual_start?: string | null;
    shift_end?: string | null;
    actual_end?: string | null;
    late_status?: string | null;
    late_by?: string | null;
}

export interface ILocationHistoryExport {
    date?: string | null;
    employee_name?: string | null;
    clock_start_time?: string | null;
    clock_end_time?: string | null;
    total_clock_duration?: number | null;
    total_break_duration?: number | null;
    branch_name?: string | null;
    designation_name?: string | null;
    clockin_location?: string | null;
    clockout_location?: string | null;
}

export enum EExportType {
    PDF = 'pdf',
    XLSX = 'xlsx',
    CSV = 'csv',
}
