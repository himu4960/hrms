import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Logger,
    Post,
    UseGuards,
    Req,
    UseInterceptors,
    Res,
    Query,
    ForbiddenException,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse } from '@nestjs/swagger';
import { Response } from 'express';
import dayjs from 'dayjs';

import { REPORT_RESPONSE } from './../../../../shared/constant/response-message';

import { Request } from '../../../../client/request';
import { LoggingInterceptor } from '../../../../client/interceptors/logging.interceptor';
import { AuthGuard, Roles, RolesGuard, RoleType } from '../../../../security';

import { ReportFilterDTO } from '../../../../service/dto/reports/report.dto';
import { EExportType, ITimeSheetAttendance, ITimeSheetLateSummary } from './models/time-sheet-report.model';
import { ResponseDTO } from 'src/service/dto/response.dto';

import { UtilService } from './../../../../shared/util.service';
import { JWTService } from '../../../../service/jwt.service';
import { TimeSheetReportService } from '../../../../service/reports/time-sheet.report.service';
import { DateTimeConvertService } from './../../../../shared/convert/date-time-convert.service';
import { ExportService } from './../../../../shared/export/export.service';
import { TimeZoneService } from '../../../../service/time-zone.service';

@Controller('api/reports')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Report')
export class TimeSheetReportController {
    logger = new Logger('TimeSheetReportController');

    constructor(
        private readonly jwtService: JWTService,
        private readonly utilService: UtilService,
        private readonly timeZoneService: TimeZoneService,
        private readonly dateTimeConvertService: DateTimeConvertService,
        private readonly timeSheetReportService: TimeSheetReportService,
        private readonly exportService: ExportService,
    ) {}

    @Post('/time-sheets')
    @ApiResponse({
        status: 200,
        description: 'List all Time Sheet records',
    })
    async getAllForReport(@Req() req: Request, @Body() filterDTO: ReportFilterDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);

            const timeZone = await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid));
            const data = await this.timeSheetReportService.getTimeSheet(filterDTO, decoded.company_id, timeZone);
            return this.utilService.getSuccessResponse(data, REPORT_RESPONSE.TIME_SHEET.SUCCESS, 'TIMESHEET.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(REPORT_RESPONSE.TIME_SHEET.ERROR, 'TIMESHEET.NOT_RETRIEVED'),
            );
        }
    }

    @Post('/time-sheets/summary')
    @ApiResponse({
        status: 200,
        description: 'List all Time Sheet Summary records',
    })
    async getAllForTimeSheetSummary(@Req() req: Request, @Body() filterDTO: ReportFilterDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);

            const timeZone = await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid));
            const data = await this.timeSheetReportService.getTimeSheet(filterDTO, decoded.company_id, timeZone);
            return this.utilService.getSuccessResponse(data, REPORT_RESPONSE.TIME_SHEET.SUCCESS, 'TIMESHEET.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(REPORT_RESPONSE.TIME_SHEET.ERROR, 'TIMESHEET.NOT_RETRIEVED'),
            );
        }
    }

    @Post('/time-sheet/late-summary')
    @ApiResponse({
        status: 200,
        description: 'List all Time Sheet Late Summary records',
    })
    async getTimeSheetLateSummary(@Req() req: Request, @Body() filterDTO: ReportFilterDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const timeZone = await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid));
            const data = await this.timeSheetReportService.getTimeSheetLateSummary(
                filterDTO,
                decoded.company_id,
                timeZone,
            );

            return this.utilService.getSuccessResponse(data, REPORT_RESPONSE.TIME_SHEET.SUCCESS, 'TIMESHEET.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(REPORT_RESPONSE.TIME_SHEET.ERROR, 'TIMESHEET.NOT_RETRIEVED'),
            );
        }
    }

    @Post('/time-sheet/attendance')
    @ApiResponse({
        status: 200,
        description: 'List all records',
    })
    async getTimeSheetAttendance(@Req() req: Request, @Body() filterDTO: ReportFilterDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);

            const timeZone = await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid));
            const data = await this.timeSheetReportService.getTimeSheetAttendance(
                filterDTO,
                decoded.company_id,
                timeZone,
            );
            return this.utilService.getSuccessResponse(data, REPORT_RESPONSE.TIME_SHEET.SUCCESS, 'TIMESHEET.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(REPORT_RESPONSE.TIME_SHEET.ERROR, 'TIMESHEET.NOT_RETRIEVED'),
            );
        }
    }

    @Post('location-history')
    @ApiResponse({
        status: 200,
        description: 'List all records',
    })
    async getLocationHistory(@Req() req: Request, @Body() filterDTO: ReportFilterDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const timeZone = await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid));
            const data = await this.timeSheetReportService.getLocationHistory(filterDTO, decoded.company_id, timeZone);
            return this.utilService.getSuccessResponse(data, REPORT_RESPONSE.TIME_SHEET.SUCCESS, 'TIMESHEET.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(REPORT_RESPONSE.TIME_SHEET.ERROR, 'TIMESHEET.NOT_RETRIEVED'),
            );
        }
    }

    @Post('/time-sheet-summary/export')
    @ApiResponse({
        status: 200,
        description: 'Successfully Generated PDF',
    })
    async getTimeSheetSummaryPDF(
        @Query('type') exportType: string,
        @Req() req: Request,
        @Res() res: Response,
        @Body() filterDTO: ReportFilterDTO,
    ): Promise<any> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);

            const timeZone = await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid));
            const today = this.dateTimeConvertService.format(dayjs());

            if (exportType === EExportType.PDF) {
                const doc = await this.timeSheetReportService.exportPdfTimeSheetSummary(
                    filterDTO,
                    decoded.company_id,
                    timeZone,
                );
                const buffer = [];
                doc.on('data', buffer.push.bind(buffer));
                doc.on('end', () => {
                    const data = Buffer.concat(buffer);
                    res.set({
                        'Content-Type': 'application/pdf',
                        'Content-Disposition': `attachment; filename=${today}_timesheet_summary.pdf`,
                        'Content-Length': data.length,
                    });

                    res.end(data);
                });
                return this.utilService.getSuccessResponse(null, REPORT_RESPONSE.PDF_EXPORT_SUCCESS, 'EXPORTED');
            } else if (exportType === EExportType.XLSX) {
                const timeSheets = await this.timeSheetReportService.getTimeSheet(
                    filterDTO,
                    decoded.company_id,
                    timeZone,
                );
                const result = await this.exportService.exportXLSXTimeSheetSummary(
                    timeSheets,
                    'Time Sheet Summary',
                    res,
                );
                return this.utilService.getSuccessResponse(null, REPORT_RESPONSE.XLSX_EXPORT_SUCCESS, 'EXPORTED');
            }
            return this.utilService.getErrorResponse(REPORT_RESPONSE.WARNING, 'NOT_EXPORTED');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'NOT_EXPORTED');
        }
    }

    @Post('/time-sheet/attendance/export')
    @ApiResponse({
        status: 200,
        description: 'Successfully Generated',
    })
    async exportTimeSheetAttendance(
        @Query('type') exportType: string,
        @Req() req: Request,
        @Res() res: Response,
        @Body() filterDTO: ReportFilterDTO,
    ): Promise<any> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);

        const timeZone = await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid));
        const today = this.dateTimeConvertService.format(dayjs());
        try {
            if (exportType === EExportType.PDF) {
                const doc = await this.timeSheetReportService.exportPdfTimeSheetAttendance(
                    filterDTO,
                    decoded.company_id,
                    timeZone,
                );
                const buffer = [];
                doc.on('data', buffer.push.bind(buffer));
                doc.on('end', () => {
                    const data = Buffer.concat(buffer);
                    res.set({
                        'Content-Type': 'application/pdf',
                        'Content-Disposition': `attachment; filename=${today}_timesheet_attendance.pdf`,
                        'Content-Length': data.length,
                    });

                    res.end(data);
                });
                return this.utilService.getSuccessResponse(null, REPORT_RESPONSE.XLSX_EXPORT_SUCCESS, 'EXPORTED');
            } else if (exportType === EExportType.XLSX) {
                const result = await this.timeSheetReportService.exportXLSXTimeSheetAttendance(
                    filterDTO,
                    decoded.company_id,
                    timeZone,
                    res,
                );
                return this.utilService.getSuccessResponse(null, REPORT_RESPONSE.XLSX_EXPORT_SUCCESS, 'EXPORTED');
            }

            return this.utilService.getErrorResponse(REPORT_RESPONSE.WARNING, 'NOT_EXPORTED');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'NOT_EXPORTED');
        }
    }

    @Post('/time-sheet/late-summary/export')
    @ApiResponse({
        status: 200,
        description: 'Successfully Exported',
    })
    async exportTimeSheetLateSummary(
        @Query('type') exportType: string,
        @Req() req: Request,
        @Res() res: Response,
        @Body() filterDTO: ReportFilterDTO,
    ): Promise<any> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);

        const timeZone = await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid));
        const today = this.dateTimeConvertService.format(dayjs());
        try {
            if (exportType === EExportType.PDF) {
                const doc = await this.timeSheetReportService.exportPdfTimeSheetLateSummary(
                    filterDTO,
                    decoded.company_id,
                    timeZone,
                );
                const buffer = [];
                doc.on('data', buffer.push.bind(buffer));
                doc.on('end', () => {
                    const data = Buffer.concat(buffer);
                    res.set({
                        'Content-Type': 'application/pdf',
                        'Content-Disposition': `attachment; filename=${today}_timesheet_late_summary.pdf`,
                        'Content-Length': data.length,
                    });

                    res.end(data);
                });
                return this.utilService.getSuccessResponse(null, REPORT_RESPONSE.PDF_EXPORT_SUCCESS, 'EXPORTED');
            } else if (exportType === EExportType.XLSX) {
                const result = await this.timeSheetReportService.exportXLSXTimeSheetLateSummary(
                    filterDTO,
                    decoded.company_id,
                    timeZone,
                    res,
                );
                return this.utilService.getSuccessResponse(null, REPORT_RESPONSE.XLSX_EXPORT_SUCCESS, 'EXPORTED');
            }
            return this.utilService.getErrorResponse(REPORT_RESPONSE.WARNING, 'NOT_EXPORTED');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'NOT_EXPORTED');
        }
    }

    @Post('/time-sheet/export')
    @ApiResponse({
        status: 200,
        description: 'Successfully Exported',
    })
    async exportTimeSheet(
        @Query('type') exportType: string,
        @Req() req: Request,
        @Res() res: Response,
        @Body() filterDTO: ReportFilterDTO,
    ): Promise<ResponseDTO> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);

        const timeZone = await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid));
        const today = this.dateTimeConvertService.format(dayjs());
        try {
            if (exportType === EExportType.PDF) {
                const doc = await this.timeSheetReportService.exportPdfTimeSheet(
                    filterDTO,
                    decoded.company_id,
                    timeZone,
                );
                const buffer = [];
                doc.on('data', buffer.push.bind(buffer));
                doc.on('end', () => {
                    const data = Buffer.concat(buffer);
                    res.set({
                        'Content-Type': 'application/pdf',
                        'Content-Disposition': `attachment; filename=${today}_timesheet.pdf`,
                        'Content-Length': data.length,
                    });

                    res.end(data);
                });
                return this.utilService.getSuccessResponse(null, REPORT_RESPONSE.PDF_EXPORT_SUCCESS, 'EXPORTED');
            } else if (exportType === EExportType.XLSX) {
                const timeSheets = await this.timeSheetReportService.getTimeSheet(
                    filterDTO,
                    decoded.company_id,
                    timeZone,
                );
                const result = await this.exportService.exportXLSXTimeSheet(timeSheets, 'Time Sheet', res);
                return this.utilService.getSuccessResponse(null, REPORT_RESPONSE.XLSX_EXPORT_SUCCESS, 'EXPORTED');
            }
            return this.utilService.getErrorResponse(REPORT_RESPONSE.WARNING, 'NOT_EXPORTED');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'NOT_EXPORTED');
        }
    }

    @Post('/location-history/export')
    @ApiResponse({
        status: 200,
        description: 'Successfully Exported',
    })
    async exportLocationHistory(
        @Query('type') exportType: string,
        @Req() req: Request,
        @Res() res: Response,
        @Body() filterDTO: ReportFilterDTO,
    ): Promise<any> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);

        const timeZone = await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid));
        const today = this.dateTimeConvertService.format(dayjs());
        try {
            if (exportType === EExportType.PDF) {
                const doc = await this.timeSheetReportService.exportPdfLocationHistory(
                    filterDTO,
                    decoded.company_id,
                    timeZone,
                    );
                const buffer = [];
                doc.on('data', buffer.push.bind(buffer));
                doc.on('end', () => {
                    const data = Buffer.concat(buffer);
                    res.set({
                        'Content-Type': 'application/pdf',
                        'Content-Disposition': `attachment; filename=${today}_location_history.pdf`,
                        'Content-Length': data.length,
                    });

                    res.end(data);
                });
                return this.utilService.getSuccessResponse(null, REPORT_RESPONSE.PDF_EXPORT_SUCCESS, 'EXPORTED');
            } else if (exportType === EExportType.XLSX) {
                const result = await this.timeSheetReportService.exportXLSXLocationHistory(
                    filterDTO,
                    decoded.company_id,
                    timeZone,
                    res,
                );
                return this.utilService.getSuccessResponse(null, REPORT_RESPONSE.XLSX_EXPORT_SUCCESS, 'EXPORTED');
            }
            return this.utilService.getErrorResponse(REPORT_RESPONSE.WARNING, 'NOT_EXPORTED');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'NOT_EXPORTED');
        }
    }
}
