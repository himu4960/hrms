import { Controller, ClassSerializerInterceptor, Get, Logger, Req, UseInterceptors } from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { Request } from 'express';

import { UserDTO } from '../../service/dto/user.dto';

import { PageRequest, Page } from '../../domain/base/pagination.entity';

import { HeaderUtil } from '../../client/header-util';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { AuthService } from '../../service/auth.service';

@Controller('api')
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiUseTags('Public User Controller')
export class PublicUserController {
    logger = new Logger('PublicUserController');

    constructor(private readonly authService: AuthService) {}

    @Get('/users')
    @ApiOperation({ title: 'Get the list of users' })
    @ApiResponse({
        status: 200,
        description: 'List all users records',
        type: UserDTO,
    })
    async getAllPublicUsers(@Req() req: Request) {
        const sortField = req.query.sort;
        const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, sortField);
        const [results, count] = await this.authService.getAllUsers({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }
}
