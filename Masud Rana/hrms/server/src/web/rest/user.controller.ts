import {
    Body,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
    ClassSerializerInterceptor,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { UserDTO } from '../../service/dto/user.dto';

import { PageRequest, Page } from '../../domain/base/pagination.entity';

import { Request } from '../../client/request';
import { HeaderUtil } from '../../client/header-util';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';

import { UserService } from '../../service/user.service';

@Controller('api/admin/users')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('User Resource')
export class UserController {
    logger = new Logger('UserController');

    constructor(private readonly userService: UserService) {}

    @Get('/')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Get the list of users' })
    @ApiResponse({
        status: 200,
        description: 'List all users',
        type: UserDTO,
    })
    async getAllUsers(@Req() req: Request) {
        const sortField = req.query.sort;
        const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, sortField);
        const [results, count] = await this.userService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Post('/')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Create user' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: UserDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async createUser(@Req() req: Request, @Body() userDTO: UserDTO): Promise<UserDTO> {
        userDTO.password = userDTO.username;
        const created = await this.userService.save(userDTO, req.user?.id);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'User', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update user' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: UserDTO,
    })
    async updateUser(@Req() req: Request, @Body() userDTO: UserDTO): Promise<UserDTO> {
        const userOnDb = await this.userService.find({ where: { username: userDTO.username } });
        if (userOnDb && userOnDb.id) {
            userDTO.id = userOnDb.id;
        } else {
            userDTO.password = userDTO.username;
        }
        return await this.userService.update(userDTO, req.user?.id);
    }

    @Get('/:login')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Get user' })
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: UserDTO,
    })
    async getUser(@Param('login') loginValue: string): Promise<UserDTO> {
        return await this.userService.find({ where: { username: loginValue } });
    }

    @Delete('/:userId')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Delete user' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully deleted.',
        type: UserDTO,
    })
    async deleteUser(@Param('userId') userId: number): Promise<UserDTO> {
        return await this.userService.delete(userId);
    }
}
