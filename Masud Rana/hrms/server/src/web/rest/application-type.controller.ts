import { ClassSerializerInterceptor, Controller, Get, Logger, UseInterceptors } from '@nestjs/common';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

import { ApplicationTypeDTO } from '../../service/dto/application-type.dto';

import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { ApplicationTypeService } from '../../service/application-type.service';

@Controller('api/application-types')
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiUseTags('Application Types')
export class ApplicationTypeController {
    logger = new Logger('ApplicationTypeController');

    constructor(private readonly applicationTypeService: ApplicationTypeService) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List all active records',
        type: ApplicationTypeDTO,
    })
    async getActiveApplicationTypes(): Promise<ApplicationTypeDTO[]> {
        return await this.applicationTypeService.getActiveApplicationTypes();
    }

    @Get('/all')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: ApplicationTypeDTO,
    })
    async getAll(): Promise<ApplicationTypeDTO[]> {
        return await this.applicationTypeService.getAll();
    }
}
