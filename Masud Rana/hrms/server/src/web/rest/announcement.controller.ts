import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
    ForbiddenException,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { ANNOUNCEMENT_RESPONSE } from './../../shared/constant/response-message';

import { AnnouncementDTO } from '../../service/dto/announcement/announcement.dto';
import { AnnouncementCreateDTO } from './../../service/dto/announcement/announcement-create.dto';
import { AnnouncementUpdateDTO } from './../../service/dto/announcement/announcement-update.dto';
import { ResponseDTO } from './../../service/dto/response.dto';

import { JWTService } from './../../service/jwt.service';
import { UtilService } from './../../shared/util.service';
import { AnnouncementService } from '../../service/announcement.service';

import { PageRequest, Page } from '../../domain/base/pagination.entity';

import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/announcements')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('announcements')
export class AnnouncementController {
    logger = new Logger('AnnouncementController');

    constructor(
        private readonly announcementService: AnnouncementService,
        private readonly jWTService: JWTService,
        private readonly utilService: UtilService,
    ) {}

    @Get('/')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: AnnouncementDTO,
    })
    async getAll(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jWTService.decodedJWTToken(req.headers.authorization);
            const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
            const [results, count] = await this.announcementService.findAndCount(
                {
                    skip: +pageRequest.page * pageRequest.size,
                    take: +pageRequest.size,
                    order: pageRequest.sort.asOrder(),
                },
                decoded.company_id,
            );
            HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
            return this.utilService.getSuccessResponse(
                results,
                ANNOUNCEMENT_RESPONSE.READ.SUCCESS,
                'ANNOUNCEMENT.RETRIEVED',
            );
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(ANNOUNCEMENT_RESPONSE.READ.SUCCESS, 'ANNOUNCEMENT.NOT_RETRIEVED'),
            );
        }
    }

    @Get('/:id')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: AnnouncementDTO,
    })
    async getOne(@Param('id') id: number): Promise<ResponseDTO> {
        try {
            const result = await this.announcementService.findById(id);
            return this.utilService.getSuccessResponse(
                result,
                ANNOUNCEMENT_RESPONSE.READ.SUCCESS,
                'ANNOUNCEMENT.RETRIEVED',
            );
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(ANNOUNCEMENT_RESPONSE.READ.SUCCESS, 'ANNOUNCEMENT.NOT_RETRIEVED'),
            );
        }
    }

    @PostMethod('/')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Create announcement' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: AnnouncementDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() announcementCreateDTO: AnnouncementCreateDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jWTService.decodedJWTToken(req.headers.authorization);
            const created = await this.announcementService.save(
                announcementCreateDTO,
                decoded.company_id,
                req.user?.id,
            );
            return this.utilService.getSuccessResponse(
                created,
                ANNOUNCEMENT_RESPONSE.CREATE.SUCCESS,
                'ANNOUNCEMENT.CREATED',
            );
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(ANNOUNCEMENT_RESPONSE.CREATE.ERROR, 'ANNOUNCEMENT.NOT_CREATED'),
            );
        }
    }

    @Put('/:id')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update announcement with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: AnnouncementDTO,
    })
    async putId(
        @Req() req: Request,
        @Param('id') id: number,
        @Body() announcementUpdateDTO: AnnouncementUpdateDTO,
    ): Promise<ResponseDTO> {
        try {
            const updated = await this.announcementService.update({ ...announcementUpdateDTO, id }, req.user?.id);
            return this.utilService.getSuccessResponse(
                updated,
                ANNOUNCEMENT_RESPONSE.UPDATE.SUCCESS,
                'ANNOUNCEMENT.UPDATED',
            );
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(ANNOUNCEMENT_RESPONSE.UPDATE.ERROR, 'ANNOUNCEMENT.NOT_UPDATED'),
            );
        }
    }

    @Delete('/:id')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Delete announcement' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Param('id') id: number): Promise<ResponseDTO> {
        try {
            const deleted = await this.announcementService.deleteById(id);
            return this.utilService.getSuccessResponse(
                deleted,
                ANNOUNCEMENT_RESPONSE.DELETE.SUCCESS,
                'ANNOUNCEMENT.DELETED',
            );
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(ANNOUNCEMENT_RESPONSE.DELETE.ERROR, 'ANNOUNCEMENT.NOT_DELETED'),
            );
        }
    }
}
