import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
    ForbiddenException,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { SkillDTO } from '../../service/dto/skill.dto';
import { ResponseDTO } from './../../service/dto/response.dto';

import { SKILLS_RESPONSE } from './../../shared/constant/response-message';

import { SkillService } from '../../service/skill.service';
import { JWTService } from '../../service/jwt.service';
import { UtilService } from '../../shared/util.service';

import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/skills')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Skills')
export class SkillController {
    logger = new Logger('SkillController');

    constructor(
        private readonly skillService: SkillService,
        private readonly jwtService: JWTService,
        private readonly utilService: UtilService,
    ) {}

    @Get('/')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: SkillDTO,
    })
    async getAll(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const results = await this.skillService.findAll(decoded.company_id);
            return this.utilService.getSuccessResponse(results, SKILLS_RESPONSE.READ.SUCCESS, 'SKILLS.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(error?.message ?? SKILLS_RESPONSE.READ.ERROR, 'SKILLS.NOT_RETRIEVE'),
            );
        }
    }

    @Get('/:id')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: SkillDTO,
    })
    async getOne(@Param('id') id: number): Promise<ResponseDTO> {
        try {
            const findSkill = await this.skillService.findById(id);
            return this.utilService.getSuccessResponse(findSkill, SKILLS_RESPONSE.READ.SUCCESS, 'SKILLS.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(error?.message ?? SKILLS_RESPONSE.READ.ERROR, 'SKILLS.NOT_RETRIEVE'),
            );
        }
    }

    @PostMethod('/')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiOperation({ title: 'Create skill' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: SkillDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() skillDTO: SkillDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const createdSkill = await this.skillService.save(skillDTO, decoded.company_id, req.user?.id);
            return this.utilService.getSuccessResponse(createdSkill, SKILLS_RESPONSE.CREATE.SUCCESS, 'SKILLS.CREATED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(error?.message ?? SKILLS_RESPONSE.CREATE.ERROR, 'SKILLS.NOT_CREATED'),
            );
        }
    }

    @Put('/')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiOperation({ title: 'Update skill' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: SkillDTO,
    })
    async put(@Req() req: Request, @Body() skillDTO: SkillDTO): Promise<ResponseDTO> {
        try {
            HeaderUtil.addEntityCreatedHeaders(req.res, 'Skill', skillDTO.id);
            const updatedSkill = await this.skillService.update(skillDTO, req.user?.id);
            return this.utilService.getSuccessResponse(updatedSkill, SKILLS_RESPONSE.UPDATE.SUCCESS, 'SKILLS.UPDATED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(error?.message ?? SKILLS_RESPONSE.UPDATE.ERROR, 'SKILLS.NOT_UPDATED'),
            );
        }
    }

    @Put('/:id')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiOperation({ title: 'Update skill with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: SkillDTO,
    })
    async putId(@Req() req: Request, @Param('id') id: number, @Body() skillDTO: SkillDTO): Promise<ResponseDTO> {
        try {
            HeaderUtil.addEntityCreatedHeaders(req.res, 'Skill', skillDTO.id);
            const updatedSkill = await this.skillService.updateById(skillDTO, id, req.user?.id);
            return this.utilService.getSuccessResponse(updatedSkill, SKILLS_RESPONSE.UPDATE.SUCCESS, 'SKILLS.UPDATED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(error?.message ?? SKILLS_RESPONSE.UPDATE.ERROR, 'SKILLS.NOT_UPDATED'),
            );
        }
    }

    @Delete('/:id')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiOperation({ title: 'skill' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Param('id') id: number): Promise<ResponseDTO> {
        try {
            const deletedSkill = await this.skillService.deleteById(id);
            if (deletedSkill) {
                return this.utilService.getSuccessResponse(
                    deletedSkill,
                    SKILLS_RESPONSE.DELETE.SUCCESS,
                    'SKILLS.DELETED',
                );
            } else {
                throw new ForbiddenException(
                    this.utilService.getErrorResponse(SKILLS_RESPONSE.DELETE.ERROR, 'SKILLS.NOT_DELETED'),
                );
            }
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(error?.message ?? SKILLS_RESPONSE.DELETE.ERROR, 'SKILLS.NOT_DELETED'),
            );
        }
    }
}
