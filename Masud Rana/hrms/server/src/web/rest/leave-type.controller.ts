import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { LeaveTypeDTO } from '../../service/dto/leave-type.dto';

import { LeaveTypeService } from '../../service/leave-type.service';
import { JWTService } from '../../service/jwt.service';

import { PageRequest, Page } from '../../domain/base/pagination.entity';

import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/leave-types')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Leave Types')
export class LeaveTypeController {
    logger = new Logger('LeaveTypeController');

    constructor(private readonly leaveTypeService: LeaveTypeService, private readonly jwtService: JWTService) {}

    @Get('/')
    @Roles(RoleType.OWNER)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: LeaveTypeDTO,
    })
    async getAll(@Req() req: Request): Promise<LeaveTypeDTO[]> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
        const [results, count] = await this.leaveTypeService.findAndCount(
            {
                skip: +pageRequest.page * pageRequest.size,
                take: +pageRequest.size,
                order: pageRequest.sort.asOrder(),
            },
            decoded.company_id,
        );
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.OWNER)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: LeaveTypeDTO,
    })
    async getOne(@Param('id') id: number): Promise<LeaveTypeDTO> {
        return await this.leaveTypeService.findById(id);
    }

    @PostMethod('/')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Create leaveType' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: LeaveTypeDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() leaveTypeDTO: LeaveTypeDTO): Promise<LeaveTypeDTO> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization.replace('Bearer ', ''));
        const created = await this.leaveTypeService.save(
            { ...leaveTypeDTO, company_id: decoded.company_id },
            req.user?.id,
        );
        HeaderUtil.addEntityCreatedHeaders(req.res, 'LeaveType', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update leaveType' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: LeaveTypeDTO,
    })
    async put(@Req() req: Request, @Body() leaveTypeDTO: LeaveTypeDTO): Promise<LeaveTypeDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'LeaveType', leaveTypeDTO.id);
        return await this.leaveTypeService.update(leaveTypeDTO, req.user?.id);
    }

    @Put('/:id')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update leaveType with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: LeaveTypeDTO,
    })
    async putId(@Req() req: Request, @Body() leaveTypeDTO: LeaveTypeDTO): Promise<LeaveTypeDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'LeaveType', leaveTypeDTO.id);
        return await this.leaveTypeService.update(leaveTypeDTO, req.user?.id);
    }

    @Delete('/:id')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Delete leaveType' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: number): Promise<LeaveTypeDTO> {
        return await this.leaveTypeService.deleteById(id);
    }
}
