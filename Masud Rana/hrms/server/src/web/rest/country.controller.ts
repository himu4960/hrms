import { ClassSerializerInterceptor, Controller, Get, Logger, UseInterceptors } from '@nestjs/common';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

import { CountryDTO } from '../../service/dto/country.dto';

import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { CountryService } from '../../service/country.service';

@Controller('api/countries')
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiUseTags('Country')
export class CountryController {
    logger = new Logger('CountryController');

    constructor(private readonly countryService: CountryService) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List active records',
        type: CountryDTO,
    })
    async getActiveCountries(): Promise<CountryDTO[]> {
        return await this.countryService.getActiveCountries();
    }

    @Get('/all')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: CountryDTO,
    })
    async getAll(): Promise<CountryDTO[]> {
        return await this.countryService.getAll();
    }
}
