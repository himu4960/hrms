import { ClassSerializerInterceptor, Controller, Get, Logger, Query, UseInterceptors } from '@nestjs/common';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

import { EmployeeTypeDTO } from '../../service/dto/employee-type.dto';

import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { EmployeeTypeService } from '../../service/employee-type.service';

@Controller('api/employee-types')
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiUseTags('Employee Types')
export class EmployeeTypeController {
    logger = new Logger('EmployeeTypeController');

    constructor(private readonly employeeTypeService: EmployeeTypeService) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List active employee type records',
        type: EmployeeTypeDTO,
    })
    async getActiveEmployeeTypes(): Promise<EmployeeTypeDTO[]> {
        return this.employeeTypeService.getActiveEmployeeTypes();
    }

    @Get('/all')
    @ApiResponse({
        status: 200,
        description: 'List all employee type records',
        type: EmployeeTypeDTO,
    })
    async getAll(): Promise<EmployeeTypeDTO[]> {
        return await this.employeeTypeService.getAll();
    }
}
