import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { CustomFieldDTO } from '../../service/dto/custom-field.dto';

import { PageRequest, Page } from '../../domain/base/pagination.entity';

import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { CustomFieldService } from '../../service/custom-field.service';
import { JWTService } from '../../service/jwt.service';

@Controller('api/custom-fields')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Custom fields')
export class CustomFieldController {
    logger = new Logger('CustomFieldController');

    constructor(private readonly customFieldService: CustomFieldService, private readonly jwtService: JWTService) {}

    @Get('/')
    @Roles(RoleType.OWNER)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: CustomFieldDTO,
    })
    async getAll(@Req() req: Request): Promise<CustomFieldDTO[]> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
        const [results, count] = await this.customFieldService.findAndCount(
            {
                skip: +pageRequest.page * pageRequest.size,
                take: +pageRequest.size,
                order: pageRequest.sort.asOrder(),
            },
            decoded.company_id,
        );
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.OWNER)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: CustomFieldDTO,
    })
    async getOne(@Param('id') id: number): Promise<CustomFieldDTO> {
        return await this.customFieldService.findById(id);
    }

    @PostMethod('/')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Create customField' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: CustomFieldDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() customFieldDTO: CustomFieldDTO): Promise<CustomFieldDTO> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const created = await this.customFieldService.save(
            { ...customFieldDTO, company_id: decoded.company_id },
            req.user?.id,
        );
        HeaderUtil.addEntityCreatedHeaders(req.res, 'CustomField', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update customField' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: CustomFieldDTO,
    })
    async put(@Req() req: Request, @Body() customFieldDTO: CustomFieldDTO): Promise<CustomFieldDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'CustomField', customFieldDTO.id);
        return await this.customFieldService.update(customFieldDTO, req.user?.id);
    }

    @Put('/:id')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update customField with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: CustomFieldDTO,
    })
    async putId(@Req() req: Request, @Body() customFieldDTO: CustomFieldDTO): Promise<CustomFieldDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'CustomField', customFieldDTO.id);
        return await this.customFieldService.update(customFieldDTO, req.user?.id);
    }

    @Delete('/:id')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Delete customField' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: number): Promise<CustomFieldDTO> {
        return await this.customFieldService.deleteById(id);
    }
}
