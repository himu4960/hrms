import { ClassSerializerInterceptor, Controller, Get, Logger, UseInterceptors } from '@nestjs/common';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';

import { CompanyTypeDTO } from '../../service/dto/company-type.dto';

import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { CompanyTypeService } from '../../service/company-type.service';

@Controller('api/company-types')
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiUseTags('Company Types')
export class CompanyTypeController {
    logger = new Logger('CompanyTypeController');

    constructor(private readonly companyTypeService: CompanyTypeService) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List active company type records',
        type: CompanyTypeDTO,
    })
    async getActiveCompanyTypes(): Promise<CompanyTypeDTO[]> {
        return this.companyTypeService.getActiveCompanyTypes();
    }

    @Get('/all')
    @ApiResponse({
        status: 200,
        description: 'List all company type records',
        type: CompanyTypeDTO,
    })
    async getAll(): Promise<CompanyTypeDTO[]> {
        return await this.companyTypeService.getAll();
    }
}
