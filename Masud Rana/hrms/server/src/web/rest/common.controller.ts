/* eslint-disable @typescript-eslint/no-unused-vars */
import {
    Get,
    UseGuards,
    Controller,
    Logger,
    UseInterceptors,
    ClassSerializerInterceptor,
    Req,
    ForbiddenException,
} from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';

import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { ResponseDTO } from '../../service/dto/response.dto';
import { CommonHomeDTO } from './../../service/dto/common/home.dto';
import { CommonEmployeeDTO } from './../../service/dto/common/employee.dto';

import { UtilService } from '../../shared/util.service';
import { CommonService } from './../../service/common.service';
import { JWTService } from './../../service/jwt.service';
import { TimeZoneService } from './../../service/time-zone.service';

import { Request } from '../../client/request';

import { AuthGuard, RolesGuard } from '../../security';

import { EMPLOYEE_RESPONSE, HOME_RESPONSE } from '../../shared/constant/response-message';

@Controller('api')
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@UseGuards(AuthGuard, RolesGuard)
@ApiUseTags('Common Resource')
export class CommonController {
    logger = new Logger('CommonController');

    constructor(
        private readonly utilService: UtilService,
        private readonly commonService: CommonService,
        private readonly jWTService: JWTService,
        private readonly timeZoneService: TimeZoneService,
    ) {}

    @Get('/common-data/home')
    @ApiOperation({ title: 'Home Data' })
    @ApiResponse({
        status: 201,
        description: 'Get Home Data',
        type: CommonHomeDTO,
    })
    @ApiBearerAuth()
    async getDashboardData(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const timeZone = req.headers?.timezoneid
                ? await this.timeZoneService.getTimeZoneById(Number(req.headers.timezoneid))
                : {
                      offset: '+06:00',
                  };
            const decoded: any = await this.jWTService.decodedJWTToken(req.headers.authorization);
            const data = await this.commonService.getDashboardData(timeZone, decoded);
            return this.utilService.getSuccessResponse(data, HOME_RESPONSE.READ.SUCCESS, 'HOME.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(error?.message ?? HOME_RESPONSE.READ.ERROR, 'HOME.NOT_RETRIEVED'),
            );
        }
    }

    @Get('/common-data/employee')
    @ApiOperation({ title: 'Employee Data' })
    @ApiResponse({
        status: 201,
        description: 'Get Employee Data',
        type: CommonEmployeeDTO,
    })
    @ApiBearerAuth()
    async getEmployeedData(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jWTService.decodedJWTToken(req.headers.authorization);
            const data = await this.commonService.getEmployeeData(decoded.company_id);
            return this.utilService.getSuccessResponse(data, EMPLOYEE_RESPONSE.READ.SUCCESS, 'EMPLOYEE.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(
                    error?.message ?? EMPLOYEE_RESPONSE.READ.ERROR,
                    'EMPLOYEE.NOT_RETRIEVED',
                ),
            );
        }
    }
}
