/* eslint-disable @typescript-eslint/no-unused-vars */
import {
    Body,
    Param,
    Post,
    Put,
    Res,
    UseGuards,
    Controller,
    Get,
    Logger,
    Req,
    UseInterceptors,
    ClassSerializerInterceptor,
    InternalServerErrorException,
    ForbiddenException,
    HttpException,
    HttpStatus,
    Query,
} from '@nestjs/common';
import { Response, Request } from 'express';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import {
    AUTH_TOKEN_RENEWED,
    CHANGE_PASSWORD_RESPONSE,
    EMPLOYEE_RESPONSE,
    FORGET_PASSWORD_RESPONSE,
    REGISTER_COMPANY_RESPONSE,
    REGISTER_RESPONSE,
    RESET_PASSWORD_RESPONSE,
    ROLE_UPDATE_RESPONSE,
    USER_RESPONSE,
} from './../../shared/constant/response-message';

import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';
import { AuthGuard, Roles, RoleType, RolesGuard } from '../../security';

import { UserDTO } from '../../service/dto/user.dto';
import { PasswordChangeDTO } from '../../service/dto/password-change.dto';
import { RegisterCompanyDTO } from '../../service/dto/auth/register-company.dto';
import { UserRegistrationDTO } from './../../service/dto/auth/user-registration.dto';
import { BusinessChangeDTO } from './../../service/dto/business-change.dto';
import { ResponseDTO } from './../../service/dto/response.dto';
import { PasswordResetDTO } from '../../service/dto/password-reset.dto';
import { RefreshTokenDTO } from '../../service/dto/auth/refresh-token.dto';
import { RoleUpdateDTO } from '../../service/dto/role-update.dto';

import { AuthService } from '../../service/auth.service';
import { JWTService } from './../../service/jwt.service';
import { UtilService } from '../../shared/util.service';
import { UserService } from '../../service/user.service';

@Controller('api')
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiUseTags('Account Resource')
export class AccountController {
    logger = new Logger('AccountController');

    constructor(
        private readonly authService: AuthService,
        private readonly jwtService: JWTService,
        private readonly utilService: UtilService,
        private readonly userService: UserService,
    ) {}

    @Post('/register')
    @ApiOperation({ title: 'Register user' })
    @ApiResponse({
        status: 201,
        description: 'Registered user',
        type: UserRegistrationDTO,
    })
    async registerAccount(@Body() userDTO: UserRegistrationDTO): Promise<ResponseDTO> {
        try {
            const user = await this.authService.registerNewUser(userDTO);
            if (user) {
                return this.utilService.getSuccessResponse(user, USER_RESPONSE.CREATE.SUCCESS, 'USER.CREATED');
            }
        } catch (error) {
            throw new HttpException(this.utilService.getErrorResponse(error.message, 'USER.NOT_CREATED'), error.status);
        }
    }

    @Get('/register/confirm/:token')
    @ApiOperation({ title: 'Confirm Registration' })
    @ApiResponse({
        status: HttpStatus.ACCEPTED,
        description: 'Confirm Registration',
    })
    async confirmRegistration(@Param('token') token: string): Promise<ResponseDTO> {
        try {
            const result = await this.authService.confirmRegistration(token);
            return this.utilService.getSuccessResponse(result, REGISTER_RESPONSE.CONFIRM.SUCCESS, 'REGISTER.CONFIRMED');
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'REGISTER.NOT_CONFIRMED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Get('/register/employee/confirm/:token')
    @ApiOperation({ title: 'Confirm Employee Account' })
    @ApiResponse({
        status: HttpStatus.ACCEPTED,
        description: 'Confirm Employee Account',
    })
    async confirmEmployee(@Param('token') token: string): Promise<ResponseDTO> {
        try {
            const result = await this.authService.confirmEmployee(token);
            return this.utilService.getSuccessResponse(result, REGISTER_RESPONSE.CONFIRM.SUCCESS, 'EMPLOYEE.CONFIRMED');
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'EMPLOYEE.NOT_CONFIRMED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Get('/register/send/:token')
    @ApiOperation({ title: 'Send Email' })
    @ApiResponse({
        status: HttpStatus.ACCEPTED,
        description: 'Send Email',
    })
    async sendConfirmationMail(@Param('token') token: string): Promise<ResponseDTO> {
        try {
            const result = await this.authService.sendConfirmationEmail(token);
            return this.utilService.getSuccessResponse(result, EMPLOYEE_RESPONSE.SENT_EMAIL.SUCCESS, 'MAIL.SENT');
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'MAIL.NOT_SENT'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Post('/register/company')
    @ApiOperation({ title: 'Register company' })
    @ApiResponse({
        status: 201,
        description: 'Registered company',
        type: RegisterCompanyDTO,
    })
    async registerCompanyAccount(@Body() registerCompanyDTO: RegisterCompanyDTO): Promise<ResponseDTO> {
        try {
            const company = await this.authService.registerCompany(registerCompanyDTO);
            if (company) {
                return this.utilService.getSuccessResponse(
                    company,
                    REGISTER_COMPANY_RESPONSE.CREATE.SUCCESS,
                    'COMPANY.CREATED',
                );
            }
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'COMPANY.NOT_CREATED'),
                error.status,
            );
        }
    }

    @Get('/activate')
    @ApiBearerAuth()
    @UseGuards(AuthGuard, RolesGuard)
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Activate an account' })
    @ApiResponse({
        status: 200,
        description: 'activated',
    })
    activateAccount(@Param() key: string, @Res() res: Response): any {
        throw new InternalServerErrorException();
    }

    @Get('/authenticate')
    @ApiBearerAuth()
    @UseGuards(AuthGuard)
    @ApiOperation({ title: 'Check if the user is authenticated' })
    @ApiResponse({
        status: 200,
        description: 'login authenticated',
    })
    isAuthenticated(@Req() req: Request): any {
        const user: any = req.user;
        return user.username;
    }

    @Get('/account')
    @ApiBearerAuth()
    @UseGuards(AuthGuard)
    @ApiOperation({ title: 'Get the current user.' })
    @ApiResponse({
        status: 200,
        description: 'user retrieved',
    })
    async getAccount(@Req() req: Request): Promise<ResponseDTO> {
        const user: any = req.user;
        const userProfileFound = await this.authService.getAccount(user.id);

        if (userProfileFound) {
            const responseData = {
                data: userProfileFound,
                message: 'User was sucessfully retrieved!',
                translateCode: 'USER.RETRIEVED',
            };
            return responseData;
        }

        const errorResponse = {
            message: 'User Was Not Retrieved!',
            translateCode: 'USER.NOT_RETRIEVED',
        };
        throw new ForbiddenException(errorResponse);
    }

    @Get('/account/menu')
    @ApiBearerAuth()
    @UseGuards(AuthGuard)
    @ApiOperation({ title: 'Get the role.' })
    @ApiResponse({
        status: 200,
        description: 'role retrieved',
    })
    async getRoleMenus(@Req() req: Request): Promise<any> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        return await this.authService.getRole(decoded.role_id);
    }

    @Post('/account/change-business')
    @ApiBearerAuth()
    @UseGuards(AuthGuard)
    @ApiOperation({ title: 'Change current business' })
    @ApiResponse({
        status: 201,
        description: 'user business changed',
    })
    async changeBusiness(@Body() businessChange: BusinessChangeDTO): Promise<ResponseDTO> {
        try {
            const result = await this.authService.changeBusiness(businessChange);
            return this.utilService.getSuccessResponse(result, REGISTER_COMPANY_RESPONSE.SELECTED, 'COMPANY.SELECTED');
        } catch (error) {
            throw new ForbiddenException(this.utilService.getErrorResponse(error.message, 'COMPANY.NOT_SELECTED'));
        }
    }

    @Post('/account/refresh-token')
    @ApiResponse({
        status: 201,
    })
    async generateAuthToken(@Body() refreshToken: RefreshTokenDTO, @Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(refreshToken.refresh_token);
            refreshToken.user_id = decoded.user_id;
            refreshToken.company_id = decoded.company_id;

            const response = await this.authService.generateAuthToken(refreshToken);
            if (response) {
                return this.utilService.getSuccessResponse(response, AUTH_TOKEN_RENEWED.SUCCESS, '');
            } else {
                return this.utilService.getSuccessResponse(response, AUTH_TOKEN_RENEWED.ERROR, '');
            }
        } catch (error) {
            throw new ForbiddenException(this.utilService.getErrorResponse(error.message, ''));
        }
    }

    @Get('/')
    @ApiBearerAuth()
    @UseGuards(AuthGuard)
    @ApiOperation({ title: 'Get the all user.' })
    @ApiResponse({
        status: 200,
        description: 'user retrieved',
    })
    async getAccounts(): Promise<any> {
        return await this.authService.getAccounts();
    }

    @Post('/account')
    @ApiBearerAuth()
    @UseGuards(AuthGuard)
    @ApiOperation({ title: 'Update the current user information' })
    @ApiResponse({
        status: 201,
        description: 'user info updated',
        type: UserDTO,
    })
    async saveAccount(@Req() req: Request, @Body() newUserInfo: UserDTO): Promise<any> {
        const user: any = req.user;
        return await this.authService.updateUserSettings(user.username, newUserInfo);
    }

    @Put('/account/role')
    @ApiBearerAuth()
    @UseGuards(AuthGuard)
    @ApiOperation({ title: 'Update user role' })
    @ApiResponse({
        status: 201,
        description: 'user role updated',
    })
    async updateRole(@Body() roleUpdateDTO: RoleUpdateDTO, @Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const result = await this.authService.updateRole(roleUpdateDTO, decoded.company_id);
            return this.utilService.getSuccessResponse(result, ROLE_UPDATE_RESPONSE.SUCCESS, 'ROLE_UPDATE.SUCCESS');
        } catch (error) {
            throw new ForbiddenException(this.utilService.getErrorResponse(error.message, 'ROLE_UPDATE.FAIL'));
        }
    }

    @Post('/account/change-password')
    @ApiBearerAuth()
    @UseGuards(AuthGuard)
    @ApiOperation({ title: 'Change current password' })
    @ApiResponse({
        status: 201,
        description: 'user password changed',
        type: PasswordChangeDTO,
    })
    async changePassword(@Req() req: Request, @Body() passwordChange: PasswordChangeDTO): Promise<any> {
        try {
            const user: any = req.user;
            const response = await this.authService.changePassword(
                user,
                passwordChange.currentPassword,
                passwordChange.newPassword,
            );
            return this.utilService.getSuccessResponse(response, CHANGE_PASSWORD_RESPONSE.SUCCESS, 'PASSWORD.CHANGE');
        } catch (error) {
            const errorResponse = {
                message: error.message,
                translateCode: 'PASSWORD.NOT_CHANGE',
            };
            throw new ForbiddenException(errorResponse);
        }
    }

    @Post('/account/reset-password/init')
    @ApiBearerAuth()
    @UseGuards(AuthGuard)
    @ApiOperation({ title: 'Send an email to reset the password of the user' })
    @ApiResponse({
        status: 201,
        description: 'mail to reset password sent',
        type: 'string',
    })
    requestPasswordReset(@Req() req: Request, @Body() email: string, @Res() res: Response): any {
        throw new InternalServerErrorException();
    }

    @Post('/account/reset-password')
    @ApiBearerAuth()
    @UseGuards(AuthGuard)
    @ApiOperation({ title: 'Finish to reset the password of the user' })
    @ApiResponse({
        status: 201,
        description: 'password reset',
        type: 'string',
    })
    async resetPassword(@Req() req: Request, @Body() passwordResetDTO: PasswordResetDTO): Promise<ResponseDTO> {
        try {
            const user: any = req.user;
            const existingUser = await this.userService.findByFields({ where: { username: user.username } });
            const response = await this.authService.resetPassword(existingUser, passwordResetDTO.newPassword);
            return this.utilService.getSuccessResponse(response, RESET_PASSWORD_RESPONSE.SUCCESS, 'PASSWORD.RESET');
        } catch (error) {
            const errorResponse = {
                data: { code: error.response?.code },
                message: error.message,
                translateCode: 'USER.NOT_LOGIN',
            };
            throw new ForbiddenException(errorResponse);
        }
    }

    @Get('/forgot-password')
    @ApiOperation({ title: 'Forgot Password' })
    @ApiResponse({
        status: 201,
        description: 'Forgot Password',
        type: ResponseDTO,
    })
    async forgotPassword(@Query('email') email: string): Promise<ResponseDTO> {
        try {
            if (!email) {
                throw new ForbiddenException(this.utilService.getErrorResponse('Email not found', 'EMAIL.NOT_FOUND'));
            }
            const password_recovery_token = await this.authService.forgotPassword(email);
            return this.utilService.getSuccessResponse(
                password_recovery_token,
                FORGET_PASSWORD_RESPONSE.SENT_EMAIL.SUCCESS,
                'EMAIL.SENT',
            );
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(FORGET_PASSWORD_RESPONSE.SENT_EMAIL.ERROR, 'EMAIL.NOT_SENT'),
            );
        }
    }

    @Get('/forgot-password/reset/:id/:token')
    @ApiOperation({ title: 'Reset Password Token Verify' })
    @ApiResponse({
        status: 201,
        description: 'Reset Password Token Verify',
        type: ResponseDTO,
    })
    async passwordRecoveryTokenVerify(@Param('id') id: number, @Param('token') token: string): Promise<ResponseDTO> {
        try {
            const password_recovery_token = await this.authService.passwordRecoveryTokenVerify(Number(id), token);
            return this.utilService.getSuccessResponse(
                password_recovery_token,
                FORGET_PASSWORD_RESPONSE.TOKEN.SUCCESS,
                'TOKEN.VERIFIED',
            );
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(FORGET_PASSWORD_RESPONSE.TOKEN.ERROR, 'TOKEN.INVALID'),
            );
        }
    }

    @Post('/forgot-password/reset/:id/:token')
    @ApiOperation({ title: 'Reset Password' })
    @ApiResponse({
        status: 201,
        description: 'Reset Password',
        type: ResponseDTO,
    })
    async resetForgotPassword(
        @Param('id') id: number,
        @Param('token') token: string,
        @Body() passwordResetDTO: PasswordResetDTO,
    ): Promise<ResponseDTO> {
        try {
            const result = await this.authService.resetForgotPassword(Number(id), token, passwordResetDTO);
            return this.utilService.getSuccessResponse(result, RESET_PASSWORD_RESPONSE.SUCCESS, 'PASSWORD.RESET');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(RESET_PASSWORD_RESPONSE.ERROR, 'PASSWORD.NOT_RESET'),
            );
        }
    }
}
