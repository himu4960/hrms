import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { ReligionAllDTO } from './../../service/dto/religion/religion-all.dto';
import { ReligionCreateDTO } from './../../service/dto/religion/religion-create.dto';
import { ReligionUpdateDTO } from './../../service/dto/religion/religion-update.dto';
import { ReligionGetByLangCodeDTO } from './../../service/dto/religion/religion-get-by-langcode.dto';
import { ReligionDTO } from './../../service/dto/religion/religion.dto';

import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { ReligionService } from '../../service/religion.service';

@Controller('api/religions')
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiUseTags('Religions')
export class ReligionController {
    logger = new Logger('ReligionController');

    constructor(private readonly religionService: ReligionService) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: ReligionAllDTO,
    })
    async getAll(@Req() req: Request) {
        return await this.religionService.findAll();
    }

    @Get('/:langCode')
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: ReligionGetByLangCodeDTO,
    })
    async getOne(@Param('langCode') langCode: string) {
        return await this.religionService.findByLangCode(langCode);
    }

    @PostMethod('/')
    @ApiOperation({ title: 'Create religion' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: ReligionCreateDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() createReligion: ReligionCreateDTO) {
        const created = await this.religionService.save(createReligion, req.user?.id);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Religion', req.user?.id);
        return created;
    }

    @Put('/:id')
    @ApiOperation({ title: 'Update religion with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: ReligionUpdateDTO,
    })
    async putId(@Req() req: Request, @Param('id') id: number, @Body() updateReligion: ReligionUpdateDTO) {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Religion', id);
        return await this.religionService.update(id, updateReligion, req.user?.id);
    }

    @Delete('/:id')
    @ApiOperation({ title: 'Delete religion' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Param('id') id: number): Promise<ReligionDTO> {
        return await this.religionService.deleteById(id);
    }
}
