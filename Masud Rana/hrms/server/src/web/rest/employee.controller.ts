import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
    Query,
    HttpStatus,
    HttpException,
    UploadedFile,
    ForbiddenException,
    Res,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation, ApiImplicitBody } from '@nestjs/swagger';
import { Response } from 'express';

import { EMPLOYEE_RESPONSE, FILE_UPLOAD_RESPONSE } from './../../shared/constant/response-message';

import { EmployeeDTO } from './../../service/dto/employee/employee.dto';
import { SendInviteDTO } from './../../service/dto/auth/send-invite.dto';
import { EmployeeUpdateDTO } from './../../service/dto/employee/employee-update.dto';
import { InvitedNewUserRegistrationDTO } from './../../service/dto/auth/invited-new-user-registration.dto';
import { EmployeeCreateDTO } from './../../service/dto/employee/employee-create.dto';
import { EmployeeDesignationsDTO } from '../../service/dto/employee/employee-designations.dto';
import { ResponseDTO } from './../../service/dto/response.dto';
import { EmployeeSkillDTO } from './../../service/dto/employee-skill.dto';
import { EmployeeAnniversaryResponseDTO } from './../../service/dto/employee/employee-anniversary-response.dto';
import { EmployeeBirthdayResponseDTO } from './../../service/dto/employee/employee-birthday-response.dto';
import { EmployeeFilterDTO } from './../../service/dto/employee/employee-filter.dto';
import { EmployeeListDTO } from './../../service/dto/employee/employee-list.dto';
import { EmployeeActivationDTO } from '../../service/dto/employee/employee-activation.dto';

import { EmployeeService } from '../../service/employee.service';
import { JWTService } from './../../service/jwt.service';
import { UtilService } from './../../shared/util.service';

import { PageRequest, Page } from '../../domain/base/pagination.entity';

import { AuthGuard, RolesGuard } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/employees')
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiUseTags('Employee')
export class EmployeeController {
    logger = new Logger('EmployeeController');

    constructor(
        private readonly employeeService: EmployeeService,
        private readonly jwtService: JWTService,
        private readonly utilService: UtilService,
    ) {}

    @PostMethod('/register/invite/send-invite/:employee_id')
    @ApiBearerAuth()
    @UseGuards(AuthGuard, RolesGuard)
    @ApiOperation({ title: 'Send invite' })
    @ApiResponse({
        status: 201,
        description: 'Send invite',
        type: SendInviteDTO,
    })
    async sendInviteEmail(@Param('employee_id') employee_id: number, @Body() company: any): Promise<ResponseDTO> {
        try {
            const sentEmail = await this.employeeService.sendInviteEmail(employee_id, company);
            return this.utilService.getSuccessResponse(sentEmail, EMPLOYEE_RESPONSE.SENT_EMAIL.SUCCESS, 'EMAIL.SENT');
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'EMAIL.NOT_SENT'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Get('/register/invite/check')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: EmployeeDTO,
    })
    async checkInvite(@Query('token') token: string): Promise<any> {
        try {
            const result = await this.employeeService.checkInvitedToken(token);
            return this.utilService.getSuccessResponse(
                result,
                EMPLOYEE_RESPONSE.VERIFY_TOKEN.SUCCESS,
                'TOKEN.VERIFIED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(
                    error?.message ?? EMPLOYEE_RESPONSE.VERIFY_TOKEN.ERROR,
                    'TOKEN.NOT_VERIFIED',
                ),
                error?.status ?? HttpStatus.FORBIDDEN,
            );
        }
    }

    @PostMethod('/invite/register')
    @ApiResponse({
        status: 200,
        description: 'Register Employee',
        type: EmployeeDTO,
    })
    async registerEmployee(@Body() userDTO: InvitedNewUserRegistrationDTO): Promise<any> {
        try {
            const invitedEmployee = await this.employeeService.registerInvitedEmployee(userDTO);
            return this.utilService.getSuccessResponse(
                invitedEmployee,
                EMPLOYEE_RESPONSE.REGISTER.SUCCESS,
                'EMPLOYEE.REGISTERED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(
                    error?.message ?? EMPLOYEE_RESPONSE.REGISTER.ERROR,
                    'EMPLOYEE.NOT_REGISTERED',
                ),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Get('/')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: EmployeeListDTO,
    })
    async getAll(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const [results, count] = await this.employeeService.findAndCount(
                {
                    skip: +pageRequest.page * pageRequest.size,
                    take: +pageRequest.size,
                    order: {
                        id: 'ASC',
                    },
                },
                decoded.company_id,
            );
            HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
            return this.utilService.getSuccessResponse(results, EMPLOYEE_RESPONSE.READ.SUCCESS, 'EMPLOYEE.RETRIEVED');
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'EMPLOYEE.NOT_RETRIEVED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Get('/dropdown')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiResponse({
        status: 200,
        description: 'List all active records',
        type: EmployeeDTO,
    })
    async getAllForDropdown(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const employees = await this.employeeService.getAllActivatedEmployeesByCompanyId(decoded.company_id);
            return this.utilService.getSuccessResponse(employees, EMPLOYEE_RESPONSE.READ.SUCCESS, 'EMPLOYEE.RETRIEVED');
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'EMPLOYEE.NOT_RETRIEVED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Get('/active')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiResponse({
        status: 200,
        description: 'List all active records',
        type: EmployeeListDTO,
    })
    async getAllActive(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const [employees, counts] = await this.employeeService.filterAndCountFromUserCompany(
                {},
                decoded.company_id,
                null,
            );
            return this.utilService.getSuccessResponse(employees, EMPLOYEE_RESPONSE.READ.SUCCESS, 'EMPLOYEE.RETRIEVED');
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'EMPLOYEE.NOT_RETRIEVED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Get('/inactive')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiResponse({
        status: 200,
        description: 'List all inactive records',
        type: EmployeeListDTO,
    })
    async getAllInactiveEmployees(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const [results, count] = await this.employeeService.getAllInactiveEmployees(
                {
                    skip: +pageRequest.page * pageRequest.size,
                    take: +pageRequest.size,
                    order: pageRequest.sort.asOrder(),
                },
                decoded.company_id,
            );
            HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
            return this.utilService.getSuccessResponse(
                { employees: results, count },
                EMPLOYEE_RESPONSE.READ.SUCCESS,
                'EMPLOYEE.RETRIEVED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'EMPLOYEE.NOT_RETRIEVED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @PostMethod('/filter')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiOperation({ title: 'Filter Employee' })
    @ApiResponse({
        status: 201,
        description: 'List all employees',
        type: EmployeeListDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async filter(@Req() req: Request, @Body() employeeFilterDTO: EmployeeFilterDTO): Promise<ResponseDTO> {
        try {
            const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const [filteredEmployees, count] = await this.employeeService.filterAndCountFromUserCompany(
                {
                    skip: +pageRequest.page * pageRequest.size,
                    take: +pageRequest.size,
                    order: pageRequest.sort.asOrder(),
                },
                decoded.company_id,
                employeeFilterDTO,
            );
            return this.utilService.getSuccessResponse(
                { employees: filteredEmployees, count },
                EMPLOYEE_RESPONSE.READ.SUCCESS,
                'EMPLOYEE.RETRIEVED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'EMPLOYEE.NOT_RETRIEVED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Get('/anniversary')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiResponse({
        status: 200,
        description: 'List all anniversary records',
        type: EmployeeAnniversaryResponseDTO,
        isArray: true,
    })
    async getEmployeesAnniversary(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const month = Number(req.query.month);
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const results = await this.employeeService.getEmployeeAnniversary(decoded.company_id, month);
            return this.utilService.getSuccessResponse(results, EMPLOYEE_RESPONSE.READ.SUCCESS, 'EMPLOYEE.RETRIEVED');
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'EMPLOYEE.NOT_RETRIEVED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Get('/birthday')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiResponse({
        status: 200,
        description: 'List all birthday records',
        type: EmployeeBirthdayResponseDTO,
        isArray: true,
    })
    async getEmployeesBirthday(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const month = Number(req.query.month);
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const results = await this.employeeService.getEmployeeBirthday(decoded.company_id, month);
            return this.utilService.getSuccessResponse(results, EMPLOYEE_RESPONSE.READ.SUCCESS, 'EMPLOYEE.RETRIEVED');
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'EMPLOYEE.NOT_RETRIEVED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Get('/:id')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: EmployeeDTO,
    })
    async getOne(@Param('id') employeeId: number, @Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const findEmployee = await this.employeeService.findById(employeeId, decoded.company_id);
            if (findEmployee) {
                return this.utilService.getSuccessResponse(
                    findEmployee,
                    EMPLOYEE_RESPONSE.READ.SUCCESS,
                    'EMPLOYEE.RETRIEVED',
                );
            }
            return this.utilService.getSuccessResponse(null, EMPLOYEE_RESPONSE.READ.NOT_FOUND, 'EMPLOYEE.RETRIEVED');
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'EMPLOYEE.NOT_RETRIEVED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @PostMethod('/')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create employee' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: [EmployeeDTO],
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    @ApiImplicitBody({ name: 'employeeDTOs', type: [EmployeeCreateDTO] })
    async post(@Req() req: Request, @Body() employeeDTOs: any): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const createdEmployee = await this.employeeService.save(employeeDTOs, decoded.company_id, req.user?.id);
            return this.utilService.getSuccessResponse(
                createdEmployee,
                EMPLOYEE_RESPONSE.CREATE.SUCCESS,
                'EMPLOYEE.CREATED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'EMPLOYEE.NOT_CREATED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Put('/activation')
    @ApiOperation({ title: 'Enable or disable employee' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
    })
    async controlActivation(
        @Req() req: Request,
        @Body() employeeActivationDTO: EmployeeActivationDTO,
    ): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const response = await this.employeeService.updateActiveStatus(employeeActivationDTO, decoded.company_id);
            return this.utilService.getSuccessResponse(
                response,
                EMPLOYEE_RESPONSE.ACTIVATION_UPDATE.SUCCESS,
                'EMPLOYEE.ACTIVATION_STATUS_UPDATE',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'EMPLOYEE.ACTIVATION_STATUS_NOT_UPDATE'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @PostMethod('/skill/assign')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiOperation({ title: 'Assign designation' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: EmployeeDesignationsDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async assignSkill(@Req() req: Request, @Body() employeeSkill: EmployeeSkillDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const assignedSkill = await this.employeeService.assignSkill(employeeSkill, decoded.company_id);
            return this.utilService.getSuccessResponse(
                assignedSkill,
                EMPLOYEE_RESPONSE.ASSIGN_SKILL.SUCCESS,
                'EMPLOYEE.ASSIGNED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'EMPLOYEE.NOT_ASSIGNED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Delete('/skill/unassign')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiOperation({ title: 'Unassign skill' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully unassigned.',
    })
    async unassignSkill(@Req() req: Request, @Body() employeeSkill: EmployeeSkillDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const deletedSkill = await this.employeeService.unassignSkill(employeeSkill, decoded.company_id);
            return this.utilService.getSuccessResponse(
                deletedSkill,
                EMPLOYEE_RESPONSE.UNASSIGN_SKILL.SUCCESS,
                'EMPLOYEE.DELETED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'EMPLOYEE.NOT_ASSIGNED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @PostMethod('/designation/assign')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiOperation({ title: 'Assign designation' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: EmployeeDesignationsDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async assignDesignation(
        @Req() req: Request,
        @Body() employeeDesginationDTO: EmployeeDesignationsDTO,
    ): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const assignedDesignation = await this.employeeService.assignDesignation(
                employeeDesginationDTO,
                decoded.company_id,
            );
            return this.utilService.getSuccessResponse(
                assignedDesignation,
                EMPLOYEE_RESPONSE.ASSIGN_DESIGNATION.SUCCESS,
                'EMPLOYEE.ASSIGNED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'EMPLOYEE.NOT_ASSIGNED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Delete('/designation/unassign')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiOperation({ title: 'Unassign designation' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async unassignDesignation(
        @Req() req: Request,
        @Body() employee_designation_ids: EmployeeDesignationsDTO,
    ): Promise<ResponseDTO> {
        const ids = {
            employee_id: employee_designation_ids.employee_id,
            designation_id: employee_designation_ids.designation_id,
        };
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const deletedResult = await this.employeeService.unassignDesignation(ids, decoded.company_id);
            return this.utilService.getSuccessResponse(
                deletedResult,
                EMPLOYEE_RESPONSE.UNASSIGN_DESIGNATION.SUCCESS,
                'DESIGNATION.UNASSIGNED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'DESIGNATION.NOT_UNASSIGNED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Put('/:employee_id')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update employee with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: EmployeeDTO,
    })
    async putId(
        @Req() req: Request,
        @Param('employee_id') employee_id: number,
        @Body() employeeDTO: EmployeeUpdateDTO,
    ): Promise<ResponseDTO> {
        try {
            HeaderUtil.addEntityCreatedHeaders(req.res, 'Employee', employeeDTO.id);
            const updatedResult = await this.employeeService.updateById(employee_id, employeeDTO, req.user?.id);
            return this.utilService.getSuccessResponse(
                updatedResult,
                EMPLOYEE_RESPONSE.UPDATE.SUCCESS,
                'EMPLOYEE.UPDATED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(EMPLOYEE_RESPONSE.UPDATE.ERROR, 'EMPLOYEE.NOT_UPDATED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Put('/change-branch/:employee_id')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiOperation({ title: 'Change Branch employee with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: EmployeeDTO,
    })
    async changeBranchByEmployeeId(
        @Req() req: Request,
        @Param('employee_id') employee_id: number,
        @Body() employeeDTO: EmployeeUpdateDTO,
    ): Promise<ResponseDTO> {
        try {
            HeaderUtil.addEntityCreatedHeaders(req.res, 'Employee', employeeDTO.id);
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const updatedResult = await this.employeeService.changeBranchByEmployeeId(
                employee_id,
                employeeDTO.branch_id,
                decoded.company_id,
            );
            return this.utilService.getSuccessResponse(
                updatedResult,
                EMPLOYEE_RESPONSE.UPDATE.CHANGE_BRANCH_SUCCESS,
                'EMPLOYEE.UPDATED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(EMPLOYEE_RESPONSE.UPDATE.CHANGE_BRANCH_ERROR, 'EMPLOYEE.NOT_UPDATED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Delete('/:id')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete employee' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Param('id') id: number): Promise<ResponseDTO> {
        try {
            const deletedResult = await this.employeeService.deleteById(id);
            return this.utilService.getSuccessResponse(
                deletedResult,
                EMPLOYEE_RESPONSE.DELETE.SUCCESS,
                'EMPLOYEE.DELETED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(EMPLOYEE_RESPONSE.DELETE.ERROR, 'EMPLOYEE.NOT_DELETED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @PostMethod('/image/upload')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiOperation({ title: 'Upload image' })
    @ApiResponse({
        status: 201,
        description: 'The file was successfully uploaded.',
    })
    @UseInterceptors(FileInterceptor('file'))
    async upload(@UploadedFile() file: Express.Multer.File, @Body() body: any): Promise<ResponseDTO> {
        try {
            const response = await this.employeeService.uploadImage(file, body.id);
            return this.utilService.getSuccessResponse(
                response,
                FILE_UPLOAD_RESPONSE.SUCCESS,
                'FILE_UPLOAD_RESPONSE.SUCCESS',
            );
        } catch {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(FILE_UPLOAD_RESPONSE.ERROR, 'FILE_UPLOAD_RESPONSE.ERROR'),
            );
        }
    }

    @PostMethod('/export')
    @UseGuards(AuthGuard, RolesGuard)
    @ApiBearerAuth()
    @ApiResponse({
        status: 200,
        description: 'List all records',
    })
    async exportEmployees(
        @Query('type') exportType: string,
        @Req() req: Request,
        @Res() res: Response,
    ): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const result = await this.employeeService.exportEmployees(decoded.company_id, res, exportType);
            return this.utilService.getSuccessResponse(null, EMPLOYEE_RESPONSE.EXPORT.SUCCESS, 'EXPORTED');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'NOT_EXPORTED');
        }
    }
}
