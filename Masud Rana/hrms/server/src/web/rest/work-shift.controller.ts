import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
    HttpStatus,
    HttpException,
    Query,
    UsePipes,
    ValidationPipe,
    ParseBoolPipe,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { ResponseDTO } from './../../service/dto/response.dto';
import { WorkShiftDTO } from '../../service/dto/work-shift/work-shift.dto';
import { WorkShiftFilterParamsDTO } from './../../service/dto/work-shift/work-shift-filter-params.dto';
import { WorkShiftCreateDTO } from './../../service/dto/work-shift/work-shift-create.dto';
import { WorkShiftUpdateDTO } from './../../service/dto/work-shift/work-shift-update.dto';
import { DateRangeDTO } from '../../service/dto/date-range.dto';
import { CreateManyWorkShiftsDTO } from './../../service/dto/work-shift/work-shift-create-many.dto';

import { JWTService } from '../../service/jwt.service';
import { WorkShiftService } from '../../service/work-shift.service';
import { UtilService } from '../../shared/util.service';

import { PageRequest, Page } from '../../domain/base/pagination.entity';

import { AuthGuard, RolesGuard } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { WORK_SHIFT_RESPONSE } from '../../shared/constant/response-message';

@Controller('api/work-shifts')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Work Shifts')
@UsePipes(new ValidationPipe({ transform: true }))
export class WorkShiftController {
    logger = new Logger('WorkShiftController');

    constructor(
        private readonly workShiftService: WorkShiftService,
        private readonly jwtService: JWTService,
        private readonly utilService: UtilService,
    ) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        isArray: true,
        type: WorkShiftDTO,
    })
    async getAll(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
            const [results, count] = await this.workShiftService.findAndCount(
                {
                    skip: +pageRequest.page * pageRequest.size,
                    take: +pageRequest.size,
                    order: pageRequest.sort.asOrder(),
                },
                decoded.company_id,
            );
            HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
            return this.utilService.getSuccessResponse(
                results,
                WORK_SHIFT_RESPONSE.READ.SUCCESS,
                'WORK_SHIFT_RETRIVED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(WORK_SHIFT_RESPONSE.READ.ERROR, 'WORK_SHIFT.NOT_RETRIEVED'),
                error?.status ?? HttpStatus.NOT_FOUND,
            );
        }
    }

    @Get('/upcoming')
    @ApiResponse({
        status: 200,
        description: 'List all upcoming records',
        isArray: true,
        type: WorkShiftDTO,
    })
    async getAllUpcoming(
        @Query('isAll', new ParseBoolPipe()) isAll: boolean,
        @Req() req: Request,
    ): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const results = await this.workShiftService.getUpcomingShifts(
                decoded.company_id,
                decoded.employee_id,
                isAll,
            );
            return this.utilService.getSuccessResponse(
                results,
                WORK_SHIFT_RESPONSE.READ.SUCCESS,
                'WORK_SHIFT_RETRIVED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(WORK_SHIFT_RESPONSE.READ.ERROR, 'WORK_SHIFT.NOT_RETRIEVED'),
                error?.status ?? HttpStatus.NOT_FOUND,
            );
        }
    }

    @Get('/upcoming/:employeeId')
    @ApiResponse({
        status: 200,
        description: 'List of employee upcoming records',
        isArray: true,
        type: WorkShiftDTO,
    })
    async getUpcomingShiftsByEmployeeId(
        @Query('isAll', new ParseBoolPipe()) isAll: boolean,
        @Param('employeeId') employeeId: number,
        @Req() req: Request,
    ): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const results = await this.workShiftService.getUpcomingShifts(decoded.company_id, employeeId, isAll);
            return this.utilService.getSuccessResponse(
                results,
                WORK_SHIFT_RESPONSE.READ.SUCCESS,
                'WORK_SHIFT_RETRIVED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(WORK_SHIFT_RESPONSE.READ.ERROR, 'WORK_SHIFT.NOT_RETRIEVED'),
                error?.status ?? HttpStatus.NOT_FOUND,
            );
        }
    }

    @Get('/:id')
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: WorkShiftDTO,
    })
    async getOne(@Param('id') id: number): Promise<ResponseDTO> {
        try {
            const response = await this.workShiftService.findById(id);
            return this.utilService.getSuccessResponse(
                response,
                WORK_SHIFT_RESPONSE.READ.SUCCESS,
                'WORK_SHIFT_RETRIVED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(WORK_SHIFT_RESPONSE.READ.ERROR, 'WORK_SHIFT.NOT_RETRIEVED'),
                error?.status ?? HttpStatus.NOT_FOUND,
            );
        }
    }

    @PostMethod('/')
    @ApiOperation({ title: 'Create workShift' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: WorkShiftDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() workShiftDTO: WorkShiftCreateDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const total_work_shift_duration: number = (workShiftDTO.end_date - workShiftDTO.start_date) / 1000;

            const {
                branch_id,
                employee_id,
                title,
                designation_id,
                start_date,
                end_date,
                start_time,
                end_time,
            } = workShiftDTO;

            const createWorkShiftDTO = {
                title,
                branch_id,
                designation_id,
                start_date,
                end_date,
                start_time,
                end_time,
                company_id: decoded.company_id,
            };

            const employees = [
                {
                    company_id: decoded.company_id,
                    branch_id,
                    id: employee_id,
                    designation_id,
                },
            ];
            const created = await this.workShiftService.save(
                {
                    ...createWorkShiftDTO,
                    total_work_shift_duration: total_work_shift_duration,
                    employees,
                },
                decoded.company_id,
                req.user?.id,
            );
            HeaderUtil.addEntityCreatedHeaders(req.res, 'WorkShift', created.id);
            return this.utilService.getSuccessResponse(
                created,
                WORK_SHIFT_RESPONSE.CREATE.SUCCESS,
                'WORK_SHIFT.CREATED',
            );
        } catch (error) {
            return this.utilService.getErrorResponse(
                error?.message ?? WORK_SHIFT_RESPONSE.CREATE.ERROR,
                'WORK_SHIFT.NOT_CREATED',
            );
        }
    }

    @PostMethod('/many')
    @ApiOperation({ title: 'Create many workShift' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: WorkShiftDTO,
        isArray: true,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async postMany(@Req() req: Request, @Body() workShiftDTO: CreateManyWorkShiftsDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);

            const created = await this.workShiftService.saveMany(workShiftDTO, decoded.company_id, req.user?.id);
            return this.utilService.getSuccessResponse(
                created,
                WORK_SHIFT_RESPONSE.CREATE.SUCCESS,
                'WORK_SHIFT.CREATED',
            );
        } catch (error) {
            return this.utilService.getErrorResponse(
                error?.message ?? WORK_SHIFT_RESPONSE.CREATE.ERROR,
                'WORK_SHIFT.NOT_CREATED',
            );
        }
    }

    @PostMethod('/filter')
    @ApiOperation({ title: 'Filter workShift' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully filtered.',
        isArray: true,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async filterWorkShift(
        @Req() req: Request,
        @Body() workShiftFilterParamsDTO: WorkShiftFilterParamsDTO,
    ): Promise<ResponseDTO> {
        try {
            const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
            const options = {
                skip: +pageRequest.page * pageRequest.size,
                take: +pageRequest.size,
                order: pageRequest.sort.asOrder(),
            };

            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const result = await this.workShiftService.filter(options, workShiftFilterParamsDTO, decoded.company_id);
            return this.utilService.getSuccessResponse(result, WORK_SHIFT_RESPONSE.READ.SUCCESS, 'WORK_SHIFT.FILTERED');
        } catch (error) {
            return this.utilService.getErrorResponse(
                error?.message ?? WORK_SHIFT_RESPONSE.READ.ERROR,
                'WORK_SHIFT.NOT_FILTERED',
            );
        }
    }

    @Put('/publish')
    @ApiOperation({ title: 'Publish workShifts' })
    @ApiResponse({
        status: 200,
        description: 'The records has been successfully published.',
    })
    async publish(@Req() req: Request, @Body() workShiftIds: number[]) {
        try {
            const result = await workShiftIds.map(async (id: number) => {
                const workShift = await this.workShiftService.findById(id);
                return await this.workShiftService.publishWorkShift(workShift, req.user?.id);
            });
            return this.utilService.getSuccessResponse(
                result,
                WORK_SHIFT_RESPONSE.PUBLISH.SUCCESS,
                'WORK_SHIFT.PUBLISHED',
            );
        } catch (error) {
            return this.utilService.getErrorResponse(
                error?.message ?? WORK_SHIFT_RESPONSE.PUBLISH.ERROR,
                'WORK_SHIFT.NOT_PUBLISHED',
            );
        }
    }

    @Put('/:id')
    @ApiOperation({ title: 'Update workShift with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: WorkShiftDTO,
    })
    async putId(
        @Req() req: Request,
        @Param('id') id: number,
        @Body() workShiftDTO: WorkShiftUpdateDTO,
    ): Promise<ResponseDTO> {
        try {
            HeaderUtil.addEntityCreatedHeaders(req.res, 'WorkShift', id);
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);

            const response = await this.workShiftService.update(
                id,
                { ...workShiftDTO },
                decoded.company_id,
                req.user?.id,
            );
            return this.utilService.getSuccessResponse(
                response,
                WORK_SHIFT_RESPONSE.UPDATE.SUCCESS,
                'WORK_SHIFT_UPDATED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(WORK_SHIFT_RESPONSE.READ.ERROR, 'WORK_SHIFT.NOT_UPDATED'),
                error?.status ?? HttpStatus.NOT_FOUND,
            );
        }
    }

    @Delete('/:id')
    @ApiOperation({ title: 'Delete workShift' })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'The record has been successfully deleted.',
        type: WorkShiftDTO,
    })
    async deleteById(@Req() req: Request, @Param('id') id: number): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const deletedResult = await this.workShiftService.deleteById(id, decoded.company_id);
            if (deletedResult) {
                const deletedResponse = {
                    data: deletedResult,
                    message: `Work shift has been deleted successfully`,
                };
                return deletedResponse;
            }
        } catch (error) {
            throw new HttpException(error.response, error.status);
        }
    }

    @Delete()
    @ApiOperation({ title: 'Delete workShifts' })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'The records has been successfully deleted.',
        type: WorkShiftDTO,
    })
    async deleteByDateRange(@Req() req: Request, @Body() dateRange: DateRangeDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const deletedResult = await this.workShiftService.deleteByDateRange(dateRange, decoded.company_id);
            if (deletedResult) {
                return this.utilService.getSuccessResponse(deletedResult, WORK_SHIFT_RESPONSE.DELETE.SUCCESS, '');
            }
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, '');
        }
    }
}
