import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
    ForbiddenException,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { LEAVE_RESPONSE } from './../../shared/constant/response-message';

import { LeaveDTO } from '../../service/dto/leave/leave.dto';
import { LeaveCreateDTO } from '../../service/dto/leave/leave-create.dto';
import { LeaveParamsDTO } from './../../service/dto/leave/leave-params.dto';
import { LeaveApproveDTO } from '../../service/dto/leave/leave-approve.dto';
import { LeaveRejectDTO } from '../../service/dto/leave/leave-reject.dto';
import { FilterLeaveDTO } from '../../service/dto/leave/leave-filter.dto';
import { LeaveRequestDTO } from '../../service/dto/leave/leave-request.dto';
import { EmployeeDTO } from './../../service/dto/employee/employee.dto';
import { ResponseDTO } from './../../service/dto/response.dto';

import { LeaveService } from '../../service/leave.service';
import { EmployeeService } from '../../service/employee.service';
import { JWTService } from '../../service/jwt.service';
import { UtilService } from './../../shared/util.service';

import { PageRequest, Page } from '../../domain/base/pagination.entity';

import { AuthGuard, RolesGuard } from '../../security';

import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/leaves')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Leaves')
export class LeaveController {
    logger = new Logger('LeaveController');

    constructor(
        private readonly leaveService: LeaveService,
        private readonly employeeService: EmployeeService,
        private readonly jwtService: JWTService,
        private readonly utilService: UtilService,
    ) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: LeaveDTO,
    })
    async getAll(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
            const [results, count] = await this.leaveService.findAndCount(
                {
                    skip: +pageRequest.page * pageRequest.size,
                    take: +pageRequest.size,
                    order: pageRequest.sort.asOrder(),
                },
                decoded.company_id,
            );
            HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
            return this.utilService.getSuccessResponse(results, LEAVE_RESPONSE.READ.SUCCESS, 'LEAVE.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(LEAVE_RESPONSE.READ.ERROR, 'LEAVE.NOT_RETRIEVED'),
            );
        }
    }

    @Get('/on-leaves')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: LeaveDTO,
    })
    async getOnLeaves(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
            const [results, count] = await this.leaveService.findAndCountOnLeaves(
                {
                    skip: +pageRequest.page * pageRequest.size,
                    take: +pageRequest.size,
                    order: pageRequest.sort.asOrder(),
                },
                decoded.company_id,
            );
            HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
            return this.utilService.getSuccessResponse(results, LEAVE_RESPONSE.READ.SUCCESS, 'LEAVE.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(LEAVE_RESPONSE.READ.ERROR, 'LEAVE.NOT_RETRIEVED'),
            );
        }
    }

    @PostMethod('/data')
    @ApiResponse({
        status: 200,
        description: 'List all leave data',
        type: LeaveDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async getLeaveData(@Req() req: Request, @Body() leaveParamsDTO: LeaveParamsDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const leaveData = await this.leaveService.getLeaveData(leaveParamsDTO, decoded.company_id);
            return this.utilService.getSuccessResponse(leaveData, LEAVE_RESPONSE.READ.SUCCESS, 'LEAVE.RETRIEVED');
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(LEAVE_RESPONSE.READ.ERROR, 'LEAVE.NOT_RETRIEVED'),
            );
        }
    }

    @PostMethod('/filter')
    @ApiResponse({
        status: 200,
        description: 'List filtered records',
        type: LeaveDTO,
    })
    async getAllByFiltered(@Req() req: Request, @Body() filterLeaveDTO: FilterLeaveDTO): Promise<EmployeeDTO[]> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        return await this.leaveService.filter(filterLeaveDTO, decoded.company_id);
    }

    @Get('/:id')
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: LeaveDTO,
    })
    async getOne(@Param('id') id: number): Promise<LeaveDTO> {
        return await this.leaveService.findById(id);
    }

    @PostMethod('/')
    @ApiOperation({ title: 'Create leave request by admin' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: LeaveDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() leaveDTO: LeaveCreateDTO): Promise<LeaveDTO> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const employee: EmployeeDTO = await this.employeeService.findOneById(leaveDTO.employee_id);

        const created = await this.leaveService.save(
            {
                ...leaveDTO,
                company_id: decoded.company_id,
                branch_id: employee.branch_id,
            },
            req.user?.id,
        );
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Leave', created.id);
        return created;
    }

    @PostMethod('/request')
    @ApiOperation({ title: 'Create leave request by employee' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: LeaveRequestDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async postByEmployee(@Req() req: Request, @Body() leaveDTO: LeaveRequestDTO): Promise<LeaveDTO> {
        const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
        const employee: EmployeeDTO = await this.employeeService.findOneById(req.user?.id);

        const created = await this.leaveService.save(
            {
                ...leaveDTO,
                company_id: decoded.company_id,
                employee_id: employee.id,
                branch_id: employee.branch_id,
            },
            req.user?.id,
        );
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Leave', created.id);
        return created;
    }

    @Put('/')
    @ApiOperation({ title: 'Update leave' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: LeaveDTO,
    })
    async put(@Req() req: Request, @Body() leaveDTO: LeaveDTO): Promise<LeaveDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Leave', leaveDTO.id);
        return await this.leaveService.update(leaveDTO, req.user?.id);
    }

    @Put('/review/request/:id')
    @ApiOperation({ title: 'Review Request Leave' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully approved.',
    })
    async reviewRequest(@Req() req: Request, @Param('id') id: number) {
        try {
            const leave: LeaveDTO = await this.leaveService.findById(id);
            const response = await this.leaveService.reviewRequest(leave);
            return this.utilService.getSuccessResponse(response, LEAVE_RESPONSE.REQUEST.SUCCESS, 'REQUEST.SUCCESS');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'REQUEST.FAILED');
        }
    }

    @Put('/review/approve/:id')
    @ApiOperation({ title: 'Approve review leave' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully approved.',
    })
    async reviewApprove(@Param('id') id: number) {
        try {
            let leave: LeaveDTO = await this.leaveService.findById(id);
            const response = await this.leaveService.reviewApprove(leave);
            return this.utilService.getSuccessResponse(response, LEAVE_RESPONSE.APPROVE.SUCCESS, 'APPROVE.SUCCESS');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'APPROVE.FAILED');
        }
    }

    @Put('/review/reject/:id')
    @ApiOperation({ title: 'Approve review leave' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully approved.',
    })
    async reviewReject(@Param('id') id: number) {
        try {
            let leave: LeaveDTO = await this.leaveService.findById(id);
            const response = await this.leaveService.reviewReject(leave);
            return this.utilService.getSuccessResponse(response, LEAVE_RESPONSE.APPROVE.SUCCESS, 'APPROVE.SUCCESS');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'APPROVE.FAILED');
        }
    }

    @Put('/approve/:id')
    @ApiOperation({ title: 'Approve Leave' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully approved.',
    })
    async publish(@Req() req: Request, @Param('id') id: number, @Body() leaveApproveDTO: LeaveApproveDTO) {
        try {
            let leave: LeaveDTO = await this.leaveService.findById(id);
            leave.approved_note = leaveApproveDTO.approved_note;
            const response = await this.leaveService.approveLeave(leave, req.user?.id);
            return this.utilService.getSuccessResponse(response, LEAVE_RESPONSE.APPROVE.SUCCESS, 'APPROVE.SUCCESS');
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'APPROVE.FAILED');
        }
    }

    @Put('/reject/:id')
    @ApiOperation({ title: 'Reject Leave' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully rejected.',
    })
    async reject(@Req() req: Request, @Param('id') id: number, @Body() leaveRejectDTO: LeaveRejectDTO) {
        let leave: LeaveDTO = await this.leaveService.findById(id);
        leave.rejected_note = leaveRejectDTO.rejected_note;
        return await this.leaveService.rejectLeave(leave, req.user?.id);
    }

    @Put('/:id')
    @ApiOperation({ title: 'Update leave with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: LeaveDTO,
    })
    async putId(@Req() req: Request, @Body() leaveDTO: LeaveDTO): Promise<LeaveDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Leave', leaveDTO.id);
        return await this.leaveService.update(leaveDTO, req.user?.id);
    }

    @Put('/cancel/:id')
    @ApiOperation({ title: 'Cancel Request leave' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
    })
    async cancelRequest(@Req() req: Request, @Param('id') id: number): Promise<ResponseDTO> {
        try {
            const leave: LeaveDTO = await this.leaveService.findById(id);
            const result = await this.leaveService.cancelRequest(leave, req.user?.id);
            return this.utilService.getSuccessResponse(result, LEAVE_RESPONSE.CANCEL.SUCCESS, 'LEAVE.CANCELED');
        } catch (error) {
            throw new ForbiddenException(this.utilService.getErrorResponse(error.message, 'LEAVE.NOT_CANCELED'));
        }
    }
}
