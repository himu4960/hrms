import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { MaritalStatusAllDTO } from './../../service/dto/marital-status/marital-status-all.dto';
import { MaritalStatusCreateDTO } from './../../service/dto/marital-status/marital-status-create.dto';
import { MaritalStatusUpdateDTO } from './../../service/dto/marital-status/marital-status-update.dto';
import { MaritalStatusGetByLangCodeDTO } from '../../service/dto/marital-status/marital-status-get-by-langcode.dto';
import { MaritalStatusDTO } from './../../service/dto/marital-status/marital-status.dto';

import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { MaritalStatusService } from './../../service/marital-status.service';

@Controller('api/marital-statuses')
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiUseTags('Marital Status')
export class MaritalStatusController {
    logger = new Logger('MaritalStatusController');

    constructor(private readonly maritalStatusService: MaritalStatusService) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: MaritalStatusAllDTO,
    })
    async getAll(@Req() req: Request) {
        return await this.maritalStatusService.findAll();
    }

    @Get('/:langCode')
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: MaritalStatusGetByLangCodeDTO,
    })
    async getOne(@Param('langCode') langCode: string) {
        return await this.maritalStatusService.findByLangCode(langCode);
    }

    @PostMethod('/')
    @ApiOperation({ title: 'Create maritalStatus' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: MaritalStatusCreateDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() createMaritalStatus: MaritalStatusCreateDTO) {
        const created = await this.maritalStatusService.save(createMaritalStatus, req.user?.id);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'MaritalStatus', req.user?.id);
        return created;
    }

    @Put('/:id')
    @ApiOperation({ title: 'Update maritalStatus with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: MaritalStatusUpdateDTO,
    })
    async putId(@Req() req: Request, @Param('id') id: number, @Body() updateMaritalStatus: MaritalStatusUpdateDTO) {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'MaritalStatus', id);
        return await this.maritalStatusService.update(id, updateMaritalStatus, req.user?.id);
    }

    @Delete('/:id')
    @ApiOperation({ title: 'Delete maritalStatus' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Param('id') id: number): Promise<MaritalStatusDTO> {
        return await this.maritalStatusService.deleteById(id);
    }
}
