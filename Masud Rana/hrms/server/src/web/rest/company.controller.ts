import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
    UploadedFile,
    ForbiddenException,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
    ApiBearerAuth,
    ApiUseTags,
    ApiResponse,
    ApiOperation,
    ApiConsumes,
    ApiImplicitBody,
    ApiImplicitFile,
} from '@nestjs/swagger';

import { CompanyDTO } from '../../service/dto/company.dto';
import { ResponseDTO } from '../../service/dto/response.dto';
import { UploadLogoDTO } from '../../service/dto/upload-logo.dto';

import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { AuthGuard } from '../../security';

import { CompanyService } from '../../service/company.service';
import { UtilService } from '../../shared/util.service';
import { JWTService } from '../../service/jwt.service';

import { FILE_UPLOAD_RESPONSE, REGISTER_COMPANY_RESPONSE } from '../../shared/constant/response-message';

@Controller('api/companies')
@UseGuards(AuthGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Companies')
export class CompanyController {
    logger = new Logger('CompanyController');

    constructor(
        private readonly companyService: CompanyService,
        private readonly jwtService: JWTService,
        private readonly utilService: UtilService,
    ) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: CompanyDTO,
    })
    async getAll(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const respose = await this.companyService.findByUserId(req.user?.id);
            return this.utilService.getSuccessResponse(
                respose,
                REGISTER_COMPANY_RESPONSE.READ.SUCCESS,
                'ADD_NEW_COMPANY',
            );
        } catch (error) {
            return this.utilService.getErrorResponse(error.message, 'FAILED_ADD_NEW_COMPANY');
        }
    }

    @Get('/:id')
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: CompanyDTO,
    })
    async getOne(@Param('id') id: number): Promise<CompanyDTO> {
        return await this.companyService.findById(id);
    }

    @PostMethod('/')
    @ApiOperation({ title: 'Create company' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: CompanyDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() companyDTO: CompanyDTO): Promise<ResponseDTO> {
        try {
            const response = await this.companyService.save(companyDTO, req.user?.id);
            return this.utilService.getSuccessResponse(
                response,
                REGISTER_COMPANY_RESPONSE.CREATE.SUCCESS,
                'COMPANY.CREATED',
            );
        } catch (error) {
            return this.utilService.getErrorResponse(
                error?.message ?? REGISTER_COMPANY_RESPONSE.CREATE.ERROR,
                'COMPANY.NOT_CREATED',
            );
        }
    }

    @Put('/:id')
    @ApiOperation({ title: 'Update company with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: CompanyDTO,
    })
    async putId(@Req() req: Request, @Body() companyDTO: CompanyDTO): Promise<ResponseDTO> {
        try {
            HeaderUtil.addEntityCreatedHeaders(req.res, 'Company', companyDTO.id);
            const updated = await this.companyService.update(companyDTO, req.user?.id);
            return this.utilService.getSuccessResponse(
                updated,
                REGISTER_COMPANY_RESPONSE.UPDATE.SUCCESS,
                'COMPANY.UPDATED',
            );
        } catch (error) {
            return this.utilService.getErrorResponse(
                error?.message ?? REGISTER_COMPANY_RESPONSE.UPDATE.ERROR,
                'COMPANY.NOT_UPDATED',
            );
        }
    }

    @Delete('/:id')
    @ApiOperation({ title: 'Delete company' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: number): Promise<ResponseDTO> {
        try {
            const deleted = await this.companyService.deleteById(id);
            return this.utilService.getSuccessResponse(
                deleted,
                REGISTER_COMPANY_RESPONSE.DELETE.ERROR,
                'COMPANY.DELETED',
            );
        } catch (error) {
            return this.utilService.getErrorResponse(
                error?.message ?? REGISTER_COMPANY_RESPONSE.DELETE.ERROR,
                'COMPANY.NOT_DELETED',
            );
        }
    }

    @PostMethod('upload/logo')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Upload company logo' })
    @ApiResponse({
        status: 201,
        description: 'The file was successfully uploaded.',
    })
    @UseInterceptors(FileInterceptor('file'))
    @ApiConsumes('multipart/form-data')
    @ApiImplicitFile({
        name: 'file',
        required: true,
    })
    @ApiImplicitBody({
        name: 'file',
        type: UploadLogoDTO,
    })
    async upload(
        @UploadedFile() file: Express.Multer.File,
        @Body() uploadFileDto: UploadLogoDTO,
        @Req() req: Request,
    ): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            if (decoded || req.user?.id) {
                uploadFileDto.user_id = decoded.user_id ? decoded.user_id : req.user.id;
            }
            const response = await this.companyService.uploadLogo(file, uploadFileDto);
            return this.utilService.getSuccessResponse(
                response,
                FILE_UPLOAD_RESPONSE.SUCCESS,
                'FILE_UPLOAD_RESPONSE.SUCCESS',
            );
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(error.message, 'FILE_UPLOAD_RESPONSE.ERROR'),
            );
        }
    }
}
