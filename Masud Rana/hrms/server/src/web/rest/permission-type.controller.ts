import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
} from '@nestjs/common';

import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';

import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';

import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { PageRequest, Page } from '../../domain/base/pagination.entity';

import { PermissionTypeDTO } from '../../service/dto/permission-type.dto';

import { PermissionTypeService } from '../../service/permission-type.service';

@Controller('api/permission-types')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Permission Types')
export class PermissionTypeController {
    logger = new Logger('PermissionTypeController');

    constructor(private readonly permissionTypeService: PermissionTypeService) { }

    @Get('/')
    @Roles(RoleType.OWNER)
    @ApiResponse({
        status: 200,
        description: 'List all permission type records',
        type: PermissionTypeDTO,
    })
    async getAll(@Req() req: Request): Promise<PermissionTypeDTO[]> {
        const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
        const [results, count] = await this.permissionTypeService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.OWNER)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: PermissionTypeDTO,
    })
    async getOne(@Param('id') id: number): Promise<PermissionTypeDTO> {
        return await this.permissionTypeService.findById(id);
    }

    @PostMethod('/')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Create permissionType' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: PermissionTypeDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() permissionTypeDTO: PermissionTypeDTO): Promise<PermissionTypeDTO> {
        const created = await this.permissionTypeService.save(permissionTypeDTO, req.user?.id);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'PermissionType', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update permissionType' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: PermissionTypeDTO,
    })
    async put(@Req() req: Request, @Body() permissionTypeDTO: PermissionTypeDTO): Promise<PermissionTypeDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'PermissionType', permissionTypeDTO.id);
        return await this.permissionTypeService.update(permissionTypeDTO, req.user?.id);
    }

    @Put('/:id')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Update permissionType with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: PermissionTypeDTO,
    })
    async putId(@Req() req: Request, @Body() permissionTypeDTO: PermissionTypeDTO): Promise<PermissionTypeDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'PermissionType', permissionTypeDTO.id);
        return await this.permissionTypeService.update(permissionTypeDTO, req.user?.id);
    }

    @Delete('/:id')
    @Roles(RoleType.OWNER)
    @ApiOperation({ title: 'Delete permissionType' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Param('id') id: number): Promise<PermissionTypeDTO> {
        return await this.permissionTypeService.deleteById(id);
    }
}
