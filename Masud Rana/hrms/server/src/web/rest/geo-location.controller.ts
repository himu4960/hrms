import {
    ClassSerializerInterceptor,
    Controller,
    ForbiddenException,
    Get,
    Logger,
    Param,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';

import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { ResponseDTO } from '../../service/dto/response.dto';
import { GeoLocationService } from '../../service/geo-location.service';
import { GEO_LOCATION } from '../../shared/constant/response-message';

@Controller('api/geo-location')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('GeoLocation')
export class GeoLocationController {
    logger = new Logger('GeoLocationController');

    constructor(private readonly geoLocation: GeoLocationService) {}

    @Get('/:ip')
    @Roles(RoleType.OWNER, RoleType.EMPLOYEE)
    @ApiResponse({
        status: 200,
        description: 'The found record',
    })
    async getGeoLocation(@Param('ip') ip: string): Promise<ResponseDTO> {
        try {
            const address = await this.geoLocation.getGeoLocation(ip);
            if (address) {
                const responseData = {
                    data: address,
                    message: GEO_LOCATION.SUCCESS,
                    translateCode: 'GEO_LOCATION.SUCCESS',
                };
                return responseData;
            } else {
                const responseData = {
                    data: null,
                    message: GEO_LOCATION.ERROR,
                    translateCode: 'GEO_LOCATION.ERROR',
                };
                return responseData;
            }
        } catch (error) {
            const errorResponse = {
                message: error.message,
                translateCode: 'GEO_LOCATION.ERROR',
            };
            throw new ForbiddenException(errorResponse);
        }
    }
}
