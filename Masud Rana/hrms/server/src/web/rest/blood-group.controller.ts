import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    Req,
    UseInterceptors,
    HttpStatus,
} from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { BloodGroupAllDTO } from './../../service/dto/blood-group/blood-group-all.dto';
import { BloodGroupGetByLangCodeDTO } from './../../service/dto/blood-group/blood-group-get-by-langcode.dto';
import { BloodGroupUpdateDTO } from '../../service/dto/blood-group/blood-group-update.dto';
import { BloodGroupCreateDTO } from '../../service/dto/blood-group/blood-group-create.dto';
import { BloodGroupDTO } from './../../service/dto/blood-group/blood-group.dto';

import { HeaderUtil } from './../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { BloodGroupService } from '../../service/blood-group.service';

@Controller('api/blood-groups')
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiUseTags('Blood Groups')
export class BloodGroupController {
    logger = new Logger('BloodGroupController');

    constructor(private readonly bloodGroupService: BloodGroupService) {}

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: BloodGroupAllDTO,
    })
    async getAll() {
        return await this.bloodGroupService.findAll();
    }

    @Get('/:langCode')
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: BloodGroupGetByLangCodeDTO,
    })
    async getOne(@Param('langCode') langCode: string) {
        return await this.bloodGroupService.findByLangCode(langCode);
    }

    @PostMethod('/')
    @ApiOperation({ title: 'Create blood group' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: [BloodGroupDTO],
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() addBloodGroup: BloodGroupCreateDTO) {
        return await this.bloodGroupService.save(addBloodGroup, req.user?.id);
    }

    @Put('/:id')
    @ApiOperation({ title: 'Update blood group with id' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully updated.',
        type: BloodGroupUpdateDTO,
    })
    async putId(@Req() req: Request, @Param('id') id: number, @Body() updateBloodGroup: BloodGroupUpdateDTO) {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'BloodGroup', id);
        return await this.bloodGroupService.update(id, updateBloodGroup, req.user?.id);
    }

    @Delete('/:id')
    @ApiOperation({ title: 'Delete blood group' })
    @ApiResponse({
        status: HttpStatus.NO_CONTENT,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Param('id') id: number) {
        return await this.bloodGroupService.deleteById(id);
    }
}
