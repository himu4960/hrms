import {
    Body,
    ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
    Query,
    ForbiddenException,
    HttpException,
    HttpStatus,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { TimeClockDTO } from '../../service/dto/time-clock/time-clock.dto';
import { TimeClockUpdateDTO } from './../../service/dto/time-clock/time-clock-update.dto';
import { TimeClockFilterForEmployeeDTO } from '../../service/dto/time-clock/time-clock-filter-for-employee.dto';
import { TimeSheetDTO } from './../../service/dto/time-clock/time-sheet.dto';
import { TimeClockFilterDTO } from './../../service/dto/time-clock/time-clock-filter.dto';
import { ResponseDTO } from './../../service/dto/response.dto';
import { TimeSheetManageFilterParamsDTO } from './../../service/dto/time-clock/time-sheet-manage-filter-params.dto';
import { TimeSheetImportDTO } from '../../service/dto/time-clock/time-sheet-import.dto';
import { TimeClockCreateDTO } from './../../service/dto/time-clock/time-clock-create.dto';

import { JWTService } from './../../service/jwt.service';
import { TimeClockService } from '../../service/time-clock.service';
import { UtilService } from './../../shared/util.service';

import { PageRequest, Page } from '../../domain/base/pagination.entity';

import { AuthGuard, RolesGuard } from '../../security';

import { HeaderUtil } from '../../client/header-util';
import { Request } from '../../client/request';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';
import { TIME_CLOCK_RESPONSE } from '../../shared/constant/response-message';

@Controller('api/time-clocks')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiBearerAuth()
@ApiUseTags('Time Clocks')
export class TimeClockController {
    logger = new Logger('TimeClockController');

    constructor(
        private readonly timeClockService: TimeClockService,
        private readonly jwtService: JWTService,
        private readonly utilService: UtilService,
    ) {}

    @Get('/all')
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: TimeClockDTO,
    })
    async getAll(@Req() req: Request): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
            const [results, count] = await this.timeClockService.findAndCount(
                {
                    skip: +pageRequest.page * pageRequest.size,
                    take: +pageRequest.size,
                    order: pageRequest.sort.asOrder(),
                },
                decoded.company_id,
            );
            HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
            return this.utilService.getSuccessResponse(
                results,
                TIME_CLOCK_RESPONSE.READ.SUCCESS,
                'TIMECLOCK.RETRIEVED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(TIME_CLOCK_RESPONSE.READ.ERROR, 'TIMECLOCK.NOT_RETRIEVED'),
                error?.status ?? HttpStatus.NOT_FOUND,
            );
        }
    }

    @Get('/')
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: TimeClockDTO,
    })
    async getOneForEmployee(
        @Req() req: Request,
        @Query() timeClockGetByEmployeeDTO: TimeClockFilterForEmployeeDTO,
    ): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const timeClock = await this.timeClockService.findForEmployee(
                decoded?.company_id,
                timeClockGetByEmployeeDTO,
            );
            if (timeClock) {
                return this.utilService.getSuccessResponse(
                    timeClock,
                    TIME_CLOCK_RESPONSE.READ.SUCCESS,
                    'TIMECLOCK.RETRIEVED',
                );
            } else {
                return this.utilService.getSuccessResponse({}, TIME_CLOCK_RESPONSE.READ.SUCCESS, 'TIMECLOCK.RETRIEVED');
            }
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(TIME_CLOCK_RESPONSE.READ.ERROR, 'TIMECLOCK.NOT_RETRIEVED'),
            );
        }
    }

    @Get('/:id')
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: TimeClockDTO,
    })
    async getOne(@Param('id') id: number): Promise<ResponseDTO> {
        try {
            const timeClock = await this.timeClockService.findById(id);
            return this.utilService.getSuccessResponse(
                timeClock,
                TIME_CLOCK_RESPONSE.READ.SUCCESS,
                'TIMECLOCK.RETRIEVED',
            );
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(TIME_CLOCK_RESPONSE.READ.ERROR, 'TIMECLOCK.NOT_RETRIEVED'),
            );
        }
    }

    @PostMethod('/time-sheet')
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: TimeClockDTO,
    })
    async getAllTimeSheetForEmployee(
        @Req() req: Request,
        @Body() timeClockGetByEmployeeDTO: TimeClockFilterForEmployeeDTO,
    ): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
            const [results, count] = await this.timeClockService.timeSheetFindAndCount(
                {
                    skip: +pageRequest.page * pageRequest.size,
                    take: +pageRequest.size,
                    order: {
                        clock_start_time: 'DESC',
                    },
                },
                decoded.company_id,
                timeClockGetByEmployeeDTO,
            );
            return this.utilService.getSuccessResponse(
                { ...results, count },
                TIME_CLOCK_RESPONSE.TIME_SHEET.READ.SUCCESS,
                'TIMESHEET.RETRIEVED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(TIME_CLOCK_RESPONSE.TIME_SHEET.READ.ERROR, 'TIMESHEET.NOT_RETRIEVED'),
                error?.status ?? HttpStatus.NOT_FOUND,
            );
        }
    }

    @PostMethod('/time-sheet/filter')
    @ApiOperation({ title: 'Filter timeClock' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully filtered.',
        type: TimeSheetDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async filterTimeClock(@Req() req: Request, @Body() filterDTO: TimeClockFilterDTO): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
            const [result, count] = await this.timeClockService.filter(
                {
                    skip: +pageRequest.page * pageRequest.size,
                    take: +pageRequest.size,
                    order: {
                        clock_start_time: 'DESC',
                    },
                },
                filterDTO,
                decoded.company_id,
            );
            return this.utilService.getSuccessResponse(
                { ...result, count },
                TIME_CLOCK_RESPONSE.READ.SUCCESS,
                'TIMECLOCK.FILTERED',
            );
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(TIME_CLOCK_RESPONSE.READ.ERROR, 'TIMECLOCK.NOT_RETRIEVED'),
            );
        }
    }

    @PostMethod('/time-sheet/manage')
    @ApiOperation({ title: 'Manage time sheet' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully filtered.',
        type: TimeSheetDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async manageTimeSheet(
        @Req() req: Request,
        @Body() timeSheetManageFilterParams: TimeSheetManageFilterParamsDTO,
    ): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const timeSheetManage = await this.timeClockService.timeSheetManage(
                timeSheetManageFilterParams,
                decoded.company_id,
            );
            return this.utilService.getSuccessResponse(
                timeSheetManage,
                TIME_CLOCK_RESPONSE.READ.SUCCESS,
                'TIMECLOCK.FILTERED',
            );
        } catch (error) {
            throw new ForbiddenException(
                this.utilService.getErrorResponse(TIME_CLOCK_RESPONSE.READ.ERROR, 'TIMECLOCK.NOT_RETRIEVED'),
            );
        }
    }

    @PostMethod('/time-sheet/import')
    @ApiOperation({ title: 'Import time sheet' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully filtered.',
        type: TimeSheetImportDTO,
        isArray: true,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async importTimeSheet(@Req() req: Request, @Body() timeSheetImportDTO: TimeSheetImportDTO[]): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const timeSheets = await this.timeClockService.importTimeSheet(timeSheetImportDTO, decoded.company_id);
            if (timeSheets) {
                return this.utilService.getSuccessResponse(
                    timeSheets,
                    'Time Sheet Successfully Uploaded',
                    'TIME_CLOCK.UPLOADED',
                );
            }
            throw new ForbiddenException(
                this.utilService.getErrorResponse('Something went wrong!', 'TIME_CLOCK.NOT_UPLOADED'),
            );
        } catch (error) {
            throw new ForbiddenException(this.utilService.getErrorResponse(error.message, 'TIME_CLOCK.NOT_UPLOADED'));
        }
    }

    @PostMethod('/')
    @ApiOperation({ title: 'Create timeClock' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: TimeClockDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() timeClockDTO: TimeClockCreateDTO): Promise<ResponseDTO> {
        try {
            let clientIp = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
            const requestIp = this.utilService.getIp(clientIp);

            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            const newTimeClock = await this.timeClockService.save(
                { ...timeClockDTO, request_ip: requestIp },
                decoded.company_id,
                req.user?.id,
            );
            HeaderUtil.addEntityCreatedHeaders(req.res, 'TimeClock', newTimeClock.id);
            return this.utilService.getSuccessResponse(
                newTimeClock,
                TIME_CLOCK_RESPONSE.CREATE.SUCCESS,
                'TIMECLOCK.CREATED',
            );
        } catch (error) {
            throw new HttpException(
                this.utilService.getErrorResponse(error.message, 'TIMECLOCK.NOT_CREATED'),
                error?.status ?? HttpStatus.BAD_REQUEST,
            );
        }
    }

    @Put('/:id/out')
    @ApiOperation({ title: 'Update timeClock with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: TimeClockDTO,
    })
    async putId(
        @Req() req: Request,
        @Param('id') id: number,
        @Body() timeClockDTO: TimeClockUpdateDTO,
    ): Promise<ResponseDTO> {
        try {
            HeaderUtil.addEntityCreatedHeaders(req.res, 'TimeClock', id);
            const clockOutResult = await this.timeClockService.clockOut({ ...timeClockDTO, id }, req.user?.id);
            return this.utilService.getSuccessResponse(
                clockOutResult,
                TIME_CLOCK_RESPONSE.CLOCK_OUT.SUCCESS,
                'TIMECLOCK.DELETED',
            );
        } catch (error) {
            throw new ForbiddenException(this.utilService.getErrorResponse(error.message, 'TIMECLOCK.NOT_DELETED'));
        }
    }

    @Put('/:id')
    @ApiOperation({ title: 'Update timeClock with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: TimeClockDTO,
    })
    async timeClockUpdate(
        @Req() req: Request,
        @Param('id') id: number,
        @Body() timeClockDTO: TimeClockUpdateDTO,
    ): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            HeaderUtil.addEntityCreatedHeaders(req.res, 'TimeClock', id);
            const updatedTimeClock = await this.timeClockService.timeClockUpdate(
                { ...timeClockDTO, id: Number(id) },
                decoded.company_id,
                req.user?.id,
            );
            return this.utilService.getSuccessResponse(
                updatedTimeClock,
                TIME_CLOCK_RESPONSE.UPDATE.SUCCESS,
                'TIMECLOCK.UPDATED',
            );
        } catch (error) {
            throw new ForbiddenException(this.utilService.getErrorResponse(error.message, 'TIMECLOCK.NOT_UPDATED'));
        }
    }

    @Put('/edit/:id')
    @ApiOperation({ title: 'Time Clock Edit Mode with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: TimeClockDTO,
    })
    async timeClockEditMode(
        @Req() req: Request,
        @Param('id') id: number,
        @Body() timeClockDTO: TimeClockUpdateDTO,
    ): Promise<ResponseDTO> {
        try {
            const decoded: any = await this.jwtService.decodedJWTToken(req.headers.authorization);
            HeaderUtil.addEntityCreatedHeaders(req.res, 'TimeClock', id);
            const updatedTimeClock = await this.timeClockService.timeClockEditMode(
                { ...timeClockDTO, id: Number(id) },
                decoded.company_id,
                req.user?.id,
            );
            if (updatedTimeClock) {
                return this.utilService.getSuccessResponse(
                    updatedTimeClock,
                    TIME_CLOCK_RESPONSE.UPDATE.SUCCESS,
                    'TIMECLOCK.UPDATED',
                );
            } else {
                return this.utilService.getSuccessResponse({}, TIME_CLOCK_RESPONSE.UPDATE.SUCCESS, 'TIMECLOCK.UPDATED');
            }
        } catch (error) {
            throw new ForbiddenException(this.utilService.getErrorResponse(error.message, 'TIMECLOCK.NOT_UPDATED'));
        }
    }

    @Delete('/:id')
    @ApiOperation({ title: 'Delete timeClock' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Param('id') id: number): Promise<any> {
        try {
            const deletedTimeClock = await this.timeClockService.deleteById(id);
            if (deletedTimeClock) {
                return this.utilService.getSuccessResponse(
                    deletedTimeClock,
                    TIME_CLOCK_RESPONSE.DELETE.SUCCESS,
                    'TIMECLOCK.DELETED',
                );
            } else {
                return this.utilService.getSuccessResponse({}, TIME_CLOCK_RESPONSE.DELETE.SUCCESS, 'TIMECLOCK.DELETED');
            }
        } catch (error) {
            throw new ForbiddenException(this.utilService.getErrorResponse(error.message, 'TIMECLOCK.NOT_DELETED'));
        }
    }
}
