/* eslint-disable @typescript-eslint/no-unused-vars */
import {
    Controller,
    Get,
    Logger,
    UseInterceptors,
    ClassSerializerInterceptor,
} from '@nestjs/common';
import { ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';

import { AUTH_SETUP_RESPONSE } from './../../shared/constant/response-message';

import { AuthSetupDTO } from './../../service/dto/auth/auth-setup.dto';
import { ResponseDTO } from './../../service/dto/response.dto';

import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

import { UtilService } from './../../shared/util.service';
import { CompanyTypeService } from './../../service/company-type.service';
import { ApplicationTypeService } from './../../service/application-type.service';

@Controller('api/auth')
@UseInterceptors(LoggingInterceptor, ClassSerializerInterceptor)
@ApiUseTags('Auth Setup Data')
export class AuthController {
    logger = new Logger('AuthController');

    constructor(private readonly applicationTypeService: ApplicationTypeService, private readonly companyTypeService: CompanyTypeService, private readonly utilService: UtilService) { }

    @Get('/setup')
    @ApiOperation({ title: 'Get the all data.' })
    @ApiResponse({
        status: 200,
        description: 'Auth retrieved',
    })
    async getAuthData(): Promise<ResponseDTO> {
        try {
            const application_types = await this.applicationTypeService.getActiveApplicationTypes();
            const company_types = await this.companyTypeService.getActiveCompanyTypes();
            const data: AuthSetupDTO = {
                application_types,
                company_types
            }
            return this.utilService.getSuccessResponse(data, AUTH_SETUP_RESPONSE.SUCCESS, 'AUTH.RETRIVED')
        } catch (error) {
            return this.utilService.getErrorResponse(AUTH_SETUP_RESPONSE.ERROR, 'AUTH.NOT_RETRIVED')
        }
    }
}
