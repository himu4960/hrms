import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { PREFIX } from './shared/constant/database.constant';

const commonConf = {
    SYNCRONIZE: false,
    ENTITIES: [__dirname + '/domain/*.entity{.ts,.js}'],
    MIGRATIONS: [__dirname + '/migrations/**/*{.ts,.js}'],
    CLI: {
        migrationsDir: 'src/migrations',
    },
    MIGRATIONS_RUN: false,
};

// Uncomment following block if you want to develop in local environment

// export const typeOrmConfig: TypeOrmModuleOptions = {
//     name: 'default',
//     type: 'mysql',
//     database: 'hrms',
//     host: 'localhost',
//     port: 3306,
//     username: 'root',
//     password: 'admin',
//     entityPrefix: PREFIX,
//     entities: commonConf.ENTITIES,
//     migrations: commonConf.MIGRATIONS,
//     cli: commonConf.CLI,
//     extra: {
//         charset: 'utf8mb4_unicode_ci',
//     },
//     synchronize: false,
//     migrationsRun: false,
//     logging: true,
//     bigNumberStrings: false,
// };

// Uncomment following block if you want to develop in docker environment

export const typeOrmConfig: TypeOrmModuleOptions = {
    name: 'default',
    type: 'mysql',
    database: process.env.DATABASE_NAME,
    host: process.env.DATABASE_HOST,
    port: parseInt(process.env.DATABASE_PORT),
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    entityPrefix: PREFIX,
    entities: commonConf.ENTITIES,
    migrations: commonConf.MIGRATIONS,
    cli: commonConf.CLI,
    extra: {
        charset: 'utf8mb4_unicode_ci',
    },
    synchronize: false,
    logging: true,
    bigNumberStrings: false,
};

function ormConfig(): TypeOrmModuleOptions {
    let ormconfig: TypeOrmModuleOptions = {
        name: 'default',
        type: 'mysql',
        database: 'hrms',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: 'admin',
        logging: false,
        entityPrefix: PREFIX,
        synchronize: false,
        keepConnectionAlive: false,
        entities: commonConf.ENTITIES,
        migrations: commonConf.MIGRATIONS,
        cli: commonConf.CLI,
        migrationsRun: commonConf.MIGRATIONS_RUN,
        bigNumberStrings: false,
    };

    if (process.env.BACKEND_ENV === 'dev') {
        ormconfig = {
            name: 'default',
            type: 'mysql',
            database: process.env.DATABASE_NAME,
            host: process.env.DATABASE_HOST,
            port: parseInt(process.env.DATABASE_PORT),
            username: process.env.DATABASE_USERNAME,
            password: process.env.DATABASE_PASSWORD,
            logging: false,
            entityPrefix: PREFIX,
            keepConnectionAlive: false,
            synchronize: commonConf.SYNCRONIZE,
            entities: commonConf.ENTITIES,
            migrations: commonConf.MIGRATIONS,
            cli: commonConf.CLI,
            migrationsRun: commonConf.MIGRATIONS_RUN,
            bigNumberStrings: false,
        };
    }

    if (process.env.BACKEND_ENV === 'prod') {
        ormconfig = {
            type: 'mysql',
            host: process.env.DATABASE_HOST,
            port: parseInt(process.env.DATABASE_PORT),
            username: process.env.DATABASE_USERNAME,
            password: process.env.DATABASE_PASSWORD,
            database: process.env.DATABASE_NAME,
            synchronize: false,
            entityPrefix: PREFIX,
            entities: commonConf.ENTITIES,
            migrations: commonConf.MIGRATIONS,
            cli: commonConf.CLI,
            extra: {
                charset: 'utf8mb4_unicode_ci',
            },
            migrationsRun: commonConf.MIGRATIONS_RUN,
            bigNumberStrings: false,
        };
    }

    if (process.env.BACKEND_ENV === 'staging') {
        ormconfig = {
            name: 'default',
            type: 'mysql',
            database: process.env.DATABASE_NAME,
            host: process.env.DATABASE_HOST,
            port: parseInt(process.env.DATABASE_PORT),
            username: process.env.DATABASE_USERNAME,
            password: process.env.DATABASE_PASSWORD,
            entityPrefix: PREFIX,
            keepConnectionAlive: false,
            logging: true,
            synchronize: false,
            entities: commonConf.ENTITIES,
            migrations: commonConf.MIGRATIONS,
            cli: commonConf.CLI,
            migrationsRun: commonConf.MIGRATIONS_RUN,
            bigNumberStrings: false,
        };
    }
    return ormconfig;
}

export { ormConfig };
