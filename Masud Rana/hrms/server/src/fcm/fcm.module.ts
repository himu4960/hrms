import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { JWTModule } from '../module/jwt.module';
import { MediaModule } from '../module/media.module';

import { NotificationService } from './notification.service';

import { UserDeviceRepository } from '../repository/user-device.repository';

@Module({
    imports: [TypeOrmModule.forFeature([UserDeviceRepository]), JWTModule, MediaModule],
    controllers: [],
    providers: [NotificationService],
    exports: [NotificationService],
})
export class FcmModule {}
