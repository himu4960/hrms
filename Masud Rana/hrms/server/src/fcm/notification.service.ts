import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { FindManyOptions } from 'typeorm';

import { FirebaseAdminService } from '../firebase-admin';

import { UserDeviceRepository } from '../repository/user-device.repository';

import { UserDeviceDTO } from '../service/dto/user-device.dto';

@Injectable()
export class NotificationService {
    constructor(
        @InjectRepository(UserDeviceRepository) private userDeviceRepository: UserDeviceRepository,
        private readonly firebaseAdminService: FirebaseAdminService,
    ) {}

    async getDeviceTokens(options: FindManyOptions<UserDeviceDTO>): Promise<string[]> {
        const userDevices = await this.userDeviceRepository.find(options);
        return userDevices.map(device => {
            return device.device_token;
        });
    }

    async getDeviceTokensByRole(role: string, company_id: number): Promise<string[]> {
        const userDevices = await this.userDeviceRepository
            .createQueryBuilder('userDevice')
            .leftJoinAndSelect('userDevice.role', 'role')
            .where('userDevice.company_id = :company_id', { company_id })
            .andWhere('role.name = :role', { role })
            .select(['userDevice.device_token'])
            .getMany();

        return userDevices.map(device => {
            return device.device_token;
        });
    }

    async sendNotification(deviceIds: string[], payload: any): Promise<any> {
        try {
            await this.firebaseAdminService.sendToDevice(deviceIds, payload, false);
            return { success: true, message: 'Notification sent successfully' };
        } catch (error) {
            return { success: false, message: error?.message };
        }
    }
}
