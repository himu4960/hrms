import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { EmployeeTypeController } from '../web/rest/employee-type.controller';

import { EmployeeTypeService } from '../service/employee-type.service';

import { EmployeeTypeRepository } from '../repository/employee-type.repository';

@Module({
    imports: [TypeOrmModule.forFeature([EmployeeTypeRepository])],
    controllers: [EmployeeTypeController],
    providers: [EmployeeTypeService],
    exports: [EmployeeTypeService],
})
export class EmployeeTypeModule {}
