import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { JWTModule } from './jwt.module';
import { SharedModule } from './../shared/shared.module';
import { FcmModule } from '../fcm/fcm.module';

import { AnnouncementController } from '../web/rest/announcement.controller';

import { AnnouncementRepository } from '../repository/announcement.repository';

import { AnnouncementService } from '../service/announcement.service';

@Module({
    imports: [TypeOrmModule.forFeature([AnnouncementRepository]), SharedModule, JWTModule, FcmModule],
    controllers: [AnnouncementController],
    providers: [AnnouncementService],
    exports: [AnnouncementService],
})
export class AnnouncementModule {}
