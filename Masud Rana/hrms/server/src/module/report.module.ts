import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { TimeClockRepository } from '../repository/time-clock.repository';
import { WorkShiftRepository } from '../repository/work-shift.repository';
import { SettingsTimeClockRepository } from './../repository/settings-time-clock.repository';

import { JWTModule } from './jwt.module';
import { SharedModule } from './../shared/shared.module';
import { TimeZoneModule } from './time-zone.module';

import { TimeSheetReportController } from '../web/rest/reports/time-clock/time-sheet.report.controller';
import { ScheduleReportController } from '../web/rest/reports/schedule/schedule.report.controller';

import { TimeSheetReportService } from '../service/reports/time-sheet.report.service';
import { ScheduleReportService } from '../service/reports/schedule.report.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([TimeClockRepository, WorkShiftRepository, SettingsTimeClockRepository]),
        JWTModule,
        SharedModule,
        TimeZoneModule,
    ],
    controllers: [TimeSheetReportController, ScheduleReportController],
    providers: [TimeSheetReportService, ScheduleReportService],
    exports: [TimeSheetReportService, ScheduleReportService],
})
export class ReportModule {}
