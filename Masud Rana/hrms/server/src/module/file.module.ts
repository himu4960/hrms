import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { MediaModule } from './media.module';
import { JWTModule } from './jwt.module';
import { FcmModule } from '../fcm/fcm.module';

import { FileController } from '../web/rest/file.controller';

import { FileRepository } from '../repository/file.repository';

import { FileService } from '../service/file.service';

@Module({
    imports: [TypeOrmModule.forFeature([FileRepository]), MediaModule, JWTModule, FcmModule],
    controllers: [FileController],
    providers: [FileService],
    exports: [FileService],
})
export class FileModule {}
