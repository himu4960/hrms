import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UserController } from '../web/rest/user.controller';
import { ManagementController } from '../web/rest/management.controller';

import { UserService } from '../service/user.service';

import { UserRepository } from '../repository/user.repository';
import { CompanyRepository } from './../repository/company.repository';
import { UserCompanyRepository } from './../repository/user-company.repository';

@Module({
    imports: [TypeOrmModule.forFeature([UserRepository, UserCompanyRepository, CompanyRepository])],
    controllers: [UserController, ManagementController],
    providers: [UserService],
    exports: [UserService],
})
export class UserModule { }
