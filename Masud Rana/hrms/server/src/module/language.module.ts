import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { LanguageController } from '../web/rest/language.controller';
import { LanguageService } from '../service/language.service';
import { LanguageRepository } from '../repository/language.repository';

@Module({
    imports: [TypeOrmModule.forFeature([LanguageRepository])],
    controllers: [LanguageController],
    providers: [LanguageService],
    exports: [LanguageService],
})
export class LanguageModule {}
