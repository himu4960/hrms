import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BullModule } from '@nestjs/bull';

import { JWTModule } from './jwt.module';
import { EmployeeModule } from './employee.module';
import { FcmModule } from '../fcm/fcm.module';

import { WorkShiftProcessor } from './../processor/work-shift.processor';

import { WorkShiftController } from '../web/rest/work-shift.controller';

import { LeaveRepository } from './../repository/leave.repository';
import { EmployeeRepository } from './../repository/employee.repository';
import { WorkShiftRepository } from '../repository/work-shift.repository';

import { WorkShiftService } from '../service/work-shift.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([WorkShiftRepository, EmployeeRepository, LeaveRepository]),
        FcmModule,
        JWTModule,
        EmployeeModule,
        BullModule.registerQueue({
            name: 'work-shift',
        }),
    ],
    controllers: [WorkShiftController],
    providers: [WorkShiftService, WorkShiftProcessor],
    exports: [WorkShiftService],
})
export class WorkShiftModule {}
