import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ReligionController } from '../web/rest/religion.controller';

import { ReligionService } from '../service/religion.service';

import { LanguageRepository } from './../repository/language.repository';
import { ReligionRepository } from '../repository/religion.repository';
import { ReligionTranslationRepository } from './../repository/religion-translation.repository';

@Module({
    imports: [TypeOrmModule.forFeature([ReligionRepository, ReligionTranslationRepository, LanguageRepository])],
    controllers: [ReligionController],
    providers: [ReligionService],
    exports: [ReligionService],
})
export class ReligionModule {}
