import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule } from '@nestjs/axios';

import { GeoLocationRepository } from '../repository/geo-location.repository';
import { GeoLocationController } from '../web/rest/geo-location.controller';
import { GeoLocationService } from '../service/geo-location.service';

@Module({
    imports: [TypeOrmModule.forFeature([GeoLocationRepository]),HttpModule],
    controllers: [GeoLocationController],
    providers: [GeoLocationService],
    exports: [GeoLocationService],
})
export class GeoLocationModule {}
