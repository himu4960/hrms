import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { MaritalStatusController } from '../web/rest/marital-status.controller';

import { MaritalStatusService } from '../service/marital-status.service';

import { LanguageRepository } from './../repository/language.repository';
import { MaritalStatusRepository } from '../repository/marital-status.repository';
import { MaritalStatusTranslationRepository } from './../repository/marital-status-translation.repository';

@Module({
    imports: [
        TypeOrmModule.forFeature([MaritalStatusRepository, LanguageRepository, MaritalStatusTranslationRepository]),
    ],
    controllers: [MaritalStatusController],
    providers: [MaritalStatusService],
    exports: [MaritalStatusService],
})
export class MaritalStatusModule {}
