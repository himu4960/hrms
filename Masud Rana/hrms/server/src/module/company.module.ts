import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CompanyController } from '../web/rest/company.controller';

import { MediaModule } from './media.module';
import { JWTModule } from './jwt.module';

import { CompanyService } from '../service/company.service';
import { CreateCompanyTransactionService } from '../service/transaction/create-company-transaction.service';

import { CompanyRepository } from '../repository/company.repository';
import { UserCompanyRepository } from '../repository/user-company.repository';
import { UserRepository } from '../repository/user.repository';

@Module({
    imports: [TypeOrmModule.forFeature([CompanyRepository, UserCompanyRepository, UserRepository]), MediaModule, JWTModule,],
    controllers: [CompanyController],
    providers: [CompanyService, CreateCompanyTransactionService],
    exports: [CompanyService],
})
export class CompanyModule { }
