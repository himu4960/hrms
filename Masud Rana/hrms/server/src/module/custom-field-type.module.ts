import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CustomFieldTypeController } from '../web/rest/custom-field-type.controller';

import { CustomFieldTypeRepository } from '../repository/custom-field-type.repository';

import { CustomFieldTypeService } from '../service/custom-field-type.service';

@Module({
    imports: [TypeOrmModule.forFeature([CustomFieldTypeRepository])],
    controllers: [CustomFieldTypeController],
    providers: [CustomFieldTypeService],
    exports: [CustomFieldTypeService],
})
export class CustomFieldTypeModule {}
