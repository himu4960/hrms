import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { config } from '../config';

import { JWTService } from '../service/jwt.service';

@Module({
    imports: [
        PassportModule,
        JwtModule.register({
            secret: config['hrms.security.authentication.jwt.base64-secret'],
            signOptions: { expiresIn: '24h' },
        }),
    ],
    providers: [JWTService],
    exports: [JWTService],
})
export class JWTModule {}
