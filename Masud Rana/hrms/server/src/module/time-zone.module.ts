import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { TimeZoneController } from '../web/rest/time-zone.controller';

import { TimeZoneRepository } from '../repository/time-zone.repository';

import { TimeZoneService } from '../service/time-zone.service';

@Module({
    imports: [TypeOrmModule.forFeature([TimeZoneRepository])],
    controllers: [TimeZoneController],
    providers: [TimeZoneService],
    exports: [TimeZoneService],
})
export class TimeZoneModule {}
