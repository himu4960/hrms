import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { BranchController } from '../web/rest/branch.controller';

import { BranchService } from '../service/branch.service';

import { BranchRepository } from '../repository/branch.repository';

import { JWTModule } from './jwt.module';

@Module({
    imports: [TypeOrmModule.forFeature([BranchRepository]), JWTModule],
    controllers: [BranchController],
    providers: [BranchService],
    exports: [BranchService],
})
export class BranchModule {}
