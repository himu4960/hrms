import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CustomFieldController } from '../web/rest/custom-field.controller';

import { CustomFieldRepository } from '../repository/custom-field.repository';

import { CustomFieldService } from '../service/custom-field.service';

import { JWTModule } from './jwt.module';

@Module({
    imports: [TypeOrmModule.forFeature([CustomFieldRepository]), JWTModule],
    controllers: [CustomFieldController],
    providers: [CustomFieldService],
    exports: [CustomFieldService],
})
export class CustomFieldModule {}
