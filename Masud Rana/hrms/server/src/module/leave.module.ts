import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { JWTModule } from './jwt.module';
import { FcmModule } from '../fcm/fcm.module';
import { EmployeeModule } from './employee.module';

import { LeaveTypeController } from '../web/rest/leave-type.controller';
import { LeaveController } from '../web/rest/leave.controller';

import { LeaveRepository } from '../repository/leave.repository';
import { LeaveTypeRepository } from '../repository/leave-type.repository';
import { LeaveBalanceRepository } from '../repository/leave-balance.repository';

import { LeaveService } from '../service/leave.service';
import { LeaveTypeService } from '../service/leave-type.service';
import { LeaveBalanceService } from '../service/leave-balance.service';
import { DateTimeConvertService } from '../shared/convert/date-time-convert.service';

import { EmployeeRepository } from '../repository/employee.repository';
import { SettingsApplicationRepository } from '../repository/settings-application.repository';
import { HolidayRepository } from '../repository/holiday.repository';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            LeaveRepository,
            LeaveTypeRepository,
            LeaveBalanceRepository,
            HolidayRepository,
            EmployeeRepository,
            SettingsApplicationRepository,
        ]),
        JWTModule,
        FcmModule,
        EmployeeModule,
    ],
    controllers: [LeaveController, LeaveTypeController],
    providers: [LeaveService, LeaveTypeService, LeaveBalanceService, DateTimeConvertService],
    exports: [LeaveService, LeaveTypeService],
})
export class LeaveModule {}
