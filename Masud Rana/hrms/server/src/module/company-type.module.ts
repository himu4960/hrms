import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CompanyTypeController } from '../web/rest/company-type.controller';

import { CompanyTypeService } from '../service/company-type.service';

import { CompanyTypeRepository } from '../repository/company-type.repository';

@Module({
    imports: [TypeOrmModule.forFeature([CompanyTypeRepository])],
    controllers: [CompanyTypeController],
    providers: [CompanyTypeService],
    exports: [CompanyTypeService],
})
export class CompanyTypeModule {}
