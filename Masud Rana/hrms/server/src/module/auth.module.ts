import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { config } from '../config';

import { JWTModule } from './jwt.module';
import { RoleModule } from './role.module';
import { UserModule } from '../module/user.module';
import { MailModule } from './../shared/mail/mail.module';
import { CompanyTypeModule } from './company-type.module';
import { ApplicationTypeModule } from './application-type.module';
import { FcmModule } from '../fcm/fcm.module';

import { PublicUserController } from '../web/rest/public.user.controller';
import { AccountController } from '../web/rest/account.controller';
import { UserJWTController } from '../web/rest/user.jwt.controller';

import { AuthService } from '../service/auth.service';
import { RegisterCompanyTransactionService } from '../service/transaction/register-company-transaction.service';

import { JwtStrategy } from './../security/passport.jwt.strategy';

import { CompanyRepository } from './../repository/company.repository';
import { EmployeeRepository } from './../repository/employee.repository';
import { UserCompanyRepository } from './../repository/user-company.repository';
import { UserDeviceRepository } from '../repository/user-device.repository';

@Global()
@Module({
    imports: [
        TypeOrmModule.forFeature([UserCompanyRepository, CompanyRepository, EmployeeRepository, UserDeviceRepository]),
        UserModule,
        RoleModule,
        CompanyTypeModule,
        ApplicationTypeModule,
        PassportModule,
        JWTModule,
        MailModule,
        FcmModule,
        JwtModule.register({
            secret: config['hrms.security.authentication.jwt.base64-secret'],
        }),
    ],
    controllers: [UserJWTController, PublicUserController, AccountController],
    providers: [AuthService, JwtStrategy, RegisterCompanyTransactionService],
    exports: [AuthService, JwtStrategy],
})
export class AuthModule {}
