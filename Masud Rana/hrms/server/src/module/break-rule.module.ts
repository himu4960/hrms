import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { JWTModule } from './jwt.module';

import { BreakRuleController } from '../web/rest/break-rule.controller';

import { BreakRuleRepository } from '../repository/break-rule.repository';
import { BreakRuleConditionRepository } from './../repository/break-rule-condition.repository';

import { BreakRuleService } from '../service/break-rule.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([BreakRuleRepository]),
        TypeOrmModule.forFeature([BreakRuleConditionRepository]),
        JWTModule,
    ],
    controllers: [BreakRuleController],
    providers: [BreakRuleService],
    exports: [BreakRuleService],
})
export class BreakRuleModule {}
