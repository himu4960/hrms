import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { JWTModule } from './jwt.module';

import { SkillController } from '../web/rest/skill.controller';

import { SkillRepository } from '../repository/skill.repository';

import { SkillService } from '../service/skill.service';

@Module({
    imports: [TypeOrmModule.forFeature([SkillRepository]),
        JWTModule
    ],
    controllers: [SkillController],
    providers: [SkillService],
    exports: [SkillService],
})
export class SkillModule { }
