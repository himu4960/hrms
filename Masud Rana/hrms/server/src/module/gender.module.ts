import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { GenderController } from '../web/rest/gender.controller';

import { GenderService } from '../service/gender.service';

import { LanguageRepository } from './../repository/language.repository';
import { GenderRepository } from '../repository/gender.repository';
import { GenderTranslationRepository } from './../repository/gender-translation.repository';

@Module({
    imports: [TypeOrmModule.forFeature([GenderRepository, GenderTranslationRepository, LanguageRepository])],
    controllers: [GenderController],
    providers: [GenderService],
    exports: [GenderService],
})
export class GenderModule {}
