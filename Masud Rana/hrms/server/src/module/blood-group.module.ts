import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { BloodGroupController } from '../web/rest/blood-group.controller';

import { BloodGroupService } from '../service/blood-group.service';

import { LanguageRepository } from './../repository/language.repository';
import { BloodGroupRepository } from '../repository/blood-group.repository';
import { BloodGroupTranslationRepository } from '../repository/blood-group-translation.repository';

@Module({
    imports: [TypeOrmModule.forFeature([BloodGroupRepository, BloodGroupTranslationRepository, LanguageRepository])],
    controllers: [BloodGroupController],
    providers: [BloodGroupService],
    exports: [BloodGroupService],
})
export class BloodGroupModule {}
