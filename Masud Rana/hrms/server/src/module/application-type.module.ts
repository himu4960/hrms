import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ApplicationTypeController } from '../web/rest/application-type.controller';

import { ApplicationTypeService } from '../service/application-type.service';

import { ApplicationTypeRepository } from '../repository/application-type.repository';

@Module({
    imports: [TypeOrmModule.forFeature([ApplicationTypeRepository])],
    controllers: [ApplicationTypeController],
    providers: [ApplicationTypeService],
    exports: [ApplicationTypeService],
})
export class ApplicationTypeModule {}
