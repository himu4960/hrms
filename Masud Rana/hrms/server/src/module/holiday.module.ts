import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HolidayController } from '../web/rest/holiday.controller';
import { HolidayRepository } from '../repository/holiday.repository';
import { HolidayService } from '../service/holiday.service';

@Module({
    imports: [TypeOrmModule.forFeature([HolidayRepository])],
    controllers: [HolidayController],
    providers: [HolidayService],
    exports: [HolidayService],
})
export class HolidayModule {}
