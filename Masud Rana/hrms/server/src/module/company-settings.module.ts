import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CompanySettingsController } from '../web/rest/company-settings.controller';

import { JWTModule } from './jwt.module';

import { SettingsTimeClockRepository } from './../repository/settings-time-clock.repository';
import { SettingsApplicationRepository } from './../repository/settings-application.repository';
import { SettingsShiftPlanningRepository } from './../repository/settings-shift-planning.repository';
import { SettingsLeaveAvailabilityRepository } from './../repository/settings-leave-availability.repository';

import { CompanySettingsService } from '../service/company-settings.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            SettingsTimeClockRepository,
            SettingsApplicationRepository,
            SettingsShiftPlanningRepository,
            SettingsLeaveAvailabilityRepository
        ]),
        JWTModule,
    ],
    controllers: [CompanySettingsController],
    providers: [CompanySettingsService],
    exports: [CompanySettingsService],
})
export class CompanySettingsModule {}
