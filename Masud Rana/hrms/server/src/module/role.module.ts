import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { RoleController } from '../web/rest/role.controller';

import { RoleService } from '../service/role.service';

import { RoleRepository } from '../repository/role.repository';

import { CasbinModule } from '../shared/nestjs-casbin';


@Module({
    imports: [TypeOrmModule.forFeature([RoleRepository]), CasbinModule.forRootAsync()],
    controllers: [RoleController],
    providers: [RoleService],
    exports: [RoleService],
})
export class RoleModule { }
