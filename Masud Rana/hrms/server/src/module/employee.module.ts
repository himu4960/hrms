import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { JWTModule } from './jwt.module';
import { CompanyModule } from './company.module';
import { RoleModule } from './role.module';
import { UserModule } from './user.module';
import { MediaModule } from './media.module';
import { DesignationModule } from './designation.module';
import { MailModule } from './../shared/mail/mail.module';
import { FcmModule } from '../fcm/fcm.module';

import { EmployeeController } from '../web/rest/employee.controller';

import { SkillRepository } from './../repository/skill.repository';
import { EmployeeRepository } from '../repository/employee.repository';
import { UserCompanyRepository } from './../repository/user-company.repository';
import { EmployeeDesignationRepository } from './../repository/employee-designation.repository';
import { BranchRepository } from './../repository/branch.repository';
import { TimeClockRepository } from './../repository/time-clock.repository';

import { EmployeeService } from '../service/employee.service';
import { EmployeeExportService } from '../service/employee-export.service';
import { RegisterEmployeeInvitationTransactionService } from './../service/transaction/register-emplyee-invitation-transaction.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            EmployeeRepository,
            UserCompanyRepository,
            EmployeeDesignationRepository,
            BranchRepository,
            SkillRepository,
            TimeClockRepository,
        ]),
        FcmModule,
        JWTModule,
        MailModule,
        RoleModule,
        CompanyModule,
        UserModule,
        MediaModule,
        DesignationModule,
    ],
    controllers: [EmployeeController],
    providers: [EmployeeService, EmployeeExportService, RegisterEmployeeInvitationTransactionService],
    exports: [EmployeeService],
})
export class EmployeeModule {}
