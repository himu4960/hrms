import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { JWTModule } from './jwt.module';

import { DesignationController } from '../web/rest/designation.controller';

import { DesignationRepository } from '../repository/designation.repository';
import { EmployeeDesignationRepository } from '../repository/employee-designation.repository';
import { SkillRepository } from './../repository/skill.repository';
import { BranchRepository } from './../repository/branch.repository';

import { DesignationService } from '../service/designation.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            DesignationRepository,
            EmployeeDesignationRepository,
            BranchRepository,
            SkillRepository,
        ]),
        JWTModule,
    ],
    controllers: [DesignationController],
    providers: [DesignationService],
    exports: [DesignationService],
})
export class DesignationModule {}
