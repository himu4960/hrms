import { HttpModule, Module } from '@nestjs/common';
import { BullModule } from '@nestjs/bull';
import { TypeOrmModule } from '@nestjs/typeorm';

import { SocketGateway } from '../gateway/socket.gateway';

import { TimeClockProcessor } from '../processor/time-clock.processor';

import { TimeClockController } from '../web/rest/time-clock.controller';
import { TimeClockEventsController } from '../web/rest/time-clock-event.controller';

import { TimeClockRepository } from '../repository/time-clock.repository';
import { WorkShiftRepository } from './../repository/work-shift.repository';
import { TimeClockEventRepository } from './../repository/time-clock-event.repository';

import { TimeClockService } from '../service/time-clock.service';
import { TimeClockEventsService } from './../service/time-clock-events.service';
import { TimeClockUpdateTransactionService } from './../service/transaction/time-clock-update-transaction.service';

import { JWTModule } from './jwt.module';
import { EmployeeModule } from './employee.module';
import { BranchModule } from './branch.module';
import { DesignationModule } from './designation.module';
import { SharedModule } from './../shared/shared.module';
import { CompanySettingsModule } from './company-settings.module';
import { FcmModule } from '../fcm/fcm.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([TimeClockRepository, TimeClockEventRepository, WorkShiftRepository]),
        BullModule.registerQueue({
            name: 'time-clock',
        }),
        HttpModule,
        JWTModule,
        BranchModule,
        DesignationModule,
        EmployeeModule,
        CompanySettingsModule,
        SharedModule,
        FcmModule,
    ],
    controllers: [TimeClockController, TimeClockEventsController],
    providers: [
        TimeClockService,
        TimeClockEventsService,
        TimeClockUpdateTransactionService,
        SocketGateway,
        TimeClockProcessor,
    ],
    exports: [TimeClockService, TimeClockEventsService],
})
export class TimeClockModule {}
