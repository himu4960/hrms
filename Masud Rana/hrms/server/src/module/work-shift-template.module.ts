import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { JWTModule } from './jwt.module';

import { WorkShiftTemplateController } from '../web/rest/work-shift-template.controller';
import { WorkShiftTemplateRepository } from '../repository/work-shift-template.repository';
import { WorkShiftTemplateService } from '../service/work-shift-template.service';

@Module({
    imports: [TypeOrmModule.forFeature([WorkShiftTemplateRepository]), JWTModule],
    controllers: [WorkShiftTemplateController],
    providers: [WorkShiftTemplateService],
    exports: [WorkShiftTemplateService],
})
export class WorkShiftTemplateModule {}
