import {MigrationInterface, QueryRunner} from "typeorm";

export class AddUserDeviceEntity1677822489269 implements MigrationInterface {
    name = 'AddUserDeviceEntity1677822489269'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `hrms_user_devices` (`id` int NOT NULL AUTO_INCREMENT, `created_at` bigint NULL DEFAULT '1677822491502', `updated_at` bigint NULL, `updated_by` int NULL, `created_by` int NULL, `company_id` int NOT NULL, `employee_id` int NOT NULL, `role_id` int NOT NULL, `device_token` varchar(255) NOT NULL, UNIQUE INDEX `IDX_49b07b14e3fe8f801e4bd37e45` (`device_token`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `hrms_company_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_application_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_roles` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_users` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_break_rule_conditions` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_break_rules` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_skills` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_designations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_time_zones` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_branches` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_leave_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_leaves` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_employee_designations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_workshifts` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_employees` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_user_companies` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_settings_time_clock` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_countries` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_settings_application` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_settings_shift_planning` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_settings_leave_availability` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_companies` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_announcements` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_blood_groups` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_blood_group_translations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_custom_fields` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_employee_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_files` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_genders` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_gender_translations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_geo_locations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_holidays` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_leave_balances` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_marital_statuses` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_marital_status_translations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_menus` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_permission_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_religions` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_religion_translations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_time_clocks` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_time_clock_events` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_workshift_templates` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677822491502'");
        await queryRunner.query("ALTER TABLE `hrms_user_devices` ADD CONSTRAINT `FK_a18b996faa06129a973165a7c1c` FOREIGN KEY (`company_id`) REFERENCES `hrms_companies`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `hrms_user_devices` ADD CONSTRAINT `FK_c55949909200f3feeae459bf55c` FOREIGN KEY (`employee_id`) REFERENCES `hrms_employees`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `hrms_user_devices` ADD CONSTRAINT `FK_e948bee02ab8f50e819c77e94bb` FOREIGN KEY (`role_id`) REFERENCES `hrms_roles`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `hrms_user_devices` DROP FOREIGN KEY `FK_e948bee02ab8f50e819c77e94bb`");
        await queryRunner.query("ALTER TABLE `hrms_user_devices` DROP FOREIGN KEY `FK_c55949909200f3feeae459bf55c`");
        await queryRunner.query("ALTER TABLE `hrms_user_devices` DROP FOREIGN KEY `FK_a18b996faa06129a973165a7c1c`");
        await queryRunner.query("ALTER TABLE `hrms_workshift_templates` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_time_clock_events` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_time_clocks` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_religion_translations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_religions` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_permission_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_menus` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_marital_status_translations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_marital_statuses` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_leave_balances` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_holidays` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_geo_locations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_gender_translations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_genders` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_files` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_employee_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_custom_fields` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_blood_group_translations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_blood_groups` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_announcements` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_companies` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_settings_leave_availability` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_settings_shift_planning` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_settings_application` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_countries` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_settings_time_clock` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_user_companies` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_employees` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_workshifts` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_employee_designations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_leaves` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_leave_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_branches` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_time_zones` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_designations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_skills` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_break_rules` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_break_rule_conditions` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_users` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_roles` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_application_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("ALTER TABLE `hrms_company_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677608551998'");
        await queryRunner.query("DROP INDEX `IDX_49b07b14e3fe8f801e4bd37e45` ON `hrms_user_devices`");
        await queryRunner.query("DROP TABLE `hrms_user_devices`");
    }

}
