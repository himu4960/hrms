import {MigrationInterface, QueryRunner} from "typeorm";

export class updateHolidayEntity1677579155856 implements MigrationInterface {
    name = 'updateHolidayEntity1677579155856'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `hrms_holidays` ADD `code` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `hrms_company_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_application_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_roles` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_users` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_break_rule_conditions` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_break_rules` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_skills` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_designations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_time_zones` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_branches` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_leave_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_leaves` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_employee_designations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_workshifts` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_employees` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_user_companies` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_settings_time_clock` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_countries` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_settings_application` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_settings_shift_planning` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_settings_leave_availability` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_companies` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_announcements` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_blood_groups` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_blood_group_translations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_custom_fields` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_employee_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_files` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_genders` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_gender_translations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_geo_locations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_holidays` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_leave_balances` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_marital_statuses` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_marital_status_translations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_menus` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_permission_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_religions` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_religion_translations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_time_clocks` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_time_clock_events` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
        await queryRunner.query("ALTER TABLE `hrms_workshift_templates` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1677579158101'");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `hrms_workshift_templates` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_time_clock_events` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_time_clocks` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_religion_translations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_religions` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_permission_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_menus` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_marital_status_translations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_marital_statuses` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_leave_balances` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_holidays` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_geo_locations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_gender_translations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_genders` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_files` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_employee_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_custom_fields` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_blood_group_translations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_blood_groups` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_announcements` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_companies` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_settings_leave_availability` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_settings_shift_planning` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_settings_application` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_countries` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_settings_time_clock` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_user_companies` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_employees` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_workshifts` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_employee_designations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_leaves` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_leave_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_branches` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_time_zones` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_designations` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_skills` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_break_rules` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_break_rule_conditions` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_users` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_roles` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_application_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_company_types` CHANGE `created_at` `created_at` bigint NULL DEFAULT '1676444467351'");
        await queryRunner.query("ALTER TABLE `hrms_holidays` DROP COLUMN `code`");
    }

}
