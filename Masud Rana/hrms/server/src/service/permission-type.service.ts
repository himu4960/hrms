import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { FindManyOptions, FindOneOptions } from 'typeorm';

import { PermissionTypeDTO } from '../service/dto/permission-type.dto';

import { PermissionTypeMapper } from '../service/mapper/permission-type.mapper';

import { PermissionTypeRepository } from '../repository/permission-type.repository';

@Injectable()
export class PermissionTypeService {
    logger = new Logger('PermissionTypeService');

    constructor(
        @InjectRepository(PermissionTypeRepository) private permissionTypeRepository: PermissionTypeRepository,
    ) {}

    async findById(id: number): Promise<PermissionTypeDTO | undefined> {
        const result = await this.permissionTypeRepository.findOne(id);
        return PermissionTypeMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<PermissionTypeDTO>): Promise<PermissionTypeDTO | undefined> {
        const result = await this.permissionTypeRepository.findOne(options);
        return PermissionTypeMapper.fromEntityToDTO(result);
    }

    async findAndCount(options: FindManyOptions<PermissionTypeDTO>): Promise<[PermissionTypeDTO[], number]> {
        const resultList = await this.permissionTypeRepository.findAndCount(options);
        const permissionTypeDTO: PermissionTypeDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach(permissionType =>
                permissionTypeDTO.push(PermissionTypeMapper.fromEntityToDTO(permissionType)),
            );
            resultList[0] = permissionTypeDTO;
        }
        return resultList;
    }

    async save(permissionTypeDTO: PermissionTypeDTO, creator?: number): Promise<PermissionTypeDTO | undefined> {
        const entity = PermissionTypeMapper.fromDTOtoEntity(permissionTypeDTO);
        if (creator) {
            if (!entity.created_by) {
                entity.created_by = creator;
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                entity.created_at = Date.now();
            }
            entity.updated_by = creator;
        }
        const result = await this.permissionTypeRepository.save(entity);
        return PermissionTypeMapper.fromEntityToDTO(result);
    }

    async update(permissionTypeDTO: PermissionTypeDTO, updater?: number): Promise<PermissionTypeDTO | undefined> {
        const entity = PermissionTypeMapper.fromDTOtoEntity(permissionTypeDTO);
        if (updater) {
            entity.updated_by = updater;
            entity.updated_at = Date.now();
        }
        const result = await this.permissionTypeRepository.save(entity);
        return PermissionTypeMapper.fromEntityToDTO(result);
    }

    async deleteById(id: number): Promise<PermissionTypeDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('Permission type is not found!', HttpStatus.NOT_FOUND);
        }
        await this.permissionTypeRepository.delete(id);
        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Permission type cannot be deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }
}
