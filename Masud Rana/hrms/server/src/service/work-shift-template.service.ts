import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';

import { WorkShiftTemplateDTO } from '../service/dto/work-shift-template.dto';
import { WorkShiftTemplateMapper } from '../service/mapper/work-shift-template.mapper';
import { WorkShiftTemplateRepository } from '../repository/work-shift-template.repository';

const relationshipNames = ['branch', 'designation'];
@Injectable()
export class WorkShiftTemplateService {
    logger = new Logger('WorkShiftTemplateService');

    constructor(
        @InjectRepository(WorkShiftTemplateRepository) private workShiftTemplateRepository: WorkShiftTemplateRepository,
    ) {}

    async findById(id: number): Promise<WorkShiftTemplateDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.workShiftTemplateRepository.findOne(id, options);
        return WorkShiftTemplateMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<WorkShiftTemplateDTO>): Promise<WorkShiftTemplateDTO | undefined> {
        options.relations = relationshipNames;
        const result = await this.workShiftTemplateRepository.findOne(options);
        return WorkShiftTemplateMapper.fromEntityToDTO(result);
    }

    async findAndCount(options: FindManyOptions<WorkShiftTemplateDTO>): Promise<[WorkShiftTemplateDTO[], number]> {
        options.relations = relationshipNames;
        const resultList = await this.workShiftTemplateRepository.findAndCount(options);
        const workShiftTemplateDTO: WorkShiftTemplateDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach(workShiftTemplate =>
                workShiftTemplateDTO.push(WorkShiftTemplateMapper.fromEntityToDTO(workShiftTemplate)),
            );
            resultList[0] = workShiftTemplateDTO;
        }
        return resultList;
    }

    async save(
        workShiftTemplateDTO: WorkShiftTemplateDTO,
        creator?: number,
    ): Promise<WorkShiftTemplateDTO | undefined> {
        const entity = WorkShiftTemplateMapper.fromDTOtoEntity(workShiftTemplateDTO);
        const shiftDuration = entity.end_time - entity.start_time;
        if (creator) {
            if (!entity.created_by) {
                entity.created_by = creator;
            }
        }
        const result = await this.workShiftTemplateRepository.save({
            ...entity,
            total_work_shift_duration: shiftDuration,
        });
        return WorkShiftTemplateMapper.fromEntityToDTO(result);
    }

    async deleteById(id: number): Promise<WorkShiftTemplateDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('entity is not found!', HttpStatus.NOT_FOUND);
        }

        await this.workShiftTemplateRepository.delete(id);

        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }
}
