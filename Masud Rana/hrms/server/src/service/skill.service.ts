import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, IsNull } from 'typeorm';

import { SkillDTO } from './dto/skill.dto';

import { SkillMapper } from './mapper/skill.mapper';

import { SkillRepository } from '../repository/skill.repository';

@Injectable()
export class SkillService {
    logger = new Logger('SkillService');

    constructor(@InjectRepository(SkillRepository) private skillRepository: SkillRepository) {}

    async findById(id: number): Promise<SkillDTO | undefined> {
        const result = await this.skillRepository.findOne(id);
        return SkillMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<SkillDTO>): Promise<SkillDTO | undefined> {
        const result = await this.skillRepository.findOne(options);
        return SkillMapper.fromEntityToDTO(result);
    }

    async findAll(company_id: number): Promise<SkillDTO[]> {
        return await this.skillRepository.find({
            where: { company_id },
        });
    }

    async findAndCount(company_id: number): Promise<[SkillDTO[], number]> {
        const options = { where: { company_id } };
        return await this.skillRepository.findAndCount(options);
    }

    async save(SkillDTO: SkillDTO, company_id: number, creator?: number): Promise<SkillDTO | undefined> {
        const entity = SkillMapper.fromDTOtoEntity(SkillDTO);
        if (creator) {
            if (!entity.created_by) {
                entity.created_by = creator;
            }
            entity.updated_by = creator;
        }
        const result = await this.skillRepository.save({
            ...entity,
            company_id,
            // Created_at default value is fixed in base.entity so we need to store current time in every post method.
            created_at: Date.now(),
        });
        if (!result) {
            throw new HttpException('Skill cannot be created!', HttpStatus.BAD_REQUEST);
        }
        return SkillMapper.fromEntityToDTO(result);
    }

    async update(SkillDTO: SkillDTO, updater?: number): Promise<SkillDTO | undefined> {
        const entity = SkillMapper.fromDTOtoEntity(SkillDTO);
        if (updater) {
            entity.updated_by = updater;
            entity.updated_at = Date.now();
        }
        const result = await this.skillRepository.save(entity);
        if (!result) {
            throw new HttpException('Skill cannot be updated!', HttpStatus.BAD_REQUEST);
        }
        return SkillMapper.fromEntityToDTO(result);
    }

    async updateById(SkillDTO: SkillDTO, id: number, updater?: number): Promise<SkillDTO | undefined> {
        const updatedResult = await this.skillRepository.update({ id: id }, { ...SkillDTO, updated_by: updater });
        if (updatedResult.raw.affectedRows > 0) {
            return await this.findById(id);
        }
        throw new HttpException('Skill cannot be updated!', HttpStatus.BAD_REQUEST);
    }

    async deleteById(id: number): Promise<SkillDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('Skill is not found!', HttpStatus.NOT_FOUND);
        }

        await this.skillRepository.delete(id);

        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Skill cannot be deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }
}
