import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';

import { DesignationDTO } from './dto/designation/designation.dto';
import { DesignationCreateDTO } from './dto/designation/designation-create.dto';
import { DesignationUpdateDTO } from './dto/designation/designation-update.dto';
import { EmployeeDesignationsDTO } from './dto/employee/employee-designations.dto';
import { BranchDTO } from './dto/branch/branch.dto';
import { SkillDTO } from './dto/skill.dto';

import {
    DesignationCreateMapper,
    DesignationMapper,
    DesignationUpdateMapper,
} from '../service/mapper/designation.mapper';

import { BranchRepository } from './../repository/branch.repository';
import { SkillRepository } from './../repository/skill.repository';
import { DesignationRepository } from '../repository/designation.repository';
import { EmployeeDesignationRepository } from '../repository/employee-designation.repository';

const relationshipNames = [];

@Injectable()
export class DesignationService {
    logger = new Logger('DesignationService');

    constructor(
        @InjectRepository(DesignationRepository) private designationRepository: DesignationRepository,
        @InjectRepository(EmployeeDesignationRepository)
        private employeeDesignationRepository: EmployeeDesignationRepository,
        @InjectRepository(BranchRepository) private branchRepository: BranchRepository,
        @InjectRepository(SkillRepository) private skillRepository: SkillRepository,
    ) {}

    async getEmployeeDesignationBasedSkills(employee_id: number, branch_id = null): Promise<SkillDTO[]> {
        const employeeDesignations = await this.employeeDesignationRepository.find({
            where: {
                employee_id,
            },
        });

        const designationIds = employeeDesignations.map(employeeDesignation => employeeDesignation.designation_id);

        let skills = this.skillRepository
            .createQueryBuilder('skills')
            .leftJoinAndSelect('skills.designations', 'designation')
            .select(['skills.id', 'skills.name']);

        if (designationIds.length) {
            skills = skills.where('designation.id IN(:...designationIds)', { designationIds });
        }

        if (branch_id) {
            skills = skills.andWhere('designation.branch_id = :branch_id', { branch_id });
        }
        return await skills.getMany();
    }

    async getVisibleDesignations(company_id: number): Promise<BranchDTO[] | undefined> {
        return await this.branchRepository
            .createQueryBuilder('branch')
            .leftJoinAndSelect('branch.designations', 'designation', 'designation.is_visible = true')
            .where('branch.company_id = :company_id', { company_id })
            .select(['branch.id', 'branch.name'])
            .addSelect(['designation.id', 'designation.name'])
            .getMany();
    }

    async getVisibleDesignationOne(company_id: number, branch_id: number): Promise<BranchDTO | undefined> {
        return await this.branchRepository
            .createQueryBuilder('branch')
            .leftJoinAndSelect('branch.designations', 'designation', 'designation.is_visible = true')
            .where('branch.company_id = :company_id', { company_id })
            .andWhere('branch.id = :branch_id', { branch_id })
            .select(['branch.id', 'branch.name'])
            .addSelect(['designation.id', 'designation.name'])
            .getOne();
    }

    async getDesignationBasedSkills(company_id: number, employee_id = null, branch_id = null): Promise<SkillDTO[]> {
        if (employee_id) {
            return await this.getEmployeeDesignationBasedSkills(employee_id, branch_id);
        }
        const skills = await this.skillRepository
            .createQueryBuilder('skill')
            .leftJoinAndSelect('skill.designations', 'designation')
            .where('skill.company_id = :company_id', { company_id })
            .andWhere('designation.id is not null')
            .andWhere('designation.is_visible = true')
            .getMany();

        if (!skills.length) {
            return await this.skillRepository.find({
                where: {
                    company_id,
                },
            });
        }
        return skills;
    }

    async findById(id: number): Promise<DesignationDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.designationRepository.findOne(id, options);
        return DesignationMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<DesignationDTO>): Promise<DesignationDTO | undefined> {
        const result = await this.designationRepository.findOne(options);
        return DesignationMapper.fromEntityToDTO(result);
    }

    async findByEmployeeId(
        options: FindOneOptions<EmployeeDesignationsDTO>,
        company_id: number,
    ): Promise<DesignationDTO[] | undefined> {
        const employeeDesignations: EmployeeDesignationsDTO[] = await this.employeeDesignationRepository.find(options);
        const designationIds = [];
        employeeDesignations.forEach(employeeDesignation => {
            designationIds.push(employeeDesignation.designation_id);
        });
        const conditions = {
            where: { company_id },
        };
        return await this.designationRepository.findByIds(designationIds, conditions);
    }

    async findAndCount(
        options: FindManyOptions<DesignationDTO>,
        company_id: number,
        branch_id: number,
    ): Promise<[DesignationDTO[], number]> {
        options.relations = relationshipNames;
        options.where = { company_id };
        if (branch_id) {
            options.where = { ...options.where, branch_id };
        }
        return await this.designationRepository.findAndCount(options);
    }

    async save(
        designationDTO: DesignationCreateDTO,
        company_id: number,
        creator?: number,
    ): Promise<DesignationDTO | undefined> {
        const entity = DesignationCreateMapper.fromDTOtoEntity(designationDTO);
        if (creator) {
            if (!entity.created_by) {
                entity.created_by = creator;
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                entity.created_at = Date.now();
            }
            entity.updated_by = creator;
        }
        const result = await this.designationRepository.save({ ...entity, company_id });
        return DesignationMapper.fromEntityToDTO(result);
    }

    async update(designationDTO: DesignationUpdateDTO, updater?: number): Promise<DesignationDTO | undefined> {
        const entity = DesignationUpdateMapper.fromDTOtoEntity(designationDTO);
        if (updater) {
            entity.updated_by = updater;
            entity.updated_at = Date.now();
        }
        const result = await this.designationRepository.save(entity);
        return DesignationMapper.fromEntityToDTO(result);
    }

    async deleteById(id: number): Promise<DesignationDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('Designation is not found!', HttpStatus.NOT_FOUND);
        }
        await this.designationRepository.delete(id);
        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Designation cannot be deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }
}
