import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as AWS from 'aws-sdk';

import { S3ResponseDTO } from './dto/s3-response.dto';

@Injectable()
export class MediaService {
    logger = new Logger('MediaService');

    constructor(private config: ConfigService) { }

    AWS_S3_BUCKET = this.config.get('S3_BUCKET_NAME');
    s3 = new AWS.S3({
        accessKeyId: this.config.get('S3_ACCESS_KEY'),
        secretAccessKey: this.config.get('S3_SECRET_KEY'),
    });

    async uploadFile(file: any): Promise<S3ResponseDTO> {
        const { originalname } = file;
        return await this.s3Upload(file.buffer, this.AWS_S3_BUCKET, originalname, file.mimetype);
    }

    async uploadLogo(originalFile: any, files: any[]): Promise<any> {
        let responseData = {};
        const { originalname, mimetype } = originalFile;

        for (let index = 0; index < files.length; index++) {
            const filename = `${Date.now()}_${originalname}`;
            responseData[files[index].logo_type] = await this.s3Upload(files[index].buffer, this.AWS_S3_BUCKET, filename, mimetype);
        }

        return responseData;
    }

    async s3Upload(file: Buffer, bucket: string, name: string, mimetype: string): Promise<S3ResponseDTO> {
        const params = {
            Bucket: bucket,
            Key: String(name),
            Body: file,
            ACL: 'public-read',
            ContentType: mimetype,
            ContentDisposition: 'inline',
            CreateBucketConfiguration: {
                LocationConstraint: this.config.get('S3_REGION_NAME'),
            },
        };
        try {
            let s3Response = await this.s3.upload(params).promise();
            return s3Response;
        } catch (e) {
            return e;
        }
    }

    async deleteFile(key: string): Promise<any> {
        const params = {
            Bucket: this.AWS_S3_BUCKET,
            Delete: {
                Objects: [
                    {
                        Key: key,
                    },
                ],
            },
        };
        return this.s3.deleteObjects(params).promise();
    }
}
