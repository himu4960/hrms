import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';

import { BRANCH_RESPONSE } from './../shared/constant/response-message';

import { BranchDTO } from './dto/branch/branch.dto';

import { BranchMapper } from '../service/mapper/branch.mapper';

import { BranchRepository } from '../repository/branch.repository';
import { BranchResponseDTO } from './dto/branch/branch-response.dto';

const relationshipNames = ['break_rule', 'time_zone', 'designations'];

@Injectable()
export class BranchService {
    logger = new Logger('BranchService');

    constructor(@InjectRepository(BranchRepository) private branchRepository: BranchRepository) {}

    async findById(id: number): Promise<BranchDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.branchRepository.findOne(id, options);
        return BranchMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<BranchDTO>): Promise<BranchDTO | undefined> {
        const result = await this.branchRepository.findOne(options);
        return BranchMapper.fromEntityToDTO(result);
    }

    async findAllWithEmployees(company_id: number): Promise<BranchDTO[]> {
        return await this.branchRepository
            .createQueryBuilder('branch')
            .leftJoinAndSelect('branch.employees', 'employee', 'employee.invitation_accepted = true')
            .leftJoinAndSelect('branch.designations', 'designation')
            .where('branch.company_id = :company_id', {
                company_id,
            })
            .select(['branch.id', 'branch.name'])
            .addSelect(['employee.id', 'employee.email', 'designation'])
            .getMany();
    }

    async findAll(company_id: number): Promise<BranchDTO[] | undefined> {
        return await this.branchRepository
            .createQueryBuilder('branch')
            .leftJoinAndSelect('branch.designations', 'designation')
            .leftJoinAndSelect('branch.break_rule', 'break_rule')
            .leftJoinAndSelect('branch.time_zone', 'time_zone')
            .leftJoinAndSelect('designation.skills', 'skill')
            .where('branch.company_id = :company_id', { company_id })
            .getMany();
    }

    async findAndCount(options: FindManyOptions<BranchResponseDTO>) {
        options.relations = relationshipNames;
        return await this.branchRepository.findAndCount(options);
    }

    async save(branchDTO: BranchDTO, creator?: number): Promise<BranchDTO | undefined> {
        const entity = BranchMapper.fromDTOtoEntity(branchDTO);
        if (creator) {
            if (!entity.created_by) {
                entity.created_by = creator;
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                entity.created_at = Date.now();
            }
            entity.updated_by = creator;
        }
        const result = await this.branchRepository.save(entity);
        return BranchMapper.fromEntityToDTO(result);
    }

    async update(branchDTO: BranchDTO, updater?: number): Promise<BranchDTO | undefined> {
        const entity = BranchMapper.fromDTOtoEntity(branchDTO);
        if (updater) {
            entity.updated_by = updater;
            entity.updated_at = Date.now();
        }
        const result = await this.branchRepository.save(entity);
        return BranchMapper.fromEntityToDTO(result);
    }

    async deleteById(id: number): Promise<BranchDTO | undefined> {
        const branchFind = await this.findById(id);
        if (!branchFind) {
            throw new HttpException(BRANCH_RESPONSE.NOT_FOUND, HttpStatus.NOT_FOUND);
        }

        if (branchFind.is_primary) {
            throw new HttpException(BRANCH_RESPONSE.DELETE.PRIMARY_ERROR, HttpStatus.FORBIDDEN);
        }
        await this.branchRepository.delete(id);
        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException(BRANCH_RESPONSE.DELETE.ERROR, HttpStatus.FOUND);
        }
        return branchFind;
    }
}
