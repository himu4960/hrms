import { JwtService } from '@nestjs/jwt';
import { Injectable } from '@nestjs/common';

import { config } from '../config';

@Injectable()
export class JWTService {
    constructor(private jwtService: JwtService) {}

    async verifyJWTToken(token: string) {
        const secret = config['hrms.security.authentication.jwt.base64-secret'];
        return this.jwtService.verify(token, { secret });
    }

    async generateJWTToken(credential: any) {
        const secret = config['hrms.security.authentication.jwt.base64-secret'];
        return this.jwtService.sign(credential, { secret, expiresIn: '24h' });
    }

    async decodedJWTToken(credential: string) {
        return this.jwtService.decode(credential?.replace('Bearer ', ''));
    }
}
