import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CountryDTO } from '../service/dto/country.dto';

import { CountryRepository } from '../repository/country.repository';

@Injectable()
export class CountryService {
    logger = new Logger('CountryService');

    constructor(@InjectRepository(CountryRepository) private countryRepository: CountryRepository) {}

    async getAll(): Promise<CountryDTO[]> {
        return await this.countryRepository.find({
            select: ['id', 'code', 'name', 'iso_code', 'is_active', 'is_deleted', 'is_default'],
        });
    }

    async getActiveCountries(): Promise<CountryDTO[]> {
        return await this.countryRepository.find({
            select: ['id', 'code', 'name', 'iso_code', 'is_default'],
            where: {
                is_active: true,
                is_deleted: false,
            },
        });
    }
}
