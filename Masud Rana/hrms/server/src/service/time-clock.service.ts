import { Injectable, HttpException, HttpStatus, Logger, HttpService } from '@nestjs/common';
import { Between, FindManyOptions, FindOneOptions, IsNull, Not } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ETimeClockEventType } from './../domain/enum/time-clock.enum';

import { EmployeeDesignation } from './../domain/employee-designation.entity';

import { TimeClockDTO } from './dto/time-clock/time-clock.dto';
import { TimeClockEventDTO } from './dto/time-clock/time-clock-event.dto';
import { TimeClockUpdateDTO } from './dto/time-clock/time-clock-update.dto';
import { TimeClockFilterForEmployeeDTO } from './dto/time-clock/time-clock-filter-for-employee.dto';
import { TimeClockCalculationDTO } from './dto/time-clock/time-clock-calculation.dto';
import { TimeSheetDTO } from './dto/time-clock/time-sheet.dto';
import { TimeClockFilterDTO } from './dto/time-clock/time-clock-filter.dto';
import { TimeSheetManageFilterParamsDTO } from './dto/time-clock/time-sheet-manage-filter-params.dto';
import { TimeSheetImportDTO } from './dto/time-clock/time-sheet-import.dto';
import { TimeClockCreateDTO } from './dto/time-clock/time-clock-create.dto';
import { WorkShiftDTO } from './dto/work-shift/work-shift.dto';

import { TimeClockMapper } from '../service/mapper/time-clock.mapper';

import { TimeClockRepository } from '../repository/time-clock.repository';
import { WorkShiftRepository } from './../repository/work-shift.repository';

import { EmployeeService } from './employee.service';
import { BranchService } from './branch.service';
import { DesignationService } from './designation.service';
import { DateTimeConvertService } from '../shared/convert/date-time-convert.service';
import { NotificationService } from '../fcm/notification.service';
import { TimeClockUpdateTransactionService } from './transaction/time-clock-update-transaction.service';
import { CompanySettingsService } from './company-settings.service';

const timeClockrelationshipNames = ['time_clock_events'];

@Injectable()
export class TimeClockService {
    logger = new Logger('TimeClockService');
    constructor(
        @InjectQueue('time-clock') private timeClockQueue: Queue,
        @InjectRepository(TimeClockRepository) private timeClockRepository: TimeClockRepository,
        @InjectRepository(WorkShiftRepository) private workShiftRepository: WorkShiftRepository,
        private httpService: HttpService,
        private companySettingsService: CompanySettingsService,
        private branchService: BranchService,
        private employeeService: EmployeeService,
        private designationService: DesignationService,
        private dateTimeConvertService: DateTimeConvertService,
        private notificationService: NotificationService,
        private timeClockUpdateTransactionService: TimeClockUpdateTransactionService,
    ) {}

    async importTimeSheet(
        timeSheetImportDTOs: TimeSheetImportDTO[],
        company_id: number,
    ): Promise<TimeClockDTO[] | undefined> {
        let findEmployeeErrorCount = 0;
        let designation_id = null;

        const importableTimeSheets = await Promise.all(
            timeSheetImportDTOs.map(async (timeSheet: TimeSheetImportDTO) => {
                const { clock_start_time, clock_end_time, position, notes, email } = timeSheet;

                const findEmployee = await this.employeeService.findByEmail(email);
                if (!findEmployee) {
                    findEmployeeErrorCount++;
                }

                findEmployee?.employee_designations.map((employee_designation: EmployeeDesignation) => {
                    const { designation } = employee_designation;
                    if (designation.name.toLowerCase() === position.toLowerCase()) {
                        designation_id = employee_designation.designation_id;
                    }
                });
                let total_clock_duration = 0;

                total_clock_duration = this.dateTimeConvertService.getDateDifferenceInSeconds(
                    timeSheet.clock_end_time,
                    timeSheet.clock_start_time,
                );

                const newTimeClock = {
                    clock_start_time,
                    clock_end_time,
                    notes,
                    employee_id: findEmployee?.id ?? null,
                    branch_id: findEmployee?.branch_id ?? null,
                    company_id,
                    designation_id,
                    total_clock_duration,
                    // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                    created_at: Date.now(),
                };
                return newTimeClock;
            }),
        );

        if (findEmployeeErrorCount > 0 || !designation_id) {
            return null;
        }

        return await this.timeClockRepository.save(importableTimeSheets);
    }

    async findById(id: number): Promise<TimeClockDTO | undefined> {
        const result = await this.timeClockRepository
            .createQueryBuilder('timeClock')
            .leftJoinAndSelect('timeClock.time_clock_events', 'time_clock_event')
            .leftJoinAndSelect('time_clock_event.new_designation', 'new_designation')
            .where('timeClock.id=:id', { id })
            .addSelect([
                'time_clock_event.created_at',
                'timeClock.designation_id',
                'timeClock.employee_id',
                'timeClock.work_shift_id',
            ])
            .getOne();

        result.clock_start_time = Number(result.clock_start_time);
        result.pre_clock_time = Number(result.pre_clock_time);
        if (result.clock_end_time !== null) {
            result.clock_end_time = Number(result.clock_end_time);
        }
        return TimeClockMapper.fromEntityToDTO(result);
    }

    async getTotalDurationOfTimeClock(
        timeClockGetByEmployeeDTO: TimeClockFilterForEmployeeDTO,
        company_id: number,
    ): Promise<TimeClockCalculationDTO> {
        const { employee_id } = timeClockGetByEmployeeDTO;
        let timeClockQuery = this.timeClockRepository
            .createQueryBuilder('timeClock')
            .where('timeClock.company_id = :company_id', { company_id })
            .andWhere('timeClock.employee_id = :employee_id', { employee_id });

        const timeClockDuration = await timeClockQuery
            .select('SUM(timeClock.total_clock_duration)', 'total_duration')
            .addSelect('SUM(timeClock.total_break_duration)', 'total_break_duration')
            .getRawOne();

        const { total_duration, total_break_duration } = timeClockDuration;

        return {
            total_duration: Number(total_duration),
            total_break_duration: Number(total_break_duration),
            total_without_break_duration: Number(total_duration) - Number(total_break_duration),
        };
    }

    async getTimeSheetCount(company_id: number): Promise<number> {
        return await this.timeClockRepository
            .createQueryBuilder('timeClock')
            .where('timeClock.company_id = :company_id', { company_id })
            .where('timeClock.is_approved = true')
            .getCount();
    }

    async findForEmployee(
        company_id: number,
        timeClockGetByEmployeeDTO: TimeClockFilterForEmployeeDTO,
    ): Promise<TimeClockDTO | null> {
        const { employee_id } = timeClockGetByEmployeeDTO;
        let lastClockIn = this.timeClockRepository
            .createQueryBuilder('timeClock')
            .leftJoinAndSelect('timeClock.time_clock_events', 'time_clock_event')
            .leftJoinAndSelect('time_clock_event.new_designation', 'new_designation')
            .where('timeClock.company_id=:company_id', { company_id })
            .andWhere('timeClock.employee_id=:employee_id', { employee_id });

        return await lastClockIn
            .addSelect(['time_clock_event.created_at'])
            .orderBy('timeClock.id', 'DESC')
            .addOrderBy('time_clock_event.id', 'ASC')
            .getOne();
    }

    async findByFields(options: FindOneOptions<TimeClockDTO>): Promise<TimeClockDTO | undefined> {
        const result = await this.timeClockRepository.findOne(options);
        return TimeClockMapper.fromEntityToDTO(result);
    }

    async findAndCount(
        options: FindManyOptions<TimeClockDTO>,
        company_id: number,
        timeClockGetByEmployeeDTO?: TimeClockFilterForEmployeeDTO,
    ): Promise<[TimeClockDTO[], number]> {
        options.relations = timeClockrelationshipNames;
        options.where = {
            company_id,
            ...timeClockGetByEmployeeDTO,
            clock_start_time: Not(IsNull()),
            clock_end_time: Not(IsNull()),
        };
        return await this.timeClockRepository.findAndCount(options);
    }

    async timeSheetFindAndCount(
        options: FindManyOptions<TimeClockDTO>,
        company_id: number,
        timeClockGetByEmployeeDTO: TimeClockFilterForEmployeeDTO,
    ): Promise<[TimeSheetDTO, number]> {
        const [time_clocks, count] = await this.findAndCount(options, company_id, timeClockGetByEmployeeDTO);
        const time_clock_duration = await this.getTotalDurationOfTimeClock(timeClockGetByEmployeeDTO, company_id);
        const result = { time_clocks, ...time_clock_duration };
        return [result, count];
    }

    async timeSheetManage(
        timeSheetManageFilterParams: TimeSheetManageFilterParamsDTO,
        company_id: number,
    ): Promise<any | undefined> {
        const start_date = new Date(timeSheetManageFilterParams.start_date);
        const end_date = new Date(timeSheetManageFilterParams.end_date);
        end_date.setDate(end_date.getDate() + 1);

        const startDateTimeStamp = start_date.getTime();
        const endDateTimeStamp = end_date.getTime();

        let whereStatement = `WHERE 1 AND clock_start_time BETWEEN ${startDateTimeStamp} AND ${endDateTimeStamp} AND hrms_time_clocks.company_id = ${company_id} AND clock_end_time IS NOT NULL`;
        if (timeSheetManageFilterParams?.designation_id) {
            const { designation_id } = timeSheetManageFilterParams;
            whereStatement += ` AND hrms_time_clocks.designation_id = ${designation_id}`;
        }

        if (timeSheetManageFilterParams?.employee_id) {
            const { employee_id } = timeSheetManageFilterParams;
            whereStatement += ` AND hrms_time_clocks.employee_id = ${employee_id}`;
        }

        if (timeSheetManageFilterParams?.is_approved !== null) {
            const { is_approved } = timeSheetManageFilterParams;
            whereStatement += ` AND hrms_time_clocks.is_approved = ${is_approved}`;
        }

        const orderBy = `ORDER BY clock_start_time, employee_name`;

        const selectStatement = `SELECT
            hrms_time_clocks.id AS time_clock_id,
            CONCAT(
                hrms_employees.first_name,
                ' ',
                hrms_employees.last_name
            ) AS employee_name,
            hrms_designations.name AS designation_name,
            clock_start_time,
            clock_end_time,
            is_approved,
            total_clock_duration,
            total_break_duration
            FROM
            hrms_time_clocks
            INNER JOIN hrms_employees ON hrms_employees.id = hrms_time_clocks.employee_id
            LEFT JOIN hrms_designations ON hrms_designations.id = hrms_time_clocks.designation_id`;

        const sql = `${selectStatement} ${whereStatement} ${orderBy}`;

        return await this.timeClockRepository.query(sql);
    }

    async filter(
        paginationOptions: FindManyOptions<TimeClockDTO>,
        timeClockFilterDTO: TimeClockFilterDTO,
        company_id: number,
    ): Promise<[TimeSheetDTO, number]> {
        const { filter_start_date, filter_end_date, employee_id, is_approved } = timeClockFilterDTO;

        let whereOptions: any = {
            employee_id,
            company_id,
        };

        if (is_approved) {
            whereOptions = {
                ...whereOptions,
                is_approved,
            };
        }

        if (filter_start_date && filter_end_date) {
            const end_date = new Date(filter_end_date);
            end_date.setDate(end_date.getDate() + 1);
            whereOptions = {
                ...whereOptions,
                clock_start_time: Between(filter_start_date, end_date.getTime()),
            };
        }

        paginationOptions.where = { ...whereOptions, clock_end_time: Not(IsNull()) };
        const [time_clocks, count] = await this.timeClockRepository.findAndCount(paginationOptions);
        const time_clock_duration = await this.getTotalDurationOfTimeClock({ employee_id }, company_id);

        const result = { time_clocks, ...time_clock_duration };

        return [result, count];
    }

    async isClockOut(employee_id: number) {
        const existingTimeClock = await this.timeClockRepository.findOne({
            where: { employee_id: employee_id },
            order: { id: 'DESC' },
        });

        if (existingTimeClock && !existingTimeClock.clock_end_time) {
            return false;
        }

        return true;
    }

    async findWorkShiftForTimeClock(
        company_id: number,
        employee_id: number,
        clock_start_time: number,
    ): Promise<WorkShiftDTO | null> {
        const timeClockSettings = await this.companySettingsService.getTimeClockByCompanyId(company_id);

        const beforeClockInCount = timeClockSettings.pre_shift_punch_in_enabled_time;

        let work_shift = null;

        if (clock_start_time) {
            work_shift = await this.workShiftRepository
                .createQueryBuilder('workShift')
                .leftJoinAndSelect('workShift.employees', 'employee')
                .where('employee.id = :employee_id', {
                    employee_id,
                })
                .andWhere('workShift.is_published = true')
                .andWhere('workShift.end_date >= :clock_start_time AND workShift.start_date <= :beforeClockInTime', {
                    clock_start_time,
                    beforeClockInTime: clock_start_time + beforeClockInCount,
                })
                .select(['workShift.id', 'workShift.end_date'])
                .addSelect('workShift.designation_id')
                .getOne();
        }
        return work_shift;
    }

    async save(
        timeClockDTO: TimeClockCreateDTO,
        company_id: number,
        creator?: number,
    ): Promise<TimeClockDTO | undefined> {
        const { branch_id, employee_id, designation_id, clock_start_time } = timeClockDTO;
        const location =
            timeClockDTO?.clockin_latitude &&
            timeClockDTO?.clockin_longitude &&
            (await this.getLocation(timeClockDTO.clockin_latitude, timeClockDTO.clockin_longitude).toPromise());
        const stringifiedLocation = await JSON.stringify(location);
        const timeClockSettings = await this.companySettingsService.getTimeClockByCompanyId(company_id);

        if (!(await this.isClockOut(timeClockDTO.employee_id))) {
            throw new HttpException('Please clock out!', HttpStatus.NOT_FOUND);
        }

        const existingBranch = await this.branchService.findById(branch_id);
        if (!existingBranch) {
            throw new HttpException('Branch not found!', HttpStatus.NOT_FOUND);
        }

        const existingEmployee = await this.employeeService.findById(employee_id, company_id);
        if (!existingEmployee) {
            throw new HttpException('Employee not found!', HttpStatus.NOT_FOUND);
        }

        const existingDesignation = await this.designationService.findById(designation_id);
        if (!existingDesignation) {
            throw new HttpException('Designation not found!', HttpStatus.NOT_FOUND);
        }

        let total_clock_duration = 0;

        if (timeClockDTO?.clock_end_time) {
            total_clock_duration = this.dateTimeConvertService.getDateDifferenceInSeconds(
                timeClockDTO.clock_end_time,
                timeClockDTO.clock_start_time,
            );
        }

        const work_shift = await this.findWorkShiftForTimeClock(company_id, employee_id, clock_start_time);
        let work_shift_id = work_shift?.id ?? null;

        const result = await this.timeClockRepository.save({
            ...timeClockDTO,
            company_id,
            created_by: creator,
            clock_end_time: timeClockDTO?.clock_end_time ?? null,
            designation_id: work_shift?.designation_id ?? null,
            work_shift_id,
            total_clock_duration,
            total_break_duration: 0,
            total_break_taken: 0,
            is_running: !timeClockDTO?.clock_end_time ? true : false,
            clockin_location: stringifiedLocation,
            // Created_at default value is fixed in base.entity so we need to store current time in every post method.
            created_at: Date.now(),
        });
        result.clock_start_time = Number(result.clock_start_time);

        if (result?.id && timeClockSettings.enable_auto_punch_out && work_shift) {
            const delay = work_shift.end_date + timeClockSettings.allow_employee_to_work_after_shift - clock_start_time;
            this.timeClockQueue.add(
                'auto-clock-out',
                {
                    time_clock_id: result.id,
                },
                {
                    delay,
                },
            );
        }

        return TimeClockMapper.fromEntityToDTO(result);
    }

    async clockOut(timeClockDTO: TimeClockUpdateDTO, updater?: number): Promise<TimeClockDTO | undefined> {
        const currentTime = Date.now();
        const location = await this.getLocation(
            timeClockDTO.clockout_latitude,
            timeClockDTO.clockout_longitude,
        ).toPromise();
        const stringifiedLocation = JSON.stringify(location);

        const existingTimeClock = await this.timeClockRepository
            .createQueryBuilder('timeClock')
            .leftJoinAndSelect('timeClock.time_clock_events', 'time_clock_events')
            .where('timeClock.id = :id', { id: timeClockDTO.id })
            .addSelect('timeClock.employee_id')
            .getOne();

        if (!existingTimeClock) {
            throw new HttpException('Time clock not found!', HttpStatus.NOT_FOUND);
        }

        let designation_id = existingTimeClock.designation_id;

        let total_break_duration = 0;
        existingTimeClock?.time_clock_events.forEach((event: TimeClockEventDTO) => {
            if (event.event_type === ETimeClockEventType.BREAK && event?.total_break_duration) {
                total_break_duration = total_break_duration + event.total_break_duration;
            }

            if (event.event_type === ETimeClockEventType.POSITION) {
                designation_id = event?.new_designation?.id ?? existingTimeClock.designation_id;
            }
        });

        const total_clock_duration = this.dateTimeConvertService.getDateDifferenceInSeconds(
            currentTime,
            existingTimeClock.clock_start_time,
        );

        const work_shift = await this.workShiftRepository
            .createQueryBuilder('workShift')
            .where('workShift.id = :work_shift_id', {
                work_shift_id: existingTimeClock.work_shift_id,
            })
            .andWhere('workShift.is_published = true')
            .andWhere('workShift.start_date < :clock_end_time', {
                clock_end_time: currentTime,
            })
            .getOne();

        let work_shift_id = work_shift?.id ?? null;

        await this.timeClockRepository.update(
            { id: timeClockDTO.id },
            {
                ...timeClockDTO,
                clock_end_time: currentTime,
                total_clock_duration,
                total_break_duration,
                designation_id,
                work_shift_id: work_shift_id ? existingTimeClock.work_shift_id : work_shift_id,
                total_break_taken: existingTimeClock.time_clock_events.length,
                clockout_location: stringifiedLocation,
                updated_by: updater,
                is_running: false,
                updated_at: currentTime,
            },
        );
        const result: TimeClockDTO = await this.findById(timeClockDTO.id);
        // BigInt type was returning values as string
        result.clock_start_time = Number(result.clock_start_time);
        result.clock_end_time = Number(result.clock_end_time);
        return result;
    }

    async timeClockEditMode(
        timeClockDTO: TimeClockUpdateDTO,
        company_id: number,
        updater?: number,
    ): Promise<TimeClockDTO | undefined> {
        try {
            const existingTimeClock = await this.findById(timeClockDTO.id);
            if (!existingTimeClock) {
                throw new HttpException('Time clock not found!', HttpStatus.NOT_FOUND);
            }

            const { clock_start_time, clock_end_time } = timeClockDTO;

            const total_clock_duration = this.dateTimeConvertService.getDateDifferenceInSeconds(
                clock_end_time,
                clock_start_time,
            );

            const newTimeClockEvents = this.addCompanyId(timeClockDTO.time_clock_events, company_id);

            let total_break_duration = 0;
            newTimeClockEvents.forEach((event: TimeClockEventDTO) => {
                if (event.event_type === ETimeClockEventType.BREAK) {
                    total_break_duration = total_break_duration + event.total_break_duration;
                }
                if (event.event_type === ETimeClockEventType.POSITION) {
                    timeClockDTO = { ...timeClockDTO, designation_id: event.new_position_id };
                }
            });

            if (total_clock_duration < total_break_duration) {
                throw new HttpException('Clock duration should be greater than break duration!', HttpStatus.NOT_FOUND);
            }

            const result = await this.timeClockUpdateTransactionService.run({
                ...timeClockDTO,
                id: Number(timeClockDTO.id),
                company_id,
                total_clock_duration: total_clock_duration ? total_clock_duration : 1,
                total_break_duration: total_break_duration ? total_break_duration : 0,
                total_break_taken: newTimeClockEvents.length,
                time_clock_events: newTimeClockEvents,
                updated_by: updater,
                updated_at: Date.now(),
            });

            const deviceTokens = await this.notificationService.getDeviceTokens({
                where: {
                    company_id,
                    employee_id: existingTimeClock.employee_id,
                },
            });

            if (deviceTokens.length) {
                const payload = {
                    notification: {
                        title: 'Attendance Time Sheet Update.',
                        body:
                            'your attendance schedule has been updated manually by your manager, [Manager Name]. Please log in to the Hazira app to check your report for the updated schedule.',
                    },
                };
                const data = await this.notificationService.sendNotification(deviceTokens, payload);
            }

            return result;
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async timeClockUpdate(
        timeClockDTO: TimeClockUpdateDTO,
        company_id: number,
        updater?: number,
    ): Promise<TimeClockDTO | undefined> {
        const timeClockSettings = await this.companySettingsService.getTimeClockByCompanyId(company_id);
        const existingTimeClock = await this.findById(timeClockDTO.id);
        if (!existingTimeClock) {
            throw new HttpException('Time clock not found!', HttpStatus.NOT_FOUND);
        }

        let work_shift = null;
        if (!existingTimeClock.work_shift_id) {
            work_shift = await this.findWorkShiftForTimeClock(
                company_id,
                existingTimeClock.employee_id,
                timeClockDTO?.clock_start_time,
            );
        }

        let work_shift_id = work_shift?.id ?? existingTimeClock.work_shift_id;

        await this.timeClockRepository.update(
            { id: timeClockDTO.id },
            {
                ...timeClockDTO,
                designation_id: work_shift?.designation_id ?? null,
                work_shift_id,
                updated_by: updater,
                updated_at: Date.now(),
            },
        );
        const result: TimeClockDTO = await this.findById(timeClockDTO.id);
        // BigInt type was returning values as string
        result.clock_start_time = Number(result.clock_start_time);
        if (result.clock_end_time !== null) {
            result.clock_end_time = Number(result.clock_end_time);
        }
        result.pre_clock_time = Number(result.pre_clock_time);

        if (timeClockSettings.enable_auto_punch_out && work_shift && work_shift.end_date > result.clock_start_time) {
            const delay =
                work_shift.end_date + timeClockSettings.allow_employee_to_work_after_shift - result.clock_start_time;
            this.timeClockQueue.add(
                'auto-clock-out',
                {
                    time_clock_id: result.id,
                },
                {
                    delay,
                },
            );
        }
        return result;
    }

    async deleteById(id: number): Promise<TimeClockDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('Time clock is not found!', HttpStatus.NOT_FOUND);
        }
        await this.timeClockRepository.delete(id);
        return entityFind;
    }

    addCompanyId(timeClockEvents: TimeClockEventDTO[], company_id: number): TimeClockEventDTO[] {
        return timeClockEvents.map(timeClockEvent => {
            let total_break_duration = 0;

            if (timeClockEvent.event_type === ETimeClockEventType.BREAK) {
                total_break_duration = this.dateTimeConvertService.getDateDifferenceInSeconds(
                    timeClockEvent.break_end_time,
                    timeClockEvent.break_start_time,
                );
            }

            return {
                ...timeClockEvent,
                company_id,
                total_break_duration,
            };
        });
    }

    getLocation(latitude: number, longitude: number): Observable<any> {
        return this.httpService
            .get(
                `https://nominatim.openstreetmap.org/reverse?format=json&lat=${latitude}&lon=${longitude}&accept-language=en`,
            )
            .pipe(
                map(response => {
                    return response.data;
                }),
            );
    }
}
