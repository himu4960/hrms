import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { LanguageDTO } from '../service/dto/language.dto';

import { LanguageRepository } from '../repository/language.repository';

@Injectable()
export class LanguageService {
    logger = new Logger('LanguageService');

    constructor(@InjectRepository(LanguageRepository) private languageRepository: LanguageRepository) {}

    async getAll(): Promise<LanguageDTO[]> {
        return await this.languageRepository.find({
            select: ['code', 'name', 'is_active', 'is_deleted', 'is_default'],
        });
    }

    async getActiveLanguages(): Promise<LanguageDTO[]> {
        return await this.languageRepository.find({
            select: ['code', 'name', 'is_default'],
            where: {
                is_active: true,
                is_deleted: false,
            },
        });
    }
}
