import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';

import { CustomFieldTypeDTO } from '../service/dto/custom-field-type.dto';

import { CustomFieldTypeMapper } from '../service/mapper/custom-field-type.mapper';

import { CustomFieldTypeRepository } from '../repository/custom-field-type.repository';

@Injectable()
export class CustomFieldTypeService {
    logger = new Logger('CustomFieldTypeService');

    constructor(
        @InjectRepository(CustomFieldTypeRepository) private customFieldTypeRepository: CustomFieldTypeRepository,
    ) {}

    async findById(id: number): Promise<CustomFieldTypeDTO | undefined> {
        const result = await this.customFieldTypeRepository.findOne(id);
        return CustomFieldTypeMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<CustomFieldTypeDTO>): Promise<CustomFieldTypeDTO | undefined> {
        const result = await this.customFieldTypeRepository.findOne(options);
        return CustomFieldTypeMapper.fromEntityToDTO(result);
    }

    async findAndCount(options?: FindManyOptions<CustomFieldTypeDTO>): Promise<[CustomFieldTypeDTO[], number]> {
        return await this.customFieldTypeRepository.findAndCount(options);
    }

    async save(customFieldTypeDTO: CustomFieldTypeDTO): Promise<CustomFieldTypeDTO | undefined> {
        const entity = CustomFieldTypeMapper.fromDTOtoEntity(customFieldTypeDTO);

        const result = await this.customFieldTypeRepository.save({
            ...entity,
            // Created_at default value is fixed in base.entity so we need to store current time in every post method.
            created_at: Date.now(),
        });
        return CustomFieldTypeMapper.fromEntityToDTO(result);
    }

    async update(customFieldTypeDTO: CustomFieldTypeDTO): Promise<CustomFieldTypeDTO | undefined> {
        const entity = CustomFieldTypeMapper.fromDTOtoEntity(customFieldTypeDTO);

        const result = await this.customFieldTypeRepository.save({ ...entity, updated_at: Date.now() });
        return CustomFieldTypeMapper.fromEntityToDTO(result);
    }

    async deleteById(id: number): Promise<CustomFieldTypeDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('Custom field type is not found', HttpStatus.NOT_FOUND);
        }
        await this.customFieldTypeRepository.delete(id);
        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Custom field type cannot be deleted', HttpStatus.FOUND);
        }
        return entityFind;
    }
}
