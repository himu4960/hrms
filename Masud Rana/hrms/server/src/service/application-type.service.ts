import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions } from 'typeorm';

import { ApplicationTypeDTO } from '../service/dto/application-type.dto';

import { ApplicationTypeMapper } from './mapper/application-type.mapper';

import { ApplicationTypeRepository } from '../repository/application-type.repository';

@Injectable()
export class ApplicationTypeService {
    logger = new Logger('ApplicationTypeService');

    constructor(
        @InjectRepository(ApplicationTypeRepository) private applicationTypeRepository: ApplicationTypeRepository,
    ) {}

    async findById(id: number): Promise<ApplicationTypeDTO | undefined> {
        const result = await this.applicationTypeRepository.findOne(id);
        return ApplicationTypeMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<ApplicationTypeDTO>): Promise<ApplicationTypeDTO | undefined> {
        const result = await this.applicationTypeRepository.findOne(options);
        return ApplicationTypeMapper.fromEntityToDTO(result);
    }

    async getAll(): Promise<ApplicationTypeDTO[]> {
        return await this.applicationTypeRepository.find({
            select: ['id', 'name', 'is_active', 'is_deleted'],
        });
    }

    async getActiveApplicationTypes(): Promise<ApplicationTypeDTO[]> {
        return await this.applicationTypeRepository.find({
            select: ['id', 'name'],
            where: {
                is_active: true,
                is_deleted: false,
            },
        });
    }
}
