import { Injectable, Logger } from '@nestjs/common';

import { AnnouncementService } from './announcement.service';
import { EmployeeService } from './employee.service';
import { FileService } from './file.service';
import { LeaveService } from './leave.service';
import { TimeSheetReportService } from './reports/time-sheet.report.service';
import { WorkShiftService } from './work-shift.service';
import { TimeClockService } from './time-clock.service';
import { DateTimeConvertService } from './../shared/convert/date-time-convert.service';
import { BranchService } from './branch.service';
import { SkillService } from './skill.service';

import { CompanyPayload } from 'src/security/payload.interface';

import { ETimeSheetStatus } from '../shared/enum/time-sheet.enum';

@Injectable()
export class CommonService {
    logger = new Logger('CommonService');

    constructor(
        private readonly announcementService: AnnouncementService,
        private readonly fileService: FileService,
        private readonly leaveService: LeaveService,
        private readonly timeSheetReportService: TimeSheetReportService,
        private readonly employeeService: EmployeeService,
        private readonly skillService: SkillService,
        private readonly workShiftService: WorkShiftService,
        private readonly branchService: BranchService,
        private readonly timeClockService: TimeClockService,
        private readonly dateTimeConvertService: DateTimeConvertService,
    ) {}

    async getDashboardData(timeZone: any, decoded: CompanyPayload): Promise<any> {
        const { company_id, employee_id } = decoded;
        const filterDTO = {
            start_date: new Date(this.dateTimeConvertService.format(new Date())),
            end_date: new Date(
                this.dateTimeConvertService.format(new Date(new Date().setTime(new Date().getTime() + 1))),
            ),
        };

        const announcements = await this.announcementService.getAll(company_id);
        const files = await this.fileService.getAll(company_id);
        const on_leaves = await this.leaveService.getAllOnLeaves(company_id);
        const latesData = await this.timeSheetReportService.getTimeSheetLateSummary(
            filterDTO,
            company_id.toString(),
            timeZone,
        );

        const lates = latesData.filter(
            data => data.late_status === ETimeSheetStatus.YES || data.late_status === ETimeSheetStatus.ABSENT,
        );

        const upcoming_shifts = await this.workShiftService.getUpcomingShifts(company_id, employee_id);

        const anniversaries = await this.employeeService.getEmployeeAnniversary(company_id, null);
        const birthdays = await this.employeeService.getEmployeeBirthday(company_id, null);
        const leave_request_count = await this.leaveService.getTotalLeaveRequestCount(company_id);
        const all_shift_count = await this.workShiftService.getTotalShiftCount(company_id);
        const unpublished_shift_count = await this.workShiftService.getTotalShiftRepublishCount(company_id);
        const time_sheet_count = await this.timeClockService.getTimeSheetCount(company_id);

        return {
            announcements,
            files,
            on_leaves,
            lates,
            anniversaries,
            birthdays,
            upcoming_shifts,
            leave_request_count,
            all_shift_count,
            unpublished_shift_count,
            time_sheet_count,
        };
    }

    async getEmployeeData(company_id: number): Promise<any> {
        const [employees, counts] = await this.employeeService.filterAndCountFromUserCompany({}, company_id, null);
        const branches = await this.branchService.findAllWithEmployees(company_id);
        const skills = await this.skillService.findAll(company_id);
        const count = await this.employeeService.employeeCount(company_id);

        return {
            employees,
            branches,
            skills,
            total_employee_count: count?.total_employee_count ?? null,
            not_activated: count?.not_activated ?? null,
            disabled: count?.disabled ?? null,
        };
    }
}
