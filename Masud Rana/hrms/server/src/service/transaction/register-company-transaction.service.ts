import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import dayjs from 'dayjs';
import * as bcrypt from 'bcrypt';

import { EmployeeDTO } from '../dto/employee/employee.dto';
import { BranchDTO } from '../dto/branch/branch.dto';
import { CompanyDTO } from '../dto/company.dto';
import { UserDTO } from '../dto/user.dto';
import { RegisterCompanyDTO } from '../dto/auth/register-company.dto';
import { RegisterCompanyResponsenDTO } from '../dto/auth/register-company-response.dto';
import { BreakRuleCreateDTO } from '../dto/break-rule/break-rule-create.dto';

import { User } from '../../domain/user.entity';
import { Company } from '../../domain/company.entity';
import { SettingsApplication } from '../../domain/settings-application.entity';
import { SettingsShiftPlanning } from '../../domain/settings-shift-planning.entity';
import { SettingsTimeClock } from '../../domain/settings-time-clock.entity';
import { BreakRule } from '../../domain/break-rule.entity';
import { Branch } from '../../domain/branch.entity';
import { Employee } from '../../domain/employee.entity';
import { UserCompany } from '../../domain/user-company.entity';
import { SettingsLeaveAvailability } from './../../domain/settings-leave-availability.entity';

import { config } from '../../config';

import { RoleType } from '../../security/role-type';

import { defaultBranch, defaultBreakRule, DEFAULT_COMPANY_SETTINGS } from '../../database/seed-data/default.seed';

import { MailService } from './../../shared/mail/mail.service';

@Injectable()
export class RegisterCompanyTransactionService {
    constructor(private connection: Connection, private mailService: MailService) {}

    async run(newUserCompanyDTO: RegisterCompanyDTO, existingUser: UserDTO): Promise<RegisterCompanyResponsenDTO> {
        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            const { user_id, name, phone, company_type_id, application_type_id, password } = newUserCompanyDTO;
            const { firstName, lastName, email } = existingUser;

            const bycriptedPassword = await bcrypt.hash(
                password,
                config.get('hrms.security.authentication.jwt.hash-salt-or-rounds'),
            );

            await queryRunner.manager.save(User, {
                ...existingUser,
                password: bycriptedPassword,
                created_by: user_id,
                password_reset_at: Date.now(),
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                created_at: Date.now(),
            });

            const company: CompanyDTO = await queryRunner.manager.save(Company, {
                name,
                company_type_id,
                application_type_id,
                phone,
                email: existingUser.email,
                created_by: existingUser.id,
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                created_at: Date.now(),
            });

            await queryRunner.manager.save(SettingsLeaveAvailability, {
                company_id: company.id,
            });

            await queryRunner.manager.save(SettingsApplication, {
                locale_country_id: DEFAULT_COMPANY_SETTINGS.LOCALE_COUNTRY_ID,
                locale_timezone_id: DEFAULT_COMPANY_SETTINGS.LOCALE_TIME_ZONE_ID,
                company_id: company.id,
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                created_at: Date.now(),
            });

            await queryRunner.manager.save(SettingsShiftPlanning, {
                company_id: company.id,
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                created_at: Date.now(),
            });

            await queryRunner.manager.save(SettingsTimeClock, {
                company_id: company.id,
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                created_at: Date.now(),
            });

            const breakRule: BreakRuleCreateDTO = await queryRunner.manager.save(BreakRule, {
                ...defaultBreakRule,
                company_id: company.id,
                created_by: existingUser.id,
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                created_at: Date.now(),
            });

            const branch: BranchDTO = await queryRunner.manager.save(Branch, {
                ...defaultBranch,
                break_rule_id: breakRule.id,
                company_id: company.id,
                created_by: existingUser.id,
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                created_at: Date.now(),
            });

            const invitation_expiry = dayjs()
                .add(config['employee.invitation.expiry.day'], 'day')
                .unix();

            const employee: EmployeeDTO = await queryRunner.manager.save(Employee, {
                first_name: firstName,
                last_name: lastName,
                email,
                company_id: company.id,
                branch_id: branch.id,
                created_by: existingUser.id,
                invitation_expiry,
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                created_at: Date.now(),
            });

            await queryRunner.manager.save(UserCompany, {
                user_id: existingUser.id,
                company_id: company.id,
                role_id: RoleType.OWNER,
                is_owner: true,
                employee_id: employee.id,
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                created_at: Date.now(),
            });

            await this.mailService.sendRegistrationConfirmationEmail(employee);
            await queryRunner.commitTransaction();
            return { employee, company, branch };
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        } finally {
            await queryRunner.release();
        }
    }
}
