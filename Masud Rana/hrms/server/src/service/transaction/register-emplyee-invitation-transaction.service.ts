import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import * as bcrypt from 'bcrypt';

import { config } from '../../config';

import { RoleType } from './../../security/role-type';

import { User } from './../../domain/user.entity';
import { Employee } from './../../domain/employee.entity';
import { UserCompany } from './../../domain/user-company.entity';

import { InvitedNewUserRegistrationDTO } from '../dto/auth/invited-new-user-registration.dto';

@Injectable()
export class RegisterEmployeeInvitationTransactionService {
    constructor(private connection: Connection) {}

    async run(newEmployeeUser: InvitedNewUserRegistrationDTO, isNewUser = true): Promise<any> {
        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            const { password, firstName, lastName, company_id, employee_id, user_id } = newEmployeeUser;
            const bycriptedPassword = await bcrypt.hash(
                password,
                config.get('hrms.security.authentication.jwt.hash-salt-or-rounds'),
            );
            let user = null;

            if (isNewUser) {
                user = await queryRunner.manager.save(User, {
                    ...newEmployeeUser,
                    password: bycriptedPassword,
                    created_by: newEmployeeUser.id,
                    password_reset_at: Date.now(),
                    // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                    created_at: Date.now(),
                });
            }

            await queryRunner.manager.update(
                Employee,
                { id: employee_id },
                {
                    first_name: firstName,
                    last_name: lastName,
                    invitation_accepted: true,
                    invitation_expiry: null,
                    invitation_token: null,
                },
            );

            const user_company = await queryRunner.manager.save(UserCompany, {
                user_id: user ? user.id : user_id,
                company_id,
                employee_id,
                role_id: RoleType.EMPLOYEE,
                created_at: Date.now(),
            });

            await queryRunner.commitTransaction();
            return user_company;
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        } finally {
            await queryRunner.release();
        }
    }
}
