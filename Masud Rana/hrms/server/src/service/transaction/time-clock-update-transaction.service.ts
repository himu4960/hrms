import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';

import { TimeClock } from './../../domain/time-clock.entity';
import { TimeClockDTO } from './../dto/time-clock/time-clock.dto';

@Injectable()
export class TimeClockUpdateTransactionService {
    constructor(private connection: Connection) {}

    async run(timeClock: any): Promise<TimeClockDTO> {
        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();
        try {
            const { id } = timeClock;

            await queryRunner.manager.delete(TimeClock, {
                id,
            });

            await queryRunner.manager.save(TimeClock, {
                ...timeClock,
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                created_at: Date.now(),
            });

            await queryRunner.commitTransaction();
            return await queryRunner.manager.findOne(TimeClock, {
                id,
            });
        } catch (error) {
            await queryRunner.rollbackTransaction();
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        } finally {
            await queryRunner.release();
        }
    }
}
