import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions } from 'typeorm';

import { LeaveBalanceMapper } from './mapper/leave-balance.mapper';

import { LeaveBalanceRepository } from '../repository/leave-balance.repository';

import { LeaveBalanceDTO } from './dto/leave/leave-balance.dto';

@Injectable()
export class LeaveBalanceService {
    logger = new Logger('LeaveService');

    constructor(@InjectRepository(LeaveBalanceRepository) private leaveBalanceRepository: LeaveBalanceRepository) {}

    async findById(id: number): Promise<LeaveBalanceDTO | undefined> {
        return await this.leaveBalanceRepository.findOne(id);
    }

    async getAll(options: FindManyOptions<LeaveBalanceDTO>, company_id: number): Promise<[LeaveBalanceDTO[], number]> {
        options.where = { company_id };
        return await this.leaveBalanceRepository.findAndCount(options);
    }

    async save(leaveBalanceDTO: LeaveBalanceDTO, creator?: number): Promise<LeaveBalanceDTO | undefined> {
        const entity = LeaveBalanceMapper.fromDTOtoEntity(leaveBalanceDTO);
        if (creator) {
            if (!entity.created_by) {
                entity.created_by = creator;
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                entity.created_at = Date.now();
            }
            entity.updated_by = creator;
        }
        const result = await this.leaveBalanceRepository.save(entity);
        return LeaveBalanceMapper.fromEntityToDTO(result);
    }
}
