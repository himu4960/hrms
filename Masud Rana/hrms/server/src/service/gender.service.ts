import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In } from 'typeorm';

import { GenderDTO } from './dto/gender/gender.dto';
import { GenderCreateDTO } from './dto/gender/gender-create.dto';
import { GenderUpdateDTO } from './dto/gender/gender-update.dto';

import { Gender } from './../domain/gender.entity';
import { GenderTranslation } from './../domain/gender-translation.entity';

import { GenderMapper } from '../service/mapper/gender.mapper';

import { LanguageRepository } from './../repository/language.repository';
import { GenderRepository } from '../repository/gender.repository';
import { GenderTranslationRepository } from './../repository/gender-translation.repository';

const relationshipNames = [];

@Injectable()
export class GenderService {
    logger = new Logger('GenderService');

    constructor(
        @InjectRepository(LanguageRepository)
        private languageRepository: LanguageRepository,
        @InjectRepository(GenderRepository) private genderRepository: GenderRepository,
        @InjectRepository(GenderTranslationRepository) private genderTranslationRepository: GenderTranslationRepository,
    ) { }

    async findById(id: number): Promise<GenderDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.genderRepository.findOne(id, options);
        return GenderMapper.fromEntityToDTO(result);
    }

    async findByLangCode(langCode: string) {
        return await this.genderRepository
            .createQueryBuilder('gender')
            .leftJoinAndSelect('gender.translations', 'translation')
            .where('translation.language_code=:language_code', { language_code: langCode })
            .select(['gender.id as id', 'translation.name as name'])
            .getRawMany();
    }

    async findAll() {
        return await this.genderRepository
            .createQueryBuilder('gender')
            .leftJoinAndMapMany(
                'gender.translations',
                GenderTranslation,
                'translation',
                'gender.id = translation.gender_id',
            )
            .select(['gender.id', 'translation.name', 'translation.language_code'])
            .getMany();
    }

    async save(createGender: GenderCreateDTO, creator?: number) {
        const language_codes = createGender.translations.map(gender => gender.language_code);

        const languageResult = await this.languageRepository.find({ where: { code: In(language_codes) } });

        if (languageResult.length > 0) {
            const result = await this.genderRepository
                .createQueryBuilder()
                .insert()
                .into(Gender)
                .values({})
                .execute();

            const gender_id = result.raw.insertId;

            createGender.translations.forEach(async genderTranslation => {
                const entity = GenderMapper.fromDTOtoEntity(genderTranslation);
                if (creator) {
                    if (!entity.created_by) {
                        entity.created_by = creator;
                    }
                    entity.updated_by = creator;
                }

                const { name, language_code } = genderTranslation;

                const languageResult = await this.languageRepository.findOne(language_code);

                if (languageResult) {
                    const genderTranlationResult = await this.genderTranslationRepository.findOne({
                        where: { name },
                    });
                    if (!genderTranlationResult) {
                        return await this.genderTranslationRepository
                            .createQueryBuilder()
                            .insert()
                            .into(GenderTranslation)
                            .values({ name, language_code, gender_id })
                            .execute();
                    } else {
                        throw new HttpException('Gender already exsist', HttpStatus.FOUND);
                    }
                } else {
                    throw new HttpException('Language not found!', HttpStatus.NOT_FOUND);
                }
            });
        } else {
            throw new HttpException('Language not found!', HttpStatus.NOT_FOUND);
        }
    }

    async update(id: number, updateGender: GenderUpdateDTO, updater?: number) {
        updateGender.translations.forEach(async genderTranslation => {
            const entity = GenderMapper.fromDTOtoEntity(genderTranslation);
            if (updater) {
                entity.updated_by = updater;
                entity.updated_at = Date.now();
            }
            const { name, language_code } = genderTranslation;

            const languageResult = await this.languageRepository.findOne({ where: { code: language_code } });

            if (languageResult) {
                const isExist = await this.genderTranslationRepository
                    .createQueryBuilder('genderTranslation')
                    .where('gender_id=:id', { id })
                    .andWhere('language_code=:language_code', { language_code })
                    .getOne();

                if (isExist) {
                    await this.genderTranslationRepository
                        .createQueryBuilder()
                        .update(GenderTranslation)
                        .set({
                            name,
                        })
                        .where('gender_id=:id', { id })
                        .andWhere('language_code=:language_code', { language_code })
                        .execute();
                } else {
                    await this.genderTranslationRepository
                        .createQueryBuilder()
                        .insert()
                        .into(GenderTranslation)
                        .values([
                            {
                                name,
                                language_code,
                                gender_id: id,
                            },
                        ])
                        .execute();
                }
            } else {
                throw new HttpException('Language not found!', HttpStatus.NOT_FOUND);
            }
        });
        return;
    }

    async deleteById(id: number): Promise<GenderDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('Gender is not found!', HttpStatus.NOT_FOUND);
        }
        await this.genderTranslationRepository.delete(id);
        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Gender cannot be deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }
}
