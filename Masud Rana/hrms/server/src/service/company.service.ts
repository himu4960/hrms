import { Injectable, HttpException, HttpStatus, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions } from 'typeorm';
import sharp from 'sharp';

import { CompanyDTO } from '../service/dto/company.dto';
import { UserCompanyDTO } from './dto/user-company/user-company.dto';
import { UploadLogoDTO } from './dto/upload-logo.dto';

import { CompanyMapper } from '../service/mapper/company.mapper';

import { CompanyRepository } from '../repository/company.repository';
import { UserCompanyRepository } from '../repository/user-company.repository';

import { CreateCompanyTransactionService } from './transaction/create-company-transaction.service';
import { MediaService } from './media.service';

import { FILE_UPLOAD_RESPONSE, REGISTER_COMPANY_RESPONSE, USER_RESPONSE } from '../shared/constant/response-message';
import { ELogoSize, ELogoTypeOnSize } from '../domain/enum/upload-logo.enum';

const relationshipNames = ['companyType', 'applicationType'];

@Injectable()
export class CompanyService {
    logger = new Logger('CompanyService');

    constructor(
        @InjectRepository(CompanyRepository) private companyRepository: CompanyRepository,
        @InjectRepository(UserCompanyRepository) private userCompanyRepository: UserCompanyRepository,
        private readonly createCompanyTransactionService: CreateCompanyTransactionService,
        private mediaService: MediaService,
    ) {}

    async findById(id: number): Promise<CompanyDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.companyRepository.findOne(id, options);
        return CompanyMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<CompanyDTO>): Promise<CompanyDTO | undefined> {
        const result = await this.companyRepository.findOne(options);
        return CompanyMapper.fromEntityToDTO(result);
    }

    async findByUserId(userId: number): Promise<UserCompanyDTO[]> {
        if (!userId) {
            throw new NotFoundException(USER_RESPONSE.READ.NOT_FOUND);
        }

        const options = {
            where: { user_id: userId },
        };
        return await this.userCompanyRepository
            .createQueryBuilder('userCompany')
            .leftJoinAndSelect('userCompany.company', 'company')
            .leftJoinAndSelect('company.companyType', 'companyType')
            .leftJoinAndSelect('company.applicationType', 'applicationType')
            .leftJoinAndSelect('userCompany.employee', 'employee')
            .leftJoinAndSelect('userCompany.role', 'role')
            .addSelect('userCompany.company_id')
            .where('userCompany.user_id = :userId', { userId })
            .getMany();
    }

    async save(companyDTO: CompanyDTO, userId: number): Promise<any | undefined> {
        return await this.createCompanyTransactionService.run(companyDTO, userId);
    }

    async update(companyDTO: CompanyDTO, updater?: number): Promise<CompanyDTO | undefined> {
        const entity = CompanyMapper.fromDTOtoEntity(companyDTO);
        if (updater) {
            entity.updated_by = updater;
            entity.updated_at = Date.now();
        }
        const result = await this.companyRepository.save(entity);
        return CompanyMapper.fromEntityToDTO(result);
    }

    async deleteById(id: number): Promise<CompanyDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('Company is not found!', HttpStatus.NOT_FOUND);
        }
        await this.companyRepository.delete(id);
        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Company cannot be deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }

    async uploadLogo(originalFile: any, uploadFileDto: UploadLogoDTO): Promise<CompanyDTO> {
        try {
            const { company_id, user_id } = uploadFileDto;
            const company = await this.findById(company_id);
            if (company) {
                const largeFile = await sharp(originalFile.buffer)
                    .resize(ELogoSize.LARGE_FILE)
                    .toBuffer();
                const mediumFile = await sharp(originalFile.buffer)
                    .resize(ELogoSize.MEDIUM_FILE)
                    .toBuffer();
                const smallFile = await sharp(originalFile.buffer)
                    .resize(ELogoSize.SMALL_FILE)
                    .toBuffer();

                const files = [
                    { logo_type: ELogoTypeOnSize.LARGE, buffer: largeFile },
                    { logo_type: ELogoTypeOnSize.MEDIUM, buffer: mediumFile },
                    { logo_type: ELogoTypeOnSize.SMALL, buffer: smallFile },
                ];

                const response = await this.mediaService.uploadLogo(originalFile, files);

                const logoUrlLarge = response[ELogoTypeOnSize.LARGE]?.Location;
                const logoUrlMedium = response[ELogoTypeOnSize.MEDIUM]?.Location;
                const logoUrlSmall = response[ELogoTypeOnSize.SMALL]?.Location;

                if (logoUrlLarge && logoUrlMedium && logoUrlSmall) {
                    company.updated_by = user_id;
                    company.logo_url_large = logoUrlLarge;
                    company.logo_url_medium = logoUrlMedium;
                    company.logo_url_small = logoUrlSmall;

                    return await this.companyRepository.save(company);
                } else {
                    throw new HttpException(FILE_UPLOAD_RESPONSE.ERROR, HttpStatus.NOT_MODIFIED);
                }
            }
            throw new HttpException(REGISTER_COMPANY_RESPONSE.READ.NOT_FOUND, HttpStatus.NOT_MODIFIED);
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.NOT_MODIFIED);
        }
    }
}
