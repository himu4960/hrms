import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';

import { AnnouncementDTO } from './dto/announcement/announcement.dto';
import { AnnouncementCreateDTO } from './dto/announcement/announcement-create.dto';
import { AnnouncementUpdateDTO } from './dto/announcement/announcement-update.dto';

import { AnnouncementRepository } from '../repository/announcement.repository';

import { AnnouncementMapper } from '../service/mapper/announcement.mapper';

import { NotificationService } from '../fcm/notification.service';

const relationshipNames = [];

@Injectable()
export class AnnouncementService {
    logger = new Logger('AnnouncementService');

    constructor(
        @InjectRepository(AnnouncementRepository) private announcementRepository: AnnouncementRepository,
        private notificationService: NotificationService,
    ) {}

    async findById(id: number): Promise<AnnouncementDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.announcementRepository.findOne(id, options);
        return AnnouncementMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<AnnouncementDTO>): Promise<AnnouncementDTO | undefined> {
        const result = await this.announcementRepository.findOne(options);
        return AnnouncementMapper.fromEntityToDTO(result);
    }

    async findAndCount(
        options: FindManyOptions<AnnouncementDTO>,
        company_id: number,
    ): Promise<[AnnouncementDTO[], number]> {
        return await this.announcementRepository
            .createQueryBuilder('announcement')
            .where('announcement.company_id = :company_id', { company_id })
            .addSelect('announcement.created_at')
            .skip(options.skip)
            .take(options.take)
            .orderBy('announcement.id', 'DESC')
            .getManyAndCount();
    }

    async getAll(company_id: number): Promise<AnnouncementDTO[]> {
        return await this.announcementRepository
            .createQueryBuilder('announcement')
            .where('announcement.company_id = :company_id', { company_id })
            .addSelect('announcement.created_at')
            .orderBy('announcement.id', 'DESC')
            .getMany();
    }

    async save(
        announcementCreateDTO: AnnouncementCreateDTO,
        company_id: number,
        creator?: number,
    ): Promise<AnnouncementDTO | undefined> {
        try {
            const result = await this.announcementRepository.save({
                ...announcementCreateDTO,
                company_id,
                created_by: creator,
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                created_at: Date.now(),
            });

            const deviceTokens = await this.notificationService.getDeviceTokens({
                where: {
                    company_id,
                },
            });

            if (deviceTokens.length) {
                const payload = {
                    notification: {
                        title: announcementCreateDTO.title,
                        body: announcementCreateDTO.description ? announcementCreateDTO.description : '',
                    },
                };
               await this.notificationService.sendNotification(deviceTokens, payload);
            }

            return AnnouncementMapper.fromEntityToDTO(result);
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async update(announcementUpdateDTO: AnnouncementUpdateDTO, updater?: number): Promise<AnnouncementDTO | undefined> {
        await this.announcementRepository.update(
            { id: announcementUpdateDTO.id },
            { ...announcementUpdateDTO, updated_by: updater, updated_at: Date.now() },
        );
        return this.findById(announcementUpdateDTO.id);
    }

    async deleteById(id: number): Promise<AnnouncementDTO | undefined> {
        const findAnnouncement = await this.findById(id);
        await this.announcementRepository.delete(id);
        const entityFind = await this.findById(id);
        if (entityFind) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
        }
        return findAnnouncement;
    }
}
