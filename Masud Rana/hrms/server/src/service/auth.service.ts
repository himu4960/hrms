import { ConfigService } from '@nestjs/config';
import { Injectable, HttpException, HttpStatus, Logger, ForbiddenException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { FindManyOptions, getRepository } from 'typeorm';
import dayjs from 'dayjs';
import * as bcrypt from 'bcrypt';
import { v4 as uuidv4 } from 'uuid';

import { config } from '../config';

import { REGISTER_RESPONSE } from './../shared/constant/response-message';

import { RoleType } from './../security/role-type';

import { UserDTO } from './dto/user.dto';
import { UserLoginDTO } from '../service/dto/user-login.dto';
import { RegisterCompanyDTO } from './dto/auth/register-company.dto';
import { UserRegistrationDTO } from './dto/auth/user-registration.dto';
import { RegisterCompanyResponsenDTO } from './dto/auth/register-company-response.dto';
import { BusinessChangeDTO } from './dto/business-change.dto';
import { RefreshTokenDTO } from './dto/auth/refresh-token.dto';
import { PasswordResetDTO } from './dto/password-reset.dto';
import { UserCompanyDTO } from './dto/user-company/user-company.dto';
import { RoleUpdateDTO } from './dto/role-update.dto';

import { UserService } from '../service/user.service';
import { RoleService } from '../service/role.service';
import { NotificationService } from '../fcm/notification.service';
import { CompanyTypeService } from './company-type.service';
import { ApplicationTypeService } from './application-type.service';
import { JWTService } from './jwt.service';
import { RegisterCompanyTransactionService } from './transaction/register-company-transaction.service';
import { MailService } from './../shared/mail/mail.service';

import { CompanyPayload, UserPayload } from '../security/payload.interface';

import { CompanyRepository } from './../repository/company.repository';
import { EmployeeRepository } from './../repository/employee.repository';
import { UserCompanyRepository } from './../repository/user-company.repository';
import { UserDeviceRepository } from '../repository/user-device.repository';

import { LOGIN_RESPONSE, LOGIN_RESPONSE_CODE, USER_RESPONSE } from '../shared/constant/response-message';

import { TABLE_USER_PASSWORD } from '../shared/constant/database.constant';
import { UserCompanyMapper } from './mapper/user-company.mapper';
import { UserDeviceMapper } from './mapper/user-device.mapper';

import { UserCompany } from 'src/domain/user-company.entity';

@Injectable()
export class AuthService {
    logger = new Logger('AuthService');
    accessTokenExpiryTime = this.config.get('JWT_ACCESS_TOKEN_EXPIRY_TIME');
    refreshTokenExpiryTime = this.config.get('JWT_REFRESH_TOKEN_EXPIRY_TIME');

    constructor(
        private readonly jwtService: JwtService,
        @InjectRepository(UserCompanyRepository) private userCompanyRepository: UserCompanyRepository,
        @InjectRepository(EmployeeRepository) private employeeRepository: EmployeeRepository,
        @InjectRepository(CompanyRepository) private companyRepository: CompanyRepository,
        @InjectRepository(UserDeviceRepository) private userDeviceRepository: UserDeviceRepository,
        private userService: UserService,
        private roleService: RoleService,
        private notificationService: NotificationService,
        private companyTypeService: CompanyTypeService,
        private applicationTypeService: ApplicationTypeService,
        private registerCompanyTransactionService: RegisterCompanyTransactionService,
        private readonly JwtService: JWTService,
        private mailService: MailService,
        private config: ConfigService,
    ) {}

    async sendConfirmationEmail(token: string): Promise<boolean> {
        const today = dayjs();
        const result = await this.userCompanyRepository
            .createQueryBuilder('userCompany')
            .leftJoinAndSelect('userCompany.employee', 'employee')
            .where('employee.invitation_token = :token', { token })
            .andWhere('userCompany.is_owner = :is_owner', { is_owner: true })
            .addSelect('employee.company_id')
            .getOne();

        if (!result) {
            throw new NotFoundException('Wrong token!');
        }

        const newExpiryDate = today.add(config['employee.invitation.expiry.day'], 'day').unix();

        const uuid = uuidv4();
        await this.employeeRepository.update(
            { id: result.employee.id },
            { invitation_token: uuid, invitation_expiry: newExpiryDate },
        );
        const afterUpdatedEmployee = await this.employeeRepository.findOne(result.employee.id);
        await this.mailService.sendRegistrationConfirmationEmail(afterUpdatedEmployee);
        return true;
    }

    async confirmEmployee(token: string): Promise<boolean> {
        const today = dayjs().unix();
        const employee = await this.employeeRepository
            .createQueryBuilder('employee')
            .where('employee.invitation_token = :token', { token })
            .select([
                'employee.id',
                'employee.first_name',
                'employee.last_name',
                'employee.email',
                'employee.invitation_accepted',
                'employee.invitation_expiry',
                'employee.invitation_token',
            ])
            .addSelect(['employee.company_id', 'employee.branch_id'])
            .getOne();

        if (!employee) {
            throw new NotFoundException('Wrong token!');
        }

        if (employee.invitation_accepted) {
            throw new HttpException(REGISTER_RESPONSE.ALREADY_ACCEPTED, HttpStatus.ACCEPTED);
        }

        const invitation_expiry = Number(employee.invitation_expiry);
        const diff = invitation_expiry - today;

        if (diff >= 0) {
            const user = await this.userService.findByFields({ where: { email: employee.email } });

            if (!user) {
                throw new NotFoundException('User not found!');
            }

            await this.employeeRepository.update({ id: employee.id }, { invitation_accepted: true, is_active: true });

            await this.userCompanyRepository.save({
                user_id: user.id,
                employee_id: employee.id,
                company_id: employee.company_id,
                branch_id: employee.branch_id,
                is_active: true,
                role_id: RoleType.EMPLOYEE,
            });
            return true;
        }

        throw new HttpException(REGISTER_RESPONSE.EXPIRED, HttpStatus.UNAUTHORIZED);
    }

    async confirmRegistration(token: string): Promise<boolean> {
        const today = dayjs().unix();
        const result = await this.userCompanyRepository
            .createQueryBuilder('userCompany')
            .leftJoinAndSelect('userCompany.employee', 'employee')
            .where('employee.invitation_token = :token', { token })
            .andWhere('userCompany.is_owner = :is_owner', { is_owner: true })
            .addSelect('employee.company_id')
            .getOne();

        if (!result) {
            throw new NotFoundException('Wrong token!');
        }

        if (result.employee.invitation_accepted) {
            throw new ForbiddenException(REGISTER_RESPONSE.ALREADY_ACCEPTED);
        }

        const invitation_expiry = Number(result.employee.invitation_expiry);
        const diff = invitation_expiry - today;

        if (diff >= 0) {
            await this.employeeRepository.update({ id: result.employee.id }, { invitation_accepted: true });
            await this.companyRepository.update({ id: result.employee.company_id }, { status: true });
            return true;
        }

        throw new ForbiddenException(REGISTER_RESPONSE.EXPIRED);
    }

    async registerCompany(newUserCompanyDTO: RegisterCompanyDTO): Promise<RegisterCompanyResponsenDTO> {
        const { user_id, company_type_id, application_type_id, name } = newUserCompanyDTO;

        const role = await this.roleService.findById(RoleType.OWNER);
        if (!role) {
            throw new HttpException('Role not found!', HttpStatus.NOT_FOUND);
        }

        const companyType = await this.companyTypeService.findById(company_type_id);
        if (!companyType) {
            throw new HttpException('Company Type not found!', HttpStatus.NOT_FOUND);
        }

        const applicationType = await this.applicationTypeService.findById(application_type_id);
        if (!applicationType) {
            throw new HttpException('Application Type not found!', HttpStatus.NOT_FOUND);
        }

        const existingUser: UserDTO = await this.userService.findById(user_id);

        if (!existingUser) {
            throw new HttpException('User not found!', HttpStatus.NOT_FOUND);
        }

        const existingCompany = await this.userCompanyRepository
            .createQueryBuilder('userCompany')
            .leftJoinAndSelect('userCompany.user', 'user')
            .leftJoinAndSelect('userCompany.company', 'company')
            .where('userCompany.user_id = :user_id', { user_id })
            .andWhere('user.email = company.email')
            .andWhere('company.name = :name', { name })
            .getOne();

        if (existingCompany) {
            throw new HttpException('You already have a company with same name!', HttpStatus.FOUND);
        }

        return await this.registerCompanyTransactionService.run(newUserCompanyDTO, existingUser);
    }

    async registerNewUser(newUserDTO: UserRegistrationDTO): Promise<any> {
        const { fullName, email } = newUserDTO;
        const lastIndex = fullName.lastIndexOf(' ');
        const newUserFirstName = lastIndex > 0 ? fullName.trim().slice(0, lastIndex) : fullName;
        const newUserLastName = lastIndex > 0 ? fullName.trim().slice(lastIndex + 1) : '';
        const newUsername = email.split('@')[0];

        const existingUser = await this.userService.findByFields({
            where: { email },
            select: ['id', 'firstName', 'lastName', 'email', 'username'],
        });

        if (existingUser) {
            const existingUserCompany = await this.userCompanyRepository.find({ where: { user_id: existingUser.id } });
            if (existingUserCompany.length) {
                throw new HttpException('User already exsist!', HttpStatus.FOUND);
            }
            return existingUser;
        }

        const newUser = await this.userService.save({
            firstName: newUserFirstName,
            lastName: newUserLastName,
            username: newUsername,
            email,
            // Created_at default value is fixed in base.entity so we need to store current time in every post method.
            created_at: Date.now(),
        });
        return newUser;
    }

    async login(userLogin: UserLoginDTO): Promise<any> {
        const loginUserNameOrEmail = userLogin.username;
        const loginPassword = userLogin.password;
        let existingUser: UserDTO;
        let passwordExists: boolean;

        if (loginUserNameOrEmail.includes('@')) {
            existingUser = await this.userService.findByFields({ where: { email: loginUserNameOrEmail } });
        } else {
            existingUser = await this.userService.findByFields({ where: { username: loginUserNameOrEmail } });
        }

        if (!existingUser) {
            throw new NotFoundException('User not found!');
        }

        const user = await this.findUserWithAuthById(existingUser.id);

        const validPassword = !!existingUser && (await bcrypt.compare(loginPassword, user.password));
        if (existingUser && !validPassword) {
            const userPasswordRepository = getRepository(TABLE_USER_PASSWORD);
            const options = {
                where: { user_id: existingUser.id },
            };
            const userExistingPasswords: any[] = await userPasswordRepository.find(options);

            for (let index = 0; index < userExistingPasswords?.length; index++) {
                passwordExists = await bcrypt.compare(loginPassword, userExistingPasswords[index].password);
                if (passwordExists) {
                    break;
                }
            }
        }
        const payload: UserPayload = {
            user_id: user.id,
            user_name: user.username,
            email: user.email,
        };

        if (!validPassword && passwordExists) {
            return { code: LOGIN_RESPONSE_CODE.OLD_PASSWORD_CODE, message: LOGIN_RESPONSE.OLD_PASSWORD_ALERT };
        } else if (!existingUser || (!validPassword && !passwordExists)) {
            return { code: LOGIN_RESPONSE_CODE.PASSWORD_EXPIRY_CODE, message: LOGIN_RESPONSE.INVALID_CREDENTIAL_ERROR };
        }
        /* eslint-disable */
        return {
            token: this.jwtService.sign(payload),
            refresh_token: this.jwtService.sign(payload, { expiresIn: this.refreshTokenExpiryTime }),
        };
    }

    /* eslint-enable */
    async validateUser(payload: UserPayload): Promise<UserDTO | undefined> {
        return await this.findUserWithAuthById(payload.user_id);
    }

    async findUserWithAuthById(userId: number): Promise<UserDTO | undefined> {
        const userDTO: UserDTO = await this.userService.findUserWithCompaniesById(userId);
        return userDTO;
    }

    async getAccount(userId: number): Promise<UserDTO | undefined> {
        const userDTO: UserDTO = await this.findUserWithAuthById(userId);
        if (!userDTO) {
            return;
        }
        return userDTO;
    }

    async getAccounts(): Promise<UserDTO | undefined> {
        const userDTO = await this.userService.findAll();
        if (!userDTO) {
            return;
        }
        return userDTO;
    }

    async getRole(roleId: number): Promise<any | undefined> {
        const role = await this.roleService.findById(roleId);
        if (!role && !role.menus) {
            return;
        }
        return JSON.parse(role.menus);
    }

    async changeBusiness(businessChange: BusinessChangeDTO): Promise<any> {
        const { company_id, user_id, device_token } = businessChange;

        const userCompany = await this.userCompanyRepository.findOne({
            where: { company_id, user_id },
        });

        if (!userCompany.employee.invitation_accepted) {
            throw new ForbiddenException('You have to confirm your account!');
        }

        if (!userCompany.is_active) {
            throw new ForbiddenException('Your company account has been disabled');
        }

        const companySettings = await this.companyRepository
            .createQueryBuilder('company')
            .leftJoinAndSelect('company.settings_time_clock', 'settings_time_clock')
            .leftJoinAndSelect('company.settings_application', 'settings_application')
            .leftJoinAndSelect('settings_application.time_zone', 'time_zone')
            .leftJoinAndSelect('company.settings_shift_planning', 'settings_shift_planning')
            .leftJoinAndSelect('company.settings_leave_availability', 'settings_leave_availability')
            .select([
                'company.id',
                'company.name',
                'settings_time_clock',
                'settings_application',
                'settings_shift_planning',
                'settings_leave_availability',
                'time_zone',
            ])
            .where('company.id=:company_id', { company_id })
            .getOne();

        const menu = await this.getRole(userCompany.role_id);

        if (!userCompany) {
            throw new HttpException('Invalid Company name!', HttpStatus.BAD_REQUEST);
        }

        const employee_id = userCompany.employee_id;
        const role_id = userCompany.role_id;

        const payload: CompanyPayload = {
            user_id: user_id,
            role_id: role_id,
            company_id: userCompany.company_id,
            employee_id: employee_id,
            is_employee: userCompany.role.is_employee,
        };

        const refreshToken = this.jwtService.sign(payload, { expiresIn: this.refreshTokenExpiryTime });
        const userCompanyEntity = UserCompanyMapper.fromDTOtoEntity(userCompany);

        await this.userCompanyRepository.save({ ...userCompanyEntity, refresh_token: refreshToken });

        const options = {
            where: {
                company_id,
                employee_id,
                device_token,
            },
        };

        const tokenExists = await this.userDeviceRepository.findOne(options);

        if (device_token && !tokenExists) {
            const entity = UserDeviceMapper.fromDTOtoEntity({ company_id, employee_id, role_id, device_token });
            await this.userDeviceRepository.save(entity);
        }

        return {
            token: this.jwtService.sign(payload, { expiresIn: this.accessTokenExpiryTime }),
            refresh_token: refreshToken,
            settings: companySettings,
            menu,
        };
    }

    async generateAuthToken(refreshToken: RefreshTokenDTO) {
        const { company_id, user_id } = refreshToken;

        const userCompany = await this.userCompanyRepository.findOne({
            where: { company_id, user_id },
        });

        const payload: CompanyPayload = {
            user_id: user_id,
            role_id: userCompany.role_id,
            company_id: userCompany.company_id,
            is_employee: userCompany.role.is_employee,
        };

        const userCompanyEntity = UserCompanyMapper.fromDTOtoEntity(userCompany);
        const newRefreshToken = this.jwtService.sign(payload, { expiresIn: this.refreshTokenExpiryTime });

        await this.userCompanyRepository.save({ ...userCompanyEntity, refresh_token: newRefreshToken });

        return {
            token: this.jwtService.sign(payload, { expiresIn: this.accessTokenExpiryTime }),
            refresh_token: newRefreshToken,
        };
    }

    async updateRole(roleUpdateDTO: RoleUpdateDTO, company_id: number): Promise<UserCompanyDTO> {
        try {
            const { employee_id, role_id } = roleUpdateDTO;
            const options = {
                where: { employee_id, company_id },
            };
            const userCompany: UserCompany = await this.userCompanyRepository.findOne(options);
            const entity = {
                role_id: role_id,
                updated_at: Date.now(),
            };

            await this.userCompanyRepository.update(userCompany.id, entity);
            const result = await this.userCompanyRepository.findOne(userCompany.id);

            const deviceTokens = await this.notificationService.getDeviceTokens({
                where: {
                    company_id,
                    employee_id,
                },
            });

            if (deviceTokens.length) {
                const payload = {
                    notification: {
                        title: 'Your role updated',
                        body: `You have been nominated for ${result.role.name}`,
                    },
                };
                await this.notificationService.sendNotification(deviceTokens, payload);
            }
            return result;
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async changePassword(userLogin: UserDTO, currentClearTextPassword: string, newPassword: string): Promise<UserDTO> {
        const existingUser: UserDTO = await this.userService.findByFields({ where: { username: userLogin.username } });
        if (!existingUser) {
            throw new HttpException('Invalid login name!', HttpStatus.BAD_REQUEST);
        }

        if (!(await bcrypt.compare(currentClearTextPassword, existingUser.password))) {
            throw new HttpException('Invalid password!', HttpStatus.BAD_REQUEST);
        }
        existingUser.password = newPassword;
        return await this.userService.save(existingUser, userLogin.id, true);
    }

    async resetPassword(existingUser: UserDTO, newPassword: string): Promise<any> {
        let passwordExists: boolean;

        if (existingUser) {
            const userPasswordRepository = getRepository(TABLE_USER_PASSWORD);
            const options = {
                where: { user_id: existingUser.id },
            };
            const userExistingPasswords: any[] = await userPasswordRepository.find(options);

            for (let index = 0; index < userExistingPasswords?.length; index++) {
                passwordExists = await bcrypt.compare(newPassword, userExistingPasswords[index].password);
                if (passwordExists) {
                    break;
                }
            }

            if (passwordExists) {
                throw new ForbiddenException({
                    code: LOGIN_RESPONSE_CODE.OLD_PASSWORD_CODE,
                    message: LOGIN_RESPONSE.OLD_PASSWORD_ALERT,
                });
            }

            existingUser.password = newPassword;
            existingUser.password_reset_at = Date.now();
            return await this.userService.save(existingUser, existingUser.id, true);
        }
        throw new ForbiddenException({ message: USER_RESPONSE.READ.NOT_FOUND });
    }

    async forgotPassword(email: string) {
        const user = await this.userService.findByEmail(email);

        if (!user) {
            throw new ForbiddenException('User not found!');
        }

        const secret = config['hrms.security.authentication.jwt.base64-secret'] + user.password;

        const payload = {
            email: user.email,
            id: user.id,
        };

        const password_recovery_token = this.jwtService.sign(payload, { secret, expiresIn: '15min' });

        const link = `${process.env.CLIENT_URL}/forgot-password/reset/${user.id}/${password_recovery_token}`;

        await this.mailService.sendForgotPasswordOneTimeEmail(user, link);

        return { password_recovery_token };
    }

    async passwordRecoveryTokenVerify(id: number, token: string): Promise<any> {
        const user = await this.userService.findById(id);

        if (!user) {
            throw new ForbiddenException('User not found!');
        }

        const secret = config['hrms.security.authentication.jwt.base64-secret'] + user.password;

        const payload = this.jwtService.verify(token, { secret });

        return payload;
    }

    async resetForgotPassword(userId: number, token: string, passwordResetDTO: PasswordResetDTO): Promise<UserDTO> {
        const user = await this.userService.findById(userId);

        if (!user) {
            throw new ForbiddenException('User not found!');
        }

        const secret = config['hrms.security.authentication.jwt.base64-secret'] + user.password;

        const payload = this.jwtService.verify(token, { secret });

        if (!payload) {
            throw new ForbiddenException('Invalid Token!');
        }

        const newPassword = passwordResetDTO.newPassword;
        const bycriptedPassword = await bcrypt.hash(
            newPassword,
            config.get('hrms.security.authentication.jwt.hash-salt-or-rounds'),
        );

        return await this.userService.update({ ...user, password: bycriptedPassword });
    }

    async updateUserSettings(userLogin: number, newUserInfo: UserDTO): Promise<UserDTO> {
        const existingUser: UserDTO = await this.userService.findByFields({ where: { username: userLogin } });
        if (!existingUser) {
            throw new HttpException('Invalid login name!', HttpStatus.BAD_REQUEST);
        }
        const existingUserEmail: UserDTO = await this.userService.findByFields({ where: { email: newUserInfo.email } });
        if (existingUserEmail && newUserInfo.email !== existingUser.email) {
            throw new HttpException('Email is already in use!', HttpStatus.BAD_REQUEST);
        }

        existingUser.firstName = newUserInfo.firstName;
        existingUser.lastName = newUserInfo.lastName;
        existingUser.email = newUserInfo.email;
        existingUser.langKey = newUserInfo.langKey;
        return await this.userService.save(existingUser, userLogin);
    }

    async getAllUsers(options: FindManyOptions<UserDTO>) {
        return await this.userService.findAndCount(options);
    }

    async checkAuthorization(data: any) {
        const user: any = await this.JwtService.decodedJWTToken(data.authorization);
        return await this.roleService.rolePermission({ user: user, route: data.route });
    }
}
