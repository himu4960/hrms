import { Injectable, HttpException, HttpStatus, Logger, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';

import { RoleDTO } from '../service/dto/role.dto';

import { RoleMapper } from '../service/mapper/role.mapper';

import { RoleRepository } from '../repository/role.repository';

import { CasbinRolePermission, CasbinService } from '../shared/nestjs-casbin';

const relationshipNames = ['applicationType'];

@Injectable()
export class RoleService {
    logger = new Logger('RoleService');

    constructor(
        @InjectRepository(RoleRepository) private roleRepository: RoleRepository,
        private readonly casbinService: CasbinService,
    ) {}

    async findById(id: number): Promise<RoleDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.roleRepository.findOne(id, options);
        return RoleMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<RoleDTO>): Promise<RoleDTO | undefined> {
        const result = await this.roleRepository.findOne(options);
        return RoleMapper.fromEntityToDTO(result);
    }

    async findAndCount(options: FindManyOptions<RoleDTO>): Promise<[RoleDTO[], number]> {
        options.relations = relationshipNames;
        const resultList = await this.roleRepository.findAndCount(options);
        const roleDTO: RoleDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach(role => roleDTO.push(RoleMapper.fromEntityToDTO(role)));
            resultList[0] = roleDTO;
        }
        return resultList;
    }

    async save(roleDTO: RoleDTO, creator?: number): Promise<RoleDTO | undefined> {
        const entity = RoleMapper.fromDTOtoEntity(roleDTO);
        if (creator) {
            if (!entity.created_by) {
                entity.created_by = creator;
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                entity.created_at = Date.now();
            }
            entity.updated_by = creator;
        }
        const result = await this.roleRepository.save(entity);
        if (entity && entity.menus?.length > 0) {
            await this.addPolicyMenuPermission(entity);
        }
        return RoleMapper.fromEntityToDTO(result);
    }

    async update(roleDTO: RoleDTO, updater?: number): Promise<RoleDTO | undefined> {
        const entity = RoleMapper.fromDTOtoEntity(roleDTO);
        if (updater) {
            entity.updated_by = updater;
            entity.updated_at = Date.now();
        }
        const result = await this.roleRepository.save(entity);
        const role = RoleMapper.fromEntityToDTO(result);
        await this.updateRoleAndPolicy(`${role.id}`);
        await this.addPolicyMenuPermission(role);
        return role;
    }

    async deleteById(id: number): Promise<RoleDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('Role is not found!', HttpStatus.NOT_FOUND);
        }

        await this.updateRoleAndPolicy(`${id}`);
        await this.roleRepository.delete(id);

        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Role cannot be deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }

    async rolePermission(data: any): Promise<boolean> {
        const role = data.user.role_id;
        const path = data.route.path + '/';

        const method = Object.keys(data.route.methods)[0].toUpperCase();

        const roleIsEnabled = await this.checkRoleIsEnabled(role);
        const permissionAllowed = await this.casbinService.checkPermission(role, path, method);
        return roleIsEnabled && permissionAllowed;
    }

    private async checkRoleIsEnabled(id: number): Promise<any> {
        try {
            const result = await this.findById(id);
            const role = RoleMapper.fromEntityToDTO(result);
            return role?.is_active === true ? true : false;
        } catch (error) {
            return false;
        }
    }

    private getMethod(data: any): string {
        const method = [];
        if (data.can_view) method.push('(GET)');
        if (data.can_add) method.push('(POST)');
        if (data.can_edit) method.push('(PUT)');
        if (data.can_change_password) method.push('(PUT)');
        if (data.can_delete) method.push('(DELETE)');
        if (data.can_approve) method.push('(PATCH)');
        if (data.can_reject) method.push('(PATCH)');
        if (data.can_publish) method.push('(PATCH)');

        if (method.length) {
            if (method.length === 1) {
                return method[0].slice(1, -1);
            } else {
                return method.join('|');
            }
        } else {
            return '';
        }
    }

    private async addPolicyMenuPermission(role: RoleDTO): Promise<any> {
        const menus = JSON.parse(role.menus);
        if (!!menus && menus.length) {
            const policy: CasbinRolePermission[] = menus
                ?.filter((m: any) => !!m.end_point)
                .map((menu: any) => ({
                    roleName: `${role.id}`,
                    path: menu.end_point.includes('/*') ? menu.end_point : `${menu.end_point}/*`,
                    methods: this.getMethod(menu),
                    menu: menu.name,
                }));
            return await this.addMultiplePolicy(policy);
        } else {
            throw new BadRequestException('Not Found Menus');
        }
    }

    private async addMultiplePolicy(data: any[]): Promise<Object> {
        try {
            for (let index = 0; index < data.length; index++) {
                await this.casbinService.addPolicy(data[index]);
            }
        } catch (error) {
            return error;
        }
    }

    private async updateRoleAndPolicy(roleName: string): Promise<any> {
        try {
            await this.casbinService.updatePolicy(roleName);
        } catch (error) {
            throw new BadRequestException(error);
        }
    }
}
