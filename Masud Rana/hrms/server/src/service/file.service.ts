import { Injectable, HttpException, HttpStatus, Logger, ForbiddenException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';

import { FileDTO } from '../service/dto/file.dto';

import { FileMapper } from '../service/mapper/file.mapper';

import { FileRepository } from '../repository/file.repository';

import { MediaService } from './media.service';
import { NotificationService } from '../fcm/notification.service';

import { S3ResponseDTO } from './dto/s3-response.dto';

const relationshipNames = [];

@Injectable()
export class FileService {
    logger = new Logger('FileService');

    constructor(
        @InjectRepository(FileRepository) private fileRepository: FileRepository,
        private mediaService: MediaService,
        private notificationService: NotificationService,
    ) {}

    async findById(id: number): Promise<FileDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.fileRepository.findOne(id, options);
        return FileMapper.fromEntityToDTO(result);
    }

    async findAndCount(options: FindManyOptions<FileDTO>, company_id: number): Promise<[FileDTO[], number]> {
        return await this.fileRepository
            .createQueryBuilder('file')
            .where('file.company_id = :company_id', { company_id })
            .addSelect(['file.created_at', 'file.updated_at'])
            .take(options.take)
            .skip(options.skip)
            .orderBy('file.id', 'DESC')
            .getManyAndCount();
    }

    async getAll(company_id: number): Promise<FileDTO[]> {
        return await this.fileRepository
            .createQueryBuilder('file')
            .where('file.company_id = :company_id', { company_id })
            .addSelect(['file.created_at', 'file.updated_at'])
            .orderBy('file.id', 'DESC')
            .getMany();
    }

    async save(file: Express.Multer.File, fileDTO: FileDTO, company_id: number): Promise<FileDTO | undefined> {
        try {
            const s3Response: S3ResponseDTO = await this.mediaService.uploadFile(file);
            const result = await this.fileRepository.save({
                ...fileDTO,
                company_id,
                path_url: s3Response.Location,
                created_at: Date.now(),
            });

            const deviceTokens = await this.notificationService.getDeviceTokens({
                where: {
                    company_id,
                },
            });

            if (deviceTokens.length) {
                const payload = {
                    notification: {
                        title: `New file uploaded`,
                        body: '',
                    },
                };
                await this.notificationService.sendNotification(deviceTokens, payload);
            }

            return result;
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async download(fileDTO: FileDTO): Promise<FileDTO | undefined> {
        const file = await this.findById(fileDTO.id);
        await this.fileRepository.update(
            { id: fileDTO.id },
            { ...fileDTO, download_count: file.download_count ? Number(file.download_count) + 1 : 1 },
        );
        return await this.findById(fileDTO.id);
    }

    async deleteById(id: number): Promise<void | undefined> {
        const beforeEntityFind = await this.findById(id);
        const deletedFromS3 = await this.mediaService.deleteFile(beforeEntityFind.orginal_filename);

        if (!deletedFromS3.Errors.length) {
            await this.fileRepository.delete(id);
            const afterEntityFind = await this.findById(id);
            if (afterEntityFind) {
                throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
            }
            return null;
        }
        throw new ForbiddenException('Failed!');
    }
}
