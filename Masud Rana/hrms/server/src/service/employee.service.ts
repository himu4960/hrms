import { Injectable, HttpException, HttpStatus, Logger, NotFoundException, ForbiddenException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions, In } from 'typeorm';
import { Response } from 'express-serve-static-core';
import dayjs from 'dayjs';

import { config } from '../config';

import { EMPLOYEE_RESPONSE, REGISTER_RESPONSE, SKILLS_RESPONSE } from './../shared/constant/response-message';

import { InvitedNewUserRegistrationDTO } from './dto/auth/invited-new-user-registration.dto';
import { EmployeeDTO } from './dto/employee/employee.dto';
import { EmployeeUpdateDTO } from './dto/employee/employee-update.dto';
import { EmployeeDesignationsDTO } from './dto/employee/employee-designations.dto';
import { EmployeeCreateDTO } from './dto/employee/employee-create.dto';
import { EmployeeSkillDTO } from './dto/employee-skill.dto';
import { S3ResponseDTO } from './dto/s3-response.dto';
import { EmployeeAnniversaryResponseDTO } from './dto/employee/employee-anniversary-response.dto';
import { EmployeeBirthdayResponseDTO } from './dto/employee/employee-birthday-response.dto';
import { EmployeeFilterDTO } from './dto/employee/employee-filter.dto';
import { EmployeeListDTO } from './dto/employee/employee-list.dto';
import { UserCompanyDTO } from './dto/user-company/user-company.dto';
import { EmployeeActivationDTO } from './dto/employee/employee-activation.dto';
import { UserCompanyForWorkShiftDTO } from './dto/user-company/user-company-for-work-shift.dto';
import { CompanyDTO } from './dto/company.dto';

import { RoleType } from './../security/role-type';

import { EmployeeMapper } from '../service/mapper/employee.mapper';

import { BranchRepository } from './../repository/branch.repository';
import { EmployeeRepository } from '../repository/employee.repository';
import { UserCompanyRepository } from './../repository/user-company.repository';
import { EmployeeDesignationRepository } from './../repository/employee-designation.repository';
import { SkillRepository } from './../repository/skill.repository';

import { MailService } from './../shared/mail/mail.service';
import { RoleService } from './role.service';
import { CompanyService } from './company.service';
import { UtilService } from './../shared/util.service';
import { UserService } from './user.service';
import { MediaService } from './media.service';
import { EmployeeExportService } from './employee-export.service';
import { DesignationService } from './designation.service';
import { NotificationService } from '../fcm/notification.service';
import { RegisterEmployeeInvitationTransactionService } from './transaction/register-emplyee-invitation-transaction.service';

import { EExportType } from '../web/rest/reports/time-clock/models/time-sheet-report.model';
import { Skill } from 'src/domain/skill.entity';

@Injectable()
export class EmployeeService {
    logger = new Logger('EmployeeService');

    constructor(
        @InjectRepository(EmployeeRepository) private employeeRepository: EmployeeRepository,
        @InjectRepository(BranchRepository) private branchRepository: BranchRepository,
        @InjectRepository(SkillRepository) private skillRepository: SkillRepository,
        @InjectRepository(EmployeeDesignationRepository)
        private employeeDesignationRepository: EmployeeDesignationRepository,
        @InjectRepository(UserCompanyRepository)
        private userCompanyRepository: UserCompanyRepository,
        private employeeExportService: EmployeeExportService,
        private mailService: MailService,
        private roleService: RoleService,
        private companyService: CompanyService,
        private designationService: DesignationService,
        private notificationService: NotificationService,
        private userService: UserService,
        private utilService: UtilService,
        private mediaService: MediaService,
        private registerEmployeeInvitationTransactionService: RegisterEmployeeInvitationTransactionService,
    ) {}

    async sendInviteEmail(employee_id: number, company?: CompanyDTO) {
        const invitation_expiry = dayjs()
            .add(config['employee.invitation.expiry.day'], 'day')
            .unix();
        const employee = await this.employeeRepository.findOne(employee_id);

        if (!employee) {
            throw new HttpException('Employee not found!', HttpStatus.NOT_FOUND);
        }

        const user = await this.userService.findByFields({
            where: { email: employee.email },
            select: ['id', 'email'],
        });

        await this.employeeRepository.update({ id: employee_id }, { invitation_expiry });

        if (!user) {
            return await this.mailService.sendInviteRegisterEmail(employee, company);
        } else {
            return await this.mailService.sendInviteConfirmationEmail(employee, company);
        }
    }

    async registerInvitedEmployee(newEmployeeUser: InvitedNewUserRegistrationDTO) {
        const role = await this.roleService.findById(RoleType.EMPLOYEE);

        if (!role) {
            throw new HttpException('Role not found!', HttpStatus.NOT_FOUND);
        }

        const company = await this.companyService.findById(newEmployeeUser.company_id);
        if (!company) {
            throw new HttpException('Company not found!', HttpStatus.NOT_FOUND);
        }

        const existingUserByEmail = await this.userService.findByFields({
            where: { email: newEmployeeUser.email },
        });

        const existingUserByUsername = await this.userService.findByFields({
            where: { username: newEmployeeUser.username },
        });

        if (existingUserByEmail || existingUserByUsername) {
            const userCompany = await this.userCompanyRepository.findOne({
                where: { user_id: existingUserByEmail.id, company_id: company.id },
            });

            if (userCompany) {
                throw new HttpException(`You are already registered in ${company.name}!`, HttpStatus.NOT_FOUND);
            }

            const user = await this.registerEmployeeInvitationTransactionService.run(
                {
                    ...newEmployeeUser,
                    user_id: existingUserByEmail.id,
                },
                false,
            );
            return user;
        }

        const user = await this.registerEmployeeInvitationTransactionService.run(newEmployeeUser);
        return user;
    }

    async checkInvitedToken(token: string): Promise<EmployeeDTO | undefined> {
        const today = dayjs().unix();

        const employee = await this.employeeRepository.findOne({
            where: {
                invitation_token: token,
            },
            select: [
                'id',
                'first_name',
                'middle_name',
                'last_name',
                'email',
                'phone',
                'invitation_accepted',
                'invitation_token',
                'invitation_expiry',
                'company_id',
                'branch_id',
            ],
        });

        if (!employee) {
            throw new NotFoundException('Wrong token!');
        }

        if (employee.invitation_accepted) {
            throw new ForbiddenException(REGISTER_RESPONSE.ALREADY_ACCEPTED);
        }

        const invitation_expiry = Number(employee.invitation_expiry);
        const diff = invitation_expiry - today;

        if (diff >= 0) {
            return employee;
        }

        throw new ForbiddenException(REGISTER_RESPONSE.EXPIRED);
    }

    async employeeCount(company_id: number): Promise<any> {
        const total_employee_count = await this.userCompanyRepository.count({
            where: { company_id },
        });
        const not_activated = await this.employeeRepository.count({
            where: { company_id, invitation_accepted: false },
        });

        const disabled = await this.userCompanyRepository.count({
            where: { company_id, is_active: false },
        });

        return {
            total_employee_count,
            not_activated,
            disabled,
        };
    }

    async findOne(token: string): Promise<EmployeeDTO | undefined> {
        return await this.employeeRepository.findOne({ invitation_token: token });
    }

    async findDetailsToCreateWorkShift(
        employee_id: number,
        company_id: number,
        start_date: number,
    ): Promise<UserCompanyForWorkShiftDTO | undefined> {
        const result = await this.userCompanyRepository
            .createQueryBuilder('userCompany')
            .leftJoinAndSelect('userCompany.employee', 'employee')
            .leftJoinAndSelect('employee.branch', 'branch')
            .leftJoinAndSelect(
                'employee.leaves',
                'leaves',
                'leaves.is_approved = true AND leaves.is_review_accepted = false AND leaves.start_date <= :start_date AND leaves.end_date >= :start_date',
                { start_date },
            )
            .leftJoinAndSelect('employee.employee_designations', 'employee_designations')
            .where('userCompany.company = :company_id', { company_id })
            .andWhere('userCompany.employee_id = :employee_id', { employee_id })
            .andWhere('userCompany.is_active = true')
            .select(['userCompany.id'])
            .addSelect([
                'employee.id',
                'employee.first_name',
                'employee.branch_id',
                'employee_designations.id',
                'employee_designations.designation_id',
                'leaves.id',
                'leaves.start_date',
                'leaves.end_date',
            ])
            .getOne();
        return result;
    }

    async getAllActivatedEmployeesByCompanyId(company_id: number): Promise<EmployeeDTO[] | undefined> {
        const result = await this.employeeRepository
            .createQueryBuilder('employee')
            .leftJoinAndSelect('employee.user_company', 'user_company', 'user_company.employee_id = employee.id')
            .leftJoinAndSelect('employee.employee_designations', 'employee_designation')
            .leftJoinAndSelect('employee.branch', 'branch')
            .leftJoinAndSelect('employee_designation.designation', 'designation')
            .where('employee.company_id = :company_id', { company_id })
            .andWhere(
                'user_company.is_active = true AND employee.is_active = true AND employee.invitation_accepted = true',
            )
            .select(['employee.id', 'employee.first_name', 'employee.last_name'])
            .addSelect(['employee_designation.id', 'designation.id', 'designation.name', 'branch.id'])
            .getMany();
        return result;
    }

    async findById(employee_id: number, company_id: number): Promise<UserCompanyDTO | undefined> {
        const result = await this.userCompanyRepository
            .createQueryBuilder('userCompany')
            .leftJoinAndSelect('userCompany.employee', 'employee')
            .leftJoinAndSelect('userCompany.role', 'role')
            .leftJoinAndSelect('employee.branch', 'branch')
            .leftJoinAndSelect('employee.work_shifts', 'work_shifts')
            .leftJoinAndSelect('employee.leaves', 'leaves')
            .leftJoinAndSelect('employee.skills', 'skills')
            .leftJoinAndSelect('employee.employee_designations', 'employee_designations')
            .leftJoinAndSelect('employee_designations.designation', 'designations')
            .select([
                'userCompany',
                'employee',
                'role',
                'branch',
                'work_shifts',
                'leaves',
                'skills',
                'employee_designations',
                'designations',
            ])
            .addSelect([
                'employee.blood_group_id',
                'employee.branch_id',
                'employee.gender_id',
                'employee.religion_id',
                'employee.marital_status_id',
            ])
            .where('userCompany.company = :company_id', { company_id })
            .andWhere('userCompany.employee_id = :employee_id', { employee_id })
            .getOne();
        return result;
    }

    async findOneById(id: number): Promise<EmployeeDTO | undefined> {
        return await this.employeeRepository.findOne(id);
    }

    async findByFields(options: FindOneOptions<EmployeeDTO>): Promise<EmployeeDTO | undefined> {
        return await this.employeeRepository.findOne(options);
    }

    async findByEmail(email: string): Promise<EmployeeDTO | undefined> {
        return await this.employeeRepository
            .createQueryBuilder('employee')
            .leftJoinAndSelect('employee.employee_designations', 'employee_designation')
            .leftJoinAndSelect('employee_designation.designation', 'designation')
            .where('employee.email = :email', { email })
            .addSelect('employee.branch_id')
            .getOne();
    }

    async findAll(company_id: number): Promise<EmployeeDTO[]> {
        return await this.employeeRepository
            .createQueryBuilder('employee')
            .leftJoinAndSelect('employee.branch', 'branch')
            .leftJoinAndSelect('employee.employee_designations', 'employee_designations')
            .leftJoinAndSelect('employee_designations.designation', 'designation')
            .select(['employee', 'employee_designations', 'designation'])
            .addSelect(['branch.id', 'branch.name'])
            .where('employee.company_id = :company_id', { company_id })
            .getMany();
    }

    async findAndCount(options: FindManyOptions<EmployeeDTO>, company_id: number): Promise<[EmployeeDTO[], number]> {
        return await this.employeeRepository
            .createQueryBuilder('employee')
            .leftJoinAndSelect('employee.branch', 'branch')
            .leftJoinAndSelect('employee.employee_designations', 'employee_designations')
            .leftJoinAndSelect('employee_designations.designation', 'designation')
            .select(['employee', 'employee_designations', 'designation'])
            .addSelect(['branch.id', 'branch.name'])
            .where('employee.company_id = :company_id', { company_id })
            .skip(options.skip)
            .take(options.take)
            .getManyAndCount();
    }

    async getAllInactiveEmployees(
        options: FindManyOptions<EmployeeDTO>,
        company_id: number,
    ): Promise<[EmployeeDTO[], number]> {
        let query = this.employeeRepository
            .createQueryBuilder('employee')
            .where('employee.company_id = :company_id', { company_id })
            .andWhere('employee.invitation_accepted = false')
            .orderBy('employee.id', 'DESC');

        if ((options.skip || options.skip === 0) && options.take) {
            query = query.skip(options.skip).take(options.take);
        }

        return await query.getManyAndCount();
    }

    async filterAndCountFromUserCompany(
        options: FindManyOptions<EmployeeDTO>,
        company_id: number,
        employeeFilterDTO: EmployeeFilterDTO,
    ): Promise<[EmployeeListDTO[], number]> {
        let query = this.userCompanyRepository
            .createQueryBuilder('userCompany')
            .leftJoinAndSelect('userCompany.employee', 'employee')
            .leftJoinAndSelect('userCompany.role', 'role')
            .leftJoinAndSelect('employee.branch', 'branch')
            .leftJoinAndSelect('employee.employee_designations', 'employee_designation')
            .leftJoinAndSelect('employee_designation.designation', 'designation')
            .leftJoinAndSelect('employee.skills', 'skill')
            .where('employee.company_id = :company_id', { company_id })
            .andWhere('employee.is_deleted = false')
            .select([
                'employee.id as id',
                'employee.first_name as first_name',
                'employee.last_name as last_name',
                'employee.code as code',
                'employee.email as email',
                'employee.dob as birth_date',
                'employee.personal_email as personal_email',
                'employee.photo_url as photo_url',
                'employee.join_date as join_date',
                'employee.phone as phone',
                'employee.telephone as telephone',
                'employee.nationality as nationality',
                'employee.invitation_accepted as invitation_accepted',
                'role.name as role_name',
                'userCompany.is_active as is_active',
            ])
            .groupBy('employee.id');

        if (employeeFilterDTO?.search_text) {
            query.andWhere('employee.first_name like :name', { name: `%${employeeFilterDTO.search_text}%` });
            query.orWhere('employee.middle_name like :name', { name: `%${employeeFilterDTO.search_text}%` });
            query.orWhere('employee.last_name like :name', { name: `%${employeeFilterDTO.search_text}%` });
        }

        if (employeeFilterDTO?.branch_id) {
            query.andWhere('branch.id = :id', { id: employeeFilterDTO.branch_id });
        }

        if (employeeFilterDTO?.is_active !== undefined) {
            query.andWhere('userCompany.is_active = :is_active', { is_active: employeeFilterDTO.is_active });
        }

        if (employeeFilterDTO?.invitation_accepted !== undefined) {
            query.andWhere('employee.invitation_accepted = :invitation_accepted', {
                invitation_accepted: employeeFilterDTO.invitation_accepted,
            });
        }

        if (employeeFilterDTO?.designations?.length) {
            query.andWhere('designation.id IN (:...ids)', { ids: employeeFilterDTO.designations });
        }

        if (employeeFilterDTO?.skills?.length) {
            query.andWhere('skill.id IN (:...ids)', { ids: employeeFilterDTO.skills });
        }

        if (options.take && (options.skip || options.skip === 0)) {
            query = query.limit(options.take).offset(options.skip);
        }

        const count = await query.getCount();
        const employees = await query.getRawMany();

        return [employees, count];
    }

    async getEmployeeAnniversary(company_id: number, month: number): Promise<EmployeeAnniversaryResponseDTO[]> {
        const currentUTCMonth = new Date(Date.now()).getUTCMonth();
        const currentMonth = month || currentUTCMonth;
        const currentYear = new Date(Date.now()).getUTCFullYear();

        if (currentMonth > 12) {
            throw new NotFoundException('Month cannot be greater than 12');
        }

        const currentDate = currentUTCMonth === currentMonth ? new Date(Date.now()).getUTCDate() : 0;

        const employees = await this.employeeRepository
            .createQueryBuilder('employee')
            .leftJoinAndSelect('employee.employee_designations', 'employee_designation')
            .leftJoinAndSelect('employee_designation.designation', 'designation')
            .where('employee.company_id = :company_id', { company_id })
            .andWhere('employee.join_date IS NOT NULL')
            .select([
                'employee.id',
                'employee.first_name',
                'employee.last_name',
                'employee.join_date',
                'employee.photo_url',
                'employee.code',
            ])
            .addSelect(['employee_designation.id', 'designation.id', 'designation.name'])
            .getMany();

        const result = employees.filter((employee: EmployeeDTO) => {
            const employeeJoinMonth = new Date(Number(employee.join_date)).getUTCMonth();
            const employeeJoinDate = new Date(Number(employee.join_date)).getUTCDate();
            const employeeJoinYear = new Date(Number(employee.join_date)).getUTCFullYear();
            return (
                (currentYear > employeeJoinYear &&
                    currentMonth === employeeJoinMonth &&
                    currentDate <= employeeJoinDate) ||
                (currentYear > employeeJoinYear && currentMonth === 12 && currentMonth - 11 >= employeeJoinMonth)
            );
        });

        return result;
    }

    async getEmployeeBirthday(company_id: number, month: number): Promise<EmployeeBirthdayResponseDTO[]> {
        const currentUTCMonth = new Date(Date.now()).getUTCMonth();
        const currentMonth = month || currentUTCMonth;
        const currentDate = currentUTCMonth === currentMonth ? new Date(Date.now()).getUTCDate() : 0;

        const employees = await this.employeeRepository
            .createQueryBuilder('employee')
            .leftJoinAndSelect('employee.employee_designations', 'employee_designation')
            .leftJoinAndSelect('employee_designation.designation', 'designation')
            .where('employee.company_id = :company_id', { company_id })
            .andWhere('employee.dob IS NOT NULL')
            .select([
                'employee.id',
                'employee.first_name',
                'employee.last_name',
                'employee.dob',
                'employee.photo_url',
                'employee.code',
            ])
            .addSelect(['employee_designation.id', 'designation.id', 'designation.name'])
            .getMany();

        const result = employees.filter((employee: EmployeeDTO) => {
            const employeeBirthdayMonth = new Date(Number(employee.dob)).getUTCMonth();
            const employeeBirthdayDate = new Date(Number(employee.dob)).getUTCDate();
            return (
                (currentMonth === employeeBirthdayMonth && currentDate <= employeeBirthdayDate) ||
                (currentMonth === 12 && currentMonth - 11 >= employeeBirthdayMonth)
            );
        });

        return result;
    }

    async updateActiveStatus(employeeActivationDTO: EmployeeActivationDTO, company_id: number) {
        const options = {
            where: { employee_id: employeeActivationDTO.employee_id, company_id: company_id },
        };

        const userCompany = await this.userCompanyRepository.findOne(options);

        if (!userCompany) {
            throw new HttpException(EMPLOYEE_RESPONSE.NOT_FOUND, HttpStatus.NOT_FOUND);
        }

        await this.userCompanyRepository.save({ ...userCompany, is_active: employeeActivationDTO.is_active });

        return await this.findById(employeeActivationDTO.employee_id, company_id);
    }

    async save(employeeDTOs: EmployeeCreateDTO[], company_id: number, creator?: number): Promise<EmployeeDTO[]> {
        const defaultBranch = await this.branchRepository.findOne({
            where: { company_id, is_default: true },
            order: { id: 'DESC' },
        });

        if (!defaultBranch) {
            throw new HttpException('Default branch not found!', HttpStatus.NOT_FOUND);
        }

        const emails = employeeDTOs.map(employee => employee.email);
        const existingEmployee = await this.employeeRepository.findOne({
            where: {
                email: In(emails),
                company_id,
            },
        });

        if (existingEmployee) {
            throw new HttpException('Employee already exist!', HttpStatus.FOUND);
        }

        const invitation_expiry = dayjs()
            .add(config['employee.invitation.expiry.day'], 'day')
            .unix();

        const employeeResult = employeeDTOs.map((employeeDTO: EmployeeDTO) => {
            if (creator) {
                if (!employeeDTO.created_by) {
                    employeeDTO.created_by = creator;
                    // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                    employeeDTO.created_at = Date.now();
                }
                employeeDTO.updated_by = creator;
            }
            return {
                ...employeeDTO,
                company_id,
                branch_id: defaultBranch.id,
                invitation_expiry,
            };
        });
        const createdEmployees = await this.employeeRepository.save(employeeResult);

        const company = await this.companyService.findByFields({
            where: { id: company_id },
            select: ['id', 'name', 'logo_url_small'],
        });

        createdEmployees.map(async employee => {
            await this.sendInviteEmail(employee.id, company);
        });
        return createdEmployees;
    }

    async saveOne(employeeDTO: EmployeeCreateDTO, company_id: number, creator?: number): Promise<EmployeeDTO> {
        return await this.employeeRepository.save({ ...employeeDTO, company_id, created_by: creator });
    }

    async assignDesignation(employeeDesignation: EmployeeDesignationsDTO, company_id: number): Promise<any> {
        try {
            const { employee_id, designation_id } = employeeDesignation;
            const designation = await this.designationService.findById(designation_id);

            const existingDesignation = await this.employeeDesignationRepository.findOne({
                where: {
                    employee_id,
                    designation_id,
                },
            });

            if (existingDesignation) {
                throw new HttpException(EMPLOYEE_RESPONSE.READ.ERROR, HttpStatus.FOUND);
            }

            await this.employeeDesignationRepository.save({
                ...employeeDesignation,
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                created_at: Date.now(),
            });

            const result = await this.designationService.getEmployeeDesignationBasedSkills(employee_id);

            const deviceTokens = await this.notificationService.getDeviceTokens({
                where: {
                    employee_id,
                    company_id,
                },
            });
            const payload = {
                notification: {
                    title: 'New Designation Assigned to You',
                    body: `You have been assigned a new designation. Your new designation is ${designation.name}.`,
                },
            };

            await this.notificationService.sendNotification(deviceTokens, payload);
            return result;
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async assignSkill(employeeSkill: EmployeeSkillDTO, company_id: number): Promise<EmployeeDTO | undefined> {
        try {
            const { employee_id, skill_id } = employeeSkill;

            const existingEmployee: EmployeeDTO = await this.employeeRepository.findOne({
                where: { id: employee_id },
                relations: ['skills'],
            });
            if (!existingEmployee) {
                throw new HttpException(EMPLOYEE_RESPONSE.READ.NOT_FOUND, HttpStatus.NOT_FOUND);
            }

            const existingSkill = await this.skillRepository.findOne({ id: skill_id });
            if (!existingSkill) {
                throw new HttpException(SKILLS_RESPONSE.READ.NOT_FOUND, HttpStatus.NOT_FOUND);
            }

            const employee = EmployeeMapper.fromDTOtoEntity(existingEmployee);
            employee.skills.push(existingSkill);

            const result = await this.employeeRepository.save({
                ...employee,
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                created_at: Date.now(),
            });

            const employeeSkills = await this.getEmployeeSkillsName(employee.skills);

            const deviceTokens = await this.notificationService.getDeviceTokens({
                where: {
                    employee_id,
                    company_id,
                },
            });

            const payload = {
                notification: {
                    title: 'New Skill Assigned to You',
                    body: `Your skills  has been updated to reflect your recent achievements and accomplishments. The updated skills profile includes the following additions: ${employeeSkills}`,
                },
            };

            await this.notificationService.sendNotification(deviceTokens, payload);
            return result;
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async unassignSkill(employeeSkill: EmployeeSkillDTO, company_id: number): Promise<EmployeeDTO | undefined> {
        try {
            const { employee_id, skill_id } = employeeSkill;

            const existingEmployee: EmployeeDTO = await this.employeeRepository.findOne({
                where: { id: employee_id },
                relations: ['skills'],
            });
            if (!existingEmployee) {
                throw new HttpException(EMPLOYEE_RESPONSE.READ.NOT_FOUND, HttpStatus.NOT_FOUND);
            }

            const existingSkill = await this.skillRepository.findOne({ id: skill_id });
            if (!existingSkill) {
                throw new HttpException(SKILLS_RESPONSE.READ.NOT_FOUND, HttpStatus.NOT_FOUND);
            }

            const employee = EmployeeMapper.fromDTOtoEntity(existingEmployee);

            const removedSkills = this.utilService.removeObjectWithId(employee.skills, skill_id);

            employee.skills = removedSkills;

            const employeeSkills = await this.getEmployeeSkillsName(removedSkills);

            const result = await this.employeeRepository.save(employee);
            const deviceTokens = await this.notificationService.getDeviceTokens({
                where: {
                    employee_id,
                    company_id,
                },
            });

            const payload = {
                notification: {
                    title: 'Skill Unassigned to You',
                    body: `One of your assigned skills has been unassigned by your manager. The updated skills profile includes the following additions: ${employeeSkills}`,
                },
            };

            await this.notificationService.sendNotification(deviceTokens, payload);
            return result;
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async unassignAllSkills(employee_id: number): Promise<EmployeeDTO | undefined> {
        const existingEmployee: EmployeeDTO = await this.employeeRepository.findOne({
            where: { id: employee_id },
            relations: ['skills'],
        });
        if (!existingEmployee) {
            throw new HttpException(EMPLOYEE_RESPONSE.READ.NOT_FOUND, HttpStatus.NOT_FOUND);
        }

        const employee = EmployeeMapper.fromDTOtoEntity(existingEmployee);

        employee.skills = [];

        return await this.employeeRepository.save(employee);
    }

    async unassignDesignation(
        employeeDesignation: EmployeeDesignationsDTO,
        company_id: number,
    ): Promise<any | undefined> {
        try {
            const { employee_id, designation_id } = employeeDesignation;
            const designation = await this.designationService.findById(designation_id);
            await this.employeeDesignationRepository
                .createQueryBuilder()
                .delete()
                .where('employee_id = :employee_id', { employee_id })
                .andWhere('designation_id = :designation_id', { designation_id })
                .execute();

            const deviceTokens = await this.notificationService.getDeviceTokens({
                where: {
                    employee_id,
                    company_id,
                },
            });

            const payload = {
                notification: {
                    title: `You are Discharged from ${designation.name}`,
                    body: `You have been discharged from your position as ${designation.name} by your manager. You are no longer responsible for ${designation.name}. Please contact your manager for more information or clarification.`,
                },
            };

            await this.notificationService.sendNotification(deviceTokens, payload);
            return await this.designationService.getEmployeeDesignationBasedSkills(employee_id);
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async unassignAllDesignations(employee_id: number): Promise<any | undefined> {
        try {
            await this.employeeDesignationRepository
                .createQueryBuilder()
                .delete()
                .where('employee_id = :employee_id', { employee_id })
                .execute();
            return await this.designationService.getEmployeeDesignationBasedSkills(employee_id);
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async updateById(
        employee_id: number,
        employeeDTO: EmployeeUpdateDTO,
        updater?: number,
    ): Promise<EmployeeDTO | undefined> {
        const findEmployee = await this.employeeRepository.findOne(employee_id);
        if (!findEmployee) {
            throw new HttpException(EMPLOYEE_RESPONSE.READ.NOT_FOUND, HttpStatus.NOT_FOUND);
        }
        await this.employeeRepository.update(
            { id: employee_id },
            { ...employeeDTO, updated_by: updater, updated_at: Date.now() },
        );
        return await this.employeeRepository.findOne(employee_id);
    }

    async changeBranchByEmployeeId(
        employee_id: number,
        branch_id: number,
        company_id: number,
    ): Promise<EmployeeDTO | undefined> {
        try {
            const findEmployee = await this.employeeRepository.findOne(employee_id);
            if (!findEmployee) {
                throw new HttpException(EMPLOYEE_RESPONSE.READ.NOT_FOUND, HttpStatus.NOT_FOUND);
            }
            await this.employeeRepository.update({ id: employee_id }, { branch_id, updated_at: Date.now() });
            await this.unassignAllDesignations(employee_id);
            await this.unassignAllSkills(employee_id);
            const result = await this.employeeRepository.findOne(employee_id);

            const deviceTokens = await this.notificationService.getDeviceTokens({
                where: {
                    employee_id,
                    company_id,
                },
            });

            const payload = {
                notification: {
                    title: `Employee Branch Transfer Notification`,
                    body: `${findEmployee.first_name} ${findEmployee.last_name} have been transferred from ${findEmployee.branch.name} to ${result.branch.name}.`,
                },
            };

            await this.notificationService.sendNotification(deviceTokens, payload);

            return result;
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async update(employeeDTO: EmployeeDTO, updater?: number): Promise<EmployeeDTO | undefined> {
        const entity = EmployeeMapper.fromDTOtoEntity(employeeDTO);
        if (updater) {
            entity.updated_by = updater;
        }
        try {
            const result = await this.employeeRepository.save(entity);
            return EmployeeMapper.fromEntityToDTO(result);
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.NOT_MODIFIED);
        }
    }

    async deleteById(id: number): Promise<EmployeeDTO | undefined> {
        const entityFind = await this.employeeRepository.findOne(id);
        if (!entityFind) {
            throw new HttpException('Employee is not found!', HttpStatus.NOT_FOUND);
        }
        await this.employeeRepository.delete(id);
        const afterDeletedEntity = await this.employeeRepository.findOne(id);
        if (afterDeletedEntity) {
            throw new HttpException('Employee cannot be deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }

    async uploadImage(file: any, userId: number): Promise<EmployeeDTO> {
        const s3Response: S3ResponseDTO = await this.mediaService.uploadFile(file);
        if (s3Response) {
            const employee = await this.employeeRepository.findOne(userId);
            if (employee) {
                employee.updated_by = userId;
                employee.photo_url = s3Response.Location;
            }
            try {
                const result = await this.employeeRepository.save(employee);
                return result;
            } catch (error) {
                throw new HttpException(error.message, HttpStatus.NOT_MODIFIED);
            }
        }
        throw new HttpException('Upload Failed!', HttpStatus.NOT_MODIFIED);
    }

    async exportEmployees(company_id: number, res: Response, exportType: string): Promise<any | undefined> {
        const [employees, count] = await this.filterAndCountFromUserCompany({}, company_id, null);
        if (exportType === EExportType.XLSX) {
            return this.employeeExportService.exportEmployeesXLSX(employees, 'Stuff list', res);
        } else if (exportType === EExportType.CSV) {
            return this.employeeExportService.exportEmployeesCSV(employees, 'Stuff list', res);
        }
    }

    async getEmployeeSkillsName(skills: Skill[]) {
        let employeeSkills = '';
        await skills.map((skill, index) => {
            if (index === skills.length - 1) {
                employeeSkills += `${skill.name}`;
            } else {
                employeeSkills += `${skill.name}, `;
            }
        });

        return employeeSkills;
    }
}
