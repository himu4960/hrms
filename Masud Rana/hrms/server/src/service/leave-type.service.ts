import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';

import { LeaveBalanceService } from './leave-balance.service';

import { LeaveTypeMapper } from '../service/mapper/leave-type.mapper';
import { LeaveBalanceMapper } from './mapper/leave-balance.mapper';

import { LeaveTypeRepository } from '../repository/leave-type.repository';
import { LeaveBalanceRepository } from '../repository/leave-balance.repository';
import { EmployeeRepository } from '../repository/employee.repository';

import { LeaveBalance } from '../domain/leave-balance.entity';
import { LeaveTypeDTO } from '../service/dto/leave-type.dto';
import { LeaveBalanceDTO } from './dto/leave/leave-balance.dto';
import { DEFAULT_WORK_HOUR_IN_MINS } from '../shared/constant/util.constant';

const relationshipNames = ['company'];

@Injectable()
export class LeaveTypeService {
    logger = new Logger('LeaveTypeService');

    constructor(
        @InjectRepository(LeaveTypeRepository) private leaveTypeRepository: LeaveTypeRepository,
        private leaveBalanceService: LeaveBalanceService,
        private leaveBalanceRepository: LeaveBalanceRepository,
        private employeeRepository: EmployeeRepository,
    ) {}

    async findById(id: number): Promise<LeaveTypeDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.leaveTypeRepository.findOne(id, options);
        return LeaveTypeMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<LeaveTypeDTO>): Promise<LeaveTypeDTO | undefined> {
        const result = await this.leaveTypeRepository.findOne(options);
        return LeaveTypeMapper.fromEntityToDTO(result);
    }

    async findAndCount(options: FindManyOptions<LeaveTypeDTO>, company_id: number): Promise<[LeaveTypeDTO[], number]> {
        options.relations = relationshipNames;
        options.where = { company_id };
        const resultList = await this.leaveTypeRepository.findAndCount(options);
        const leaveTypeDTO: LeaveTypeDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach(leaveType => leaveTypeDTO.push(LeaveTypeMapper.fromEntityToDTO(leaveType)));
            resultList[0] = leaveTypeDTO;
        }
        return resultList;
    }

    async save(leaveTypeDTO: LeaveTypeDTO, creator?: number): Promise<LeaveTypeDTO | undefined> {
        const entity = LeaveTypeMapper.fromDTOtoEntity(leaveTypeDTO);
        if (creator) {
            if (!entity.created_by) {
                entity.created_by = creator;
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                entity.created_at = Date.now();
            }
            entity.updated_by = creator;
        }
        const result = await this.leaveTypeRepository.save(entity);

        const response = await this.employeeRepository.findAndCount();
        response[0].map(async (employee: any) => {
            let leaveBalance: LeaveBalance = {
                employee_id: employee.id,
                company_id: entity.company_id,
                leave_type_id: result.id,
                allocated_leave_mins: entity.max_days_allowed * DEFAULT_WORK_HOUR_IN_MINS,
                remaining_leave_mins: entity.max_days_allowed * DEFAULT_WORK_HOUR_IN_MINS,
            };
            if (creator) {
                if (!leaveBalance.created_by) {
                    leaveBalance.created_by = creator;
                }
                leaveBalance.updated_by = creator;
            }
            await this.leaveBalanceService.save(leaveBalance);
        });

        return LeaveTypeMapper.fromEntityToDTO(result);
    }

    async update(leaveTypeDTO: LeaveTypeDTO, updater?: number): Promise<LeaveTypeDTO | undefined> {
        const entity = LeaveTypeMapper.fromDTOtoEntity(leaveTypeDTO);
        if (updater) {
            entity.updated_by = updater;
            entity.updated_at = Date.now();
        }
        const result = await this.leaveTypeRepository.save(entity);

        const response = await this.leaveBalanceRepository.findAndCount({ where: { leave_type_id: entity.id } });
        response[0].map(async (leaveBalanceDTO: LeaveBalanceDTO) => {
            let leaveBalance = LeaveBalanceMapper.fromDTOtoEntity(leaveBalanceDTO);

            if (updater) {
                leaveBalance.remaining_leave_mins =
                    leaveBalance.remaining_leave_mins +
                    (entity.max_days_allowed * DEFAULT_WORK_HOUR_IN_MINS - leaveBalance.allocated_leave_mins);
                leaveBalance.allocated_leave_mins = entity.max_days_allowed * DEFAULT_WORK_HOUR_IN_MINS;
                leaveBalance.updated_by = updater;
            }
            await this.leaveBalanceService.save(leaveBalance);
        });

        return LeaveTypeMapper.fromEntityToDTO(result);
    }

    async deleteById(id: number): Promise<LeaveTypeDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('Leave is not found!', HttpStatus.NOT_FOUND);
        }
        await this.leaveTypeRepository.delete(id);
        const response = await this.leaveBalanceRepository.findAndCount({ where: { leave_type_id: id } });
        response[0].map(async (leaveBalanceDTO: LeaveBalanceDTO) => {
            await this.leaveBalanceRepository.delete(leaveBalanceDTO.id);
        });
        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Leave cannot be deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }
}
