import { BloodGroupDTO } from './dto/blood-group/blood-group.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { In } from 'typeorm';

import { BloodGroupUpdateDTO } from './dto/blood-group/blood-group-update.dto';
import { BloodGroupCreateDTO } from './dto/blood-group/blood-group-create.dto';

import { BloodGroupTranslation } from './../domain/blood-group-translation.entity';

import { LanguageRepository } from './../repository/language.repository';

import { BloodGroupRepository } from '../repository/blood-group.repository';
import { BloodGroupTranslationRepository } from '../repository/blood-group-translation.repository';

@Injectable()
export class BloodGroupService {
    logger = new Logger('BloodGroupService');

    constructor(
        @InjectRepository(BloodGroupRepository) private bloodGroupRepository: BloodGroupRepository,
        @InjectRepository(LanguageRepository)
        private languageRepository: LanguageRepository,
        @InjectRepository(BloodGroupTranslationRepository)
        private bloodGroupTranslationRepository: BloodGroupTranslationRepository,
    ) { }

    async findAll(): Promise<BloodGroupDTO[] | undefined> {
        return await this.bloodGroupRepository
            .createQueryBuilder('bloodGroup')
            .leftJoinAndMapMany(
                'bloodGroup.translations',
                BloodGroupTranslation,
                'translation',
                'bloodGroup.id = translation.blood_group_id',
            )
            .select(['bloodGroup.id', 'translation.name', 'translation.language_code'])
            .getMany();
    }

    async findById(id: number): Promise<BloodGroupDTO | undefined> {
        return await this.bloodGroupRepository.findOne(id);
    }

    async findByLangCode(langCode: string): Promise<BloodGroupDTO[] | undefined> {
        return await this.bloodGroupRepository
            .createQueryBuilder('bloodGroup')
            .leftJoinAndSelect('bloodGroup.translations', 'translation')
            .where('translation.language_code=:language_code', { language_code: langCode })
            .select(['bloodGroup.id as id', 'translation.name as name'])
            .getRawMany();
    }

    async save(createBloodGroup: BloodGroupCreateDTO, creator?: number): Promise<BloodGroupDTO | undefined> {
        const language_codes = createBloodGroup.translations.map(bloodGroup => bloodGroup.language_code);

        const languageResult = await this.languageRepository.find({ where: { code: In(language_codes) } });

        if (languageResult.length > 0) {
            return await this.bloodGroupRepository.save({ ...createBloodGroup, created_by: creator });
        }
        throw new HttpException('Language not found!', HttpStatus.NOT_FOUND);
    }

    async update(
        id: number,
        updateBloodGroup: BloodGroupUpdateDTO,
        updater?: number,
    ): Promise<BloodGroupDTO | undefined> {
        updateBloodGroup.translations.forEach(async bloodGroupTranslation => {
            const { name, language_code } = bloodGroupTranslation;

            const languageResult = await this.languageRepository.findOne({ where: { code: language_code } });

            if (languageResult) {
                const isExist = await this.bloodGroupTranslationRepository
                    .createQueryBuilder('bloodGroupTranslation')
                    .where('blood_group_id=:id', { id })
                    .andWhere('language_code=:language_code', { language_code })
                    .getOne();
                if (isExist) {
                    await this.bloodGroupTranslationRepository
                        .createQueryBuilder()
                        .update(BloodGroupTranslation)
                        .set({
                            name,
                            updated_by: updater,
                        })
                        .where('blood_group_id=:id', { id })
                        .andWhere('language_code=:language_code', { language_code })
                        .execute();
                } else {
                    await this.bloodGroupTranslationRepository
                        .createQueryBuilder()
                        .insert()
                        .into(BloodGroupTranslation)
                        .values([
                            {
                                name: bloodGroupTranslation.name,
                                language_code: bloodGroupTranslation.language_code,
                                blood_group_id: id,
                                created_by: updater,
                            },
                        ])
                        .execute();
                }
            } else {
                throw new HttpException('Language not found!', HttpStatus.NOT_FOUND);
            }
        });
        return await this.bloodGroupRepository.findOne(id);
    }

    async deleteById(id: number): Promise<BloodGroupDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('Blood group not found!', HttpStatus.NOT_FOUND);
        }
        await this.bloodGroupRepository.delete({ id });
        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Blood group can not be deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }
}
