import { MaritalStatus } from '../../domain/marital-status.entity';

import { MaritalStatusDTO } from '../dto/marital-status/marital-status.dto';

/**
 * A MaritalStatus mapper object.
 */
export class MaritalStatusMapper {
    static fromDTOtoEntity(entityDTO: MaritalStatusDTO): MaritalStatus {
        if (!entityDTO) {
            return;
        }
        const entity = new MaritalStatus();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: MaritalStatus): MaritalStatusDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new MaritalStatusDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
