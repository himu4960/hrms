import { WorkShiftTemplate } from '../../domain/work-shift-template.entity';
import { WorkShiftTemplateDTO } from '../dto/work-shift-template.dto';

/**
 * A WorkShiftTemplate mapper object.
 */
export class WorkShiftTemplateMapper {
    static fromDTOtoEntity(entityDTO: WorkShiftTemplateDTO): WorkShiftTemplate {
        if (!entityDTO) {
            return;
        }
        let entity = new WorkShiftTemplate();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach((field) => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: WorkShiftTemplate): WorkShiftTemplateDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new WorkShiftTemplateDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach((field) => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
