import { PermissionType } from '../../domain/permission-type.entity';

import { PermissionTypeDTO } from '../dto/permission-type.dto';

/**
 * A PermissionType mapper object.
 */
export class PermissionTypeMapper {
    static fromDTOtoEntity(entityDTO: PermissionTypeDTO): PermissionType {
        if (!entityDTO) {
            return;
        }
        const entity = new PermissionType();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach((field) => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: PermissionType): PermissionTypeDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new PermissionTypeDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach((field) => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
