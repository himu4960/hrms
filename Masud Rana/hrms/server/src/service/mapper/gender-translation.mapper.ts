import { GenderTranslation } from '../../domain/gender-translation.entity';

import { GenderTranslationDTO } from '../dto/gender/gender-translation.dto';

/**
 * A GenderTranslation mapper object.
 */
export class GenderTranslationMapper {
    static fromDTOtoEntity(entityDTO: GenderTranslationDTO): GenderTranslation {
        if (!entityDTO) {
            return;
        }
        const entity = new GenderTranslation();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: GenderTranslation): GenderTranslationDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new GenderTranslationDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
