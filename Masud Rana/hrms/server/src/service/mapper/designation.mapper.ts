import { Designation } from '../../domain/designation.entity';

import { DesignationDTO } from '../dto/designation/designation.dto';
import { DesignationCreateDTO } from '../dto/designation/designation-create.dto';
import { DesignationUpdateDTO } from './../dto/designation/designation-update.dto';

/**
 * A Designation mapper object.
 */
export class DesignationMapper {
    static fromDTOtoEntity(entityDTO: DesignationDTO): Designation {
        if (!entityDTO) {
            return;
        }
        let entity = new Designation();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: Designation): DesignationDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new DesignationDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}

export class DesignationCreateMapper {
    static fromDTOtoEntity(entityDTO: DesignationCreateDTO): Designation {
        if (!entityDTO) {
            return;
        }
        let entity = new Designation();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }
}

export class DesignationUpdateMapper {
    static fromDTOtoEntity(entityDTO: DesignationUpdateDTO): Designation {
        if (!entityDTO) {
            return;
        }
        let entity = new Designation();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }
}
