import { TimeClock } from '../../domain/time-clock.entity';
import { TimeClockDTO } from '../dto/time-clock/time-clock.dto';
import { GeoLocation } from '../../domain/geo-location.entity';
/**
 * A TimeClock mapper object.
 */
export class TimeClockMapper {
    static fromDTOtoEntity(entityDTO: TimeClockDTO): TimeClock {
        if (!entityDTO) {
            return;
        }
        let entity = new TimeClock();

        const fields = Object.getOwnPropertyNames(entityDTO);

        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });

        return entity;
    }

    static fromEntityToDTO(entity: TimeClock): TimeClockDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new TimeClockDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }

    static fromResponseToEntity(data: any): GeoLocation {
        if (!data) {
            return;
        }
        let address = { ...data };
        let timeClockInAddress = new GeoLocation();
        const { location } = address;
        delete address['location'];
        let newAddress = { ...address, ...location };
        const fields = Object.getOwnPropertyNames(newAddress);
        
        fields.forEach(field => {
            timeClockInAddress[field] = newAddress[field];
        });

        return timeClockInAddress;
    }
}
