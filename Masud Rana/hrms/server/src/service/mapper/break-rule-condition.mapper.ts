import { BreakRuleCondition } from '../../domain/break-rule-condition.entity';
import { BreakRuleConditionDTO } from '../dto/break-rule/break-rule-condition.dto';

/**
 * A BreakRuleCondition mapper object.
 */
export class BreakRuleConditionMapper {
    static fromDTOtoEntity(entityDTO: BreakRuleConditionDTO): BreakRuleCondition {
        if (!entityDTO) {
            return;
        }
        let entity = new BreakRuleCondition();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: BreakRuleCondition): BreakRuleConditionDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new BreakRuleConditionDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
