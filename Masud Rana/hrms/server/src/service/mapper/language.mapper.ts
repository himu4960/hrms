import { LanguageDTO } from '../dto/language.dto';

import { Language } from '../../domain/language.entity';

/**
 * A Language mapper object.
 */
export class LanguageMapper {
    static fromDTOtoEntity(entityDTO: LanguageDTO): Language {
        if (!entityDTO) {
            return;
        }
        const entity = new Language();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: Language): LanguageDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new LanguageDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
