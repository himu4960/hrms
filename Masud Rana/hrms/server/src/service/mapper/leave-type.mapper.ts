import { LeaveType } from '../../domain/leave-type.entity';
import { LeaveTypeDTO } from '../dto/leave-type.dto';

/**
 * A LeaveType mapper object.
 */
export class LeaveTypeMapper {
    static fromDTOtoEntity(entityDTO: LeaveTypeDTO): LeaveType {
        if (!entityDTO) {
            return;
        }
        let entity = new LeaveType();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach((field) => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: LeaveType): LeaveTypeDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new LeaveTypeDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach((field) => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
