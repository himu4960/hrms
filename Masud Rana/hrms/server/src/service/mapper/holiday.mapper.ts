import { Holiday } from '../../domain/holiday.entity';
import { HolidayDTO } from '../dto/holiday.dto';

/**
 * A Holiday mapper object.
 */
export class HolidayMapper {
    static fromDTOtoEntity(entityDTO: HolidayDTO): Holiday {
        if (!entityDTO) {
            return;
        }
        let entity = new Holiday();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach((field) => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: Holiday): HolidayDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new HolidayDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach((field) => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
