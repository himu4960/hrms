import { WorkShift } from '../../domain/work-shift.entity';
import { WorkShiftDTO } from '../dto/work-shift/work-shift.dto';

/**
 * A WorkShift mapper object.
 */
export class WorkShiftMapper {
    static fromDTOtoEntity(entityDTO: WorkShiftDTO): WorkShift {
        if (!entityDTO) {
            return;
        }
        let entity = new WorkShift();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: WorkShift): WorkShiftDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new WorkShiftDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
