import { BloodGroupDTO } from '../dto/blood-group/blood-group.dto';

import { BloodGroup } from '../../domain/blood-group.entity';

/**
 * A BloodGroup mapper object.
 */
export class BloodGroupMapper {
    static fromDTOtoEntity(entityDTO: BloodGroupDTO): BloodGroup {
        if (!entityDTO) {
            return;
        }
        const entity = new BloodGroup();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: BloodGroup): BloodGroupDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new BloodGroupDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
