import { Gender } from '../../domain/gender.entity';

import { GenderDTO } from '../dto/gender/gender.dto';

/**
 * A Gender mapper object.
 */
export class GenderMapper {
    static fromDTOtoEntity(entityDTO: GenderDTO): Gender {
        if (!entityDTO) {
            return;
        }
        const entity = new Gender();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: Gender): GenderDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new GenderDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
