import { Religion } from '../../domain/religion.entity';

import { ReligionDTO } from '../dto/religion/religion.dto';

/**
 * A Religion mapper object.
 */
export class ReligionMapper {
    static fromDTOtoEntity(entityDTO: ReligionDTO): Religion {
        if (!entityDTO) {
            return;
        }
        const entity = new Religion();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: Religion): ReligionDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new ReligionDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
