import { Menu } from '../../domain/menu.entity';

import { MenuDTO } from '../dto/menu.dto';

/**
 * A Menu mapper object.
 */
export class MenuMapper {
    static fromDTOtoEntity(entityDTO: MenuDTO): Menu {
        if (!entityDTO) {
            return;
        }
        const entity = new Menu();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach((field) => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: Menu): MenuDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new MenuDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach((field) => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
