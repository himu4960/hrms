import { CustomField } from '../../domain/custom-field.entity';

import { CustomFieldDTO } from '../dto/custom-field.dto';

/**
 * A CustomField mapper object.
 */
export class CustomFieldMapper {
    static fromDTOtoEntity(entityDTO: CustomFieldDTO): CustomField {
        if (!entityDTO) {
            return;
        }
        let entity = new CustomField();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach((field) => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: CustomField): CustomFieldDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new CustomFieldDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach((field) => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
