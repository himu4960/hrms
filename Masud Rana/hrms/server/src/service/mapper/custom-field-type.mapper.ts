import { CustomFieldType } from '../../domain/custom-field-type.entity';
import { CustomFieldTypeDTO } from '../dto/custom-field-type.dto';

/**
 * A CustomFieldType mapper object.
 */
export class CustomFieldTypeMapper {
    static fromDTOtoEntity(entityDTO: CustomFieldTypeDTO): CustomFieldType {
        if (!entityDTO) {
            return;
        }
        let entity = new CustomFieldType();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach((field) => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: CustomFieldType): CustomFieldTypeDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new CustomFieldTypeDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach((field) => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
