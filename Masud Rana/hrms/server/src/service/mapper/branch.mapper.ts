import { Branch } from '../../domain/branch.entity';

import { BranchDTO } from '../dto/branch/branch.dto';

/**
 * A Branch mapper object.
 */
export class BranchMapper {
    static fromDTOtoEntity(entityDTO: BranchDTO): Branch {
        if (!entityDTO) {
            return;
        }
        const entity = new Branch();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: Branch): BranchDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new BranchDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
