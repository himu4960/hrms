import { UserDevice } from "../../domain/user-device.entity";
import { UserDeviceDTO } from "../dto/user-device.dto";

/**
 * A UserDevice mapper object.
 */
export class UserDeviceMapper {
    static fromDTOtoEntity(entityDTO: UserDeviceDTO): UserDevice {
        if (!entityDTO) {
            return;
        }
        let entity = new UserDevice();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach((field) => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: UserDevice): UserDeviceDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new UserDeviceDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach((field) => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
