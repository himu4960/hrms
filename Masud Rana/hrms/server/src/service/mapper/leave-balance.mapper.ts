import { LeaveBalance } from "../../domain/leave-balance.entity";
import { LeaveBalanceDTO } from "../dto/leave/leave-balance.dto";

/**
 * A Leave Balance mapper object.
 */
export class LeaveBalanceMapper {
    static fromDTOtoEntity(entityDTO: LeaveBalanceDTO): LeaveBalance {
        if (!entityDTO) {
            return;
        }
        let entity = new LeaveBalance();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach((field) => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: LeaveBalance): LeaveBalanceDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new LeaveBalanceDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach((field) => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
