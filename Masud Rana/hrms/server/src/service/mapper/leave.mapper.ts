import { Leave } from '../../domain/leave.entity';

import { LeaveDTO } from '../dto/leave/leave.dto';

/**
 * A Leave mapper object.
 */
export class LeaveMapper {
    static fromDTOtoEntity(entityDTO: LeaveDTO): Leave {
        if (!entityDTO) {
            return;
        }
        let entity = new Leave();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach((field) => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: Leave): LeaveDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new LeaveDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach((field) => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
