import { MaritalStatusTranslation } from '../../domain/marital-status-translation.entity';

import { MaritalStatusTranslationDTO } from '../dto/marital-status/marital-status-translation.dto';

/**
 * A MaritalStatusTranslation mapper object.
 */
export class MaritalStatusTranslationMapper {
    static fromDTOtoEntity(entityDTO: MaritalStatusTranslationDTO): MaritalStatusTranslation {
        if (!entityDTO) {
            return;
        }
        const entity = new MaritalStatusTranslation();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: MaritalStatusTranslation): MaritalStatusTranslationDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new MaritalStatusTranslationDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
