import { Announcement } from '../../domain/announcement.entity';
import { AnnouncementDTO } from '../dto/announcement/announcement.dto';

/**
 * A Announcement mapper object.
 */
export class AnnouncementMapper {
    static fromDTOtoEntity(entityDTO: AnnouncementDTO): Announcement {
        if (!entityDTO) {
            return;
        }
        let entity = new Announcement();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach((field) => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: Announcement): AnnouncementDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new AnnouncementDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach((field) => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
