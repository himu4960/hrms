import { ReligionTranslation } from '../../domain/religion-translation.entity';

import { ReligionTranslationDTO } from '../dto/religion/religion-translation.dto';

/**
 * A ReligionTranslation mapper object.
 */
export class ReligionTranslationMapper {
    static fromDTOtoEntity(entityDTO: ReligionTranslationDTO): ReligionTranslation {
        if (!entityDTO) {
            return;
        }
        const entity = new ReligionTranslation();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: ReligionTranslation): ReligionTranslationDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new ReligionTranslationDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
