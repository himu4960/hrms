import { ApplicationType } from '../../domain/application-type.entity';

import { ApplicationTypeDTO } from '../dto/application-type.dto';

/**
 * A ApplicationType mapper object.
 */
export class ApplicationTypeMapper {
    static fromDTOtoEntity(entityDTO: ApplicationTypeDTO): ApplicationType {
        if (!entityDTO) {
            return;
        }
        const entity = new ApplicationType();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: ApplicationType): ApplicationTypeDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new ApplicationTypeDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
