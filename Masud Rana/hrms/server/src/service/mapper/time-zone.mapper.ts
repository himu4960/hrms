import { TimeZone } from '../../domain/time-zone.entity';

import { TimeZoneDTO } from '../dto/time-zone.dto';

/**
 * A TimeZone mapper object.
 */
export class TimeZoneMapper {
    static fromDTOtoEntity(entityDTO: TimeZoneDTO): TimeZone {
        if (!entityDTO) {
            return;
        }
        const entity = new TimeZone();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: TimeZone): TimeZoneDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new TimeZoneDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
