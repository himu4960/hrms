import { File } from '../../domain/file.entity';
import { FileDTO } from '../dto/file.dto';

/**
 * A File mapper object.
 */
export class FileMapper {
    static fromDTOtoEntity(entityDTO: FileDTO): File {
        if (!entityDTO) {
            return;
        }
        let entity = new File();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach((field) => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: File): FileDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new FileDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach((field) => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
