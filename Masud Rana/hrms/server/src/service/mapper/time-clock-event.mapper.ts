import { TimeClockEvent } from '../../domain/time-clock-event.entity';
import { TimeClockEventDTO } from '../dto/time-clock/time-clock-event.dto';

/**
 * A TimeClockEvent mapper object.
 */
export class TimeClockEventMapper {
    static fromDTOtoEntity(entityDTO: TimeClockEventDTO): TimeClockEvent {
        if (!entityDTO) {
            return;
        }
        let entity = new TimeClockEvent();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: TimeClockEvent): TimeClockEventDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new TimeClockEventDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
