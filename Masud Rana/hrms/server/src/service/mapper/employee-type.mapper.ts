import { EmployeeType } from '../../domain/employee-type.entity';

import { EmployeeTypeDTO } from '../dto/employee-type.dto';

/**
 * A EmployeeType mapper object.
 */
export class EmployeeTypeMapper {
    static fromDTOtoEntity(entityDTO: EmployeeTypeDTO): EmployeeType {
        if (!entityDTO) {
            return;
        }
        const entity = new EmployeeType();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: EmployeeType): EmployeeTypeDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new EmployeeTypeDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
