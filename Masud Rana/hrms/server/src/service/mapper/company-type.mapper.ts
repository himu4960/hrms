import { CompanyType } from '../../domain/company-type.entity';

import { CompanyTypeDTO } from '../dto/company-type.dto';

/**
 * A CompanyType mapper object.
 */
export class CompanyTypeMapper {
    static fromDTOtoEntity(entityDTO: CompanyTypeDTO): CompanyType {
        if (!entityDTO) {
            return;
        }
        const entity = new CompanyType();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: CompanyType): CompanyTypeDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new CompanyTypeDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
