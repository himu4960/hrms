import { BloodGroupTranslationDTO } from '../dto/blood-group/blood-group-translation.dto';

import { BloodGroupTranslation } from '../../domain/blood-group-translation.entity';

/**
 * A BloodGroupTranslation mapper object.
 */
export class BloodGroupTranslationMapper {
    static fromDTOtoEntity(entityDTO: BloodGroupTranslationDTO): BloodGroupTranslationDTO {
        if (!entityDTO) {
            return;
        }
        const entity = new BloodGroupTranslation();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: BloodGroupTranslationDTO): BloodGroupTranslationDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new BloodGroupTranslationDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
