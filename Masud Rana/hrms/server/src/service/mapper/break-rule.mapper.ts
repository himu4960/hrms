import { BreakRule } from '../../domain/break-rule.entity';
import { BreakRuleDTO } from '../dto/break-rule/break-rule.dto';

/**
 * A BreakRule mapper object.
 */
export class BreakRuleMapper {
    static fromDTOtoEntity(entityDTO: BreakRuleDTO): BreakRule {
        if (!entityDTO) {
            return;
        }
        let entity = new BreakRule();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: BreakRule): BreakRuleDTO {
        if (!entity) {
            return;
        }
        let entityDTO = new BreakRuleDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
