import { UserCompany } from '../../domain/user-company.entity';

import { UserCompanyDTO } from '../dto/user-company/user-company.dto';

/**
 * A UserCompany mapper object.
 */
export class UserCompanyMapper {
    static fromDTOtoEntity(entityDTO: UserCompanyDTO): UserCompany {
        if (!entityDTO) {
            return;
        }
        const entity = new UserCompany();
        const fields = Object.getOwnPropertyNames(entityDTO);
        fields.forEach(field => {
            entity[field] = entityDTO[field];
        });
        return entity;
    }

    static fromEntityToDTO(entity: UserCompany): UserCompanyDTO {
        if (!entity) {
            return;
        }
        const entityDTO = new UserCompanyDTO();

        const fields = Object.getOwnPropertyNames(entity);

        fields.forEach(field => {
            entityDTO[field] = entity[field];
        });

        return entityDTO;
    }
}
