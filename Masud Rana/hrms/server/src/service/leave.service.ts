import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, In, MoreThan } from 'typeorm';

import { LeaveDTO } from './dto/leave/leave.dto';
import { LeaveCreateDTO } from './dto/leave/leave-create.dto';
import { EmployeeDTO } from './dto/employee/employee.dto';
import { LeaveParamsDTO } from './dto/leave/leave-params.dto';
import { HolidayDTO } from './dto/holiday.dto';
import { LeaveDataDTO } from './dto/leave/leave-data.dto';
import { FilterLeaveDTO } from './dto/leave/leave-filter.dto';

import { LeaveMapper } from '../service/mapper/leave.mapper';

import { LeaveType } from '../domain/leave-type.entity';

import { LeaveRepository } from '../repository/leave.repository';
import { LeaveBalanceRepository } from '../repository/leave-balance.repository';
import { EmployeeRepository } from '../repository/employee.repository';
import { LeaveTypeRepository } from '../repository/leave-type.repository';
import { SettingsApplicationRepository } from '../repository/settings-application.repository';
import { HolidayRepository } from '../repository/holiday.repository';

import { NotificationService } from '../fcm/notification.service';
import { DateTimeConvertService } from '../shared/convert/date-time-convert.service';

import { ELeaveStatusType } from '../domain/enum/leave-status.enum';

import { DEFAULT_WORK_HOUR_IN_MINS, ERoleType } from '../shared/constant/util.constant';
import { LEAVE_RESPONSE } from '../shared/constant/response-message';

const relationshipNames = ['leaveType'];

@Injectable()
export class LeaveService {
    logger = new Logger('LeaveService');

    constructor(
        @InjectRepository(LeaveRepository) private leaveRepository: LeaveRepository,
        @InjectRepository(LeaveTypeRepository) private leaveTypeRepository: LeaveTypeRepository,
        @InjectRepository(EmployeeRepository) private employeeRepository: EmployeeRepository,
        @InjectRepository(HolidayRepository) private holidayRepository: HolidayRepository,
        @InjectRepository(SettingsApplicationRepository)
        private settingsApplicationRepository: SettingsApplicationRepository,
        private leaveBalanceRepository: LeaveBalanceRepository,
        private readonly notificationService: NotificationService,
        private readonly dateTimeConvertService: DateTimeConvertService,
    ) {}

    async findById(id: number): Promise<LeaveDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.leaveRepository.findOne(id, options);
        return LeaveMapper.fromEntityToDTO(result);
    }

    async getLeaveData(leaveParamsDTO: LeaveParamsDTO, company_id: number): Promise<LeaveDataDTO | undefined> {
        const { employee_id } = leaveParamsDTO;
        let options = {
            relations: relationshipNames,
            where: {
                company_id: company_id,
                employee_id,
            },
        };

        const pending_leaves = await this.leaveRepository.find({
            ...options,
            where: { ...options.where, status: ELeaveStatusType.PENDING, is_deleted: false },
        });
        const upcoming_leaves = await this.leaveRepository.find({
            ...options,
            where: {
                ...options.where,
                status: In([ELeaveStatusType.APPROVED, ELeaveStatusType.REQUEST_REJECTED]),
                is_deleted: false,
                end_date: MoreThan(Date.now()),
            },
        });
        const leave_balances = await this.leaveBalanceRepository.find(options);

        const leaveData = {
            pending_leaves,
            upcoming_leaves,
            leave_balances,
        };

        return leaveData;
    }

    async findForEmployee(options: FindManyOptions<LeaveDTO>): Promise<LeaveDTO[] | undefined> {
        options.relations = relationshipNames;
        return await this.leaveRepository.find(options);
    }

    async findAndCount(options: FindManyOptions<LeaveDTO>, company_id: number): Promise<[LeaveDTO[], number]> {
        options.relations = relationshipNames;
        options.where = { company_id, is_deleted: false, status: ELeaveStatusType.PENDING };
        return await this.leaveRepository.findAndCount(options);
    }

    async getTotalLeaveRequestCount(company_id: number): Promise<number> {
        const currentTime = new Date(Date.now()).getTime();
        return await this.leaveRepository
            .createQueryBuilder('leaves')
            .where('leaves.company_id = :company_id', { company_id })
            .andWhere('leaves.status IN (:status) AND leaves.is_deleted = false', {
                status: [ELeaveStatusType.PENDING, ELeaveStatusType.REQUESTED],
            })
            .andWhere('leaves.end_date > :currentTime', { currentTime })
            .getCount();
    }

    async findAndCountOnLeaves(options: FindManyOptions<LeaveDTO>, company_id: number): Promise<[LeaveDTO[], number]> {
        const currentTime = new Date(Date.now()).getTime();
        return await this.leaveRepository
            .createQueryBuilder('leaves')
            .leftJoinAndSelect('leaves.employee', 'employee')
            .leftJoinAndSelect('employee.employee_designations', 'employee_designations')
            .leftJoinAndSelect('employee_designations.designation', 'designation')
            .leftJoinAndSelect('leaves.leaveType', 'leaveType')
            .select([
                'leaves',
                'leaveType.id',
                'leaveType.name',
                'employee.id',
                'employee.first_name',
                'employee.last_name',
                'employee.photo_url',
                'employee_designations',
                'designation',
            ])
            .take(options.take)
            .skip(options.skip)
            .where('leaves.company_id = :company_id', { company_id })
            .andWhere('leaves.is_deleted = false')
            .andWhere('leaves.status = status', { status: ELeaveStatusType.APPROVED })
            .andWhere('leaves.start_date < :currentTime AND leaves.end_date > :currentTime', { currentTime })
            .groupBy('leaves.id')
            .addGroupBy('employee.id')
            .addGroupBy('employee_designations.id')
            .getManyAndCount();
    }

    async getAllOnLeaves(company_id: number): Promise<LeaveDTO[]> {
        const currentTime = new Date(Date.now()).getTime();
        return await this.leaveRepository
            .createQueryBuilder('leaves')
            .leftJoinAndSelect('leaves.employee', 'employee')
            .leftJoinAndSelect('employee.employee_designations', 'employee_designations')
            .leftJoinAndSelect('employee_designations.designation', 'designation')
            .leftJoinAndSelect('leaves.leaveType', 'leaveType')
            .select([
                'leaves',
                'leaveType.id',
                'leaveType.name',
                'employee.id',
                'employee.first_name',
                'employee.last_name',
                'employee.photo_url',
                'employee_designations',
                'designation',
            ])
            .addSelect(['employee.photo_url'])
            .where('leaves.company_id = :company_id', { company_id })
            .andWhere('leaves.is_deleted = false')
            .andWhere('leaves.is_approved = true')
            .andWhere('leaves.is_review_accepted = false')
            .andWhere('leaves.start_date < :currentTime AND leaves.end_date > :currentTime', { currentTime })
            .getMany();
    }

    async filter(filterLeaveDTO: FilterLeaveDTO, company_id: number): Promise<EmployeeDTO[] | undefined> {
        const { branch_id, designation_id, start_date, end_date, is_approved } = filterLeaveDTO;
        const status = [
            ELeaveStatusType.PENDING,
            ELeaveStatusType.REQUESTED,
            ELeaveStatusType.APPROVED,
            ELeaveStatusType.REQUEST_APPROVED,
        ];
        const filter_start_date = new Date(start_date);
        filter_start_date.setDate(filter_start_date.getDate() - 1);

        const filter_end_date = new Date(end_date);
        filter_end_date.setDate(filter_end_date.getDate() + 1);

        let employee = this.employeeRepository
            .createQueryBuilder('employee')
            .leftJoinAndSelect(
                'employee.leaves',
                'leave',
                `${start_date && end_date ? 'leave.start_date > :start_date AND leave.end_date < :end_date' : 'TRUE'}`,
                {
                    start_date: filter_start_date.getTime(),
                    end_date: filter_end_date.getTime(),
                },
            )
            .leftJoinAndSelect('leave.leaveType', 'leave_type')
            .where('employee.company_id = :company_id', { company_id })
            .andWhere('leave.is_deleted = false')
            .andWhere('leave.status IN (:status)', { status })
            .andWhere(
                'leave.is_approved = :is_approved OR leave.is_review_requested = true AND leave.is_review_accepted = false',
                { is_approved },
            );

        if (designation_id) {
            employee
                .leftJoinAndSelect('employee.employee_designations', 'employee_designation')
                .andWhere('employee_designation.designation_id = :designation_id', {
                    designation_id,
                });
        }
        if (branch_id) {
            employee.andWhere('employee.branch_id = :branch_id', { branch_id });
        }
        const employees: EmployeeDTO[] = await employee
            .select(['employee.first_name', 'employee.last_name'])
            .addSelect([
                'leave.start_date as start_date',
                'leave.end_date as end_date',
                'leave.is_approved as is_approved',
                'leave.status as status',
                'leave.total_leave_duration_mins as total_leave_duration_mins',
                'leave.updated_at as updated_at',
                'leave.comments as comments',
                'leave.approved_at as approved_at',
                'leave.approved_by as approved_by',
                'leave.approved_note as approved_note',
                'leave.id as leave_id',
                'leave.is_deleted as is_deleted',
                'leave.is_partial_day_allowed as is_partial_day_allowed',
                'leave.is_rejected as is_rejected',
                'leave.rejected_at as rejected_at',
                'leave.rejected_by as rejected_by',
                'leave.rejected_note as rejected_note',
                'leave_type.id',
                'leave_type.name',
                'leave.is_review_accepted as is_review_accepted',
            ])
            .getRawMany();
        return employees;
    }

    async save(leaveDTO: LeaveCreateDTO, company_id: number, creator?: number): Promise<LeaveDTO | undefined> {
        try {
            let notificationBody = '';

            const entity = LeaveMapper.fromDTOtoEntity(leaveDTO);

            const employee = await this.employeeRepository.findOne(leaveDTO.employee_id);
            const leaveType = await this.leaveTypeRepository.findOne(entity.leave_type_id);

            const employeeFullName = `${employee.first_name} ${employee.last_name}`;

            const leaveStartDate = this.dateTimeConvertService.dateDisplayFormat(entity.start_date);
            const leaveEndDate = this.dateTimeConvertService.dateDisplayFormat(entity.end_date);

            const startDate = new Date(entity.start_date);
            const endDate = new Date(entity.end_date);
            const [holidays, count] = await this.holidayRepository.findAndCount({});

            entity.status = ELeaveStatusType.PENDING;

            if (entity.is_partial_day_allowed) {
                const differenceInSeconds = (entity.end_date - entity.start_date) / 1000;
                entity.total_leave_duration_mins = Math.floor(differenceInSeconds / 60);
                notificationBody = `${employeeFullName} has requested leave for ${leaveStartDate}. The type of leave requested is ${leaveType.name}`;
            } else {
                const countWeekendAndHoliDay = await this.findWeekendAndHolidays(
                    startDate,
                    endDate,
                    leaveDTO.weekend_day,
                    holidays,
                );
                const differenceInSeconds = this.dateTimeConvertService.getDateDifferenceInSeconds(
                    new Date(entity.end_date).getTime(),
                    new Date(entity.start_date).getTime(),
                );

                entity.total_leave_duration_mins =
                    differenceInSeconds / 60 - countWeekendAndHoliDay * DEFAULT_WORK_HOUR_IN_MINS;

                notificationBody = `${employeeFullName} has requested leave for ${leaveStartDate} to ${leaveEndDate}. The type of leave requested is ${leaveType.name}`;
            }

            if (creator) {
                if (!entity.created_by) {
                    entity.created_by = creator;
                    // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                    entity.created_at = Date.now();
                }
                entity.updated_by = creator;
            }
            const result = await this.leaveRepository.save(entity);

            const deviceTokens = await this.notificationService.getDeviceTokensByRole(ERoleType.MANAGER, company_id);

            if (deviceTokens?.length) {
                const payload = {
                    notification: {
                        title: 'New Leave Request Notification',
                        body: notificationBody,
                    },
                };

                await this.notificationService.sendNotification(deviceTokens, payload);
            }

            return LeaveMapper.fromEntityToDTO(result);
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async update(leaveDTO: LeaveDTO, updater?: number): Promise<LeaveDTO | undefined> {
        const entity = LeaveMapper.fromDTOtoEntity(leaveDTO);
        const startDate = new Date(entity.start_date);
        const endDate = new Date(entity.end_date);

        if (entity.is_partial_day_allowed) {
            entity.total_leave_duration_mins = Math.floor((entity.end_date - entity.start_date) / 60);
        } else {
            entity.total_leave_duration_mins =
                this.dateTimeConvertService.getDateDifferenceInSeconds(endDate.getTime(), startDate.getTime()) / 60;
        }

        if (updater) {
            entity.updated_by = updater;
            entity.updated_at = Date.now();
        }
        const result = await this.leaveRepository.save(entity);
        return LeaveMapper.fromEntityToDTO(result);
    }

    async approveLeave(leaveDTO: LeaveDTO, approver?: number) {
        let notificationBody = '';

        const entity = LeaveMapper.fromDTOtoEntity(leaveDTO);

        const startDate = this.dateTimeConvertService.dateDisplayFormat(entity.start_date);
        const endDate = this.dateTimeConvertService.dateDisplayFormat(entity.end_date);

        const applicationSettings = await this.settingsApplicationRepository.findOne({
            where: { company_id: entity.company_id },
        });

        const leaveType: LeaveType = await this.leaveTypeRepository.findOne(entity.leaveType.id);
        const [leaves] = await this.findAndCountOnLeaves({}, entity.company_id);
        const onLeaveCount: number = leaves.filter(
            leave =>
                (leave.start_date <= entity.start_date && leave.end_date >= entity.start_date) ||
                (leave.start_date >= entity.start_date && leave.start_date <= entity.end_date),
        ).length;

        const isAllowForApproved: boolean =
            !leaveType.is_max_employee_allowed_enabled || leaveType.max_employee_allowed >= onLeaveCount;

        if (!entity.is_approved && entity.status == ELeaveStatusType.PENDING && isAllowForApproved) {
            if (approver) {
                entity.status = ELeaveStatusType.APPROVED;
                entity.approved_by = approver;
                entity.is_approved = true;
                entity.approved_at = Date.now();
                entity.updated_at = Date.now();
            }
            const result = await this.leaveRepository.save(entity);

            const options = {
                where: {
                    company_id: entity.company_id,
                    employee_id: entity.employee_id,
                    leave_type_id: entity.leave_type_id,
                },
            };
            const leaveBalance = await this.leaveBalanceRepository.findOne(options);

            if (result.is_partial_day_allowed) {
                const partialTotalLeaveDuration =
                    ((24 / applicationSettings.office_working_hours) * (entity.end_date - entity.start_date)) /
                    1000 /
                    60;
                leaveBalance.remaining_leave_mins = leaveBalance.remaining_leave_mins - partialTotalLeaveDuration;

                notificationBody = `Your leave request for ${startDate}, has been approved`;
            } else {
                leaveBalance.remaining_leave_mins =
                    leaveBalance.remaining_leave_mins - entity.total_leave_duration_mins;
                notificationBody = `Your leave request for ${startDate} to ${endDate}, has been approved`;
            }
            await this.leaveBalanceRepository.save(leaveBalance);

            const deviceTokens = await this.notificationService.getDeviceTokens({
                where: {
                    employee_id: entity.employee_id,
                    company_id: entity.company_id,
                },
            });

            const payload = {
                notification: {
                    title: 'Leave request Approved',
                    body: notificationBody,
                },
            };

            await this.notificationService.sendNotification(deviceTokens, payload);

            return LeaveMapper.fromEntityToDTO(result);
        } else {
            throw new HttpException(LEAVE_RESPONSE.APPROVE.ERROR, HttpStatus.PRECONDITION_FAILED);
        }
    }

    async reviewRequest(leaveDTO: LeaveDTO): Promise<LeaveDTO> {
        const entity = LeaveMapper.fromDTOtoEntity(leaveDTO);
        entity.is_review_requested = true;
        entity.status = ELeaveStatusType.REQUESTED;
        return await this.leaveRepository.save(entity);
    }

    async reviewApprove(leaveDTO: LeaveDTO): Promise<LeaveDTO> {
        try {
            let notificationBody = '';

            const entity = LeaveMapper.fromDTOtoEntity(leaveDTO);

            const startDate = this.dateTimeConvertService.dateDisplayFormat(entity.start_date);
            const endDate = this.dateTimeConvertService.dateDisplayFormat(entity.end_date);

            entity.is_review_accepted = true;
            entity.is_approved = true;
            entity.status = ELeaveStatusType.REQUEST_APPROVED;

            const options = {
                where: {
                    company_id: entity.company_id,
                    employee_id: entity.employee_id,
                    leave_type_id: entity.leaveType.id,
                },
            };

            const leaveBalance = await this.leaveBalanceRepository.findOne(options);
            leaveBalance.remaining_leave_mins = leaveBalance.remaining_leave_mins + entity.total_leave_duration_mins;
            await this.leaveBalanceRepository.save(leaveBalance);

            const result = await this.leaveRepository.save(entity);

            if (result.is_partial_day_allowed) {
                notificationBody = `Your leave review request for ${startDate}, has been approved`;
            } else {
                notificationBody = `Your leave review request for ${startDate} to ${endDate}, has been approved `;
            }

            const deviceTokens = await this.notificationService.getDeviceTokens({
                where: {
                    employee_id: entity.employee_id,
                    company_id: entity.company_id,
                },
            });

            const payload = {
                notification: {
                    title: 'Leave review request Approved',
                    body: notificationBody,
                },
            };

            await this.notificationService.sendNotification(deviceTokens, payload);

            return result;
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async reviewReject(leaveDTO: LeaveDTO): Promise<LeaveDTO> {
        try {
            let notificationBody = '';

            const entity = LeaveMapper.fromDTOtoEntity(leaveDTO);

            const startDate = this.dateTimeConvertService.dateDisplayFormat(entity.start_date);
            const endDate = this.dateTimeConvertService.dateDisplayFormat(entity.end_date);

            entity.is_approved = true;
            entity.status = ELeaveStatusType.REQUEST_REJECTED;

            const result = await this.leaveRepository.save(entity);

            if (result.is_partial_day_allowed) {
                notificationBody = `Your leave review request for ${startDate}, has been rejected`;
            } else {
                notificationBody = `Your leave review request for ${startDate} to ${endDate}, has been rejected `;
            }

            const deviceTokens = await this.notificationService.getDeviceTokens({
                where: {
                    employee_id: entity.employee_id,
                    company_id: entity.company_id,
                },
            });

            const payload = {
                notification: {
                    title: 'Leave review request Rejected',
                    body: notificationBody,
                },
            };

            await this.notificationService.sendNotification(deviceTokens, payload);

            return result;
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async rejectLeave(leaveDTO: LeaveDTO, rejecter?: number) {
        try {
            let notificationBody = '';

            const entity = LeaveMapper.fromDTOtoEntity(leaveDTO);

            const startDate = this.dateTimeConvertService.dateDisplayFormat(entity.start_date);
            const endDate = this.dateTimeConvertService.dateDisplayFormat(entity.end_date);

            if (
                !entity.is_rejected &&
                (entity.status == ELeaveStatusType.PENDING || entity.status == ELeaveStatusType.REQUESTED)
            ) {
                if (rejecter) {
                    entity.status = ELeaveStatusType.REJECTED;
                    entity.rejected_by = rejecter;
                    entity.is_rejected = true;
                    entity.rejected_at = Date.now();
                    entity.updated_at = Date.now();
                }
                const result = await this.leaveRepository.save(entity);

                if (result.is_partial_day_allowed) {
                    notificationBody = `Your leave  request for ${startDate}, has been rejected`;
                } else {
                    notificationBody = `Your leave  request for ${startDate} to ${endDate}, has been rejected `;
                }

                const deviceTokens = await this.notificationService.getDeviceTokens({
                    where: {
                        employee_id: entity.employee_id,
                        company_id: entity.company_id,
                    },
                });

                const payload = {
                    notification: {
                        title: 'Leave request Rejected',
                        body: notificationBody,
                    },
                };

                await this.notificationService.sendNotification(deviceTokens, payload);

                return LeaveMapper.fromEntityToDTO(result);
            }
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async cancelRequest(leaveDTO: LeaveDTO, deleter?: number): Promise<LeaveDTO | undefined> {
        const entity = LeaveMapper.fromDTOtoEntity(leaveDTO);
        if (!entity.is_deleted) {
            if (deleter) {
                entity.deleted_by = deleter;
                entity.deleted_at = Date.now();
                entity.is_deleted = true;
            }
            return await this.leaveRepository.save(entity);
        }
    }

    private async findWeekendAndHolidays(
        startDate: Date,
        endDate: Date,
        weekendDays: number[],
        holidays: HolidayDTO[],
    ) {
        let countWeekEndDay = 0;
        const totalDays = this.dateTimeConvertService.getDateDifferenceInDays(endDate, startDate);
        let isWeekendOrHoliday = false;
        for (let i = 0; i < totalDays; i++) {
            if (holidays?.length) {
                isWeekendOrHoliday =
                    (await holidays.findIndex(holiDay => {
                        let isHoliday = false;
                        const holiDayDate = holiDay?.date ? new Date(holiDay?.date) : undefined;
                        if (holiDayDate) {
                            isHoliday =
                                holiDayDate.getDay() === startDate.getDay() &&
                                holiDayDate.getMonth() === startDate.getMonth() &&
                                holiDayDate.getFullYear() === startDate.getFullYear();
                        }
                        return isHoliday;
                    })) !== -1;

                if (!isWeekendOrHoliday && weekendDays?.length) {
                    isWeekendOrHoliday = weekendDays.includes(startDate.getDay());
                }
            }

            if (!holidays.length && weekendDays?.length) {
                isWeekendOrHoliday = weekendDays.includes(startDate.getDay());
            }

            if (isWeekendOrHoliday) {
                countWeekEndDay++;
            }
            startDate.setDate(startDate.getDate() + 1);
        }
        return countWeekEndDay;
    }
}
