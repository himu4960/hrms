import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';

import { CustomFieldDTO } from '../service/dto/custom-field.dto';

import { CustomFieldMapper } from '../service/mapper/custom-field.mapper';

import { CustomFieldRepository } from '../repository/custom-field.repository';

const relationshipNames = ['company', 'custom_field_type'];

@Injectable()
export class CustomFieldService {
    logger = new Logger('CustomFieldService');

    constructor(@InjectRepository(CustomFieldRepository) private customFieldRepository: CustomFieldRepository) {}

    async findById(id: number): Promise<CustomFieldDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.customFieldRepository.findOne(id, options);
        return CustomFieldMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<CustomFieldDTO>): Promise<CustomFieldDTO | undefined> {
        const result = await this.customFieldRepository.findOne(options);
        return CustomFieldMapper.fromEntityToDTO(result);
    }

    async findAndCount(
        options: FindManyOptions<CustomFieldDTO>,
        company_id: number,
    ): Promise<[CustomFieldDTO[], number]> {
        options.relations = relationshipNames;
        options.where = { company_id };
        return await this.customFieldRepository.findAndCount(options);
    }

    async save(customFieldDTO: CustomFieldDTO, creator?: number): Promise<CustomFieldDTO | undefined> {
        const entity = CustomFieldMapper.fromDTOtoEntity(customFieldDTO);
        if (creator) {
            if (!entity.created_by) {
                entity.created_by = creator;
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                entity.created_at = Date.now();
            }
            entity.updated_by = creator;
        }
        const result = await this.customFieldRepository.save(entity);
        return CustomFieldMapper.fromEntityToDTO(result);
    }

    async update(customFieldDTO: CustomFieldDTO, updater?: number): Promise<CustomFieldDTO | undefined> {
        const entity = CustomFieldMapper.fromDTOtoEntity(customFieldDTO);
        if (updater) {
            entity.updated_by = updater;
            entity.updated_at = Date.now();
        }
        const result = await this.customFieldRepository.save(entity);
        return CustomFieldMapper.fromEntityToDTO(result);
    }

    async deleteById(id: number): Promise<CustomFieldDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('Custom field is not found!', HttpStatus.NOT_FOUND);
        }
        await this.customFieldRepository.delete(id);
        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Custom field cannot be deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }
}
