import { Injectable, Logger, NotFoundException } from '@nestjs/common';

import { Workbook } from 'exceljs';
import { Response } from 'express-serve-static-core';

import dayjs from 'dayjs';

import { DateTimeConvertService } from '../shared/convert/date-time-convert.service';
import { ExportService } from '../shared/export/export.service';

import { EMPLOYEE_RESPONSE } from '../shared/constant/response-message';
import { EMPLOYEE_EXPORT_HIDEN_FIELDS } from '../shared/constant/util.constant';

const CsvParser = require('json2csv').Parser;

@Injectable()
export class EmployeeExportService {
    logger = new Logger('EmployeeExportService');

    constructor(
        private readonly dateTimeConvertService: DateTimeConvertService,
        private readonly exportService: ExportService,
    ) {}

    exportEmployeesXLSX(rows: any[], workSheetName: string, res: Response) {
        if (!rows || rows.length <= 0) {
            throw new NotFoundException(EMPLOYEE_RESPONSE.EXPORT.NOT_FOUND);
        }

        const today = this.dateTimeConvertService.format(dayjs());

        const workbook = new Workbook();
        const worksheet = workbook.addWorksheet(workSheetName);

        const columns = [];
        const headers = Object.keys(rows[0]);

        for (let index = 0; index < headers.length; index++) {
            const hidenField = EMPLOYEE_EXPORT_HIDEN_FIELDS.includes(headers[index]);
            if (!hidenField) {
                columns.push({
                    header: this.exportService.buildHeader(headers[index]),
                    key: headers[index],
                    width: 20,
                });
            }
        }

        const newRows = rows.map(singleRow => ({
            ...singleRow,
            is_active: singleRow?.is_active ? 'Activated' : 'Deactivated',
            join_date: this.dateTimeConvertService.dateDisplayFormat(new Date(Number(singleRow.join_date))),
            birth_date: this.dateTimeConvertService.dateDisplayFormat(new Date(Number(singleRow.birth_date))),
        }));

        worksheet.columns = columns;
        worksheet.addRows(newRows);

        worksheet.getRow(1).eachCell(cell => {
            cell.font = { bold: true };
        });

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', 'attachment; filename=' + `${today}_${workSheetName}`);

        return workbook.xlsx.write(res);
    }

    exportEmployeesCSV(rows: any[], workSheetName: string, res: Response) {
        if (!rows || rows.length <= 0) {
            throw new NotFoundException(EMPLOYEE_RESPONSE.EXPORT.NOT_FOUND);
        }

        const today = this.dateTimeConvertService.format(dayjs());
        const csvFields = [];

        Object.keys(rows[0]).map(key => {
            csvFields.push(key);
        });

        const csvParser = new CsvParser({ csvFields });

        const newRows = rows.map(singleRow => ({
            ...singleRow,
            join_date: this.dateTimeConvertService.dateDisplayFormat(new Date(Number(singleRow.join_date))),
            birth_date: this.dateTimeConvertService.dateDisplayFormat(new Date(Number(singleRow.birth_date))),
        }));
        const csvData = csvParser.parse(newRows);

        res.setHeader('Content-Type', 'text/csv');
        res.setHeader('Content-Disposition', 'attachment; filename=' + `${today}_${workSheetName}`);

        return res.end(csvData);
    }
}
