import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';

import { MenuDTO } from '../service/dto/menu.dto';

import { MenuMapper } from '../service/mapper/menu.mapper';

import { MenuRepository } from '../repository/menu.repository';

@Injectable()
export class MenuService {
    logger = new Logger('MenuService');

    constructor(@InjectRepository(MenuRepository) private menuRepository: MenuRepository) {}

    async findById(id: number): Promise<MenuDTO | undefined> {
        const result = await this.menuRepository.findOne(id);
        return MenuMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<MenuDTO>): Promise<MenuDTO | undefined> {
        const result = await this.menuRepository.findOne(options);
        return MenuMapper.fromEntityToDTO(result);
    }

    async findAndCount(): Promise<[MenuDTO[], number]> {
        return await this.menuRepository.findAndCount();
    }

    async save(menuDTO: MenuDTO, creator?: number): Promise<MenuDTO | undefined> {
        const entity = MenuMapper.fromDTOtoEntity(menuDTO);
        if (creator) {
            if (!entity.created_by) {
                entity.created_by = creator;
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                entity.created_at = Date.now();
            }
            entity.updated_by = creator;
        }
        const result = await this.menuRepository.save(entity);
        return MenuMapper.fromEntityToDTO(result);
    }

    async update(menuDTO: MenuDTO, updater?: number): Promise<MenuDTO | undefined> {
        const entity = MenuMapper.fromDTOtoEntity(menuDTO);
        if (updater) {
            entity.updated_by = updater;
            entity.updated_at = Date.now();
        }
        const result = await this.menuRepository.save(entity);
        return MenuMapper.fromEntityToDTO(result);
    }

    async deleteById(id: number): Promise<MenuDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('Menu is not found!', HttpStatus.NOT_FOUND);
        }
        await this.menuRepository.delete(id);
        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Menu cannot be deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }
}
