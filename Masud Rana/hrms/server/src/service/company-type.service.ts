import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions } from 'typeorm';

import { CompanyTypeDTO } from '../service/dto/company-type.dto';

import { CompanyTypeRepository } from '../repository/company-type.repository';

import { CompanyTypeMapper } from './mapper/company-type.mapper';

@Injectable()
export class CompanyTypeService {
    logger = new Logger('CompanyTypeService');

    constructor(@InjectRepository(CompanyTypeRepository) private companyTypeRepository: CompanyTypeRepository) {}

    async findById(id: number): Promise<CompanyTypeDTO | undefined> {
        const result = await this.companyTypeRepository.findOne(id);
        return CompanyTypeMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<CompanyTypeDTO>): Promise<CompanyTypeDTO | undefined> {
        const result = await this.companyTypeRepository.findOne(options);
        return CompanyTypeMapper.fromEntityToDTO(result);
    }

    async getAll(): Promise<CompanyTypeDTO[]> {
        return await this.companyTypeRepository.find({
            select: ['id', 'name', 'is_active', 'is_deleted'],
        });
    }

    async getActiveCompanyTypes(): Promise<CompanyTypeDTO[]> {
        return await this.companyTypeRepository.find({
            select: ['id', 'name'],
            where: {
                is_active: true,
                is_deleted: false,
            },
        });
    }
}
