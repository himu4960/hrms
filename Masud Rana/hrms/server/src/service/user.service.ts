import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions, getRepository } from 'typeorm';

import { transformPassword } from '../security';

import { UserDTO } from './dto/user.dto';

import { UserRepository } from '../repository/user.repository';

import { UserMapper } from './mapper/user.mapper';

import { UserPassword } from '../domain/user-password.entity';

import { TABLE_USER_PASSWORD } from '../shared/constant/database.constant';

@Injectable()
export class UserService {
    constructor(@InjectRepository(UserRepository) private userRepository: UserRepository) {}

    async findById(id: number): Promise<UserDTO | undefined> {
        const result = await this.userRepository.findOne(id);
        return UserMapper.fromEntityToDTO(result);
    }

    async findUserWithCompaniesById(id: number): Promise<any | undefined> {
        return await this.userRepository
            .createQueryBuilder('user')
            .leftJoinAndSelect('user.user_companies', 'user_companies')
            .leftJoinAndSelect('user_companies.company', 'company')
            .leftJoinAndSelect('user_companies.role', 'role')
            .leftJoinAndSelect('user_companies.employee', 'employee')
            .leftJoinAndSelect('employee.employee_designations', 'employee_designations')
            .leftJoinAndSelect('employee_designations.designation', 'designation')
            .select([
                'user',
                'user_companies.is_owner',
                'user_companies.company',
                'user_companies.role',
                'user_companies.is_active',
            ])
            .addSelect([
                'company',
                'role.id',
                'role.name',
                'employee',
                'employee_designations',
                'designation',
                'employee.branch_id',
            ])
            .where('user.id=:id', { id })
            .getOne();
    }

    async findByFields(options: FindOneOptions<UserDTO>): Promise<UserDTO | undefined> {
        options.relations = ['user_companies'];
        const result = await this.userRepository.findOne(options);
        return UserMapper.fromEntityToDTO(result);
    }

    async find(options: any): Promise<UserDTO | undefined> {
        const result = await this.userRepository.findOne(options);
        return UserMapper.fromEntityToDTO(result);
    }

    async findByEmail(email: string): Promise<UserDTO | undefined> {
        const result = await this.userRepository
            .createQueryBuilder('users')
            .where('users.email = :email', { email })
            .getOne();
        return UserMapper.fromEntityToDTO(result);
    }

    async findAll(): Promise<any | undefined> {
        return await this.userRepository
            .createQueryBuilder('user')
            .leftJoinAndSelect('user.user_companies', 'user_companies')
            .leftJoinAndSelect('user_companies.company', 'company')
            .leftJoinAndSelect('user_companies.role', 'role')
            .select(['user', 'user_companies.is_owner', 'user_companies.company', 'user_companies.role'])
            .addSelect(['company', 'role.id', 'role.name'])
            .getMany();
    }

    async findAndCount(options: FindManyOptions<UserDTO>): Promise<[UserDTO[], number]> {
        options.relations = ['user_companies'];
        const resultList = await this.userRepository.findAndCount(options);
        const usersDTO: UserDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach(user => usersDTO.push(UserMapper.fromEntityToDTO(user)));
            resultList[0] = usersDTO;
        }
        return resultList;
    }

    async save(userDTO: UserDTO, creator?: number, updatePassword = false): Promise<UserDTO | undefined> {
        const user = UserMapper.fromDTOtoEntity(userDTO);

        if (updatePassword) {
            await transformPassword(user);
        }
        if (creator) {
            if (!user.created_by) {
                user.created_by = creator;
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                user.created_at = Date.now();
            }
            user.updated_by = creator;
            user.updated_at = Date.now();
        }
        if (user.password) {
            const userPasswordRepository = getRepository(TABLE_USER_PASSWORD);
            const userPassword: UserPassword = {
                user_id: user.id,
                password: user.password,
            };
            await userPasswordRepository.save(userPassword);
        }

        const result = await this.userRepository.save(user);
        return UserMapper.fromEntityToDTO(result);
    }

    async update(userDTO: UserDTO, updater?: number): Promise<UserDTO | undefined> {
        return this.save(userDTO, updater);
    }

    async delete(userId: number): Promise<UserDTO | undefined> {
        const entityFind = await this.findById(userId);
        if (!entityFind) {
            throw new HttpException('User is not found!', HttpStatus.NOT_FOUND);
        }
        await this.userRepository.delete(userId);
        const afterDeletedEntity = await this.findById(userId);
        if (afterDeletedEntity) {
            throw new HttpException('User cannot be deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }
}
