import { ApiModelProperty } from '@nestjs/swagger';

/**
 * A TimeSheetLateSummaryDTO  object.
 */
export class TimeSheetLateSummaryDTO {
    @ApiModelProperty({ example: 1, description: 'employee_id field' })
    employee_id: string;

    @ApiModelProperty({ example: 'Habiba Eva', description: 'employee_name field' })
    employee_name: string;

    @ApiModelProperty({ example: 'UI/UX Designer', description: 'designation_name field' })
    designation_name: string;

    @ApiModelProperty({ example: new Date().toISOString(), description: 'date field' })
    date: string;

    @ApiModelProperty({ example: '09: 00 AM', description: 'shift_start field' })
    shift_start: string;

    @ApiModelProperty({ example: '09: 00 AM', description: 'actual_start field' })
    actual_start: string;

    @ApiModelProperty({ example: '09: 00 AM', description: 'shift_end field' })
    shift_end: string;

    @ApiModelProperty({ example: '09: 00 AM', description: 'actual_end field' })
    actual_end: string;

    @ApiModelProperty({ example: 'Absent', description: 'late_status field' })
    late_status: string;

    @ApiModelProperty({ example: '', description: 'late_status field' })
    late_by: string;
}
