import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

/**
 * A ReportFilterDTO  object.
 */
export class ReportFilterDTO {
    @IsOptional()
    @ApiModelProperty({ type: 'Date', example: '2022-09-20', description: 'start_date field' })
    start_date?: Date;

    @IsOptional()
    @ApiModelProperty({ type: 'Date', example: '2022-09-20', description: 'end_date field' })
    end_date?: Date;

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'employee_id field' })
    employee_id?: number;

    @ApiModelProperty({ example: 1, description: 'branch_id field' })
    branch_id?: number;

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'designation_id field' })
    designation_id?: number;

    @IsOptional()
    @ApiModelProperty({ description: 'is_export field', default: false })
    is_export?: boolean;
}
