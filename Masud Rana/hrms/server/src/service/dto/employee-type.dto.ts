/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, Max } from 'class-validator';

import { BaseDTO } from './base.dto';

/**
 * A EmployeeTypeDTO object.
 */
export class EmployeeTypeDTO extends BaseDTO {
    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ description: 'name field', required: false })
    name: string;

    @IsNotEmpty()
    @Max(20)
    @ApiModelProperty({ description: 'company_id field' })
    company_id?: number;

    @ApiModelProperty({ description: 'is_active field', required: false })
    is_active: boolean;

    @ApiModelProperty({ description: 'is_deleted field', required: false })
    is_deleted: boolean;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
