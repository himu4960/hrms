/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';

import { IsNotEmpty, MaxLength, IsOptional } from 'class-validator';

import { BaseDTO } from './base.dto';

/**
 * A PermissionTypeDTO object.
 */
export class PermissionTypeDTO extends BaseDTO {
    @IsNotEmpty()
    @MaxLength(50)
    @ApiModelProperty({ description: 'name field' })
    name: string;

    @MaxLength(50)
    @IsOptional()
    @ApiModelProperty({ description: 'key field', required: false })
    key: string;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
