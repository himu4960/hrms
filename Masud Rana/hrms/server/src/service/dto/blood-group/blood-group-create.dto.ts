import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from './../base.dto';
import { BloodGroupTranslationDTO } from './blood-group-translation.dto';

export class BloodGroupCreateDTO extends BaseDTO {
    @ApiModelProperty({ type: [BloodGroupTranslationDTO], description: 'Create blood group with translation' })
    translations: BloodGroupTranslationDTO[];
}
