/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, Max, MaxLength } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A BloodGroupTranslationDTO object.
 */
export class BloodGroupTranslationDTO extends BaseDTO {
    @Max(50)
    @IsNotEmpty()
    @MaxLength(50)
    @ApiModelProperty({ example: 'P+', description: 'Blood group translation name field' })
    name: string;

    @IsNotEmpty()
    @MaxLength(2)
    @ApiModelProperty({ example: 'en', description: 'language code' })
    language_code: string;
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
