import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from './../base.dto';
import { BloodGroupTranslationDTO } from './blood-group-translation.dto';

export class BloodGroupUpdateDTO extends BaseDTO {
    @ApiModelProperty({ type: [BloodGroupTranslationDTO], description: 'Update blood group translation' })
    translations: BloodGroupTranslationDTO[];
}
