import { ApiModelProperty } from '@nestjs/swagger';

export class BloodGroupGetByLangCodeDTO {
    @ApiModelProperty({ description: 'Blood group id' })
    id: number;

    @ApiModelProperty({ description: 'Blood group with name' })
    name: string;
}
