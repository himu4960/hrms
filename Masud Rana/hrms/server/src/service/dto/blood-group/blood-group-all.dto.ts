import { ApiModelProperty } from '@nestjs/swagger';
import { BloodGroupTranslationDTO } from './blood-group-translation.dto';

export class BloodGroupAllDTO {
    @ApiModelProperty({ description: 'Blood group id' })
    id: number;

    @ApiModelProperty({ type: [BloodGroupTranslationDTO], description: 'Blood group with translation' })
    translations: BloodGroupTranslationDTO[];
}
