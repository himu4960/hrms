/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { Max } from 'class-validator';

import { BaseDTO } from '../base.dto';
import { BloodGroupTranslationDTO } from './blood-group-translation.dto';

/**
 * A BloodGroupDTO object.
 */
export class BloodGroupDTO extends BaseDTO {
    @Max(11)
    @ApiModelProperty({ description: 'company_id field', required: false })
    company_id?: number;

    @ApiModelProperty({ description: 'is_active field' })
    is_active?: boolean;

    @ApiModelProperty({ description: 'is_deleted field' })
    is_deleted?: boolean;

    @ApiModelProperty({
        example: [
            {
                name: 'P+',
                language_code: 'en',
            },
        ],
        description: 'translations field',
    })
    translations?: BloodGroupTranslationDTO[];
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
