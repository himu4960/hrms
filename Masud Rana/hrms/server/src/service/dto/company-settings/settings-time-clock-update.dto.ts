/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { Max, IsOptional } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A SettingsTimeClock Update object.
 */
export class SettingsTimeClockUpdateDTO extends BaseDTO {
    @ApiModelProperty({ description: 'allow_employee_to_work_after_shift field', required: false })
    allow_employee_to_work_after_shift?: number;

    @ApiModelProperty({ description: 'enable_auto_punch_out field', required: false })
    enable_auto_punch_out?: boolean;

    @ApiModelProperty({ description: 'time_clock_enabled field', required: false })
    time_clock_enabled?: boolean;

    @ApiModelProperty({ description: 'employee_should_use_webcam field', required: false })
    employee_should_use_webcam?: boolean;

    @ApiModelProperty({ description: 'allow_webcam_mobile field', required: false })
    allow_webcam_mobile?: boolean;

    @ApiModelProperty({ description: 'webcam_resolution field', required: false })
    webcam_resolution?: any;

    @ApiModelProperty({ description: 'gps_data_required field', required: false })
    gps_data_required?: boolean;

    @ApiModelProperty({ description: 'show_address_under_time_clock field', required: false })
    show_address_under_time_clock?: boolean;

    @ApiModelProperty({ description: 'pre_clock_enabled field', required: false })
    pre_clock_enabled?: boolean;

    @ApiModelProperty({ description: 'pre_clock_mandatory field', required: false })
    pre_clock_mandatory?: boolean;

    @ApiModelProperty({ description: 'restrict_employee_clock_in_out field', required: false })
    restrict_employee_clock_in_out?: boolean;

    @ApiModelProperty({ description: 'lock_timeclocking_to_locations field', required: false })
    lock_timeclocking_to_locations?: boolean;

    @ApiModelProperty({ description: 'should_have_position_set field', required: false })
    should_have_position_set?: boolean;

    @ApiModelProperty({ description: 'should_have_remote_site_set field', required: false })
    should_have_remote_site_set?: boolean;

    @ApiModelProperty({ description: 'clock_out_notes_required field', required: false })
    clock_out_notes_required?: boolean;

    @ApiModelProperty({ description: 'break_button_enabled field', required: false })
    break_button_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'auto_clock_out_hour field', required: false })
    auto_clock_out_hour?: number;

    @ApiModelProperty({ description: 'time_clock_tips_enabled field', required: false })
    time_clock_tips_enabled?: boolean;

    @ApiModelProperty({ description: 'scheduled_details_enabled field', required: false })
    scheduled_details_enabled?: boolean;

    @ApiModelProperty({ description: 'show_break_times_details field', required: false })
    show_break_times_details?: boolean;

    @ApiModelProperty({ description: 'restrict_overlapping_timesheets field', required: false })
    restrict_overlapping_timesheets?: boolean;

    @ApiModelProperty({ description: 'round_totals_to_nearest field', required: false })
    round_totals_to_nearest?: number;

    @ApiModelProperty({ description: 'round_clock_in_times_to_nearest field', required: false })
    round_clock_in_times_to_nearest?: number;

    @ApiModelProperty({ description: 'clock_in_rounding_direction field', required: false })
    clock_in_rounding_direction?: number;

    @ApiModelProperty({ description: 'round_clock_out_times_to_nearest field', required: false })
    round_clock_out_times_to_nearest?: number;

    @ApiModelProperty({ description: 'clock_out_rounding_direction field', required: false })
    clock_out_rounding_direction?: number;

    @Max(11)
    @IsOptional()
    @ApiModelProperty({ description: 'allow_clock_in_before_timeframe field', required: false })
    allow_clock_in_before_timeframe?: number;

    @Max(11)
    @IsOptional()
    @ApiModelProperty({ description: 'allow_clock_in_after_timeframe field', required: false })
    allow_clock_in_after_timeframe?: number;

    @Max(11)
    @IsOptional()
    @ApiModelProperty({ description: 'allow_clock_out_before_timeframe field', required: false })
    allow_clock_out_before_timeframe?: number;

    @Max(11)
    @IsOptional()
    @ApiModelProperty({ description: 'allow_clock_out_after_timeframe field', required: false })
    allow_clock_out_after_timeframe?: number;

    @ApiModelProperty({ description: 'timesheet_employee_can_import field', required: false })
    timesheet_employee_can_import?: boolean;

    @ApiModelProperty({ description: 'timesheet_employee_can_manually_add field', required: false })
    timesheet_employee_can_manually_add?: boolean;

    @ApiModelProperty({ description: 'timesheet_empl_can_edit field', required: false })
    timesheet_empl_can_edit?: boolean;

    @ApiModelProperty({ description: 'timesheet_prevent_employee_to_edit_approved_time field', required: false })
    timesheet_prevent_employee_to_edit_approved_time?: boolean;

    @ApiModelProperty({ description: 'timesheet_allow_employee_to_add_past_x_days field', required: false })
    timesheet_allow_employee_to_add_past_x_days?: number;

    @ApiModelProperty({ description: 'timesheet_employee_can_view_notes field', required: false })
    timesheet_employee_can_view_notes?: boolean;

    @ApiModelProperty({ description: 'timesheet_notes_mandatory field', required: false })
    timesheet_notes_mandatory?: boolean;

    @ApiModelProperty({ description: 'timesheet_employee_can_edit_without_reason field', required: false })
    timesheet_employee_can_edit_without_reason?: boolean;

    @ApiModelProperty({ description: 'notification_late_reminder_for_schedulers field', required: false })
    notification_late_reminder_for_schedulers?: number;

    @ApiModelProperty({ description: 'notification_late_reminder_for_managers field', required: false })
    notification_late_reminder_for_managers?: number;

    @ApiModelProperty({ description: 'notification_not_clocked_out_reminder_for_schedulers field', required: false })
    notification_not_clocked_out_reminder_for_schedulers?: number;

    @ApiModelProperty({ description: 'notification_not_clocked_out_reminder_for_managers field', required: false })
    notification_not_clocked_out_reminder_for_managers?: number;

    @ApiModelProperty({ description: 'is_active field', required: false })
    is_active?: boolean;

    @ApiModelProperty({ description: 'is_deleted field', required: false })
    is_deleted?: boolean;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
