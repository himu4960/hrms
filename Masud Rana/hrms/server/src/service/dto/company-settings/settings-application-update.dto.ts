/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional, MaxLength, Max } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A SettingsApplicationDTO update object.
 */
export class SettingsApplicationUpdateDTO extends BaseDTO {
    @IsOptional()
    @MaxLength(20)
    @ApiModelProperty({ example: 'bn', description: 'locale_default_lang_code field', required: false })
    locale_default_lang_code?: string;

    @IsOptional()
    @MaxLength(20)
    @ApiModelProperty({ example: 'dd/mm/yy', description: 'locale_date_format field', required: false })
    locale_date_format?: string;

    @IsOptional()
    @Max(11)
    @ApiModelProperty({ example: 1, description: 'locale_week_number field', required: false })
    locale_week_number?: number;

    @IsOptional()
    @ApiModelProperty({ description: 'notification_email_enabled field', required: false })
    notification_email_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'notification_sms_enabled field', required: false })
    notification_sms_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'notification_mobile_push_enabled field', required: false })
    notification_mobile_push_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'notification_force_email_enabled field', required: false })
    notification_force_email_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'notification_birthday_enabled field', required: false })
    notification_birthday_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'notification_birthday_card_enabled field', required: false })
    notification_birthday_card_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'training_module_enabled field', required: false })
    training_module_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'training_download_pdf_enabled field', required: false })
    training_download_pdf_enabled?: boolean;

    @IsOptional()
    @MaxLength(20)
    @ApiModelProperty({ example: 'nickname', description: 'employee_name_format field', required: false })
    employee_name_format?: string;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_view_report field', required: false })
    employee_can_view_report?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_edit_profile field', required: false })
    employee_can_edit_profile?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_view_staff_gallery field', required: false })
    employee_can_view_staff_gallery?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_view_staff_details field', required: false })
    employee_can_view_staff_details?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_mgnt_can_view_eid field', required: false })
    employee_mgnt_can_view_eid?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_view_whos_on field', required: false })
    employee_can_view_whos_on?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_request_account field', required: false })
    employee_can_request_account?: boolean;

    @IsOptional()
    @MaxLength(255)
    @ApiModelProperty({ example: 'question1', description: 'employee_security_question field', required: false })
    employee_security_question?: string;

    @IsOptional()
    @MaxLength(255)
    @ApiModelProperty({ example: 'question2', description: 'employee_security_answer field', required: false })
    employee_security_answer?: string;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_view_uploaded_files field', required: false })
    employee_can_view_uploaded_files?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'comm_message_wall_enabled field', required: false })
    comm_message_wall_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'comm_employee_can_post_msg field', required: false })
    comm_employee_can_post_msg?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'comm_employee_can_comment field', required: false })
    comm_employee_can_comment?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'comm_messaging_module_enabled field', required: false })
    comm_messaging_module_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'comm_employee_can_send_private_msg field', required: false })
    comm_employee_can_send_private_msg?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'comm_employee_can_reply_received_msg field', required: false })
    comm_employee_can_reply_received_msg?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'comm_employee_can_ping_users field', required: false })
    comm_employee_can_ping_users?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'auth_show_logo field', required: false })
    auth_show_logo?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'auth_auto_logout_time field', required: false })
    auth_auto_logout_time?: number;

    @IsOptional()
    @ApiModelProperty({ description: 'auth_password_expiry_time field', required: false })
    auth_password_expiry_time?: number;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
