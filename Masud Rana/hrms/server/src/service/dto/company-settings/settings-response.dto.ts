import { ApiModelPropertyOptional } from '@nestjs/swagger';

import { BaseDTO } from '../base.dto';
import { SettingsApplicationDTO } from './settings-application.dto';
import { SettingsLeaveAvailabilityDTO } from './settings-leave-availability.dto';
import { SettingsShiftPlanningDTO } from './settings-shift-planning.dto';
import { SettingsTimeClockDTO } from './settings-time-clock.dto';

export class SettingsResponseDTO extends BaseDTO {
    @ApiModelPropertyOptional({ example: SettingsApplicationDTO, description: 'settings_application field' })
    settings_application: SettingsApplicationDTO;

    @ApiModelPropertyOptional({ example: SettingsShiftPlanningDTO, description: 'settings_shift_planning field' })
    settings_shift_planning: SettingsShiftPlanningDTO;

    @ApiModelPropertyOptional({ example: SettingsTimeClockDTO, description: 'settings_time_clock field' })
    settings_time_clock: SettingsTimeClockDTO;

    @ApiModelPropertyOptional({ example: SettingsTimeClockDTO, description: 'settings_time_clock field' })
    settings_leave_availability: SettingsLeaveAvailabilityDTO;
}
