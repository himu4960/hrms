/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
import { BaseDTO } from '../base.dto';

/**
 * Settings Leave Availability Update DTO.
 */
export class SettingsLeaveAvailabilityUpdateDTO extends BaseDTO {
    @IsOptional()
    @ApiModelProperty({ description: 'leave_module_enabled field', required: false })
    leave_module_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'deduct_weekends_from_leaves field', required: false })
    deduct_weekends_from_leaves?: string;

    @IsOptional()
    @ApiModelProperty({ description: 'employees_can_book_vacation field', required: false })
    employees_can_book_vacation?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'show_deleted_employees_in_leave_approvals field', required: false })
    show_deleted_employees_in_leave_approvals?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'schedulers_should_receive_leave_notifications field', required: false })
    schedulers_should_receive_leave_notifications?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'allow_schedulers_to_approve_past_leave_requests field', required: false })
    allow_schedulers_to_approve_past_leave_requests?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'leave_must_be_booked_in_days_advance field', required: false })
    leave_must_be_booked_in_days_advance?: number;

    @IsOptional()
    @ApiModelProperty({ description: 'max_staff_can_be_booked_at_once field', required: false })
    max_staff_can_be_booked_at_once?: number;

    @IsOptional()
    @ApiModelProperty({ description: 'employees_can_cancel_previously_approved_leave_requests field', required: false })
    employees_can_cancel_previously_approved_leave_requests?: boolean;
    
    @IsOptional()
    @ApiModelProperty({ description: 'availability_module_enabled field', required: false })
    availability_module_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employees_can_modify_unavailability field', required: false })
    employees_can_modify_unavailability?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'availability_must_be_approved_by_management field', required: false })
    availability_must_be_approved_by_management?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'allow_schedulers_to_approve_availability field', required: false })
    allow_schedulers_to_approve_availability?: boolean;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
