/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { BaseDTO } from '../base.dto';
import { CompanyDTO } from '../company.dto';

/**
 * Settings Leave Availability DTO.
 */
export class SettingsLeaveAvailabilityDTO extends BaseDTO {
    @ApiModelProperty({ description: 'leave_module_enabled field', required: false })
    leave_module_enabled?: boolean;

    @ApiModelProperty({ description: 'deduct_weekends_from_leaves field', required: false })
    deduct_weekends_from_leaves?: string;

    @ApiModelProperty({ description: 'employees_can_book_vacation field', required: false })
    employees_can_book_vacation?: boolean;

    @ApiModelProperty({ description: 'show_deleted_employees_in_leave_approvals field', required: false })
    show_deleted_employees_in_leave_approvals?: boolean;

    @ApiModelProperty({ description: 'schedulers_should_receive_leave_notifications field', required: false })
    schedulers_should_receive_leave_notifications?: boolean;

    @ApiModelProperty({ description: 'allow_schedulers_to_approve_past_leave_requests field', required: false })
    allow_schedulers_to_approve_past_leave_requests?: boolean;

    @ApiModelProperty({ description: 'leave_must_be_booked_in_days_advance field', required: false })
    leave_must_be_booked_in_days_advance?: number;

    @ApiModelProperty({ description: 'max_staff_can_be_booked_at_once field', required: false })
    max_staff_can_be_booked_at_once?: number;

    @ApiModelProperty({ description: 'employees_can_cancel_previously_approved_leave_requests field', required: false })
    employees_can_cancel_previously_approved_leave_requests?: boolean;
    
    @ApiModelProperty({ description: 'availability_module_enabled field', required: false })
    availability_module_enabled?: boolean;

    @ApiModelProperty({ description: 'employees_can_modify_unavailability field', required: false })
    employees_can_modify_unavailability?: boolean;

    @ApiModelProperty({ description: 'availability_must_be_approved_by_management field', required: false })
    availability_must_be_approved_by_management?: boolean;

    @ApiModelProperty({ description: 'allow_schedulers_to_approve_availability field', required: false })
    allow_schedulers_to_approve_availability?: boolean;

    @ApiModelProperty({ description: 'company_id field' })
    company_id: number;

    company?: CompanyDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
