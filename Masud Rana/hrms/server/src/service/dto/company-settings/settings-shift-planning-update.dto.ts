/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A SettingsShiftPlanningDTO update object.
 */
export class SettingsShiftPlanningUpdateDTO extends BaseDTO {
    @IsOptional()
    @ApiModelProperty({ description: 'sorting_by_address_enabled field', required: false })
    sorting_by_address_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'show_locations_in_shifts field', required: false })
    show_locations_in_shifts?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'show_overnight_shifts field', required: false })
    show_overnight_shifts?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'shift_planning_start_day field', required: false })
    shift_planning_start_day?: number;

    @IsOptional()
    @ApiModelProperty({ description: 'use_draft_publish_schedule_method field', required: false })
    use_draft_publish_schedule_method?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'use_shift_approval field', required: false })
    use_shift_approval?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'shift_planning_tasks_enabled field', required: false })
    shift_planning_tasks_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'longer_than_24hours_shifts_allowed field', required: false })
    longer_than_24hours_shifts_allowed?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'past_date_shifts_allowed field', required: false })
    past_date_shifts_allowed?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'notes_permissions_allowed_users field', required: false })
    notes_permissions_allowed_users?: number;

    @IsOptional()
    @ApiModelProperty({ description: 'schedule_automatic_conflicts_allowed field', required: false })
    schedule_automatic_conflicts_allowed?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'show_pending_leave_request field', required: false })
    show_pending_leave_request?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'shift_notes_visibility_allowed_users field', required: false })
    shift_notes_visibility_allowed_users?: number;

    @ApiModelProperty({ description: 'employee_can_view_shifts_in_advance field', required: false })
    employee_can_view_shifts_in_advance?: number;

    @ApiModelProperty({ description: 'show_costing_data_in_scheduler field', required: false })
    show_costing_data_in_scheduler?: number;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_view_all_schedules field', required: false })
    employee_can_view_all_schedules?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_see_assigned_staffs field', required: false })
    employee_can_see_assigned_staffs?: boolean;

    @ApiModelProperty({ description: 'max_working_days_in_a_row field', required: false })
    max_working_days_in_a_row?: number;

    @IsOptional()
    @ApiModelProperty({ description: 'show_12am_as_midnight field', required: false })
    show_12am_as_midnight?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'on_call_management_enabled field', required: false })
    on_call_management_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_see_co_worker_vacations field', required: false })
    employee_can_see_co_worker_vacations?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'shift_loading_indicator_enabled field', required: false })
    shift_loading_indicator_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'show_employee_skills_in_scheduler field', required: false })
    show_employee_skills_in_scheduler?: boolean;

    @ApiModelProperty({ description: 'required_skills_on_shift field', required: false })
    required_skills_on_shift?: number;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_release_shifts field', required: false })
    employee_can_release_shifts?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_trade_shifts field', required: false })
    employee_can_trade_shifts?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'same_day_trades_allowed field', required: false })
    same_day_trades_allowed?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'shift_overlapping_disallowed field', required: false })
    shift_overlapping_disallowed?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'shift_trades_between_positions_disabled field', required: false })
    shift_trades_between_positions_disabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'shift_trades_manager_must_release_before_request field', required: false })
    shift_trades_manager_must_release_before_request?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'shift_trades_manager_must_release_after_request field', required: false })
    shift_trades_manager_must_release_after_request?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'shift_acknowledgment_enabled field', required: false })
    shift_acknowledgment_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_see_co_worker_shifts field', required: false })
    employee_can_see_co_worker_shifts?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_drop_shifts field', required: false })
    employee_can_drop_shifts?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_requested_open_shifts_auto_approved field', required: false })
    employee_requested_open_shifts_auto_approved?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_pick_up_shifts_with_overtime_enabled field', required: false })
    employee_pick_up_shifts_with_overtime_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_cannot_be_scheduled_within_time field', required: false })
    employee_cannot_be_scheduled_within_time?: number;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_be_scheduled_to_immediate_shifts field', required: false })
    employee_can_be_scheduled_to_immediate_shifts?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'shift_trade_release_drop_time_limit field', required: false })
    shift_trade_release_drop_time_limit?: number;

    @IsOptional()
    @ApiModelProperty({ description: 'work_unit_module_enabled field', required: false })
    work_unit_module_enabled?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_add_work_units field', required: false })
    employee_can_add_work_units?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'work_units_submitted_by_scheduler_auto_approved field', required: false })
    work_units_submitted_by_scheduler_auto_approved?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'work_units_submitted_by_employee_auto_approved field', required: false })
    work_units_submitted_by_employee_auto_approved?: boolean;

    @IsOptional()
    @ApiModelProperty({ description: 'employee_can_see_own_work_report field', required: false })
    employee_can_see_own_work_report?: boolean;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
