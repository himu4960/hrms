import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { BaseDTO } from '../base.dto';

export class SettingsCreateDTO extends BaseDTO {
    @ApiModelPropertyOptional({ description: 'company_id field' })
    company_id: number;

    @ApiModelPropertyOptional({ description: 'locale_country_id field' })
    locale_country_id: number;

    @ApiModelPropertyOptional({ description: 'locale_timezone_id field' })
    locale_timezone_id: number;
}
