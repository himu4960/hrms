/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MinLength, MaxLength, Length, Min, Max, Matches } from 'class-validator';
import { BaseDTO } from './base.dto';

/**
 * A FileDTO object.
 */
export class FileDTO extends BaseDTO {
    @IsNotEmpty()
    @MaxLength(150)
    @ApiModelProperty({ description: 'name field' })
    name: string;

    @IsNotEmpty()
    @MaxLength(50)
    @ApiModelProperty({ description: 'mimetype field' })
    mimetype: string;

    @IsNotEmpty()
    @MaxLength(150)
    @ApiModelProperty({ description: 'orginal_filename field' })
    orginal_filename: string;

    @ApiModelProperty({ description: 'path_url field', required: false })
    path_url: string;

    @IsNotEmpty()
    @Max(4)
    @ApiModelProperty({ description: 'file_size field' })
    file_size: number;

    @ApiModelProperty({ description: 'download_count field', required: false })
    download_count: number;

    @ApiModelProperty({ description: 'branch_id field', required: false })
    branch_id: number;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
