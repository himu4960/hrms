import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

import { LeaveRequestDTO } from './leave-request.dto';

export class LeaveCreateDTO extends LeaveRequestDTO {
    @IsNotEmpty()
    @ApiModelProperty({ example: 1, description: 'employee_id field' })
    employee_id: number;
}
