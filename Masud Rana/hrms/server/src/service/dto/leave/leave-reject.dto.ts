import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class LeaveRejectDTO {

    @IsOptional()
    @ApiModelProperty({ description: 'rejected_note field', required: false })
    rejected_note?: string;
}