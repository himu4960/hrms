/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';

import { LeaveBalanceDTO } from './leave-balance.dto';
import { LeaveDTO } from './leave.dto';

/**
 * A LeaveDTO object.
 */
export class LeaveDataDTO {
    @ApiModelProperty({ example: LeaveDTO, description: 'pending_leaves', isArray: true })
    pending_leaves: LeaveDTO[];

    @ApiModelProperty({ example: LeaveDTO, description: 'upcoming_leaves', isArray: true })
    upcoming_leaves: LeaveDTO[];

    @ApiModelProperty({ example: LeaveBalanceDTO, description: 'leave_balances', isArray: true })
    leave_balances: LeaveBalanceDTO[];
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
