/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

import { BaseDTO } from '../base.dto';
import { BranchDTO } from '../branch/branch.dto';
import { CompanyDTO } from '../company.dto';
import { EmployeeDTO } from '../employee/employee.dto';
import { LeaveTypeDTO } from '../leave-type.dto';

/**
 * A LeaveDTO object.
 */
export class LeaveDTO extends BaseDTO {
    company_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 1, description: 'employee_id field' })
    employee_id: number;

    branch_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 1, description: 'leave_type_id field' })
    leave_type_id: number;

    status: any;

    is_approved: boolean;

    approved_by?: number;

    approved_at?: number;

    approved_note?: string;

    is_rejected: boolean;

    is_review_requested?: boolean;

    is_review_accepted?: boolean;

    rejected_by?: number;

    rejected_at?: number;

    rejected_note?: string;

    @ApiModelProperty({ example: false, description: 'is_partial_day_allowed field', required: false })
    is_partial_day_allowed: boolean;

    @IsOptional()
    @ApiModelProperty({ example: Date.now(), description: 'start_date field', required: false })
    start_date: number;

    @IsOptional()
    @ApiModelProperty({ example: Date.now(), description: 'end_date field', required: false })
    end_date: number;

    total_leave_duration_mins?: number;

    @IsOptional()
    @ApiModelProperty({ example: 'comment test', description: 'comments field', required: false })
    comments: string;

    deleted_at?: number;

    deleted_by?: number;

    is_deleted?: boolean;

    company?: CompanyDTO;

    employee?: EmployeeDTO;

    branch?: BranchDTO;

    leaveType?: LeaveTypeDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
