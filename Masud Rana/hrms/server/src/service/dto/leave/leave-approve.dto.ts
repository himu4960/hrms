import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class LeaveApproveDTO {

    @IsOptional()
    @ApiModelProperty({ description: 'approved_note field', required: false })
    approved_note?: string;
}