import { BaseDTO } from '../base.dto';
import { CompanyDTO } from '../company.dto';
import { EmployeeDTO } from '../employee/employee.dto';
import { LeaveTypeDTO } from '../leave-type.dto';

export class LeaveBalanceDTO extends BaseDTO {
    company_id: number;

    employee_id: number;

    leave_type_id: number;

    allocated_leave_mins: number;

    remaining_leave_mins: number;

    company?: CompanyDTO;

    employee?: EmployeeDTO;

    leaveType?: LeaveTypeDTO;
}
