/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class LeaveParamsDTO {
    @IsNotEmpty()
    @ApiModelProperty({ example: 1, required: true })
    employee_id: number;

    @IsOptional()
    @ApiModelProperty({ example: 1, required: false })
    branch_id?: number;

    @IsOptional()
    @ApiModelProperty({ example: 1, required: false })
    leave_type_id?: number;

    @IsOptional()
    @ApiModelProperty({ example: 1, required: false })
    designation_id?: number;

    @IsOptional()
    @ApiModelProperty({ example: 'pending', required: false })
    status?: string;

    @IsOptional()
    @ApiModelProperty({ example: Date.now(), required: false })
    start_date?: number;

    @IsOptional()
    @ApiModelProperty({ example: Date.now(), required: false })
    end_date?: number;

}
