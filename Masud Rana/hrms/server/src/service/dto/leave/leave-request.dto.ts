import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

import { BaseDTO } from '../base.dto';
import { BranchDTO } from '../branch/branch.dto';
import { CompanyDTO } from '../company.dto';
import { EmployeeDTO } from '../employee/employee.dto';
import { LeaveTypeDTO } from '../leave-type.dto';

export class LeaveRequestDTO extends BaseDTO {
    company_id: number;

    branch_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'leave_type_id field' })
    leave_type_id: number;

    @IsOptional()
    @ApiModelProperty({ description: 'weekend_day field' })
    weekend_day: number[];

    status: any;

    is_approved: boolean;

    approved_by?: number;

    approved_at?: number;

    approved_note?: string;

    is_rejected: boolean;

    rejected_by?: number;

    rejected_at?: number;

    rejected_note?: string;

    @ApiModelProperty({ description: 'is_partial_day_allowed field', default: false })
    is_partial_day_allowed: boolean;

    @IsOptional()
    @ApiModelProperty({ example: Date.now(), description: 'start_date field', required: false })
    start_date: number;

    @IsOptional()
    @ApiModelProperty({ example: Date.now(), description: 'end_date field', required: false })
    end_date: number;

    leave_duration_hours?: number;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'comments field', required: true })
    comments: string;

    deleted_at?: number;

    deleted_by?: number;

    is_deleted?: boolean;

    company?: CompanyDTO;

    employee?: EmployeeDTO;

    branch?: BranchDTO;

    leaveType?: LeaveTypeDTO;
}
