import { ApiModelProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

/**
 * A DTO representing a login user.
 */
export class UserLoginDTO {
    @ApiModelProperty({ example: 'admin', description: 'Username/Email' })
    @IsString()
    readonly username: string;

    @ApiModelProperty({ example: 'admin', description: 'User password' })
    @IsString()
    readonly password: string;

    @ApiModelProperty({ description: 'User remember', required: false })
    readonly rememberMe?: boolean;
}
