/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';

/**
 * A DesignationResponseDTO object.
 */
export class DesignationResponseDTO {
    @ApiModelProperty({ example: 1, description: 'id field' })
    id: number;

    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ example: 'Software Engineer', description: 'name field' })
    name: string;

    @ApiModelProperty({ description: 'is_visible field' })
    is_visible: boolean;

    @ApiModelProperty({ example: new Date().toISOString(), description: 'updated_at field' })
    updated_at?: string;
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
