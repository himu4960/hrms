/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';

import { BaseDTO } from '../base.dto';
import { BranchDTO } from '../branch/branch.dto';
import { CompanyDTO } from '../company.dto';
import { SkillDTO } from '../skill.dto';

/**
 * A DesignationDTO object.
 */
export class DesignationDTO extends BaseDTO {
    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ description: 'name field' })
    name: string;

    @ApiModelProperty({ description: 'is_visible field' })
    is_visible?: boolean;

    @ApiModelProperty({ type: SkillDTO, isArray: true, description: 'skills field' })
    skills?: SkillDTO[];

    @ApiModelProperty({ description: 'company_id field' })
    company_id: number;

    @ApiModelProperty({ description: 'branch_id field' })
    branch_id: number;

    @ApiModelProperty({ type: CompanyDTO, description: 'company relationship' })
    company?: CompanyDTO;

    @ApiModelProperty({ type: BranchDTO, description: 'branch relationship' })
    branch?: BranchDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
