/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A DesignationDTO create object.
 */
export class DesignationCreateDTO extends BaseDTO {
    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ example: 'Full Stack Developer', description: 'name field' })
    name: string;

    @ApiModelProperty({ example: 1, description: 'branch_id field' })
    branch_id: number;
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
