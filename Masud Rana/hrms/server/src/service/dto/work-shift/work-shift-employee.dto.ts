/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A WorkShiftEmployeeDTO object.
 */
export class WorkShiftEmployeeDTO extends BaseDTO {
    @IsNotEmpty()
    @ApiModelProperty({ example: 1, description: 'company_id field' })
    company_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 1, description: 'branch_id field' })
    branch_id: number;

    @IsOptional()
    @ApiModelProperty({ description: 'workshift_id field' })
    workshift_id?: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 1, description: 'employee_id field' })
    employee_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 1, description: 'designation_id field' })
    designation_id: number;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
