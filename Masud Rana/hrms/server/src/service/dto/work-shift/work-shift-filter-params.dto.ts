/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A WorkShiftFilterParamsDTO object.
 */
export class WorkShiftFilterParamsDTO extends BaseDTO {
    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'branch_id field' })
    branch_id: number;

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'designation_id field' })
    designation_ids: number[];

    @IsOptional()
    @ApiModelProperty({ example: Date.now(), description: 'start_date field' })
    start_date: number;

    @IsOptional()
    @ApiModelProperty({ example: Date.now(), description: 'end_date field' })
    end_date: number;

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'employee_ids field' })
    employee_ids: number[];

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'skills_ids field' })
    skill_ids: number[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
