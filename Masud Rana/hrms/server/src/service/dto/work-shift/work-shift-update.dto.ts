import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional, MaxLength, Min } from 'class-validator';

export class WorkShiftUpdateDTO {
    @MaxLength(255)
    @IsOptional()
    @ApiModelProperty({ example: 'test title', description: 'title field', required: false })
    title?: string;

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'designation_id field' })
    designation_id?: number;

    @IsOptional()
    @ApiModelProperty({ example: Date.now(), description: 'start_date field' })
    start_date?: number;

    @IsOptional()
    @ApiModelProperty({ example: Date.now(), description: 'end_date field' })
    end_date?: number;

    @IsOptional()
    @Min(1)
    @ApiModelProperty({ example: 400, description: 'start_time field' })
    start_time?: number;

    @IsOptional()
    @Min(1)
    @ApiModelProperty({ example: 800, description: 'end_time field' })
    end_time?: number;

    @IsOptional()
    @ApiModelProperty({ example: '[1,2,8,3]', description: 'skills field' })
    skills?: string;

    @IsOptional()
    @ApiModelProperty({ example: 'information', description: 'note field' })
    note?: string;
}
