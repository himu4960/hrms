/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, IsOptional, Min } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A WorkShiftDTO object.
 */
export class WorkShiftDTO extends BaseDTO {
    company_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 1, description: 'branch_id field' })
    branch_id: number;

    @MaxLength(255)
    @IsOptional()
    @ApiModelProperty({ example: 'test title', description: 'title field', required: false })
    title: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: 1, description: 'designation_id field' })
    designation_id: number;

    @IsOptional()
    @ApiModelProperty({ example: false, description: 'is_published field' })
    is_published?: boolean;

    published_at?: number;

    published_by?: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: Date.now(), description: 'start_date field' })
    start_date: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: Date.now(), description: 'end_date field' })
    end_date: number;

    @IsNotEmpty()
    @Min(1)
    @ApiModelProperty({ example: 400, description: 'start_time field' })
    start_time: number;

    @IsNotEmpty()
    @Min(1)
    @ApiModelProperty({ example: 800, description: 'end_time field' })
    end_time: number;

    total_work_shift_duration: number;

    employees: any[];

    @IsOptional()
    @ApiModelProperty({ example: '[1,2,8,3]', description: 'skills field' })
    skills?: string;

    @IsOptional()
    @ApiModelProperty({ example: 'information', description: 'note field' })
    note?: string;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
