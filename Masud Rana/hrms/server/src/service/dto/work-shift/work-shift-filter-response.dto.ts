/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from '../base.dto';
import { LeaveDTO } from '../leave/leave.dto';
import { WorkShiftDTO } from './work-shift.dto';

/**
 * A WorkShiftFilterResponseDTO object.
 */
export class WorkShiftFilterResponseDTO extends BaseDTO {
    @ApiModelProperty({ example: 1, description: 'employee_id field' })
    id: number;

    @ApiModelProperty({ example: [WorkShiftDTO], description: 'work_shifts field' })
    work_shifts: WorkShiftDTO[];

    @ApiModelProperty({ example: [LeaveDTO], description: 'leaves field' })
    leaves: LeaveDTO[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
