/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, IsOptional, Min } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A CreateManyWorkShiftsDTO object.
 */
export class CreateManyWorkShiftsDTO extends BaseDTO {
    @MaxLength(255)
    @IsOptional()
    @ApiModelProperty({ example: 'test title', description: 'title field', required: false })
    title: string;

    @IsNotEmpty()
    @ApiModelProperty({
        example: [
            {
                id: 1,
                name: 'Masud Rana',
            },
        ],
        description: 'employees field',
    })
    employees: any[];

    @IsNotEmpty()
    @ApiModelProperty({ example: Date.now(), description: 'start_date field' })
    start_date: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: Date.now(), description: 'end_date field' })
    end_date: number;

    @IsNotEmpty()
    @Min(1)
    @ApiModelProperty({ example: 400, description: 'start_time field' })
    start_time: number;

    @IsNotEmpty()
    @Min(1)
    @ApiModelProperty({ example: 800, description: 'end_time field' })
    end_time: number;

    total_work_shift_duration: number;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
