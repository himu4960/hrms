/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, IsOptional } from 'class-validator';

import { BaseDTO } from './base.dto';

/**
 * A LeaveTypeDTO object.
 */
export class LeaveTypeDTO extends BaseDTO {
    company_id: number;

    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ example: 'leave type', description: 'name field' })
    name: string;

    @ApiModelProperty({ example: true, description: 'is_partial_day_request_allowed field' })
    is_partial_day_request_allowed: boolean;

    @IsOptional()
    @ApiModelProperty({ example: 12, description: 'max_days_allowed field', required: false })
    max_days_allowed: number;

    @ApiModelProperty({ description: 'is_max_employee_allowed_enabled field' })
    is_max_employee_allowed_enabled: boolean;

    @IsOptional()
    @ApiModelProperty({ example: 5, description: 'max_employee_allowed field', required: false })
    max_employee_allowed: number;

    @ApiModelProperty({ example: true, description: 'is_leave_in_advance_days_enabled field' })
    is_leave_in_advance_days_enabled: boolean;

    @IsOptional()
    @ApiModelProperty({ example: 3, description: 'leave_in_advance_days field', required: false })
    leave_in_advance_days: number;

    @ApiModelProperty({ example: true, description: 'auto_assign_new_employee_enabled field' })
    auto_assign_new_employee_enabled: boolean;

    @ApiModelProperty({ example: 'description test', description: 'description field', required: false })
    description: string;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
