/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';

import { BaseDTO } from './base.dto';
import { CompanyTypeDTO } from './company-type.dto';
import { ApplicationTypeDTO } from './application-type.dto';

/**
 * A CompanyDTO object.
 */
export class CompanyDTO extends BaseDTO {
    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ description: 'name field' })
    name: string;

    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ description: 'email field' })
    email: string;

    @MaxLength(20)
    @ApiModelProperty({ description: 'phone field', required: false })
    phone?: string;

    @MaxLength(500)
    @ApiModelProperty({ description: 'address field', required: false })
    address?: string;

    @ApiModelProperty({ description: 'logo_url_large field', required: false })
    logo_url_large?: string;

    @ApiModelProperty({ description: 'logo_url_medium field', required: false })
    logo_url_medium?: string;

    @ApiModelProperty({ description: 'logo_url_small field', required: false })
    logo_url_small?: string;

    @ApiModelProperty({ description: 'status field', required: false })
    status?: boolean;

    @ApiModelProperty({ description: 'Company Type ID field' })
    company_type_id?: number;

    @ApiModelProperty({ type: CompanyTypeDTO, description: 'companyType relationship' })
    companyType?: CompanyTypeDTO;

    @ApiModelProperty({ type: ApplicationTypeDTO, description: 'applicationType relationship' })
    applicationType?: ApplicationTypeDTO;

    @ApiModelProperty({ description: 'Application Type ID field' })
    application_type_id?: number;

    @ApiModelProperty({ description: 'UserId field' })
    user_id?: number;

    @ApiModelProperty({ description: 'User Companies' })
    user_companies?: any[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
