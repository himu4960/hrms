import { BaseDTO } from "./base.dto";

export class UserDeviceDTO extends BaseDTO {
    company_id: number;

    employee_id: number;

    role_id: number;

    device_token: string;
}
