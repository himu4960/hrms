/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';

/**
 * A CustomFieldTypeDTO object.
 */
export class CustomFieldTypeDTO {

    @IsNotEmpty()
    @MaxLength(11)
    @ApiModelProperty({ description: 'id field' })
    id: number;

    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ description: 'name field' })
    name: string;

    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ description: 'data_type field' })
    data_type: string;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
