/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

import { BaseDTO } from './base.dto';

/**
 * A WorkShiftTemplateDTO object.
 */
export class WorkShiftTemplateDTO extends BaseDTO {
    company_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'branch_id field' })
    branch_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'designation_id field' })
    designation_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'start_time field' })
    start_time: number;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'end_time field' })
    end_time: number;

    total_work_shift_duration: number;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
