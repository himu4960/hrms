/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, Max, IsOptional } from 'class-validator';

import { BaseDTO } from '../base.dto';
import { EmployeeDesignationsDTO } from './employee-designations.dto';
import { WorkShift } from './../../../domain/work-shift.entity';
import { LeaveDTO } from '../leave/leave.dto';
import { SkillDTO } from '../skill.dto';

/**
 * A EmployeeDTO object.
 */
export class EmployeeDTO extends BaseDTO {
    @IsOptional()
    @MaxLength(100)
    @ApiModelProperty({ example: 'Zabcefghijkl123', description: 'code field', required: false })
    code?: string;

    @IsNotEmpty()
    @MaxLength(50)
    @ApiModelProperty({ example: 'Tertha', description: 'first_name field' })
    first_name: string;

    @IsOptional()
    @MaxLength(50)
    @ApiModelProperty({ example: 'Dev', description: 'middle_name field', required: false })
    middle_name?: string;

    @IsNotEmpty()
    @MaxLength(50)
    @ApiModelProperty({ example: 'Sarker', description: 'last_name field' })
    last_name: string;

    @ApiModelProperty({ example: Date.now(), description: 'dob field', required: false })
    dob?: number;

    @IsNotEmpty()
    @MaxLength(100)
    @ApiModelProperty({ example: 'devteertha28@gmail.com', description: 'email field' })
    email: string;

    @IsOptional()
    @MaxLength(100)
    @ApiModelProperty({ example: 'devteertha28@gmail.com', description: 'personal_email field', required: false })
    personal_email?: string;

    @IsOptional()
    @MaxLength(30)
    @ApiModelProperty({ example: '01672066834', description: 'phone field', required: false })
    phone?: string;

    @IsOptional()
    @MaxLength(30)
    @ApiModelProperty({ example: '+8801672066834', description: 'telephone field', required: false })
    telephone?: string;

    @IsOptional()
    @MaxLength(100)
    @ApiModelProperty({ example: 'Bangladeshi', description: 'nationality field', required: false })
    nationality?: string;

    @ApiModelProperty({ description: 'photo_url field', required: false })
    photo_url?: string;

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'gender_id field', required: false })
    gender_id?: number;

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'religion_id field', required: false })
    religion_id?: number;

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'marital_status_id field', required: false })
    marital_status_id?: number;

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'blood_group_id field', required: false })
    blood_group_id?: number;

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'branch_id field' })
    branch_id?: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 1, description: 'company_id field' })
    company_id: number;

    @ApiModelProperty({ description: 'is_active field', required: false })
    is_active?: boolean;

    @ApiModelProperty({ description: 'join_date field', required: false })
    join_date?: number;

    @ApiModelProperty({ description: 'is_deleted field', required: false })
    is_deleted?: boolean;

    @ApiModelProperty({ description: 'invitation_accepted field', required: false })
    invitation_accepted?: boolean;

    @ApiModelProperty({ description: 'invitation_token field', required: false })
    invitation_token?: string;

    @ApiModelProperty({ description: 'invitation_expiry field', required: false })
    invitation_expiry?: number;

    employee_designations?: EmployeeDesignationsDTO[];

    @ApiModelProperty({ description: 'work_shifts field', required: false })
    work_shifts?: WorkShift[];

    @ApiModelProperty({ description: 'leaves field', required: false })
    leaves?: LeaveDTO[];

    @ApiModelProperty({ description: 'skills field', required: false })
    skills?: SkillDTO[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
