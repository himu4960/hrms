/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

/**
 * A EmployeeFilterDTO object.
 */
export class EmployeeFilterDTO {
    @IsOptional()
    @ApiModelProperty({ example: 'admin', description: 'search_text field' })
    search_text: string;

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'branch_id field' })
    branch_id: number;

    @IsOptional()
    @ApiModelProperty({ example: false, description: 'is_active field' })
    is_active: boolean;

    @IsOptional()
    @ApiModelProperty({ example: false, description: 'invitation_accepted field' })
    invitation_accepted: boolean;

    @IsOptional()
    @ApiModelProperty({ example: [1], isArray: true, description: 'designations field' })
    designations: number[];

    @IsOptional()
    @ApiModelProperty({ example: [1], isArray: true, description: 'skills field' })
    skills: number[];
}
