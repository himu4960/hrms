/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { MaxLength, Max, IsOptional } from 'class-validator';

import { BaseDTO } from '../base.dto';
import { EmployeeDesignationsDTO } from './employee-designations.dto';

export class EmployeeUpdateDTO extends BaseDTO {
    @IsOptional()
    @MaxLength(100)
    @ApiModelProperty({ example: 'ZXCVBNBN123', description: 'code field', required: false })
    code?: string;

    @IsOptional()
    @MaxLength(50)
    @ApiModelProperty({ example: 'Tertha', description: 'first_name field' })
    first_name?: string;

    @IsOptional()
    @MaxLength(50)
    @ApiModelProperty({ example: 'Dev', description: 'middle_name field', required: false })
    middle_name?: string;

    @IsOptional()
    @MaxLength(50)
    @ApiModelProperty({ example: 'Sarker', description: 'last_name field' })
    last_name?: string;

    @ApiModelProperty({ example: Date.now(), description: 'dob field', required: false })
    dob?: number;

    @IsOptional()
    @MaxLength(100)
    @ApiModelProperty({ example: 'devteertha28@gmail.com', description: 'email field' })
    email?: string;

    @IsOptional()
    @MaxLength(100)
    @ApiModelProperty({ example: 'devteertha28@gmail.com', description: 'personal_email field', required: false })
    personal_email?: string;

    @IsOptional()
    @MaxLength(30)
    @ApiModelProperty({ example: '01672066834', description: 'phone field', required: false })
    phone?: string;

    @IsOptional()
    @MaxLength(30)
    @ApiModelProperty({ example: '01672066834', description: 'telephone field', required: false })
    telephone?: string;

    @IsOptional()
    @MaxLength(100)
    @ApiModelProperty({ example: 'Bangladeshi', description: 'nationality field', required: false })
    nationality?: string;

    @ApiModelProperty({
        example:
            'https://www.pngitem.com/pimgs/m/30-307416_profile-icon-png-image-free-download-searchpng-employee.png',
        description: 'photo_url field',
        required: false,
    })
    photo_url?: string;

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'gender_id field', required: false })
    gender_id?: number;

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'religion_id field', required: false })
    religion_id?: number;

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'marital_status_id field', required: false })
    marital_status_id?: number;

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'blood_group_id field', required: false })
    blood_group_id?: number;

    @IsOptional()
    @ApiModelProperty({ example: 1, description: 'branch_id field' })
    branch_id?: number;

    @ApiModelProperty({ example: true, description: 'is_active field', required: false })
    is_active?: boolean;

    @ApiModelProperty({ example: false, description: 'is_deleted field', required: false })
    is_deleted?: boolean;

    @ApiModelProperty({ example: true, description: 'invitation_accepted field', required: false })
    invitation_accepted?: boolean;

    @ApiModelProperty({
        example: 'QWEQWEQWEQWEQWEQWEQWEQWEQWE',
        description: 'invitation_token field',
        required: false,
    })
    invitation_token?: string;

    @ApiModelProperty({ example: null, description: 'invitation_expiry field', required: false })
    invitation_expiry?: any;
}
