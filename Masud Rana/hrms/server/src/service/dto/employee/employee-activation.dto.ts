import { ApiModelProperty } from '@nestjs/swagger';

export class EmployeeActivationDTO {
    @ApiModelProperty({ description: 'employee id field' })
    employee_id: number;

    @ApiModelProperty({ description: 'is_active field', default: false })
    is_active: boolean;
}
