/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A EmployeeDesignationDTO object.
 */
export class EmployeeDesignationsDTO extends BaseDTO {
    @IsNotEmpty()
    @ApiModelProperty({ example: 1, description: 'employee_id field' })
    employee_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 1, description: 'designation_id field' })
    designation_id: number;
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
