/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, IsOptional } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A EmployeeBirthdayResponseDTO object.
 */
export class EmployeeBirthdayResponseDTO extends BaseDTO {
    @IsOptional()
    @MaxLength(100)
    @ApiModelProperty({ example: 'Zabcefghijkl123', description: 'code field', required: false })
    code?: string;

    @IsNotEmpty()
    @MaxLength(50)
    @ApiModelProperty({ example: 'Tertha', description: 'first_name field' })
    first_name: string;

    @IsOptional()
    @MaxLength(50)
    @ApiModelProperty({ example: 'Dev', description: 'middle_name field', required: false })
    middle_name?: string;

    @IsNotEmpty()
    @MaxLength(50)
    @ApiModelProperty({ example: 'Sarker', description: 'last_name field' })
    last_name: string;

    @ApiModelProperty({ description: 'dob field', required: false })
    dob?: number;

    @ApiModelProperty({ description: 'photo_url field', required: false })
    photo_url?: string;
}
