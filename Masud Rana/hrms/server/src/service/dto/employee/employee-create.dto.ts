/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, MaxLength } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A EmployeeCreateDTO object.
 */

export class EmployeeCreateDTO extends BaseDTO {
    @IsNotEmpty()
    @MaxLength(50)
    @ApiModelProperty({ example: 'Tertha', description: 'first_name field' })
    first_name: string;

    @IsNotEmpty()
    @MaxLength(50)
    @ApiModelProperty({ example: 'Sarker', description: 'last_name field' })
    last_name: string;

    @IsNotEmpty()
    @MaxLength(100)
    @ApiModelProperty({ example: 'devteertha28@gmail.com', description: 'email field' })
    email: string;

    @IsOptional()
    @ApiModelProperty({ example: true, description: 'invitation_accepted field' })
    invitation_accepted?: boolean;
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
