/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, IsOptional } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A EmployeeListDTO object.
 */
export class EmployeeListDTO extends BaseDTO {
    @IsNotEmpty()
    @MaxLength(50)
    @ApiModelProperty({ example: 'Tertha', description: 'first_name field' })
    first_name: string;

    @IsNotEmpty()
    @MaxLength(50)
    @ApiModelProperty({ example: 'Sarker', description: 'last_name field' })
    last_name: string;

    @IsNotEmpty()
    @MaxLength(100)
    @ApiModelProperty({ example: 'devteertha28@gmail.com', description: 'email field' })
    email: string;

    @IsOptional()
    @MaxLength(30)
    @ApiModelProperty({ example: '01672066834', description: 'phone field', required: false })
    phone: string;

    @ApiModelProperty({ description: 'photo_url field', required: false })
    photo_url: string;

    @ApiModelProperty({ example: true, description: 'invitation_accepted field', required: false })
    invitation_accepted: boolean;

    @ApiModelProperty({ example: 1, description: 'role_id field', required: false })
    role_id: number;

    @ApiModelProperty({ example: 'Owner', description: 'role_name field', required: false })
    role_name: string;
}
