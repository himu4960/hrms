/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsEmail, MaxLength } from 'class-validator';

import { BaseDTO } from '../base.dto';
import { BreakRuleDTO } from '../break-rule/break-rule.dto';
import { DesignationResponseDTO } from '../designation/designation-response.dto';
import { TimeZoneDTO } from '../time-zone.dto';

/**
 * A BranchResponseDTO object.
 */
export class BranchResponseDTO extends BaseDTO {
    @MaxLength(255)
    @ApiModelProperty({ example: 'Main Branch', description: 'name field' })
    name: string;

    @MaxLength(20)
    @ApiModelProperty({ example: '01672072720', description: 'phone field', required: false })
    phone: string;

    @IsEmail()
    @MaxLength(255)
    @ApiModelProperty({ example: 'info@hazira.com', description: 'email field', required: false })
    email: string;

    @ApiModelProperty({ example: null, description: 'address_line1 field', required: false })
    address_line1?: string;

    @ApiModelProperty({ example: null, description: 'address_line2 field', required: false })
    address_line2?: string;

    @ApiModelProperty({ example: 'Dhaka', description: 'address_city field', required: false })
    address_city?: string;

    @ApiModelProperty({ example: null, description: 'address_province field', required: false })
    address_province?: string;

    @ApiModelProperty({ example: null, description: 'address_country field', required: false })
    address_country?: string;

    @ApiModelProperty({ example: null, description: 'address_latitude field', required: false })
    address_latitude?: number;

    @ApiModelProperty({ example: null, description: 'address_longitude field', required: false })
    address_longitude?: number;

    @ApiModelProperty({ example: true, description: 'is_active field', required: false })
    is_active?: boolean;

    @ApiModelProperty({ type: BreakRuleDTO, description: 'break_rule relationship' })
    break_rule: BreakRuleDTO;

    @ApiModelProperty({ type: DesignationResponseDTO, isArray: true, description: 'designations relationship' })
    designations: DesignationResponseDTO[];

    @ApiModelProperty({ type: TimeZoneDTO, description: 'time_zone relationship' })
    time_zone: TimeZoneDTO;

    @ApiModelProperty({ example: true, description: 'is_default' })
    is_default: boolean;

    @ApiModelProperty({ example: false, description: 'is_primary' })
    is_primary?: boolean;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
