/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsOptional, MaxLength } from 'class-validator';

import { BaseDTO } from '../base.dto';
import { BreakRuleDTO } from '../break-rule/break-rule.dto';
import { CompanyDTO } from '../company.dto';
import { TimeZoneDTO } from '../time-zone.dto';

/**
 * A BranchDTO object.
 */
export class BranchDTO extends BaseDTO {
    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ description: 'name field' })
    name: string;

    @IsOptional()
    @MaxLength(20)
    @ApiModelProperty({ description: 'phone field', required: false })
    phone?: string;

    @IsOptional()
    @IsEmail()
    @MaxLength(255)
    @ApiModelProperty({ description: 'email field', required: false })
    email?: string;

    @ApiModelProperty({ description: 'address_line1 field', required: false })
    address_line1?: string;

    @ApiModelProperty({ description: 'address_line2 field', required: false })
    address_line2?: string;

    @ApiModelProperty({ description: 'address_city field', required: false })
    address_city?: string;

    @ApiModelProperty({ description: 'address_province field', required: false })
    address_province?: string;

    @ApiModelProperty({ description: 'address_country field', required: false })
    address_country?: string;

    @ApiModelProperty({ description: 'address_latitude field', required: false })
    address_latitude?: number;

    @ApiModelProperty({ description: 'address_longitude field', required: false })
    address_longitude?: number;

    @ApiModelProperty({ description: 'is_active field', required: false })
    is_active?: boolean;

    company?: CompanyDTO;

    @ApiModelProperty({ description: 'Company ID' })
    company_id?: number;

    @ApiModelProperty({ type: BreakRuleDTO, description: 'break_rule relationship' })
    break_rule?: BreakRuleDTO;

    @ApiModelProperty({ description: 'break_rule ID' })
    break_rule_id: number;

    @ApiModelProperty({ type: TimeZoneDTO, description: 'time_zone relationship' })
    time_zone?: TimeZoneDTO;

    @ApiModelProperty({ description: 'time_zone ID' })
    time_zone_id: number;

    @ApiModelProperty({ example: true, description: 'is_default' })
    is_default?: boolean;

    @ApiModelProperty({ example: false, description: 'is_primary' })
    is_primary?: boolean;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
