import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class PasswordResetDTO {
    @ApiModelProperty({ description: 'New password' })
    @IsString()
    @IsNotEmpty()
    readonly newPassword: string;
}
