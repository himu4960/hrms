/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';

import { BaseDTO } from './base.dto';

/**
 * A SkillDTO object.
 */
export class SkillDTO extends BaseDTO {
    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ example: 'React JS', description: 'name field' })
    name: string;
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
