import { ApiModelProperty } from "@nestjs/swagger";

export class DateRangeDTO  {
    @ApiModelProperty({ example: Date.now() })
    start_date: number;

    @ApiModelProperty({ example: Date.now() })
    end_date: number;
}
