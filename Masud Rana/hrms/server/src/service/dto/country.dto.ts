/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';

import { BaseDTO } from './base.dto';

/**
 * A CountryDTO object.
 */
export class CountryDTO extends BaseDTO {
    @IsNotEmpty()
    @MaxLength(100)
    @ApiModelProperty({ description: 'name field' })
    name: string;

    @IsNotEmpty()
    @MaxLength(20)
    @ApiModelProperty({ description: 'code field' })
    code: string;

    @IsNotEmpty()
    @MaxLength(20)
    @ApiModelProperty({ description: 'iso_code field' })
    iso_code: string;

    @ApiModelProperty({ description: 'is_active field', required: false })
    is_active: boolean;

    @ApiModelProperty({ description: 'is_deleted field', required: false })
    is_deleted: boolean;

    @ApiModelProperty({ description: 'is_default field', required: false })
    is_default: boolean;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
