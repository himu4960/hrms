import { ApiModelProperty } from '@nestjs/swagger';

export class ReligionGetByLangCodeDTO {
    @ApiModelProperty({ description: 'Religion id' })
    id: number;

    @ApiModelProperty({ description: 'Religion with name' })
    name: string;
}
