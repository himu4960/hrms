import { ApiModelProperty } from '@nestjs/swagger';
import { ReligionTranslationDTO } from './religion-translation.dto';

export class ReligionAllDTO {
    @ApiModelProperty({ description: 'Marital status id' })
    id: number;

    @ApiModelProperty({ type: [ReligionTranslationDTO], description: 'Religion with translation' })
    translations: ReligionTranslationDTO[];
}
