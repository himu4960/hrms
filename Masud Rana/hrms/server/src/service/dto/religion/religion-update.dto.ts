import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from './../base.dto';
import { ReligionTranslationDTO } from './religion-translation.dto';

export class ReligionUpdateDTO extends BaseDTO {
    @ApiModelProperty({ type: [ReligionTranslationDTO], description: 'Update religion with translation' })
    translations: ReligionTranslationDTO[];
}
