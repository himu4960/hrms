import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from './../base.dto';
import { ReligionTranslationDTO } from './religion-translation.dto';

export class ReligionCreateDTO extends BaseDTO {
    @ApiModelProperty({ type: [ReligionTranslationDTO], description: 'Create religion with translation' })
    translations: ReligionTranslationDTO[];
}
