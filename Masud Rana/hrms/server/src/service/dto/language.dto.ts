/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';

import { BaseDTO } from './base.dto';

/**
 * A LanguageDTO object.
 */
export class LanguageDTO extends BaseDTO {
    @IsNotEmpty()
    @MaxLength(2)
    @ApiModelProperty({ description: 'code field' })
    code: string;

    @IsNotEmpty()
    @MaxLength(15)
    @ApiModelProperty({ description: 'name field' })
    name: string;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'is_active field' })
    is_active?: boolean;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'is_deleted field' })
    is_deleted?: boolean;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'is_default field' })
    is_default: boolean;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
