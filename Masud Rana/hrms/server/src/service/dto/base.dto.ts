/**
 * A DTO base object.
 */
export class BaseDTO {
    id?: number;

    created_at?: number;

    updated_at?: number;

    created_by?: number;

    updated_by?: number;
}
