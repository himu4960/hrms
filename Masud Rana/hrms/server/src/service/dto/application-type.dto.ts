/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';

import { BaseDTO } from './base.dto';

/**
 * A ApplicationTypeDTO object.
 */
export class ApplicationTypeDTO extends BaseDTO {
    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ description: 'name field' })
    name: string;

    @ApiModelProperty({ description: 'is_active field', required: false })
    is_active: boolean;

    @ApiModelProperty({ description: 'is_deleted field', required: false })
    is_deleted: boolean;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
