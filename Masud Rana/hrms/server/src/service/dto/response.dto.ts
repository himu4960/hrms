export class ResponseDTO {
    status?: boolean;
    statusCode?: number;
    message?: string;
    translateCode?: string;
    data?: any | any[];
}
