/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, Max } from 'class-validator';

import { BaseDTO } from './base.dto';

/**
 * A EmployeeSkillDTO object.
 */
export class EmployeeSkillDTO extends BaseDTO {
    @IsNotEmpty()
    @Max(11)
    @ApiModelProperty({ example: 1, description: 'employee_id field' })
    employee_id: number;

    @IsNotEmpty()
    @Max(11)
    @ApiModelProperty({ example: 1, description: 'skill_id field' })
    skill_id: number;
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
