import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class RoleUpdateDTO {
    @IsNotEmpty()
    @ApiModelProperty({ example: 1, description: 'employee_id field' })
    employee_id: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 2, description: 'role_id field', required: true })
    role_id: number;
}
