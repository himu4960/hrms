/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, Max } from 'class-validator';

import { BaseDTO } from '../base.dto';
import { CompanyDTO } from '../company.dto';

import { BreakRuleConditionDTO } from './break-rule-condition.dto';

/**
 * A BreakRuleDTO object.
 */
export class BreakRuleDTO extends BaseDTO {
    company_id?: number;

    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ example: 'test break rule', description: 'name field' })
    name: string;

    @IsNotEmpty()
    @ApiModelProperty({ example: 'test rule description', description: 'rule_description field' })
    rule_description: string;

    company?: CompanyDTO;

    @ApiModelProperty({
        example: [
            {
                type: 'test type',
                min_shift_length: 480,
                max_shift_length: 960,
                break_duration: 60,
                from: 100,
                to: 150,
                break_starts_at: 200,
            },
        ],
        type: [BreakRuleConditionDTO],
        description: 'break rule conditions',
    })
    break_rule_conditions?: BreakRuleConditionDTO[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
