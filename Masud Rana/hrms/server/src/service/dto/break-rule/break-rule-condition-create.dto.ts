/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, Max } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A BreakRuleConditionDTO object.
 */
export class BreakRuleConditionCreateDTO extends BaseDTO {
    @IsNotEmpty()
    @Max(11)
    @ApiModelProperty({ example: 1, description: 'break_rule_id field' })
    break_rule_id: number;

    @IsNotEmpty()
    @MaxLength(50)
    @ApiModelProperty({ example: 'test type', description: 'type field' })
    type: any;

    @ApiModelProperty({ example: true, description: 'is_paid field' })
    is_paid: boolean;

    @IsNotEmpty()
    @ApiModelProperty({ example: 420, description: 'min_shift_length field' })
    min_shift_length: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 420, description: 'max_shift_length field' })
    max_shift_length: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 30, description: 'break_duration field' })
    break_duration: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 420, description: 'from field' })
    from: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 420, description: 'to field' })
    to: number;

    @IsNotEmpty()
    @ApiModelProperty({ example: 420, description: 'break_starts_at field' })
    break_starts_at: number;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
