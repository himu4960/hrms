/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { MaxLength, IsOptional } from 'class-validator';

import { BaseDTO } from '../base.dto';
import { BreakRuleConditionUpdateDTO } from './break-rule-condition-update.dto';

/**
 * A BreakRuleDTO object.
 */
export class BreakRuleUpdateDTO extends BaseDTO {
    @IsOptional()
    @MaxLength(255)
    @ApiModelProperty({ example: 'test break rule', description: 'name field' })
    name?: string;

    @IsOptional()
    @ApiModelProperty({ example: 'test rule description', description: 'rule_description field' })
    rule_description?: string;

    @ApiModelProperty({
        example: [
            {
                id: 1,
                type: 'test type',
                min_shift_length: 480,
                max_shift_length: 960,
                break_duration: 60,
                from: 100,
                to: 150,
                break_starts_at: 200,
            },
        ],
        type: [BreakRuleUpdateDTO],
        description: 'break rule conditions',
    })
    break_rule_conditions?: BreakRuleConditionUpdateDTO[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
