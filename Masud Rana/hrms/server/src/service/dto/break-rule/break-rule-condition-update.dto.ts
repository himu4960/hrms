/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { MaxLength, IsOptional } from 'class-validator';

/**
 * A BreakRuleConditionDTO object.
 */
export class BreakRuleConditionUpdateDTO {
    @ApiModelProperty({ example: 1, description: 'break_rule_condition id field' })
    id: number;

    @ApiModelProperty({ example: 1, description: 'break_rule_id field' })
    break_rule_id: number;

    @IsOptional()
    @MaxLength(50)
    @ApiModelProperty({ example: 'test type', description: 'type field' })
    type: any;

    @ApiModelProperty({ example: true, description: 'is_paid field' })
    is_paid: boolean;

    @IsOptional()
    @ApiModelProperty({ example: '01:30', description: 'min_shift_length field' })
    min_shift_length: number;

    @IsOptional()
    @ApiModelProperty({ example: '06:30', description: 'max_shift_length field' })
    max_shift_length: number;

    @IsOptional()
    @ApiModelProperty({ example: 30, description: 'break_duration field' })
    break_duration: number;

    @IsOptional()
    @ApiModelProperty({ example: '01:30', description: 'from field' })
    from: number;

    @IsOptional()
    @ApiModelProperty({ example: '05:30', description: 'to field' })
    to: number;

    @IsOptional()
    @ApiModelProperty({ example: '02:30', description: 'break_starts_at field' })
    break_starts_at: number;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
