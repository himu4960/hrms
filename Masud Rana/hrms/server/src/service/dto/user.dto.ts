import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsEmail } from 'class-validator';
import { Exclude } from 'class-transformer';

import { BaseDTO } from './base.dto';

/**
 * An User DTO object.
 */
export class UserDTO extends BaseDTO {
    @ApiModelProperty({ uniqueItems: true, example: 'samdani', description: 'Username' })
    @IsString()
    username: string;

    @ApiModelProperty({ example: 'Golam', description: 'User first name', required: false })
    firstName?: string;

    @ApiModelProperty({ example: 'Samdani', description: 'User last name', required: false })
    lastName?: string;

    @ApiModelProperty({ example: 'samdani@gmail.com', description: 'User email' })
    @IsEmail()
    email: string;

    @Exclude()
    @ApiModelProperty({ example: 'myuser', description: 'User password', required: false })
    password?: string;

    password_reset_at?: number;

    @ApiModelProperty({ example: 'en', description: 'User language', required: false })
    langKey?: string;

    @ApiModelProperty({ example: 'http://my-image-url', description: 'Image url', required: false })
    imageUrl?: string;

    @ApiModelProperty({ description: 'User Companies', required: false })
    user_companies?: any[];

    @ApiModelProperty({ description: 'User Passwords', required: false })
    user_passwords?: any[];
}
