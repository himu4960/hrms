/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from '../base.dto';

/**
 * A TimeClockEventDTO Note object.
 */

export class TimeClockEventNoteDTO extends BaseDTO {
    @ApiModelProperty({ example: 1, description: 'time_clock_id field' })
    time_clock_id: number;

    @ApiModelProperty({ example: 'Example notes', description: 'note field', required: false })
    note: string;
}
