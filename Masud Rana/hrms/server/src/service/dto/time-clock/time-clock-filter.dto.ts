/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

/**
 * A TimeClockDTO filter object.
 */
export class TimeClockFilterDTO {
    @IsOptional()
    @ApiModelProperty({ example: Date.now(), description: 'filter_start_date field' })
    filter_start_date?: number;

    @IsOptional()
    @ApiModelProperty({ example: Date.now(), description: 'filter_end_date field' })
    filter_end_date?: number;

    @IsOptional()
    @ApiModelProperty({ example: false, description: 'is_approved field' })
    is_approved?: boolean;

    @ApiModelProperty({ example: 1, description: 'employee_id field' })
    employee_id: number;
}
