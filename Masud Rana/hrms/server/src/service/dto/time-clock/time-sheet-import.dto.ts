import { ApiModelProperty } from '@nestjs/swagger';

export class TimeSheetImportDTO {
    @ApiModelProperty({ example: Date.now(), description: 'clock_start_time field', required: false })
    clock_start_time: number;

    @ApiModelProperty({ example: Date.now(), description: 'clock_end_time field', required: false })
    clock_end_time: number;

    @ApiModelProperty({ example: 'info@info.com', description: 'email field', required: false })
    email: string;

    @ApiModelProperty({ example: 1, description: 'notes field', required: false })
    notes: string;

    @ApiModelProperty({ example: 'Software Engineer', description: 'position field', required: false })
    position: string;

    @ApiModelProperty({ example: 'Down Town', description: 'remote_site field', required: false })
    remote_site: string;

    @ApiModelProperty({ example: 90, description: 'tips field', required: false })
    tips: number;
}