/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional, Max } from 'class-validator';

import { BaseDTO } from '../base.dto';
import { TimeClockEventUpdateDTO } from './time-clock-event-update.dto';

import { ETimeClockEventType } from './../../../domain/enum/time-clock.enum';

/**
 * A TimeClockDTO Update object.
 */
export class TimeClockUpdateDTO extends BaseDTO {
    @IsOptional()
    @ApiModelProperty({ example: true, description: 'is_approved field', required: false })
    is_approved?: boolean;

    @IsOptional()
    @Max(11)
    @ApiModelProperty({ example: 3, description: 'approved_by field', required: false })
    approved_by?: number;

    @IsOptional()
    @ApiModelProperty({ example: 'approved', description: 'approved_note field', required: false })
    approved_note?: string;

    @IsOptional()
    @ApiModelProperty({ example: 'approved note', description: 'notes field', required: false })
    notes?: string;

    @IsOptional()
    @ApiModelProperty({ example: Date.now(), description: 'pre_clock_time field', required: false })
    pre_clock_time?: number;

    @IsOptional()
    @ApiModelProperty({ example: Date.now(), description: 'clock_start_time field', required: false })
    clock_start_time?: number;

    @IsOptional()
    @ApiModelProperty({ example: Date.now(), description: 'clock_end_time field', required: false })
    clock_end_time?: number;

    @ApiModelProperty({ example: 1, description: 'branch_id field', required: false })
    branch_id?: number;

    @ApiModelProperty({ example: 1, description: 'employee_id field', required: false })
    employee_id?: number;

    @ApiModelProperty({ example: 1, description: 'designation_id field', required: false })
    designation_id?: number;

    @ApiModelProperty({ example: 1, description: 'request_ip field', required: false })
    request_ip?: string;

    @IsOptional()
    @ApiModelProperty({ example: '23.7104', description: 'clockin_latitude field', required: false })
    clockin_latitude?: number;

    @IsOptional()
    @ApiModelProperty({ example: '90.4074', description: 'clockin_longitude field', required: false })
    clockin_longitude?: number;

    @IsOptional()
    @ApiModelProperty({ example: '23.7104', description: 'clockout_latitude field', required: false })
    clockout_latitude?: number;

    @IsOptional()
    @ApiModelProperty({ example: '90.4074', description: 'clockout_longitude field', required: false })
    clockout_longitude?: number;

    @IsOptional()
    @ApiModelProperty({
        example: [
            {
                event_type: ETimeClockEventType.NOTE,
                created_at: new Date().toISOString(),
                note: 'first note',
            },
        ],
        description: 'clock_end_time field',
        required: false,
        isArray: true,
    })
    time_clock_events?: TimeClockEventUpdateDTO[];
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
