/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from '../base.dto';

/**
 * A TimeClockEventDTO Break object.
 */

export class TimeClockEventBreakDTO extends BaseDTO {
    @ApiModelProperty({ example: 1, description: 'time_clock_id field' })
    time_clock_id: number;
}
