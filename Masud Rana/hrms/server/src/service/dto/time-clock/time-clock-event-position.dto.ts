/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from '../base.dto';

/**
 * A TimeClockEventDTO Position object.
 */

export class TimeClockEventPositionDTO extends BaseDTO {
    @ApiModelProperty({ example: 1, description: 'time_clock_id field' })
    time_clock_id: number;

    @ApiModelProperty({ description: 'new_position_id field', required: false })
    new_position_id: number;
}
