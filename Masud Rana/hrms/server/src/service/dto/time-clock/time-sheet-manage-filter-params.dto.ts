import { ApiModelProperty } from '@nestjs/swagger';

export class TimeSheetManageFilterParamsDTO {
    @ApiModelProperty({ example: Date.now(), description: 'start_date field', required: false })
    start_date: number;

    @ApiModelProperty({ example: Date.now(), description: 'end_date field', required: false })
    end_date: number;

    @ApiModelProperty({ example: 1, description: 'designation_id field', required: false })
    designation_id: string;

    @ApiModelProperty({ example: 1, description: 'skill_id field', required: false })
    skill_id: string;

    @ApiModelProperty({ example: 1, description: 'employee_id field', required: false })
    employee_id: string;

    @ApiModelProperty({ example: 1, description: 'is_approved field', required: false })
    is_approved: boolean;
}
