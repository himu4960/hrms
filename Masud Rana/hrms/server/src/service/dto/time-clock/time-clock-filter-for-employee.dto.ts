/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from '../base.dto';

/**
 * A TimeClockFilterForEmployeeDTO object.
 */

export class TimeClockFilterForEmployeeDTO extends BaseDTO {
    @ApiModelProperty({ example: 1, description: 'employee_id field', required: false })
    employee_id: number;
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
