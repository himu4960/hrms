/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional, Max } from 'class-validator';

import { BaseDTO } from '../base.dto';
import { BranchDTO } from '../branch/branch.dto';
import { CompanyDTO } from '../company.dto';
import { EmployeeDTO } from '../employee/employee.dto';
import { TimeClockEventDTO } from './time-clock-event.dto';

/**
 * A TimeClockDTO object.
 */

export class TimeClockDTO extends BaseDTO {
    @ApiModelProperty({ description: 'is_approved field', required: false })
    is_approved?: boolean;

    @IsOptional()
    @Max(11)
    @ApiModelProperty({ description: 'approved_by field', required: false })
    approved_by?: number;

    @ApiModelProperty({ example: Date.now(), description: 'approved_at field', required: false })
    approved_at?: number;

    @ApiModelProperty({ description: 'approved_note field', required: false })
    approved_note?: string;

    @ApiModelProperty({ example: Date.now(), description: 'pre_clock_time field', required: false })
    pre_clock_time?: number;

    @ApiModelProperty({ example: Date.now(), description: 'clock_start_time field', required: false })
    clock_start_time?: number;

    @ApiModelProperty({ example: Date.now(), description: 'clock_end_time field', required: false })
    clock_end_time?: number;

    @IsOptional()
    @Max(8)
    @ApiModelProperty({ description: 'total_clock_duration field', required: false })
    total_clock_duration?: number;

    @IsOptional()
    @Max(8)
    @ApiModelProperty({ description: 'total_break_duration field', required: false })
    total_break_duration?: number;

    @IsOptional()
    @Max(11)
    @ApiModelProperty({ description: 'total_break_taken field', required: false })
    total_break_taken?: number;

    @ApiModelProperty({ description: 'notes field', required: false })
    notes?: string;

    @ApiModelProperty({ description: 'branch_id field', required: false })
    branch_id: number;

    @ApiModelProperty({ description: 'employee_id field', required: false })
    employee_id: number;

    @ApiModelProperty({ description: 'designation_id field', required: false })
    designation_id: number;

    @ApiModelProperty({ description: 'request_ip field', required: false })
    request_ip?: string;

    @ApiModelProperty({ description: 'time_clock_events field', required: false })
    time_clock_events?: TimeClockEventDTO[];

    @ApiModelProperty({ description: 'is_running field', required: false })
    is_running?: boolean;

    work_shift_id?: number;

    company?: CompanyDTO;

    employee?: EmployeeDTO;

    branch?: BranchDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
