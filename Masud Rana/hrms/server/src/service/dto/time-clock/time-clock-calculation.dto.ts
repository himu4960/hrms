/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';

/**
 * A TimeClockCalculationDTO object.
 */

export class TimeClockCalculationDTO {
    @ApiModelProperty({ description: 'total_time_clock_duration field' })
    total_duration: number;

    @ApiModelProperty({ description: 'total_time_clock_break_duration field' })
    total_break_duration: number;

    @ApiModelProperty({ description: 'total_time_clock_without_break_duration field' })
    total_without_break_duration: number;
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
