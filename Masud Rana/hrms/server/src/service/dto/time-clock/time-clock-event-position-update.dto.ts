/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from '../base.dto';

/**
 * A TimeClockEventDTO Update object.
 */

export class TimeClockEventPositionUpdateDTO extends BaseDTO {
    @ApiModelProperty({ example: 1, description: 'prev_position_id field', required: false })
    prev_position_id?: number;

    @ApiModelProperty({ example: 1, description: 'new_position_id field', required: false })
    new_position_id?: number;
}
