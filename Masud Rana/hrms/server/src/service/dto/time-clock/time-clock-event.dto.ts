/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { Max, IsOptional } from 'class-validator';

import { BaseDTO } from '../base.dto';
import { TimeClockDTO } from './time-clock.dto';
import { DesignationDTO } from '../designation/designation.dto';

import { ETimeClockEventType } from './../../../domain/enum/time-clock.enum';

/**
 * A TimeClockEventDTO object.
 */

export class TimeClockEventDTO extends BaseDTO {
    @ApiModelProperty({
        example: [ETimeClockEventType.BREAK, ETimeClockEventType.NOTE, ETimeClockEventType.POSITION],
        description: 'event_type field',
        required: false,
    })
    event_type: ETimeClockEventType;

    @ApiModelProperty({ example: Date.now(), description: 'break_start_time field', required: false })
    break_start_time: number;

    @ApiModelProperty({ example: Date.now(), description: 'break_end_time field', required: false })
    break_end_time: number;

    @IsOptional()
    @Max(8)
    @ApiModelProperty({ description: 'total_break_duration field', required: false })
    total_break_duration: number;

    @ApiModelProperty({ description: 'prev_position_id field', required: false })
    prev_position_id: number;

    @ApiModelProperty({ description: 'new_position_id field', required: false })
    new_position_id: number;

    @ApiModelProperty({ description: 'note field', required: false })
    note: string;

    @ApiModelProperty({ example: 1, description: 'time_clock_id field' })
    time_clock_id: number;

    @ApiModelProperty({ type: TimeClockDTO, description: 'time_clock relationship' })
    time_clock?: TimeClockDTO;

    @ApiModelProperty({ type: DesignationDTO, description: 'new_designation' })
    new_designation?: DesignationDTO;

    company_id?: number;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
