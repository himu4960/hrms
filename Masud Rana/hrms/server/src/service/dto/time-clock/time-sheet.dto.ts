/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from '../base.dto';
import { TimeClockDTO } from './time-clock.dto';

/**
 * A TimeSheetDTO object.
 */

export class TimeSheetDTO extends BaseDTO {
    @ApiModelProperty({ description: 'time_clocks field' })
    time_clocks: TimeClockDTO[];

    @ApiModelProperty({ description: 'total_time_clock_duration field', required: false })
    total_time_clock_duration?: number;

    @ApiModelProperty({ description: 'total_time_clock_break_duration field', required: false })
    total_time_clock_break_duration?: number;

    @ApiModelProperty({ description: 'total_time_clock_without_break_duration field', required: false })
    total_time_clock_without_break_duration?: number;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
