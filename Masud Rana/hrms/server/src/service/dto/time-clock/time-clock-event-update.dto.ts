/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { Max, IsOptional } from 'class-validator';

import { BaseDTO } from '../base.dto';

import { ETimeClockEventType } from '../../../domain/enum/time-clock.enum';

/**
 * A TimeClockEventDTO object.
 */

export class TimeClockEventUpdateDTO extends BaseDTO {
    @ApiModelProperty({
        example: [ETimeClockEventType.BREAK, ETimeClockEventType.NOTE, ETimeClockEventType.POSITION],
        description: 'event_type field',
        required: false,
    })
    event_type: ETimeClockEventType;

    @ApiModelProperty({ example: Date.now(), description: 'break_start_time field', required: false })
    break_start_time: number;

    @ApiModelProperty({ example: Date.now(), description: 'break_end_time field', required: false })
    break_end_time: number;

    @IsOptional()
    @Max(8)
    @ApiModelProperty({ description: 'total_break_duration field', required: false })
    total_break_duration: number;

    @ApiModelProperty({ description: 'prev_position_id field', required: false })
    prev_position_id: number;

    @ApiModelProperty({ description: 'new_position_id field', required: false })
    new_position_id: number;

    @ApiModelProperty({ description: 'note field', required: false })
    note: string;

    @ApiModelProperty({ example: 1, description: 'time_clock_id field', required: false })
    time_clock_id: number;

    @ApiModelProperty({ example: 1, description: 'company_id field', required: false })
    company_id?: number;
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
