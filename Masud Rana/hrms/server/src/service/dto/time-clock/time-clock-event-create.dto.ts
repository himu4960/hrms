/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { Max, IsOptional } from 'class-validator';

import { BaseDTO } from '../base.dto';

import { ETimeClockEventType } from './../../../domain/enum/time-clock.enum';

/**
 * A TimeClockEventDTO Create object.
 */

export class TimeClockEventCreateDTO extends BaseDTO {
    @ApiModelProperty({ example: 1, description: 'time_clock_id field' })
    time_clock_id: number;

    @ApiModelProperty({
        enum: [ETimeClockEventType.NOTE],
        description: 'event_type field',
    })
    event_type: string | any;

    @ApiModelProperty({ example: 1, description: 'new_position_id field', required: false })
    new_position_id?: number;

    @ApiModelProperty({ example: 'example notes', description: 'note field', required: false })
    note?: string;
}
