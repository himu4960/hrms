import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from '../base.dto';

export class RegisterCompanyDTO extends BaseDTO {
    @ApiModelProperty({ example: 6, description: 'User ID' })
    user_id: number;

    @ApiModelProperty({ example: 'Mind Orbital Technologies', description: 'Company Name' })
    name: string;

    @ApiModelProperty({ example: '01672066834', description: 'Phone Number' })
    phone?: string;

    @ApiModelProperty({ example: 'admin', description: 'User password' })
    password: string;

    @ApiModelProperty({ example: 1, description: 'Company Type ID' })
    company_type_id: number;

    @ApiModelProperty({ example: 1, description: 'Application Type ID' })
    application_type_id: number;
}
