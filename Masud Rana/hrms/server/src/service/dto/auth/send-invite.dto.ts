import { ApiModelProperty } from '@nestjs/swagger';

export class SendInviteDTO {
    @ApiModelProperty({ description: 'Token' })
    token: string;
}
