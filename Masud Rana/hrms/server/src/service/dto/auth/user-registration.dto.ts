import { ApiModelProperty } from '@nestjs/swagger';
import { IsEmail } from 'class-validator';
import { BaseDTO } from '../base.dto';

export class UserRegistrationDTO extends BaseDTO {
    @ApiModelProperty({ example: 'Tertha Dev Sarker', description: 'Full name' })
    fullName: string;

    @ApiModelProperty({ example: 'devteertha28@gmail.com', description: 'Email address' })
    @IsEmail()
    email: string;
}
