import { ApiModelProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

import { BaseDTO } from '../base.dto';

export class InvitedNewUserRegistrationDTO extends BaseDTO {
    @ApiModelProperty({ uniqueItems: true, example: 'subUser', description: 'Sub User login' })
    @IsString()
    username: string;

    @ApiModelProperty({ example: '01672066834', description: 'Phone Number' })
    phone?: string;

    @ApiModelProperty({ example: 'subuser', description: 'User password' })
    password: string;

    @ApiModelProperty({ example: 'subuser', description: 'User first name', required: false })
    firstName?: string;

    @ApiModelProperty({ example: 'subuser', description: 'User last name', required: false })
    lastName?: string;

    @ApiModelProperty({ example: 'ironman.man28@gmail.com', description: 'Email' })
    email?: string;

    @ApiModelProperty({ example: 1, description: 'company_id' })
    company_id?: number;

    @ApiModelProperty({ example: 1, description: 'branch_id' })
    branch_id?: number;

    @ApiModelProperty({ example: 1, description: 'employee_id' })
    employee_id?: number;

    @ApiModelProperty({ example: 1, description: 'user_id' })
    user_id?: number;
}
