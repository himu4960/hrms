import { ApiModelProperty } from '@nestjs/swagger';

export class RefreshTokenDTO {
    @ApiModelProperty({ description: 'refresh_token field' })
    refresh_token?: string;

    user_id: number;

    company_id: number;
}