import { ApiModelProperty } from '@nestjs/swagger';

import { BranchDTO } from '../branch/branch.dto';
import { CompanyDTO } from '../company.dto';
import { EmployeeDTO } from '../employee/employee.dto';

export class RegisterCompanyResponsenDTO {
    @ApiModelProperty({ example: EmployeeDTO, description: 'employee' })
    employee: EmployeeDTO;

    @ApiModelProperty({ example: CompanyDTO, description: 'company' })
    company: CompanyDTO;

    @ApiModelProperty({ example: BranchDTO, description: 'branch' })
    branch: BranchDTO;
}
