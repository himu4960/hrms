import { ApiModelProperty } from '@nestjs/swagger';

import { ApplicationTypeDTO } from '../application-type.dto';
import { CompanyTypeDTO } from '../company-type.dto';

export class AuthSetupDTO {
    @ApiModelProperty({ example: ApplicationTypeDTO, description: 'Application Type field' })
    application_types: ApplicationTypeDTO[];

    @ApiModelProperty({ example: CompanyTypeDTO, description: 'Company Type field' })
    company_types: CompanyTypeDTO[];
}