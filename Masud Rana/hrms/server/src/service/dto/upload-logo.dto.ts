import { ApiModelProperty } from '@nestjs/swagger';

export class UploadLogoDTO {
    @ApiModelProperty({ type: 'string', format: 'binary', required: true })
    file: Express.Multer.File;

    @ApiModelProperty({ type: 'integer', required: true })
    company_id: number;

    user_id?: number;
}
