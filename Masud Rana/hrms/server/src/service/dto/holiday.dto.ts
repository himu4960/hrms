/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, Length } from 'class-validator';
import { BaseDTO } from './base.dto';

/**
 * A HolidayDTO object.
 */
export class HolidayDTO extends BaseDTO {
    @IsNotEmpty()
    @ApiModelProperty({ description: 'date field' })
    date: number;

    @IsNotEmpty()
    @Length(3, 255)
    @ApiModelProperty({ description: 'title field' })
    title: string;

    @ApiModelProperty({ description: 'description field', required: false })
    description: string;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'code field', required: true })
    code: string;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
