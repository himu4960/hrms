import { ApiModelProperty } from '@nestjs/swagger';

/**
 * A DTO representing a password change required data - current and new password.
 */
export class BusinessChangeDTO {
    @ApiModelProperty({ example: 3, description: 'Current user id', required: true })
    user_id: number;

    @ApiModelProperty({ example: 1, description: 'Current user company id', required: true })
    company_id: number;

    @ApiModelProperty({ example: "HR45600JK", description: 'Current user device token', required: false })
    device_token?: string;
}
