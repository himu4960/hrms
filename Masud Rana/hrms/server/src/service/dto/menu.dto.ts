/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, Max, IsOptional } from 'class-validator';

import { BaseDTO } from './base.dto';

/**
 * A MenuDTO object.
 */
export class MenuDTO extends BaseDTO {
    @IsNotEmpty()
    @MaxLength(50)
    @ApiModelProperty({ description: 'name field' })
    name: string;

    @MaxLength(50)
    @IsOptional()
    @ApiModelProperty({ description: 'icon field', required: false })
    icon: string;

    @IsOptional()
    @MaxLength(50)
    @ApiModelProperty({ description: 'end_point field' })
    end_point: string;

    @IsOptional()
    @MaxLength(50)
    @ApiModelProperty({ description: 'router_link field' })
    router_link: string;

    @IsOptional()
    @ApiModelProperty({ description: 'permissions field', required: false })
    permissions: string;

    @IsOptional()
    @ApiModelProperty({ description: 'parent_id field', required: false })
    parent_id: number;

    @IsOptional()
    @ApiModelProperty({ description: 'position field' })
    position: number;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'is_menu field' })
    is_menu: boolean;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'is_active field' })
    is_active: boolean;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'is_deleted field' })
    is_deleted: boolean;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
