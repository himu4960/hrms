/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, IsOptional } from 'class-validator';

import { BaseDTO } from './base.dto';
import { CustomFieldTypeDTO } from './custom-field-type.dto';

/**
 * A CustomFieldDTO object.
 */
export class CustomFieldDTO extends BaseDTO {
    
    @ApiModelProperty({ description: 'company id' })
    company_id: number;

    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ description: 'name field' })
    name: string;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'custom_field_type ID field' })
    custom_field_type_id: number;

    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ description: 'permission_type field' })
    permission_type: string;

    @IsOptional()
    @MaxLength(800)
    @ApiModelProperty({ description: 'value field', required: false })
    value: string;
    
    custom_field_type?: CustomFieldTypeDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}