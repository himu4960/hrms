/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, Max } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A GenderTranslationDTO object.
 */
export class GenderTranslationDTO extends BaseDTO {
    @Max(50)
    @IsNotEmpty()
    @MaxLength(50)
    @ApiModelProperty({ description: 'name field' })
    name: string;

    @IsNotEmpty()
    @MaxLength(2)
    @ApiModelProperty({ description: 'language_code field', required: false })
    language_code: string;
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
