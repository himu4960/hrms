import { ApiModelProperty } from '@nestjs/swagger';
import { GenderTranslationDTO } from './gender-translation.dto';

export class GenderAllDTO {
    @ApiModelProperty({ description: 'Gender id' })
    id: number;

    @ApiModelProperty({ type: [GenderTranslationDTO], description: 'Gender with translation' })
    translations: GenderTranslationDTO[];
}
