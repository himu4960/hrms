import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from '../base.dto';
import { GenderTranslationDTO } from './gender-translation.dto';

export class GenderUpdateDTO extends BaseDTO {
    @ApiModelProperty({ type: [GenderTranslationDTO], description: 'Update gender with translation' })
    translations: GenderTranslationDTO[];
}
