import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from '../base.dto';
import { GenderTranslationDTO } from './gender-translation.dto';

export class GenderCreateDTO extends BaseDTO {
    @ApiModelProperty({ type: [GenderTranslationDTO], description: 'Create gender with translation' })
    translations: GenderTranslationDTO[];
}
