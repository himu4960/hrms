import { ApiModelProperty } from '@nestjs/swagger';

export class GenderGetByLangCodeDTO {
    @ApiModelProperty({ description: 'Gender id' })
    id: number;

    @ApiModelProperty({ description: 'Gender with name' })
    name: string;
}
