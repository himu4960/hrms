/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, MaxLength, Max } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A AnnouncementCreateDTO object.
 */
export class AnnouncementCreateDTO extends BaseDTO {
    @IsNotEmpty()
    @MaxLength(100)
    @ApiModelProperty({ example: 'We will be off tomorrow on the occasion of Durga Puja.', description: 'title field' })
    title: string;

    @IsOptional()
    @ApiModelProperty({ example: 'test', description: 'description field', required: false })
    description: string;

    @IsOptional()
    @Max(11)
    @ApiModelProperty({ example: 1, description: 'branch_id field', required: false, nullable: true })
    branch_id: number;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
