/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';

/**
 * A BranchRelationshipDTO object.
 */
export class BranchRelationshipExampleDTO {
    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ example: 'Main Branch', description: 'name field' })
    name: string;
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
