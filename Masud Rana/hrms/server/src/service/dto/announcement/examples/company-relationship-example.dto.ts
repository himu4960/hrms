/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength } from 'class-validator';

/**
 * A CompanyRelationshipDTO object.
 */
export class CompanyRelationshipExampleDTO {
    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ example: 'Admin Company', description: 'name field' })
    name: string;

    @IsNotEmpty()
    @MaxLength(255)
    @ApiModelProperty({ example: 'admin@admin.com', description: 'email field' })
    email: string;
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
