/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { MaxLength } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A AnnouncementUpdateDTO object.
 */
export class AnnouncementUpdateDTO extends BaseDTO {
    @MaxLength(100)
    @ApiModelProperty({ example: 'We will be off tomorrow on the occasion of Durga Puja.', description: 'title field' })
    title?: string;

    @MaxLength(255)
    @ApiModelProperty({ example: 'test', description: 'description field', required: false })
    description?: string;
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
