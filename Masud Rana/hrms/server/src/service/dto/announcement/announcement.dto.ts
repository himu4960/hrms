/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, Max } from 'class-validator';

import { BaseDTO } from '../base.dto';
import { BranchDTO } from '../branch/branch.dto';
import { CompanyDTO } from '../company.dto';

import { BranchRelationshipExampleDTO } from './examples/branch-relationship-example.dto';
import { CompanyRelationshipExampleDTO } from './examples/company-relationship-example.dto';

/**
 * A AnnouncementDTO object.
 */
export class AnnouncementDTO extends BaseDTO {
    @IsNotEmpty()
    @MaxLength(100)
    @ApiModelProperty({ example: 'We will be off tomorrow on the occasion of Durga Puja.', description: 'title field' })
    title: string;

    @ApiModelProperty({ example: 'test', description: 'description field', required: false })
    description: string;

    @IsNotEmpty()
    @Max(11)
    @ApiModelProperty({ example: 1, description: 'company_id field' })
    company_id: number;

    @Max(11)
    @ApiModelProperty({ example: 1, description: 'branch_id field', required: false })
    branch_id: number;

    @ApiModelProperty({ type: CompanyRelationshipExampleDTO, description: 'company relationship' })
    company: CompanyDTO;

    @ApiModelProperty({ type: BranchRelationshipExampleDTO, description: 'branch relationship' })
    branch: BranchDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
