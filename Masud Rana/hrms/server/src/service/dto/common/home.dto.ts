import { ApiModelProperty } from '@nestjs/swagger';

import { AnnouncementDTO } from '../announcement/announcement.dto';
import { EmployeeAnniversaryResponseDTO } from '../employee/employee-anniversary-response.dto';
import { EmployeeBirthdayResponseDTO } from '../employee/employee-birthday-response.dto';
import { FileDTO } from '../file.dto';
import { LeaveDTO } from '../leave/leave.dto';
import { TimeSheetLateSummaryDTO } from '../reports/time-sheet-late-summary.dto';

export class CommonHomeDTO {
    @ApiModelProperty({ type: AnnouncementDTO, isArray: true, description: 'announcements field' })
    announcements: AnnouncementDTO[];

    @ApiModelProperty({ type: FileDTO, isArray: true, description: 'files field' })
    files: FileDTO[];

    @ApiModelProperty({ type: LeaveDTO, isArray: true, description: 'on_leaves field' })
    on_leaves: LeaveDTO[];

    @ApiModelProperty({ type: TimeSheetLateSummaryDTO, isArray: true, description: 'lates field' })
    lates: TimeSheetLateSummaryDTO[];

    @ApiModelProperty({ type: EmployeeAnniversaryResponseDTO, isArray: true, description: 'anniversaries field' })
    anniversaries: EmployeeAnniversaryResponseDTO[];

    @ApiModelProperty({ type: EmployeeBirthdayResponseDTO, isArray: true, description: 'birthdays field' })
    birthdays: EmployeeBirthdayResponseDTO[];

    @ApiModelProperty({ example: 5, description: 'leave_request_count field' })
    leave_request_count: number;

    @ApiModelProperty({ example: 5, description: 'all_shift_count field' })
    all_shift_count: number;

    @ApiModelProperty({ example: 5, description: 'unpublished_shift_count field' })
    unpublished_shift_count: number;

    @ApiModelProperty({ example: 5, description: 'time_sheet_count field' })
    time_sheet_count: number;
}
