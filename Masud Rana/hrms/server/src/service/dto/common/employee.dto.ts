import { ApiModelProperty } from '@nestjs/swagger';

import { BranchDTO } from '../branch/branch.dto';
import { EmployeeListDTO } from '../employee/employee-list.dto';
import { SkillDTO } from '../skill.dto';

export class CommonEmployeeDTO {
    @ApiModelProperty({ type: EmployeeListDTO, isArray: true, description: 'employees field' })
    employees: EmployeeListDTO[];

    @ApiModelProperty({ type: BranchDTO, isArray: true, description: 'branches field' })
    branches: BranchDTO[];

    @ApiModelProperty({ type: SkillDTO, isArray: true, description: 'skills field' })
    skills: SkillDTO[];

    @ApiModelProperty({ example: 5, description: 'total_employee_count field' })
    total_employee_count: number;

    @ApiModelProperty({ example: 5, description: 'not_activated field' })
    not_activated: number;

    @ApiModelProperty({ example: 5, description: 'disabled field' })
    disabled: number;
}
