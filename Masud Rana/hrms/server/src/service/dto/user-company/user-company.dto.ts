/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from '../base.dto';

/**
 * A UserCompanyDTO object.
 */
export class UserCompanyDTO extends BaseDTO {
    @ApiModelProperty({ description: 'is_active field', required: false })
    is_active?: boolean;

    @ApiModelProperty({ description: 'is_deleted field', required: false })
    is_deleted?: boolean;

    @ApiModelProperty({ description: 'user id field' })
    user_id: number;

    @ApiModelProperty({ description: 'company id field' })
    company_id: number;

    @ApiModelProperty({ description: 'role id field' })
    role_id: number;

    @ApiModelProperty({ description: 'employee id field' })
    employee_id: number;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
