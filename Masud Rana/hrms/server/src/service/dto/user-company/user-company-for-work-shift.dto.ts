/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from '../base.dto';
import { LeaveDTO } from '../leave/leave.dto';
import { EmployeeDTO } from '../employee/employee.dto';

/**
 * A UserCompanyForWorkShiftDTO object.
 */
export class UserCompanyForWorkShiftDTO extends BaseDTO {
    @ApiModelProperty({ description: 'branch_id field' })
    branch_id?: number;

    @ApiModelProperty({ description: 'leaves field' })
    leaves?: LeaveDTO;

    @ApiModelProperty({ description: 'employee field' })
    employee?: EmployeeDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
