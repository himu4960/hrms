/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

/**
 * A GeoLocationDTO object.
 */
export class GeoLocationDTO {
    @IsOptional()
    @ApiModelProperty({ example: '134.201.250.155', description: 'ip field' })
    ip?: string;

    @IsOptional()
    @ApiModelProperty({ example: '134.201.250.155', description: 'hostname field' })
    hostname?: string;

    @IsOptional()
    @ApiModelProperty({ example: 'ipv4', description: 'type field' })
    type?: string;

    @IsOptional()
    @ApiModelProperty({ example: 'NA', description: 'continent_code field' })
    continent_code?: string;

    @IsOptional()
    @ApiModelProperty({ example: 'North America', description: 'continent_name field' })
    continent_name?: string;

    @IsOptional()
    @ApiModelProperty({ example: 'US', description: 'country_code field' })
    country_code?: string;

    @IsOptional()
    @ApiModelProperty({ example: 'United States', description: 'country_name field' })
    country_name?: string;

    @IsOptional()
    @ApiModelProperty({ example: 'CA', description: 'region_code field' })
    region_code?: string;

    @IsOptional()
    @ApiModelProperty({ example: 'California', description: 'region_name field' })
    region_name?: string;

    @IsOptional()
    @ApiModelProperty({ example: 'Los Angeles', description: 'city field' })
    city?: string;

    @IsOptional()
    @ApiModelProperty({ example: '90013', description: 'zip field' })
    zip?: string;

    @IsOptional()
    @ApiModelProperty({ example: 34.0453, description: 'latitude field' })
    latitude?: number;

    @IsOptional()
    @ApiModelProperty({ example: -118.2413, description: 'longitude field' })
    longitude?: number;

    @IsOptional()
    @ApiModelProperty({ example: 5368361, description: 'geoname_id field' })
    geoname_id?: number;

    @IsOptional()
    @ApiModelProperty({ example: 'Washington D.C.', description: 'capital field' })
    capital?: string;

    @IsOptional()
    @ApiModelProperty({ example: 'https://assets.ipstack.com/images/assets/flags_svg/us.svg', description: 'country_flag field' })
    country_flag?: string;

    @IsOptional()
    @ApiModelProperty({ example: '🇺🇸', description: 'country_flag_emoji field' })
    country_flag_emoji?: string;

    @IsOptional()
    @ApiModelProperty({ example: 'U+1F1FA U+1F1F8', description: 'country_flag_emoji_unicode field' })
    country_flag_emoji_unicode?: string;

    @IsOptional()
    @ApiModelProperty({ example: '1', description: 'calling_code field' })
    calling_code?: string;

    @IsOptional()
    @ApiModelProperty({ example: false, description: 'is_eu field' })
    is_eu?: boolean;

    @IsOptional()
    @ApiModelProperty({ example: 'America/Los_Angeles', description: 'time_zone_id field' })
    time_zone_id?: string;

    @IsOptional()
    @ApiModelProperty({ example: '2018-03-29T07:35:08-07:00', description: 'time_zone_current_time field' })
    time_zone_current_time?: string;

    @IsOptional()
    @ApiModelProperty({ example: -25200, description: 'time_zone_gmt_offset field' })
    time_zone_gmt_offset?: number;

    @IsOptional()
    @ApiModelProperty({ example: false, description: 'security_is_proxy field' })
    security_is_proxy?: boolean;

    @IsOptional()
    @ApiModelProperty({ example: 'example', description: 'security_proxy_type field' })
    security_proxy_type?: string;

    @IsOptional()
    @ApiModelProperty({ example: false, description: 'security_is_crawler field' })
    security_is_crawler?: boolean;

    @IsOptional()
    @ApiModelProperty({ example: 'example', description: 'security_crawler_name field' })
    security_crawler_name?: string;

    @IsOptional()
    @ApiModelProperty({ example: 'example', description: 'security_crawler_type field' })
    security_crawler_type?: string;

    @IsOptional()
    @ApiModelProperty({ example: false, description: 'security_is_tor field' })
    security_is_tor?: boolean;

    @IsOptional()
    @ApiModelProperty({ example: 'low', description: 'security_threat_level field' })
    security_threat_level?: string;

    @IsOptional()
    @ApiModelProperty({ example: 'example', description: 'security_threat_types field' })
    security_threat_types?: string;
}
