import { ApiModelProperty } from '@nestjs/swagger';
import { MaritalStatusTranslationDTO } from './marital-status-translation.dto';

export class MaritalStatusAllDTO {
    @ApiModelProperty({ description: 'Marital status id' })
    id: number;

    @ApiModelProperty({ type: [MaritalStatusTranslationDTO], description: 'Marital status with translation' })
    translations: MaritalStatusTranslationDTO[];
}
