import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from './../base.dto';
import { MaritalStatusTranslationDTO } from './marital-status-translation.dto';

export class MaritalStatusUpdateDTO extends BaseDTO {
    @ApiModelProperty({ type: [MaritalStatusTranslationDTO], description: 'Update marital status with translation' })
    translations: MaritalStatusTranslationDTO[];
}
