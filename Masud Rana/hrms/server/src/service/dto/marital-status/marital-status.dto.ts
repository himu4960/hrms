/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { Max } from 'class-validator';

import { BaseDTO } from '../base.dto';

/**
 * A MaritalStatusDTO object.
 */
export class MaritalStatusDTO extends BaseDTO {
    @Max(11)
    @ApiModelProperty({ description: 'company_id field', required: false })
    company_id?: number;

    @ApiModelProperty({ description: 'is_active field', required: false })
    is_active?: boolean;

    @ApiModelProperty({ description: 'is_deleted field', required: false })
    is_deleted?: boolean;

    translations?: any[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
