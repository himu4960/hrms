import { ApiModelProperty } from '@nestjs/swagger';

export class MaritalStatusGetByLangCodeDTO {
    @ApiModelProperty({ description: 'Marital status id' })
    id: number;

    @ApiModelProperty({ description: 'Marital status with name' })
    name: string;
}
