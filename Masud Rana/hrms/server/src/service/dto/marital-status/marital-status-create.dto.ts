import { ApiModelProperty } from '@nestjs/swagger';

import { BaseDTO } from './../base.dto';
import { MaritalStatusTranslationDTO } from './marital-status-translation.dto';

export class MaritalStatusCreateDTO extends BaseDTO {
    @ApiModelProperty({ type: [MaritalStatusTranslationDTO], description: 'Create marital status with translation' })
    translations: MaritalStatusTranslationDTO[];
}
