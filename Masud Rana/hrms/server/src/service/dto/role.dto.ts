/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

import { BaseDTO } from './base.dto';
import { ApplicationTypeDTO } from './application-type.dto';

/**
 * A RoleDTO object.
 */
export class RoleDTO extends BaseDTO {
    @ApiModelProperty({ example: 'SUPER_ADMIN', description: 'name field' })
    name: string;

    @IsNotEmpty()
    @ApiModelProperty({ description: 'menus field' })
    menus: string;

    @ApiModelProperty({ description: 'is_active field', required: false })
    is_active: boolean;

    @ApiModelProperty({ description: 'is_deleted field', required: false })
    is_deleted: boolean;

    @ApiModelProperty({ description: 'is_employee field', required: false })
    is_employee: boolean;

    @ApiModelProperty({ type: ApplicationTypeDTO, description: 'applicationType relationship' })
    applicationType: ApplicationTypeDTO;

    application_type_id: number;

    @ApiModelProperty({ description: 'User Companies' })
    user_companies?: any[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
