import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { EmployeeTypeDTO } from '../service/dto/employee-type.dto';

import { EmployeeTypeRepository } from '../repository/employee-type.repository';

@Injectable()
export class EmployeeTypeService {
    logger = new Logger('EmployeeTypeService');

    constructor(@InjectRepository(EmployeeTypeRepository) private employeeTypeRepository: EmployeeTypeRepository) {}

    async getAll(): Promise<EmployeeTypeDTO[]> {
        return await this.employeeTypeRepository.find({
            select: ['id', 'name', 'is_active', 'is_deleted'],
        });
    }

    async getActiveEmployeeTypes(): Promise<EmployeeTypeDTO[]> {
        return await this.employeeTypeRepository.find({
            select: ['id', 'name'],
            where: {
                is_active: true,
                is_deleted: false,
            },
        });
    }
}
