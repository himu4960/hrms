import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

import { WorkShiftRepository } from '../repository/work-shift.repository';
import { EmployeeRepository } from './../repository/employee.repository';

import { NotificationService } from '../fcm/notification.service';
import { DateTimeConvertService } from '../shared/convert/date-time-convert.service';

import { WorkShiftMapper } from '../service/mapper/work-shift.mapper';

import { LeaveDTO } from './dto/leave/leave.dto';
import { WorkShiftDTO } from './dto/work-shift/work-shift.dto';
import { WorkShiftFilterParamsDTO } from './dto/work-shift/work-shift-filter-params.dto';
import { WorkShiftUpdateDTO } from './dto/work-shift/work-shift-update.dto';
import { DateRangeDTO } from './dto/date-range.dto';
import { CreateManyWorkShiftsDTO } from './dto/work-shift/work-shift-create-many.dto';

const relationshipNames = ['company', 'branch', 'designation', 'employees'];

@Injectable()
export class WorkShiftService {
    logger = new Logger('WorkShiftService');

    constructor(
        @InjectQueue('work-shift') private workShiftQueue: Queue,
        @InjectRepository(WorkShiftRepository) private workShiftRepository: WorkShiftRepository,
        @InjectRepository(EmployeeRepository) private employeeRepository: EmployeeRepository,
        private notificationService: NotificationService,
        private dateTimeConvertService: DateTimeConvertService,
    ) {}

    async getTotalShiftCount(company_id: number): Promise<number> {
        return await this.workShiftRepository
            .createQueryBuilder('workShift')
            .where('workShift.company_id = :company_id', { company_id })
            .getCount();
    }

    async getTotalShiftRepublishCount(company_id: number): Promise<number> {
        return await this.workShiftRepository
            .createQueryBuilder('workShift')
            .where('workShift.company_id = :company_id', { company_id })
            .andWhere('workShift.is_published = false')
            .getCount();
    }

    async getUpcomingShifts(
        company_id: number,
        employee_id: number,
        isAll = false,
    ): Promise<WorkShiftDTO[] | undefined> {
        const currentTime = Date.now();
        let upcomingshiftQuery = this.workShiftRepository
            .createQueryBuilder('workShift')
            .leftJoinAndSelect('workShift.designation', 'designation')
            .leftJoinAndSelect('workShift.employees', 'employee')
            .where('workShift.company_id = :company_id', { company_id })
            .andWhere('workShift.start_date > :currentTime', { currentTime })
            .andWhere('workShift.is_published = true')
            .andWhere('employee.id = :employee_id', { employee_id })
            .select(['workShift.start_date', 'workShift.end_date', 'workShift.title', 'workShift.note'])
            .addSelect(['designation.id', 'designation.name'])
            .orderBy('workShift.start_date', 'ASC');

        if (!isAll) {
            upcomingshiftQuery = upcomingshiftQuery.limit(3);
        }

        return await upcomingshiftQuery.getMany();
    }

    async findById(id: number): Promise<WorkShiftDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.workShiftRepository.findOne(id, options);
        return WorkShiftMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<WorkShiftDTO>): Promise<WorkShiftDTO | undefined> {
        const result = await this.workShiftRepository.findOne(options);
        return WorkShiftMapper.fromEntityToDTO(result);
    }

    async findAndCount(options: FindManyOptions<WorkShiftDTO>, company_id: number): Promise<[WorkShiftDTO[], number]> {
        options.relations = relationshipNames;
        options.where = { company_id };
        return await this.workShiftRepository.findAndCount(options);
    }

    async filter(
        options?: FindManyOptions<WorkShiftDTO | LeaveDTO>,
        workShiftFilterParamsDTO?: WorkShiftFilterParamsDTO,
        company_id?: number,
    ) {
        const { branch_id, designation_ids, start_date, end_date, employee_ids, skill_ids } = workShiftFilterParamsDTO;

        const filter_start_date = new Date(start_date).getTime();

        const filter_end_date = new Date(end_date).getTime();

        let employee = this.employeeRepository
            .createQueryBuilder('employee')
            .leftJoinAndSelect(
                'employee.leaves',
                'leave',
                `${start_date && end_date ? 'leave.start_date > :start_date AND leave.end_date < :end_date' : 'TRUE'}`,
                {
                    start_date: filter_start_date,
                    end_date: filter_end_date,
                },
            )
            .leftJoinAndSelect('employee.user_company', 'user_company')
            .leftJoinAndSelect('employee.employee_designations', 'employee_designation')
            .leftJoinAndSelect('employee_designation.designation', 'designation')
            .leftJoinAndSelect('employee.skills', 'skills')
            .leftJoinAndSelect(
                'employee.work_shifts',
                'work_shift',
                `${
                    start_date && end_date
                        ? 'work_shift.start_date > :start_date AND work_shift.end_date < :end_date'
                        : 'TRUE'
                }`,
                {
                    start_date: filter_start_date,
                    end_date: filter_end_date,
                },
            )
            .select([
                'employee',
                'leave',
                'work_shift.id',
                'work_shift.title',
                'work_shift.is_published',
                'work_shift.start_date',
                'work_shift.end_date',
                'work_shift.start_time',
                'work_shift.end_time',
                'work_shift.total_work_shift_duration',
                'work_shift.skills',
                'work_shift.note',
                'employee_designation',
                'designation.id',
                'designation.name',
                'skills',
            ])
            .addSelect(['work_shift.designation_id', 'employee.branch_id'])
            .where('employee.company_id = :company_id', { company_id })
            .andWhere('user_company.is_active = true');

        if (skill_ids?.length) {
            employee.andWhere('skills.id IN (:skill_ids)', { skill_ids });
        }

        if (employee_ids?.length) {
            employee.andWhere('employee.id IN (:employee_ids)', { employee_ids });
        }

        if (designation_ids.length) {
            employee.andWhere('designation.id IN (:designation_ids)', {
                designation_ids,
            });
        }

        if (branch_id) {
            employee.andWhere('employee.branch_id = :branch_id', { branch_id });
        }

        return await employee.getMany();
    }

    async save(workShiftDTO: WorkShiftDTO, company_id: number, creator?: number): Promise<WorkShiftDTO | undefined> {
        try {
            const entity = WorkShiftMapper.fromDTOtoEntity(workShiftDTO);
            if (creator) {
                if (!entity.created_by) {
                    entity.created_by = creator;
                    // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                    entity.created_at = Date.now();
                }
                entity.updated_by = creator;
            }
            const result = await this.workShiftRepository.save(entity);

            const deviceTokens = await this.notificationService.getDeviceTokens({
                where: {
                    company_id,
                    employee_id: entity.employees[0].id,
                },
            });

            const date = this.dateTimeConvertService.dateDisplayFormat(result.start_date);
            const startTime = this.dateTimeConvertService.getTimeFromUnixTimestamp(result.start_date);
            const endTime = this.dateTimeConvertService.getTimeFromUnixTimestamp(result.end_date);

            if (deviceTokens.length) {
                const payload = {
                    notification: {
                        title: 'New shift schedule has been assigned to you',
                        body: `Your shift timings will be from ${startTime} to ${endTime} on ${date}`,
                    },
                };
                await this.notificationService.sendNotification(deviceTokens, payload);
            }
            return WorkShiftMapper.fromEntityToDTO(result);
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async saveMany(
        workShiftCreateManyDTO: CreateManyWorkShiftsDTO,
        company_id: number,
        creator?: number,
    ): Promise<any | undefined> {
        const job = await this.workShiftQueue.add('create-work-shifts-at-once', {
            work_shift_create_many: { ...workShiftCreateManyDTO, created_by: creator },
            company_id,
        });
        return job;
    }

    async update(
        id: number,
        workShiftUpdateDTO: WorkShiftUpdateDTO,
        company_id: number,
        updater?: number,
    ): Promise<any | undefined> {
        try {
            const { title, start_date, end_date, start_time, end_time, skills, note } = workShiftUpdateDTO;
            const total_work_shift_duration: number = (end_date - start_date) / 1000;

            const workShiftEntity = await this.workShiftRepository.findOne(id);

            workShiftEntity.title = title;
            workShiftEntity.start_date = start_date;
            workShiftEntity.end_date = end_date;
            workShiftEntity.start_time = start_time;
            workShiftEntity.end_time = end_time;
            workShiftEntity.total_work_shift_duration = total_work_shift_duration;
            workShiftEntity.skills = skills;
            workShiftEntity.note = note;

            if (updater) {
                workShiftEntity.updated_by = updater;
                workShiftEntity.updated_at = Date.now();
            }

            await this.workShiftRepository.save(workShiftEntity);
            const updatedEntity = await this.workShiftRepository.findOne(id);
            if (!updatedEntity) {
                throw new HttpException('Work Shift not found!', HttpStatus.NOT_FOUND);
            }

            const deviceTokens = await this.notificationService.getDeviceTokens({
                where: {
                    company_id,
                    employee_id: updatedEntity.employees[0].id,
                },
            });

            const date = this.dateTimeConvertService.dateDisplayFormat(updatedEntity.start_date);
            const startTime = this.dateTimeConvertService.getTimeFromUnixTimestamp(updatedEntity.start_date);
            const endTime = this.dateTimeConvertService.getTimeFromUnixTimestamp(updatedEntity.end_date);

            if (deviceTokens.length) {
                const payload = {
                    notification: {
                        title: 'Your shift schedule has been updated',
                        body: `Your shift timings will be from ${startTime} to ${endTime} on ${date}`,
                    },
                };
                await this.notificationService.sendNotification(deviceTokens, payload);
            }

            return updatedEntity;
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async publishWorkShift(workShiftDTO: WorkShiftDTO, publisher?: number) {
        const entity = WorkShiftMapper.fromDTOtoEntity(workShiftDTO);

        if (publisher && !entity.is_published) {
            entity.published_by = publisher;
            entity.is_published = true;
            entity.published_at = Date.now();
            entity.updated_at = Date.now();
        }
        const result = await this.workShiftRepository.save(entity);
        return WorkShiftMapper.fromEntityToDTO(result);
    }

    async deleteById(id: number, company_id: number): Promise<WorkShiftDTO | undefined> {
        try {
            const entityFind = await this.findById(id);
            if (!entityFind) {
                throw new HttpException('Work shift is not found!', HttpStatus.NOT_FOUND);
            }
            await this.workShiftRepository.delete(id);
            const afterDeletedEntity = await this.findById(id);
            if (afterDeletedEntity) {
                throw new HttpException('Work shift cannot be deleted!', HttpStatus.FOUND);
            }

            const deviceTokens = await this.notificationService.getDeviceTokens({
                where: {
                    company_id,
                    employee_id: entityFind.employees[0].id,
                },
            });

            const date = this.dateTimeConvertService.dateDisplayFormat(entityFind.start_date);

            if (deviceTokens.length) {
                const payload = {
                    notification: {
                        title: 'Your shift schedule has been deleted',
                        body: `Your scheduled shift on ${date} has been canceled. This decision has been taken due to unforeseen circumstances, and we apologize for any inconvenience caused.`,
                    },
                };
                await this.notificationService.sendNotification(deviceTokens, payload);
            }

            return entityFind;
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async deleteByDateRange(dateRange: DateRangeDTO, company_id: string): Promise<any | undefined> {
        const { start_date, end_date } = dateRange;
        const workShifts = await this.workShiftRepository
            .createQueryBuilder()
            .delete()
            .where('company_id = (:company_id)', { company_id })
            .andWhere('start_date > (:start_date)  AND start_date < (:end_date)', { start_date, end_date })
            .execute();
        return workShifts;
    }
}
