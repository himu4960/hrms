import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';

import { HolidayDTO } from '../service/dto/holiday.dto';
import { HolidayMapper } from '../service/mapper/holiday.mapper';
import { HolidayRepository } from '../repository/holiday.repository';

const relationshipNames = [];

@Injectable()
export class HolidayService {
    logger = new Logger('HolidayService');

    constructor(@InjectRepository(HolidayRepository) private holidayRepository: HolidayRepository) {}

    async findById(id: number): Promise<HolidayDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.holidayRepository.findOne(id, options);
        return HolidayMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<HolidayDTO>): Promise<HolidayDTO | undefined> {
        const result = await this.holidayRepository.findOne(options);
        return HolidayMapper.fromEntityToDTO(result);
    }

    async findAndCount(options: FindManyOptions<HolidayDTO>): Promise<[HolidayDTO[], number]> {
        options.relations = relationshipNames;
        const resultList = await this.holidayRepository.findAndCount(options);
        const holidayDTO: HolidayDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach(holiday => holidayDTO.push(HolidayMapper.fromEntityToDTO(holiday)));
            resultList[0] = holidayDTO;
        }
        return resultList;
    }

    async save(holidayList: HolidayDTO[], creator?: number): Promise<HolidayDTO[] | undefined> {
        const options = {
            relations: relationshipNames,
        };

        holidayList.map(holiday => {
            const entity = HolidayMapper.fromDTOtoEntity(holiday);
            if (creator) {
                if (!entity.created_by) {
                    entity.created_by = creator;
                }
                entity.updated_by = creator;
            }
            this.holidayRepository.save(entity);
        });

        const [resultList, count] = await this.findAndCount(options);
        return resultList;
    }

    async update(holidayDTO: HolidayDTO, updater?: number): Promise<HolidayDTO | undefined> {
        const entity = HolidayMapper.fromDTOtoEntity(holidayDTO);
        if (updater) {
            entity.updated_by = updater;
        }
        const result = await this.holidayRepository.save(entity);
        return HolidayMapper.fromEntityToDTO(result);
    }

    async deleteById(id: number): Promise<HolidayDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('entity is not found!', HttpStatus.NOT_FOUND);
        }

        await this.holidayRepository.delete(id);

        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }
}
