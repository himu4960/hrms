import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { TimeZoneDTO } from '../service/dto/time-zone.dto';

import { TimeZoneRepository } from '../repository/time-zone.repository';

@Injectable()
export class TimeZoneService {
    logger = new Logger('TimeZoneService');

    constructor(@InjectRepository(TimeZoneRepository) private timeZoneRepository: TimeZoneRepository) {}

    async getAll(): Promise<TimeZoneDTO[]> {
        return await this.timeZoneRepository.find({
            select: ['id', 'name', 'abbr', 'offset', 'description', 'is_active', 'is_deleted', 'is_default'],
        });
    }

    async getTimeZoneById(id: number | string): Promise<TimeZoneDTO> {
        return await this.timeZoneRepository.findOne(id);
    }

    async getActiveTimeZones(): Promise<TimeZoneDTO[]> {
        return await this.timeZoneRepository.find({
            select: ['id', 'name', 'abbr', 'offset', 'description', 'is_default'],
            where: {
                is_active: true,
                is_deleted: false,
            },
        });
    }
}
