import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';

import { ETimeClockEventType } from './../domain/enum/time-clock.enum';

import { TimeClockEventDTO } from './dto/time-clock/time-clock-event.dto';
import { TimeClockEventBreakDTO } from './dto/time-clock/time-clock-event-break.dto';
import { TimeClockEventNoteDTO } from './dto/time-clock/time-clock-event-note.dto';
import { TimeClockEventPositionDTO } from './dto/time-clock/time-clock-event-position.dto';
import { TimeClockEventPositionUpdateDTO } from './dto/time-clock/time-clock-event-position-update.dto';

import { TimeClockEventMapper } from './mapper/time-clock-event.mapper';

import { TimeClockEventRepository } from './../repository/time-clock-event.repository';

import { DesignationService } from './designation.service';
import { DateTimeConvertService } from '../shared/convert/date-time-convert.service';
import { TimeClockService } from './time-clock.service';
import { BaseDTO } from './dto/base.dto';

const relationshipNames = ['time_clock', 'prev_designation', 'new_designation'];

@Injectable()
export class TimeClockEventsService {
    logger = new Logger('TimeClockEventsService');

    constructor(
        @InjectRepository(TimeClockEventRepository) private timeClockEventRepository: TimeClockEventRepository,
        private timeClockService: TimeClockService,
        private designationService: DesignationService,
        private dateTimeConvertService: DateTimeConvertService,
    ) {}

    async findById(id: number): Promise<TimeClockEventDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.timeClockEventRepository.findOne(id, options);
        return TimeClockEventMapper.fromEntityToDTO(result);
    }

    async findByFields(options: FindOneOptions<TimeClockEventDTO>): Promise<TimeClockEventDTO | undefined> {
        const result = await this.timeClockEventRepository.findOne(options);
        return TimeClockEventMapper.fromEntityToDTO(result);
    }

    async findAndCount(
        options: FindManyOptions<TimeClockEventDTO>,
        company_id: number,
    ): Promise<[TimeClockEventDTO[], number]> {
        options.relations = relationshipNames;
        options.where = { company_id };
        return await this.timeClockEventRepository.findAndCount(options);
    }

    async isBreakTimeOut(time_clock_id: number): Promise<boolean> {
        const existingBreakTimeEvent = await this.timeClockEventRepository.findOne({
            where: { time_clock_id, event_type: ETimeClockEventType.BREAK },
            order: {
                id: 'DESC',
            },
        });

        if (existingBreakTimeEvent && !existingBreakTimeEvent?.break_end_time) {
            return false;
        }

        return true;
    }

    async addNoteEvent(
        timeClockEventDTO: TimeClockEventNoteDTO,
        company_id: number,
    ): Promise<TimeClockEventDTO | undefined> {
        const { note, time_clock_id } = timeClockEventDTO;
        const existingTimeClock = await this.timeClockService.findById(time_clock_id);

        if (!existingTimeClock) {
            throw new HttpException('Time Clock not found!', HttpStatus.NOT_FOUND);
        }

        if (!(await this.isBreakTimeOut(time_clock_id))) {
            throw new HttpException('Please break time out!', HttpStatus.NOT_FOUND);
        }

        if (!note) {
            throw new HttpException('Please add notes!', HttpStatus.NOT_FOUND);
        }
        const result = await this.timeClockEventRepository.save({
            ...timeClockEventDTO,
            event_type: ETimeClockEventType.NOTE,
            company_id,
            created_at: Date.now(),
        });

        return TimeClockEventMapper.fromEntityToDTO(result);
    }

    async updateNoteEvent(timeClockEventDTO: TimeClockEventNoteDTO): Promise<TimeClockEventDTO | undefined> {
        const { id, note } = timeClockEventDTO;

        if (!note) {
            throw new HttpException('Please add notes!', HttpStatus.NOT_FOUND);
        }
        await this.timeClockEventRepository.update(
            { id },
            {
                ...timeClockEventDTO,
                updated_at: Date.now(),
            },
        );

        return await this.findById(id);
    }

    async addPositionEvent(
        timeClockEventDTO: TimeClockEventPositionDTO,
        company_id: number,
    ): Promise<TimeClockEventDTO | undefined> {
        const { time_clock_id, new_position_id } = timeClockEventDTO;
        const existingPosition = await this.designationService.findById(new_position_id);

        if (!existingPosition) {
            throw new HttpException('Position not found!', HttpStatus.NOT_FOUND);
        }

        if (!(await this.isBreakTimeOut(time_clock_id))) {
            throw new HttpException('Please break time out!', HttpStatus.NOT_FOUND);
        }

        if (!new_position_id) {
            throw new HttpException('Please add position!', HttpStatus.NOT_FOUND);
        }

        const previousTimeClockEvent = await this.timeClockEventRepository.findOne({
            where: { time_clock_id, event_type: ETimeClockEventType.POSITION },
            order: { id: 'DESC' },
        });

        const prev_position_id = previousTimeClockEvent?.new_position_id ?? null;

        const result = await this.timeClockEventRepository.save({
            ...timeClockEventDTO,
            event_type: ETimeClockEventType.POSITION,
            prev_position_id,
            company_id,
            created_at: Date.now(),
        });

        return TimeClockEventMapper.fromEntityToDTO(result);
    }

    async updatePositionEvent(
        timeClockEventDTO: TimeClockEventPositionUpdateDTO,
    ): Promise<TimeClockEventDTO | undefined> {
        const { id, new_position_id } = timeClockEventDTO;
        const existingPosition = await this.designationService.findById(new_position_id);
        if (!existingPosition) {
            throw new HttpException('Designation not found!', HttpStatus.NOT_FOUND);
        }
        try {
            await this.timeClockEventRepository.update(
                { id },
                {
                    ...timeClockEventDTO,
                    updated_at: Date.now(),
                },
            );
            return await this.findById(id);
        } catch (error) {
            throw new HttpException('Cannot update position!', HttpStatus.FORBIDDEN);
        }
    }

    async startBreakTimeEvent(
        timeClockEventDTO: TimeClockEventBreakDTO,
        company_id: number,
    ): Promise<TimeClockEventDTO | undefined> {
        const { time_clock_id } = timeClockEventDTO;
        const existingTimeClock = await this.timeClockService.findById(time_clock_id);

        if (!(await this.isBreakTimeOut(time_clock_id))) {
            throw new HttpException('Please break time out!', HttpStatus.NOT_FOUND);
        }

        if (!existingTimeClock) {
            throw new HttpException('Time Clock not found!', HttpStatus.NOT_FOUND);
        }
        const result = await this.timeClockEventRepository.save({
            ...timeClockEventDTO,
            break_start_time: Date.now(),
            event_type: ETimeClockEventType.BREAK,
            // Created_at default value is fixed in base.entity so we need to store current time in every post method.
            created_at: Date.now(),
            company_id,
        });
        return TimeClockEventMapper.fromEntityToDTO(result);
    }

    async endBreakTimeEvent(timeClockEventDTO: BaseDTO): Promise<TimeClockEventDTO | undefined> {
        try {
            const { id } = timeClockEventDTO;
            const break_end_time = Date.now();
            const existingTimeClockEvent = await this.findById(id);
            const total_break_duration = this.dateTimeConvertService.getDateDifferenceInSeconds(
                break_end_time,
                existingTimeClockEvent.break_start_time,
            );
            await this.timeClockEventRepository.update(
                { id },
                { total_break_duration, break_end_time, updated_at: Date.now() },
            );
            const result: TimeClockEventDTO = await this.findById(id);
            // BigInt type was returning values as string
            result.break_start_time = Number(result.break_start_time);
            result.break_end_time = Number(result.break_end_time);
            return result;
        } catch (error) {
            throw new HttpException('Cannot update end break time!', HttpStatus.FORBIDDEN);
        }
    }

    async deleteById(id: number): Promise<TimeClockEventDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('Event is not found!', HttpStatus.NOT_FOUND);
        }
        await this.timeClockEventRepository.delete(id);
        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Event cannot be deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }
}
