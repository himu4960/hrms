import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { SettingsTimeClockDTO } from './dto/company-settings/settings-time-clock.dto';
import { SettingsTimeClockUpdateDTO } from './dto/company-settings/settings-time-clock-update.dto';
import { SettingsApplicationDTO } from './dto/company-settings/settings-application.dto';
import { SettingsApplicationUpdateDTO } from './dto/company-settings/settings-application-update.dto';
import { SettingsShiftPlanningUpdateDTO } from './dto/company-settings/settings-shift-planning-update.dto';
import { SettingsCreateDTO } from './dto/company-settings/settings-create.dto';
import { SettingsShiftPlanningDTO } from './dto/company-settings/settings-shift-planning.dto';
import { SettingsLeaveAvailabilityDTO } from './dto/company-settings/settings-leave-availability.dto';
import { SettingsResponseDTO } from './dto/company-settings/settings-response.dto';

import { SettingsTimeClockRepository } from '../repository/settings-time-clock.repository';
import { SettingsApplicationRepository } from './../repository/settings-application.repository';
import { SettingsShiftPlanningRepository } from './../repository/settings-shift-planning.repository';
import { SettingsLeaveAvailabilityRepository } from './../repository/settings-leave-availability.repository';

@Injectable()
export class CompanySettingsService {
    logger = new Logger('CompanySettingsService');

    constructor(
        @InjectRepository(SettingsTimeClockRepository)
        private settingsTimeClockRepository: SettingsTimeClockRepository,
        @InjectRepository(SettingsApplicationRepository)
        private settingsApplicationRepository: SettingsApplicationRepository,
        @InjectRepository(SettingsShiftPlanningRepository)
        private settingsShiftPlanningRepository: SettingsShiftPlanningRepository,
        @InjectRepository(SettingsLeaveAvailabilityRepository)
        private settingsLeaveAvailabilityRepository: SettingsLeaveAvailabilityRepository,
    ) {}

    async save(settingsCreateDTO: SettingsCreateDTO): Promise<SettingsResponseDTO | undefined> {
        const { company_id } = settingsCreateDTO;
        const settings_application = await this.settingsApplicationRepository.save({
            ...settingsCreateDTO,
            // Created_at default value is fixed in base.entity so we need to store current time in every post method.
            created_at: Date.now(),
        });
        const settings_shift_planning = await this.settingsShiftPlanningRepository.save({
            company_id,
            // Created_at default value is fixed in base.entity so we need to store current time in every post method.
            created_at: Date.now(),
        });
        const settings_time_clock = await this.settingsTimeClockRepository.save({
            company_id,
            // Created_at default value is fixed in base.entity so we need to store current time in every post method.
            created_at: Date.now(),
        });
        const settings_leave_availability = await this.settingsLeaveAvailabilityRepository.save({
            company_id,
            // Created_at default value is fixed in base.entity so we need to store current time in every post method.
            created_at: Date.now(),
        });
        return {
            settings_application,
            settings_shift_planning,
            settings_time_clock,
            settings_leave_availability,
        };
    }

    async getApplicationByCompanyId(companyId: number): Promise<SettingsApplicationDTO | undefined> {
        return await this.settingsApplicationRepository.findOne({ where: { company_id: companyId } });
    }

    async updateApplicationByCompanyId(
        applicationDTO: SettingsApplicationUpdateDTO,
        company_id: number,
        updater?: number,
    ): Promise<SettingsApplicationUpdateDTO | undefined> {
        const existingApplication = await this.settingsApplicationRepository.findOne({ where: { company_id } });
        if (!existingApplication) {
            throw new HttpException('Application Settings not found!', HttpStatus.NOT_FOUND);
        }

        const updatedResult = await this.settingsApplicationRepository.update(
            { company_id: company_id },
            { ...applicationDTO, updated_by: updater, updated_at: Date.now() },
        );
        if (updatedResult.raw.affectedRows > 0) {
            return await this.settingsApplicationRepository
                .createQueryBuilder('applicationSettings')
                .leftJoinAndSelect('applicationSettings.time_zone', 'time_zone')
                .select(['applicationSettings', 'time_zone'])
                .where('applicationSettings.company_id = :company_id', { company_id })
                .getOne();
        }
        throw new HttpException('Application Settings cannot be updated!', HttpStatus.NOT_MODIFIED);
    }

    async getShiftPlanningByCompanyId(companyId: number): Promise<SettingsShiftPlanningDTO | undefined> {
        return await this.settingsShiftPlanningRepository.findOne({ where: { company_id: companyId } });
    }

    async updateShiftPlanningByCompanyId(
        shiftPlanningDTO: SettingsShiftPlanningUpdateDTO,
        company_id: number,
        updater?: number,
    ): Promise<SettingsShiftPlanningUpdateDTO | undefined> {
        const existingShiftPlanning = await this.settingsShiftPlanningRepository.findOne({ where: { company_id } });
        if (!existingShiftPlanning) {
            throw new HttpException('Shift Planning Settings not found!', HttpStatus.NOT_FOUND);
        }

        const updatedResult = await this.settingsShiftPlanningRepository.update(
            { company_id: company_id },
            { ...shiftPlanningDTO, updated_by: updater, updated_at: Date.now() },
        );

        if (updatedResult.raw.affectedRows > 0) {
            return await this.settingsShiftPlanningRepository.findOne({ where: { company_id } });
        }

        throw new HttpException('Shift Planning Settings cannot be updated!', HttpStatus.NOT_MODIFIED);
    }

    async getTimeClockByCompanyId(companyId: number): Promise<SettingsTimeClockDTO | undefined> {
        return await this.settingsTimeClockRepository.findOne({ where: { company_id: companyId } });
    }

    async updateTimeClockByCompanyId(
        timeClockDTO: SettingsTimeClockUpdateDTO,
        company_id: number,
        updater?: number,
    ): Promise<SettingsTimeClockUpdateDTO | undefined> {
        const existingTimeClock = await this.settingsTimeClockRepository.findOne({ where: { company_id } });
        if (!existingTimeClock) {
            throw new HttpException('Time clock settings not found!', HttpStatus.NOT_FOUND);
        }

        const updatedResult = await this.settingsTimeClockRepository.update(
            { company_id: company_id },
            { ...timeClockDTO, updated_by: updater, updated_at: Date.now() },
        );
        if (updatedResult.raw.affectedRows > 0) {
            return await this.settingsTimeClockRepository.findOne({ where: { company_id } });
        }
        throw new HttpException('Time clock settings cannot be updated!', HttpStatus.NOT_MODIFIED);
    }

    async getLeaveAvailabilityByCompanyId(companyId: number): Promise<SettingsLeaveAvailabilityDTO | undefined> {
        return await this.settingsLeaveAvailabilityRepository.findOne({ where: { company_id: companyId } });
    }

    async updateLeaveAvailabilityByCompanyId(
        leaveAvailabilityDTO: SettingsLeaveAvailabilityDTO,
        company_id: number,
        updater?: number,
    ): Promise<SettingsLeaveAvailabilityDTO | undefined> {
        const existingLeaveAvailability = await this.settingsLeaveAvailabilityRepository.findOne({
            where: { company_id },
        });
        if (!existingLeaveAvailability) {
            throw new HttpException('Leave Availability settings not found!', HttpStatus.NOT_FOUND);
        }

        const updatedResult = await this.settingsLeaveAvailabilityRepository.update(
            { company_id: company_id },
            { ...leaveAvailabilityDTO, updated_by: updater, updated_at: Date.now() },
        );
        if (updatedResult.raw.affectedRows > 0) {
            return await this.settingsLeaveAvailabilityRepository.findOne({ where: { company_id } });
        }
        throw new HttpException('Leave Availability settings cannot be updated!', HttpStatus.NOT_MODIFIED);
    }
}
