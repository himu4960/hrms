import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';

import { BreakRuleDTO } from './dto/break-rule/break-rule.dto';
import { BreakRuleUpdateDTO } from './dto/break-rule/break-rule-update.dto';
import { BreakRuleConditionDTO } from './dto/break-rule/break-rule-condition.dto';
import { BreakRuleConditionCreateDTO } from './dto/break-rule/break-rule-condition-create.dto';
import { BreakRuleConditionUpdateDTO } from './dto/break-rule/break-rule-condition-update.dto';
import { BreakRuleCreateDTO } from './dto/break-rule/break-rule-create.dto';

import { BreakRuleMapper } from '../service/mapper/break-rule.mapper';
import { BreakRuleConditionMapper } from './mapper/break-rule-condition.mapper';

import { BreakRuleRepository } from '../repository/break-rule.repository';
import { BreakRuleConditionRepository } from './../repository/break-rule-condition.repository';

const relationshipNames = ['break_rule_conditions', 'company'];

@Injectable()
export class BreakRuleService {
    logger = new Logger('BreakRuleService');

    constructor(
        @InjectRepository(BreakRuleRepository) private breakRuleRepository: BreakRuleRepository,
        @InjectRepository(BreakRuleConditionRepository)
        private breakRuleConditionRepository: BreakRuleConditionRepository,
    ) {}

    async breakRuleFindById(id: number): Promise<BreakRuleDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.breakRuleRepository.findOne(id, options);
        return BreakRuleMapper.fromEntityToDTO(result);
    }

    async breakRuleFindByFields(options: FindOneOptions<BreakRuleDTO>): Promise<BreakRuleDTO | undefined> {
        const result = await this.breakRuleRepository.findOne(options);
        return BreakRuleMapper.fromEntityToDTO(result);
    }

    async breakRuleFindAndCount(
        options: FindManyOptions<BreakRuleDTO>,
        company_id: number,
    ): Promise<[BreakRuleDTO[], number]> {
        options.relations = relationshipNames;
        options.where = { company_id };
        return await this.breakRuleRepository.findAndCount(options);
    }

    async breakRuleSave(
        breakRuleDTO: BreakRuleCreateDTO,
        company_id: number,
        creator?: number,
    ): Promise<BreakRuleDTO | undefined> {
        const result = await this.breakRuleRepository.save({ ...breakRuleDTO, company_id, created_by: creator });
        const break_rule_id = result.id;
        breakRuleDTO?.break_rule_conditions?.map(async (createBreakRuleCondition: BreakRuleConditionCreateDTO) => {
            await this.breakRuleConditionSave({
                ...createBreakRuleCondition,
                break_rule_id,
                // Created_at default value is fixed in base.entity so we need to store current time in every post method.
                created_at: Date.now(),
            });
        });
        return BreakRuleMapper.fromEntityToDTO(result);
    }

    async breakRuleUpdate(
        break_rule_id: number,
        breakRuleDTO: BreakRuleUpdateDTO,
        updater?: number,
    ): Promise<BreakRuleDTO | undefined> {
        const { name, rule_description } = breakRuleDTO;

        const existingBreakRule = await this.breakRuleFindById(break_rule_id);
        if (!existingBreakRule) {
            throw new HttpException('Error, Break rule not found!', HttpStatus.NOT_FOUND);
        }

        await this.breakRuleRepository.update(
            { id: break_rule_id },
            { name: breakRuleDTO.name, rule_description: breakRuleDTO.rule_description, updated_by: updater },
        );

        breakRuleDTO?.break_rule_conditions?.map(async (updateBreakRuleCondition: BreakRuleConditionUpdateDTO) => {
            await this.breakRuleConditionUpdate({ ...updateBreakRuleCondition, break_rule_id });
        });

        return await this.breakRuleFindById(break_rule_id);
    }

    async breakRuleDeleteById(id: number): Promise<BreakRuleDTO | undefined> {
        const entityFind = await this.breakRuleFindById(id);
        if (!entityFind) {
            throw new HttpException('Break rule not found', HttpStatus.NOT_FOUND);
        }
        await this.breakRuleRepository.delete(id);
        const afterDeletedEntity = await this.breakRuleFindById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }

    async breakRuleConditionFindById(id: number): Promise<BreakRuleConditionDTO | undefined> {
        const result = await this.breakRuleConditionRepository.findOne(id);
        return BreakRuleConditionMapper.fromEntityToDTO(result);
    }

    async breakRuleConditionFindByFields(
        options: FindOneOptions<BreakRuleConditionDTO>,
    ): Promise<BreakRuleConditionDTO | undefined> {
        const result = await this.breakRuleConditionRepository.findOne(options);
        return BreakRuleConditionMapper.fromEntityToDTO(result);
    }

    async breakRuleConditionFindAndCount(options: FindManyOptions<BreakRuleConditionDTO>): Promise<[any, number]> {
        return await this.breakRuleConditionRepository.findAndCount(options);
    }

    async breakRuleConditionSave(
        breakRuleConditionDTO: BreakRuleConditionCreateDTO,
        creator?: number,
    ): Promise<BreakRuleConditionCreateDTO | undefined> {
        return await this.breakRuleConditionRepository.save({
            ...breakRuleConditionDTO,
            created_by: creator,
            // Created_at default value is fixed in base.entity so we need to store current time in every post method.
            created_at: Date.now(),
        });
    }

    async breakRuleConditionUpdate(
        breakRuleConditionDTO: BreakRuleConditionUpdateDTO,
        updater?: number,
    ): Promise<void> {
        const existingBreakRuleCondition = await this.breakRuleConditionFindById(breakRuleConditionDTO.id);

        if (existingBreakRuleCondition) {
            await this.breakRuleConditionRepository.update(
                { id: breakRuleConditionDTO.id },
                { ...breakRuleConditionDTO, updated_by: updater, updated_at: Date.now() },
            );
            await this.breakRuleConditionFindById(breakRuleConditionDTO.id);
        }
        await this.breakRuleConditionSave(breakRuleConditionDTO);
    }

    async breakRuleConditionDeleteById(id: number): Promise<void | undefined> {
        await this.breakRuleConditionRepository.delete(id);
        const entityFind = await this.breakRuleConditionFindById(id);
        if (entityFind) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
        }
        return;
    }
}
