import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In } from 'typeorm';

import { MaritalStatusDTO } from './dto/marital-status/marital-status.dto';
import { MaritalStatusCreateDTO } from './dto/marital-status/marital-status-create.dto';
import { MaritalStatusUpdateDTO } from './dto/marital-status/marital-status-update.dto';

import { MaritalStatus } from './../domain/marital-status.entity';
import { MaritalStatusTranslation } from './../domain/marital-status-translation.entity';

import { MaritalStatusMapper } from '../service/mapper/marital-status.mapper';

import { LanguageRepository } from './../repository/language.repository';
import { MaritalStatusRepository } from '../repository/marital-status.repository';
import { MaritalStatusTranslationRepository } from './../repository/marital-status-translation.repository';

const relationshipNames = [];

@Injectable()
export class MaritalStatusService {
    logger = new Logger('MaritalStatusService');

    constructor(
        @InjectRepository(MaritalStatusRepository) private maritalStatusRepository: MaritalStatusRepository,
        @InjectRepository(MaritalStatusTranslation)
        private maritalStatusTranslationRepository: MaritalStatusTranslationRepository,
        @InjectRepository(LanguageRepository)
        private languageRepository: LanguageRepository,
    ) { }

    async findAll() {
        return await this.maritalStatusRepository
            .createQueryBuilder('maritalStatus')
            .leftJoinAndMapMany(
                'maritalStatus.translations',
                MaritalStatusTranslation,
                'translation',
                'maritalStatus.id = translation.marital_status_id',
            )
            .select(['maritalStatus.id', 'translation.name', 'translation.language_code'])
            .getMany();
    }

    async findById(id: number): Promise<MaritalStatusDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.maritalStatusRepository.findOne(id, options);
        return MaritalStatusMapper.fromEntityToDTO(result);
    }

    async findByLangCode(langCode: string) {
        return await this.maritalStatusRepository
            .createQueryBuilder('maritalStatus')
            .leftJoinAndSelect('maritalStatus.translations', 'translation')
            .where('translation.language_code=:language_code', { language_code: langCode })
            .select(['maritalStatus.id as id', 'translation.name as name'])
            .getRawMany();
    }

    async save(createMaritalStatus: MaritalStatusCreateDTO, creator?: number) {
        const language_codes = createMaritalStatus.translations.map(maritalStatus => maritalStatus.language_code);

        const languageResult = await this.languageRepository.find({ where: { code: In(language_codes) } });

        if (languageResult.length > 0) {
            const result = await this.maritalStatusRepository
                .createQueryBuilder()
                .insert()
                .into(MaritalStatus)
                .values({})
                .execute();

            const marital_status_id = result.raw.insertId;

            createMaritalStatus.translations.forEach(async maritalStatusTranslation => {
                const entity = MaritalStatusMapper.fromDTOtoEntity(maritalStatusTranslation);
                if (creator) {
                    if (!entity.created_by) {
                        entity.created_by = creator;
                    }
                    entity.updated_by = creator;
                }

                const { name, language_code } = maritalStatusTranslation;

                const languageResult = await this.languageRepository.findOne(language_code);

                if (languageResult) {
                    const maritalStatusTranlationResult = await this.maritalStatusTranslationRepository.findOne({
                        where: { name },
                    });
                    if (!maritalStatusTranlationResult) {
                        await this.maritalStatusTranslationRepository
                            .createQueryBuilder()
                            .insert()
                            .into(MaritalStatusTranslation)
                            .values({ name, language_code, marital_status_id })
                            .execute();
                    } else {
                        throw new HttpException('Marital status already exsist', HttpStatus.FOUND);
                    }
                } else {
                    throw new HttpException('Language not found!', HttpStatus.NOT_FOUND);
                }
            });
        } else {
            throw new HttpException('Language not found!', HttpStatus.NOT_FOUND);
        }
    }

    async update(id: number, updateMaritalStatus: MaritalStatusUpdateDTO, updater?: number) {
        updateMaritalStatus.translations.forEach(async maritalStatusTranslation => {
            const entity = MaritalStatusMapper.fromDTOtoEntity(maritalStatusTranslation);
            if (updater) {
                entity.updated_by = updater;
                entity.updated_at = Date.now();
            }
            const { name, language_code } = maritalStatusTranslation;

            const languageResult = await this.languageRepository.findOne({ where: { code: language_code } });

            if (languageResult) {
                const isExist = await this.maritalStatusTranslationRepository
                    .createQueryBuilder('maritalStatusTranslation')
                    .where('marital_status_id=:id', { id })
                    .andWhere('language_code=:language_code', { language_code })
                    .getOne();
                if (isExist) {
                    await this.maritalStatusTranslationRepository
                        .createQueryBuilder()
                        .update(MaritalStatusTranslation)
                        .set({
                            name,
                        })
                        .where('marital_status_id=:id', { id })
                        .andWhere('language_code=:language_code', { language_code })
                        .execute();
                } else {
                    await this.maritalStatusTranslationRepository
                        .createQueryBuilder()
                        .insert()
                        .into(MaritalStatusTranslation)
                        .values([
                            {
                                name,
                                language_code,
                                marital_status_id: id,
                            },
                        ])
                        .execute();
                }
            } else {
                throw new HttpException('Language not found!', HttpStatus.NOT_FOUND);
            }
        });
        return;
    }

    async deleteById(id: number): Promise<MaritalStatusDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('Marital Status not found!', HttpStatus.NOT_FOUND);
        }
        await this.maritalStatusRepository.delete(id);
        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }
}
