import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';

import { TimeClockMapper } from './mapper/time-clock.mapper';
import { GeoLocation } from '../domain/geo-location.entity';
import { GeoLocationRepository } from '../repository/geo-location.repository';
import { DateTimeConvertService } from '../shared/convert/date-time-convert.service';

@Injectable()
export class GeoLocationService {
    logger = new Logger('GeoLocationService');

    constructor(
        @InjectRepository(GeoLocationRepository) private geoLocationRepository: GeoLocationRepository,
        private readonly httpService: HttpService,
        private dateTimeConvertService: DateTimeConvertService,
        private config: ConfigService
    ) { }

    async getGeoLocation(ip: string): Promise<GeoLocation> {
        let geoLocation = await this.geoLocationRepository.findOne({ where: { ip: ip } });
        if (geoLocation) {
            const daysDiff = this.dateTimeConvertService.getDateDifferenceInDays(new Date(), new Date(geoLocation?.updated_at));
            if (daysDiff >= 30) {
                const newGeoLocation = await this.fetchGeoLocation(ip);
                await this.geoLocationRepository.update(geoLocation.id, newGeoLocation);
                return await this.geoLocationRepository.findOne({ where: { ip: ip } });
            }
            return geoLocation;
        }
        if (!geoLocation) {
            const newGeoLocation = await this.fetchGeoLocation(ip);
            return await this.geoLocationRepository.save(newGeoLocation);
        }
    }

    async fetchGeoLocation(ip: string): Promise<GeoLocation> {
        const ipStackUrl = `http://api.ipstack.com/${ip}?access_key=${this.config.get('IP_STACK_ACCESS_KEY')}`;
        const geoLocation = await this.httpService.get(ipStackUrl).toPromise();
        const entity = TimeClockMapper.fromResponseToEntity(geoLocation.data);
        return entity;
    }
}
