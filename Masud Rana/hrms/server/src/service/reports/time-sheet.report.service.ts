import { Response } from 'express';
import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import PDFDocument from 'pdfkit-table';

import { ReportFilterDTO } from '../dto/reports/report.dto';

import { TimeClockRepository } from '../../repository/time-clock.repository';
import { SettingsTimeClockRepository } from './../../repository/settings-time-clock.repository';

import { DateTimeConvertService } from '../../shared/convert/date-time-convert.service';
import { ExportService } from './../../shared/export/export.service';

import { HELVETICA, HELVETICA_BOLD } from './../../shared/constant/font.constant';

import {
    ILocationHistoryExport,
    ITimeSheetAttendance,
    ITimeSheetAttendanceExport,
    ITimeSheetLateSummary,
    ITimeSheetLateSummaryeExport,
} from './../../web/rest/reports/time-clock/models/time-sheet-report.model';

import { SERVER_TIME_ZONE } from '../../shared/constant/date-format';

@Injectable()
export class TimeSheetReportService {
    logger = new Logger('TimeSheetReportService');

    constructor(
        @InjectRepository(TimeClockRepository) private timeClockRepository: TimeClockRepository,
        @InjectRepository(SettingsTimeClockRepository) private settingsTimeClockRepository: SettingsTimeClockRepository,
        private readonly dateTimeConvertService: DateTimeConvertService,
        private readonly exportService: ExportService,
    ) {}

    async getTimeSheet(filterDTO: ReportFilterDTO, company_id: string, time_zone: any): Promise<any | undefined> {
        const { employee_id, branch_id, designation_id } = filterDTO;
        const startDate = this.dateTimeConvertService.convertLocalDateToUTCDate(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.convertLocalDateToUTCDate(filterDTO.end_date);
        endDate.setDate(endDate.getDate() + 1);
        const startDateTimeStamp = startDate.getTime();
        const endDateTimeStamp = endDate.getTime();

        const totalDays = this.dateTimeConvertService.getDateDifferenceInDays(endDate, startDate);

        startDate.setDate(startDate.getDate() - 1);

        let caseStatement = '';
        let dateColumns = '';
        for (let i = 0; i < totalDays; i++) {
            startDate.setDate(startDate.getDate() + 1);
            const start = this.dateTimeConvertService.format(startDate);

            caseStatement += `, CASE WHEN tcs.start_date = '${start}' THEN JSON_OBJECT('start', tcs.start_time, 'end', tcs.end_time, 'diff', tcs.total_clock_duration) END AS '${start}'`;

            dateColumns +=
                ', IF(COUNT(pvt.`' +
                start +
                '`) = 0, JSON_ARRAY(), JSON_ARRAYAGG(pvt.`' +
                start +
                "`)) AS '" +
                start +
                "'";
        }

        let whereStatement = `WHERE 1 
                        AND clock_start_time BETWEEN '${startDateTimeStamp}' AND '${endDateTimeStamp}'
                        AND hrms_time_clocks.company_id = ${company_id}`;

        if (employee_id) {
            whereStatement += ` AND hrms_time_clocks.employee_id = ${employee_id}`;
        }
        if (branch_id) {
            whereStatement += ` AND hrms_time_clocks.branch_id = ${branch_id}`;
        }
        if (designation_id) {
            whereStatement += ` AND hrms_time_clocks.designation_id = ${designation_id}`;
        }

        const selectStatement = `SELECT pvt.employee_id, 
                        CONCAT(hrms_employees.first_name, ' ', hrms_employees.last_name) AS 'employee_name',
                        SUM(pvt.total_clock_duration) AS total_clock_in
                        ${dateColumns}
                FROM (
                    SELECT tcs.employee_id,
                        tcs.total_clock_duration,
                        tcs.start_date AS start_date
                        ${caseStatement}
                FROM (
                    SELECT employee_id,
                        TIME_FORMAT(CONVERT_TZ(FROM_UNIXTIME(clock_start_time / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%h: %i %p") AS start_time,
                        DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(clock_start_time / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%Y-%m-%d") AS start_date,
                        TIME_FORMAT(CONVERT_TZ(FROM_UNIXTIME(clock_end_time / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%h: %i %p") AS end_time,
                        DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(clock_end_time / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%Y-%m-%d") AS end_date,
                        total_clock_duration
                    FROM hrms_time_clocks
                 ${whereStatement}
                 ) AS tcs
                   ORDER BY employee_id, start_date
                ) AS pvt
                INNER JOIN hrms_employees ON hrms_employees.id = pvt.employee_id`;

        const groupByStatement = `GROUP BY pvt.employee_id`;

        const sqlStatement = `${selectStatement}  ${groupByStatement}`;

        return await this.timeClockRepository.query(sqlStatement);
    }

    async getTimeSheetLateSummary(
        filterDTO: ReportFilterDTO,
        company_id: string,
        time_zone: any,
    ): Promise<ITimeSheetLateSummary[] | undefined> {
        const { employee_id, branch_id, designation_id } = filterDTO;
        const startDate = this.dateTimeConvertService.convertLocalDateToUTCDate(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.convertLocalDateToUTCDate(filterDTO.end_date);
        endDate.setDate(endDate.getDate() + 1);
        const startDateTimeStamp = startDate.getTime();
        const endDateTimeStamp = endDate.getTime();

        const timeClockSettings = await this.settingsTimeClockRepository.findOne({
            where: {
                company_id,
            },
        });

        const lateCountableTime = timeClockSettings.late_countable_time_start;

        let whereStatement = `WHERE 1
                        AND work_shifts.company_id = ${company_id}
                        AND work_shifts.start_date BETWEEN '${startDateTimeStamp}' AND '${endDateTimeStamp}'`;

        if (employee_id) {
            whereStatement += ` AND employees.id = ${employee_id}`;
        }
        if (branch_id) {
            whereStatement += ` AND work_shifts.branch_id = ${branch_id}`;
        }
        if (designation_id) {
            whereStatement += ` AND work_shifts.designation_id = ${designation_id}`;
        }

        const conditionStatement = ` AND DATE_FORMAT(FROM_UNIXTIME(work_shifts.start_date / 1000), "%Y-%m-%d") = DATE_FORMAT(FROM_UNIXTIME(clock_start_time / 1000), "%Y-%m-%d") `;

        const sqlStatement = `SELECT employees.id AS 'employee_id',
               designations.name AS 'designation_name',
               employees.photo_url AS 'photo_url',
               CONCAT(employees.first_name, ' ', employees.last_name) AS 'employee_name',
               CONVERT_TZ(FROM_UNIXTIME(work_shifts.start_date / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}') AS date,
               TIME_FORMAT(CONVERT_TZ(FROM_UNIXTIME(work_shifts.start_date / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%h: %i %p") AS 'shift_start',
               IF(
                ISNULL(clock_start_time),
                '', 
                TIME_FORMAT(CONVERT_TZ(FROM_UNIXTIME(clock_start_time / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%h: %i %p")
               ) AS 'actual_start',
               TIME_FORMAT(CONVERT_TZ(FROM_UNIXTIME(work_shifts.end_date / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%h: %i %p") AS 'shift_end',
               IF(
                ISNULL(clock_end_time), 
                '', 
                TIME_FORMAT(CONVERT_TZ(FROM_UNIXTIME(clock_end_time / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%h: %i %p")
                ) AS 'actual_end',
               (
                   CASE
                       WHEN work_shifts.start_date >= (clock_start_time - ${lateCountableTime}) ${conditionStatement} THEN 'No'
                       WHEN work_shifts.start_date < clock_start_time AND work_shifts.end_date > clock_start_time ${conditionStatement} THEN 'Yes'
                       ELSE 'Absent'
                   END
               ) AS "late_status",
               IF(
                   work_shifts.start_date < (clock_start_time - ${lateCountableTime}) ${conditionStatement},
                   (((clock_start_time - ${lateCountableTime}) / 1000) - (work_shifts.start_date / 1000)),
                   ''
               ) AS 'late_by'
           FROM hrms_workshifts work_shifts
               LEFT JOIN hrms_time_clocks time_clocks ON time_clocks.work_shift_id = work_shifts.id
               INNER JOIN hrms_work_shift_employees work_shift_employees ON work_shift_employees.work_shift_id = work_shifts.id
               INNER JOIN hrms_designations designations ON designations.id = work_shifts.designation_id
               INNER JOIN hrms_employees employees ON employees.id = work_shift_employees.employee_id
               ${whereStatement}
           ORDER BY employees.id`;

        return await this.timeClockRepository.query(sqlStatement);
    }

    async getTimeSheetAttendance(
        filterDTO: ReportFilterDTO,
        company_id: string,
        time_zone: any,
    ): Promise<ITimeSheetAttendance[] | undefined> {
        const { employee_id, branch_id, designation_id } = filterDTO;

        const startDate = this.dateTimeConvertService.convertLocalDateToUTCDate(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.convertLocalDateToUTCDate(filterDTO.end_date);
        endDate.setDate(endDate.getDate() + 1);
        const startDateTimeStamp = startDate.getTime();
        const endDateTimeStamp = endDate.getTime();

        let whereStatement = `WHERE 1
                        AND work_shifts.company_id = ${company_id}
                        AND work_shifts.start_date BETWEEN '${startDateTimeStamp}' AND '${endDateTimeStamp}'`;

        if (employee_id) {
            whereStatement += ` AND employees.id = ${employee_id}`;
        }
        if (branch_id) {
            whereStatement += ` AND work_shifts.branch_id = ${branch_id}`;
        }
        if (designation_id) {
            whereStatement += ` AND work_shifts.designation_id = ${designation_id}`;
        }

        const isDateMatch = `DATE_FORMAT(FROM_UNIXTIME(work_shifts.start_date / 1000), "%Y-%m-%d") = DATE_FORMAT(FROM_UNIXTIME(time_clocks.clock_start_time / 1000), "%Y-%m-%d")`;

        const totalWorkShiftDuration = `SUM(work_shifts.total_work_shift_duration)`;
        const totalClockInDuration = `SUM(
            IF(
                time_clocks.clock_start_time < work_shifts.start_date,
                (time_clocks.clock_end_time - work_shifts.start_date) / 1000,
                time_clocks.total_clock_duration
                )
            )`;

        const isClockInlate = `work_shifts.start_date < time_clocks.clock_start_time`;
        const totalLate = `SUM(${isClockInlate} AND ${isDateMatch})`;

        const totalAbsent = `SUM(ISNULL(time_clocks.clock_start_time))`;

        const clockInPercentage = `(${totalClockInDuration} / ${totalWorkShiftDuration}) * 100`;

        const sqlStatement = `SELECT CONCAT(employees.first_name, ' ', employees.last_name) AS employee_name,
                COUNT(work_shifts.id) AS shifts,
                ${totalWorkShiftDuration} AS 'hours_scheduled',
                (CASE WHEN ISNULL(${totalClockInDuration}) THEN 0 ELSE (${totalClockInDuration}) END) AS hours_clocked,
                (CASE WHEN ISNULL(${totalLate}) THEN 0 ELSE ${totalLate} END) AS late_count,
                (CASE WHEN ISNULL(${totalAbsent}) THEN 0 ELSE ${totalAbsent} END) AS absent_count,
                (CASE WHEN ISNULL(${clockInPercentage}) THEN 0 ELSE ${clockInPercentage} END) AS 'clock_vs_shift'
                FROM hrms_workshifts work_shifts
                LEFT JOIN hrms_time_clocks time_clocks ON time_clocks.work_shift_id = work_shifts.id
                INNER JOIN hrms_work_shift_employees work_shift_employees ON work_shift_employees.work_shift_id = work_shifts.id
                INNER JOIN hrms_employees employees ON employees.id = work_shift_employees.employee_id
                ${whereStatement}
                GROUP BY work_shift_employees.employee_id`;

        return await this.timeClockRepository.query(sqlStatement);
    }

    async getLocationHistory(
        filterDTO: ReportFilterDTO,
        company_id: string,
        time_zone: any,
    ): Promise<any[] | undefined> {
        const { employee_id, branch_id, designation_id } = filterDTO;

        const startDate = this.dateTimeConvertService.convertLocalDateToUTCDate(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.convertLocalDateToUTCDate(filterDTO.end_date);
        endDate.setDate(endDate.getDate() + 1);
        const startDateTimeStamp = startDate.getTime();
        const endDateTimeStamp = endDate.getTime();

        let whereStatement = `WHERE 1 AND clock_start_time BETWEEN ${startDateTimeStamp} AND ${endDateTimeStamp} AND hrms_time_clocks.company_id = ${company_id}`;

        if (designation_id) {
            whereStatement += ` AND hrms_time_clocks.designation_id = ${designation_id}`;
        }

        if (employee_id) {
            whereStatement += ` AND hrms_time_clocks.employee_id = ${employee_id}`;
        }

        if (branch_id) {
            whereStatement += ` AND hrms_time_clocks.branch_id = ${branch_id}`;
        }

        const orderBy = `ORDER BY clock_start_time, employee_name`;

        const selectStatement = `SELECT
            DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(clock_start_time / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%Y-%m-%d") AS date,
            CONCAT(
                hrms_employees.first_name,
                ' ',
                hrms_employees.last_name
            ) AS employee_name,
            hrms_branches.name AS branch_name,
            IF(ISNULL(hrms_designations.name),'',hrms_designations.name) AS designation_name,
            IF(
                ISNULL(clock_start_time), 
                '', 
                TIME_FORMAT(CONVERT_TZ(FROM_UNIXTIME(clock_start_time / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%h: %i %p")
            ) AS clock_start_time,
            IF(
                ISNULL(clock_end_time), 
                '', 
                TIME_FORMAT(CONVERT_TZ(FROM_UNIXTIME(clock_end_time / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%h: %i %p")
            ) AS clock_end_time,
            total_clock_duration,
            total_break_duration,
            IF(ISNULL(clockin_latitude), '', clockin_latitude) AS clockin_latitude,
            IF(ISNULL(clockin_longitude), '', clockin_longitude) AS clockin_longitude,
            IF(ISNULL(clockout_latitude), '', clockout_latitude) AS clockout_latitude,
            IF(ISNULL(clockout_longitude), '', clockout_longitude) AS clockout_longitude,
            IF(ISNULL(clockin_location), '', clockin_location) AS clockin_location,
            IF(ISNULL(clockout_location), '', clockout_location) AS clockout_location
            FROM
            hrms_time_clocks
            INNER JOIN hrms_employees ON hrms_employees.id = hrms_time_clocks.employee_id
            LEFT JOIN hrms_branches ON hrms_branches.id = hrms_time_clocks.branch_id
            LEFT JOIN hrms_designations ON hrms_designations.id = hrms_time_clocks.designation_id`;

        const sql = `${selectStatement} ${whereStatement} ${orderBy}`;
        return await this.timeClockRepository.query(sql);
    }

    async exportPdfTimeSheetAttendance(filterDTO: ReportFilterDTO, company_id: string, time_zone: any) {
        const timeSheetAttendances: ITimeSheetAttendance[] = await this.getTimeSheetAttendance(
            filterDTO,
            company_id,
            time_zone,
        );

        const startDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.end_date);

        let row: ITimeSheetAttendanceExport = {};
        const tableDataArray = [];

        timeSheetAttendances.forEach(timeSheetAttendance => {
            row = timeSheetAttendance;
            tableDataArray.push(row);
        });
        let doc = new PDFDocument({ margin: 20, size: 'A4' });

        const table = {
            headers: [
                { label: 'Employee', property: 'employee_name', width: 100, renderer: null },
                { label: 'Shifts', property: 'shifts', width: 60, renderer: null },
                {
                    label: 'Hours Scheduled',
                    property: 'hours_scheduled',
                    width: 100,
                    renderer: (value, indexColumn, indexRow, row) => {
                        return `${Number(value / 3600).toFixed(2)}`;
                    },
                },
                {
                    label: 'Hours Clocked',
                    property: 'hours_clocked',
                    width: 100,
                    renderer: (value, indexColumn, indexRow, row) => {
                        return `${Number(value / 3600).toFixed(2)}`;
                    },
                },
                { label: 'Late', property: 'late_count', width: 60, renderer: null },
                { label: 'Absent', property: 'absent_count', width: 60, renderer: null },
                {
                    label: 'Clock VS Shift',
                    property: 'clock_vs_shift',
                    width: 80,
                    renderer: (value, indexColumn, indexRow, row) => {
                        return `${Number(value).toFixed(0)} %`;
                    },
                },
            ],
            datas: tableDataArray,
        };

        doc.text(`Time Sheet Attendance Statistics Between:  ${startDate}  -  ${endDate}`);

        doc.moveDown(2);

        doc.table(table, {
            prepareHeader: () => doc.font(HELVETICA_BOLD).fontSize(8),
            prepareRow: (indexColumn, indexRow, rectRow) => {
                return doc.font(HELVETICA).fontSize(8);
            },
        });

        doc.moveDown(1);

        doc.end();

        return doc;
    }

    async exportXLSXTimeSheetAttendance(filterDTO: ReportFilterDTO, company_id: string, time_zone: any, res: Response) {
        const timeSheetAttendances: ITimeSheetAttendance[] = await this.getTimeSheetAttendance(
            filterDTO,
            company_id,
            time_zone,
        );
        return this.exportService.exportXLSX(timeSheetAttendances, 'Time Sheet Attendance', res);
    }

    async exportXLSXTimeSheetLateSummary(
        filterDTO: ReportFilterDTO,
        company_id: string,
        time_zone: any,
        res: Response,
    ) {
        const timeSheetLateSummary: ITimeSheetLateSummary[] = await this.getTimeSheetLateSummary(
            filterDTO,
            company_id,
            time_zone,
        );
        return this.exportService.exportXLSX(timeSheetLateSummary, 'Time Sheet Late Summary', res);
    }

    async exportPdfTimeSheetLateSummary(filterDTO: ReportFilterDTO, company_id: string, time_zone: any): Promise<any> {
        const timeSheetLateSummary: ITimeSheetLateSummary[] = await this.getTimeSheetLateSummary(
            filterDTO,
            company_id,
            time_zone,
        );

        const startDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.end_date);

        let row: ITimeSheetLateSummaryeExport = {};
        const tableDataArray = [];

        timeSheetLateSummary.forEach(lateSummary => {
            row = lateSummary;
            row.date = this.dateTimeConvertService.dateDisplayFormat(lateSummary.date);
            tableDataArray.push(row);
        });
        let doc = new PDFDocument({ margin: 20, size: 'A4' });

        const table = {
            headers: [
                { label: 'Employee Name', property: 'employee_name', width: 80, renderer: null },
                { label: 'Position', property: 'designation_name', width: 80, renderer: null },
                { label: 'Date', property: 'date', width: 60, renderer: null },
                { label: 'Shift Start', property: 'shift_start', width: 60, renderer: null },
                { label: 'Actual Start', property: 'actual_start', width: 60, renderer: null },
                { label: 'Shift End', property: 'shift_end', width: 60, renderer: null },
                { label: 'Actual End', property: 'actual_end', width: 60, renderer: null },
                { label: 'Late', property: 'late_status', width: 50, renderer: null },
                {
                    label: 'Late By ',
                    property: 'late_by',
                    width: 50,
                    renderer: (value, indexColumn, indexRow, row) => {
                        if (value) {
                            return this.dateTimeConvertService.getTimeFromSecond(value);
                        } else {
                            return '';
                        }
                    },
                },
            ],
            datas: tableDataArray,
        };

        doc.text(`Time Sheet Late Summary Between:  ${startDate}  -  ${endDate}`);

        doc.moveDown(2);

        doc.table(table, {
            prepareHeader: () => doc.font(HELVETICA_BOLD).fontSize(8),
            prepareRow: (indexColumn, indexRow, rectRow) => {
                return doc.font(HELVETICA).fontSize(8);
            },
        });

        doc.moveDown(1);

        doc.end();

        return doc;
    }

    async exportPdfTimeSheet(filterDTO: ReportFilterDTO, company_id: string, time_zone: any) {
        const timeSheets: any[] = await this.getTimeSheet(filterDTO, company_id, time_zone);

        const startDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.end_date);
        const dateColumns = this.dateTimeConvertService.generateDateColumnsByRange(
            filterDTO.start_date,
            filterDTO.end_date,
        );

        let totalColckInDuration = 0;
        let columnSums: any = {};
        let headers = [
            { label: 'Name', property: 'employee_name', align: 'center', width: 150, renderer: null },
            {
                label: 'Total',
                property: 'total_clock_in',
                align: 'center',
                width: 100,
                renderer: (value, indexColumn, indexRow, row) =>
                    this.dateTimeConvertService.getTimeFromSecond(Number(value)),
            },
        ];

        let timesheetObj = {};
        let timesheetList = [];

        timeSheets.forEach((row: any, index: number) => {
            let i = 0;
            if (row?.total_clock_in) {
                totalColckInDuration += Number(row.total_clock_in);
            }
            Object.keys(row).forEach((column: string) => {
                let totalDuration = 0;
                if (dateColumns[i] === column) {
                    timesheetObj[column] = JSON.stringify(row?.[column]);
                    const parsedTimeSheet = row?.[column];
                    parsedTimeSheet.forEach((item: any) => {
                        if (item?.diff) {
                            totalDuration += Number(item.diff);
                        }
                    });
                    if (!columnSums[column]) {
                        columnSums[column] = 0;
                    }
                    columnSums[column] += totalDuration;
                    i++;
                } else {
                    timesheetObj[column] = row?.[column];
                }
            });
            timesheetList.push(timesheetObj);
        });

        let lastRow = ['Total', `${totalColckInDuration}`];

        dateColumns.forEach((column: string) => {
            headers.push({
                label: `${this.dateTimeConvertService.dateDisplayFormat(column)}`,
                align: 'center',
                property: column,
                width: 150,
                renderer: (value, indexColumn, indexRow, row) => {
                    let dateColumnValue = ``;
                    let totalDuration = 0;
                    try {
                        let parsedValue = JSON.parse(value);

                        parsedValue.map((item: any, index: number) => {
                            if (item?.diff) {
                                totalDuration += Number(item.diff);
                            }
                            if (parsedValue.length == index + 1) {
                                if (item?.start && item?.end) {
                                    dateColumnValue += `\n${item.start} - ${
                                        item.end
                                    }\n Total time: ${this.dateTimeConvertService.getTimeFromSecond(totalDuration)}`;
                                } else if (item?.start) {
                                    dateColumnValue += `\n${
                                        item.start
                                    } -\n Total time: ${this.dateTimeConvertService.getTimeFromSecond(totalDuration)}`;
                                } else {
                                    dateColumnValue += `\n Total time: ${this.dateTimeConvertService.getTimeFromSecond(
                                        totalDuration,
                                    )}`;
                                }
                            } else {
                                if (item?.start && item?.end) {
                                    dateColumnValue += `\n${item.start} - ${item.end}`;
                                } else if (item?.start) {
                                    dateColumnValue += `\n${item.start}  -  `;
                                }
                            }
                        });

                        return dateColumnValue;
                    } catch {
                        return value;
                    }
                },
            });

            lastRow.push(this.dateTimeConvertService.getTimeFromSecond(columnSums[column]));
        });

        const table = {
            headers: headers,
            datas: timesheetList,
            rows: [lastRow],
        };

        let doc = new PDFDocument({ margin: 25, size: [150 * headers.length, 800] });

        doc.text(`Time Sheet Statistics Between:  ${startDate}  -  ${endDate}`);

        doc.moveDown(2);

        doc.table(table, {
            columnSpacing: 10,
            padding: [6],
            columnsSize: [200, 220, 135],
            prepareHeader: () => {
                return doc.font(HELVETICA_BOLD).fontSize(10);
            },
            prepareRow: (row, indexColumn, indexRow, rectRow, rectCell) => {
                const { x, y, width, height } = rectCell;
                // first line
                if (indexColumn === 0) {
                    doc.lineWidth(0.5)
                        .moveTo(x, y)
                        .lineTo(x, y + height)
                        .stroke();
                }

                doc.lineWidth(0.5)
                    .moveTo(x + width, y)
                    .lineTo(x + width, y + height)
                    .stroke();
                return doc.font(HELVETICA).fontSize(8);
            },
        });

        doc.end();

        return doc;
    }

    async exportPdfTimeSheetSummary(filterDTO: ReportFilterDTO, company_id: string, time_zone: any) {
        const timeSheets: any[] = await this.getTimeSheet(filterDTO, company_id, time_zone);

        const startDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.end_date);
        const dateColumns = this.dateTimeConvertService.generateDateColumnsByRange(
            filterDTO.start_date,
            filterDTO.end_date,
        );

        const whiteSpace5 = '\xa0'.repeat(5);
        const whiteSpace10 = '\xa0'.repeat(10);
        const headerLabel = `Start ${whiteSpace10} End ${whiteSpace10} Hrs`;

        let totalColckInDuration = 0;
        let columnSums: any = {};
        let headers = [
            { label: 'Name', property: 'employee_name', align: 'center', width: 150, renderer: null },
            {
                label: 'Total',
                property: 'total_clock_in',
                align: 'center',
                width: 100,
                renderer: (value, indexColumn, indexRow, row) =>
                    this.dateTimeConvertService.getTimeFromSecond(Number(value)),
            },
        ];

        let timesheetObj = {};
        let timesheetList = [];

        timeSheets.forEach((row: any, index: number) => {
            let i = 0;
            if (row?.total_clock_in) {
                totalColckInDuration += Number(row.total_clock_in);
            }
            Object.keys(row).forEach((column: string) => {
                let totalDuration = 0;
                if (dateColumns[i] === column) {
                    timesheetObj[column] = JSON.stringify(row?.[column]);
                    const parsedTimeSheet = row?.[column];
                    parsedTimeSheet.forEach((item: any) => {
                        if (item?.diff) {
                            totalDuration += Number(item.diff);
                        }
                    });
                    if (!columnSums[column]) {
                        columnSums[column] = 0;
                    }
                    columnSums[column] += totalDuration;
                    i++;
                } else {
                    timesheetObj[column] = row?.[column];
                }
            });
            timesheetList.push(timesheetObj);
        });

        let lastRow = ['Total', `${totalColckInDuration}`];

        dateColumns.forEach((column: string, index) => {
            headers.push({
                label: `${this.dateTimeConvertService.dateDisplayFormat(column)}\n ${headerLabel}`,
                align: 'center',
                property: column,
                width: 150,
                renderer: (value, indexColumn, indexRow, row) => {
                    let data = ``;
                    try {
                        let parsedValue = JSON.parse(value);

                        parsedValue.map((item: any) => {
                            if (item?.start && item?.end && item?.diff) {
                                const startTime = item.start;
                                const endTime = item.end;
                                const colckInDuration = this.dateTimeConvertService.getTimeFromSecond(item.diff);
                                data += `\n${startTime}  ${whiteSpace5} ${endTime} ${whiteSpace5}  ${colckInDuration}\n`;
                            }
                        });

                        return data;
                    } catch {
                        return value;
                    }
                },
            });

            lastRow.push(this.dateTimeConvertService.getTimeFromSecond(columnSums[column]));
        });

        const table = {
            headers: headers,
            datas: timesheetList,
            rows: [lastRow],
        };

        let doc = new PDFDocument({ margin: 25, size: [150 * headers.length, 800] });

        doc.text(`Time Sheet Summary Statistics Between:  ${startDate}  -  ${endDate}`);

        doc.moveDown(2);

        doc.table(table, {
            columnSpacing: 10,
            padding: [6],
            columnsSize: [200, 220, 135],
            prepareHeader: () => {
                return doc.font(HELVETICA_BOLD).fontSize(10);
            },
            prepareRow: (row, indexColumn, indexRow, rectRow, rectCell) => {
                const { x, y, width, height } = rectCell;
                // first line
                if (indexColumn === 0) {
                    doc.lineWidth(0.5)
                        .moveTo(x, y)
                        .lineTo(x, y + height)
                        .stroke();
                }

                doc.lineWidth(0.5)
                    .moveTo(x + width, y)
                    .lineTo(x + width, y + height)
                    .stroke();
                return doc.font(HELVETICA).fontSize(8);
            },
        });

        doc.end();

        return doc;
    }

    async exportPdfLocationHistory(filterDTO: ReportFilterDTO, company_id: string, time_zone: any): Promise<any> {
        const locationHistoryList: ILocationHistoryExport[] = await this.getLocationHistory(
            filterDTO,
            company_id,
            time_zone,
        );

        const startDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.end_date);

        let row: ILocationHistoryExport = {};
        const tableDataArray = [];

        await locationHistoryList.forEach(async locationHistory => {
            const clockInLocation = (await locationHistory?.clockin_location)
                ? JSON.parse(locationHistory?.clockin_location)
                : null;
            const clockOutLocation = (await locationHistory?.clockin_location)
                ? JSON.parse(locationHistory?.clockin_location)
                : null;
            row = locationHistory;
            row.clockin_location = clockInLocation
                ? `${clockInLocation?.address?.road}, ${clockInLocation?.address?.neighbourhood}`
                : '';

            row.clockout_location = clockOutLocation
                ? `${clockOutLocation?.address?.road}, ${clockOutLocation?.address?.neighbourhood}`
                : '';
            tableDataArray.push(row);
        });
        let doc = new PDFDocument({ margin: 20, size: [800, 600] });

        const table = await {
            headers: [
                { label: 'Date', property: 'date', width: 60, renderer: null },
                { label: 'Employee Name', property: 'employee_name', width: 100, renderer: null },
                { label: 'Clock In', property: 'clock_start_time', width: 60, renderer: null },
                { label: 'Clock Out', property: 'clock_end_time', width: 60, renderer: null },
                {
                    label: 'Length',
                    property: 'total_clock_duration',
                    width: 60,
                    renderer: (value, indexColumn, indexRow, row) => {
                        if (value) {
                            return this.dateTimeConvertService.getTimeFromSecond(value);
                        } else {
                            return '';
                        }
                    },
                },
                {
                    label: 'Break',
                    property: 'total_break_duration',
                    width: 60,
                    renderer: (value, indexColumn, indexRow, row) => {
                        if (value) {
                            return this.dateTimeConvertService.getTimeFromSecond(value);
                        } else {
                            return '';
                        }
                    },
                },
                { label: 'Branch', property: 'branch_name', width: 80, renderer: null },
                {
                    label: 'Designation',
                    property: 'designation_name',
                    width: 80,
                    renderer: (value, indexColumn, indexRow, row) => {
                        if (value) {
                            return value;
                        } else {
                            return '';
                        }
                    },
                },
                { label: 'Clock In Location', property: 'clockin_location', width: 100, renderer: null },
                { label: 'Clock out Location', property: 'clockout_location', width: 100, renderer: null },
            ],
            datas: tableDataArray,
        };

        doc.text(`ClockIn and ClockOut Location history Between:  ${startDate}  -  ${endDate}`);

        doc.moveDown(2);

        doc.table(table, {
            prepareHeader: () => doc.font(HELVETICA_BOLD).fontSize(8),
            prepareRow: (indexColumn, indexRow, rectRow) => {
                return doc.font(HELVETICA).fontSize(8);
            },
        });

        doc.moveDown(1);

        doc.end();

        return doc;
    }

    async exportXLSXLocationHistory(filterDTO: ReportFilterDTO, company_id: string, time_zone: any, res: Response) {
        const locationHistory = await this.getLocationHistory(filterDTO, company_id, time_zone);
        return this.exportService.exportXLSXLocationHistory(locationHistory, 'Time Sheet Location History', res);
    }
}
