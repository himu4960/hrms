import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Response } from 'express';

import PDFDocument from 'pdfkit-table';

import { WorkShiftRepository } from '../../repository/work-shift.repository';

import { DateTimeConvertService } from '../../shared/convert/date-time-convert.service';
import { ExportService } from './../../shared/export/export.service';

import { ReportFilterDTO } from '../dto/reports/report.dto';
import { IHoursScheduled } from '../../web/rest/reports/schedule/models/schedule.report.model';
import { HELVETICA, HELVETICA_BOLD } from '../../shared/constant/font.constant';
import { SERVER_TIME_ZONE } from '../../shared/constant/date-format';

@Injectable()
export class ScheduleReportService {
    logger = new Logger('ScheduleReportService');

    constructor(
        @InjectRepository(WorkShiftRepository) private workShiftRepository: WorkShiftRepository,
        private readonly dateTimeConvertService: DateTimeConvertService,
        private readonly exportService: ExportService,
    ) {}

    async getScheduleSummary(filterDTO: ReportFilterDTO, company_id: string, time_zone: any): Promise<any | undefined> {
        const { employee_id, branch_id, designation_id } = filterDTO;
        const startDate = this.dateTimeConvertService.convertLocalDateToUTCDate(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.convertLocalDateToUTCDate(filterDTO.end_date);
        endDate.setDate(endDate.getDate() + 1);
        const startDateTimeStamp = startDate.getTime();
        const endDateTimeStamp = endDate.getTime();

        const totalDays = this.dateTimeConvertService.getDateDifferenceInDays(endDate, startDate);

        startDate.setDate(startDate.getDate() - 1);

        let caseStatement = '';
        let dateColumns = '';
        for (let i = 0; i < totalDays; i++) {
            startDate.setDate(startDate.getDate() + 1);
            const start = this.dateTimeConvertService.format(startDate);

            caseStatement += `, CASE
                                WHEN work_shift.start_date = '${start}'
                                THEN JSON_OBJECT('start', work_shift.start_time,
                                        'end', work_shift.end_time,
                                        'diff', work_shift.total_work_shift_duration)
                                END AS '${start}'`;

            dateColumns +=
                ', IF(COUNT(pvt.`' +
                start +
                '`) = 0, JSON_ARRAY(), JSON_ARRAYAGG(pvt.`' +
                start +
                "`)) AS '" +
                start +
                "'";
        }

        let whereStatement = `WHERE 1 
                        AND start_date BETWEEN '${startDateTimeStamp}' AND '${endDateTimeStamp}'
                        AND hrms_workshifts.company_id = ${company_id}`;

        if (employee_id) {
            whereStatement += ` AND hrms_employees.id = ${employee_id}`;
        }
        if (branch_id) {
            whereStatement += ` AND hrms_workshifts.branch_id = ${branch_id}`;
        }
        if (designation_id) {
            whereStatement += ` AND hrms_workshifts.designation_id = ${designation_id}`;
        }

        const selectStatement = `SELECT pvt.employee_id,
                                pvt.employee_name,
                                SUM(pvt.total_work_shift_duration) AS total_work_shift_duration
                                ${dateColumns}
                        FROM (
                                SELECT work_shift.employee_id,
                                work_shift.employee_name,
                                work_shift.total_work_shift_duration
                                ${caseStatement}
                        FROM (
                                SELECT hrms_employees.id AS employee_id,
                                CONCAT(hrms_employees.first_name, ' ', hrms_employees.last_name) AS 'employee_name',
                                TIME_FORMAT(CONVERT_TZ(FROM_UNIXTIME(start_date / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%h: %i %p") AS start_time,
                                DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(start_date / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%Y-%m-%d") AS start_date,
                                TIME_FORMAT(CONVERT_TZ(FROM_UNIXTIME(end_date / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%h: %i %p") AS end_time,
                                DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(end_date / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%Y-%m-%d") AS end_date,
                                total_work_shift_duration
                        FROM hrms_workshifts
                                INNER JOIN hrms_work_shift_employees work_shift_employees ON work_shift_employees.work_shift_id = hrms_workshifts.id
                                INNER JOIN hrms_employees ON hrms_employees.id = work_shift_employees.employee_id
                                ${whereStatement}
                        ) AS work_shift
                                ORDER BY start_date
                        ) AS pvt`;

        const groupByStatement = `GROUP BY pvt.employee_id`;

        const sqlStatement = `${selectStatement}  ${groupByStatement}`;

        return await this.workShiftRepository.query(sqlStatement);
    }

    async getPositionSummary(filterDTO: ReportFilterDTO, company_id: string, time_zone: any): Promise<any | undefined> {
        const { branch_id, designation_id, is_export } = filterDTO;
        const startDate = this.dateTimeConvertService.convertLocalDateToUTCDate(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.convertLocalDateToUTCDate(filterDTO.end_date);
        endDate.setDate(endDate.getDate() + 1);
        const startDateTimeStamp = startDate.getTime();
        const endDateTimeStamp = endDate.getTime();

        const totalDays = this.dateTimeConvertService.getDateDifferenceInDays(endDate, startDate);

        startDate.setDate(startDate.getDate() - 1);

        let branchIdColumn = `branches.id AS branch_id,`;

        if (is_export) {
            branchIdColumn = ``;
        }

        let dateColumns = '';
        for (let i = 0; i < totalDays; i++) {
            startDate.setDate(startDate.getDate() + 1);
            const start = this.dateTimeConvertService.format(startDate);

            dateColumns += `, SUM(CASE WHEN DATE_FORMAT(FROM_UNIXTIME(start_date / 1000), "%Y-%m-%d") = '${start}' THEN total_work_shift_duration ELSE 0 END) AS '${start}'`;
        }

        let whereStatement = `WHERE 1 
                        AND start_date BETWEEN '${startDateTimeStamp}' AND '${endDateTimeStamp}'
                        AND work_shift.company_id = ${company_id}`;

        if (branch_id) {
            whereStatement += ` AND work_shift.branch_id = ${branch_id}`;
        }
        if (designation_id) {
            whereStatement += ` AND work_shift.designation_id = ${designation_id}`;
        }

        const selectStatement = `SELECT ${branchIdColumn}
                        branches.name AS branch_name,
                        designations.name AS designation_name,
                        SUM(total_work_shift_duration) AS total_work_shift
                        ${dateColumns}
                FROM hrms_workshifts work_shift
                        INNER JOIN hrms_branches branches ON branches.id = work_shift.branch_id
                        INNER JOIN hrms_designations designations ON designations.id = work_shift.designation_id
                        ${whereStatement}`;

        const groupByStatement = `GROUP BY work_shift.branch_id, work_shift.designation_id`;

        const sqlStatement = `${selectStatement}  ${groupByStatement}`;

        return await this.workShiftRepository.query(sqlStatement);
    }

    async getHoursScheduled(filterDTO: ReportFilterDTO, company_id: string): Promise<IHoursScheduled[] | undefined> {
        const { employee_id, branch_id, designation_id } = filterDTO;
        const startDate = this.dateTimeConvertService.convertLocalDateToUTCDate(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.convertLocalDateToUTCDate(filterDTO.end_date);
        endDate.setDate(endDate.getDate() + 1);
        const startDateTimeStamp = startDate.getTime();
        const endDateTimeStamp = endDate.getTime();

        let whereStatement = `WHERE 1 
                        AND work_shifts.start_date BETWEEN '${startDateTimeStamp}' AND '${endDateTimeStamp}'
                        AND work_shifts.company_id = ${company_id}`;

        if (employee_id) {
            whereStatement += ` AND work_shift_employees.employee_id = ${employee_id}`;
        }
        if (branch_id) {
            whereStatement += ` AND work_shifts.branch_id = ${branch_id}`;
        }
        if (designation_id) {
            whereStatement += ` AND work_shifts.designation_id = ${designation_id}`;
        }

        const selectStatement = `SELECT employees.id AS employee_id,
                        CONCAT(employees.first_name, ' ', employees.last_name) AS employee_name,
                        SUM( work_shifts.total_work_shift_duration) AS 'work_shift_hours'
                FROM hrms_workshifts work_shifts
                        INNER JOIN hrms_work_shift_employees work_shift_employees ON work_shift_employees.work_shift_id = work_shifts.id
                        INNER JOIN hrms_employees employees ON employees.id = work_shift_employees.employee_id`;

        const groupByStatement = `GROUP BY employees.id`;
        const orderByStatement = `ORDER BY employees.id`;

        const sqlStatement = `${selectStatement} ${whereStatement} ${groupByStatement} ${orderByStatement}`;

        return await this.workShiftRepository.query(sqlStatement);
    }

    async exportPdfHoursScheduled(filterDTO: ReportFilterDTO, company_id: string) {
        const startDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.end_date);

        const hoursScheduled: any[] = await this.getHoursScheduled(filterDTO, company_id);

        let headers = [
            { label: 'Employee', property: 'employee_name', align: 'center', width: 240, renderer: null },
            {
                label: 'Hours',
                property: 'work_shift_hours',
                align: 'center',
                width: 240,
                renderer: (value, indexColumn, indexRow, row) => this.dateTimeConvertService.getTimeFromSecond(value),
            },
        ];

        let totalHoursScheduled = 0;

        hoursScheduled.forEach((row: any, index: number) => {
            if (row?.work_shift_hours) {
                totalHoursScheduled += Number(row.work_shift_hours);
            }
        });

        let lastRow = [`Total: (${hoursScheduled.length}) `, `${totalHoursScheduled}`];

        const table = {
            headers: headers,
            datas: hoursScheduled,
            rows: [lastRow],
        };

        let doc = new PDFDocument({ margin: 50, size: 'A4' });

        doc.text(`Hours Scheduled Statistics Between:  ${startDate}  -  ${endDate}`);

        doc.moveDown(2);

        doc.table(table, {
            columnSpacing: 10,
            padding: [6],
            prepareHeader: () => {
                return doc.font(HELVETICA_BOLD).fontSize(11);
            },
            prepareRow: (row, indexColumn, indexRow, rectRow, rectCell) => {
                const { x, y, width, height } = rectCell;
                // first line
                if (indexColumn === 0) {
                    doc.lineWidth(0.5)
                        .moveTo(x, y)
                        .lineTo(x, y + height)
                        .stroke();
                }

                doc.lineWidth(0.5)
                    .moveTo(x + width, y)
                    .lineTo(x + width, y + height)
                    .stroke();
                return doc.font(HELVETICA).fontSize(10);
            },
        });

        doc.end();

        return doc;
    }

    async exportXLSXHoursScheduled(
        filterDTO: ReportFilterDTO,
        company_id: string,
        res: Response,
    ): Promise<any | undefined> {
        const hoursScheduled = await this.getHoursScheduled(filterDTO, company_id);
        return this.exportService.exportXLSX(hoursScheduled, 'Hours Scheduled', res);
    }

    async exportXLSXPositionSummary(
        filterDTO: ReportFilterDTO,
        company_id: string,
        res: Response,
        time_zone: any,
    ): Promise<any | undefined> {
        const positionSummary = await this.getPositionSummary(filterDTO, company_id, time_zone);
        return this.exportService.exportXLSXPositionSummary(positionSummary, 'Position Summary', res);
    }

    async exportPdfPositionSummary(filterDTO: ReportFilterDTO, company_id: string, time_zone: any) {
        const positionSummary: any[] = await this.getPositionSummary(filterDTO, company_id, time_zone);

        const startDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.end_date);
        const dateColumns = this.dateTimeConvertService.generateDateColumnsByRange(
            filterDTO.start_date,
            filterDTO.end_date,
        );

        let totalPositionSummaryDuration = 0;
        let columnSums: any = {};
        let headers = [
            { label: 'Branch Name', property: 'branch_name', align: 'center', width: 100, renderer: null },
            { label: 'Designation Name', property: 'designation_name', align: 'center', width: 100, renderer: null },
            {
                label: 'Total',
                property: 'total_work_shift',
                align: 'center',
                width: 60,
                renderer: (value, indexColumn, indexRow, row) => {
                    const convertedValue = Number(value);
                    if (convertedValue) {
                        return this.dateTimeConvertService.getTimeFromSecond(value);
                    } else {
                        return;
                    }
                },
            },
        ];

        positionSummary.forEach((row: any, index: number) => {
            let i = 0;
            if (row?.total_work_shift) {
                totalPositionSummaryDuration += Number(row.total_work_shift);
            }
            Object.keys(row).forEach((column: string) => {
                let totalDuration = 0;
                if (dateColumns[i] === column) {
                    totalDuration += Number(row[column]);

                    if (!columnSums[column]) {
                        columnSums[column] = 0;
                    }
                    columnSums[column] += totalDuration;
                    i++;
                }
            });
        });

        let lastRow = ['', 'Total', `${totalPositionSummaryDuration}`];

        dateColumns.forEach((column: string) => {
            headers.push({
                label: `${this.dateTimeConvertService.dateDisplayFormat(column)}`,
                align: 'center',
                property: column,
                width: 80,
                renderer: (value, indexColumn, indexRow, row) => {
                    const convertedValue = Number(value);
                    if (convertedValue) {
                        return this.dateTimeConvertService.getTimeFromSecond(value);
                    } else {
                        return;
                    }
                },
            });

            lastRow.push(columnSums[column]);
        });

        const table = {
            headers: headers,
            datas: positionSummary,
            rows: [lastRow],
        };

        let doc = new PDFDocument({ margin: 20, size: [80 * headers.length + 60, positionSummary.length * 30 + 600] });

        doc.text(`Position Summary Statistics Between:  ${startDate}  -  ${endDate}`);

        doc.moveDown(2);

        doc.table(table, {
            columnSpacing: 10,
            padding: [6],
            columnsSize: [200, 220, 135],
            prepareHeader: () => {
                return doc.font(HELVETICA_BOLD).fontSize(10);
            },
            prepareRow: (row, indexColumn, indexRow, rectRow, rectCell) => {
                const { x, y, width, height } = rectCell;
                // first line
                if (indexColumn === 0) {
                    doc.lineWidth(0.5)
                        .moveTo(x, y)
                        .lineTo(x, y + height)
                        .stroke();
                }

                doc.lineWidth(0.5)
                    .moveTo(x + width, y)
                    .lineTo(x + width, y + height)
                    .stroke();
                return doc.font(HELVETICA).fontSize(8);
            },
        });

        doc.end();

        return doc;
    }

    async exportPdfScheduleSummary(filterDTO: ReportFilterDTO, company_id: string, time_zone: any) {
        const scheduleSummary: any[] = await this.getScheduleSummary(filterDTO, company_id, time_zone);

        const startDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.end_date);
        const dateColumns = this.dateTimeConvertService.generateDateColumnsByRange(
            filterDTO.start_date,
            filterDTO.end_date,
        );

        const whiteSpace5 = '\xa0'.repeat(5);
        const whiteSpace10 = '\xa0'.repeat(10);
        const headerLabel = `Start ${whiteSpace10} End ${whiteSpace10} Hrs`;

        let totalScheduleSummaryDuration = 0;
        let columnSums: any = {};
        let headers = [
            { label: 'Name', property: 'employee_name', align: 'center', width: 150, renderer: null },
            {
                label: 'Total',
                property: 'total_work_shift_duration',
                align: 'center',
                width: 100,
                renderer: (value, indexColumn, indexRow, row) =>
                    this.dateTimeConvertService.getTimeFromSecond(Number(value)),
            },
        ];

        let scheduleSummaryObj = {};
        let scheduleSummaryList = [];

        scheduleSummary.forEach((row: any, index: number) => {
            let i = 0;
            if (row?.total_work_shift_duration) {
                totalScheduleSummaryDuration += Number(row.total_work_shift_duration);
            }
            Object.keys(row).forEach((column: string) => {
                let totalDuration = 0;
                if (dateColumns[i] === column) {
                    scheduleSummaryObj[column] = JSON.stringify(row?.[column]);
                    const parsedScheduleSummary = row?.[column];
                    parsedScheduleSummary.forEach((item: any) => {
                        if (item?.diff) {
                            totalDuration += Number(item.diff);
                        }
                    });
                    if (!columnSums[column]) {
                        columnSums[column] = 0;
                    }
                    columnSums[column] += totalDuration;
                    i++;
                } else {
                    scheduleSummaryObj[column] = row?.[column];
                }
            });
            scheduleSummaryList.push(scheduleSummaryObj);
        });

        let lastRow = ['Total', `${totalScheduleSummaryDuration}`];

        dateColumns.forEach((column: string) => {
            headers.push({
                label: `${this.dateTimeConvertService.dateDisplayFormat(column)}\n ${headerLabel}`,
                align: 'center',
                property: column,
                width: 150,
                renderer: (value, indexColumn, indexRow, row) => {
                    let data = ``;
                    try {
                        let parsedValue = JSON.parse(value);

                        parsedValue.map((item: any) => {
                            if (item?.start && item?.end && item?.diff) {
                                const startTime = item.start;
                                const endTime = item.end;
                                const scheduleDuration = this.dateTimeConvertService.getTimeFromSecond(item.diff);
                                data += `\n${startTime}  ${whiteSpace5} ${endTime} ${whiteSpace5}  ${scheduleDuration}\n`;
                            }
                        });

                        return data;
                    } catch {
                        return value;
                    }
                },
            });

            lastRow.push(this.dateTimeConvertService.getTimeFromSecond(columnSums[column]));
        });

        const table = {
            headers: headers,
            datas: scheduleSummaryList,
            rows: [lastRow],
        };

        let doc = new PDFDocument({ margin: 25, size: [150 * headers.length, 800] });

        doc.text(`Schedule Summary Statistics Between:  ${startDate}  -  ${endDate}`);

        doc.moveDown(2);

        doc.table(table, {
            columnSpacing: 10,
            padding: [6],
            columnsSize: [200, 220, 135],
            prepareHeader: () => {
                return doc.font(HELVETICA_BOLD).fontSize(10);
            },
            prepareRow: (row, indexColumn, indexRow, rectRow, rectCell) => {
                const { x, y, width, height } = rectCell;
                // first line
                if (indexColumn === 0) {
                    doc.lineWidth(0.5)
                        .moveTo(x, y)
                        .lineTo(x, y + height)
                        .stroke();
                }

                doc.lineWidth(0.5)
                    .moveTo(x + width, y)
                    .lineTo(x + width, y + height)
                    .stroke();
                return doc.font(HELVETICA).fontSize(8);
            },
        });

        doc.end();

        return doc;
    }

    async getShiftsScheduled(
        filterDTO: ReportFilterDTO,
        company_id: string,
        time_zone: any,
    ): Promise<any[] | undefined> {
        const { employee_id, branch_id, designation_id } = filterDTO;
        const startDate = this.dateTimeConvertService.convertLocalDateToUTCDate(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.convertLocalDateToUTCDate(filterDTO.end_date);
        endDate.setDate(endDate.getDate() + 1);
        const startDateTimeStamp = startDate.getTime();
        const endDateTimeStamp = endDate.getTime();

        const totalDays = this.dateTimeConvertService.getDateDifferenceInDays(endDate, startDate);

        startDate.setDate(startDate.getDate() - 1);

        let caseStatement = '';
        let dateColumns = '';
        for (let i = 0; i < totalDays; i++) {
            startDate.setDate(startDate.getDate() + 1);
            const start = this.dateTimeConvertService.format(startDate);

            caseStatement += `, CASE
                                WHEN work_shift.start_date = '${start}'
                                THEN JSON_OBJECT('start', work_shift.start_time,
                                        'end', work_shift.end_time,
                                        'designation', work_shift.designation_name)
                                END AS '${start}'`;

            dateColumns +=
                ', IF(COUNT(pvt.`' +
                start +
                '`) = 0, JSON_ARRAY(), JSON_ARRAYAGG(pvt.`' +
                start +
                "`)) AS '" +
                start +
                "'";
        }

        let whereStatement = `WHERE 1 
                        AND start_date BETWEEN '${startDateTimeStamp}' AND '${endDateTimeStamp}'
                        AND hrms_workshifts.company_id = ${company_id}`;

        if (employee_id) {
            whereStatement += ` AND hrms_employees.id = ${employee_id}`;
        }
        if (branch_id) {
            whereStatement += ` AND hrms_workshifts.branch_id = ${branch_id}`;
        }
        if (designation_id) {
            whereStatement += ` AND hrms_workshifts.designation_id = ${designation_id}`;
        }

        const selectStatement = `SELECT pvt.employee_id,
                                pvt.employee_name,
                                SUM(pvt.total_work_shift_duration) AS total_work_shift_duration
                                ${dateColumns}
                        FROM (
                                SELECT work_shift.employee_id,
                                work_shift.employee_name,
                                work_shift.total_work_shift_duration,
                                DATE_FORMAT(work_shift.start_date, "%Y-%m-%d") AS start_date
                                ${caseStatement}
                        FROM (
                                SELECT hrms_employees.id AS employee_id,
                                CONCAT(hrms_employees.first_name, ' ', hrms_employees.last_name) AS 'employee_name',
                                TIME_FORMAT(CONVERT_TZ(FROM_UNIXTIME(start_date / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%h: %i %p") AS start_time,
                                DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(start_date / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%Y-%m-%d") AS start_date,
                                TIME_FORMAT(CONVERT_TZ(FROM_UNIXTIME(end_date / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%h: %i %p") AS end_time,
                                DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(end_date / 1000), '${SERVER_TIME_ZONE}', '${time_zone.offset}'), "%Y-%m-%d") AS end_date,
                                total_work_shift_duration,
                                hrms_designations.name AS designation_name
                        FROM hrms_workshifts
                                INNER JOIN hrms_work_shift_employees work_shift_employees ON work_shift_employees.work_shift_id = hrms_workshifts.id
                                INNER JOIN hrms_employees ON hrms_employees.id = work_shift_employees.employee_id
                                INNER JOIN hrms_designations ON hrms_designations.id = hrms_workshifts.designation_id
                                ${whereStatement}
                        ) AS work_shift
                                ORDER BY start_date
                        ) AS pvt`;

        const groupByStatement = `GROUP BY pvt.employee_id`;

        const sqlStatement = await `${selectStatement}  ${groupByStatement}`;

        return await this.workShiftRepository.query(sqlStatement);
    }

    async exportPdfShiftsScheduled(filterDTO: ReportFilterDTO, company_id: string, time_zone: any) {
        const scheduleSummaryList: any[] = await this.getShiftsScheduled(filterDTO, company_id, time_zone);

        const startDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.start_date);
        const endDate = this.dateTimeConvertService.dateDisplayFormat(filterDTO.end_date);
        const dateColumns = this.dateTimeConvertService.generateDateColumnsByRange(
            filterDTO.start_date,
            filterDTO.end_date,
        );

        let headers = [
            { label: 'Employee Name', property: 'employee_name', align: 'center', width: 120, renderer: null },
            {
                label: 'Total',
                property: 'total_work_shift_duration',
                align: 'center',
                width: 80,
                renderer: (value, indexColumn, indexRow, row) =>
                    this.dateTimeConvertService.getTimeFromSecond(Number(value)),
            },
        ];

        let parsedScheduleSummaryObj = {};
        let parsedScheduleSummaryList = [];

        scheduleSummaryList.forEach(scheduleSummary => {
            let i = 0;
            Object.keys(scheduleSummary).forEach((column: string) => {
                if (dateColumns[i] === column) {
                    parsedScheduleSummaryObj[column] = JSON.stringify(scheduleSummary[column]);
                    i++;
                } else {
                    parsedScheduleSummaryObj[column] = scheduleSummary[column];
                }
            });
            parsedScheduleSummaryList.push(parsedScheduleSummaryObj);
        });

        dateColumns.forEach((column: string) => {
            headers.push({
                label: `${this.dateTimeConvertService.dateDisplayFormat(column)}`,
                align: 'center',
                property: column,
                width: 120,
                renderer: (value, indexColumn, indexRow, row) => {
                    let data = ``;
                    try {
                        let parsedValue = JSON.parse(value);

                        parsedValue.map((item: any) => {
                            if (item?.start && item?.end && item?.designation) {
                                const startTime = item.start;
                                const endTime = item.end;
                                data += `\n${item?.designation}\n ${startTime} - ${endTime}\n`;
                            }
                        });

                        return data;
                    } catch {
                        return value;
                    }
                },
            });
        });

        const table = {
            headers: headers,
            datas: parsedScheduleSummaryList,
        };

        let doc = new PDFDocument({
            margin: 20,
            size: [120 * headers.length, 30 * parsedScheduleSummaryList.length + 600],
        });

        doc.text(`Shifts Scheduled Statistics Between:  ${startDate}  -  ${endDate}`);

        doc.moveDown(2);

        doc.table(table, {
            columnSpacing: 8,
            padding: [6],
            columnsSize: [200, 200, 135],
            prepareHeader: () => {
                return doc.font(HELVETICA_BOLD).fontSize(10);
            },
            prepareRow: (row, indexColumn, indexRow, rectRow, rectCell) => {
                const { x, y, width, height } = rectCell;
                // first line
                if (indexColumn === 0) {
                    doc.lineWidth(0.5)
                        .moveTo(x, y)
                        .lineTo(x, y + height)
                        .stroke();
                }

                doc.lineWidth(0.5)
                    .moveTo(x + width, y)
                    .lineTo(x + width, y + height)
                    .stroke();
                return doc.font(HELVETICA).fontSize(8);
            },
        });

        doc.end();

        return doc;
    }

    async exportXLSXShiftsScheduled(
        filterDTO: ReportFilterDTO,
        company_id: string,
        res: Response,
        time_zone: any,
    ): Promise<any | undefined> {
        const shiftsScheduled = await this.getShiftsScheduled(filterDTO, company_id, time_zone);
        return this.exportService.exportXLSXShiftsScheduled(shiftsScheduled, 'Shifts Scheduled', res);
    }
}
