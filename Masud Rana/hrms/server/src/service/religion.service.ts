import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In } from 'typeorm';

import { ReligionDTO } from './dto/religion/religion.dto';
import { ReligionCreateDTO } from './dto/religion/religion-create.dto';
import { ReligionUpdateDTO } from './dto/religion/religion-update.dto';

import { ReligionTranslation } from './../domain/religion-translation.entity';
import { Religion } from './../domain/religion.entity';

import { ReligionMapper } from '../service/mapper/religion.mapper';

import { LanguageRepository } from './../repository/language.repository';
import { ReligionRepository } from '../repository/religion.repository';
import { ReligionTranslationRepository } from './../repository/religion-translation.repository';

@Injectable()
export class ReligionService {
    logger = new Logger('ReligionService');

    constructor(
        @InjectRepository(ReligionRepository) private religionRepository: ReligionRepository,
        @InjectRepository(ReligionTranslationRepository)
        private religionTranslationRepository: ReligionTranslationRepository,
        @InjectRepository(LanguageRepository) private languageRepository: LanguageRepository,
    ) {}

    async findAll() {
        return await this.religionRepository
            .createQueryBuilder('religion')
            .leftJoinAndMapMany(
                'religion.translations',
                ReligionTranslation,
                'translation',
                'religion.id = translation.religion_id',
            )
            .select(['religion.id', 'translation.name', 'translation.language_code'])
            .getMany();
    }

    async findById(id: number): Promise<ReligionDTO | undefined> {
        const result = await this.religionRepository.findOne(id);
        return ReligionMapper.fromEntityToDTO(result);
    }

    async findByLangCode(langCode: string) {
        return await this.religionRepository
            .createQueryBuilder('religion')
            .leftJoinAndSelect('religion.translations', 'translation')
            .where('translation.language_code=:language_code', { language_code: langCode })
            .select(['religion.id as id', 'translation.name as name'])
            .getRawMany();
    }

    async save(createReligion: ReligionCreateDTO, creator?: number) {
        const language_codes = createReligion.translations.map(religion => religion.language_code);

        const languageResult = await this.languageRepository.find({ where: { code: In(language_codes) } });

        if (languageResult.length > 0) {
            const result = await this.religionRepository
                .createQueryBuilder()
                .insert()
                .into(Religion)
                .values({})
                .execute();

            const religion_id = result.raw.insertId;
            createReligion.translations.forEach(async religionTranslation => {
                const entity = ReligionMapper.fromDTOtoEntity(religionTranslation);
                if (creator) {
                    if (!entity.created_by) {
                        entity.created_by = creator;
                    }
                    entity.updated_by = creator;
                }
                const { name, language_code } = religionTranslation;

                const languageResult = await this.languageRepository.findOne(language_code);

                if (languageResult) {
                    const religionTranlationResult = await this.religionTranslationRepository.findOne({
                        where: { name },
                    });
                    if (!religionTranlationResult) {
                        await this.religionTranslationRepository
                            .createQueryBuilder()
                            .insert()
                            .into(ReligionTranslation)
                            .values({ name, language_code, religion_id })
                            .execute();
                    } else {
                        throw new HttpException('Religion already exist', HttpStatus.FOUND);
                    }
                } else {
                    throw new HttpException('Language not found!', HttpStatus.NOT_FOUND);
                }
            });
        } else {
            throw new HttpException('Language not found!', HttpStatus.NOT_FOUND);
        }
    }

    async update(id: number, updateReligion: ReligionUpdateDTO, updater?: number) {
        updateReligion.translations.forEach(async religionTranslation => {
            const entity = ReligionMapper.fromDTOtoEntity(religionTranslation);
            if (updater) {
                entity.updated_by = updater;
                entity.updated_at = Date.now();
            }
            const { name, language_code } = religionTranslation;

            const languageResult = await this.languageRepository.findOne(language_code);

            if (languageResult) {
                const isExist = await this.religionTranslationRepository
                    .createQueryBuilder('religionTranslation')
                    .where('religion_id=:id', { id })
                    .andWhere('language_code=:language_code', { language_code })
                    .getOne();

                if (isExist) {
                    await this.religionTranslationRepository
                        .createQueryBuilder()
                        .update(ReligionTranslation)
                        .set({
                            name,
                        })
                        .where('religion_id=:id', { id })
                        .andWhere('language_code=:language_code', { language_code })
                        .execute();
                } else {
                    await this.religionTranslationRepository
                        .createQueryBuilder()
                        .insert()
                        .into(ReligionTranslation)
                        .values([
                            {
                                name,
                                language_code,
                                religion_id: id,
                            },
                        ])
                        .execute();
                }
            } else {
                throw new HttpException('Language not found!', HttpStatus.NOT_FOUND);
            }
        });
        return;
    }

    async deleteById(id: number): Promise<ReligionDTO | undefined> {
        const entityFind = await this.findById(id);
        if (!entityFind) {
            throw new HttpException('Religion is not found!', HttpStatus.NOT_FOUND);
        }
        await this.religionRepository.delete(id);
        const afterDeletedEntity = await this.findById(id);
        if (afterDeletedEntity) {
            throw new HttpException('Religion cannot be deleted!', HttpStatus.FOUND);
        }
        return entityFind;
    }
}
