import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServeStaticModule } from '@nestjs/serve-static';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { BullModule } from '@nestjs/bull';

import path from 'path';

import { FirebaseAdminModule } from './firebase-admin';

import { config } from './config';
import { ormConfig } from './orm.config';


import { AuthModule } from './module/auth.module';
import { BloodGroupModule } from './module/blood-group.module';
import { LanguageModule } from './module/language.module';
import { GeoLocationModule } from './module/geo-location.module';
import { GenderModule } from './module/gender.module';
import { ReligionModule } from './module/religion.module';
import { MaritalStatusModule } from './module/marital-status.module';
import { ApplicationTypeModule } from './module/application-type.module';
import { CompanyTypeModule } from './module/company-type.module';
import { EmployeeTypeModule } from './module/employee-type.module';
import { CompanyModule } from './module/company.module';
import { BranchModule } from './module/branch.module';
import { RoleModule } from './module/role.module';
import { CountryModule } from './module/country.module';
import { MenuModule } from './module/menu.module';
import { PermissionTypeModule } from './module/permission-type.module';
import { TimeZoneModule } from './module/time-zone.module';
import { CompanySettingsModule } from './module/company-settings.module';
import { SkillModule } from './module/skill.module';
import { EmployeeModule } from './module/employee.module';
import { BreakRuleModule } from './module/break-rule.module';
import { DesignationModule } from './module/designation.module';
import { CustomFieldModule } from './module/custom-field.module';
import { CustomFieldTypeModule } from './module/custom-field-type.module';
import { WorkShiftModule } from './module/work-shift.module';
import { TimeClockModule } from './module/time-clock.module';
import { LeaveModule } from './module/leave.module';
import { ReportModule } from './module/report.module';
import { AnnouncementModule } from './module/announcement.module';
import { FileModule } from './module/file.module';
import { JWTModule } from './module/jwt.module';
import { WorkShiftTemplateModule } from './module/work-shift-template.module';
import { HolidayModule } from './module/holiday.module';

import { AuthController } from './web/rest/auth.controller';
import { CommonController } from './web/rest/common.controller';

import { CommonService } from './service/common.service';
import { MailModule } from './shared/mail/mail.module';
import { FcmModule } from './fcm/fcm.module';

// jhipster-needle-add-entity-module-to-main-import - JHipster will import entity modules here, do not remove
// jhipster-needle-add-controller-module-to-main-import - JHipster will import controller modules here, do not remove
// jhipster-needle-add-service-module-to-main-import - JHipster will import service modules here, do not remove

@Module({
    imports: [
        MailModule,
        ConfigModule.forRoot({ isGlobal: true, envFilePath: '.env' }),
        FirebaseAdminModule.forRoot({
            firebaseSpecsPath: path.join(__dirname, './config/hazira-app-firebase-adminsdk-e76z2-caab3ccd1a.json'),
        }),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: ormConfig,
        }),
        ServeStaticModule.forRoot({
            rootPath: config.getClientPath(),
        }),
        BullModule.forRootAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: async (configService: ConfigService) => ({
                redis: {
                    host: configService.get('QUEUE_HOST'),
                    port: configService.get('QUEUE_PORT'),
                },
            }),
        }),
        AuthModule,
        BloodGroupModule,
        LanguageModule,
        GenderModule,
        MaritalStatusModule,
        ReligionModule,
        ApplicationTypeModule,
        CompanyTypeModule,
        EmployeeTypeModule,
        CompanyModule,
        BranchModule,
        RoleModule,
        CountryModule,
        MenuModule,
        PermissionTypeModule,
        TimeZoneModule,
        CompanySettingsModule,
        SkillModule,
        EmployeeModule,
        BreakRuleModule,
        DesignationModule,
        CustomFieldModule,
        CustomFieldTypeModule,
        WorkShiftModule,
        TimeClockModule,
        LeaveModule,
        ReportModule,
        GeoLocationModule,
        AnnouncementModule,
        FileModule,
        JWTModule,
        WorkShiftTemplateModule,
        HolidayModule,
        FcmModule,
        // jhipster-needle-add-entity-module-to-main - JHipster will add entity modules here, do not remove
    ],
    controllers: [
        // jhipster-needle-add-controller-module-to-main - JHipster will add controller modules here, do not remove
        AuthController,
        CommonController,
    ],
    providers: [
        // jhipster-needle-add-service-module-to-main - JHipster will add service modules here, do not remove
        CommonService,
        ConfigService,
    ],
})
export class AppModule {}
