/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne, ManyToMany } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Company } from './company.entity';
import { Employee } from './employee.entity';
import { Designation } from './designation.entity';

import { ENTITY_SKILL } from './../shared/constant/database.constant';

/**
 * A EmployeeSkill.
 */
@Entity(ENTITY_SKILL)
export class Skill extends BaseEntity {
    @Column({ type: 'integer', name: 'company_id', select: false })
    company_id: number;

    @Column({ name: 'name', length: 255 })
    name: string;

    @ManyToOne(
        () => Company,
        (company: Company) => company.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
    company?: Company;

    @ManyToMany(
        () => Employee,
        employee => employee.skills,
    )
    employees: Employee[];

    @ManyToMany(
        () => Designation,
        designation => designation.skills,
    )
    designations: Designation[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
