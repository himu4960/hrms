/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column } from 'typeorm';

import { BaseEntity } from './base/base.entity';

import { ENTITY_TIME_ZONE } from './../shared/constant/database.constant';

/**
 * A TimeZone.
 */
@Entity(ENTITY_TIME_ZONE)
export class TimeZone extends BaseEntity {
    @Column({ name: 'name', length: 100 })
    name: string;

    @Column({ name: 'abbr', length: 20 })
    abbr: string;

    @Column({ name: 'offset', length: 20 })
    offset: string;

    @Column({ name: 'description', length: 255 })
    description: string;

    @Column({ type: 'boolean', name: 'is_active', default: true })
    is_active: boolean;

    @Column({ type: 'boolean', name: 'is_deleted', default: false })
    is_deleted: boolean;

    @Column({ type: 'boolean', name: 'is_default' })
    is_default: boolean;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
