export enum ESettingsTimeClockResolution {
    SMALL = '320X240',
}

export enum ESettingsTimeClockRounding {
    DONT_ROUND = -1,
    MIN_5 = 5,
    MIN_10 = 10,
    MIN_15 = 15,
    MIN_30 = 30,
    MIN_60 = 60,
}

export enum ESettingsTimeClockShiftBuffer {
    ANYTIME = -1,
    NONE = 0,
    MIN_5 = 5,
    MIN_10 = 10,
    MIN_15 = 15,
    MIN_30 = 30,
    MIN_60 = 60,
    MIN_90 = 90,
    HOUR_2 = 120,
    HOUR_3 = 180,
    HOUR_6 = 360,
}

export enum ESettingsTimeClockPastDays {
    ANYTIME = 0,
    DAY_1 = -1,
    DAY_3 = -3,
    DAY_5 = -5,
    DAY_10 = -10,
    DAY_15 = -15,
    DAY_30 = -30,
    DAY_60 = -60,
}

export enum ESettingsTimeClockDirection {
    DOWN = -1,
    NEAREST = 0,
    UP = 1,
}

export enum ESettingsTimeClockLateReminder {
    BEFORE_15_MIN_SHIFT_START = -15,
    BEGINNING_SHIFT = 0,
    AFTER_15_MIN_SHIFT_START = 15,
    AFTER_30_MIN_SHIFT_START = 30,
    AFTER_45_MIN_SHIFT_START = 45,
    AFTER_1_HOUR_SHIFT_START = 60,
}

export enum ESettingsTimeClockCount {
    OFF = 0,
    MIN_10 = 600 * 1000,
    MIN_15 = 900 * 1000,
    MIN_20 = 1200 * 1000,
    MIN_25 = 1500 * 1000,
    MIN_30 = 1800 * 1000,
}
