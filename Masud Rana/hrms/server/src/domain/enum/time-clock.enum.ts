export enum ETimeClockEventType {
    BREAK = 'break',
    POSITION = 'position',
    NOTE = 'note',
}
