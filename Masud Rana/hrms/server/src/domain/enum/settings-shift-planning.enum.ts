export enum EShiftPlanningDays {
    SUNDAY = 1,
    MONDAY = 2,
    TUESDAY = 3,
    WEDNESDAY = 4,
    THURSDAY = 5,
    FRIDAY = 6,
    SATURDAY = 7,
}

export enum EShiftPlanningAllowedUsers {
    MANAGERS_AND_SUPERVISORS = 1,
    MANAGERS_AND_SUPERVISORS_AND_SCHEDULER = 2,
    ALL_USERS = 3,
}

export enum EShiftPlanningEnabledRequirement {
    OFF = 0,
    ANY = 1,
    ALL = 2,
}

export enum EShiftPlanningDropTimeLimit {
    OFF = 0,
    HOUR_12 = 12,
    HOUR_24 = 24,
    HOUR_48 = 48,
    DAY_7 = 7,
    DAY_14 = 14,
}

export enum EShiftPlanningEachMinutes {
    NONE = 0,
    MIN_15 = 1,
    MIN_30 = 2,
    MIN_45 = 3,
    HOUR_1 = 4,
    HOUR_1_MIN_15 = 5,
    HOUR_1_MIN_30 = 6,
    HOUR_1_MIN_45 = 7,
    HOUR_2 = 8,
    HOUR_3 = 9,
    HOUR_4 = 10,
    HOUR_5 = 11,
    HOUR_6 = 12,
    HOUR_7 = 13,
    HOUR_8 = 14,
    HOUR_9 = 15,
    HOUR_10 = 16,
    HOUR_11 = 17,
    HOUR_12 = 18,
}

export enum EShiftPlanningShifts {
    ALL_SHIFT = 1,
    WEEK_1 = 7,
    WEEK_2 = 14,
    WEEK_3 = 21,
    MONTH_1 = 30,
    MONTH_2 = 60,
    MONTH_3 = 90,
    MONTH_4 = 120,
    MONTH_5 = 150,
}

export enum EShiftPlanningSchedules {
    NONE = 0,
    HOURS_ONLY = 1,
    HOURS_AND_STAFF_COUNT = 2,
    HOURS_AND_STAFF_COST = 3,
}

export enum EShiftPlanningDates {
    UNLIMITED = 0,
    DAY_1 = 1,
    DAY_2 = 2,
    DAY_3 = 3,
    DAY_4 = 4,
    DAY_5 = 5,
    DAY_6 = 6,
    DAY_7 = 7,
    DAY_8 = 8,
    DAY_9 = 9,
    DAY_10 = 10,
    DAY_11 = 11,
    DAY_12 = 12,
    DAY_13 = 13,
    DAY_14 = 14,
    DAY_15 = 15,
    DAY_16 = 16,
    DAY_17 = 17,
    DAY_18 = 18,
    DAY_19 = 19,
    DAY_20 = 20,
    DAY_21 = 21,
    DAY_22 = 22,
    DAY_23 = 23,
    DAY_24 = 24,
    DAY_25 = 25,
    DAY_26 = 26,
    DAY_27 = 27,
    DAY_28 = 28,
    DAY_29 = 29,
    DAY_30 = 30,
}
