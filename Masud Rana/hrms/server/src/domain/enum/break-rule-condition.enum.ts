export enum EBreakRuleConditionType {
    PAID_BREAK = 'paid_break',
    UNPAID_BREAK = 'unpaid_break',
}
