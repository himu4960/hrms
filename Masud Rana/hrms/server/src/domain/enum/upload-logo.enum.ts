export enum ELogoTypeOnSize {
    LARGE = 'large',
    MEDIUM = 'medium',
    SMALL = 'small',
}

export enum ELogoSize {
    LARGE_FILE = 500,
    MEDIUM_FILE = 300,
    SMALL_FILE = 100
}
