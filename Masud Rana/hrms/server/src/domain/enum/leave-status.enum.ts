export enum ELeaveStatusType {
    PENDING = 'Pending',
    APPROVED = 'Approved',
    REJECTED = 'Rejected',
    REQUESTED = 'Requested',
    REQUEST_APPROVED = 'Request Approved',
    REQUEST_REJECTED = 'Request Rejected',
}
