/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Language } from './language.entity';
import { MaritalStatus } from './marital-status.entity';

import { ENTITY_MARITAL_STATUS_TRANSLATION } from './../shared/constant/database.constant';

/**
 * A MaritalStatusTranslation.
 */
@Entity(ENTITY_MARITAL_STATUS_TRANSLATION)
export class MaritalStatusTranslation extends BaseEntity {
    @Column({ type: 'integer', name: 'marital_status_id' })
    marital_status_id: number;

    @Column({ name: 'language_code', length: 2 })
    language_code: string;

    @Column({ name: 'name', length: 50 })
    name: string;

    @ManyToOne(
        () => MaritalStatus,
        (maritalStatus: MaritalStatus) => maritalStatus.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'marital_status_id', referencedColumnName: 'id' })
    maritalStatus: MaritalStatus;

    @ManyToOne(
        () => Language,
        (lang: Language) => lang.code,
    )
    @JoinColumn({ name: 'language_code', referencedColumnName: 'code' })
    language: Language;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
