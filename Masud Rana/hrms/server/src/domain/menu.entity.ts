/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column } from 'typeorm';

import { BaseEntity } from './base/base.entity';

import { ENTITY_MENU } from './../shared/constant/database.constant';

/**
 * A Menu.
 */
@Entity(ENTITY_MENU)
export class Menu extends BaseEntity {
    @Column({ name: 'name', length: 50 })
    name: string;

    @Column({ name: 'translate_name', length: 150, nullable: true })
    translate_name: string;

    @Column({ name: 'icon', length: 50, nullable: true })
    icon: string;

    @Column({ name: 'end_point', length: 50 })
    end_point: string;

    @Column({ name: 'router_link', length: 50, nullable: true })
    router_link: string;

    @Column({ name: 'permissions', nullable: true, length: 510 })
    permissions: string;

    @Column({ type: 'integer', name: 'parent_id', nullable: true })
    parent_id: number;

    @Column({ type: 'integer', name: 'position', nullable: true })
    position: number;

    @Column({ type: 'boolean', name: 'is_menu', default: true })
    is_menu: boolean;

    @Column({ type: 'boolean', name: 'is_active', default: true })
    is_active: boolean;

    @Column({ type: 'boolean', name: 'is_deleted', default: false })
    is_deleted: boolean;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
