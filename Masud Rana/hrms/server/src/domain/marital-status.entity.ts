/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, OneToMany } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { MaritalStatusTranslation } from './marital-status-translation.entity';

import { ENTITY_MARITAL_STATUS } from './../shared/constant/database.constant';

/**
 * A MaritalStatus.
 */
@Entity(ENTITY_MARITAL_STATUS)
export class MaritalStatus extends BaseEntity {
    @Column({ type: 'integer', name: 'company_id', nullable: true })
    company_id?: number;

    @Column({ type: 'boolean', name: 'is_active', default: true })
    is_active: boolean;

    @Column({ type: 'boolean', name: 'is_deleted', default: false })
    is_deleted: boolean;

    @OneToMany(
        () => MaritalStatusTranslation,
        translation => translation.maritalStatus,
    )
    translations?: MaritalStatusTranslation[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
