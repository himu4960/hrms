/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Language } from './language.entity';
import { BloodGroup } from './blood-group.entity';

import { ENTITY_BLOOD_GROUP_TRANSLATION } from './../shared/constant/database.constant';

/**
 * A BloodGroupTranslation.
 */
@Entity(ENTITY_BLOOD_GROUP_TRANSLATION)
export class BloodGroupTranslation extends BaseEntity {
    @Column({ name: 'name', length: 50 })
    name: string;

    @Column({ name: 'language_code' })
    language_code: string;

    @Column({ name: 'blood_group_id' })
    blood_group_id: number;

    @ManyToOne(
        () => Language,
        (lang: Language) => lang.code,
    )
    @JoinColumn({ name: 'language_code', referencedColumnName: 'code' })
    language: Language;

    @ManyToOne(
        () => BloodGroup,
        (bloodGroup: BloodGroup) => bloodGroup.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'blood_group_id', referencedColumnName: 'id' })
    bloodGroup: BloodGroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
