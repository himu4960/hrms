/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, OneToMany } from 'typeorm';

import { BloodGroupTranslation } from './blood-group-translation.entity';
import { BaseEntity } from './base/base.entity';

import { ENTITY_BLOOD_GROUP } from './../shared/constant/database.constant';

/**
 * A BloodGroup.
 */
@Entity(ENTITY_BLOOD_GROUP)
export class BloodGroup extends BaseEntity {
    @Column({ name: 'company_id', nullable: true, unique: true })
    company_id?: number;

    @Column({ type: 'boolean', name: 'is_active', default: true })
    is_active: boolean;

    @Column({ type: 'boolean', name: 'is_deleted', default: false })
    is_deleted: boolean;

    @OneToMany(
        () => BloodGroupTranslation,
        translation => translation.bloodGroup,
        { cascade: ['insert'], eager: true },
    )
    translations?: BloodGroupTranslation[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
