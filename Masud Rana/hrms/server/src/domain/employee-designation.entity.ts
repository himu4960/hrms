/* eslint-disable @typescript-eslint/no-unused-vars */
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Designation } from './designation.entity';
import { Employee } from './employee.entity';

import { ENTITY_EMPLOYEE_DESIGNATION } from './../shared/constant/database.constant';

/**
 * A EmployeeDesignation.
 */
@Entity(ENTITY_EMPLOYEE_DESIGNATION)
export class EmployeeDesignation extends BaseEntity {
    @Column({ type: 'integer', name: 'designation_id' })
    designation_id: number;

    @Column({ type: 'integer', name: 'employee_id', select: false })
    employee_id: number;

    @ManyToOne(
        () => Designation,
        designation => designation.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'designation_id', referencedColumnName: 'id' })
    designation?: Designation;

    @ManyToOne(
        () => Employee,
        employee => employee.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'employee_id', referencedColumnName: 'id' })
    employee?: Employee;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
