/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, OneToOne } from 'typeorm';

import { ENTITY_SETTINGS_LEAVE_AVAILABILITY } from './../shared/constant/database.constant';

import { BaseEntity } from './base/base.entity';
import { Company } from './company.entity';

/**
 * Settings Leave Availability.
 */
@Entity(ENTITY_SETTINGS_LEAVE_AVAILABILITY)
export class SettingsLeaveAvailability extends BaseEntity {
    @Column({ type: 'boolean', name: 'leave_module_enabled', default: true })
    leave_module_enabled?: boolean;

    @Column({ type: 'longtext', name: 'deduct_weekends_from_leaves', nullable: true })
    deduct_weekends_from_leaves?: string;

    @Column({ type: 'boolean', name: 'employees_can_book_vacation', default: true })
    employees_can_book_vacation?: boolean;

    @Column({ type: 'boolean', name: 'show_deleted_employees_in_leave_approvals', default: false })
    show_deleted_employees_in_leave_approvals?: boolean;

    @Column({ type: 'boolean', name: 'schedulers_should_receive_leave_notifications', default: false })
    schedulers_should_receive_leave_notifications?: boolean;

    @Column({ type: 'boolean', name: 'allow_schedulers_to_approve_past_leave_requests', default: false })
    allow_schedulers_to_approve_past_leave_requests?: boolean;

    @Column({ type: 'integer', name: 'leave_must_be_booked_in_days_advance', default: 0 })
    leave_must_be_booked_in_days_advance?: number;

    @Column({ type: 'integer', name: 'max_staff_can_be_booked_at_once', default: 0 })
    max_staff_can_be_booked_at_once?: number;

    @Column({ type: 'boolean', name: 'employees_can_cancel_previously_approved_leave_requests', default: false })
    employees_can_cancel_previously_approved_leave_requests?: boolean;

    @Column({ type: 'boolean', name: 'availability_module_enabled', default: true })
    availability_module_enabled?: boolean;

    @Column({ type: 'boolean', name: 'employees_can_modify_unavailability', default: true })
    employees_can_modify_unavailability?: boolean;

    @Column({ type: 'boolean', name: 'availability_must_be_approved_by_management', default: false })
    availability_must_be_approved_by_management?: boolean;

    @Column({ type: 'boolean', name: 'allow_schedulers_to_approve_availability', default: false })
    allow_schedulers_to_approve_availability?: boolean;

    @Column({ type: 'integer', name: 'company_id', unique: true, select: false })
    company_id: number;

    @OneToOne(
        () => Company,
        company => company.settings_time_clock,
        { onDelete: 'CASCADE' }
    )
    @JoinColumn({ name: 'company_id' })
    company?: Company;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
