/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Language } from './language.entity';
import { Religion } from './religion.entity';

import { ENTITY_RELIGION_TRANSLATION } from './../shared/constant/database.constant';

/**
 * A ReligionTranslation.
 */
@Entity(ENTITY_RELIGION_TRANSLATION)
export class ReligionTranslation extends BaseEntity {
    @Column({ type: 'integer', name: 'religion_id' })
    religion_id: number;

    @Column({ name: 'language_code', length: 2 })
    language_code: string;

    @Column({ name: 'name', length: 50 })
    name: string;

    @ManyToOne(
        () => Language,
        (lang: Language) => lang.code,
    )
    @JoinColumn({ name: 'language_code', referencedColumnName: 'code' })
    language: Language;

    @ManyToOne(
        () => Religion,
        (religion: Religion) => religion.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'religion_id', referencedColumnName: 'id' })
    religion: Religion;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
