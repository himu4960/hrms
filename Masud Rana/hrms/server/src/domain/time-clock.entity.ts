/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne, OneToMany } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Company } from './company.entity';
import { Employee } from './employee.entity';
import { Branch } from './branch.entity';
import { TimeClockEvent } from './time-clock-event.entity';
import { Designation } from './designation.entity';

import { ENTITY_TIME_CLOCK } from './../shared/constant/database.constant';
import { WorkShift } from './work-shift.entity';

/**
 * A TimeClock.
 */
@Entity(ENTITY_TIME_CLOCK)
export class TimeClock extends BaseEntity {
    @Column({ name: 'work_shift_id', nullable: true, select: false })
    work_shift_id: number;

    @ManyToOne(
        () => WorkShift,
        workShift => workShift.id,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE', nullable: true },
    )
    @JoinColumn({ name: 'work_shift_id', referencedColumnName: 'id' })
    work_shift: WorkShift;

    @Column({ type: 'boolean', name: 'is_approved', default: false })
    is_approved: boolean;

    @Column({ type: 'integer', name: 'approved_by', nullable: true })
    approved_by: number;

    @Column({ type: 'bigint', name: 'approved_at', nullable: true })
    approved_at: number;

    @Column({ name: 'approved_note', nullable: true })
    approved_note: string;

    @Column({ type: 'bigint', name: 'clock_start_time', nullable: true })
    clock_start_time: number;

    @Column({ type: 'bigint', name: 'clock_end_time', nullable: true })
    clock_end_time: number;

    @Column({ type: 'bigint', name: 'pre_clock_time', nullable: true })
    pre_clock_time: number;

    @Column({ type: 'integer', name: 'total_clock_duration', nullable: true })
    total_clock_duration: number;

    @Column({ type: 'integer', name: 'total_break_duration', nullable: true })
    total_break_duration: number;

    @Column({ type: 'integer', name: 'total_break_taken', nullable: true })
    total_break_taken: number;

    @Column({ name: 'notes', nullable: true })
    notes: string;

    @Column({ name: 'is_running', default: false })
    is_running: boolean;

    @Column({ name: 'company_id', select: false })
    company_id: number;

    @Column({ name: 'employee_id', select: false })
    employee_id: number;

    @Column({ name: 'branch_id', select: false })
    branch_id: number;

    @Column({ name: 'designation_id', select: false, nullable: true })
    designation_id: number;

    @Column({ name: 'request_ip', nullable: true })
    request_ip: string;

    @Column({ type:"longtext", name: 'clockin_location', nullable: true })
    clockin_location: string;

    @Column({ type:"longtext", name: 'clockout_location', nullable: true })
    clockout_location: string;

    @Column({ type: 'float', precision: 14, scale: 10, name: 'clockin_latitude', nullable: true })
    clockin_latitude: number;

    @Column({ type: 'float', precision: 14, scale: 10, name: 'clockin_longitude', nullable: true })
    clockin_longitude: number;

    @Column({ type: 'float', precision: 14, scale: 10, name: 'clockout_latitude', nullable: true })
    clockout_latitude: number;

    @Column({ type: 'float', precision: 14, scale: 10, name: 'clockout_longitude', nullable: true })
    clockout_longitude: number;

    @ManyToOne(
        () => Designation,
        designation => designation.id,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE', eager: true, nullable: true },
    )
    @JoinColumn({ name: 'designation_id', referencedColumnName: 'id' })
    designation: Designation;

    @ManyToOne(
        () => Company,
        company => company.id,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
    company: Company;

    @ManyToOne(
        () => Employee,
        employee => employee.id,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
    )
    @JoinColumn({ name: 'employee_id', referencedColumnName: 'id' })
    employee: Employee;

    @ManyToOne(
        () => Branch,
        branch => branch.id,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
    )
    @JoinColumn({ name: 'branch_id', referencedColumnName: 'id' })
    branch: Branch;

    @OneToMany(
        () => TimeClockEvent,
        timeClockEvent => timeClockEvent.time_clock,
        { cascade: ['insert'] },
    )
    time_clock_events: TimeClockEvent[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
