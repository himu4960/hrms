/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne, ManyToMany, JoinTable } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Branch } from './branch.entity';
import { Company } from './company.entity';
import { Designation } from './designation.entity';
import { Employee } from './employee.entity';

import { ENTITY_WORKSHIFT, ENTITY_WORK_SHIFT_EMPLOYEE } from './../shared/constant/database.constant';

/**
 * A WorkShift.
 */
@Entity(ENTITY_WORKSHIFT)
export class WorkShift extends BaseEntity {
    @Column({ type: 'integer', name: 'company_id', select: false })
    company_id: number;

    @Column({ type: 'integer', name: 'branch_id', select: false })
    branch_id: number;

    @Column({ name: 'title', length: 255, nullable: true })
    title: string;

    @Column({ type: 'integer', name: 'designation_id', select: false })
    designation_id: number;

    @Column({ type: 'boolean', name: 'is_published', default: false })
    is_published: boolean;

    @Column({ type: 'bigint', name: 'published_at', nullable: true })
    published_at?: number;

    @Column({ type: 'integer', name: 'published_by', nullable: true })
    published_by?: number;

    @Column({ type: 'bigint', name: 'start_date' })
    start_date: number;

    @Column({ type: 'bigint', name: 'end_date' })
    end_date: number;

    @Column({ type: 'integer', name: 'start_time' })
    start_time: number;

    @Column({ type: 'integer', name: 'end_time' })
    end_time: number;

    @Column({ type: 'integer', name: 'total_work_shift_duration' })
    total_work_shift_duration: number;

    @Column({ type: 'mediumtext', name: 'skills', nullable: true })
    skills?: string;

    @Column({ type: 'longtext', name: 'note', nullable: true })
    note?: string;

    @ManyToOne(
        () => Company,
        (company: Company) => company.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
    company?: Company;

    @ManyToOne(
        () => Branch,
        (branch: Branch) => branch.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'branch_id', referencedColumnName: 'id' })
    branch?: Branch;

    @ManyToOne(
        () => Designation,
        (designation: Designation) => designation.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'designation_id', referencedColumnName: 'id' })
    designation?: Designation;

    @ManyToMany(
        () => Employee,
        employee => employee.work_shifts,
        { cascade: true, eager: true },
    )
    @JoinTable({
        name: ENTITY_WORK_SHIFT_EMPLOYEE,
        joinColumn: {
            name: 'work_shift_id',
            referencedColumnName: 'id',
        },
        inverseJoinColumn: {
            name: 'employee_id',
            referencedColumnName: 'id',
        },
    })
    employees: Employee[];
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
