/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column } from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { ENTITY_HOLIDAY } from '../shared/constant/database.constant';
/**
 * A Holiday.
 */
@Entity(ENTITY_HOLIDAY)
export class Holiday extends BaseEntity {
    @Column({ type: 'bigint', name: 'date' })
    date: number;

    @Column({ name: 'title', length: 255 })
    title: string;

    @Column({ name: 'description', nullable: true })
    description: string;

    @Column({ name: 'code', nullable: false })
    code: string;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
