/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { CustomFieldType } from './custom-field-type.entity';
import { Company } from './company.entity';

import { ENTITY_CUSTOM_FIELD } from './../shared/constant/database.constant';

/**
 * A CustomField.
 */
@Entity(ENTITY_CUSTOM_FIELD)
export class CustomField extends BaseEntity {
    @Column({ type: 'integer', name: 'company_id', select: false })
    company_id: number;

    @Column({ name: 'name', length: 255 })
    name: string;

    @Column({ name: 'custom_field_type_id' })
    custom_field_type_id: number;

    @Column({ name: 'permission_type', length: 255 })
    permission_type: string;

    @Column({ name: 'value', length: 800, nullable: true })
    value: string;

    @ManyToOne(
        () => Company,
        (company: Company) => company.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
    company?: Company;

    @ManyToOne(
        () => CustomFieldType,
        (customFieldType: CustomFieldType) => customFieldType.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'custom_field_type_id', referencedColumnName: 'id' })
    custom_field_type?: CustomFieldType;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
