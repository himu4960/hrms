/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column } from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { ENTITY_EMPLOYEE_TYPE } from './../shared/constant/database.constant';

/**
 * A EmployeeType.
 */
@Entity(ENTITY_EMPLOYEE_TYPE)
export class EmployeeType extends BaseEntity {
    @Column({ name: 'name', length: 255, nullable: true })
    name: string;

    @Column({ type: 'integer', name: 'company_id', nullable: true })
    company_id: number;

    @Column({ type: 'boolean', name: 'is_active', default: true })
    is_active: boolean;

    @Column({ type: 'boolean', name: 'is_deleted', default: false })
    is_deleted: boolean;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
