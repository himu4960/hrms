/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Company } from './company.entity';
import { TimeClock } from './time-clock.entity';
import { Designation } from './designation.entity';

import { ETimeClockEventType } from './enum/time-clock.enum';

import { ENTITY_TIME_CLOCK_EVENT } from './../shared/constant/database.constant';

/**
 * A TimeClockEvent.
 */

@Entity(ENTITY_TIME_CLOCK_EVENT)
export class TimeClockEvent extends BaseEntity {
    @Column({ type: 'enum', name: 'event_type', enum: ETimeClockEventType, default: ETimeClockEventType.BREAK })
    event_type: ETimeClockEventType;

    @Column({ type: 'bigint', name: 'break_start_time', nullable: true })
    break_start_time: number;

    @Column({ type: 'bigint', name: 'break_end_time', nullable: true })
    break_end_time: number;

    @Column({ type: 'integer', name: 'total_break_duration', nullable: true })
    total_break_duration: number;

    @Column({ type: 'integer', name: 'prev_position_id', nullable: true, select: false })
    prev_position_id: number;

    @Column({ type: 'integer', name: 'new_position_id', nullable: true, select: false })
    new_position_id: number;

    @Column({ name: 'note', nullable: true })
    note: string;

    @Column({ name: 'company_id', select: false })
    company_id: number;

    @Column({ name: 'time_clock_id', select: false })
    time_clock_id: number;

    @ManyToOne(
        () => Company,
        company => company.id,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
    company: Company;

    @ManyToOne(
        () => TimeClock,
        timeClock => timeClock.id,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
    )
    @JoinColumn({ name: 'time_clock_id' })
    time_clock: TimeClock;

    @ManyToOne(
        () => Designation,
        designation => designation.id,
        { onDelete: 'CASCADE', eager: true, nullable: true },
    )
    @JoinColumn({ name: 'prev_position_id', referencedColumnName: 'id' })
    prev_designation: Designation;

    @ManyToOne(
        () => Designation,
        designation => designation.id,
        { onDelete: 'CASCADE', eager: true, nullable: true },
    )
    @JoinColumn({ name: 'new_position_id', referencedColumnName: 'id' })
    new_designation: Designation;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
