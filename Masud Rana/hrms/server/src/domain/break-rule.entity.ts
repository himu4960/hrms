/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne, OneToMany } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { BreakRuleCondition } from './break-rule-condition.entity';
import { Company } from './company.entity';

import { ENTITY_BREAK_RULE } from './../shared/constant/database.constant';

/**
 * A BreakRule.
 */
@Entity(ENTITY_BREAK_RULE)
export class BreakRule extends BaseEntity {
    @Column({ name: 'company_id', select: false })
    company_id: number;

    @Column({ name: 'name', length: 255 })
    name: string;

    @Column({ name: 'rule_description', nullable: true })
    rule_description: string;

    @ManyToOne(
        () => Company,
        company => company.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
    company?: Company;

    @OneToMany(
        () => BreakRuleCondition,
        breakRuleCondition => breakRuleCondition.break_rule,
        { eager: true },
    )
    break_rule_conditions?: any[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
