/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Branch } from './branch.entity';
import { Company } from './company.entity';
import { Employee } from './employee.entity';
import { LeaveType } from './leave-type.entity';

import { ELeaveStatusType } from './enum/leave-status.enum';

import { ENTITY_LEAVE } from '../shared/constant/database.constant';
/**
 * A Leave.
 */
@Entity(ENTITY_LEAVE)
export class Leave extends BaseEntity {
    @Column({ type: 'integer', name: 'company_id' })
    company_id: number;

    @Column({ type: 'integer', name: 'employee_id' })
    employee_id: number;

    @Column({ type: 'integer', name: 'branch_id' })
    branch_id: number;

    @Column({ type: 'integer', name: 'leave_type_id', select: false })
    leave_type_id: number;

    @Column({
        name: 'status',
        type: 'enum',
        enum: ELeaveStatusType,
        default: ELeaveStatusType.PENDING,
    })
    status: ELeaveStatusType;

    @Column({ type: 'boolean', name: 'is_approved', default: false })
    is_approved: boolean;

    @Column({ type: 'boolean', name: 'is_review_requested', default: false })
    is_review_requested: boolean;

    @Column({ type: 'boolean', name: 'is_review_accepted', default: false })
    is_review_accepted: boolean;

    @Column({ type: 'integer', name: 'approved_by', nullable: true })
    approved_by?: number;

    @Column({ type: 'bigint', name: 'approved_at', nullable: true })
    approved_at?: number;

    @Column({ name: 'approved_note', nullable: true })
    approved_note?: string;

    @Column({ type: 'boolean', name: 'is_rejected', default: false })
    is_rejected: boolean;

    @Column({ type: 'integer', name: 'rejected_by', nullable: true })
    rejected_by?: number;

    @Column({ type: 'bigint', name: 'rejected_at', nullable: true })
    rejected_at?: number;

    @Column({ name: 'rejected_note', nullable: true })
    rejected_note?: string;

    @Column({ type: 'boolean', name: 'is_partial_day_allowed', default: false })
    is_partial_day_allowed: boolean;

    @Column({ type: 'bigint', name: 'start_date', nullable: true })
    start_date: number;

    @Column({ type: 'bigint', name: 'end_date', nullable: true })
    end_date: number;

    @Column({ type: 'integer', name: 'total_leave_duration_mins', nullable: true })
    total_leave_duration_mins: number;

    @Column({ name: 'comments', nullable: false })
    comments: string;

    @Column({ type: 'bigint', name: 'deleted_at', nullable: true })
    deleted_at?: number;

    @Column({ type: 'integer', name: 'deleted_by', nullable: true })
    deleted_by?: number;

    @Column({ type: 'boolean', name: 'is_deleted', default: false })
    is_deleted?: boolean;

    @ManyToOne(
        () => Company,
        (company: Company) => company.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
    company?: Company;

    @ManyToOne(
        () => Employee,
        (employee: Employee) => employee.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'employee_id', referencedColumnName: 'id' })
    employee?: Employee;

    @ManyToOne(
        () => Branch,
        (branch: Branch) => branch.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'branch_id', referencedColumnName: 'id' })
    branch?: Branch;

    @ManyToOne(
        () => LeaveType,
        (leaveType: LeaveType) => leaveType.id,
        { onDelete: 'CASCADE', eager: true },
    )
    @JoinColumn({ name: 'leave_type_id', referencedColumnName: 'id' })
    leaveType?: LeaveType;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
