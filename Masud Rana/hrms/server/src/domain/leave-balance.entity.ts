import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Company } from './company.entity';
import { Employee } from './employee.entity';
import { LeaveType } from './leave-type.entity';

import { ENTITY_LEAVE_BALANCE } from '../shared/constant/database.constant';

@Entity(ENTITY_LEAVE_BALANCE)
export class LeaveBalance extends BaseEntity {
    @Column({ type: 'integer', name: 'company_id' })
    company_id: number;

    @Column({ type: 'integer', name: 'employee_id' })
    employee_id: number;

    @Column({ type: 'integer', name: 'leave_type_id' })
    leave_type_id: number;

    @Column({ type: 'integer', name: 'allocated_leave_mins' })
    allocated_leave_mins: number;

    @Column({ type: 'integer', name: 'remaining_leave_mins' })
    remaining_leave_mins: number;

    @ManyToOne(
        () => Company,
        (company: Company) => company.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
    company?: Company;

    @ManyToOne(
        () => Employee,
        (employee: Employee) => employee.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'employee_id', referencedColumnName: 'id' })
    employee?: Employee;

    @ManyToOne(
        () => LeaveType,
        (leaveType: LeaveType) => leaveType.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'leave_type_id', referencedColumnName: 'id' })
    leaveType?: LeaveType;
}

