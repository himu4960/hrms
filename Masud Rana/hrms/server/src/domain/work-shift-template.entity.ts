/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Branch } from './branch.entity';
import { Company } from './company.entity';
import { Designation } from './designation.entity';

import { ENTITY_WORKSHIFT_TEMPLATE } from '../shared/constant/database.constant';

/**
 * A WorkShiftTemplate.
 */
@Entity(ENTITY_WORKSHIFT_TEMPLATE)
export class WorkShiftTemplate extends BaseEntity {
    @Column({ type: 'integer', name: 'company_id' })
    company_id: number;

    @Column({ type: 'integer', name: 'branch_id' })
    branch_id: number;

    @Column({ type: 'integer', name: 'designation_id' })
    designation_id: number;

    @Column({ type: 'integer', name: 'start_time' })
    start_time: number;

    @Column({ type: 'integer', name: 'end_time' })
    end_time: number;

    @Column({ type: 'integer', name: 'total_work_shift_duration' })
    total_work_shift_duration: number;

    @ManyToOne(
        () => Company,
        (company: Company) => company.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
    company?: Company;

    @ManyToOne(
        () => Branch,
        (branch: Branch) => branch.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'branch_id', referencedColumnName: 'id' })
    branch?: Branch;

    @ManyToOne(
        () => Designation,
        (designation: Designation) => designation.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'designation_id', referencedColumnName: 'id' })
    designation?: Designation;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
