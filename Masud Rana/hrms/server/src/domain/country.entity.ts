/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column } from 'typeorm';

import { BaseEntity } from './base/base.entity';

import { ENTITY_COUNTRY } from './../shared/constant/database.constant';

/**
 * A Country.
 */
@Entity(ENTITY_COUNTRY)
export class Country extends BaseEntity {
    @Column({ name: 'name', length: 100 })
    name: string;

    @Column({ name: 'code', length: 20 })
    code: string;

    @Column({ name: 'iso_code', length: 20, unique: true })
    iso_code: string;

    @Column({ type: 'boolean', name: 'is_active', default: true })
    is_active: boolean;

    @Column({ type: 'boolean', name: 'is_deleted', default: false })
    is_deleted: boolean;

    @Column({ type: 'boolean', name: 'is_default', nullable: true })
    is_default: boolean;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
