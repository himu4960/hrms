import { Entity, Column, OneToMany, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Exclude } from 'class-transformer';

import { User } from './user.entity';

import { ENTITY_USER_PASSWORD } from '../shared/constant/database.constant';

@Entity(ENTITY_USER_PASSWORD)
export class UserPassword {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column({ type: 'varchar' })
    @Exclude()
    password?: string;

    @Column({ name: 'user_id' })
    user_id: number;

    @ManyToOne(
        () => User,
        (user: User) => user.id,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
    )
    @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
    user?: User;
}
