import { PrimaryGeneratedColumn, Column } from 'typeorm';

export abstract class BaseEntity {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column({
        type: 'bigint',
        nullable: true,
        select: false,
        default: Date.now(),
    })
    created_at?: number;

    @Column({ type: 'bigint', nullable: true, select: true })
    updated_at?: number;

    /* Cannot add referrence with user entity because of seeding failed! */
    /* TypeError: Class extends value undefined is not a constructor or null */
    @Column({ nullable: true, select: false })
    updated_by?: number;

    @Column({ nullable: true, select: false })
    created_by?: number;
}
