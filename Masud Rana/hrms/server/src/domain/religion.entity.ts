/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, OneToMany } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { ReligionTranslation } from './religion-translation.entity';

import { ENTITY_RELIGION } from './../shared/constant/database.constant';

/**
 * A Religion.
 */
@Entity(ENTITY_RELIGION)
export class Religion extends BaseEntity {
    @Column({ type: 'integer', name: 'company_id', nullable: true })
    company_id?: number;

    @Column({ type: 'boolean', name: 'is_active', default: true })
    is_active: boolean;

    @Column({ type: 'boolean', name: 'is_deleted', default: false })
    is_deleted: boolean;

    @OneToMany(
        () => ReligionTranslation,
        translation => translation.religion,
    )
    translations?: ReligionTranslation[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
