/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, OneToOne } from 'typeorm';

import { ENTITY_SETTINGS_TIME_CLOCK } from './../shared/constant/database.constant';

import { BaseEntity } from './base/base.entity';
import { Company } from './company.entity';

import {
    ESettingsTimeClockDirection,
    ESettingsTimeClockCount,
    ESettingsTimeClockLateReminder,
    ESettingsTimeClockPastDays,
    ESettingsTimeClockResolution,
    ESettingsTimeClockRounding,
} from './enum/settings-time-clock.enum';

/**
 * Settings TimeClock.
 */

@Entity(ENTITY_SETTINGS_TIME_CLOCK)
export class SettingsTimeClock extends BaseEntity {
    @Column({ type: 'boolean', name: 'enable_auto_punch_out', default: true })
    enable_auto_punch_out?: boolean;

    @Column({ type: 'int', name: 'allow_employee_to_work_after_shift', default: ESettingsTimeClockCount.MIN_10 })
    allow_employee_to_work_after_shift?: number;

    @Column({ type: 'int', name: 'late_countable_time_start', default: ESettingsTimeClockCount.MIN_10 })
    late_countable_time_start?: number;

    @Column({
        type: 'int',
        name: 'pre_shift_punch_in_enabled_time',
        default: ESettingsTimeClockCount.MIN_10,
    })
    pre_shift_punch_in_enabled_time?: number;

    @Column({ type: 'boolean', name: 'time_clock_enabled', default: true })
    time_clock_enabled?: boolean;

    @Column({ type: 'boolean', name: 'employee_should_use_webcam', default: false })
    employee_should_use_webcam?: boolean;

    @Column({ type: 'boolean', name: 'allow_webcam_mobile', default: false })
    allow_webcam_mobile?: boolean;

    @Column({
        type: 'enum',
        name: 'webcam_resolution',
        enum: ESettingsTimeClockResolution,
        default: ESettingsTimeClockResolution.SMALL,
    })
    webcam_resolution?: ESettingsTimeClockResolution;

    @Column({ type: 'boolean', name: 'gps_data_required', default: false })
    gps_data_required?: boolean;

    @Column({ type: 'boolean', name: 'show_address_under_time_clock', default: false })
    show_address_under_time_clock?: boolean;

    @Column({ type: 'boolean', name: 'pre_clock_enabled', default: false })
    pre_clock_enabled?: boolean;

    @Column({ type: 'boolean', name: 'pre_clock_mandatory', default: false })
    pre_clock_mandatory?: boolean;

    @Column({ type: 'boolean', name: 'restrict_employee_clock_in_out', default: false })
    restrict_employee_clock_in_out?: boolean;

    @Column({ type: 'boolean', name: 'lock_timeclocking_to_locations', default: false })
    lock_timeclocking_to_locations?: boolean;

    @Column({ type: 'boolean', name: 'should_have_position_set', default: false })
    should_have_position_set?: boolean;

    @Column({ type: 'boolean', name: 'should_have_remote_site_set', default: false })
    should_have_remote_site_set?: boolean;

    @Column({ type: 'boolean', name: 'clock_out_notes_required', default: false })
    clock_out_notes_required?: boolean;

    @Column({ type: 'boolean', name: 'break_button_enabled', default: true })
    break_button_enabled?: boolean;

    @Column({ type: 'float', name: 'auto_clock_out_hour', default: 24 })
    auto_clock_out_hour?: number;

    @Column({ type: 'boolean', name: 'time_clock_tips_enabled', default: true })
    time_clock_tips_enabled?: boolean;

    @Column({ type: 'boolean', name: 'scheduled_details_enabled', default: false })
    scheduled_details_enabled?: boolean;

    @Column({ type: 'boolean', name: 'show_break_times_details', default: false })
    show_break_times_details?: boolean;

    @Column({ type: 'boolean', name: 'restrict_overlapping_timesheets', default: false })
    restrict_overlapping_timesheets?: boolean;

    @Column({
        type: 'enum',
        name: 'round_totals_to_nearest',
        enum: ESettingsTimeClockRounding,
        default: ESettingsTimeClockRounding.MIN_15,
    })
    round_totals_to_nearest?: ESettingsTimeClockRounding;

    @Column({
        type: 'enum',
        name: 'round_clock_in_times_to_nearest',
        enum: ESettingsTimeClockRounding,
        default: ESettingsTimeClockRounding.DONT_ROUND,
    })
    round_clock_in_times_to_nearest?: ESettingsTimeClockRounding;

    @Column({
        type: 'enum',
        name: 'clock_in_rounding_direction',
        enum: ESettingsTimeClockDirection,
        default: ESettingsTimeClockDirection.UP,
    })
    clock_in_rounding_direction?: ESettingsTimeClockDirection;

    @Column({
        type: 'enum',
        name: 'round_clock_out_times_to_nearest',
        enum: ESettingsTimeClockRounding,
        default: ESettingsTimeClockRounding.DONT_ROUND,
    })
    round_clock_out_times_to_nearest?: ESettingsTimeClockRounding;

    @Column({
        type: 'enum',
        name: 'clock_out_rounding_direction',
        enum: ESettingsTimeClockDirection,
        default: ESettingsTimeClockDirection.DOWN,
    })
    clock_out_rounding_direction?: ESettingsTimeClockDirection;

    @Column({ type: 'integer', name: 'allow_clock_in_before_timeframe', default: false })
    allow_clock_in_before_timeframe?: number;

    @Column({ type: 'integer', name: 'allow_clock_in_after_timeframe', default: false })
    allow_clock_in_after_timeframe?: number;

    @Column({ type: 'integer', name: 'allow_clock_out_before_timeframe', default: false })
    allow_clock_out_before_timeframe?: number;

    @Column({ type: 'integer', name: 'allow_clock_out_after_timeframe', default: false })
    allow_clock_out_after_timeframe?: number;

    @Column({ type: 'boolean', name: 'timesheet_employee_can_import', default: false })
    timesheet_employee_can_import?: boolean;

    @Column({ type: 'boolean', name: 'timesheet_employee_can_manually_add', default: true })
    timesheet_employee_can_manually_add?: boolean;

    @Column({ type: 'boolean', name: 'timesheet_empl_can_edit', default: true })
    timesheet_empl_can_edit?: boolean;

    @Column({ type: 'boolean', name: 'timesheet_prevent_employee_to_edit_approved_time', default: false })
    timesheet_prevent_employee_to_edit_approved_time?: boolean;

    @Column({
        type: 'enum',
        name: 'timesheet_allow_employee_to_add_past_x_days',
        enum: ESettingsTimeClockPastDays,
        default: ESettingsTimeClockPastDays.ANYTIME,
    })
    timesheet_allow_employee_to_add_past_x_days?: ESettingsTimeClockPastDays;

    @Column({ type: 'boolean', name: 'timesheet_employee_can_view_notes', default: false })
    timesheet_employee_can_view_notes?: boolean;

    @Column({ type: 'boolean', name: 'timesheet_notes_mandatory', default: false })
    timesheet_notes_mandatory?: boolean;

    @Column({ type: 'boolean', name: 'timesheet_employee_can_edit_without_reason', default: false })
    timesheet_employee_can_edit_without_reason?: boolean;

    @Column({
        type: 'enum',
        enum: ESettingsTimeClockLateReminder,
        name: 'notification_late_reminder_for_schedulers',
        default: ESettingsTimeClockLateReminder.BEGINNING_SHIFT,
    })
    notification_late_reminder_for_schedulers?: ESettingsTimeClockLateReminder;

    @Column({
        type: 'enum',
        enum: ESettingsTimeClockLateReminder,
        name: 'notification_late_reminder_for_managers',
        default: ESettingsTimeClockLateReminder.BEGINNING_SHIFT,
    })
    notification_late_reminder_for_managers?: ESettingsTimeClockLateReminder;

    @Column({
        type: 'enum',
        enum: ESettingsTimeClockLateReminder,
        name: 'notification_not_clocked_out_reminder_for_schedulers',
        default: ESettingsTimeClockLateReminder.BEGINNING_SHIFT,
    })
    notification_not_clocked_out_reminder_for_schedulers?: ESettingsTimeClockLateReminder;

    @Column({
        type: 'enum',
        enum: ESettingsTimeClockLateReminder,
        name: 'notification_not_clocked_out_reminder_for_managers',
        default: ESettingsTimeClockLateReminder.BEGINNING_SHIFT,
    })
    notification_not_clocked_out_reminder_for_managers?: ESettingsTimeClockLateReminder;

    @Column({ type: 'boolean', name: 'is_active', default: false })
    is_active?: boolean;

    @Column({ type: 'boolean', name: 'is_deleted', default: false })
    is_deleted?: boolean;

    @Column({ type: 'integer', name: 'company_id', unique: true, select: false })
    company_id: number;

    @OneToOne(
        () => Company,
        company => company.settings_time_clock,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id' })
    company?: Company;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
