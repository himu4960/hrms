/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { BreakRule } from './break-rule.entity';

import { EBreakRuleConditionType } from './enum/break-rule-condition.enum';

import { ENTITY_BREAK_RULE_CONDITIONS } from './../shared/constant/database.constant';

/**
 * A BreakRuleCondition.
 */
@Entity(ENTITY_BREAK_RULE_CONDITIONS)
export class BreakRuleCondition extends BaseEntity {
    @Column({ name: 'break_rule_id' })
    break_rule_id: number;

    @Column({
        name: 'type',
        type: 'enum',
        enum: EBreakRuleConditionType,
        default: EBreakRuleConditionType.UNPAID_BREAK,
    })
    type: EBreakRuleConditionType;

    @Column({ name: 'is_paid', default: false })
    is_paid: boolean;

    @Column({ type: 'integer', name: 'min_shift_length' })
    min_shift_length: number;

    @Column({ type: 'integer', name: 'max_shift_length' })
    max_shift_length: number;

    @Column({ type: 'integer', name: 'break_duration' })
    break_duration: number;

    @Column({ type: 'integer', name: 'from' })
    from: number;

    @Column({ type: 'integer', name: 'to' })
    to: number;

    @Column({ type: 'integer', name: 'break_starts_at' })
    break_starts_at: number;

    @ManyToOne(
        () => BreakRule,
        breakRule => breakRule.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'break_rule_id', referencedColumnName: 'id' })
    break_rule?: BreakRule;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
