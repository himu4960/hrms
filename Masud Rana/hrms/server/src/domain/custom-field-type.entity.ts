import { Entity, Column, PrimaryColumn } from 'typeorm';
import { ENTITY_CUSTOM_FIELD_TYPE } from './../shared/constant/database.constant';

@Entity(ENTITY_CUSTOM_FIELD_TYPE)
export class CustomFieldType {
    @PrimaryColumn({ name: 'id' })
    id: number;

    @Column({ name: 'name', length: 255 })
    name: string;

    @Column({ name: 'data_type', length: 255 })
    data_type: string;
}
