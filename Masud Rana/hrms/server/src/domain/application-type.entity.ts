/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { ENTITY_APPLICATION_TYPE } from './../shared/constant/database.constant';

/**
 * A ApplicationType.
 */
@Entity(ENTITY_APPLICATION_TYPE)
export class ApplicationType extends BaseEntity {
    @Column({ name: 'name', length: 255, nullable: true })
    name: string;

    @Column({ type: 'boolean', name: 'is_active', default: true })
    is_active: boolean;

    @Column({ type: 'boolean', name: 'is_deleted', default: false })
    is_deleted: boolean;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
