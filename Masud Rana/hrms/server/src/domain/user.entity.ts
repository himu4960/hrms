import { Entity, Column, OneToMany } from 'typeorm';
import { Exclude } from 'class-transformer';

import { BaseEntity } from './base/base.entity';
import { UserCompany } from './user-company.entity';

import { ENTITY_USER } from './../shared/constant/database.constant';

@Entity(ENTITY_USER)
export class User extends BaseEntity {
    @Column({ unique: true })
    username: string;

    @Column({ nullable: true })
    firstName?: string;

    @Column({ nullable: true })
    lastName?: string;

    @Column({ unique: true })
    email: string;

    @Column({ default: 'en' })
    langKey?: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    @Exclude()
    password?: string;

    @Column({ type: 'bigint', name: 'password_reset_at', nullable: true })
    password_reset_at?: number;

    @Column({ nullable: true })
    imageUrl?: string;

    @OneToMany(
        () => UserCompany,
        userCompany => userCompany.user,
        { eager: true },
    )
    user_companies?: UserCompany[];
}
