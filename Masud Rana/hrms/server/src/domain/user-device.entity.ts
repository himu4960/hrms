import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Company } from './company.entity';
import { Employee } from './employee.entity';
import { Role } from './role.entity';

import { ENTITY_USER_DEVICE } from '../shared/constant/database.constant';

@Entity(ENTITY_USER_DEVICE)
export class UserDevice extends BaseEntity {
    @Column({ type: 'integer', name: 'company_id' })
    company_id: number;

    @Column({ type: 'integer', name: 'employee_id' })
    employee_id: number;

    @Column({ type: 'integer', name: 'role_id' })
    role_id: number;

    @Column({ type: 'varchar', name: 'device_token' })
    device_token: string;

    @ManyToOne(
        () => Company,
        (company: Company) => company.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
    company?: Company;

    @ManyToOne(
        () => Employee,
        employee => employee.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'employee_id', referencedColumnName: 'id' })
    public employee?: Employee;

    @ManyToOne(
        () => Role,
        role => role.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'role_id', referencedColumnName: 'id' })
    public role?: Role;
}
