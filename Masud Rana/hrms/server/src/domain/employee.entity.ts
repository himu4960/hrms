/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, ManyToOne, JoinColumn, OneToMany, ManyToMany, JoinTable, Generated, OneToOne } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Company } from './company.entity';
import { Branch } from './branch.entity';
import { Leave } from './leave.entity';
import { EmployeeDesignation } from './employee-designation.entity';
import { WorkShift } from './work-shift.entity';
import { Skill } from './skill.entity';
import { UserCompany } from './user-company.entity';

import { ENTITY_EMPLOYEE, ENTITY_EMPLOYEE_SKILL } from './../shared/constant/database.constant';

/**
 * A Employee.
 */
@Entity(ENTITY_EMPLOYEE)
export class Employee extends BaseEntity {
    @Column({ name: 'code', length: 100, nullable: true })
    code?: string;

    @Column({ name: 'first_name', length: 50 })
    first_name: string;

    @Column({ name: 'middle_name', length: 50, nullable: true })
    middle_name?: string;

    @Column({ name: 'last_name', length: 50 })
    last_name: string;

    @Column({ type: 'bigint', name: 'dob', nullable: true })
    dob?: number;

    @Column({ name: 'email', length: 100 })
    email: string;

    @Column({ name: 'personal_email', length: 100, nullable: true })
    personal_email?: string;

    @Column({ name: 'phone', length: 30, nullable: true })
    phone?: string;

    @Column({ name: 'telephone', length: 30, nullable: true })
    telephone?: string;

    @Column({ name: 'nationality', length: 100, nullable: true })
    nationality?: string;

    @Column({ name: 'photo_url', nullable: true })
    photo_url?: string;

    @Column({ type: 'integer', name: 'gender_id', nullable: true, select: false })
    gender_id?: number;

    @Column({ type: 'integer', name: 'religion_id', nullable: true, select: false })
    religion_id?: number;

    @Column({ type: 'integer', name: 'marital_status_id', nullable: true, select: false })
    marital_status_id?: number;

    @Column({ type: 'integer', name: 'blood_group_id', nullable: true, select: false })
    blood_group_id?: number;

    @Column({ type: 'integer', name: 'branch_id', select: false })
    branch_id?: number;

    @Column({ name: 'company_id', select: false })
    company_id: number;

    @Column({ type: 'boolean', name: 'is_active', default: true })
    is_active?: boolean;

    @Column({ type: 'boolean', name: 'is_deleted', default: false })
    is_deleted?: boolean;

    @Column({ type: 'boolean', name: 'invitation_accepted', default: false })
    invitation_accepted?: boolean;

    @Column({ type: 'bigint', name: 'join_date', nullable: true })
    join_date?: number;

    @Column({ type: 'longtext', name: 'invitation_token', nullable: true, unique: true })
    @Generated('uuid')
    invitation_token?: string;

    @Column({ type: 'bigint', name: 'invitation_expiry', nullable: true })
    invitation_expiry?: number;

    @ManyToOne(
        () => Company,
        company => company.id,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
    company?: Company;

    @ManyToOne(
        () => Branch,
        branch => branch.id,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE', eager: true },
    )
    @JoinColumn({ name: 'branch_id', referencedColumnName: 'id' })
    branch?: Branch;

    @ManyToMany(
        () => WorkShift,
        workShift => workShift.employees,
    )
    work_shifts?: WorkShift[];

    @OneToMany(
        () => Leave,
        leave => leave.employee,
        { eager: true },
    )
    leaves?: Leave[];

    @OneToOne(
        () => UserCompany,
        userCompany => userCompany.employee,
    )
    user_company?: UserCompany;

    @ManyToMany(
        () => Skill,
        skill => skill.employees,
        { cascade: true },
    )
    @JoinTable({
        name: ENTITY_EMPLOYEE_SKILL,
        joinColumn: {
            name: 'employee_id',
            referencedColumnName: 'id',
        },
        inverseJoinColumn: {
            name: 'skill_id',
            referencedColumnName: 'id',
        },
    })
    skills?: Skill[];

    @OneToMany(
        () => EmployeeDesignation,
        employeeDesignation => employeeDesignation.employee,
        { cascade: ['insert', 'update'] },
    )
    employee_designations?: EmployeeDesignation[];
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
