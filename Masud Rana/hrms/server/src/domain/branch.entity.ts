/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne, OneToMany } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { BreakRule } from './break-rule.entity';
import { Company } from './company.entity';
import { Designation } from './designation.entity';
import { Employee } from './employee.entity';
import { TimeZone } from './time-zone.entity';

import { ENTITY_BRANCH } from './../shared/constant/database.constant';

/**
 * A Branch.
 */
@Entity(ENTITY_BRANCH)
export class Branch extends BaseEntity {
    @Column({ name: 'name', length: 255 })
    name: string;

    @Column({ name: 'phone', length: 20, nullable: true })
    phone?: string;

    @Column({ name: 'email', length: 255, nullable: true })
    email?: string;

    @Column({ name: 'address_line_1', nullable: true })
    address_line1?: string;

    @Column({ name: 'address_line_2', nullable: true })
    address_line2?: string;

    @Column({ name: 'address_city', nullable: true })
    address_city?: string;

    @Column({ name: 'address_province', nullable: true })
    address_province?: string;

    @Column({ name: 'address_country', nullable: true })
    address_country?: string;

    @Column({ type: 'float', name: 'address_latitude', nullable: true })
    address_latitude?: number;

    @Column({ type: 'float', name: 'address_longitude', nullable: true })
    address_longitude?: number;

    @Column({ type: 'boolean', name: 'is_active', nullable: true })
    is_active?: boolean;

    @Column({ name: 'company_id', select: false })
    company_id: number;

    @Column({ name: 'break_rule_id', select: false })
    break_rule_id: number;

    @Column({ name: 'time_zone_id', select: false })
    time_zone_id: number;

    @Column({ name: 'is_default', default: false })
    is_default: boolean;

    @Column({ name: 'is_primary', default: false })
    is_primary: boolean;

    @OneToMany(
        () => Employee,
        employee => employee.branch,
    )
    employees?: Employee[];

    @OneToMany(
        () => Designation,
        designation => designation.branch,
        { eager: true },
    )
    designations?: Designation[];

    @ManyToOne(
        () => Company,
        (company: Company) => company.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
    company?: Company;

    @ManyToOne(
        () => BreakRule,
        (breakRule: BreakRule) => breakRule.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'break_rule_id', referencedColumnName: 'id' })
    break_rule?: BreakRule;

    @ManyToOne(
        () => TimeZone,
        (timeZone: TimeZone) => timeZone.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'time_zone_id', referencedColumnName: 'id' })
    time_zone?: TimeZone;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
