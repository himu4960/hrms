/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, OneToOne, ManyToOne } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Company } from './company.entity';
import { Country } from './country.entity';
import { TimeZone } from './time-zone.entity';

import { ENTITY_SETTINGS_APPLICATION } from './../shared/constant/database.constant';

/**
 * A SettingsApplication.
 */
@Entity(ENTITY_SETTINGS_APPLICATION)
export class SettingsApplication extends BaseEntity {
    @Column({ type: 'integer', name: 'company_id', unique: true, select: false })
    company_id: number;

    @Column({ name: 'locale_country_id' })
    locale_country_id: number;

    @Column({ name: 'locale_timezone_id', default: 73 })
    locale_timezone_id: number;

    @Column({ type: 'integer', name: 'office_working_hour', default: 8 })
    office_working_hours?: number;

    @Column({ name: 'locale_default_lang_code', length: 20, default: 'en' })
    locale_default_lang_code?: string;

    @Column({ name: 'locale_date_format', length: 20, default: 'mm/dd/yy' })
    locale_date_format?: string;

    @Column({ type: 'integer', name: 'locale_week_number', default: 0 })
    locale_week_number?: number;

    @Column({ type: 'boolean', name: 'notification_email_enabled', default: true })
    notification_email_enabled?: boolean;

    @Column({ type: 'boolean', name: 'notification_sms_enabled', default: true })
    notification_sms_enabled?: boolean;

    @Column({ type: 'boolean', name: 'notification_mobile_push_enabled', default: true })
    notification_mobile_push_enabled?: boolean;

    @Column({ type: 'boolean', name: 'notification_force_email_enabled', default: false })
    notification_force_email_enabled?: boolean;

    @Column({ type: 'boolean', name: 'notification_birthday_enabled', default: false })
    notification_birthday_enabled?: boolean;

    @Column({ type: 'boolean', name: 'notification_birthday_card_enabled', default: false })
    notification_birthday_card_enabled?: boolean;

    @Column({ type: 'boolean', name: 'training_module_enabled', default: true })
    training_module_enabled?: boolean;

    @Column({ type: 'boolean', name: 'training_download_pdf_enabled', default: false })
    training_download_pdf_enabled?: boolean;

    @Column({ name: 'employee_name_format', length: 20, default: 'nickname' })
    employee_name_format?: string;

    @Column({ type: 'boolean', name: 'employee_can_view_report', default: true })
    employee_can_view_report?: boolean;

    @Column({ type: 'boolean', name: 'employee_can_edit_profile', default: true })
    employee_can_edit_profile?: boolean;

    @Column({ type: 'boolean', name: 'employee_can_view_staff_gallery', default: true })
    employee_can_view_staff_gallery?: boolean;

    @Column({ type: 'boolean', name: 'employee_can_view_staff_details', default: true })
    employee_can_view_staff_details?: boolean;

    @Column({ type: 'boolean', name: 'employee_mgnt_can_view_eid', default: false })
    employee_mgnt_can_view_eid?: boolean;

    @Column({ type: 'boolean', name: 'employee_can_view_whos_on', default: true })
    employee_can_view_whos_on?: boolean;

    @Column({ type: 'boolean', name: 'employee_can_request_account', default: false })
    employee_can_request_account?: boolean;

    @Column({ name: 'employee_security_question', length: 255, nullable: true })
    employee_security_question?: string;

    @Column({ name: 'employee_security_answer', length: 255, nullable: true })
    employee_security_answer?: string;

    @Column({ type: 'boolean', name: 'employee_can_view_uploaded_files', default: false })
    employee_can_view_uploaded_files?: boolean;

    @Column({ type: 'boolean', name: 'comm_message_wall_enabled', default: true })
    comm_message_wall_enabled?: boolean;

    @Column({ type: 'boolean', name: 'comm_employee_can_post_msg', default: false })
    comm_employee_can_post_msg?: boolean;

    @Column({ type: 'boolean', name: 'comm_employee_can_comment', default: true })
    comm_employee_can_comment?: boolean;

    @Column({ type: 'boolean', name: 'comm_messaging_module_enabled', default: true })
    comm_messaging_module_enabled?: boolean;

    @Column({ type: 'boolean', name: 'comm_employee_can_send_private_msg', default: true })
    comm_employee_can_send_private_msg?: boolean;

    @Column({ type: 'boolean', name: 'comm_employee_can_reply_received_msg', default: true })
    comm_employee_can_reply_received_msg?: boolean;

    @Column({ type: 'boolean', name: 'comm_employee_can_ping_users', default: true })
    comm_employee_can_ping_users?: boolean;

    @Column({ type: 'boolean', name: 'auth_show_logo', default: true })
    auth_show_logo?: boolean;

    @Column({ type: 'integer', name: 'auth_auto_logout_time', default: 0 })
    auth_auto_logout_time?: number;

    @Column({ type: 'integer', name: 'auth_password_expiry_time', default: 30 })
    auth_password_expiry_time?: number;

    @OneToOne(
        () => Company,
        company => company.settings_application,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id' })
    company?: Company;

    @ManyToOne(
        () => Country,
        country => country.id,
        { cascade: true },
    )
    @JoinColumn({ name: 'locale_country_id', referencedColumnName: 'id' })
    country?: Country;

    @ManyToOne(
        () => TimeZone,
        timeZone => timeZone.id,
        { cascade: true },
    )
    @JoinColumn({ name: 'locale_timezone_id', referencedColumnName: 'id' })
    time_zone?: TimeZone;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
