/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Language } from './language.entity';
import { Gender } from './gender.entity';

import { ENTITY_GENDER_TRANSLATION } from './../shared/constant/database.constant';

/**
 * A GenderTranslation.
 */
@Entity(ENTITY_GENDER_TRANSLATION)
export class GenderTranslation extends BaseEntity {
    @Column({ type: 'integer', name: 'gender_id' })
    gender_id: number;

    @Column({ name: 'language_code', length: 2 })
    language_code: string;

    @Column({ name: 'name', length: 50 })
    name: string;

    @ManyToOne(
        () => Gender,
        (gender: Gender) => gender.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'gender_id', referencedColumnName: 'id' })
    gender: Gender;

    @ManyToOne(
        () => Language,
        (lang: Language) => lang.code,
    )
    @JoinColumn({ name: 'language_code', referencedColumnName: 'code' })
    language: Language;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
