import { CreateDateColumn, Entity, UpdateDateColumn } from 'typeorm';
import { CasbinRule } from 'typeorm-adapter';

import { ENTITY_CASBIN } from './../shared/constant/database.constant';

@Entity(ENTITY_CASBIN)
export class Casbin extends CasbinRule {
    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;
}
