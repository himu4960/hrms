/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, Index } from 'typeorm';

import { BaseEntity } from './base/base.entity';

import { ENTITY_GEO_LOCATION} from '../shared/constant/database.constant';

/**
 * A Geo Location.
 */
@Entity(ENTITY_GEO_LOCATION)
export class GeoLocation extends BaseEntity {
    @Index()
    @Column({ type: 'varchar', name: 'ip', length: 15, nullable: false })
    ip?: string;

    @Column({ type: 'longtext', name: 'hostname', nullable: true })
    hostname?: string;

    @Column({ type: 'longtext', name: 'type', nullable: true })
    type?: string;

    @Column({ type: 'longtext', name: 'continent_code', nullable: true })
    continent_code?: string;

    @Column({ type: 'longtext', name: 'continent_name', nullable: true })
    continent_name?: string;

    @Column({ type: 'longtext', name: 'country_code', nullable: true })
    country_code?: string;

    @Column({ type: 'longtext', name: 'country_name', nullable: true })
    country_name?: string;

    @Column({ type: 'longtext', name: 'region_code', nullable: true })
    region_code?: string;

    @Column({ type: 'longtext', name: 'region_name', nullable: true })
    region_name?: string;

    @Column({ type: 'longtext', name: 'city', nullable: true })
    city?: string;

    @Column({ type: 'longtext', name: 'zip', nullable: true })
    zip?: string;

    @Column({ type: 'integer', name: 'latitude', nullable: true })
    latitude?: number;

    @Column({ type: 'integer', name: 'longitude', nullable: true })
    longitude?: number;

    @Column({ type: 'integer', name: 'geoname_id', nullable: true })
    geoname_id?: number;

    @Column({ type: 'longtext', name: 'capital', nullable: true })
    capital?: string;

    @Column({ type: 'longtext', name: 'country_flag', nullable: true })
    country_flag?: string;

    @Column({ type: 'longtext', name: 'country_flag_emoji', nullable: true })
    country_flag_emoji?: string;

    @Column({ type: 'longtext', name: 'country_flag_emoji_unicode', nullable: true })
    country_flag_emoji_unicode?: string;

    @Column({ type: 'longtext', name: 'calling_code', nullable: true })
    calling_code?: string;

    @Column({ type: 'boolean', name: 'is_eu', nullable: true })
    is_eu?: boolean;

    @Column({ type: 'longtext', name: 'time_zone_id', nullable: true })
    time_zone_id?: string;

    @Column({ type: 'longtext', name: 'time_zone_current_time', nullable: true })
    time_zone_current_time?: string;

    @Column({ type: 'integer', name: 'time_zone_gmt_offset', nullable: true })
    time_zone_gmt_offset?: number;

    @Column({ type: 'boolean', name: 'security_is_proxy', nullable: true })
    security_is_proxy?: boolean;

    @Column({ type: 'longtext', name: 'security_proxy_type', nullable: true })
    security_proxy_type?: string;

    @Column({ type: 'boolean', name: 'security_is_crawler', nullable: true })
    security_is_crawler?: boolean;

    @Column({ type: 'longtext', name: 'security_crawler_name', nullable: true })
    security_crawler_name?: string;

    @Column({ type: 'longtext', name: 'security_crawler_type', nullable: true })
    security_crawler_type?: string;

    @Column({ type: 'boolean', name: 'security_is_tor', nullable: true })
    security_is_tor?: boolean;

    @Column({ type: 'longtext', name: 'security_threat_level', nullable: true })
    security_threat_level?: string;

    @Column({ type: 'longtext', name: 'security_threat_types', nullable: true })
    security_threat_types?: string;
}
