/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm';

import { ENTITY_ANNOUNCEMENT } from './../shared/constant/database.constant';

import { BaseEntity } from './base/base.entity';
import { Company } from './company.entity';
import { Branch } from './branch.entity';

/**
 * A Announcement.
 */
@Entity(ENTITY_ANNOUNCEMENT)
export class Announcement extends BaseEntity {
    @Column({ name: 'title', length: 100 })
    title: string;

    @Column({ type: 'longtext', name: 'description', nullable: true })
    description: string;

    @Column({ type: 'integer', name: 'company_id', select: false })
    company_id: number;

    @Column({ type: 'integer', name: 'branch_id', nullable: true, select: false })
    branch_id: number;

    @ManyToOne(
        () => Company,
        company => company.id,
        {
            onDelete: 'CASCADE',
        },
    )
    @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
    company: Company;

    @ManyToOne(
        () => Branch,
        branch => branch.id,
        { nullable: true },
    )
    @JoinColumn({ name: 'branch_id', referencedColumnName: 'id' })
    branch: Branch;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
