/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne, OneToMany } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { ApplicationType } from './application-type.entity';
import { UserCompany } from './user-company.entity';

import { ENTITY_ROLE } from './../shared/constant/database.constant';

/**
 * A Role.
 */
@Entity(ENTITY_ROLE)
export class Role extends BaseEntity {
    @Column({ name: 'name', length: 50 })
    name: string;

    @Column({ type: 'text', name: 'menus', nullable: true })
    menus: string;

    @Column({ type: 'boolean', name: 'is_active', default: true })
    is_active: boolean;

    @Column({ type: 'boolean', name: 'is_deleted', default: false })
    is_deleted: boolean;

    @Column({ type: 'boolean', name: 'is_employee', default: false })
    is_employee: boolean;

    @Column({ name: 'application_type_id' })
    application_type_id: number;

    @ManyToOne(
        () => ApplicationType,
        (applicationType: ApplicationType) => applicationType.id,
    )
    @JoinColumn({ name: 'application_type_id', referencedColumnName: 'id' })
    applicationType: ApplicationType;

    @OneToMany(
        () => UserCompany,
        userCompany => userCompany.role,
    )
    public user_companies?: UserCompany[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
