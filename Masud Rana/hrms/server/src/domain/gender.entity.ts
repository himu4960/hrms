/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, OneToMany } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { GenderTranslation } from './gender-translation.entity';

import { ENTITY_GENDER } from './../shared/constant/database.constant';

/**
 * A Gender.
 */
@Entity(ENTITY_GENDER)
export class Gender extends BaseEntity {
    @Column({ type: 'integer', name: 'company_id', nullable: true })
    company_id?: number;

    @Column({ type: 'boolean', name: 'is_active', default: true })
    is_active: boolean;

    @Column({ type: 'boolean', name: 'is_deleted', default: false })
    is_deleted: boolean;

    @OneToMany(
        () => GenderTranslation,
        translation => translation.gender,
    )
    translations?: GenderTranslation[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
