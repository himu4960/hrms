/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, ManyToOne, JoinColumn, OneToOne } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Company } from './company.entity';
import { Role } from './role.entity';
import { User } from './user.entity';
import { Employee } from './employee.entity';

import { ENTITY_USER_COMPANY } from './../shared/constant/database.constant';

/**
 * A UserCompany.
 */
@Entity(ENTITY_USER_COMPANY)
export class UserCompany extends BaseEntity {
    @Column({ type: 'boolean', name: 'is_active', default: true })
    is_active?: boolean;

    @Column({ type: 'boolean', name: 'is_deleted', default: false })
    is_deleted?: boolean;

    @Column({ name: 'is_owner', default: false })
    is_owner: boolean;

    @Column({ name: 'user_id', select: false })
    user_id: number;

    @Column({ name: 'company_id', select: false })
    company_id: number;

    @Column({ name: 'employee_id', select: false })
    employee_id: number;

    @Column({ name: 'role_id', select: false })
    role_id: number;

    @Column({ type: 'varchar', nullable: true })
    refresh_token?: string;

    @ManyToOne(
        () => User,
        user => user.user_companies,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
    )
    @JoinColumn({ name: 'user_id' })
    public user?: User;

    @ManyToOne(
        () => Company,
        company => company.user_companies,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE', eager: true },
    )
    @JoinColumn({ name: 'company_id' })
    public company?: Company;

    @ManyToOne(
        () => Role,
        role => role.user_companies,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE', eager: true },
    )
    @JoinColumn({ name: 'role_id' })
    public role?: Role;

    @OneToOne(
        () => Employee,
        employee => employee.user_company,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE', eager: true },
    )
    @JoinColumn({ name: 'employee_id' })
    public employee?: Employee;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
