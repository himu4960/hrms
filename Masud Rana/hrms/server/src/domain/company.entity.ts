/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne, OneToMany, OneToOne } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { CompanyType } from './company-type.entity';
import { ApplicationType } from './application-type.entity';
import { UserCompany } from './user-company.entity';
import { SettingsTimeClock } from './settings-time-clock.entity';
import { SettingsApplication } from './settings-application.entity';
import { SettingsShiftPlanning } from './settings-shift-planning.entity';

import { ENTITY_COMPANY } from './../shared/constant/database.constant';
import { SettingsLeaveAvailability } from './settings-leave-availability.entity';

/**
 * A Company.
 */
@Entity(ENTITY_COMPANY)
export class Company extends BaseEntity {
    @Column({ name: 'name', length: 255 })
    name: string;

    @Column({ name: 'email', length: 255 })
    email: string;

    @Column({ name: 'phone', length: 20, nullable: true })
    phone?: string;

    @Column({ name: 'address', length: 500, nullable: true })
    address?: string;

    @Column({ name: 'logo_url_large', nullable: true })
    logo_url_large?: string;

    @Column({ name: 'logo_url_medium', nullable: true })
    logo_url_medium?: string;

    @Column({ name: 'logo_url_small', nullable: true })
    logo_url_small?: string;

    @Column({ type: 'boolean', name: 'status', default: false })
    status?: boolean;

    @ManyToOne(
        () => CompanyType,
        (companyType: CompanyType) => companyType.id,
    )
    @JoinColumn({ name: 'company_type_id', referencedColumnName: 'id' })
    companyType?: CompanyType;

    @Column({ name: 'company_type_id', select: false })
    company_type_id?: number;

    @ManyToOne(
        () => ApplicationType,
        (applicationType: ApplicationType) => applicationType.id,
    )
    @JoinColumn({ name: 'application_type_id', referencedColumnName: 'id' })
    applicationType?: ApplicationType;

    @Column({ name: 'application_type_id', select: false })
    application_type_id?: number;

    @OneToMany(
        () => UserCompany,
        userCompany => userCompany.company,
    )
    public user_companies?: UserCompany[];

    @OneToOne(
        () => SettingsTimeClock,
        timeClock => timeClock.company,
        { eager: true },
    )
    settings_time_clock?: SettingsTimeClock;

    @OneToOne(
        () => SettingsApplication,
        application => application.company,
        { eager: true },
    )
    settings_application?: SettingsApplication;

    @OneToOne(
        () => SettingsShiftPlanning,
        shiftPlanning => shiftPlanning.company,
        { eager: true },
    )
    settings_shift_planning?: SettingsShiftPlanning;

    @OneToOne(
        () => SettingsLeaveAvailability,
        leaveAvailability => leaveAvailability.company,
        { eager: true },
    )
    settings_leave_availability?: SettingsLeaveAvailability;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
