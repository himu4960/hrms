/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column } from 'typeorm';

import { BaseEntity } from './base/base.entity';

import { ENTITY_PERMISSION_TYPE } from './../shared/constant/database.constant';

/**
 * A PermissionType.
 */
@Entity(ENTITY_PERMISSION_TYPE)
export class PermissionType extends BaseEntity {
    @Column({ name: 'name', length: 50 })
    name: string;

    @Column({ name: 'key', length: 50, nullable: true })
    key: string;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
