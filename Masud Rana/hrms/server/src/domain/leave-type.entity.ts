/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne, IsNull } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Company } from './company.entity';

import { ENTITY_LEAVE_TYPE } from '../shared/constant/database.constant';
/**
 * A LeaveType.
 */
@Entity(ENTITY_LEAVE_TYPE)
export class LeaveType extends BaseEntity {
    @Column({ type: 'integer', name: 'company_id', select: false })
    company_id: number;

    @Column({ name: 'name', length: 255 })
    name: string;

    @Column({ type: 'boolean', name: 'is_partial_day_request_allowed', default: true })
    is_partial_day_request_allowed: boolean;

    @Column({ type: 'integer', name: 'max_days_allowed', nullable: true })
    max_days_allowed: number;

    @Column({ type: 'boolean', name: 'is_max_employee_allowed_enabled', default: false })
    is_max_employee_allowed_enabled: boolean;

    @Column({ type: 'integer', name: 'max_employee_allowed', nullable: true })
    max_employee_allowed: number;

    @Column({ type: 'boolean', name: 'is_leave_in_advance_days_enabled', default: false })
    is_leave_in_advance_days_enabled: boolean;

    @Column({ type: 'integer', name: 'leave_in_advance_days', nullable: true })
    leave_in_advance_days: number;

    @Column({ type: 'boolean', name: 'auto_assign_new_employee_enabled', default: false })
    auto_assign_new_employee_enabled: boolean;

    @Column({ name: 'description', nullable: true })
    description: string;

    @ManyToOne(
        () => Company,
        (company: Company) => company.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
    company?: Company;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
