/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, OneToOne } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Company } from './company.entity';

import { ENTITY_SETTINGS_SHIFT_PLANNING } from './../shared/constant/database.constant';

import {
    EShiftPlanningAllowedUsers,
    EShiftPlanningDates,
    EShiftPlanningDays,
    EShiftPlanningDropTimeLimit,
    EShiftPlanningEachMinutes,
    EShiftPlanningEnabledRequirement,
    EShiftPlanningSchedules,
    EShiftPlanningShifts,
} from './enum/settings-shift-planning.enum';

/**
 * A SettingsShiftPlanning.
 */
@Entity(ENTITY_SETTINGS_SHIFT_PLANNING)
export class SettingsShiftPlanning extends BaseEntity {
    @Column({ type: 'boolean', name: 'sorting_by_address_enabled', default: false })
    sorting_by_address_enabled?: boolean;

    @Column({ type: 'boolean', name: 'show_locations_in_shifts', default: true })
    show_locations_in_shifts?: boolean;

    @Column({ type: 'boolean', name: 'show_overnight_shifts', default: false })
    show_overnight_shifts?: boolean;

    @Column({
        type: 'enum',
        name: 'shift_planning_start_day',
        enum: EShiftPlanningDays,
        default: EShiftPlanningDays.SUNDAY,
    })
    shift_planning_start_day?: EShiftPlanningDays;

    @Column({ type: 'boolean', name: 'use_draft_publish_schedule_method', default: true })
    use_draft_publish_schedule_method?: boolean;

    @Column({ type: 'boolean', name: 'use_shift_approval', default: false })
    use_shift_approval?: boolean;

    @Column({ type: 'boolean', name: 'shift_planning_tasks_enabled', default: false })
    shift_planning_tasks_enabled?: boolean;

    @Column({ type: 'boolean', name: 'longer_than_24_hours_shifts_allowed', default: false })
    longer_than_24hours_shifts_allowed?: boolean;

    @Column({ type: 'boolean', name: 'past_date_shifts_allowed', default: true })
    past_date_shifts_allowed?: boolean;

    @Column({
        type: 'enum',
        name: 'notes_permissions_allowed_users',
        enum: EShiftPlanningAllowedUsers,
        default: EShiftPlanningAllowedUsers.MANAGERS_AND_SUPERVISORS,
    })
    notes_permissions_allowed_users?: EShiftPlanningAllowedUsers;

    @Column({ type: 'boolean', name: 'schedule_automatic_conflicts_allowed', default: false })
    schedule_automatic_conflicts_allowed?: boolean;

    @Column({ type: 'boolean', name: 'show_pending_leave_request', default: false })
    show_pending_leave_request?: boolean;

    @Column({
        type: 'enum',
        name: 'shift_notes_visibility_allowed_users',
        enum: EShiftPlanningAllowedUsers,
        default: EShiftPlanningAllowedUsers.ALL_USERS,
    })
    shift_notes_visibility_allowed_users?: EShiftPlanningAllowedUsers;

    @Column({
        type: 'enum',
        name: 'employee_can_view_shifts_in_advance',
        enum: EShiftPlanningShifts,
        default: EShiftPlanningShifts.ALL_SHIFT,
    })
    employee_can_view_shifts_in_advance?: EShiftPlanningShifts;

    @Column({
        type: 'enum',
        name: 'show_costing_data_in_scheduler',
        enum: EShiftPlanningSchedules,
        default: EShiftPlanningSchedules.HOURS_AND_STAFF_COST,
    })
    show_costing_data_in_scheduler?: EShiftPlanningSchedules;

    @Column({ type: 'boolean', name: 'employee_can_view_all_schedules', default: false })
    employee_can_view_all_schedules?: boolean;

    @Column({ type: 'boolean', name: 'employee_can_see_assigned_staffs', default: true })
    employee_can_see_assigned_staffs?: boolean;

    @Column({
        type: 'enum',
        name: 'max_working_days_in_a_row',
        enum: EShiftPlanningDates,
        default: EShiftPlanningDates.UNLIMITED,
    })
    max_working_days_in_a_row?: EShiftPlanningDates;

    @Column({ type: 'boolean', name: 'show_12_am_as_midnight', default: false })
    show_12am_as_midnight?: boolean;

    @Column({ type: 'boolean', name: 'on_call_management_enabled', default: false })
    on_call_management_enabled?: boolean;

    @Column({ type: 'boolean', name: 'employee_can_see_co_worker_vacations', default: false })
    employee_can_see_co_worker_vacations?: boolean;

    @Column({ type: 'boolean', name: 'shift_loading_indicator_enabled', default: false })
    shift_loading_indicator_enabled?: boolean;

    @Column({ type: 'boolean', name: 'show_employee_skills_in_scheduler', default: false })
    show_employee_skills_in_scheduler?: boolean;

    @Column({
        type: 'enum',
        name: 'required_skills_on_shift',
        enum: EShiftPlanningEnabledRequirement,
        default: EShiftPlanningEnabledRequirement.OFF,
    })
    required_skills_on_shift?: EShiftPlanningEnabledRequirement;

    @Column({ type: 'boolean', name: 'employee_can_release_shifts', default: true })
    employee_can_release_shifts?: boolean;

    @Column({ type: 'boolean', name: 'employee_can_trade_shifts', default: true })
    employee_can_trade_shifts?: boolean;

    @Column({ type: 'boolean', name: 'same_day_trades_allowed', default: true })
    same_day_trades_allowed?: boolean;

    @Column({ type: 'boolean', name: 'shift_overlapping_disallowed', default: false })
    shift_overlapping_disallowed?: boolean;

    @Column({ type: 'boolean', name: 'shift_trades_between_positions_disabled', default: false })
    shift_trades_between_positions_disabled?: boolean;

    @Column({ type: 'boolean', name: 'shift_trades_manager_must_release_before_request', default: false })
    shift_trades_manager_must_release_before_request?: boolean;

    @Column({ type: 'boolean', name: 'shift_trades_manager_must_release_after_request', default: false })
    shift_trades_manager_must_release_after_request?: boolean;

    @Column({ type: 'boolean', name: 'shift_acknowledgment_enabled', default: false })
    shift_acknowledgment_enabled?: boolean;

    @Column({ type: 'boolean', name: 'employee_can_see_co_worker_shifts', default: true })
    employee_can_see_co_worker_shifts?: boolean;

    @Column({ type: 'boolean', name: 'employee_can_drop_shifts', default: false })
    employee_can_drop_shifts?: boolean;

    @Column({ type: 'boolean', name: 'employee_requested_open_shifts_auto_approved', default: false })
    employee_requested_open_shifts_auto_approved?: boolean;

    @Column({ type: 'boolean', name: 'employee_pick_up_shifts_with_overtime_enabled', default: false })
    employee_pick_up_shifts_with_overtime_enabled?: boolean;

    @Column({
        type: 'enum',
        name: 'employee_cannot_be_scheduled_within_time',
        enum: EShiftPlanningEachMinutes,
        default: EShiftPlanningEachMinutes.NONE,
    })
    employee_cannot_be_scheduled_within_time?: EShiftPlanningEachMinutes;

    @Column({ type: 'boolean', name: 'employee_can_be_scheduled_to_immediate_shifts', default: false })
    employee_can_be_scheduled_to_immediate_shifts?: boolean;

    @Column({
        type: 'enum',
        name: 'shift_trade_release_drop_time_limit',
        enum: EShiftPlanningDropTimeLimit,
        default: EShiftPlanningDropTimeLimit.OFF,
    })
    shift_trade_release_drop_time_limit?: EShiftPlanningDropTimeLimit;

    @Column({ type: 'boolean', name: 'work_unit_module_enabled', default: true })
    work_unit_module_enabled?: boolean;

    @Column({ type: 'boolean', name: 'employee_can_add_work_units', default: false })
    employee_can_add_work_units?: boolean;

    @Column({ type: 'boolean', name: 'work_units_submitted_by_scheduler_auto_approved', default: false })
    work_units_submitted_by_scheduler_auto_approved?: boolean;

    @Column({ type: 'boolean', name: 'work_units_submitted_by_employee_auto_approved', default: false })
    work_units_submitted_by_employee_auto_approved?: boolean;

    @Column({ type: 'boolean', name: 'employee_can_see_own_work_report', default: false })
    employee_can_see_own_work_report?: boolean;

    @Column({ type: 'integer', name: 'company_id', unique: true, select: false })
    company_id: number;

    @OneToOne(
        () => Company,
        company => company.settings_shift_planning,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id' })
    company?: Company;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
