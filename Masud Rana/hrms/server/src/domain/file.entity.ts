/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, ManyToOne } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Branch } from './branch.entity';
import { Company } from './company.entity';

import { ENTITY_FILE } from './../shared/constant/database.constant';
/**
 * A File.
 */
@Entity(ENTITY_FILE)
export class File extends BaseEntity {
    @Column({ name: 'name', length: 150 })
    name: string;

    @Column({ name: 'mimetype', length: 255 })
    mimetype: string;

    @Column({ name: 'orginal_filename', length: 150 })
    orginal_filename: string;

    @Column({ name: 'path_url', nullable: true })
    path_url: string;

    @Column({ type: 'integer', name: 'file_size' })
    file_size: number;

    @Column({ type: 'bigint', name: 'download_count', nullable: true })
    download_count: number;

    @Column({ type: 'integer', name: 'company_id', select: false })
    company_id: number;

    @Column({ type: 'integer', name: 'branch_id', nullable: true, select: false })
    branch_id: number;

    @ManyToOne(
        () => Company,
        (company: Company) => company.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
    company?: Company;

    @ManyToOne(
        () => Branch,
        (branch: Branch) => branch.id,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'branch_id', referencedColumnName: 'id' })
    branch?: Branch;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
