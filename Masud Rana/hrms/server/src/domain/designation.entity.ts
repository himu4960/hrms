/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, ManyToOne, JoinColumn, ManyToMany, JoinTable } from 'typeorm';

import { BaseEntity } from './base/base.entity';
import { Company } from './company.entity';
import { Branch } from './branch.entity';
import { Skill } from './skill.entity';

import { ENTITY_DESIGNATION, ENTITY_DESIGNATION_SKILL } from './../shared/constant/database.constant';

/**
 * A Designation.
 */
@Entity(ENTITY_DESIGNATION)
export class Designation extends BaseEntity {
    @Column({ name: 'name', length: 255 })
    name: string;

    @Column({ name: 'is_visible', default: false })
    is_visible: boolean;

    @Column({ name: 'company_id', select: false })
    company_id: number;

    @Column({ name: 'branch_id', select: false })
    branch_id: number;

    @ManyToOne(
        () => Company,
        company => company.id,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
    )
    @JoinColumn({ name: 'company_id', referencedColumnName: 'id' })
    company?: Company;

    @ManyToOne(
        () => Branch,
        branch => branch.id,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
    )
    @JoinColumn({ name: 'branch_id', referencedColumnName: 'id' })
    branch?: Branch;

    @ManyToMany(
        () => Skill,
        skill => skill.designations,
        { cascade: true },
    )
    @JoinTable({
        name: ENTITY_DESIGNATION_SKILL,
        joinColumn: {
            name: 'designation_id',
            referencedColumnName: 'id',
        },
        inverseJoinColumn: {
            name: 'skill_id',
            referencedColumnName: 'id',
        },
    })
    skills: Skill[];

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}
