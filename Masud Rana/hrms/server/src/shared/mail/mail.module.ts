import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MailerModule } from '@nestjs-modules/mailer';
import * as AWS from 'aws-sdk';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';

import { MailService } from './mail.service';
import { join } from 'path';

@Module({
    imports: [
        MailerModule.forRootAsync({
            useFactory: async (config: ConfigService) => ({
                transport: {
                    SES: new AWS.SES({
                        region: config.get('SES_REGION'),
                        accessKeyId: config.get('SES_ACCESS_KEY_ID'),
                        secretAccessKey: config.get('SES_SECRET_ACCESS_KEY'),
                    }),
                    host: config.get('SMTP_HOST'),
                    secure: false,
                    auth: {
                        user: config.get('SMTP_USER'),
                        pass: config.get('SMTP_PASSWORD'),
                    },
                    debug: true,
                },
                defaults: {
                    from: `"No Reply" <${config.get('EMAIL_FROM')}>`,
                },
                template: {
                    dir: join(__dirname, '../../mail-templates'),
                    adapter: new HandlebarsAdapter(),
                    options: {
                        strict: true,
                    },
                },
            }),
            inject: [ConfigService],
        }),
    ],
    providers: [MailService],
    exports: [MailService],
})
export class MailModule {}
