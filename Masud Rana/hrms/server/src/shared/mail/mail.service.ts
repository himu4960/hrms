import { ConfigService } from '@nestjs/config';
import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import * as AWS from 'aws-sdk';

import { EmployeeDTO } from './../../service/dto/employee/employee.dto';
import { UserDTO } from './../../service/dto/user.dto';
import { CompanyDTO } from 'src/service/dto/company.dto';

@Injectable()
export class MailService {
    clientUrl = '';
    constructor(private mailerService: MailerService, private config: ConfigService) {
        this.clientUrl = this.config.get('CLIENT_URL');
        AWS.config.update({
            credentials: {
                accessKeyId: this.config.get('SES_ACCESS_KEY_ID'),
                secretAccessKey: this.config.get('SES_SECRET_ACCESS_KEY'),
            },
            region: this.config.get('SES_REGION'),
        });
    }

    async sendRegistrationConfirmationEmail(payload: EmployeeDTO) {
        const { first_name, last_name, email, invitation_token } = payload;

        return await this.mailerService.sendMail({
            to: email,
            from: {
                name: 'Hazira',
                address: this.config.get('EMAIL_FROM'),
            },
            subject: `Invitation to Register for Hazira`,
            template: 'company-registration',
            context: {
                first_name,
                last_name,
                email,
                invitation_token,
                clientUrl: this.clientUrl,
            },
        });
    }

    /* 
        For new user invited by a company.
        User need to registration
     */
    async sendInviteRegisterEmail(payload: any, company: CompanyDTO) {
        const { id, first_name, last_name, email, invitation_token } = payload;

        return await this.mailerService.sendMail({
            to: email,
            from: {
                name: 'Hazira',
                address: this.config.get('EMAIL_FROM'),
            },
            subject: `Invitation to Register with ${company.name}`,
            template: 'invited-employee-registration',
            context: {
                id,
                first_name,
                last_name,
                email,
                invitation_token,
                clientUrl: this.clientUrl,
                company_name: company.name,
            },
        });
    }

    /* 
        For existing user invited by another company.
        User do not need registration.
     */
    async sendInviteConfirmationEmail(payload: any, company: CompanyDTO) {
        const { id, first_name, last_name, email, invitation_token } = payload;

        return await this.mailerService.sendMail({
            to: email,
            from: {
                name: 'Hazira',
                address: this.config.get('EMAIL_FROM'),
            },
            subject: `Confirmation of Registration with ${company.name} at Hazira`,
            template: 'invited-employee-confirmation',
            context: {
                id,
                first_name,
                last_name,
                email,
                invitation_token,
                clientUrl: this.clientUrl,
                company_name: company.name,
            },
        });
    }

    async sendForgotPasswordOneTimeEmail(user: UserDTO, link: string) {
        const { email, firstName, lastName } = user;
        return await this.mailerService.sendMail({
            to: email,
            from: {
                name: 'Hazira',
                address: this.config.get('EMAIL_FROM'),
            },
            subject: `Reset Your Hazira Account Password`,
            template: 'forgot-password',
            context: {
                first_name: firstName,
                last_name: lastName,
                email,
                link,
            },
        });
    }
}
