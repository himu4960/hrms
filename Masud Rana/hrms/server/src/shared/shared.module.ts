import { Global, Module } from '@nestjs/common';

import { UtilService } from './util.service';
import { DateTimeConvertService } from './convert/date-time-convert.service';
import { ExportService } from './export/export.service';

@Global()
@Module({
    imports: [],
    providers: [UtilService, DateTimeConvertService, ExportService],
    exports: [UtilService, DateTimeConvertService, ExportService],
})
export class SharedModule {}
