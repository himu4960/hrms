export enum ETimeSheetStatus {
    YES = 'Yes',
    NO = 'No',
    ABSENT = 'Absent',
}
