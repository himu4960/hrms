import { Injectable } from '@nestjs/common';
import dayjs from 'dayjs';

import { COMMA_SEPARATED_DATE, HYPHEN_SEPARATED_DATE } from '../constant/date-format';

@Injectable()
export class DateTimeConvertService {
    getDateDifferenceInSeconds(newTimestampValue: number, oldTimestampValue: number): number {
        return Math.round((newTimestampValue - oldTimestampValue) / 1000);
    }

    getDateDifferenceInDays(newDate: Date, oldDate: Date): number {
        return Math.round((newDate.getTime() - oldDate.getTime()) / (1000 * 3600 * 24));
    }

    format(date?: any): string {
        if (date && date !== undefined) {
            return dayjs(date).format(HYPHEN_SEPARATED_DATE);
        }
        return '';
    }

    addDays(currentDate: number, days: number): number {
        const day = dayjs(currentDate);
        return day.add(days, 'day').unix() * 1000;
    }

    differenceInDays(start_date: number, end_date: number) {
        const date1 = dayjs(start_date);
        const date2 = dayjs(end_date);
        const diff = date2.diff(date1, 'day', true);
        return Math.floor(diff);
    }

    getTimeFromSecond(second: number): string {
        const hours = Math.floor(second / 3600);
        const minutes = Math.floor((second - hours * 3600) / 60);

        if (!hours && !minutes) {
            return second > 10 ? `${second} Sec` : `0${second} Sec`;
        }

        if (!hours) {
            return minutes > 10 ? `${minutes} Min` : `0${minutes} Min`;
        }

        return `${hours}h ${minutes}m`;
    }

    dateDisplayFormat(date?: any): string {
        if (date && date !== undefined) {
            return dayjs(date).format(COMMA_SEPARATED_DATE);
        }
        return '';
    }

    getTimeFromUnixTimestamp(unixTimestamp: number) {
        return new Date(unixTimestamp).toLocaleTimeString();
    }

    getMillisecondsFromDate(date: any): number {
        if (date && date !== undefined) {
            return dayjs(date).valueOf();
        }
        return 0;
    }
    generateDateColumnsByRange(start_date, end_date): string[] {
        const dateColumns: string[] = [];
        const startDate = new Date(start_date);
        startDate.setDate(startDate.getDate() - 1);
        const endDate = new Date(end_date);
        endDate.setDate(endDate.getDate() + 1);
        const totalDays = this.getDateDifferenceInDays(endDate, new Date(start_date));

        for (let i = 0; i < totalDays; i++) {
            startDate.setDate(startDate.getDate() + 1);
            const currentDate = this.format(startDate);
            dateColumns.push(currentDate);
        }
        return dateColumns;
    }

    convertLocalDateToUTCDate(localDate: Date): Date {
        const newLocalDate = new Date(localDate);
        const offset = newLocalDate.getTimezoneOffset();
        const UTCDate = newLocalDate.setMinutes(newLocalDate.getMinutes() + offset);
        return new Date(UTCDate);
    }
}
