import { Injectable, Logger } from '@nestjs/common';

import { ResponseDTO } from './../service/dto/response.dto';

@Injectable()
export class UtilService {
    logger = new Logger('Util Service');

    removeObjectWithId(arr: any[], id: number) {
        const objWithIdIndex = arr.findIndex((obj: any) => obj.id === id);
        arr.splice(objWithIdIndex, 1);

        return arr;
    }

    getSuccessResponse(data: any, message: string, translateCode: string, statusCode?: number): ResponseDTO {
        return {
            status: true,
            data,
            message,
            translateCode,
        };
    }

    getErrorResponse(message: string, translateCode: string): ResponseDTO {
        return {
            status: false,
            message,
            translateCode,
        };
    }

    /**
     * Checks the incomming IP Format if IPv4 or IPv6
     * @param ip 
     * @returns true or false
     */
    isIPv4Format(ip: string): boolean {
        const pattern = /^:(ffff)?:(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){4}$/;
        const isIpv4Version = pattern.test(ip);
        return isIpv4Version;
    }

    getIp(clientIp: any): string {
        if (clientIp !== undefined) {
            if (clientIp instanceof Array && clientIp.length) {
                const ip = clientIp[0];
                if (this.isIPv4Format(ip)) {
                    return ip;
                } else {
                    return this.splitClientIp(ip);
                }
            } else {
                if (this.isIPv4Format(clientIp)) {
                    return clientIp;
                } else {
                    return this.splitClientIp(clientIp);
                }
            }
        }

        return '';
    }

    splitClientIp(clientIp: string): string {
        const array = clientIp.split(':');
        const remoteIP = array[array.length - 1];
        return remoteIP;
    }
}
