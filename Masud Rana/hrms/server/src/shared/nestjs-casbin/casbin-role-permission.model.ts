export interface CasbinRolePermission {
    roleName: string,
    path: string,
    menu?: string,
    methods: string
}

export interface CasbinRoleInheritance {
    roleName: string,
    parentRoleName: string,
}