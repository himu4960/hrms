import { Inject, Injectable } from '@nestjs/common';
import { Enforcer } from 'casbin';

import { CasbinRolePermission } from './casbin-role-permission.model';

import { CASBIN_ENFORCER } from './casbin.constants';

@Injectable()
export class CasbinService {
    // tslint:disable-next-line:no-empty
    constructor(@Inject(CASBIN_ENFORCER) private readonly enforcer: Enforcer) { }

    public async reloadPolicy() {
        await this.enforcer.loadPolicy();
    }

    public async addPolicy(data: CasbinRolePermission) {
        const added = await this.enforcer.addPolicy(data.roleName, data.path, data.methods, data.menu || '',);
        if (added) {
            await this.enforcer.savePolicy();
        }
    }

    public async updatePolicy(roleName: string): Promise<boolean> {
        try {
            let rules = await this.getPermissionsForUser(roleName);
            if (rules.length) {
                for (const iterator of rules) {
                    await this.removePolicy(...iterator);
                }
            } else {
                return false
            }
            return true;
        } catch (error) {
            return false
        }
    }


    public async removePolicy(...params: string[]) {
        const removed = await this.enforcer.removePolicy(...params);
        if (removed) {
            await this.enforcer.savePolicy();
        }
    }

    public async getPermissionsForUser(roleName: string) {
        return this.enforcer.getPermissionsForUser(roleName);
    }

    public async checkPermission(role: string, path: string, method: string) {
        return this.enforcer.enforce(role, path, method);
    }
}
