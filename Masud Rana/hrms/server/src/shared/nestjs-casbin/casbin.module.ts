import { DynamicModule, Module, Provider } from '@nestjs/common';
import { newEnforcer } from 'casbin';
import { join } from 'path';
import { ConnectionOptions } from 'typeorm';
import TypeORMAdapter from 'typeorm-adapter';

import { CASBIN_ENFORCER, CasbinService } from './';

import { typeOrmConfig } from '../../orm.config';

import { Casbin } from '../../domain/casbin.entity';

@Module({})
export class CasbinModule {
    public static forRootAsync(): DynamicModule {
        const casbinEnforcerProvider: Provider = {
            provide: CASBIN_ENFORCER,
            useFactory: async () => {
                const model = join(__dirname, '../../config/casbin/model.conf');
                const dbConnectionOptions: ConnectionOptions | any = typeOrmConfig;
                const adapter = await TypeORMAdapter.newAdapter(dbConnectionOptions, { customCasbinRuleEntity: Casbin });
                const enforcer = await newEnforcer(model, adapter);
                await enforcer.loadPolicy();
                return enforcer;
            },
        };
        return {
            exports: [casbinEnforcerProvider, CasbinService],
            module: CasbinModule,
            providers: [casbinEnforcerProvider, CasbinService],
        };
    }
}
