import { Injectable, NotFoundException } from '@nestjs/common';
import { Response } from 'express';
import { Workbook } from 'exceljs';
import dayjs from 'dayjs';

import { DateTimeConvertService } from '../convert/date-time-convert.service';
import { DATE_COLUMN_HEADERS } from '../constant/util.constant';

@Injectable()
export class ExportService {
    constructor(private readonly dateTimeConvertService: DateTimeConvertService) {}

    checkStringIsADate(date: string): boolean {
        return dayjs(date).isValid();
    }

    // Remove '_' And Return CamelCase Text
    buildHeader(str: string): string {
        return str.replace(/(?:_| |\b)(\w)/g, $1 => {
            return $1.toUpperCase().replace('_', ' ');
        });
    }

    buildDateColumnHeader(dateColumn: string): string {
        return this.dateTimeConvertService.dateDisplayFormat(dateColumn);
    }

    async exportXLSX(rows: any[], workSheetName: string, res: Response) {
        if (!rows || rows.length <= 0) {
            throw new NotFoundException('No data to download');
        }

        const today = this.dateTimeConvertService.format(dayjs());

        const workbook = new Workbook();
        const worksheet = workbook.addWorksheet(workSheetName);

        const columns = Object.keys(rows[0]).map(key => ({
            header: this.buildHeader(key),
            key,
            width: key === 'date' ? key.length * 4 : 20,
        }));

        worksheet.columns = columns;

        const newRows = rows.map(singleRow => ({
            ...singleRow,
            date: this.dateTimeConvertService.dateDisplayFormat(singleRow.date),
            late_by: Number(singleRow?.late_by)
                ? this.dateTimeConvertService.getTimeFromSecond(Number(singleRow?.late_by))
                : '',
            work_shift_hours: Number(singleRow?.work_shift_hours)
                ? this.dateTimeConvertService.getTimeFromSecond(Number(singleRow.work_shift_hours))
                : '',
            clock_vs_shift: `${Number(singleRow.clock_vs_shift).toFixed(0)}%`,
        }));
        worksheet.addRows(newRows);

        worksheet.getRow(1).eachCell(cell => {
            cell.font = { bold: true };
        });

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', 'attachment; filename=' + `${today}_${workSheetName}.xlsx`);

        return workbook.xlsx.write(res);
    }

    async exportXLSXPositionSummary(rows: any[], workSheetName: string, res: Response) {
        if (!rows || rows.length <= 0) {
            throw new NotFoundException('No data to download');
        }

        const today = this.dateTimeConvertService.format(dayjs());

        const workbook = new Workbook();
        const worksheet = workbook.addWorksheet(workSheetName);

        const [header, newRows, footer] = this.formatPositionSummary(rows);

        worksheet.columns = header;

        worksheet.addRows(newRows);
        worksheet.addRow(footer);

        worksheet.getRow(1).eachCell(cell => {
            cell.font = { bold: true };
        });

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', 'attachment; filename=' + `${today}_position_summary.xlsx`);

        return workbook.xlsx.write(res);
    }

    async exportXLSXHoursScheduled(rows: any[], workSheetName: string, res: Response) {
        if (!rows || rows.length <= 0) {
            throw new NotFoundException('No data to download');
        }

        const today = this.dateTimeConvertService.format(dayjs());

        const workbook = new Workbook();
        const worksheet = workbook.addWorksheet(workSheetName);

        const [header, newRows, footer] = this.formatPositionSummary(rows);

        worksheet.columns = header;

        worksheet.addRows(newRows);
        worksheet.addRow(footer);

        worksheet.getRow(1).eachCell(cell => {
            cell.font = { bold: true };
        });

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', 'attachment; filename=' + `${today}_hours_scheduled.xlsx`);

        return workbook.xlsx.write(res);
    }

    async exportXLSXTimeSheet(rows: any[], workSheetName: string, res: Response) {
        if (!rows || rows.length <= 0) {
            throw new NotFoundException('No data to download');
        }

        const today = this.dateTimeConvertService.format(dayjs());

        const workbook = new Workbook();
        const worksheet = workbook.addWorksheet(workSheetName);

        const [headers, newRows] = await this.formatTimeSheet(rows);

        worksheet.columns = headers;

        worksheet.addRows(newRows);

        worksheet.getRow(1).eachCell(cell => {
            cell.font = { bold: true };
        });

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', 'attachment; filename=' + `${today}_timesheet.xlsx`);
        return workbook.xlsx.write(res);
    }

    async exportXLSXTimeSheetSummary(rows: any[], workSheetName: string, res: Response) {
        if (!rows || rows.length <= 0) {
            throw new NotFoundException('No data to download');
        }

        const today = this.dateTimeConvertService.format(dayjs());

        const workbook = new Workbook();
        const worksheet = workbook.addWorksheet(workSheetName);

        const [headers, newRows, footer] = await this.formatTimeSheetSummary(rows);

        worksheet.columns = headers;

        worksheet.addRows(newRows);

        worksheet.addRow(footer);

        worksheet.getRow(1).eachCell(cell => {
            cell.font = { bold: true };
        });

        worksheet.getRow(newRows.length + 2).eachCell(cell => {
            cell.font = { bold: true };
        });

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', 'attachment; filename=' + `${today}_timesheet_summary.xlsx`);
        return workbook.xlsx.write(res);
    }

    async exportXLSXScheduleSummary(rows: any[], workSheetName: string, res: Response) {
        if (!rows || rows.length <= 0) {
            throw new NotFoundException('No data to download');
        }

        const today = this.dateTimeConvertService.format(dayjs());

        const workbook = new Workbook();
        const worksheet = workbook.addWorksheet(workSheetName);

        const [headers, newRows, footer] = await this.formatScheduleSummary(rows);

        worksheet.columns = headers;

        worksheet.addRows(newRows);

        worksheet.addRow(footer);

        worksheet.getRow(1).eachCell(cell => {
            cell.font = { bold: true };
        });

        worksheet.getRow(newRows.length + 2).eachCell(cell => {
            cell.font = { bold: true };
        });

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', 'attachment; filename=' + `${today}_timesheet_summary.xlsx`);
        return workbook.xlsx.write(res);
    }

    async exportXLSXShiftsScheduled(rows: any[], workSheetName: string, res: Response) {
        if (!rows || rows.length <= 0) {
            throw new NotFoundException('No data to download');
        }

        const today = this.dateTimeConvertService.format(dayjs());

        const workbook = new Workbook();
        const worksheet = workbook.addWorksheet(workSheetName);

        const [header, newRows, footer] = this.formatShiftsScheduled(rows);

        worksheet.columns = header;

        worksheet.addRows(newRows);

        worksheet.getRow(1).eachCell(cell => {
            cell.font = { bold: true };
        });

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', 'attachment; filename=' + `${today}_shifts_scheduled.xlsx`);

        return workbook.xlsx.write(res);
    }

    formatTimeSheet(rows: any[]): any[] {
        const headers = [];

        Object.keys(rows[0]).map(column => {
            const headerObj = {
                header: this.buildHeader(column),
                key: column,
                width: 20,
            };
            headers.push(headerObj);
        });

        const newRows = rows.map(singleRow => {
            const parsedTotalClockIn = Number(singleRow.total_clock_in);
            let newSingleRow = {
                ...singleRow,
                total_clock_in: this.dateTimeConvertService.getTimeFromSecond(parsedTotalClockIn),
            };
            Object.entries(singleRow).forEach(([column, value]: any) => {
                if (this.checkStringIsADate(column)) {
                    const parsedValue: any[] = value;
                    let dateColumnValue = '';
                    let totalDuration = 0;
                    parsedValue.forEach((value: any, index: number) => {
                        if (value?.diff) {
                            totalDuration += Number(value.diff);
                        }
                        if (parsedValue.length == index + 1) {
                            if (value?.start && value?.end) {
                                dateColumnValue += `  ${value.start} - ${
                                    value.end
                                } Total: ${this.dateTimeConvertService.getTimeFromSecond(totalDuration)}`;
                            } else if (value?.start) {
                                dateColumnValue += `  ${
                                    value.start
                                } -   Total: ${this.dateTimeConvertService.getTimeFromSecond(totalDuration)}`;
                            } else {
                                dateColumnValue += ` Total: ${this.dateTimeConvertService.getTimeFromSecond(
                                    totalDuration,
                                )}`;
                            }
                        } else {
                            if (value?.start && value?.end) {
                                dateColumnValue += `   ${value.start} - ${value.end}`;
                            } else if (value?.start) {
                                dateColumnValue += `  ${value.start}  -  `;
                            }
                        }
                    });

                    newSingleRow[column] = dateColumnValue;
                }
            });
            return newSingleRow;
        });

        return [headers, newRows];
    }

    formatTimeSheetSummary(rows: any[]): any[] {
        const headers = [];
        let columnSums = {};

        Object.keys(rows[0]).map(column => {
            if (this.checkStringIsADate(column)) {
                DATE_COLUMN_HEADERS.forEach((header: any) => {
                    const headerObj = {
                        header: `${header.title} (${this.buildDateColumnHeader(column)})`,
                        key: `${header.key}_${column}`,
                        width: 20,
                    };
                    headers.push(headerObj);
                });
            } else {
                const headerObj = {
                    header: this.buildHeader(column),
                    key: column,
                    width: 20,
                };
                headers.push(headerObj);
            }
        });

        let totalClockInDuration = 0;

        const newRows = rows.map(singleRow => {
            const parsedTotalClockIn = this.dateTimeConvertService.getTimeFromSecond(Number(singleRow.total_clock_in));
            totalClockInDuration += Number(singleRow.total_clock_in);
            let newSingleRow = {};
            Object.entries(singleRow).forEach(([column, value]: any) => {
                if (this.checkStringIsADate(column)) {
                    const parsedValue: any[] = value;
                    DATE_COLUMN_HEADERS.forEach((header: any) => {
                        const headerKey = header.key;
                        const dateColumn = `${headerKey}_${column}`;
                        let dateColumnValue = {};
                        let index = 0;
                        let totalDuration = 0;
                        dateColumnValue[dateColumn] = '';

                        parsedValue.forEach((item: any) => {
                            if (item?.diff) {
                                totalDuration += Number(item.diff);
                            }

                            if (item && item[headerKey] && headerKey == DATE_COLUMN_HEADERS[2].key) {
                                dateColumnValue[dateColumn] +=
                                    index >= 1
                                        ? ` | ${this.dateTimeConvertService.getTimeFromSecond(Number(item[headerKey]))}`
                                        : `${this.dateTimeConvertService.getTimeFromSecond(Number(item[headerKey]))}` ??
                                          '';
                                index++;
                            } else if (item && item[headerKey] && headerKey != DATE_COLUMN_HEADERS[2].key) {
                                dateColumnValue[dateColumn] +=
                                    index >= 1 ? ` | ${item[headerKey]}` : item[headerKey] ?? '';
                                index++;
                            }

                            if (!columnSums[dateColumn]) {
                                columnSums[dateColumn] = 0;
                            }
                            columnSums[dateColumn] += totalDuration;
                        });

                        if (dateColumn == `${DATE_COLUMN_HEADERS[2].key}_${column}`) {
                            if (columnSums[dateColumn]) {
                                columnSums[dateColumn] = Number(totalDuration);
                            } else {
                                columnSums[dateColumn] = 0;
                            }
                        } else {
                            columnSums[dateColumn] = '';
                        }

                        newSingleRow[dateColumn] = dateColumnValue[dateColumn] ? dateColumnValue[dateColumn] : '';
                    });
                } else {
                    newSingleRow[column] = value;
                }
            });
            newSingleRow = { ...newSingleRow, total_clock_in: parsedTotalClockIn };
            return newSingleRow;
        });

        const footer: any = ['Total', '', this.dateTimeConvertService.getTimeFromSecond(totalClockInDuration)];

        headers.forEach((header: any, index: number) => {
            if (index > 2) {
                if (columnSums[header.key]) {
                    footer.push(this.dateTimeConvertService.getTimeFromSecond(columnSums[header.key]));
                } else {
                    footer.push('');
                }
            }
        });

        return [headers, newRows, footer];
    }

    formatScheduleSummary(rows: any[]): any[] {
        const headers = [];
        let columnSums: any = {};

        Object.keys(rows[0]).map(column => {
            if (this.checkStringIsADate(column)) {
                DATE_COLUMN_HEADERS.forEach((header: any) => {
                    const headerObj = {
                        header: `${header.title} (${this.buildDateColumnHeader(column)})`,
                        key: `${header.key}_${column}`,
                        width: 20,
                    };
                    headers.push(headerObj);
                });
            } else {
                const headerObj = {
                    header: this.buildHeader(column),
                    key: column,
                    width: 20,
                };
                headers.push(headerObj);
            }
        });

        let totalWorkShiftDuration = 0;

        const newRows = rows.map(singleRow => {
            const parsedTotalWorkShift = this.dateTimeConvertService.getTimeFromSecond(
                Number(singleRow.total_work_shift_duration),
            );
            totalWorkShiftDuration += Number(singleRow.total_work_shift_duration);
            let newSingleRow = {};
            Object.entries(singleRow).forEach(([column, value]: any) => {
                if (this.checkStringIsADate(column)) {
                    const parsedValue: any[] = value;
                    DATE_COLUMN_HEADERS.forEach((header: any) => {
                        const headerKey = header.key;
                        const dateColumn = `${headerKey}_${column}`;
                        let dateColumnValue = {};
                        let index = 0;
                        let totalDuration = 0;
                        dateColumnValue[dateColumn] = '';

                        parsedValue.forEach((item: any) => {
                            if (item?.diff) {
                                totalDuration += Number(item.diff);
                            }

                            if (item && item[headerKey] && headerKey == DATE_COLUMN_HEADERS[2].key) {
                                dateColumnValue[dateColumn] +=
                                    index >= 1
                                        ? ` | ${this.dateTimeConvertService.getTimeFromSecond(Number(item[headerKey]))}`
                                        : `${this.dateTimeConvertService.getTimeFromSecond(Number(item[headerKey]))}` ??
                                          '';
                                index++;
                            } else if (item && item[headerKey] && headerKey != DATE_COLUMN_HEADERS[2].key) {
                                dateColumnValue[dateColumn] +=
                                    index >= 1 ? ` | ${item[headerKey]}` : item[headerKey] ?? '';
                                index++;
                            }

                            if (!columnSums[dateColumn]) {
                                columnSums[dateColumn] = 0;
                            }
                            columnSums[dateColumn] += totalDuration;
                        });

                        if (dateColumn == `${DATE_COLUMN_HEADERS[2].key}_${column}`) {
                            if (columnSums[dateColumn]) {
                                columnSums[dateColumn] = Number(totalDuration);
                            } else {
                                columnSums[dateColumn] = 0;
                            }
                        } else {
                            columnSums[dateColumn] = '';
                        }

                        newSingleRow[dateColumn] = dateColumnValue[dateColumn] ? dateColumnValue[dateColumn] : '';
                    });
                } else {
                    newSingleRow[column] = value;
                }
            });
            newSingleRow = { ...newSingleRow, total_work_shift_duration: parsedTotalWorkShift };
            return newSingleRow;
        });

        const footer: any[] = ['Total', '', this.dateTimeConvertService.getTimeFromSecond(totalWorkShiftDuration)];

        headers.forEach((header: any, index: number) => {
            if (index > 2) {
                if (columnSums[header.key]) {
                    footer.push(this.dateTimeConvertService.getTimeFromSecond(columnSums[header.key]));
                } else {
                    footer.push('');
                }
            }
        });

        return [headers, newRows, footer];
    }

    formatPositionSummary(rows: any[]): any[] {
        let dateColumns = {};
        let total_work_shift = 0;

        const header = [];
        const footer: any = ['', 'Total'];

        Object.keys(rows[0]).map(column => {
            if (this.checkStringIsADate(column)) {
                const headerObj = {
                    header: this.buildDateColumnHeader(column),
                    key: column,
                    width: 20,
                };
                header.push(headerObj);
            } else if (!this.checkStringIsADate(column)) {
                const headerObj = {
                    header: this.buildHeader(column),
                    key: column,
                    width: 20,
                };
                header.push(headerObj);
            }
        });

        const newRows = rows.map(singleRow => {
            const parsedTotalWorkShift = Number(singleRow.total_work_shift);
            let newSingleRow = {
                ...singleRow,
                total_work_shift: this.dateTimeConvertService.getTimeFromSecond(parsedTotalWorkShift),
            };
            total_work_shift = total_work_shift + parsedTotalWorkShift;
            Object.entries(singleRow).forEach(([key, value]) => {
                if (this.checkStringIsADate(key)) {
                    const parsedValue = Number(value);
                    if (dateColumns[key] == undefined || !dateColumns[key]) {
                        dateColumns[key] = 0;
                    }
                    dateColumns[key] = dateColumns[key] + parsedValue;
                    newSingleRow[key] =
                        parsedValue > 0 ? this.dateTimeConvertService.getTimeFromSecond(parsedValue) : 0;
                }
            });
            return newSingleRow;
        });

        footer.push(this.dateTimeConvertService.getTimeFromSecond(total_work_shift));

        Object.entries(dateColumns).forEach(([key, value]) => {
            if (value && value !== undefined) {
                footer.push(this.dateTimeConvertService.getTimeFromSecond(Number(value)));
            } else {
                footer.push('');
            }
        });

        return [header, newRows, footer];
    }

    formatShiftsScheduled(rows: any[]): any[] {
        const headers = [];

        Object.keys(rows[0]).map(column => {
            if (this.checkStringIsADate(column)) {
                const headerObj = {
                    header: this.buildDateColumnHeader(column),
                    key: column,
                    width: 20,
                };
                headers.push(headerObj);
            } else if (!this.checkStringIsADate(column)) {
                const headerObj = {
                    header: this.buildHeader(column),
                    key: column,
                    width: 20,
                };
                headers.push(headerObj);
            }
        });

        const newRows = rows.map(singleRow => {
            const parsedTotalShiftsScheduled = this.dateTimeConvertService.getTimeFromSecond(
                Number(singleRow.total_work_shift_duration),
            );
            let newSingleRow = { ...singleRow, total_work_shift_duration: parsedTotalShiftsScheduled };
            Object.entries(singleRow).forEach(([column, value]: any) => {
                if (this.checkStringIsADate(column)) {
                    const parsedValue: any[] = value;
                    let dateColumnValue = '';
                    parsedValue.forEach((value: any, index: number) => {
                        if (value?.start && value?.end) {
                            dateColumnValue += `    ${value.designation}    ${value.start} - ${value.end};`;
                        } else if (value?.start) {
                            dateColumnValue += `    ${value.designation}    ${value.start};`;
                        } else {
                            dateColumnValue += '';
                        }
                    });

                    newSingleRow[column] = dateColumnValue;
                }
            });
            newSingleRow = { ...newSingleRow, total_work_shift_duration: parsedTotalShiftsScheduled };
            return newSingleRow;
        });

        return [headers, newRows];
    }

    async exportXLSXLocationHistory(rows: any[], workSheetName: string, res: Response) {
        if (!rows || rows.length <= 0) {
            throw new NotFoundException('No data to download');
        }

        const today = this.dateTimeConvertService.format(dayjs());

        const workbook = new Workbook();
        const worksheet = workbook.addWorksheet(workSheetName);

        const columns = Object.keys(rows[0]).map(key => ({
            header: this.buildHeader(key),
            key,
            width: key === 'date' ? key.length * 4 : 20,
        }));

        worksheet.columns = columns;

        const newRows = rows.map(singleRow => {
            let clockInLocation;
            let clockOutLocation;

            if (singleRow?.clockin_location) {
                clockInLocation = JSON.parse(singleRow?.clockin_location);
            }

            if (singleRow?.clockout_location) {
                clockOutLocation = JSON.parse(singleRow?.clockout_location);
            }

            return {
                ...singleRow,
                date: this.dateTimeConvertService.dateDisplayFormat(singleRow.date),

                total_break_duration: Number(singleRow?.total_break_duration)
                    ? this.dateTimeConvertService.getTimeFromSecond(Number(singleRow?.total_break_duration))
                    : '',
                total_clock_duration: Number(singleRow?.total_clock_duration)
                    ? this.dateTimeConvertService.getTimeFromSecond(Number(singleRow.total_clock_duration))
                    : '',
                clockin_location: clockInLocation
                    ? `${clockInLocation?.address?.road}, ${clockInLocation?.address?.neighbourhood}`
                    : '',
                clockout_location: clockOutLocation
                    ? `${clockOutLocation?.address?.road}, ${clockOutLocation?.address?.neighbourhood}`
                    : '',
            };
        });

        worksheet.addRows(newRows);

        worksheet.getRow(1).eachCell(cell => {
            cell.font = { bold: true };
        });

        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', 'attachment; filename=' + `${today}_${workSheetName}.xlsx`);

        return workbook.xlsx.write(res);
    }
}
