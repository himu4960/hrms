export const HOME_RESPONSE = {
    READ: {
        SUCCESS: 'Home data successfully retrieved',
        ERROR: 'Home data was not retrieved',
    },
};

export const FILE_RESPONSE = {
    CREATE: {
        SUCCESS: 'File uploaded successfully',
        ERROR: 'File cannot be uploaded',
    },
    DOWNLOAD: {
        SUCCESS: 'File downloaded successfully',
        ERROR: 'File cannot be downloaded',
    },
    READ: {
        SUCCESS: 'File was sucessfully retrieved!',
        ERROR: 'File was not retrieved!',
    },
    DELETE: {
        SUCCESS: 'File deleted successfully',
        ERROR: 'File cannot be deleted',
    },
};

export const ANNOUNCEMENT_RESPONSE = {
    CREATE: {
        SUCCESS: 'Announcement created successfully',
        ERROR: 'Announcement cannot be created',
    },
    READ: {
        SUCCESS: 'Announcement was sucessfully retrieved!',
        ERROR: 'Announcement was not retrieved!',
    },
    UPDATE: {
        SUCCESS: 'Announcement updated successfully',
        ERROR: 'Announcement cannot be updated',
    },
    DELETE: {
        SUCCESS: 'Announcement deleted successfully',
        ERROR: 'Announcement cannot be deleted',
    },
};

export const REGISTER_RESPONSE = {
    COMPLETE: {
        SUCCESS: 'Registration completed successfully',
        ERROR: 'Registration failed',
    },
    CONFIRM: {
        SUCCESS: 'Registration confirmed! please login',
        ERROR: 'Registration confirmation failed!',
    },
    EXPIRED: 'Token has been expired! Please check your mail again',
    ALREADY_ACCEPTED: 'You are already accepted your invitation!',
    ERROR: 'Somthing went wrong!',
};

export const BRANCH_RESPONSE = {
    CREATE: {
        SUCCESS: 'Branch created successfully',
        ERROR: 'Branch cannot be created',
    },
    READ: {
        SUCCESS: 'Branch was sucessfully retrieved!',
        ERROR: 'Branch was not retrieved!',
    },
    UPDATE: {
        SUCCESS: 'Branch updated successfully',
        ERROR: 'Branch cannot be updated',
    },
    DELETE: {
        SUCCESS: 'Branch deleted successfully',
        ERROR: 'Branch cannot be deleted',
        PRIMARY_ERROR: 'Primary branch cannot be deleted!',
    },
    NOT_FOUND: 'Branch not found!',
};

export const DESIGNATION_RESPONSE = {
    CREATE: {
        SUCCESS: 'Designation created successfully',
        ERROR: 'Designation cannot be created',
    },
    READ: {
        SUCCESS: 'Designation was sucessfully retrieved!',
        ERROR: 'Designation was not retrieved!',
    },
    UPDATE: {
        SUCCESS: 'Designation updated successfully',
        ERROR: 'Designation cannot be updated',
    },
    DELETE: {
        SUCCESS: 'Designation deleted successfully',
        ERROR: 'Designation cannot be deleted',
    },
};

export const LEAVE_RESPONSE = {
    CREATE: {
        SUCCESS: 'Leave created successfully',
        ERROR: 'Leave cannot be created',
    },
    READ: {
        SUCCESS: 'Leave was sucessfully retrieved!',
        ERROR: 'Leave was not retrieved!',
    },
    UPDATE: {
        SUCCESS: 'Leave updated successfully',
        ERROR: 'Leave cannot be updated',
    },
    DELETE: {
        SUCCESS: 'Leave deleted successfully',
        ERROR: 'Leave cannot be deleted',
    },
    APPROVE: {
        SUCCESS: 'Successfully approved',
        ERROR: 'Failed! Max employee already on leave',
    },
    REQUEST: {
        SUCCESS: 'Your request has been sent!',
        ERROR: 'Your request sending failed!',
    },
    CANCEL: {
        SUCCESS: 'Your leave request has been cancelled!',
        ERROR: 'Your leave request cannot be cancelled!',
    },
};

export const TIME_CLOCK_RESPONSE = {
    CREATE: {
        SUCCESS: 'Time clock created successfully',
        ERROR: 'Time clock cannot be created',
    },
    READ: {
        SUCCESS: 'Time clock was sucessfully retrieved!',
        ERROR: 'Time clock was not retrieved!',
    },
    UPDATE: {
        SUCCESS: 'Time clock updated successfully',
        ERROR: 'Time clock cannot be updated',
    },
    DELETE: {
        SUCCESS: 'Time clock deleted successfully',
        ERROR: 'Time clock cannot be deleted',
    },
    TIME_SHEET: {
        READ: {
            SUCCESS: 'Time sheet was sucessfully retrieved!',
            ERROR: 'Time sheet was not retrieved!',
        },
    },
    CLOCK_OUT: {
        SUCCESS: 'Clock out successfully',
        ERROR: 'Clock out failed',
    },
};

export const TIME_ZONE_RESPONSE = {
    READ: {
        SUCCESS: 'Time Zone was sucessfully retrieved!',
        ERROR: 'Time Zone was not retrieved!',
    },
};

export const WORK_SHIFT_RESPONSE = {
    CREATE: {
        SUCCESS: 'Work Shift created successfully',
        ERROR: 'Work Shift  cannot be created',
    },
    READ: {
        SUCCESS: 'Work Shift was sucessfully retrieved!',
        ERROR: 'Work Shift was not retrieved!',
    },
    UPDATE: {
        SUCCESS: 'Work Shift updated successfully',
        ERROR: 'Work Shift cannot be updated',
    },
    DELETE: {
        SUCCESS: 'Work Shift deleted successfully',
        ERROR: 'Work Shift  cannot be deleted',
    },
    PUBLISH: {
        SUCCESS: 'Work Shift successfully published',
        ERROR: 'Work Shift cannot be published',
    },
};

export const WORK_SHIFT_TEMPLATE_RESPONSE = {
    CREATE: {
        SUCCESS: 'Workshift Template created successfully',
        ERROR: 'Workshift Template cannot be created',
    },
    READ: {
        SUCCESS: 'Workshift Template was sucessfully retrieved!',
        ERROR: 'Workshift Template was not retrieved!',
    },
    UPDATE: {
        SUCCESS: 'Workshift Template updated successfully',
        ERROR: 'Workshift Template cannot be updated',
    },
    DELETE: {
        SUCCESS: 'Workshift Template deleted successfully',
        ERROR: 'Workshift Template cannot be deleted',
    },
};

export const HOLIDAY_RESPONSE = {
    CREATE: {
        SUCCESS: 'Holiday created successfully',
        ERROR: 'Holiday cannot be created',
    },
    READ: {
        SUCCESS: 'Holiday was sucessfully retrieved!',
        ERROR: 'Holiday was not retrieved!',
    },
    UPDATE: {
        SUCCESS: 'Holiday updated successfully',
        ERROR: 'Holiday cannot be updated',
    },
    DELETE: {
        SUCCESS: 'Holiday deleted successfully',
        ERROR: 'Holiday cannot be deleted',
    },
};

export const EMPLOYEE_RESPONSE = {
    CREATE: {
        SUCCESS: 'Employee created successfully',
        ERROR: 'Employee cannot be created',
    },
    READ: {
        SUCCESS: 'Employee was sucessfully retrieved!',
        ERROR: 'Employee was not retrieved!',
        NOT_FOUND: 'Employee not found!',
    },
    UPDATE: {
        SUCCESS: 'Employee updated successfully',
        ERROR: 'Employee cannot be updated',
        CHANGE_BRANCH_SUCCESS: 'Branch changed successfully',
        CHANGE_BRANCH_ERROR: 'Branch cannot be changed',
    },
    DELETE: {
        SUCCESS: 'Employee deleted successfully',
        ERROR: 'Employee cannot be deleted',
    },

    NOT_FOUND: 'Employee not found!',

    ACTIVATION_UPDATE: {
        SUCCESS: 'Employee active status sucessfully changed!',
    },

    ASSIGN_DESIGNATION: {
        SUCCESS: 'Designation sucessfully assigned!',
        ERROR: 'Designation cannot be assigned',
    },
    ASSIGN_SKILL: {
        SUCCESS: 'Skill sucessfully assigned!',
        ERROR: 'Skill cannot be assigned',
    },

    UNASSIGN_DESIGNATION: {
        SUCCESS: 'Designation sucessfully unassigned!',
        ERROR: 'Designation cannot be unassigned',
    },
    UNASSIGN_SKILL: {
        SUCCESS: 'Skill sucessfully unassigned!',
        ERROR: 'Skill cannot be unassigned',
    },

    REGISTER: {
        SUCCESS: 'Registration successful',
        ERROR: 'Registration failed!',
    },

    SENT_EMAIL: {
        SUCCESS: 'Email has been successfully sent! Please check your email',
        ERROR: 'Email sending failed!',
    },

    VERIFY_TOKEN: {
        SUCCESS: 'Token is successfully verified!',
        ERROR: 'Token is not verified!',
    },

    VERIFY_EMAIL: {
        SUCCESS: 'Email has been successfully verified!',
        ERROR: 'Email is not verified!',
    },

    EXPORT: {
        SUCCESS: 'Successfully export XLSX',
        ERROR: 'Fail to export XLSX',
        NOT_FOUND: 'No data to download',
    },
};

export const USER_RESPONSE = {
    CREATE: {
        SUCCESS: 'User created successfully',
        ERROR: 'User cannot be created',
    },
    READ: {
        SUCCESS: 'User was sucessfully retrieved!',
        ERROR: 'User was not retrieved!',
        NOT_FOUND: 'User not found!',
    },
    UPDATE: {
        SUCCESS: 'User updated successfully',
        ERROR: 'User cannot be updated',
    },
    DELETE: {
        SUCCESS: 'User deleted successfully',
        ERROR: 'User cannot be deleted',
    },
};

export const REGISTER_COMPANY_RESPONSE = {
    CREATE: {
        SUCCESS: 'Company has been registered successfully',
        ERROR: 'Company cannot be registered',
    },
    UPDATE: {
        SUCCESS: 'Company has been updated successfully',
        ERROR: 'Company cannot be updated',
    },
    DELETE: {
        SUCCESS: 'Company has been deleted successfully',
        ERROR: 'Company cannot be deleted',
    },
    READ: {
        SUCCESS: 'Company was sucessfully retrieved!',
        ERROR: 'Company was not retrieved!',
        NOT_FOUND: 'Company not found!',
    },
    SELECTED: 'Company selected successfully',
    DUPLICATE_NAME: 'You already have a company with same name!',
};

export const ROLE_UPDATE_RESPONSE = {
    SUCCESS: 'Role has been updated successfully',
    ERROR: 'Role cannot be updated',
};

export const SKILLS_RESPONSE = {
    CREATE: {
        SUCCESS: 'Skill created successfully',
        ERROR: 'Skill cannot be created',
    },
    READ: {
        SUCCESS: 'Skill was sucessfully retrieved!',
        ERROR: 'Skill was not retrieved!',
        NOT_FOUND: 'Skill not found!',
    },
    UPDATE: {
        SUCCESS: 'Skill updated successfully',
        ERROR: 'Skill cannot be updated',
    },
    DELETE: {
        SUCCESS: 'Skill deleted successfully',
        ERROR: 'Skill cannot be deleted',
    },
};

export const REPORT_RESPONSE = {
    READ: {
        SUCCESS: 'Report sucessfully generated!',
        ERROR: 'Report was not generated!',
        NOT_FOUND: 'No data found!',
    },
    PDF_EXPORT_SUCCESS: 'Successfully Generated PDF',
    XLSX_EXPORT_SUCCESS: 'Successfully Generated XLSX',
    WARNING: 'Please select type',
    TIME_SHEET: {
        SUCCESS: 'Time was sucessfully retrieved!',
        ERROR: 'Time was not retrieved!',
    },
};

export const GEO_LOCATION = {
    SUCCESS: 'Geo Location was sucessfully retrieved!',
    ERROR: 'Geo Location was not retrieved!',
};

export const APPLICATION_SETTING_RESPONSE = {
    READ: {
        SUCCESS: 'Application setting was sucessfully retrieved!',
        ERROR: 'Application setting was not retrieved!',
    },
    UPDATE: {
        SUCCESS: 'Application setting updated successfully',
        ERROR: 'Application setting cannot be updated',
    },
};

export const SHIFT_PLANNING_SETTING_RESPONSE = {
    READ: {
        SUCCESS: 'Shift Planing settings was sucessfully retrieved!',
        ERROR: 'Shift Planing settings was not retrieved!',
    },
    UPDATE: {
        SUCCESS: 'Shift Planing settings updated successfully',
        ERROR: 'Shift Planing settings cannot be updated',
    },
};

export const LOGIN_RESPONSE = {
    SUCCESS: 'Login successfully',
    PASSWORD_EXPIRY_ALERT: 'Password expried!',
    OLD_PASSWORD_ALERT: 'You have entered an old password',
    INVALID_CREDENTIAL_ERROR: 'Invalid login name or password!',
    CHANGE_BUSSINESS: 'Company has been selected',
    ERROR: 'Something went wrong!',
};

export const LOGIN_RESPONSE_CODE = {
    PASSWORD_EXPIRY_CODE: 601,
    OLD_PASSWORD_CODE: 602,
    INVALID_CREDENTIAL_CODE: 603,
};

export const RESET_PASSWORD_RESPONSE = {
    SUCCESS: 'Password sucessfully reseted!',
    ERROR: 'Password cannot be reseted!',
};

export const FORGET_PASSWORD_RESPONSE = {
    SENT_EMAIL: {
        SUCCESS: 'Email has been sent!',
        ERROR: 'Email sending failed!',
    },
    TOKEN: {
        SUCCESS: 'Token verified!',
        ERROR: 'Invalid token!',
    },
};

export const CHANGE_PASSWORD_RESPONSE = {
    SUCCESS: 'Password sucessfully changed',
};

export const FILE_UPLOAD_RESPONSE = {
    SUCCESS: 'File was sucessfully uploaded!',
    ERROR: 'File upload failed!',
};

export const AUTH_SETUP_RESPONSE = {
    SUCCESS: 'Auth setup data successfully retrived',
    ERROR: 'Auth setup data not retrived',
};

export const AUTH_TOKEN_RENEWED = {
    SUCCESS: 'Token successfully renewed',
    ERROR: 'Token renewed failed',
};
