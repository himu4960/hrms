export const DEFAULT_WORK_HOUR = 24;
export const DEFAULT_WORK_HOUR_IN_MINS = DEFAULT_WORK_HOUR * 60;
export const DATE_COLUMN_HEADERS = [
    { title: 'Start', key: 'start' },
    { title: 'End', key: 'end' },
    { title: 'Hrs', key: 'diff' },
];

export const EMPLOYEE_EXPORT_HIDEN_FIELDS = [
    'id',
    'invitation_accepted',
    'photo_url',
    'nationality',
    'telephone',
    'personal_email',
];

export enum ERoleType {
    SUPER_ADMIN = 'SUPER_ADMIN',
    OWNER = 'OWNER',
    MANAGER = 'MANAGER',
    EMPLOYEE = 'EMPLOYEE',
}
