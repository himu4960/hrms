export const SLASH_SEPARATED_DATE = 'DD/MM/YYYY';
export const HYPHEN_SEPARATED_DATE = 'YYYY-MM-DD';
export const COMMA_SEPARATED_DATE = 'MMM DD, YYYY';
export const DATETIME = 'DD/MM/YYYY HH:MM';
export const SERVER_TIME_ZONE = "+00:00";
