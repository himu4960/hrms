import { Logger, ValidationPipe, BadRequestException } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import cloudConfigClient from 'cloud-config-client';
import * as fs from 'fs';
require('dotenv').config({ path: '.env' });

import { AppModule } from './app.module';
import { setupSwagger } from './swagger';
import { config } from './config';

import { HttpExceptionFilter } from './client/exceptions/http.exception.filter';

const logger: Logger = new Logger('Main');
const port = process.env.NODE_SERVER_PORT || config.get('server.port');
const useHrmsRegistry = config.get('eureka.client.enabled');

async function bootstrap(): Promise<void> {
    loadCloudConfig();
    registerAsEurekaService();

    const appOptions = { cors: true };
    const app = await NestFactory.create(AppModule, appOptions);
    app.useGlobalPipes(
        new ValidationPipe({
            exceptionFactory: (): BadRequestException => new BadRequestException('Validation error'),
        }),
    );
    app.useGlobalFilters(new HttpExceptionFilter());
    const staticClientPath = config.getClientPath();
    if (fs.existsSync(staticClientPath)) {
        logger.log(`Serving static client resources on ${staticClientPath}`);
    } else {
        logger.log('No client it has been found');
    }
    setupSwagger(app);

    await app.listen(port);
    logger.log(`Application listening on port ${port}`);
}

async function loadCloudConfig(): Promise<void> {
    if (useHrmsRegistry) {
        const endpoint = config.get('cloud.config.uri') || 'http://admin:admin@localhost:8761/config';
        logger.log(`Loading cloud config from ${endpoint}`);

        const cloudConfig = await cloudConfigClient.load({
            context: process.env,
            endpoint,
            name: config.get('cloud.config.name'),
            profiles: config.get('cloud.config.profile') || ['prod'],
            // auth: {
            //   user: config.get('hrms.registry.username') || 'admin',
            //   pass: config.get('hrms.registry.password') || 'admin'
            // }
        });
        config.addAll(cloudConfig.properties);
    }
}

function registerAsEurekaService(): void {
    if (useHrmsRegistry) {
        logger.log(`Registering with eureka ${config.get('cloud.config.uri')}`);
        const Eureka = require('eureka-js-client').Eureka;
        const eurekaUrl = require('url').parse(config.get('cloud.config.uri'));
        const client = new Eureka({
            instance: {
                app: config.get('eureka.instance.appname'),
                instanceId: config.get('eureka.instance.instanceId'),
                hostName: config.get('ipAddress') || 'localhost',
                ipAddr: config.get('ipAddress') || '127.0.0.1',
                status: 'UP',
                port: {
                    $: port,
                    '@enabled': 'true',
                },
                vipAddress: config.get('ipAddress') || 'localhost',
                homePageUrl: `http://${config.get('ipAddress')}:${port}/`,
                dataCenterInfo: {
                    '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
                    name: 'MyOwn',
                },
            },
            eureka: {
                // eureka server host / port
                host: eurekaUrl.hostname || '127.0.0.1',
                port: eurekaUrl.port || 8761,
                servicePath: '/eureka/apps',
            },
            requestMiddleware: (requestOpts, done): any => {
                requestOpts.auth = {
                    user: config.get('hrms.registry.username') || 'admin',
                    password: config.get('hrms.registry.password') || 'admin',
                };
                done(requestOpts);
            },
        });
        client.logger.level('debug');
        client.start(error => logger.log(error || 'Eureka registration complete'));
    }
}

bootstrap();
