export interface IReligion {
    id: number;
    company_id?: number;
    is_active?: boolean;
    is_deleted?: boolean;
}

export interface IReligionTranslation {
    id?: number;
    name: string;
    religion_id: number;
    language_code: string;
}

export const religionsSeedData: IReligion[] = [
    {
        id: 1,
    },
    {
        id: 2,
    },
    {
        id: 3,
    },
    {
        id: 4,
    },
    {
        id: 5,
    },
];

export const religionTranslationsSeedData: IReligionTranslation[] = [
    {
        id: 1,
        name: 'Hinduism',
        religion_id: 1,
        language_code: 'en',
    },
    {
        id: 2,
        name: 'হিন্দুধর্ম',
        religion_id: 1,
        language_code: 'bn',
    },
    {
        id: 3,
        name: 'Islam',
        religion_id: 2,
        language_code: 'en',
    },
    {
        id: 4,
        name: 'ইসলামধর্ম',
        religion_id: 2,
        language_code: 'bn',
    },
    {
        id: 5,
        name: 'Buddhism',
        religion_id: 3,
        language_code: 'en',
    },
    {
        id: 6,
        name: 'বৌদ্ধধর্ম',
        religion_id: 3,
        language_code: 'bn',
    },
    {
        id: 7,
        name: 'Christianity',
        religion_id: 4,
        language_code: 'en',
    },
    {
        id: 8,
        name: 'খ্রিষ্টানধর্ম',
        religion_id: 4,
        language_code: 'bn',
    },
    {
        id: 9,
        name: 'Other',
        religion_id: 5,
        language_code: 'en',
    },
    {
        id: 10,
        name: 'অন্যান্য',
        religion_id: 5,
        language_code: 'bn',
    },
];
