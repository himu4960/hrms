export interface ICompanyType {
    id: number;
    name?: string;
    is_active?: true;
    is_deleted?: false;
}

export const companyTypesSeedData: ICompanyType[] = [
    {
        id: 1,
        name: 'Health Care and Social Assistance',
    },
    {
        id: 2,
        name: 'Restaurant / Food Service',
    },
    {
        id: 3,
        name: 'Retail',
    },
    {
        id: 4,
        name: 'Education',
    },
    {
        id: 5,
        name: 'Call Centre',
    },
    {
        id: 6,
        name: 'Agriculture/Forestry',
    },
    {
        id: 7,
        name: 'Automotive',
    },
    {
        id: 8,
        name: 'Business Services',
    },
    {
        id: 9,
        name: 'Construction/Contractors',
    },
    {
        id: 10,
        name: 'Finance and Insurance',
    },
    {
        id: 11,
        name: 'Housing & Real Estate',
    },
    {
        id: 12,
        name: 'Manufacturing',
    },
    {
        id: 13,
        name: 'Telecommunications',
    },
    {
        id: 14,
        name: 'Accommodation',
    },
    {
        id: 15,
        name: 'Non-profit/Charity/Volunteers',
    },
    {
        id: 16,
        name: 'Arts, Entertainment & Recreation',
    },
    {
        id: 17,
        name: 'Consultant',
    },
    {
        id: 18,
        name: 'Emergency Services',
    },
    {
        id: 19,
        name: 'Government/Public Administration',
    },
    {
        id: 20,
        name: 'Home Improvement Services',
    },
    {
        id: 21,
        name: 'Information Technology',
    },
    {
        id: 22,
        name: 'Media',
    },
    {
        id: 23,
        name: 'Mining, Oil and Gas',
    },
    {
        id: 24,
        name: 'Reseller/White Label',
    },
    {
        id: 25,
        name: 'Security Services',
    },
    {
        id: 26,
        name: 'Software',
    },
    {
        id: 27,
        name: 'Transportation/Logistics',
    },
    {
        id: 28,
        name: 'Utilities',
    },
    {
        id: 29,
        name: 'Wholesaler',
    },
    {
        id: 30,
        name: 'Police Department',
    },
    {
        id: 31,
        name: 'Veterinarian',
    },
    {
        id: 32,
        name: 'Other',
    },
];
