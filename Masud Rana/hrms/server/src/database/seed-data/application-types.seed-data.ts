export interface IApplicationType {
    id: number;
    name?: string;
    is_active?: true;
    is_deleted?: false;
}

export const applicationTypesSeedData: IApplicationType[] = [
    {
        id: 1,
        name: 'Human Resources',
    },
    {
        id: 2,
        name: 'Operations',
    },
    {
        id: 3,
        name: 'Schedular',
    },
    {
        id: 4,
        name: 'Finance',
    },
    {
        id: 5,
        name: 'Executive',
    },
    {
        id: 6,
        name: 'Other',
    },
];
