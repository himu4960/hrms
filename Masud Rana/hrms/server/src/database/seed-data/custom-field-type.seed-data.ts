export interface ICustomFieldType {
    id: number;
    name: string;
    data_type: string;
}

export const customFieldTypeSeedData: ICustomFieldType[] = [
    {
        id: 1111,
        name: 'Number',
        data_type: 'number',
    },
    {
        id: 2222,
        name: 'Short Text (Max length 200 Chars)',
        data_type: 'string',
    },
    {
        id: 3333,
        name: 'Long Text',
        data_type: 'string',
    },
    {
        id: 4444,
        name: 'Checkbox (yes/no toggle)',
        data_type: 'boolean',
    },
    {
        id: 5555,
        name: 'Date',
        data_type: 'date',
    },
    {
        id: 6666,
        name: 'Options List (eg: small,medium,large)',
        data_type: 'list',
    }
]
