export interface IBloodGroup {
    id?: number;
    company_id?: number;
    is_active?: boolean;
    is_deleted?: boolean;
}

export interface IBloodGroupTranslation {
    id?: number;
    name: string;
    blood_group_id?: number;
    language_code?: string;
}

export const bloodGroupsSeedData: IBloodGroup[] = [
    {
        id: 1,
    },
    {
        id: 2,
    },
    {
        id: 3,
    },
    {
        id: 4,
    },
    {
        id: 5,
    },
    {
        id: 6,
    },
    {
        id: 7,
    },
    {
        id: 8,
    },
];

export const BloodGroupTranslationsSeedData: IBloodGroupTranslation[] = [
    {
        id: 1,
        name: 'A+',
        blood_group_id: 1,
        language_code: 'en',
    },
    {
        id: 2,
        name: 'এ পজেটিভ',
        blood_group_id: 1,
        language_code: 'bn',
    },
    {
        id: 3,
        name: 'A-',
        blood_group_id: 2,
        language_code: 'en',
    },
    {
        id: 4,
        name: 'এ নেগেটিভ',
        blood_group_id: 2,
        language_code: 'bn',
    },
    {
        id: 5,
        name: 'B+',
        blood_group_id: 3,
        language_code: 'en',
    },
    {
        id: 6,
        name: 'বি পজেটিভ',
        blood_group_id: 3,
        language_code: 'bn',
    },
    {
        id: 7,
        name: 'B-',
        blood_group_id: 4,
        language_code: 'en',
    },
    {
        id: 8,
        name: 'বি নেগেটিভ',
        blood_group_id: 4,
        language_code: 'bn',
    },
    {
        id: 9,
        name: 'O+',
        blood_group_id: 5,
        language_code: 'en',
    },
    {
        id: 10,
        name: 'ও পজেটিভ',
        blood_group_id: 5,
        language_code: 'bn',
    },
    {
        id: 11,
        name: 'O-',
        blood_group_id: 6,
        language_code: 'en',
    },
    {
        id: 12,
        name: 'ও নেগেটিভ',
        blood_group_id: 6,
        language_code: 'bn',
    },
    {
        id: 13,
        name: 'AB+',
        blood_group_id: 7,
        language_code: 'en',
    },
    {
        id: 14,
        name: 'এবি পজেটিভ',
        blood_group_id: 7,
        language_code: 'bn',
    },
    {
        id: 15,
        name: 'AB-',
        blood_group_id: 8,
        language_code: 'en',
    },
    {
        id: 16,
        name: 'এবি নেগেটিভ',
        blood_group_id: 8,
        language_code: 'bn',
    },
];
