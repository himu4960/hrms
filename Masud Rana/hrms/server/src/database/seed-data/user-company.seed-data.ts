import { RoleType } from './../../security/role-type';

export interface IUserCompany {
    id: number;
    is_owner: boolean;
    user_id: number;
    company_id: number;
    role_id: number;
    employee_id: number;
}

export const userCompaniesSeedData: IUserCompany[] = [
    {
        id: 1,
        is_owner: false,
        user_id: 1,
        company_id: 1,
        role_id: RoleType.EMPLOYEE,
        employee_id: 1,
    },
    {
        id: 2,
        is_owner: false,
        user_id: 2,
        company_id: 1,
        role_id: RoleType.EMPLOYEE,
        employee_id: 2,
    },
    {
        id: 3,
        is_owner: true,
        user_id: 3,
        company_id: 1,
        role_id: RoleType.SUPER_ADMIN,
        employee_id: 3,
    },
    {
        id: 4,
        is_owner: false,
        user_id: 4,
        company_id: 1,
        role_id: RoleType.OWNER,
        employee_id: 4,
    },
    {
        id: 5,
        is_owner: false,
        user_id: 5,
        company_id: 1,
        role_id: RoleType.MANAGER,
        employee_id: 5,
    },
];
