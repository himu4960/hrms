
export interface IpermissionType {
    id: number;
    name: string;
    key: string;
}

export const permissionTypesSeedData: IpermissionType[] = [
    {
        id: 1,
        name: 'view',
        key: 'can_view',
    },
    {
        id: 2,
        name: 'edit',
        key: 'can_edit',
    },
    {
        id: 3,
        name: 'add',
        key: 'can_add',
    },
    {
        id: 4,
        name: 'delete',
        key: 'can_delete',
    },
    {
        id: 5,
        name: 'change password',
        key: 'can_change_password',
    },
    {
        id: 6,
        name: 'approve',
        key: 'can_approve',
    },
    {
        id: 7,
        name: 'reject',
        key: 'can_reject',
    },
    {
        id: 8,
        name: 'publish',
        key: 'can_publish',
    }
];

