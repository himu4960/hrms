export interface IMaritalStatus {
    id: number;
    company_id?: number;
    is_active?: boolean;
    is_deleted?: boolean;
}

export interface IMaritalStatusTranslation {
    id?: number;
    name: string;
    marital_status_id?: number;
    language_code?: string;
}

export const maritalStatusesSeedData: IMaritalStatus[] = [
    {
        id: 1,
    },
    {
        id: 2,
    },
    {
        id: 3,
    },
    {
        id: 4,
    },
];

export const maritalStatusTranslationsSeedData: IMaritalStatusTranslation[] = [
    {
        id: 1,
        name: 'Single',
        marital_status_id: 1,
        language_code: 'en',
    },
    {
        id: 2,
        name: 'অবিবাহিত',
        marital_status_id: 1,
        language_code: 'bn',
    },
    {
        id: 3,
        name: 'Married',
        marital_status_id: 2,
        language_code: 'en',
    },
    {
        id: 4,
        name: 'বিবাহিত',
        marital_status_id: 2,
        language_code: 'bn',
    },
    {
        id: 5,
        name: 'Divorced',
        marital_status_id: 3,
        language_code: 'en',
    },
    {
        id: 6,
        name: 'তালাকপ্রাপ্ত',
        marital_status_id: 3,
        language_code: 'bn',
    },
    {
        id: 7,
        name: 'Other',
        marital_status_id: 4,
        language_code: 'en',
    },
    {
        id: 8,
        name: 'অন্যান্য',
        marital_status_id: 4,
        language_code: 'bn',
    },
];
