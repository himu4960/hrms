import { superAdminCasbin, employeeCasbin, managerCasbin, ownerCasbin } from './permission/casbin';

export interface ICasbin {
    id?: number;
    ptype: string;
    v0: string;
    v1: string;
    v2: string;
    v3: string;
    v4?: string;
    v5?: string;
    v6?: string;
}

export const casbinSeedData: ICasbin[] = [...superAdminCasbin, ...ownerCasbin, ...managerCasbin, ...employeeCasbin];
