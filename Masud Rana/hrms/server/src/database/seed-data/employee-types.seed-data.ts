export interface IEmployeeType {
    id: number;
    name?: string;
    company_id?: number;
    is_active?: boolean;
    is_deleted?: boolean;
}

export const employeeTypesSeedData: IEmployeeType[] = [
    {
        id: 1,
        name: 'Full Time',
    },
    {
        id: 2,
        name: 'Part Time',
    },
    {
        id: 3,
        name: 'Seasonal',
    },
    {
        id: 4,
        name: 'Temporary',
    },
    {
        id: 5,
        name: 'Contractor',
    },
    {
        id: 6,
        name: 'Volunteer',
    },
];
