export interface IGender {
    id?: number;
    company_id?: number;
    is_active?: boolean;
    is_deleted?: boolean;
}

export interface IGenderTranslation {
    id?: number;
    name: string;
    gender_id?: number;
    language_code?: string;
}

export const gendersSeedData: IGender[] = [
    {
        id: 1,
    },
    {
        id: 2,
    },
    {
        id: 3,
    },
];

export const genderTranslationsSeedData: IGenderTranslation[] = [
    {
        id: 1,
        name: 'Male',
        gender_id: 1,
        language_code: 'en',
    },
    {
        id: 2,
        name: 'পুরুষ',
        gender_id: 1,
        language_code: 'bn',
    },
    {
        id: 3,
        name: 'Female',
        gender_id: 2,
        language_code: 'en',
    },
    {
        id: 4,
        name: 'মহিলা',
        gender_id: 2,
        language_code: 'bn',
    },
    {
        id: 5,
        name: 'Other',
        gender_id: 3,
        language_code: 'en',
    },
    {
        id: 6,
        name: 'অন্যান্য',
        gender_id: 3,
        language_code: 'bn',
    },
];
