const getPreviousDate = (date: number): number => {
    const myDate = new Date();
    return new Date(myDate.setDate(myDate.getDate() - date)).getTime();
};

export interface IEmployee {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    invitation_accepted: boolean;
    company_id: number;
    branch_id: number;
    dob: number;
    join_date: number;
}

export const employeesSeedData: IEmployee[] = [
    {
        id: 1,
        first_name: 'Habiba',
        last_name: ' Eva',
        email: 'habiba.mu45@gmail.com',
        invitation_accepted: true,
        company_id: 1,
        branch_id: 1,
        dob: new Date('1997-08-01').getTime(),
        join_date: getPreviousDate(1),
    },
    {
        id: 2,
        first_name: 'Mazharul',
        last_name: 'Islam',
        email: 'mazharul@mindorbital.com',
        invitation_accepted: true,
        company_id: 1,
        branch_id: 1,
        dob: new Date('1997-10-01').getTime(),
        join_date: getPreviousDate(2),
    },
    {
        id: 3,
        first_name: 'Mahbubur',
        last_name: 'Rahman',
        email: 'mahbub.095@gmail.com',
        invitation_accepted: true,
        company_id: 1,
        branch_id: 1,
        dob: new Date().getTime(),
        join_date: Date.now(),
    },
    {
        id: 4,
        first_name: 'Masud',
        last_name: 'Rana',
        email: 'masudrana@localhost.it',
        invitation_accepted: true,
        company_id: 1,
        branch_id: 1,
        dob: new Date('1997-12-30').getTime(),
        join_date: getPreviousDate(-1),
    },
    {
        id: 5,
        first_name: 'Dev',
        last_name: 'Tertha',
        email: 'devteertha28@gmail.com',
        invitation_accepted: true,
        company_id: 1,
        branch_id: 1,
        dob: new Date('1997-01-01').getTime(),
        join_date: new Date('2022-01-15').getTime(),
    },
];
