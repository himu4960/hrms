export interface IUser {
    id: number;
    username: string;
    password: string;
    password_reset_at: number;
    firstName: string;
    lastName: string;
    email: string;
    langKey: string;
}

const date = new Date();

export const usersSeedData: IUser[] = [
    {
        id: 1,
        username: 'eva',
        password: 'eva',
        password_reset_at: date.setMonth(date.getMonth() + 1),
        firstName: 'Habiba',
        lastName: 'Eva',
        email: 'habiba.mu45@gmail.com',
        langKey: 'en',
    },
    {
        id: 2,
        username: 'mazharul',
        password: 'mazharul',
        password_reset_at: date.setMonth(date.getMonth() + 1),
        firstName: 'Mazharul',
        lastName: 'Islam',
        email: 'mazharul@mindorbital.com',
        langKey: 'en',
    },
    {
        id: 3,
        username: 'admin',
        password: 'admin',
        password_reset_at: date.setMonth(date.getMonth() + 1),
        firstName: 'Mahbubur',
        lastName: 'Rahman',
        email: 'mahbub.095@gmail.com',
        langKey: 'en',
    },
    {
        id: 4,
        username: 'masudrana',
        password: 'masudrana',
        firstName: 'Masud',
        password_reset_at: date.setMonth(date.getMonth() + 1),
        lastName: 'Rana',
        email: 'himu4960@gmail.com',
        langKey: 'en',
    },
    {
        id: 5,
        username: 'dev',
        password: 'dev',
        password_reset_at: date.setMonth(date.getMonth() + 1),
        firstName: 'Dev',
        lastName: 'Tertha',
        email: 'devteertha28@gmail.com',
        langKey: 'en',
    },
];
