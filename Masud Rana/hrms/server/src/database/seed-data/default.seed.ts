import { DesignationDTO } from './../../service/dto/designation/designation.dto';
import { BreakRuleCreateDTO } from './../../service/dto/break-rule/break-rule-create.dto';
import { BranchDTO } from './../../service/dto/branch/branch.dto';
import { CompanyDTO } from './../../service/dto/company.dto';

export const defaultCompany: CompanyDTO = {
    id: 1,
    name: 'Mind Orbital Technologies Ltd.',
    email: 'info@mindorbital.com',
    phone: '01728478348',
    address: 'Janata Housing, Shymoli, Dhaka',
    company_type_id: 1,
    application_type_id: 1,
    status: true,
};

export const defaultBreakRule: BreakRuleCreateDTO = {
    name: '30-minute unpaid break',
};

export const defaultBranch: BranchDTO = {
    name: 'Main Branch',
    is_default: true,
    break_rule_id: 1,
    time_zone_id: 1,
    is_primary: true,
};

export const defaultDesignations: DesignationDTO[] = [
    {
        id: 1,
        name: 'Manager',
        company_id: defaultCompany.id,
        branch_id: 1,
    },
    {
        id: 2,
        name: 'Accountant',
        company_id: defaultCompany.id,
        branch_id: 1,
    },
    {
        id: 3,
        name: 'Human Resource',
        company_id: 1,
        branch_id: 1,
    },
    {
        id: 4,
        name: 'Software Engineer',
        company_id: defaultCompany.id,
        branch_id: 1,
    },
    {
        id: 5,
        name: 'Full Stack Developer',
        company_id: defaultCompany.id,
        branch_id: 1,
    },
    {
        id: 6,
        name: 'UI/UX Designer',
        company_id: defaultCompany.id,
        branch_id: 1,
    },
];

export enum DEFAULT_COMPANY_SETTINGS {
    LOCALE_COUNTRY_ID = 18, //Bangladesh
    LOCALE_TIME_ZONE_ID = 73, //Bangladesh Standard Time
}
