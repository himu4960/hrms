import { owner, superAdmin, manager, employee } from './permission/role';

export interface IpermissionType {
    name: string;
    key: string;
}

export interface IMenu {
    id: number;
    name: string;
    icon: string;
    parent_id: number;
    router_link: string;
    end_point: string;
    position: number;
    is_menu: boolean;
    can_view: boolean;
    can_add: boolean;
    can_edit: boolean;
    can_delete: boolean;
    can_approve?: boolean;
    can_reject?: boolean;
    can_publish?: boolean;
    can_change_password: boolean;
    permission_type: IpermissionType[];
    children: [];
    translate_name?: string;
}

export interface IRole {
    id: number;
    name: string;
    menus?: IMenu[] | null;
    application_type_id: number;
    is_active: boolean;
    is_deleted: boolean;
    is_employee: boolean;
}

export const rolesSeedData: IRole[] = [
    {
        id: 1,
        name: 'SUPER_ADMIN',
        application_type_id: 1,
        menus: superAdmin,
        is_active: true,
        is_deleted: false,
        is_employee: false,
    },
    {
        id: 2,
        name: 'OWNER',
        application_type_id: 1,
        menus: owner,
        is_active: true,
        is_deleted: false,
        is_employee: false,
    },
    {
        id: 3,
        name: 'MANAGER',
        application_type_id: 1,
        menus: manager,
        is_active: true,
        is_deleted: false,
        is_employee: false,
    },
    {
        id: 4,
        name: 'EMPLOYEE',
        application_type_id: 2,
        menus: employee,
        is_active: true,
        is_deleted: false,
        is_employee: true,
    },
];
