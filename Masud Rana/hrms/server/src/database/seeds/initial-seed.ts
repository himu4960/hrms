import { Connection } from 'typeorm';
import { factory, Factory, Seeder } from 'typeorm-seeding';

import { Language } from '../../domain/language.entity';
import { BloodGroup } from '../../domain/blood-group.entity';
import { BloodGroupTranslation } from '../../domain/blood-group-translation.entity';
import { Gender } from './../../domain/gender.entity';
import { GenderTranslation } from './../../domain/gender-translation.entity';
import { MaritalStatus } from './../../domain/marital-status.entity';
import { MaritalStatusTranslation } from './../../domain/marital-status-translation.entity';
import { ReligionTranslation } from './../../domain/religion-translation.entity';
import { Religion } from './../../domain/religion.entity';
import { ApplicationType } from './../../domain/application-type.entity';
import { CompanyType } from './../../domain/company-type.entity';
import { EmployeeType } from './../../domain/employee-type.entity';
import { Country } from './../../domain/country.entity';
import { TimeZone } from './../../domain/time-zone.entity';
import { PermissionType } from './../../domain/permission-type.entity';
import { Casbin } from './../../domain/casbin.entity';
import { Menu } from './../../domain/menu.entity';
import { Role } from './../../domain/role.entity';
import { CustomFieldType } from '../../domain/custom-field-type.entity';
import { User } from './../../domain/user.entity';
import { SettingsLeaveAvailability } from './../../domain/settings-leave-availability.entity';
import { SettingsShiftPlanning } from './../../domain/settings-shift-planning.entity';
import { SettingsApplication } from './../../domain/settings-application.entity';
import { SettingsTimeClock } from './../../domain/settings-time-clock.entity';
import { Designation } from './../../domain/designation.entity';
import { Branch } from './../../domain/branch.entity';
import { BreakRule } from './../../domain/break-rule.entity';
import { Company } from './../../domain/company.entity';
import { Employee } from './../../domain/employee.entity';
import { UserCompany } from './../../domain/user-company.entity';

import {
    defaultBranch,
    defaultBreakRule,
    defaultCompany,
    defaultDesignations,
    DEFAULT_COMPANY_SETTINGS,
} from './../seed-data/default.seed';
import { languagesSeedData } from '../seed-data/languages.seed-data';
import { applicationTypesSeedData } from './../seed-data/application-types.seed-data';
import { companyTypesSeedData } from './../seed-data/company-types.seed-data';
import { bloodGroupsSeedData, BloodGroupTranslationsSeedData } from '../seed-data/blood-groups.seed-data';
import { gendersSeedData, genderTranslationsSeedData } from '../seed-data/genders.seed-data';
import { maritalStatusesSeedData, maritalStatusTranslationsSeedData } from '../seed-data/marital-statuses.seed-data';
import { religionsSeedData, religionTranslationsSeedData } from './../seed-data/religions.seed-data';
import { employeeTypesSeedData } from './../seed-data/employee-types.seed-data';
import { countriesSeedData } from '../seed-data/countries.seed-data';
import { timeZonesSeedData } from '../seed-data/time-zones.seed-data';
import { permissionTypesSeedData } from '../seed-data/permission-types.seed-data';
import { casbinSeedData } from '../seed-data/casbin.seed-data';
import { menusSeedData } from '../seed-data/menu.seed-data';
import { rolesSeedData } from '../seed-data/roles.seed-data';
import { customFieldTypeSeedData } from '../seed-data/custom-field-type.seed-data';
import { usersSeedData } from '../seed-data/user.seed-data';

import { bcryptedPassword } from '../../security';
import { employeesSeedData } from '../seed-data/employee.seed-data';
import { userCompaniesSeedData } from '../seed-data/user-company.seed-data';

export default class InitialDatabaseSeed implements Seeder {
    public async run(factory: Factory, connection: Connection) {
        await this.languageSeeding();
        await this.countrySeeding();
        await this.timeZoneSeeding();
        await this.bloodGroupSeeding();
        await this.genderSeeding();
        await this.maritalStatusSeeding();
        await this.religionSeeding();
        await this.applicationTypeSeeding();
        await this.companyTypeSeeding();
        await this.employeeTypeSeeding();
        await this.permissionTypeSeeding();
        await this.casbinSeeding();
        await this.menuSeeding();
        await this.roleSeeding();
        await this.customFieldTypeSeeding();
        await this.userSeeding();
        await this.companySeeding();
        await this.breakRuleSeeding();
        await this.branchSeeding();
        await this.designationSeeding();
        await this.settingsTimeClockSeeding();
        await this.settingsApplicationSeeding();
        await this.settingsShiftPlanningSeeding();
        await this.settingsLeaveAvailabilitySeeding();
        await this.employeeSeeding();
        await this.userCompanySeeding();
    }

    async casbinSeeding() {
        let casbinIndex = 0;
        await factory(Casbin)()
            .map(async casbin => {
                if (casbinIndex > casbinSeedData.length) {
                    casbinIndex = 0;
                } else {
                    const { ptype, v0, v1, v2, v3 } = casbinSeedData[casbinIndex];
                    casbin.ptype = ptype;
                    casbin.v0 = v0;
                    casbin.v1 = v1;
                    casbin.v2 = v2;
                    casbin.v3 = v3;
                    casbinIndex++;
                    return casbin;
                }
            })
            .createMany(casbinSeedData.length);
    }

    async menuSeeding() {
        let menuIndex = 0;
        await factory(Menu)()
            .map(async menu => {
                if (menuIndex > menusSeedData.length) {
                    menuIndex = 0;
                } else {
                    const {
                        id,
                        name,
                        router_link,
                        end_point,
                        position,
                        parent_id,
                        is_menu,
                        permissions,
                        icon,
                        is_active,
                        is_deleted,
                        translate_name,
                    } = menusSeedData[menuIndex];
                    menu.id = id;
                    menu.name = name;
                    menu.router_link = router_link;
                    menu.end_point = end_point;
                    menu.icon = icon;
                    menu.is_active = is_active;
                    menu.is_deleted = is_deleted;
                    menu.parent_id = parent_id;
                    menu.position = position;
                    menu.is_menu = is_menu;
                    menu.permissions = permissions && JSON.stringify(permissions);
                    menu.translate_name = translate_name;
                    menuIndex++;
                    return menu;
                }
            })
            .createMany(menusSeedData.length);
    }

    async roleSeeding() {
        let roleIndex = 0;
        await factory(Role)()
            .map(async role => {
                if (roleIndex > rolesSeedData.length) {
                    roleIndex = 0;
                } else {
                    const { id, name, application_type_id, menus, is_employee, is_active, is_deleted } = rolesSeedData[
                        roleIndex
                    ];
                    role.id = id;
                    role.name = name;
                    role.application_type_id = application_type_id;
                    role.is_employee = is_employee;
                    role.is_active = is_active;
                    role.is_deleted = is_deleted;
                    role.menus = menus && menus.length > 0 ? JSON.stringify(menus) : null;
                    roleIndex++;
                    return role;
                }
            })
            .createMany(rolesSeedData.length);
    }

    async languageSeeding() {
        let langIndex = 0;
        await factory(Language)()
            .map(async lang => {
                if (langIndex > languagesSeedData.length) {
                    langIndex = 0;
                } else {
                    const { code, name, is_default, is_active, is_deleted } = languagesSeedData[langIndex];
                    lang.code = code;
                    lang.name = name;
                    lang.is_active = is_active;
                    lang.is_deleted = is_deleted;
                    lang.is_default = is_default;
                    langIndex++;
                    return lang;
                }
            })
            .createMany(languagesSeedData.length);
    }

    async countrySeeding() {
        let countryIndex = 0;
        await factory(Country)()
            .map(async country => {
                if (countryIndex > countriesSeedData.length) {
                    countryIndex = 0;
                } else {
                    const { id, name, code, iso_code, is_default } = countriesSeedData[countryIndex];
                    country.id = id;
                    country.name = name;
                    country.code = code;
                    country.iso_code = iso_code;
                    country.is_default = is_default;
                    countryIndex++;
                    return country;
                }
            })
            .createMany(countriesSeedData.length);
    }

    async timeZoneSeeding() {
        let timeZoneIndex = 0;
        await factory(TimeZone)()
            .map(async timeZone => {
                if (timeZoneIndex > timeZonesSeedData.length) {
                    timeZoneIndex = 0;
                } else {
                    const { id, name, abbr, offset, description, is_default } = timeZonesSeedData[timeZoneIndex];
                    timeZone.id = id;
                    timeZone.name = name;
                    timeZone.abbr = abbr;
                    timeZone.offset = offset;
                    timeZone.description = description;
                    timeZone.is_default = is_default;
                    timeZoneIndex++;
                    return timeZone;
                }
            })
            .createMany(timeZonesSeedData.length);
    }

    async bloodGroupSeeding() {
        let bloodGroupIndex = 0;
        await factory(BloodGroup)()
            .map(async bloodGroup => {
                if (bloodGroupIndex > bloodGroupsSeedData.length) {
                    bloodGroupIndex = 0;
                } else {
                    const { id } = bloodGroupsSeedData[bloodGroupIndex];
                    bloodGroup.id = id;
                    bloodGroupIndex++;
                    return bloodGroup;
                }
            })
            .createMany(bloodGroupsSeedData.length);

        let bloodGroupTranslationIndex = 0;
        await factory(BloodGroupTranslation)()
            .map(async bloodGroupTranslation => {
                if (bloodGroupTranslationIndex > BloodGroupTranslationsSeedData.length) {
                    bloodGroupTranslationIndex = 0;
                } else {
                    const { id, name, blood_group_id, language_code } = BloodGroupTranslationsSeedData[
                        bloodGroupTranslationIndex
                    ];
                    bloodGroupTranslation.id = id;
                    bloodGroupTranslation.name = name;
                    bloodGroupTranslation.blood_group_id = blood_group_id;
                    bloodGroupTranslation.language_code = language_code;
                    bloodGroupTranslationIndex++;
                    return bloodGroupTranslation;
                }
            })
            .createMany(BloodGroupTranslationsSeedData.length);
    }

    async genderSeeding() {
        let genderIndex = 0;
        await factory(Gender)()
            .map(async gender => {
                if (genderIndex > gendersSeedData.length) {
                    genderIndex = 0;
                } else {
                    const { id } = gendersSeedData[genderIndex];
                    gender.id = id;
                    genderIndex++;
                    return gender;
                }
            })
            .createMany(gendersSeedData.length);

        let genderTranslationIndex = 0;
        await factory(GenderTranslation)()
            .map(async genderTranslation => {
                if (genderTranslationIndex > genderTranslationsSeedData.length) {
                    genderTranslationIndex = 0;
                } else {
                    const { id, name, gender_id, language_code } = genderTranslationsSeedData[genderTranslationIndex];
                    genderTranslation.id = id;
                    genderTranslation.name = name;
                    genderTranslation.gender_id = gender_id;
                    genderTranslation.language_code = language_code;
                    genderTranslationIndex++;
                    return genderTranslation;
                }
            })
            .createMany(genderTranslationsSeedData.length);
    }

    async maritalStatusSeeding() {
        let maritalStatusIndex = 0;
        await factory(MaritalStatus)()
            .map(async maritalStatus => {
                if (maritalStatusIndex > maritalStatusesSeedData.length) {
                    maritalStatusIndex = 0;
                } else {
                    const { id } = maritalStatusesSeedData[maritalStatusIndex];
                    maritalStatus.id = id;
                    maritalStatusIndex++;
                    return maritalStatus;
                }
            })
            .createMany(maritalStatusesSeedData.length);

        let maritalStatusTranslationIndex = 0;
        await factory(MaritalStatusTranslation)()
            .map(async maritalStatusTranslation => {
                if (maritalStatusTranslationIndex > maritalStatusTranslationsSeedData.length) {
                    maritalStatusTranslationIndex = 0;
                } else {
                    const { id, name, marital_status_id, language_code } = maritalStatusTranslationsSeedData[
                        maritalStatusTranslationIndex
                    ];
                    maritalStatusTranslation.id = id;
                    maritalStatusTranslation.name = name;
                    maritalStatusTranslation.marital_status_id = marital_status_id;
                    maritalStatusTranslation.language_code = language_code;
                    maritalStatusTranslationIndex++;
                    return maritalStatusTranslation;
                }
            })
            .createMany(maritalStatusTranslationsSeedData.length);
    }

    async religionSeeding() {
        let religionIndex = 0;
        await factory(Religion)()
            .map(async religion => {
                if (religionIndex > religionsSeedData.length) {
                    religionIndex = 0;
                } else {
                    const { id } = religionsSeedData[religionIndex];
                    religion.id = id;
                    religionIndex++;
                    return religion;
                }
            })
            .createMany(religionsSeedData.length);

        let religionTranslationIndex = 0;
        await factory(ReligionTranslation)()
            .map(async religionTranslation => {
                if (religionTranslationIndex > religionTranslationsSeedData.length) {
                    religionTranslationIndex = 0;
                } else {
                    const { id, name, religion_id, language_code } = religionTranslationsSeedData[
                        religionTranslationIndex
                    ];
                    religionTranslation.id = id;
                    religionTranslation.name = name;
                    religionTranslation.religion_id = religion_id;
                    religionTranslation.language_code = language_code;
                    religionTranslationIndex++;
                    return religionTranslation;
                }
            })
            .createMany(religionTranslationsSeedData.length);
    }

    async applicationTypeSeeding() {
        let applicationTypeIndex = 0;
        await factory(ApplicationType)()
            .map(async applicationType => {
                if (applicationTypeIndex > applicationTypesSeedData.length) {
                    applicationTypeIndex = 0;
                } else {
                    const { id, name } = applicationTypesSeedData[applicationTypeIndex];
                    applicationType.id = id;
                    applicationType.name = name;
                    applicationTypeIndex++;
                    return applicationType;
                }
            })
            .createMany(applicationTypesSeedData.length);
    }

    async companyTypeSeeding() {
        let companyTypeIndex = 0;
        await factory(CompanyType)()
            .map(async companyType => {
                if (companyTypeIndex > companyTypesSeedData.length) {
                    companyTypeIndex = 0;
                } else {
                    const { id, name, is_active, is_deleted } = companyTypesSeedData[companyTypeIndex];
                    companyType.id = id;
                    companyType.name = name;
                    companyType.is_active = is_active;
                    companyType.is_deleted = is_deleted;
                    companyTypeIndex++;
                    return companyType;
                }
            })
            .createMany(companyTypesSeedData.length);
    }

    async employeeTypeSeeding() {
        let employeeTypeIndex = 0;
        await factory(EmployeeType)()
            .map(async employeeType => {
                if (employeeTypeIndex > languagesSeedData.length) {
                    employeeTypeIndex = 0;
                } else {
                    const { id, name, is_active, is_deleted } = employeeTypesSeedData[employeeTypeIndex];
                    employeeType.id = id;
                    employeeType.name = name;
                    employeeType.is_active = is_active;
                    employeeType.is_deleted = is_deleted;
                    employeeTypeIndex++;
                    return employeeType;
                }
            })
            .createMany(employeeTypesSeedData.length);
    }

    async permissionTypeSeeding() {
        let permissionTypeIndex = 0;
        await factory(PermissionType)()
            .map(async permissionType => {
                if (permissionTypeIndex > permissionTypesSeedData.length) {
                    permissionTypeIndex = 0;
                } else {
                    const { id, name, key } = permissionTypesSeedData[permissionTypeIndex];
                    permissionType.id = id;
                    permissionType.name = name;
                    permissionType.key = key;
                    permissionTypeIndex++;
                    return permissionType;
                }
            })
            .createMany(permissionTypesSeedData.length);
    }

    async customFieldTypeSeeding() {
        let fieldTypeIndex = 0;
        await factory(CustomFieldType)()
            .map(async fieldType => {
                if (fieldTypeIndex > customFieldTypeSeedData.length) {
                    fieldTypeIndex = 0;
                } else {
                    const { id, name, data_type } = customFieldTypeSeedData[fieldTypeIndex];
                    fieldType.id = id;
                    fieldType.name = name;
                    fieldType.data_type = data_type;
                    fieldTypeIndex++;
                    return fieldType;
                }
            })
            .createMany(customFieldTypeSeedData.length);
    }

    async userSeeding() {
        let userIndex = 0;
        await factory(User)()
            .map(async user => {
                if (userIndex > usersSeedData.length) {
                    userIndex = 0;
                } else {
                    const {
                        id,
                        username,
                        password,
                        password_reset_at,
                        firstName,
                        lastName,
                        email,
                        langKey,
                    } = usersSeedData[userIndex];
                    user.id = id;
                    user.username = username;
                    user.password = await bcryptedPassword(password);
                    user.password_reset_at = password_reset_at;
                    user.firstName = firstName;
                    user.lastName = lastName;
                    user.email = email;
                    user.langKey = langKey;
                    userIndex++;
                    return user;
                }
            })
            .createMany(usersSeedData.length);
    }

    async companySeeding() {
        await factory(Company)()
            .map(async company => {
                const {
                    id,
                    name,
                    email,
                    phone,
                    address,
                    company_type_id,
                    application_type_id,
                    status,
                } = defaultCompany;
                company.id = id;
                company.name = name;
                company.email = email;
                company.phone = phone;
                company.address = address;
                company.company_type_id = company_type_id;
                company.application_type_id = application_type_id;
                company.status = status;
                return company;
            })
            .create();
    }

    async breakRuleSeeding() {
        await factory(BreakRule)()
            .map(async breakRule => {
                const { name } = defaultBreakRule;
                breakRule.id = 1;
                breakRule.name = name;
                breakRule.company_id = defaultCompany.id;
                return breakRule;
            })
            .create();
    }

    async branchSeeding() {
        await factory(Branch)()
            .map(async branch => {
                const { name, is_default, break_rule_id, time_zone_id, is_primary } = defaultBranch;
                branch.id = 1;
                branch.name = name;
                branch.is_default = is_default;
                branch.break_rule_id = break_rule_id;
                branch.time_zone_id = time_zone_id;
                branch.is_primary = is_primary;
                branch.company_id = defaultCompany.id;
                return branch;
            })
            .create();
    }

    async designationSeeding() {
        let designationIndex = 0;
        await factory(Designation)()
            .map(async designation => {
                if (designationIndex > defaultDesignations.length) {
                    designationIndex = 0;
                } else {
                    const { id, name, company_id, branch_id } = defaultDesignations[designationIndex];
                    designation.id = id;
                    designation.name = name;
                    designation.company_id = company_id;
                    designation.branch_id = branch_id;
                    designationIndex++;
                    return designation;
                }
            })
            .createMany(defaultDesignations.length);
    }

    async settingsTimeClockSeeding() {
        await factory(SettingsTimeClock)()
            .map(async settingsTimeClock => {
                settingsTimeClock.id = 1;
                settingsTimeClock.company_id = defaultCompany.id;
                return settingsTimeClock;
            })
            .create();
    }

    async settingsApplicationSeeding() {
        await factory(SettingsApplication)()
            .map(async settingsApplication => {
                settingsApplication.id = 1;
                settingsApplication.company_id = defaultCompany.id;
                settingsApplication.locale_country_id = DEFAULT_COMPANY_SETTINGS.LOCALE_COUNTRY_ID;
                settingsApplication.locale_timezone_id = DEFAULT_COMPANY_SETTINGS.LOCALE_TIME_ZONE_ID;
                return settingsApplication;
            })
            .create();
    }

    async settingsShiftPlanningSeeding() {
        await factory(SettingsShiftPlanning)()
            .map(async settingsShiftPlanning => {
                settingsShiftPlanning.id = 1;
                settingsShiftPlanning.company_id = defaultCompany.id;
                return settingsShiftPlanning;
            })
            .create();
    }

    async settingsLeaveAvailabilitySeeding() {
        await factory(SettingsLeaveAvailability)()
            .map(async settingsLeaveAvailability => {
                settingsLeaveAvailability.id = 1;
                settingsLeaveAvailability.company_id = defaultCompany.id;
                return settingsLeaveAvailability;
            })
            .create();
    }

    async employeeSeeding() {
        let employeeIndex = 0;
        await factory(Employee)()
            .map(async employee => {
                if (employeeIndex > employeesSeedData.length) {
                    employeeIndex = 0;
                } else {
                    const {
                        id,
                        first_name,
                        last_name,
                        invitation_accepted,
                        company_id,
                        branch_id,
                        dob,
                        join_date,
                        email,
                    } = employeesSeedData[employeeIndex];
                    employee.id = id;
                    employee.first_name = first_name;
                    employee.last_name = last_name;
                    employee.email = email;
                    employee.invitation_accepted = invitation_accepted;
                    employee.company_id = company_id;
                    employee.branch_id = branch_id;
                    employee.dob = dob;
                    employee.join_date = join_date;
                    employeeIndex++;
                    return employee;
                }
            })
            .createMany(employeesSeedData.length);
    }

    async userCompanySeeding() {
        let userCompanyIndex = 0;
        await factory(UserCompany)()
            .map(async userCompany => {
                if (userCompanyIndex > userCompaniesSeedData.length) {
                    userCompanyIndex = 0;
                } else {
                    const { id, is_owner, user_id, company_id, role_id, employee_id } = userCompaniesSeedData[
                        userCompanyIndex
                    ];
                    userCompany.id = id;
                    userCompany.is_owner = is_owner;
                    userCompany.user_id = user_id;
                    userCompany.company_id = company_id;
                    userCompany.role_id = role_id;
                    userCompany.employee_id = employee_id;
                    userCompanyIndex++;
                    return userCompany;
                }
            })
            .createMany(userCompaniesSeedData.length);
    }
}
