import { define } from 'typeorm-seeding';
import { Branch } from './../../domain/branch.entity';

define(Branch, () => {
    const branch = new Branch();
    return branch;
});
