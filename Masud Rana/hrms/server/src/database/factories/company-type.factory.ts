import { define } from 'typeorm-seeding';
import { CompanyType } from './../../domain/company-type.entity';

define(CompanyType, () => {
    const companyType = new CompanyType();
    companyType.is_active = true;
    companyType.is_deleted = false;
    return companyType;
});
