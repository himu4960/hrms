import { define } from 'typeorm-seeding';
import { TimeZone } from './../../domain/time-zone.entity';

define(TimeZone, () => {
    const timeZone = new TimeZone();
    return timeZone;
});
