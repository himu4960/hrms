import { define } from 'typeorm-seeding';
import { Company } from './../../domain/company.entity';

define(Company, () => {
    const company = new Company();
    return company;
});
