import { define } from 'typeorm-seeding';
import { User } from './../../domain/user.entity';

define(User, () => {
    const user = new User();
    return user;
});
