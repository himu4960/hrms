import { define } from 'typeorm-seeding';
import { Casbin } from '../../domain/casbin.entity';

define(Casbin, () => {
    const casbin = new Casbin();
    return casbin;
});
