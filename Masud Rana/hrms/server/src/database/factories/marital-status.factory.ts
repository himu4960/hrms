import { define } from 'typeorm-seeding';
import { MaritalStatus } from '../../domain/marital-status.entity';

define(MaritalStatus, () => {
    const maritalStatus = new MaritalStatus();
    maritalStatus.is_active = true;
    maritalStatus.is_deleted = false;
    return maritalStatus;
});
