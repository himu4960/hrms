import { define } from 'typeorm-seeding';
import { Employee } from './../../domain/employee.entity';

define(Employee, () => {
    const employee = new Employee();
    return employee;
});
