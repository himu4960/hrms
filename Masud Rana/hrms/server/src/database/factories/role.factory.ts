import { define } from 'typeorm-seeding';
import { Role } from './../../domain/role.entity';

define(Role, () => {
    const role = new Role();
    return role;
});
