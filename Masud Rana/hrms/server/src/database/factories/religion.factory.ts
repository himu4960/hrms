import { define } from 'typeorm-seeding';
import { Religion } from './../../domain/religion.entity';

define(Religion, () => {
    const religion = new Religion();
    religion.is_active = true;
    religion.is_deleted = false;
    return religion;
});
