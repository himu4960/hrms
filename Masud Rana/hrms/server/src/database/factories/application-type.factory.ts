import { define } from 'typeorm-seeding';
import { ApplicationType } from './../../domain/application-type.entity';

define(ApplicationType, () => {
    const applicationType = new ApplicationType();
    applicationType.is_active = true;
    applicationType.is_deleted = false;
    return applicationType;
});
