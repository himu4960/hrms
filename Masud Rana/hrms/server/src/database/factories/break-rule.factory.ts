import { define } from 'typeorm-seeding';
import { BreakRule } from './../../domain/break-rule.entity';

define(BreakRule, () => {
    const breakRule = new BreakRule();
    return breakRule;
});
