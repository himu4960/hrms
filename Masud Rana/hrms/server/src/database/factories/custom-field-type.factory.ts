import { define } from 'typeorm-seeding';

import { CustomFieldType } from '../../domain/custom-field-type.entity';

define(CustomFieldType, () => {
    const customFieldType = new CustomFieldType();
    return customFieldType;
});
