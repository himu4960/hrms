import { define } from 'typeorm-seeding';
import { UserCompany } from './../../domain/user-company.entity';

define(UserCompany, () => {
    const userCompany = new UserCompany();
    return userCompany;
});
