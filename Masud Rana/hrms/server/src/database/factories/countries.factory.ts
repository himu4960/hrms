import { define } from 'typeorm-seeding';
import { Country } from './../../domain/country.entity';

define(Country, () => {
    const country = new Country();
    return country;
});
