import { define } from 'typeorm-seeding';
import { Language } from '../../domain/language.entity';

define(Language, () => {
    const language = new Language();
    return language;
});
