import { define } from 'typeorm-seeding';

import { SettingsTimeClock } from './../../domain/settings-time-clock.entity';
import { SettingsApplication } from './../../domain/settings-application.entity';
import { SettingsShiftPlanning } from './../../domain/settings-shift-planning.entity';
import { SettingsLeaveAvailability } from './../../domain/settings-leave-availability.entity';

define(SettingsTimeClock, () => {
    const settingsTimeClock = new SettingsTimeClock();
    return settingsTimeClock;
});

define(SettingsApplication, () => {
    const settingsApplication = new SettingsApplication();
    return settingsApplication;
});

define(SettingsShiftPlanning, () => {
    const settingsShiftPlanning = new SettingsShiftPlanning();
    return settingsShiftPlanning;
});

define(SettingsLeaveAvailability, () => {
    const settingsLeaveAvailability = new SettingsLeaveAvailability();
    return settingsLeaveAvailability;
});
