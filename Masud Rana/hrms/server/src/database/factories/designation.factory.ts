import { define } from 'typeorm-seeding';
import { Designation } from './../../domain/designation.entity';

define(Designation, () => {
    const designation = new Designation();
    return designation;
});
