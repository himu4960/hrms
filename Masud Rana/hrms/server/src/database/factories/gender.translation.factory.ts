import { define } from 'typeorm-seeding';
import { GenderTranslation } from './../../domain/gender-translation.entity';

define(GenderTranslation, () => {
    const genderTranslation = new GenderTranslation();
    return genderTranslation;
});
