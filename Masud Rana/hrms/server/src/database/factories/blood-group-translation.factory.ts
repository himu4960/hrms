import { define } from 'typeorm-seeding';
import { BloodGroupTranslation } from '../../domain/blood-group-translation.entity';

define(BloodGroupTranslation, () => {
    const bloodGroupTranslation = new BloodGroupTranslation();
    return bloodGroupTranslation;
});
