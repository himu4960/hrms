import { define } from 'typeorm-seeding';
import { Gender } from './../../domain/gender.entity';

define(Gender, () => {
    const gender = new Gender();
    gender.is_active = true;
    gender.is_deleted = false;
    return gender;
});
