import { define } from 'typeorm-seeding';
import { PermissionType } from './../../domain/permission-type.entity';

define(PermissionType, () => {
    const permissionType = new PermissionType();
    return permissionType;
});
