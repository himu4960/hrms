import { define } from 'typeorm-seeding';
import { BloodGroup } from '../../domain/blood-group.entity';

define(BloodGroup, () => {
    const bloodGroup = new BloodGroup();
    bloodGroup.is_active = true;
    bloodGroup.is_deleted = false;
    return bloodGroup;
});
