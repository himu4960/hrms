import { define } from 'typeorm-seeding';
import { ReligionTranslation } from './../../domain/religion-translation.entity';

define(ReligionTranslation, () => {
    const religionTranslation = new ReligionTranslation();
    return religionTranslation;
});
