import { define } from 'typeorm-seeding';
import { MaritalStatusTranslation } from './../../domain/marital-status-translation.entity';

define(MaritalStatusTranslation, () => {
    const maritalStatusTranslation = new MaritalStatusTranslation();
    return maritalStatusTranslation;
});
