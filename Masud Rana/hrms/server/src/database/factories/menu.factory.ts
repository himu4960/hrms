import { define } from 'typeorm-seeding';
import { Menu } from '../../domain/menu.entity';

define(Menu, () => {
    const menu = new Menu();
    return menu;
});
