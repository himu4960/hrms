import { define } from 'typeorm-seeding';
import { EmployeeType } from './../../domain/employee-type.entity';

define(EmployeeType, () => {
    const employeeType = new EmployeeType();
    employeeType.is_active = true;
    employeeType.is_deleted = false;
    return employeeType;
});
