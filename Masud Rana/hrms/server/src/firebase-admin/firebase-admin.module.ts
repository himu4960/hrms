import { Global, Module, DynamicModule, Logger } from '@nestjs/common';
import { ValueProvider } from '@nestjs/common/interfaces/modules/provider.interface';
import { FirebaseAdminOptions } from './interfaces/firebase-admin-options.interface';

import { FirebaseAdminService } from './services/firebase-admin.service';

import { FCM_OPTIONS } from './firebase-admin.constants';

@Global()
@Module({})
export class FirebaseAdminModule {
  static forRoot(options: FirebaseAdminOptions): DynamicModule {
    const optionsProvider: ValueProvider = {
      provide: FCM_OPTIONS,
      useValue: options,
    };
    const logger = options.logger ? options.logger : new Logger('FirebaseAdminService');
    return {
      module: FirebaseAdminModule,
      providers: [
        { provide: Logger, useValue: logger },
        FirebaseAdminService,
        optionsProvider,
      ],
      exports: [FirebaseAdminService],
    };
  }
}
