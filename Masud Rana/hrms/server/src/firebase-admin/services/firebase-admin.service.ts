import { Injectable } from '@nestjs/common/decorators/core/injectable.decorator';
import { Inject, Logger } from '@nestjs/common';
import * as firebaseAdmin from 'firebase-admin';

import { FCM_OPTIONS } from '../firebase-admin.constants';

import { FirebaseAdminOptions } from '../interfaces/firebase-admin-options.interface';

const options = {
  priority: 'high',
  timeToLive: 60 * 60 * 24,
};

const optionsSilent = {
  priority: 'high',
  timeToLive: 60 * 60 * 24,
  content_available: true,
};

@Injectable()
export class FirebaseAdminService {
  constructor(
    @Inject(FCM_OPTIONS) private fcmOptionsProvider: FirebaseAdminOptions,
    private readonly logger: Logger,
  ) {
    const serviceAccount = require(this.fcmOptionsProvider.firebaseSpecsPath);
    firebaseAdmin.initializeApp({
      credential: firebaseAdmin.credential.cert(serviceAccount)
    });
  }

  async sendToTopic(
    topic: string,
    payload: firebaseAdmin.messaging.MessagingPayload,
    silent?: boolean,
  ) {
    try {
      return await firebaseAdmin
        .messaging()
        .sendToTopic(topic, payload, silent ? optionsSilent : options);
    } catch (error) {
      throw error;
    }
  }

  async send(payload: firebaseAdmin.messaging.Message, silent?: boolean) {
    try {
      return await firebaseAdmin.messaging().send(payload, silent);
    } catch (error) {
      throw error;
    }
  }

  async sendToDevice(
    deviceIds: Array<string>,
    payload: firebaseAdmin.messaging.MessagingPayload,
    silent: boolean,
  ) {

    if (deviceIds.length == 0) {
      throw new Error('You provide an empty device ids list!');
    }

    try {
      return await firebaseAdmin
        .messaging()
        .sendToDevice(deviceIds, payload, silent ? optionsSilent : options);
    } catch (error) {
      throw error;
    }
  }

  async subscribeToTopic(deviceIds: string | string[], topic: string) {
    try {
      return await firebaseAdmin.messaging().subscribeToTopic(deviceIds, topic);
    } catch (error) {
      throw error;
    }
  }

  async unsubscribeFromTopic(deviceIds: string | string[], topic: string) {
    try {
      return await firebaseAdmin.messaging().unsubscribeFromTopic(deviceIds, topic);
    } catch (error) {
      throw error;
    }
  }
}
