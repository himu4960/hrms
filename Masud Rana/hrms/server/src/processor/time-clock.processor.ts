import { Logger } from '@nestjs/common';
import { Process, Processor } from '@nestjs/bull';
import { InjectRepository } from '@nestjs/typeorm';
import { Job } from 'bull';

import { ETimeClockEventType } from './../domain/enum/time-clock.enum';
import { TimeClockEventDTO } from './../service/dto/time-clock/time-clock-event.dto';

import { TimeClockRepository } from './../repository/time-clock.repository';

import { DateTimeConvertService } from './../shared/convert/date-time-convert.service';

@Processor('time-clock')
export class TimeClockProcessor {
    private readonly logger = new Logger(TimeClockProcessor.name);

    constructor(
        @InjectRepository(TimeClockRepository) private timeClockRepository: TimeClockRepository,
        private dateTimeConvertService: DateTimeConvertService,
    ) {}

    @Process('auto-clock-out')
    async handleTranscode(job: Job) {
        this.logger.debug('Start transcoding...');
        const time_clock_id = job.data.time_clock_id;

        const currentTime = Date.now();

        if (time_clock_id) {
            const timeClock = await this.timeClockRepository
                .createQueryBuilder('timeClock')
                .leftJoinAndSelect('timeClock.work_shift', 'work_shift')
                .leftJoinAndSelect('timeClock.time_clock_events', 'time_clock_events')
                .where('timeClock.id = :time_clock_id', { time_clock_id })
                .andWhere('clock_end_time is null')
                .getOne();

            if (timeClock) {
                let total_break_duration = 0;

                timeClock?.time_clock_events.forEach((event: TimeClockEventDTO) => {
                    if (event.event_type === ETimeClockEventType.BREAK && event?.total_break_duration) {
                        total_break_duration = total_break_duration + event.total_break_duration;
                    }
                });
                const total_clock_duration = this.dateTimeConvertService.getDateDifferenceInSeconds(
                    currentTime,
                    timeClock.clock_start_time,
                );

                await this.timeClockRepository.update(
                    { id: time_clock_id },
                    {
                        is_running: false,
                        clock_end_time: currentTime,
                        total_clock_duration,
                        total_break_duration,
                    },
                );
            }
        }
        this.logger.debug('Transcoding completed');
    }
}
