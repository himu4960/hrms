import { Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Process, Processor } from '@nestjs/bull';
import { Job } from 'bull';

import { WorkShiftRepository } from './../repository/work-shift.repository';
import { LeaveRepository } from './../repository/leave.repository';

import { EmployeeService } from './../service/employee.service';
import { DateTimeConvertService } from '../shared/convert/date-time-convert.service';

import { CreateManyWorkShiftsDTO } from './../service/dto/work-shift/work-shift-create-many.dto';
import { WorkShiftCreateDTO } from './../service/dto/work-shift/work-shift-create.dto';
import { UserCompanyForWorkShiftDTO } from './../service/dto/user-company/user-company-for-work-shift.dto';

@Processor('work-shift')
export class WorkShiftProcessor {
    private readonly logger = new Logger(WorkShiftProcessor.name);

    constructor(
        @InjectRepository(WorkShiftRepository) private workShiftRepository: WorkShiftRepository,
        @InjectRepository(LeaveRepository) private leaveRepository: LeaveRepository,
        private dateTimeConvertService: DateTimeConvertService,
        private employeeService: EmployeeService,
    ) {}

    @Process('create-work-shifts-at-once')
    async handleTranscode(job: Job) {
        this.logger.log('Work shifts are creating');
        const workShiftCreateManyDTO = job.data.work_shift_create_many;
        const company_id = job.data.company_id;
        const workShifts: WorkShiftCreateDTO[] = await this.createManyWorkShifts(workShiftCreateManyDTO, company_id);
        await this.workShiftRepository.save(workShifts);
    }

    async createManyWorkShifts(payload: CreateManyWorkShiftsDTO, company_id: number): Promise<WorkShiftCreateDTO[]> {
        let workShifts: WorkShiftCreateDTO[] = [];
        const { start_date, end_date, start_time, end_time, employees, created_by } = payload;
        const days = this.dateTimeConvertService.differenceInDays(start_date, end_date);

        for (let i = 0; i < days + 1; i++) {
            const new_start_date = this.dateTimeConvertService.addDays(start_date, i);
            const new_end_date = this.dateTimeConvertService.addDays(end_date, i - days);
            for (let j = 0; j < employees.length; j++) {
                const employee = employees[j];
                const findUserCompany: UserCompanyForWorkShiftDTO = await this.employeeService.findDetailsToCreateWorkShift(
                    employee.id,
                    company_id,
                    new_start_date,
                );
                const findEmployee = findUserCompany.employee;
                if (findEmployee.employee_designations.length && !findEmployee.leaves.length) {
                    const total_work_shift_duration: number = (new_end_date - new_start_date) / 1000;
                    const employees = [
                        {
                            company_id,
                            branch_id: findEmployee.branch_id,
                            id: findEmployee.id,
                            designation_id: findEmployee.employee_designations[0].designation_id,
                        },
                    ];
                    const workShiftCreateDTO: WorkShiftCreateDTO = {
                        title: `work shift ${i + 1}`,
                        branch_id: findEmployee.branch_id,
                        designation_id: findEmployee.employee_designations[0].designation_id,
                        employee_id: findEmployee.id,
                        start_date: new_start_date,
                        start_time,
                        end_time,
                        end_date: new_end_date,
                        total_work_shift_duration,
                        company_id,
                        created_by,
                        employees,
                    };
                    workShifts.push(workShiftCreateDTO);
                }
            }
        }

        this.logger.log('Work shifts have been created');
        return workShifts;
    }
}
