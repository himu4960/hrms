import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';
import { Response } from 'express';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: HttpException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const status = exception.getStatus();
        response
            .status(status)
            .json({
                status: false,
                statusCode: status,
                message: exception['response'].message,
                translateCode: exception['response'].translateCode || null,
                data: exception['response'].data || null
            });
    }
}
