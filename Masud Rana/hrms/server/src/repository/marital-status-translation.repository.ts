import { EntityRepository, Repository } from 'typeorm';
import { MaritalStatusTranslation } from '../domain/marital-status-translation.entity';

@EntityRepository(MaritalStatusTranslation)
export class MaritalStatusTranslationRepository extends Repository<MaritalStatusTranslation> {}
