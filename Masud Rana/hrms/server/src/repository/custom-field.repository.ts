import { EntityRepository, Repository } from 'typeorm';
import { CustomField } from '../domain/custom-field.entity';

@EntityRepository(CustomField)
export class CustomFieldRepository extends Repository<CustomField> {}
