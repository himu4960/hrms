import { EntityRepository, Repository } from 'typeorm';

import { SettingsTimeClock } from '../domain/settings-time-clock.entity';

@EntityRepository(SettingsTimeClock)
export class SettingsTimeClockRepository extends Repository<SettingsTimeClock> {}
