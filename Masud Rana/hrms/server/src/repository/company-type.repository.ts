import { EntityRepository, Repository } from 'typeorm';
import { CompanyType } from '../domain/company-type.entity';

@EntityRepository(CompanyType)
export class CompanyTypeRepository extends Repository<CompanyType> {}
