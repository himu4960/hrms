import { EntityRepository, Repository } from 'typeorm';
import { Designation } from '../domain/designation.entity';

@EntityRepository(Designation)
export class DesignationRepository extends Repository<Designation> {}
