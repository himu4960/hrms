import { EntityRepository, Repository } from 'typeorm';
import { Religion } from '../domain/religion.entity';

@EntityRepository(Religion)
export class ReligionRepository extends Repository<Religion> {}
