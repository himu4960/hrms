import { EntityRepository, Repository } from 'typeorm';
import { EmployeeType } from '../domain/employee-type.entity';

@EntityRepository(EmployeeType)
export class EmployeeTypeRepository extends Repository<EmployeeType> {}
