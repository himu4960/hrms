import { EntityRepository, Repository } from 'typeorm';
import { LeaveType } from '../domain/leave-type.entity';

@EntityRepository(LeaveType)
export class LeaveTypeRepository extends Repository<LeaveType> {}
