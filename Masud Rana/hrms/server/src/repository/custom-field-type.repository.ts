import { EntityRepository, Repository } from 'typeorm';
import { CustomFieldType } from '../domain/custom-field-type.entity';

@EntityRepository(CustomFieldType)
export class CustomFieldTypeRepository extends Repository<CustomFieldType> {}
