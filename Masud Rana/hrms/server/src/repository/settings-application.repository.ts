import { EntityRepository, Repository } from 'typeorm';

import { SettingsApplication } from '../domain/settings-application.entity';

@EntityRepository(SettingsApplication)
export class SettingsApplicationRepository extends Repository<SettingsApplication> {}
