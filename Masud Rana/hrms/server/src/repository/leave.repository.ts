import { EntityRepository, Repository } from 'typeorm';

import { Leave } from '../domain/leave.entity';

@EntityRepository(Leave)
export class LeaveRepository extends Repository<Leave> {}
