import { EntityRepository, Repository } from 'typeorm';

import { Skill } from '../domain/skill.entity';

@EntityRepository(Skill)
export class SkillRepository extends Repository<Skill> {}
