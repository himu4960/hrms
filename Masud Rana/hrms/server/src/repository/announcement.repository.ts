import { EntityRepository, Repository } from 'typeorm';
import { Announcement } from '../domain/announcement.entity';

@EntityRepository(Announcement)
export class AnnouncementRepository extends Repository<Announcement> {}
