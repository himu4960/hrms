
import { EntityRepository, Repository } from 'typeorm';

import { SettingsLeaveAvailability } from '../domain/settings-leave-availability.entity';

@EntityRepository(SettingsLeaveAvailability)
export class SettingsLeaveAvailabilityRepository extends Repository<SettingsLeaveAvailability> {}
