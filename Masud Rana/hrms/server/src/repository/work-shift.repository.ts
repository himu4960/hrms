import { EntityRepository, Repository } from 'typeorm';

import { WorkShift } from '../domain/work-shift.entity';

@EntityRepository(WorkShift)
export class WorkShiftRepository extends Repository<WorkShift> {}
