import { EntityRepository, Repository } from 'typeorm';
import { Gender } from '../domain/gender.entity';

@EntityRepository(Gender)
export class GenderRepository extends Repository<Gender> {}
