import { EntityRepository, Repository } from 'typeorm';
import { TimeClockEvent } from '../domain/time-clock-event.entity';

@EntityRepository(TimeClockEvent)
export class TimeClockEventRepository extends Repository<TimeClockEvent> {}
