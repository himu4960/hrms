import { EntityRepository, Repository } from 'typeorm';
import { SettingsShiftPlanning } from '../domain/settings-shift-planning.entity';

@EntityRepository(SettingsShiftPlanning)
export class SettingsShiftPlanningRepository extends Repository<SettingsShiftPlanning> {}
