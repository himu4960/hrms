import { EntityRepository, Repository } from 'typeorm';
import { TimeClock } from '../domain/time-clock.entity';

@EntityRepository(TimeClock)
export class TimeClockRepository extends Repository<TimeClock> {}
