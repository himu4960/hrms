import { EntityRepository, Repository } from 'typeorm';

import { BreakRule } from '../domain/break-rule.entity';

@EntityRepository(BreakRule)
export class BreakRuleRepository extends Repository<BreakRule> {}
