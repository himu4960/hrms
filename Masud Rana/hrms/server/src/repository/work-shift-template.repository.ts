import { EntityRepository, Repository } from 'typeorm';
import { WorkShiftTemplate } from '../domain/work-shift-template.entity';

@EntityRepository(WorkShiftTemplate)
export class WorkShiftTemplateRepository extends Repository<WorkShiftTemplate> {}
