import { EntityRepository, Repository } from 'typeorm';
import { GeoLocation } from '../domain/geo-location.entity';

@EntityRepository(GeoLocation)
export class GeoLocationRepository extends Repository<GeoLocation> {}
