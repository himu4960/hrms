import { EntityRepository, Repository } from 'typeorm';

import { UserCompany } from '../domain/user-company.entity';

@EntityRepository(UserCompany)
export class UserCompanyRepository extends Repository<UserCompany> {}
