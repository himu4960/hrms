import { EntityRepository, Repository } from 'typeorm';
import { EmployeeDesignation } from '../domain/employee-designation.entity';

@EntityRepository(EmployeeDesignation)
export class EmployeeDesignationRepository extends Repository<EmployeeDesignation> {}
