import { EntityRepository, Repository } from 'typeorm';
import { Holiday } from '../domain/holiday.entity';

@EntityRepository(Holiday)
export class HolidayRepository extends Repository<Holiday> {}
