import { EntityRepository, Repository } from 'typeorm';
import { MaritalStatus } from '../domain/marital-status.entity';

@EntityRepository(MaritalStatus)
export class MaritalStatusRepository extends Repository<MaritalStatus> {}
