import { EntityRepository, Repository } from 'typeorm';

import { TimeZone } from '../domain/time-zone.entity';

@EntityRepository(TimeZone)
export class TimeZoneRepository extends Repository<TimeZone> {}
