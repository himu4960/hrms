import { EntityRepository, Repository } from 'typeorm';
import { ReligionTranslation } from '../domain/religion-translation.entity';

@EntityRepository(ReligionTranslation)
export class ReligionTranslationRepository extends Repository<ReligionTranslation> {}
