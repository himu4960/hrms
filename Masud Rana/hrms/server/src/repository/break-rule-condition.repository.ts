import { EntityRepository, Repository } from 'typeorm';
import { BreakRuleCondition } from '../domain/break-rule-condition.entity';

@EntityRepository(BreakRuleCondition)
export class BreakRuleConditionRepository extends Repository<BreakRuleCondition> {}
