import { EntityRepository, Repository } from 'typeorm';
import { ApplicationType } from '../domain/application-type.entity';

@EntityRepository(ApplicationType)
export class ApplicationTypeRepository extends Repository<ApplicationType> {}
