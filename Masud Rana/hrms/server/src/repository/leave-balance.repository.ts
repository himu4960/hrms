import { EntityRepository, Repository } from 'typeorm';

import { LeaveBalance } from '../domain/leave-balance.entity';

@EntityRepository(LeaveBalance)
export class LeaveBalanceRepository extends Repository<LeaveBalance> {}
