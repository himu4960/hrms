import { EntityRepository, Repository } from 'typeorm';
import { UserDevice } from '../domain/user-device.entity';

@EntityRepository(UserDevice)
export class UserDeviceRepository extends Repository<UserDevice> {}
