import { EntityRepository, Repository } from 'typeorm';

import { BloodGroupTranslation } from '../domain/blood-group-translation.entity';

@EntityRepository(BloodGroupTranslation)
export class BloodGroupTranslationRepository extends Repository<BloodGroupTranslation> {}
