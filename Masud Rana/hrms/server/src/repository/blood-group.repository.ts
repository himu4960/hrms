import { EntityRepository, Repository } from 'typeorm';

import { BloodGroup } from '../domain/blood-group.entity';

@EntityRepository(BloodGroup)
export class BloodGroupRepository extends Repository<BloodGroup> {}
