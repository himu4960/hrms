import { EntityRepository, Repository } from 'typeorm';
import { GenderTranslation } from '../domain/gender-translation.entity';

@EntityRepository(GenderTranslation)
export class GenderTranslationRepository extends Repository<GenderTranslation> {}
