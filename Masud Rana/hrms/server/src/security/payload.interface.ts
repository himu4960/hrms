export interface UserPayload {
    user_id: number;
    user_name: string;
    email: string;
}

export interface CompanyPayload {
    user_id: number;
    company_id?: number;
    role_id?: number;
    employee_id?: number;
    is_employee?: boolean;
}
