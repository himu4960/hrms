import * as bcrypt from 'bcrypt';
import { config } from '../config';

export async function transformPassword(user: { password?: string }): Promise<void> {
    if (user.password) {
        user.password = await bcrypt.hash(
            user.password,
            config.get('hrms.security.authentication.jwt.hash-salt-or-rounds'),
        );
    }
    return Promise.resolve();
}

export async function bcryptedPassword(password: string): Promise<string> {
    if (password) {
        return await bcrypt.hash(password, config.get('hrms.security.authentication.jwt.hash-salt-or-rounds'));
    }
}
