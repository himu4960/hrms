import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';

import { AuthService } from './../../service/auth.service';

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(
        private readonly authService: AuthService,
    ) { }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const req: any = context.switchToHttp().getRequest();
        const { route }: { route: any } = context.switchToHttp().getRequest();

        return await this.authService.checkAuthorization({ authorization: req.headers.authorization, route })

    }
}
