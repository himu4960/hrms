'use strict';

export enum RoleType {
    SUPER_ADMIN = 1,
    OWNER = 2,
    MANAGER = 3,
    EMPLOYEE = 4,
}
