import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { EmployeeSkillDTO } from './../src/service/dto/employee-skill.dto';

import { EmployeeSkillService } from '../src/service/employee-skill.service';

describe('EmployeeSkill Controller', () => {
    let token: string;
    let app: INestApplication;
    const employeeSkillEndPoint = '/api/employee-skills/';

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };

    const mockLoginUser: any = {
        username: 'admin',
        password: 'admin',
        rememberMe: true,
    };

    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(EmployeeSkillService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();

        const loginRes = await request(app.getHttpServer())
            .post('/api/login')
            .send(mockLoginUser);

        token = loginRes.body.id_token;
    });

    it('/GET all employee-skills ', async () => {
        const getEntities: EmployeeSkillDTO[] = (
            await request(app.getHttpServer())
                .get(employeeSkillEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET employee-skills by id', async () => {
        const getEntity: EmployeeSkillDTO = (
            await request(app.getHttpServer())
                .get(employeeSkillEndPoint + entityMock.id)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create employee-skills', async () => {
        const createdEntity: EmployeeSkillDTO = (
            await request(app.getHttpServer())
                .post(employeeSkillEndPoint)
                .send(entityMock)
                .set('Authorization', 'Bearer ' + token)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update employee-skills', async () => {
        const updatedEntity: EmployeeSkillDTO = (
            await request(app.getHttpServer())
                .put(employeeSkillEndPoint)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT update employee-skills from id', async () => {
        const updatedEntity: EmployeeSkillDTO = (
            await request(app.getHttpServer())
                .put(employeeSkillEndPoint + entityMock.id)
                .send(entityMock)
                .set('Authorization', 'Bearer ' + token)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE employee-skills', async () => {
        const deletedEntity: EmployeeSkillDTO = (
            await request(app.getHttpServer())
                .delete(employeeSkillEndPoint + entityMock.id)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
