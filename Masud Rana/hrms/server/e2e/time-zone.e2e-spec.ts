import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { TimeZoneDTO } from './../src/service/dto/time-zone.dto';

import { TimeZoneService } from '../src/service/time-zone.service';

describe('TimeZone Controller', () => {
    let app: INestApplication;
    const timeZoneEndPoint = '/api/time-zones';

    const entityMock: any = {
        id: 1,
        name: 'test_name',
        code: 'test_code',
        iso_code: 'test_iso_code',
        is_active: true,
        is_deleted: true,
        is_default: false,
    };

    const serviceMock = {
        getAll: (): any => entityMock,
        getActiveTimeZones: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideProvider(TimeZoneService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET active time-zone ', async () => {
        const getEntities: TimeZoneDTO = (
            await request(app.getHttpServer())
                .get(timeZoneEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
        expect(getEntities.is_active).toBe(true);
        expect(getEntities.is_default).toBe(false);
    });

    it('/GET all time-zones ', async () => {
        const getEntities: TimeZoneDTO[] = (
            await request(app.getHttpServer())
                .get(`${timeZoneEndPoint}/all`)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
