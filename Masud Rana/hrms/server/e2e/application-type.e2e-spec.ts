import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { ApplicationTypeDTO } from './../src/service/dto/application-type.dto';

import { ApplicationTypeService } from '../src/service/application-type.service';

describe('ApplicationType Controller', () => {
    let app: INestApplication;
    const applicationTypeEndPoint = '/api/application-types';

    const entityMock: any = {
        id: 1,
        name: 'entityMock',
        is_active: true,
        is_deleted: false,
    };

    const serviceMock = {
        getAll: (): any => entityMock,
        getActiveApplicationTypes: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideProvider(ApplicationTypeService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET active application-types ', async () => {
        const getEntities: ApplicationTypeDTO[] = (
            await request(app.getHttpServer())
                .get(applicationTypeEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET all application-types ', async () => {
        const getEntities: ApplicationTypeDTO[] = (
            await request(app.getHttpServer())
                .get(`${applicationTypeEndPoint}/all`)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
