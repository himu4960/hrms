import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');

import { AppModule } from '../src/app.module';
import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { UserDTO } from '../src/service/dto/user.dto';

import { UserService } from '../src/service/user.service';

describe('User', () => {
    let app: INestApplication;
    let service: UserService;
    const userEndPoint = '/api/admin/users';

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };

    const testUserRequestObject: any = {
        username: 'userTestLogin',
        firstName: 'UserTest',
        lastName: 'UserTest',
        email: 'usertest@localhost.it',
        user_companies: [],
    };

    const testUserDTO: UserDTO = {
        username: 'userTestLogin',
        firstName: 'UserTest',
        lastName: 'UserTest',
        email: 'usertest@localhost.it',
        password: 'userTestLogin',
        user_companies: [],
    };

    const testUpdateUserDTO: UserDTO = {
        username: 'updated',
        firstName: 'UserTestUpdate',
        lastName: 'UserTestUpdate',
        email: 'usertestupdate@localhost.it',
        password: 'userTestLogin',
        updated_at: new Date(),
        updated_by: null,
        imageUrl: null,
        langKey: 'en',
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
        service = moduleFixture.get<UserService>(UserService);
    });

    it('/POST create user', async () => {
        const createdUser: UserDTO = (
            await request(app.getHttpServer())
                .post(userEndPoint)
                .send(testUserRequestObject)
                .expect(201)
        ).body?.data;

        expect(createdUser.username).toEqual(testUserRequestObject.username);

        await request(app.getHttpServer())
            .delete(`${userEndPoint}/${createdUser.id}`)
            .expect(200);
    });

    it('/GET all users', () => {
        request(app.getHttpServer())
            .get(userEndPoint)
            .expect(200);
    });

    it('/PUT update user', async () => {
        const user = await service.save(testUpdateUserDTO);
        user.username = 'updated';

        const updatedUser = (
            await request(app.getHttpServer())
                .put(userEndPoint)
                .send(user)
                .expect(200)
        ).body?.data;

        expect(updatedUser.username).toEqual(testUpdateUserDTO.username);

        await request(app.getHttpServer())
            .delete(`${userEndPoint}/${user.id}`)
            .expect(200);
    });

    it('/GET user with a login name', async () => {
        testUserDTO.username = 'TestUserGet';
        const savedUser: UserDTO = await service.save(testUserDTO);
        const { password, ...savedUserWithoutPassword } = savedUser;

        const getUser: UserDTO = (
            await request(app.getHttpServer())
                .get(`${userEndPoint}/${savedUser.username}`)
                .expect(200)
        ).body?.data;

        expect(getUser.username).toEqual(savedUserWithoutPassword.username);
        expect(getUser.email).toEqual(savedUserWithoutPassword.email);

        await request(app.getHttpServer())
            .delete(`${userEndPoint}/${savedUser.id}`)
            .expect(200);
    });

    it('/DELETE user', async () => {
        const savedUser: UserDTO = await service.save(testUserDTO);

        await request(app.getHttpServer())
            .delete(`${userEndPoint}/${savedUser.id}`)
            .expect(200);

        const userUndefined = await service.findById(savedUser.id);
        expect(userUndefined).toBeUndefined();
    });

    afterEach(async () => {
        await app.close();
    });
});
