import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { CountryDTO } from './../src/service/dto/country.dto';

import { CountryService } from '../src/service/country.service';

describe('Country Controller', () => {
    let app: INestApplication;
    const countryEndPoint = '/api/countries';

    const entityMock: any = {
        id: 1,
        name: 'Afghanistan',
        code: '93',
        iso_code: 'AF',
        is_default: 0,
    };

    const serviceMock = {
        getActiveCountries: (): any => entityMock,
        getAll: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideProvider(CountryService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET active countries ', async () => {
        const getEntities: CountryDTO[] = (
            await request(app.getHttpServer())
                .get(countryEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET all countries ', async () => {
        const getEntities: CountryDTO[] = (
            await request(app.getHttpServer())
                .get(`${countryEndPoint}/all`)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
