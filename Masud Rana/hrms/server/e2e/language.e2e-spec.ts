import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { LanguageDTO } from './../src/service/dto/language.dto';

import { LanguageService } from '../src/service/language.service';

describe('Language Controller', () => {
    let app: INestApplication;
    const languageEndPoint = '/api/languages';

    const entityActiveMock: any = {
        code: 'en',
        name: 'English',
        is_active: true,
        is_deleted: false,
        is_default: true,
    };

    const serviceMock = {
        getAll: (): any => entityActiveMock,
        getActiveLanguages: (): any => entityActiveMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideProvider(LanguageService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET active and default language english ', async () => {
        const getEntities: LanguageDTO = (
            await request(app.getHttpServer())
                .get(languageEndPoint)
                .expect(200)
        ).body?.data;
        expect(getEntities).toEqual(entityActiveMock);
        expect(getEntities.code).toBe('en');
        expect(getEntities.is_active).toBe(true);
        expect(getEntities.is_default).toBe(true);
    });

    afterEach(async () => {
        await app.close();
    });
});
