import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');
import { AppModule } from '../src/app.module';
import { INestApplication } from '@nestjs/common';
import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';
import { AnnouncementDTO } from '../src/service/dto/announcement/announcement.dto';
import { AnnouncementService } from '../src/service/announcement.service';

describe('Announcement Controller', () => {
    let app: INestApplication;

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(AnnouncementService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all announcements ', async () => {
        const getEntities: AnnouncementDTO[] = (
            await request(app.getHttpServer()).get('/api/announcements').expect(200)
        ).body;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET announcements by id', async () => {
        const getEntity: AnnouncementDTO = (
            await request(app.getHttpServer())
                .get('/api/announcements/' + entityMock.id)
                .expect(200)
        ).body;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create announcements', async () => {
        const createdEntity: AnnouncementDTO = (
            await request(app.getHttpServer()).post('/api/announcements').send(entityMock).expect(201)
        ).body;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update announcements', async () => {
        const updatedEntity: AnnouncementDTO = (
            await request(app.getHttpServer()).put('/api/announcements').send(entityMock).expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT update announcements from id', async () => {
        const updatedEntity: AnnouncementDTO = (
            await request(app.getHttpServer())
                .put('/api/announcements/' + entityMock.id)
                .send(entityMock)
                .expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE announcements', async () => {
        const deletedEntity: AnnouncementDTO = (
            await request(app.getHttpServer())
                .delete('/api/announcements/' + entityMock.id)
                .expect(204)
        ).body;

        expect(deletedEntity).toEqual({});
    });

    afterEach(async () => {
        await app.close();
    });
});
