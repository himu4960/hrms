import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { MaritalStatusDTO } from './../src/service/dto/marital-status/marital-status.dto';

import { MaritalStatusService } from '../src/service/marital-status.service';

describe('MaritalStatus Controller', () => {
    let app: INestApplication;
    const maritalStatusEndPoint = '/api/marital-statuses';

    const entityMock: any = {
        id: 456,
        company_id: 123,
        is_active: true,
        is_deleted: true,
    };

    const serviceMock = {
        findAll: (): any => entityMock,
        findById: (): any => entityMock,
        findByLangCode: (): any => entityMock,
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideProvider(MaritalStatusService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all marital-statuses ', async () => {
        const getEntities: MaritalStatusDTO[] = (
            await request(app.getHttpServer())
                .get(maritalStatusEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET marital-statuses by langCode', async () => {
        const getEntity: MaritalStatusDTO = (
            await request(app.getHttpServer())
                .get(`${maritalStatusEndPoint}/${entityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create marital-statuses', async () => {
        const createdEntity: MaritalStatusDTO = (
            await request(app.getHttpServer())
                .post(maritalStatusEndPoint)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update marital-statuses from id', async () => {
        const updatedEntity: MaritalStatusDTO = (
            await request(app.getHttpServer())
                .put(`${maritalStatusEndPoint}/${entityMock.id}`)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE marital-statuses', async () => {
        const deletedEntity: MaritalStatusDTO = (
            await request(app.getHttpServer())
                .delete(`${maritalStatusEndPoint}/${entityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
