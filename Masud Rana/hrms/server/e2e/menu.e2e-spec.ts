import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { MenuDTO } from './../src/service/dto/menu.dto';

import { MenuService } from '../src/service/menu.service';

describe('Menu Controller', () => {
    let app: INestApplication;
    const menuEndPoint = '/api/menus/';

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
        name: 'entityName',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(MenuService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all menus ', async () => {
        const getEntities: MenuDTO[] = (
            await request(app.getHttpServer())
                .get(menuEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET menus by id', async () => {
        const getEntity: MenuDTO = (
            await request(app.getHttpServer())
                .get(menuEndPoint + entityMock.id)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create menus', async () => {
        const createdEntity: MenuDTO = (
            await request(app.getHttpServer())
                .post(menuEndPoint)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update menus', async () => {
        const updatedEntity: MenuDTO = (
            await request(app.getHttpServer())
                .put(menuEndPoint)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT update menus from id', async () => {
        const updatedEntity: MenuDTO = (
            await request(app.getHttpServer())
                .put(menuEndPoint + entityMock.id)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE menus', async () => {
        const deletedEntity: MenuDTO = (
            await request(app.getHttpServer())
                .delete(menuEndPoint + entityMock.id)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
