import { LeaveDTO } from './../src/service/dto/leave/leave.dto';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { LeaveService } from '../src/service/leave.service';

describe('Leave Controller', () => {
    let app: INestApplication;
    let token: string;
    const leaveEndPoint = '/api/leaves/';
    const leaveApproveEndPoint = '/api/leaves/approve/';
    const leaveRejectEndPoint = '/api/leaves/reject/';

    const mockLoginUser: any = {
        username: 'admin',
        password: 'admin',
        rememberMe: true,
    };

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
        approveLeave: (): any => entityMock,
        rejectLeave: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(LeaveService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();

        const loginRes = await request(app.getHttpServer())
            .post('/api/login')
            .send(mockLoginUser);

        token = loginRes.body.id_token;
    });

    it('/GET all leaves ', async () => {
        const getEntities: LeaveDTO[] = (
            await request(app.getHttpServer())
                .get(leaveEndPoint)
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET leaves by id', async () => {
        const getEntity: LeaveDTO = (
            await request(app.getHttpServer())
                .get(leaveEndPoint + entityMock.id)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create leaves', async () => {
        const createdEntity: LeaveDTO = (
            await request(app.getHttpServer())
                .post(leaveEndPoint)
                .send(entityMock)
                .set('Authorization', 'Bearer ' + token)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update leaves', async () => {
        const updatedEntity: LeaveDTO = (
            await request(app.getHttpServer())
                .put(leaveEndPoint)
                .send(entityMock)
                .set('Authorization', 'Bearer ' + token)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT approve leave from id', async () => {
        const updatedEntity: LeaveDTO = (
            await request(app.getHttpServer())
                .put(leaveApproveEndPoint + entityMock.id)
                .send(entityMock)
                .expect(200)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT reject leave from id', async () => {
        const updatedEntity: LeaveDTO = (
            await request(app.getHttpServer())
                .put(leaveRejectEndPoint + entityMock.id)
                .send(entityMock)
                .expect(200)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT update leaves from id', async () => {
        const updatedEntity: LeaveDTO = (
            await request(app.getHttpServer())
                .put(leaveEndPoint + entityMock.id)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE leaves', async () => {
        const deletedEntity: LeaveDTO = (
            await request(app.getHttpServer())
                .delete(leaveEndPoint + entityMock.id)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
