import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');
import { INestApplication } from '@nestjs/common';

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { CustomFieldTypeDTO } from './../src/service/dto/custom-field-type.dto';

import { CustomFieldTypeService } from '../src/service/custom-field-type.service';

describe('CustomFieldType Controller', () => {
    let app: INestApplication;
    let customFieldTypeEndPoint = '/api/custom-field-types/';

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(CustomFieldTypeService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all custom-field-types ', async () => {
        const getEntities: CustomFieldTypeDTO[] = (
            await request(app.getHttpServer())
                .get(customFieldTypeEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET custom-field-types by id', async () => {
        const getEntity: CustomFieldTypeDTO = (
            await request(app.getHttpServer())
                .get(customFieldTypeEndPoint + entityMock.id)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create custom-field-types', async () => {
        const createdEntity: CustomFieldTypeDTO = (
            await request(app.getHttpServer())
                .post(customFieldTypeEndPoint)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update custom-field-types', async () => {
        const updatedEntity: CustomFieldTypeDTO = (
            await request(app.getHttpServer())
                .put(customFieldTypeEndPoint)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT update custom-field-types from id', async () => {
        const updatedEntity: CustomFieldTypeDTO = (
            await request(app.getHttpServer())
                .put(customFieldTypeEndPoint + entityMock.id)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE custom-field-types', async () => {
        const deletedEntity: CustomFieldTypeDTO = (
            await request(app.getHttpServer())
                .delete(customFieldTypeEndPoint + entityMock.id)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
