import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { BreakRuleDTO } from './../src/service/dto/break-rule/break-rule.dto';

import { BreakRuleService } from '../src/service/break-rule.service';

describe('BreakRule Controller', () => {
    let app: INestApplication;
    let token: string;
    const breakRuleEndPoint = '/api/break-rules';

    const mockLoginUser: any = {
        username: 'admin',
        password: 'admin',
        rememberMe: true,
    };

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };

    const breakRuleMock: any = {
        id: 123,
        name: 'test',
        rule_description: 'test description',
        break_rule_conditions: [
            {
                id: 560,
                type: 'test type',
                min_shift_length: 480,
                max_shift_length: 960,
                break_duration: 60,
                from: 100,
                to: 150,
                break_starts_at: 200,
            },
        ],
    };

    const serviceMock = {
        breakRuleFindById: (): any => breakRuleMock,
        breakRuleFindAndCount: (): any => [breakRuleMock, 0],
        breakRuleSave: (): any => breakRuleMock,
        breakRuleUpdate: (): any => breakRuleMock,
        breakRuleDeleteById: (): any => breakRuleMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(BreakRuleService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();

        const loginRes = await request(app.getHttpServer())
            .post('/api/login')
            .send(mockLoginUser);

        token = loginRes.body.id_token;
    });

    it('/GET all break-rules ', async () => {
        const getEntities: BreakRuleDTO[] = (
            await request(app.getHttpServer())
                .get(breakRuleEndPoint)
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(breakRuleMock);
    });

    it('/GET break-rules by id', async () => {
        const getEntity: BreakRuleDTO = (
            await request(app.getHttpServer())
                .get(`${breakRuleEndPoint}/${breakRuleMock.id}`)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(breakRuleMock);
    });

    it('/POST create break-rules', async () => {
        const createdEntity: BreakRuleDTO = (
            await request(app.getHttpServer())
                .post(breakRuleEndPoint)
                .set('Authorization', 'Bearer ' + token)
                .send(breakRuleMock)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(breakRuleMock);
    });

    it('/PUT update break-rules from id', async () => {
        const updatedEntity: BreakRuleDTO = (
            await request(app.getHttpServer())
                .put(`${breakRuleEndPoint}/${breakRuleMock.id}`)
                .send(breakRuleMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(breakRuleMock);
    });

    it('/DELETE break-rules', async () => {
        const deletedEntity: BreakRuleDTO = (
            await request(app.getHttpServer())
                .delete(`${breakRuleEndPoint}/${breakRuleMock.id}`)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(breakRuleMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
