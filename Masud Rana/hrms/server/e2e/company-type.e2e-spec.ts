import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { CompanyTypeDTO } from './../src/service/dto/company-type.dto';

import { CompanyTypeService } from '../src/service/company-type.service';

describe('CompanyType Controller', () => {
    let app: INestApplication;
    const companyTypeEndPoint = '/api/company-types';

    const entityMock: any = {
        id: 1,
        name: 'entityMock',
        is_active: true,
        is_deleted: false,
    };

    const serviceMock = {
        getAll: (): any => entityMock,
        getActiveCompanyTypes: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideProvider(CompanyTypeService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET active company-types ', async () => {
        const getEntities: CompanyTypeDTO[] = (
            await request(app.getHttpServer())
                .get(companyTypeEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET all company-types', async () => {
        const getEntity: CompanyTypeDTO = (
            await request(app.getHttpServer())
                .get(companyTypeEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
