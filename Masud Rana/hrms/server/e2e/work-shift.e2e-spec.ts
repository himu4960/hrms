import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');
import { INestApplication } from '@nestjs/common';

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { WorkShiftDTO } from './../src/service/dto/work-shift/work-shift.dto';

import { WorkShiftService } from '../src/service/work-shift.service';

describe('WorkShift Controller', () => {
    let app: INestApplication;
    let token: string;
    let workShiftEndPoint = '/api/work-shifts/';

    const mockLoginUser: any = {
        username: 'admin',
        password: 'admin',
        rememberMe: true,
    };

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(WorkShiftService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();

        const loginRes = await request(app.getHttpServer())
            .post('/api/login')
            .send(mockLoginUser);

        token = loginRes.body.id_token;
    });

    it('/GET all work-shifts ', async () => {
        const getEntities: WorkShiftDTO[] = (
            await request(app.getHttpServer())
                .get(workShiftEndPoint)
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET work-shifts by id', async () => {
        const getEntity: WorkShiftDTO = (
            await request(app.getHttpServer())
                .get(workShiftEndPoint + entityMock.id)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create work-shifts', async () => {
        const createdEntity: WorkShiftDTO = (
            await request(app.getHttpServer())
                .post(workShiftEndPoint)
                .send(entityMock)
                .set('Authorization', 'Bearer ' + token)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update work-shifts from id', async () => {
        const updatedEntity: WorkShiftDTO = (
            await request(app.getHttpServer())
                .put(workShiftEndPoint + entityMock.id)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE work-shifts', async () => {
        const deletedEntity: WorkShiftDTO = (
            await request(app.getHttpServer())
                .delete(workShiftEndPoint + entityMock.id)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
