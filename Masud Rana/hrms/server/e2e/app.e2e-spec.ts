import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');

import { AppModule } from '../src/app.module';
import { UserLoginDTO } from '../src/service/dto/user-login.dto';

describe('App', () => {
    let app: INestApplication;

    const infoService = {
        status: true,
        statusCode: 200,
        message: null,
        translateCode: null,
        data: {
            activeProfiles: 'dev',
            'display-ribbon-on-profiles': 'dev',
        },
    };
    const testUserLogin: UserLoginDTO = {
        username: 'admin',
        password: 'admin',
        rememberMe: true,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET up running info OK', () =>
        request(app.getHttpServer())
            .get('/management/info')
            .expect(200)
            .expect(infoService));

    it('/GET public users OK', () =>
        request(app.getHttpServer())
            .get('/api/users')
            .expect(200));

    it('/POST login get jwt authenticate OK', () =>
        request(app.getHttpServer())
            .post('/api/login')
            .send(testUserLogin)
            .expect(201));

    afterEach(async () => {
        await app.close();
    });
});
