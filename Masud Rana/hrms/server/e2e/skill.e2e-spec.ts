import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { SkillDTO } from './../src/service/dto/skill.dto';

import { SkillService } from '../src/service/skill.service';

describe('Skill Controller', () => {
    let app: INestApplication;
    let token: string;
    const skillsEndPoint = '/api/skills';

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };

    const mockLoginUser: any = {
        username: 'admin',
        password: 'admin',
        rememberMe: true,
    };

    const entityMock: any = {
        id: 'entityId',
        name: 'entityName',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(SkillService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();

        const loginRes = await request(app.getHttpServer())
            .post('/api/login')
            .send(mockLoginUser);

        token = loginRes.body.id_token;
    });

    it('/GET all skills ', async () => {
        const getEntities: SkillDTO[] = (
            await request(app.getHttpServer())
                .get(skillsEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET skills by id', async () => {
        const getEntity: SkillDTO = (
            await request(app.getHttpServer())
                .get(`${skillsEndPoint}/${entityMock.id}`)
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create skills', async () => {
        const createdEntity: SkillDTO = (
            await request(app.getHttpServer())
                .post(skillsEndPoint)
                .send(entityMock)
                .set('Authorization', 'Bearer ' + token)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update skills', async () => {
        const updatedEntity: SkillDTO = (
            await request(app.getHttpServer())
                .put(skillsEndPoint)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT update skills from id', async () => {
        const updatedEntity: SkillDTO = (
            await request(app.getHttpServer())
                .put(`${skillsEndPoint}/${entityMock.id}`)
                .send(entityMock)
                .set('Authorization', 'Bearer ' + token)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE skills', async () => {
        const deletedEntity: SkillDTO = (
            await request(app.getHttpServer())
                .delete(`${skillsEndPoint}/${entityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
