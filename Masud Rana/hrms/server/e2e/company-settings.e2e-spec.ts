import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { SettingsTimeClockDTO } from './../src/service/dto/company-settings/settings-time-clock.dto';
import { SettingsShiftPlanningDTO } from './../src/service/dto/company-settings/settings-shift-planning.dto';
import { SettingsApplicationDTO } from './../src/service/dto/company-settings/settings-application.dto';

import { CompanySettingsService } from '../src/service/company-settings.service';

describe('Company Settings Controller', () => {
    let app: INestApplication;
    let token: string;
    const companySettingsEndPoint = '/api/company-settings';

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };

    const mockLoginUser: any = {
        username: 'admin',
        password: 'admin',
        rememberMe: true,
    };

    const applicationMock: any = {
        notification_email_enabled: false,
    };

    const shiftPlanningMock: any = {
        sorting_by_address_enabled: true,
        show_locations_in_shifts: true,
    };

    const timeClockMock: any = {
        time_clock_enabled: false,
    };

    const serviceMock = {
        getApplicationByCompanyId: (): any => applicationMock,
        updateApplicationByCompanyId: (): any => applicationMock,
        getShiftPlanningByCompanyId: (): any => shiftPlanningMock,
        updateShiftPlanningByCompanyId: (): any => shiftPlanningMock,
        getTimeClockByCompanyId: (): any => timeClockMock,
        updateTimeClockByCompanyId: (): any => timeClockMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(CompanySettingsService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
        const loginRes = await request(app.getHttpServer())
            .post('/api/login')
            .send(mockLoginUser);

        token = loginRes.body.id_token;
    });

    it('/GET application settings by company id', async () => {
        const getApplicationEntities: SettingsApplicationDTO = (
            await request(app.getHttpServer())
                .get(`${companySettingsEndPoint}/application`)
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
        ).body?.data;

        expect(getApplicationEntities).toEqual(applicationMock);
    });

    it('/PUT update application settings from company id', async () => {
        const updatedApplicationEntity: SettingsApplicationDTO = (
            await request(app.getHttpServer())
                .put(`${companySettingsEndPoint}/application`)
                .send(applicationMock)
                .set('Authorization', 'Bearer ' + token)
                .expect(201)
        ).body?.data;

        expect(updatedApplicationEntity).toEqual(applicationMock);
    });

    it('/GET shift planning settings by company id', async () => {
        const getShiftPlanningEntities: SettingsShiftPlanningDTO = (
            await request(app.getHttpServer())
                .get(`${companySettingsEndPoint}/shift-planning`)
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
        ).body?.data;

        expect(getShiftPlanningEntities).toEqual(shiftPlanningMock);
    });

    it('/PUT update shift planning settings from company id', async () => {
        const updatedShiftPlanningEntity: SettingsShiftPlanningDTO = (
            await request(app.getHttpServer())
                .put(`${companySettingsEndPoint}/shift-planning`)
                .send(shiftPlanningMock)
                .set('Authorization', 'Bearer ' + token)
                .expect(201)
        ).body?.data;

        expect(updatedShiftPlanningEntity).toEqual(shiftPlanningMock);
    });

    it('/GET time-clocks settings by company id', async () => {
        const getTimeClockEntities: SettingsTimeClockDTO = (
            await request(app.getHttpServer())
                .get(`${companySettingsEndPoint}/time-clocks`)
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
        ).body?.data;

        expect(getTimeClockEntities).toEqual(timeClockMock);
    });

    it('/PUT update time-clocks settings from company id', async () => {
        const updatedTimeClockEntity: SettingsTimeClockDTO = (
            await request(app.getHttpServer())
                .put(`${companySettingsEndPoint}/time-clocks`)
                .send(timeClockMock)
                .set('Authorization', 'Bearer ' + token)
                .expect(201)
        ).body?.data;

        expect(updatedTimeClockEntity).toEqual(timeClockMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
