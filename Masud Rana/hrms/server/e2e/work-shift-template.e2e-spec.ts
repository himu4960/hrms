import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');
import { INestApplication } from '@nestjs/common';

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { WorkShiftTemplateDTO } from '../src/service/dto/work-shift-template.dto';

import { WorkShiftTemplateService } from '../src/service/work-shift-template.service';

describe('WorkShiftTemplate Controller', () => {
    let app: INestApplication;

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(WorkShiftTemplateService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all work-shift-templates ', async () => {
        const getEntities: WorkShiftTemplateDTO[] = (
            await request(app.getHttpServer())
                .get('/api/work-shift-templates')
                .expect(200)
        ).body;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET work-shift-templates by id', async () => {
        const getEntity: WorkShiftTemplateDTO = (
            await request(app.getHttpServer())
                .get('/api/work-shift-templates/' + entityMock.id)
                .expect(200)
        ).body;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create work-shift-templates', async () => {
        const createdEntity: WorkShiftTemplateDTO = (
            await request(app.getHttpServer())
                .post('/api/work-shift-templates')
                .send(entityMock)
                .expect(201)
        ).body;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update work-shift-templates', async () => {
        const updatedEntity: WorkShiftTemplateDTO = (
            await request(app.getHttpServer())
                .put('/api/work-shift-templates')
                .send(entityMock)
                .expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT update work-shift-templates from id', async () => {
        const updatedEntity: WorkShiftTemplateDTO = (
            await request(app.getHttpServer())
                .put('/api/work-shift-templates/' + entityMock.id)
                .send(entityMock)
                .expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE work-shift-templates', async () => {
        const deletedEntity: WorkShiftTemplateDTO = (
            await request(app.getHttpServer())
                .delete('/api/work-shift-templates/' + entityMock.id)
                .expect(204)
        ).body;

        expect(deletedEntity).toEqual({});
    });

    afterEach(async () => {
        await app.close();
    });
});
