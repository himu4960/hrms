import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { PermissionTypeDTO } from './../src/service/dto/permission-type.dto';

import { PermissionTypeService } from '../src/service/permission-type.service';

describe('PermissionType Controller', () => {
    let app: INestApplication;

    const permissionTypeEndPoint = '/api/permission-types/';

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
        name: 'entityName',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(PermissionTypeService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all permission-types ', async () => {
        const getEntities: PermissionTypeDTO[] = (
            await request(app.getHttpServer())
                .get(permissionTypeEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET permission-types by id', async () => {
        const getEntity: PermissionTypeDTO = (
            await request(app.getHttpServer())
                .get(permissionTypeEndPoint + entityMock.id)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create permission-types', async () => {
        const createdEntity: PermissionTypeDTO = (
            await request(app.getHttpServer())
                .post(permissionTypeEndPoint)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update permission-types', async () => {
        const updatedEntity: PermissionTypeDTO = (
            await request(app.getHttpServer())
                .put(permissionTypeEndPoint)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT update permission-types from id', async () => {
        const updatedEntity: PermissionTypeDTO = (
            await request(app.getHttpServer())
                .put(permissionTypeEndPoint + entityMock.id)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE permission-types', async () => {
        const deletedEntity: PermissionTypeDTO = (
            await request(app.getHttpServer())
                .delete(permissionTypeEndPoint + entityMock.id)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
