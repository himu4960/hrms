import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { RoleDTO } from './../src/service/dto/role.dto';

import { RoleService } from '../src/service/role.service';

describe('Role Controller', () => {
    let app: INestApplication;
    const roleEndPoint = '/api/roles';

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(RoleService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all roles ', async () => {
        const getEntities: RoleDTO[] = (
            await request(app.getHttpServer())
                .get(roleEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET roles by id', async () => {
        const getEntity: RoleDTO = (
            await request(app.getHttpServer())
                .get('/api/roles/' + entityMock.id)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create roles', async () => {
        const createdEntity: RoleDTO = (
            await request(app.getHttpServer())
                .post(roleEndPoint)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update roles from id', async () => {
        const updatedEntity: RoleDTO = (
            await request(app.getHttpServer())
                .put(`${roleEndPoint}/${entityMock.id}`)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE roles', async () => {
        const deletedEntity: RoleDTO = (
            await request(app.getHttpServer())
                .delete(`${roleEndPoint}/${entityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
