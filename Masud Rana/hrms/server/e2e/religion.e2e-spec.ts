import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { ReligionDTO } from './../src/service/dto/religion/religion.dto';

import { ReligionService } from '../src/service/religion.service';

describe('Religion Controller', () => {
    let app: INestApplication;
    const religionEndPoint = '/api/religions';

    const entityMock: any = {
        id: 1,
        company_id: 123,
        is_active: true,
        is_deleted: true,
    };

    const serviceMock = {
        findAll: (): any => entityMock,
        findById: (): any => entityMock,
        findByLangCode: (): any => entityMock,
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideProvider(ReligionService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all religions ', async () => {
        const getEntities: ReligionDTO[] = (
            await request(app.getHttpServer())
                .get(religionEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET religions by id', async () => {
        const getEntity: ReligionDTO = (
            await request(app.getHttpServer())
                .get(`${religionEndPoint}/${entityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create religions', async () => {
        const createdEntity: ReligionDTO = (
            await request(app.getHttpServer())
                .post(religionEndPoint)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update religions from id', async () => {
        const updatedEntity: ReligionDTO = (
            await request(app.getHttpServer())
                .put(`${religionEndPoint}/${entityMock.id}`)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE religions', async () => {
        const deletedEntity: ReligionDTO = (
            await request(app.getHttpServer())
                .delete(`${religionEndPoint}/${entityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
