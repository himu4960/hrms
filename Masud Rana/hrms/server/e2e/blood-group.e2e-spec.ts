import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { BloodGroupDTO } from './../src/service/dto/blood-group/blood-group.dto';

import { BloodGroupService } from '../src/service/blood-group.service';

describe('BloodGroup Controller', () => {
    let app: INestApplication;
    const bloodGroupEndPoint = '/api/blood-groups';

    const entityMock: any = {
        id: 'entityId',
        company_id: 'string',
        is_active: true,
        is_deleted: true,
    };

    const serviceMock = {
        findAll: (): any => entityMock,
        findByLangCode: () => entityMock,
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideProvider(BloodGroupService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all blood-groups ', async () => {
        const getEntities: BloodGroupDTO[] = (
            await request(app.getHttpServer())
                .get(bloodGroupEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET blood-groups by id', async () => {
        const getEntity: BloodGroupDTO = (
            await request(app.getHttpServer())
                .get(`${bloodGroupEndPoint}/${entityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create blood-groups', async () => {
        const createdEntity: BloodGroupDTO = (
            await request(app.getHttpServer())
                .post(bloodGroupEndPoint)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update blood-groups', async () => {
        const updatedEntity: BloodGroupDTO = (
            await request(app.getHttpServer())
                .put(`${bloodGroupEndPoint}/${entityMock.id}`)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE blood-groups', async () => {
        const deletedEntity: BloodGroupDTO = (
            await request(app.getHttpServer())
                .delete(`${bloodGroupEndPoint}/${entityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
