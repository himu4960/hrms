import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { GenderDTO } from './../src/service/dto/gender/gender.dto';

import { GenderService } from '../src/service/gender.service';

describe('Gender Controller', () => {
    let app: INestApplication;
    const genderEndPoint = '/api/genders';

    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findAll: (): any => entityMock,
        findByLangCode: (): any => entityMock,
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideProvider(GenderService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all genders ', async () => {
        const getEntities: GenderDTO[] = (
            await request(app.getHttpServer())
                .get(genderEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET genders by langcode', async () => {
        const getEntity: GenderDTO = (
            await request(app.getHttpServer())
                .get(`${genderEndPoint}/${entityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create gender with translation', async () => {
        const createdEntity: GenderDTO = (
            await request(app.getHttpServer())
                .post(genderEndPoint)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update genders from id', async () => {
        const updatedEntity: GenderDTO = (
            await request(app.getHttpServer())
                .put(`${genderEndPoint}/${entityMock.id}`)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE gender', async () => {
        const deletedEntity: GenderDTO = (
            await request(app.getHttpServer())
                .delete(`${genderEndPoint}/${entityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
