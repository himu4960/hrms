import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request = require('supertest');

import { AppModule } from './../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { CompanyDTO } from './../src/service/dto/company.dto';

import { CompanyService } from '../src/service/company.service';

describe('Company Controller', () => {
    let app: INestApplication;
    const companyEndPoint = '/api/companies';

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        name: 'Dev12',
        phone: '01672066834',
        email: 'devteertha28@gmail.com',
        address: '123',
        logo_url_large: 'qwe',
        logo_url_medium: 'qwe',
        logo_url_small: 'qwe',
        status: true,
        companyType: {
            id: 3,
            name: 'Retail',
            is_active: true,
            is_deleted: false,
        },
        applicationType: {
            id: 3,
            name: 'Schedular',
            is_active: true,
            is_deleted: false,
        },
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(CompanyService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all companies ', async () => {
        const getEntities: CompanyDTO[] = (
            await request(app.getHttpServer())
                .get(companyEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET companies by id', async () => {
        const getEntity: CompanyDTO = (
            await request(app.getHttpServer())
                .get(`${companyEndPoint}/${entityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create companies', async () => {
        const createdEntity: CompanyDTO = (
            await request(app.getHttpServer())
                .post(companyEndPoint)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update companies from id', async () => {
        const updatedEntity: CompanyDTO = (
            await request(app.getHttpServer())
                .put(`${companyEndPoint}/${entityMock.id}`)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE companies', async () => {
        const deletedEntity: CompanyDTO = (
            await request(app.getHttpServer())
                .delete(`${companyEndPoint}/${entityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
