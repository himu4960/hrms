import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { TimeClockEventDTO } from './../src/service/dto/time-clock/time-clock-event.dto';

import { TimeClockEventsService } from '../src/service/time-clock-events.service';

describe('TimeClock Controller', () => {
    let app: INestApplication;
    let token: string;
    const timeClockEventEndPoint = '/api/time-clock-events';

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };

    const mockLoginUser: any = {
        username: 'admin',
        password: 'admin',
        rememberMe: true,
    };

    const timeClockEventEntityMock: any = {
        id: 44444444,
    };

    const serviceMock = {
        findById: (): any => timeClockEventEntityMock,
        findAndCount: (): any => [timeClockEventEntityMock, 0],
        addNoteEvent: (): any => timeClockEventEntityMock,
        updateNoteEvent: (): any => timeClockEventEntityMock,
        addPositionEvent: (): any => timeClockEventEntityMock,
        updatePositionEvent: (): any => timeClockEventEntityMock,
        startBreakTimeEvent: (): any => timeClockEventEntityMock,
        endBreakTimeEvent: (): any => timeClockEventEntityMock,
        deleteById: (): any => timeClockEventEntityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(TimeClockEventsService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();

        const loginRes = await request(app.getHttpServer())
            .post('/api/login')
            .send(mockLoginUser);

        token = loginRes.body.id_token;
    });

    it('/GET all time-clock-events ', async () => {
        const getEntities: TimeClockEventDTO[] = (
            await request(app.getHttpServer())
                .get(timeClockEventEndPoint)
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(timeClockEventEntityMock);
    });

    it('/GET time-clock-events by id', async () => {
        const getEntity: TimeClockEventDTO = (
            await request(app.getHttpServer())
                .get(`${timeClockEventEndPoint}/${timeClockEventEntityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(timeClockEventEntityMock);
    });

    it('/POST create break-start time-clock-events', async () => {
        const createdEntity: TimeClockEventDTO = (
            await request(app.getHttpServer())
                .post(`${timeClockEventEndPoint}/break`)
                .send(timeClockEventEntityMock)
                .set('Authorization', 'Bearer ' + token)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(timeClockEventEntityMock);
    });

    it('/PUT update break end time-clock-events from id', async () => {
        const updatedEntity: TimeClockEventDTO = (
            await request(app.getHttpServer())
                .put(`${timeClockEventEndPoint}/${timeClockEventEntityMock.id}/break`)
                .send(timeClockEventEntityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(timeClockEventEntityMock);
    });

    it('/POST add-notes time-clock-events', async () => {
        const createdEntity: TimeClockEventDTO = (
            await request(app.getHttpServer())
                .post(`${timeClockEventEndPoint}/note`)
                .send(timeClockEventEntityMock)
                .set('Authorization', 'Bearer ' + token)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(timeClockEventEntityMock);
    });

    it('/PUT update note time-clock-events from id', async () => {
        const updatedEntity: TimeClockEventDTO = (
            await request(app.getHttpServer())
                .put(`${timeClockEventEndPoint}/${timeClockEventEntityMock.id}/note`)
                .send(timeClockEventEntityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(timeClockEventEntityMock);
    });

    it('/POST add-position time-clock-events', async () => {
        const createdEntity: TimeClockEventDTO = (
            await request(app.getHttpServer())
                .post(`${timeClockEventEndPoint}/position`)
                .send(timeClockEventEntityMock)
                .set('Authorization', 'Bearer ' + token)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(timeClockEventEntityMock);
    });

    it('/PUT update-position time-clock-events from id', async () => {
        const updatedEntity: TimeClockEventDTO = (
            await request(app.getHttpServer())
                .put(`${timeClockEventEndPoint}/${timeClockEventEntityMock.id}/position`)
                .send(timeClockEventEntityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(timeClockEventEntityMock);
    });

    it('/DELETE time-clock-events', async () => {
        const deletedEntity: TimeClockEventDTO = (
            await request(app.getHttpServer())
                .delete(`${timeClockEventEndPoint}/${timeClockEventEntityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(timeClockEventEntityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
