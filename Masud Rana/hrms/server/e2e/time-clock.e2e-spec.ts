import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { TimeClockDTO } from './../src/service/dto/time-clock/time-clock.dto';

import { TimeClockService } from '../src/service/time-clock.service';

describe('TimeClock Controller', () => {
    let app: INestApplication;
    let token: string;
    const timeClockEndPoint = '/api/time-clocks';

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };

    const mockLoginUser: any = {
        username: 'admin',
        password: 'admin',
        rememberMe: true,
    };

    const timeClockEntityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => timeClockEntityMock,
        findAndCount: (): any => [timeClockEntityMock, 0],
        save: (): any => timeClockEntityMock,
        clockOut: (): any => timeClockEntityMock,
        update: (): any => timeClockEntityMock,
        deleteById: (): any => timeClockEntityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(TimeClockService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();

        const loginRes = await request(app.getHttpServer())
            .post('/api/login')
            .send(mockLoginUser);

        token = loginRes.body.id_token;
    });

    it('/GET all time-clocks ', async () => {
        const getEntities: TimeClockDTO[] = (
            await request(app.getHttpServer())
                .get(`${timeClockEndPoint}/all`)
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(timeClockEntityMock);
    });

    it('/GET time-clocks by id', async () => {
        const getEntity: TimeClockDTO = (
            await request(app.getHttpServer())
                .get(`${timeClockEndPoint}/${timeClockEntityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(timeClockEntityMock);
    });

    it('/POST create time-clocks', async () => {
        const createdEntity: TimeClockDTO = (
            await request(app.getHttpServer())
                .post(`${timeClockEndPoint}/in`)
                .set('Authorization', 'Bearer ' + token)
                .send(timeClockEntityMock)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(timeClockEntityMock);
    });

    it('/PUT update time-clocks from id', async () => {
        const updatedEntity: TimeClockDTO = (
            await request(app.getHttpServer())
                .put(`${timeClockEndPoint}/${timeClockEntityMock.id}/out`)
                .send(timeClockEntityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(timeClockEntityMock);
    });

    it('/DELETE time-clocks', async () => {
        const deletedEntity: TimeClockDTO = (
            await request(app.getHttpServer())
                .delete(`${timeClockEndPoint}/${timeClockEntityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(timeClockEntityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
