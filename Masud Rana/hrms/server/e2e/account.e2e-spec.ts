import { ExecutionContext, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { UserDTO } from '../src/service/dto/user.dto';
import { PasswordChangeDTO } from '../src/service/dto/password-change.dto';
import { RegisterCompanyDTO } from './../src/service/dto/auth/register-company.dto';

import { AuthService } from '../src/service/auth.service';
import { UserService } from '../src/service/user.service';
import { CompanyService } from '../src/service/company.service';

describe('Account', () => {
    let app: INestApplication;
    let service: UserService;
    let companyService: CompanyService;
    let authService: AuthService;
    const accountEndPoint = '/api';

    const testUserDTO: any = {
        fullName: 'test',
        email: 'test@gmail.com',
    };

    const testCompanyDTO: any = {
        name: 'Mind Orbital Technologies',
        phone: '01672066834',
        password: 'myuser',
        company_type_id: 1,
        application_type_id: 1,
        role_id: 1,
    };

    const testUserAuthenticated: any = {
        id: 42,
        username: 'userlogged',
        email: 'userlogged@localhost.it',
        password: 'userloggedPassword',
    };

    const testPasswordChange: PasswordChangeDTO = {
        currentPassword: testUserAuthenticated.password,
        newPassword: 'newPassword',
    };

    let userAuthenticated: UserDTO;

    const authGuardMock = {
        canActivate: (context: ExecutionContext): any => {
            const req = context.switchToHttp().getRequest();
            req.user = testUserAuthenticated;
            return true;
        },
    };

    const rolesGuardMock = { canActivate: (): any => true };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
        service = moduleFixture.get<UserService>(UserService);
        companyService = moduleFixture.get<CompanyService>(CompanyService);
        authService = moduleFixture.get<AuthService>(AuthService);
        userAuthenticated = await service.save(testUserAuthenticated, testUserAuthenticated.username, true);
    });

    it('/POST register new user with company', async () => {
        const createdUser: UserDTO = (
            await request(app.getHttpServer())
                .post(`${accountEndPoint}/register`)
                .send(testUserDTO)
                .expect(201)
        ).body?.data;

        const createdCompany: RegisterCompanyDTO = (
            await request(app.getHttpServer())
                .post(`${accountEndPoint}/register/company`)
                .send({ ...testCompanyDTO, user_id: createdUser.id })
                .expect(201)
        ).body?.data;

        expect(createdUser.email).toEqual(testUserDTO.email);

        expect(createdCompany.name).toEqual(testCompanyDTO.name);
        expect(createdCompany.phone).toEqual(testCompanyDTO.phone);
        expect(createdCompany.company_type_id).toEqual(testCompanyDTO.company_type_id);
        expect(createdCompany.application_type_id).toEqual(testCompanyDTO.application_type_id);

        await service.delete(createdUser.id);
        await companyService.deleteById(createdCompany.id);
    });

    it('/GET activate account', async () => {
        await request(app.getHttpServer())
            .get(`${accountEndPoint}/activate`)
            .expect(500);
    });

    it('/GET authenticate', async () => {
        const successFullyLoggedInWithNewPassword = await authService
            .login({
                username: testUserAuthenticated.username,
                password: testUserAuthenticated.password,
            })
            .then(
                () => true,
                () => false,
            );

        const loginValue: any = (
            await request(app.getHttpServer())
                .get(`${accountEndPoint}/authenticate`)
                .expect(200)
        ).body.data;

        expect(loginValue).toEqual(testUserAuthenticated.username);
    });

    it('/GET account', async () => {
        const loggedUser: any = JSON.parse(
            (
                await request(app.getHttpServer())
                    .get(`${accountEndPoint}/account`)
                    .expect(200)
            ).text,
        ).data;
        expect(loggedUser.id).toEqual(userAuthenticated.id);
        expect(loggedUser.username).toEqual(userAuthenticated.username);
        expect(loggedUser.email).toEqual(userAuthenticated.email);
    });

    it('/POST account update settings', async () => {
        const savedTestUser: UserDTO = {
            firstName: 'updateFirstName',
            lastName: 'updateLastName',
            ...testUserAuthenticated,
        };
        await request(app.getHttpServer())
            .post(`${accountEndPoint}/account`)
            .send(savedTestUser)
            .expect(201);

        const updatedUserSettings = await service.findByFields({
            where: { username: testUserAuthenticated.username },
        });
        expect(updatedUserSettings.firstName).toEqual(savedTestUser.firstName);
        expect(updatedUserSettings.lastName).toEqual(savedTestUser.lastName);
    });

    it('/POST change password', async () => {
        await request(app.getHttpServer())
            .post(`${accountEndPoint}/account/change-password`)
            .send(testPasswordChange)
            .expect(201);

        const successFullyLoggedInWithNewPassword = await authService
            .login({
                username: testUserAuthenticated.username,
                password: testPasswordChange.newPassword,
            })
            .then(
                () => true,
                () => false,
            );

        expect(successFullyLoggedInWithNewPassword).toEqual(true);
    });

    it('/POST reset password init', async () => {
        await request(app.getHttpServer())
            .post(`${accountEndPoint}/account/reset-password/init`)
            .expect(500);
    });

    it('/POST reset password finish', async () => {
        await request(app.getHttpServer())
            .post(`${accountEndPoint}/account/reset-password/finish`)
            .expect(500);
    });

    afterEach(async () => {
        await service.delete(userAuthenticated.id);
        await app.close();
    });
});
