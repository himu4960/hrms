import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { EmployeeDTO } from './../src/service/dto/employee/employee.dto';

import { EmployeeService } from '../src/service/employee.service';

describe('Employee Controller', () => {
    let app: INestApplication;
    let token: string;
    const employeeEndPoint = '/api/employees';

    const mockLoginUser: any = {
        username: 'admin',
        password: 'admin',
        rememberMe: true,
    };

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = [
        {
            id: 42,
            first_name: 'Dev',
            last_name: 'Teertha',
            email: 'devteertha28@gmail.com',
            company_id: 1,
            branch_id: 1,
        },
    ];

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        updateById: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(EmployeeService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();

        const loginRes = await request(app.getHttpServer())
            .post('/api/login')
            .send(mockLoginUser);

        token = loginRes.body.id_token;
    });

    it('/GET all employees ', async () => {
        const getEntities: EmployeeDTO[] = (
            await request(app.getHttpServer())
                .get(employeeEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET employees by id', async () => {
        const getEntity: EmployeeDTO = (
            await request(app.getHttpServer())
                .get(`${employeeEndPoint}/${entityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create employees', async () => {
        const createdEntity: EmployeeDTO = (
            await request(app.getHttpServer())
                .post(employeeEndPoint)
                .send(entityMock)
                .set('Authorization', 'Bearer ' + token)
                .expect(201)
        ).body?.data;
    });

    it('/PUT update employees', async () => {
        const updatedEntity: EmployeeDTO = (
            await request(app.getHttpServer())
                .put(employeeEndPoint)
                .set('Authorization', 'Bearer ' + token)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT update employees from id', async () => {
        const updatedEntity: EmployeeDTO = (
            await request(app.getHttpServer())
                .put(`${employeeEndPoint}/${entityMock.id}`)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE employees', async () => {
        const deletedEntity: EmployeeDTO = (
            await request(app.getHttpServer())
                .delete(`${employeeEndPoint}/${entityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
