import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { HolidayDTO } from '../src/service/dto/holiday.dto';

import { HolidayService } from '../src/service/holiday.service';

describe('Holiday Controller', () => {
    let app: INestApplication;

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(HolidayService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all holidays ', async () => {
        const getEntities: HolidayDTO[] = (await request(app.getHttpServer()).get('/api/holidays').expect(200)).body;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET holidays by id', async () => {
        const getEntity: HolidayDTO = (
            await request(app.getHttpServer())
                .get('/api/holidays/' + entityMock.id)
                .expect(200)
        ).body;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create holidays', async () => {
        const createdEntity: HolidayDTO = (
            await request(app.getHttpServer()).post('/api/holidays').send(entityMock).expect(201)
        ).body;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update holidays', async () => {
        const updatedEntity: HolidayDTO = (
            await request(app.getHttpServer()).put('/api/holidays').send(entityMock).expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT update holidays from id', async () => {
        const updatedEntity: HolidayDTO = (
            await request(app.getHttpServer())
                .put('/api/holidays/' + entityMock.id)
                .send(entityMock)
                .expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE holidays', async () => {
        const deletedEntity: HolidayDTO = (
            await request(app.getHttpServer())
                .delete('/api/holidays/' + entityMock.id)
                .expect(204)
        ).body;

        expect(deletedEntity).toEqual({});
    });

    afterEach(async () => {
        await app.close();
    });
});
