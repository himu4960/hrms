import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';

import { DesignationDTO } from './../src/service/dto/designation/designation.dto';

import { DesignationService } from '../src/service/designation.service';

describe('Designation Controller', () => {
    let app: INestApplication;
    let token: string;
    const designationEndPoint = '/api/designations';

    const mockLoginUser: any = {
        username: 'admin',
        password: 'admin',
        rememberMe: true,
    };

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(DesignationService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
        const loginRes = await request(app.getHttpServer())
            .post('/api/login')
            .send(mockLoginUser);

        token = loginRes.body.id_token;
    });

    it('/GET all designations ', async () => {
        const getEntities: DesignationDTO[] = (
            await request(app.getHttpServer())
                .get(designationEndPoint)
                .set('Authorization', 'Bearer ' + token)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET designations by id', async () => {
        const getEntity: DesignationDTO = (
            await request(app.getHttpServer())
                .get(`${designationEndPoint}/${entityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create designations', async () => {
        const createdEntity: DesignationDTO = (
            await request(app.getHttpServer())
                .post(designationEndPoint)
                .send(entityMock)
                .set('Authorization', 'Bearer ' + token)
                .expect(201)
        ).body?.data;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update designations', async () => {
        const updatedEntity: DesignationDTO = (
            await request(app.getHttpServer())
                .put(designationEndPoint)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT update designations from id', async () => {
        const updatedEntity: DesignationDTO = (
            await request(app.getHttpServer())
                .put(`${designationEndPoint}/${entityMock.id}`)
                .send(entityMock)
                .expect(201)
        ).body?.data;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE designations', async () => {
        const deletedEntity: DesignationDTO = (
            await request(app.getHttpServer())
                .delete(`${designationEndPoint}/${entityMock.id}`)
                .expect(200)
        ).body?.data;

        expect(deletedEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
