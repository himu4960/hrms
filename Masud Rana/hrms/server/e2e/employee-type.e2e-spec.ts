import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request = require('supertest');

import { AppModule } from '../src/app.module';

import { EmployeeTypeDTO } from './../src/service/dto/employee-type.dto';

import { EmployeeTypeService } from '../src/service/employee-type.service';

describe('EmployeeType Controller', () => {
    let app: INestApplication;
    const employeeTypeEndPoint = '/api/employee-types';

    const entityMock: any = {
        id: 1,
        name: 'entityMock',
        is_active: true,
        is_deleted: false,
    };

    const serviceMock = {
        getAll: (): any => entityMock,
        getActiveEmployeeTypes: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideProvider(EmployeeTypeService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET active employee-types ', async () => {
        const getEntities: EmployeeTypeDTO[] = (
            await request(app.getHttpServer())
                .get(employeeTypeEndPoint)
                .expect(200)
        ).body?.data;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET all employee-types', async () => {
        const getEntity: EmployeeTypeDTO[] = (
            await request(app.getHttpServer())
                .get(`${employeeTypeEndPoint}/all`)
                .expect(200)
        ).body?.data;

        expect(getEntity).toEqual(entityMock);
    });

    afterEach(async () => {
        await app.close();
    });
});
